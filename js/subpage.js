
// //$ = require('jquery');
var list = $('#kusal');
var c = null;
if (window.LMS && LMS.courses) {
    LMS.courses.forEach(function (course) {
        var item = $('<li class="list-group-item"><a href="#">' + course.name + '</a></li>');
        item.on('click', function (e) {
            e.preventDefault();
            c = window.open(
                'courses/' + course.id + '/sco-contents/index.html',
                'RAM STUDIO',
                'width=1280,height=720,resizable=no'
            );
            c.focus();
        });
        list.append(item);
    });
}