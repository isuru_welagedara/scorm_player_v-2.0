(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"civic_8.6_atlas_", frames: [[3060,722,660,413],[0,0,2256,1496],[2258,722,800,468],[3540,279,379,271],[2258,0,1280,720],[2258,1192,412,235],[3722,939,289,163],[3053,1375,378,251],[1572,1498,442,133],[3722,552,350,233],[1190,1498,380,213],[3060,1137,425,236],[3868,1367,220,165],[3540,0,380,277],[0,1498,407,229],[2724,1447,295,149],[2016,1498,233,178],[808,1498,380,228],[3433,1622,268,107],[2996,1628,204,124],[2251,1652,185,123],[3202,1628,191,127],[2438,1652,184,123],[3868,1534,228,123],[3922,0,170,116],[3922,118,159,107],[2258,1447,464,203],[3921,336,150,98],[1572,1633,190,127],[1764,1633,189,126],[3703,1659,190,107],[3922,227,140,107],[3487,1137,432,228],[3722,787,336,150],[2672,1192,379,253],[409,1498,397,228],[2724,1598,270,162],[3487,1367,379,253]]}
];


// symbols:



(lib.Image_0 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Image_1 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Image_2 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1_1 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap10 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap11 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap12 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap13 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap14 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap15 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap16 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap17 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap18 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap19 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap2 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap20 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap21 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap22 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap23 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap24 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap25 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap26 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap27 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap28 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap29 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap3 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap30 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap31 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap32 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap33 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap34 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap4 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap5 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap6 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap7 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap8 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(36);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap9 = function() {
	this.spriteSheet = ss["civic_8.6_atlas_"];
	this.gotoAndStop(37);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(7.7,11);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHIgPAVQgLAYASAZg");
	this.shape_1.setTransform(5,6.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(11.9,11.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,17.1,22.6), null);


(lib.next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhBhwICDBwIiDBx");
	this.shape.setTransform(3.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhBhwICDBwIiDBx");
	this.shape_1.setTransform(-3.2,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF7300","#F1C500"],[0,1],0,18.8,0,-18.7).s().p("AigC8QgcAAgTgUQgTgTAAgbIAAjzQAAgbATgTQATgUAcAAIFBAAQAcAAATAUQATATAAAbIAADzQAAAbgTATQgTAUgcAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.7,-18.7,45.5,37.6);


(lib.mov2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Layer 1
	this.instance = new lib.Bitmap15();
	this.instance.parent = this;
	this.instance.setTransform(-416,-198,1.373,1.373);

	this.instance_1 = new lib.Bitmap14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(114,-198,0.841,0.841);

	this.instance_2 = new lib.Bitmap13();
	this.instance_2.parent = this;
	this.instance_2.setTransform(114,7,0.668,0.668);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFD54F").s().p("AAAAwQgJAMgQAAQgXAAgMgRQgLgPABgbQABgcATgSQANgMASgCQAHAAAFABIAAAOQgIgDgLAEQgMADgHAMQgIALAAANQAAAkAZAAQAJAAAGgFQAGgFAAgKIAAgMIAOAAIAAAMQAAAKAHAFQAFAFAKAAQALAAAHgIQAHgHAAgMQAAgJgHgHQgIgHgLAAIgiAAQgBgVAJgLQAJgJARAAQAgABAAAZIgBAGIgLAAQALAEAGALQAHAKAAAMQAAAWgLAOQgMAOgTAAQgVAAgJgMgAAKgfIAVAAIAIABIABgEQAAgLgOAAQgPAAgBAOg");
	this.shape.setTransform(-215.1,186.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFD54F").s().p("AhFAiQgDgLADgJIgLAAIAAgLIAIgJIALgLQgHAAgEgFQgFgFAAgHQAAgLAJgHQAIgGALgBQASgBAJANQARgMAZAAIAIAAQAYACAPASQAQATgCAXQgBAYgPAQQgQARgYAAIgIAAIAAgMQgPAMgYAAIgDAAQglAAgHgagAgGgiQAGABAHAEQAIAEAFAFQALANAAAOQABAXgNAPIAFABQALAAAIgFQAPgKABgUQACgNgHgMQgGgLgNgGQgJgFgQAAQgKAAgGACgAgogPQgKAGgNANIAJAAQgEAJADAIQAFAQAZgBQAOgBAKgIQAJgIABgNQAAgLgIgIQgHgJgMAAQgMAAgKAHgAgxgtIgGADQAGABACADQACAEgCAFIAHgFIAGgFQgDgEgFgCIgEAAIgDAAg");
	this.shape_1.setTransform(-230.9,187);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFD54F").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_2.setTransform(-242,178.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD54F").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNIACgJQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape_3.setTransform(-247.2,187);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFD54F").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_4.setTransform(-258,187);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFD54F").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_5.setTransform(-268,185.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFD54F").s().p("AgCBRQgagBgQgQQgPgQgBgZQAAgQAGgMIACgDQAFgHAGgIIADgCIgDACQgGAIgFAHIAAgjIgLAAIAAgLIALAAIAAgQIgLAAIAAgKIAfAAQAKAAAGAEQAHAGAAAKQAAAJgIAFQgJAEgMAAIAAARIAEgEQAJgHARgRQAIgLAEgHIAYAAQgEAJgHAIQgIAIgKAIQARgBAMAEQAkAOAAAqQAAAagTASQgSAQgaAAIgDAAgAgfgMQgLAMAAAOQAAAPAKAKQANANAUAAQARAAAMgJQAPgMAAgSQAAgIgDgIQgEgLgMgGQgKgFgNAAQgWAAgMANgAgmgZIgDACIADgCIAAAAgAgmhFIAAAPQAMAAAAgHQAAgIgKAAIgCAAg");
	this.shape_6.setTransform(-281.9,184.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFD54F").s().p("AgVAzQgIAJgTAAQgLAAgHgHQgIgHAAgLQgBgMAGgIIgJAAIAAgJIAHgJIALgLIgDAAQgHAAgDgFQgEgEAAgHQAAgLAIgIQAIgJALgBQAUgBAJANQATgNAZABQAXACAQARQAQARABAXQABAngdARQgHAEgNAAIgLgBIAAgJQgJAKgLAAIgBAAQgLAAgJgJgAgJgkQASACALANQAOAPAAAUQAAAUgKALIAGABQAJAAAJgIQAIgIACgKIAAgJQAAgXgPgOQgNgMgXAAQgIAAgIACgAgngQQgLAKgKAKIAJAAQgFAIABAJQAAAEAEADQAEAEAGAAQAJAAADgKQACgGAAgQIANAAQAAARABAGQACAIAJAAQAHAAAFgIQAEgGAAgKQAAgKgGgJQgHgJgJgBIgGAAQgQAAgJAGgAg3gpQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_7.setTransform(-296.4,187);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFD54F").s().p("Ag7ApIAAgXIBqAAIAAglIAHgKQAEgFACgGIAABRg");
	this.shape_8.setTransform(-319.2,192.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFD54F").s().p("Ag5A+QgMgOABgTQABgWAPgIIgUAAIAAgOIAZAAQgFgCAAgHQAAgEACgCQAHgOAVAAIAJABQAQABAJAMQAJAMAAARIgrAAQgMAAgIAGQgIAHAAALQAAAKAHAGQAHAHALAAQAIAAAGgFQAGgFAAgIIAAgQIANAAIAAAQQAAAIAFAFQAGAFAJAAQAXAAADgpQACgZgRgUQgQgVgXAAQgkgBgNAXIgRAAQASgkAvABQAcAAATARQAOAMAHATQAHASgBATQgCAbgKARQgLAUgWAAQgSAAgJgLQgJALgTAAIgBAAQgRAAgMgNgAgagdQADAFAAADQAAADgCADIAcAAQgDgKgGgDQgGgCgHAAIgHABg");
	this.shape_9.setTransform(-319.5,185.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFD54F").s().p("AACBKQgIgEgGgJQgCAHgGAEQgHAFgKAAQgQAAgKgSQgJgPAAgWQAAgYAOgTQAQgVAZAAQARAAAMAMQALAMAAARQAAANgKAKIgGAFQgDADgBAFQAAANATAAQAOAAAJgPQAIgOAAgTQAAgYgPgTQgPgTgdAAQgOAAgNAGQgNAHgGALIgQAAQAHgSASgKQAQgKAVAAQAjAAAUAUQAKAKAHAQQAGARABAOQABAkgQAVQgOATgWAAQgMAAgIgDgAgqAuQgCACAAAEQAAAEACACQADADAEAAQAEAAACgDQADgCAAgEQAAgEgDgCQgDgDgDAAQgEAAgDADgAg0ADQgEAKAAALQAAAJADAHQAGgLANgBQALgBAJAHQABgIAHgGQAIgIgBgJQAAgIgFgFQgGgFgJAAQgYAAgJASg");
	this.shape_10.setTransform(-334.2,185.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFD54F").s().p("AgyA0QgTgWAAggQABgfAUgWQAUgUAgAAQAcAAATASQATASAAAcQAAAWgNARQgNAQgVAAQgVABgMgOQgNgMACgUQACgRANgLIgQAAIAAgNIA2AAIAAANIgKAAQgKABgHAGQgGAGgBAKQAAAJAGAHQAIAGALgBQANABAJgMQAHgIgBgOQgBgRgIgLQgMgPgbAAQgVAAgNATQgLAQAAAYQAAAWALARQALASAUADIALAAQAZAAANgTIARAAQgRAlgvAAQggAAgUgYg");
	this.shape_11.setTransform(-356.3,188.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFD54F").s().p("AhFAiQgDgLADgJIgLAAIAAgLIAIgJIALgLQgHAAgEgFQgFgFAAgHQAAgLAJgHQAIgGALgBQASgBAJANQARgMAZAAIAIAAQAYACAPASQAQATgCAXQgBAYgPAQQgQARgYAAIgIAAIAAgMQgPAMgYAAIgDAAQglAAgHgagAgGgiQAGABAHAEQAIAEAFAFQALANAAAOQABAXgNAPIAFABQALAAAIgFQAPgKABgUQACgNgHgMQgGgLgNgGQgJgFgQAAQgKAAgGACgAgogPQgKAGgNANIAJAAQgEAJADAIQAFAQAZgBQAOgBAKgIQAJgIABgNQAAgLgIgIQgHgJgMAAQgMAAgKAHgAgxgtIgGADQAGABACADQACAEgCAFIAHgFIAGgFQgDgEgFgCIgEAAIgDAAg");
	this.shape_12.setTransform(-372,187);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFD54F").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_13.setTransform(-383.1,178.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFD54F").s().p("AhFAiQgDgLADgJIgLAAIAAgLIAIgJIALgLQgHAAgEgFQgFgFAAgHQAAgLAJgHQAIgGALgBQASgBAJANQARgMAZAAIAIAAQAYACAPASQAQATgCAXQgBAYgPAQQgQARgYAAIgIAAIAAgMQgPAMgYAAIgDAAQglAAgHgagAgGgiQAGABAHAEQAIAEAFAFQALANAAAOQABAXgNAPIAFABQALAAAIgFQAPgKABgUQACgNgHgMQgGgLgNgGQgJgFgQAAQgKAAgGACgAgogPQgKAGgNANIAJAAQgEAJADAIQAFAQAZgBQAOgBAKgIQAJgIABgNQAAgLgIgIQgHgJgMAAQgMAAgKAHgAgxgtIgGADQAGABACADQACAEgCAFIAHgFIAGgFQgDgEgFgCIgEAAIgDAAg");
	this.shape_14.setTransform(-388.8,187);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFD54F").s().p("AAnAcQAKgHAAgJIgBgDQgCgIgPgCQgNgEgSAAQgRABgNADQgPADgCAHIgBADQAAAKAJAGIgNAAQgLgEAAgQQAAgHACgCQAHgPASgGQAOgEAWAAQAzgBALAaQACADAAAGQAAAPgLAFg");
	this.shape_15.setTransform(-404.7,179.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFD54F").s().p("Ag1ArQgQgOAAgZQAAgJAFgJQAEgJAHgEQgIgDAAgJIABgGQAGgOAaAAQAVAAAHALQAIgLAXAAQALAAAHAEQAJAGABAJQACAHgEAHQAFAEAEAIQAEAJAAAJQAAAZgRAPQgUAQghAAQghAAgUgRgAgwgNQgFAGABAKQAAAPASAHQAOAGAUAAQAUAAAOgGQARgHABgPQAAgKgFgGQgGgGgJAAIhBAAQgJAAgGAGgAAPgqQgGAEgBAGIAhAAIAAgDQAAgFgEgDQgEgDgFAAQgHAAgGAEgAglgtQADACABAEQABAEgCADIAZAAQgBgLgPgDIgFAAQgEAAgDABg");
	this.shape_16.setTransform(-404.7,187);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("Ag+BPQgZgTACggQABgbAXgLQgMgDAAgNQAAgPAQgHQANgHAQABQAUABAMAQQANARgBAUIgzAAQgLAAgIAGQgIAHAAAKQABAbAjAIQALABAPAAQAdAAASgTQARgSAAgbQABgegRgVQgSgWgjAAQgmgCgUAdIgTAAQAJgVAWgMQAVgLAaAAQAnAAAZAbQAaAcAAAoQAAAogXAbQgYAdgnAAQgmAAgYgRgAgpgjQADADACAFQACAGgDAGIAgAAQgBgJgHgGQgHgGgKAAIgLABg");
	this.shape_17.setTransform(-302.7,149.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AghBGIAAgRQAEADAKAAQAJAAAGgRQAGgRAAgUQAAgUgFgRQgHgTgJgBQgIAAgGAFIAAgSQAHgGAPAAQAVgBANAbQALAXAAAbQAAAcgMAVQgNAZgVAAQgMAAgJgGg");
	this.shape_18.setTransform(-315.4,151.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Ag/BBQgXgcAAgoQABgoAZgZQAYgaApAAQAjAAAYAXQAXAXAAAiQAAAcgPAUQgRAWgaAAQgaAAgQgRQgQgQADgYQACgWARgMIgUAAIAAgSIBDAAIAAASIgNAAQgMAAgJAHQgIAIAAAMQAAALAIAJQAJAIAOgBQARAAAKgNQAJgMgBgRQgBgWgKgNQgQgSghAAQgbAAgQAYQgOATAAAeQAAAcAOAVQAOAWAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgageg");
	this.shape_19.setTransform(-327.7,153.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AhWArQgEgOADgLIgOAAIAAgOIALgMIAOgOQgKAAgFgGQgFgGAAgJQAAgNALgJQAJgIAPgBQAXgBAKAQQAWgPAeAAIALAAQAeACATAXQATAYgBAdQgCAegTAUQgUAWgdAAIgLgBIAAgPQgTAPgeABIgDAAQguAAgJghgAgIgrQAIABAJAFQAKAGAFAGQAOAQABASQAAAdgPATIAGABQAOAAAJgHQATgNACgYQACgRgIgOQgIgPgQgIQgMgGgUAAQgMAAgIADgAgzgTQgLAIgRAQIAMAAQgFALADALQAHAUAfgCQARgBAMgKQAMgKABgRQABgNgKgLQgKgLgPAAIgCAAQgOAAgMAJgAg+g4QgFABgCACQAHACACAEQADAFgCAGIAIgHIAIgGQgEgFgGgCIgEgBIgFABg");
	this.shape_20.setTransform(-347.3,151.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgpBYQgSgJgLgPQgPgXAAgiQAAgWAMgTQANgTASABQAQABAFANQAJgPASAAQAQAAAMANQALAMABARQACASgPAQQgQAOgTAAQgTABgMgMQgNgMABgQQAAgHADgFIAEgLQABgDgCgCQgDgDgEABQgIABgFALQgFAKAAANQgBAYATARQATARAagBQAaABATgUQATgUgBgaQAAghgQgTQgRgWggAAQgoABgQAdIgUAAQAJgXAVgMQAUgLAaAAQAoAAAWAWQAaAaABArQACAqgYAeQgYAcgpABQgWgBgSgIgAgZgHQAAAIAIAGQAIAFAJAAQALABAJgIQAIgGAAgKIAAgFQgCAGgHAFQgHADgJAAQgUAAgEgSIgEANgAgEgcQAAAFADADQADADAEAAQAEAAAEgDQADgDAAgFQAAgDgEgEQgDgDgEAAQgKAAAAAKg");
	this.shape_21.setTransform(-367,149.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAwAiQANgIAAgLIgBgEQgDgJgSgEQgRgEgWAAQgVAAgRAEQgTAEgCAJIgBAEQAAAMALAHIgQAAQgOgEAAgVQAAgHACgEQAJgSAXgIQARgFAcAAQBAAAANAfQADAEAAAHQAAAUgPAFg");
	this.shape_22.setTransform(-385.5,142.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Ag4BDQgagVAAgiQAAgfAWgRQgBgQAGgLQANgZAbACQANABAJAKQAJgLATAAQASABAJALQAKANAAAdIhIAAIgfACQAAATAOANQAOAOATgCQAOgBALgJQALgHACgNIASAAQgEAWgRANQgRAOgXAAQgrAAgSgfIgBALQAAAWARAPQARAOAWAAQAigBARgUQAKgMgBgPIASAAQACATgNASQgYAiguAAQgiAAgYgUgAgbg+QgEAFABAJIANgBIAPAAQAAgTgOAAQgHAAgEAGgAAQgxIAdAAQAAgTgNAAQgQAAAAATg");
	this.shape_23.setTransform(-384.8,153);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAwAiQANgIAAgLIgBgEQgDgJgSgEQgRgEgWAAQgVAAgRAEQgTAEgCAJIgBAEQAAAMALAHIgQAAQgOgEAAgVQAAgHACgEQAJgSAXgIQARgFAcAAQBAAAANAfQADAEAAAHQAAAUgPAFg");
	this.shape_24.setTransform(-402.9,142.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AhCA2QgVgSAAgeQAAgMAGgLQAFgLAJgGQgJgEAAgLQAAgEABgDQAHgSAhAAQAZAAAKAOQALgOAbAAQAOAAAKAGQALAGACAMQABAIgFAJQAHAFAFALQAFALAAAMQAAAfgWASQgZAUgpAAQgqAAgYgVgAg8gQQgHAIABAMQABASAWAKQASAHAZAAQAYAAASgHQAWgKABgTQABgLgHgIQgHgIgMAAIhRAAQgMAAgHAIgAATg1QgIAFgBAIIApAAIABgEQAAgGgFgDQgGgEgGAAIgBAAQgIAAgHAEgAgug4QAEADABAEQABAFgDAEIAfAAQgBgNgSgEIgHgBQgFAAgDACg");
	this.shape_25.setTransform(-402.9,151.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#110408").s().p("EhGJAm0QgyAAgigjQgkgjAAgyMAAAhJ3QAAgyAkgjQAigjAyAAMCMTAAAQAxAAAkAjQAjAjAAAyMAAABJ3QAAAygjAjQgkAjgxAAg");

	this.instance_3 = new lib.Bitmap21();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-408,-219,1.461,1.461);

	this.instance_4 = new lib.Bitmap20();
	this.instance_4.parent = this;
	this.instance_4.setTransform(159,-219,1.047,1.047);

	this.instance_5 = new lib.Bitmap19();
	this.instance_5.parent = this;
	this.instance_5.setTransform(159,-25,0.608,0.608);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFD54F").s().p("AADBJQgLgFAAgLQgBgKAHgEQgZABgRgOQgRgOABgVQACgZAVgMQgEgBgCgEQgDgEAAgEIABgFQAFgKALgEQAIgCAPAAQAcAAAMAOQAJALABAQIgrAAQgMAAgJAJQgJAIgBAMQgBANAMALQALAKAPAAQAYABARgJIAAAOQgIAEgIACQgPAEAAAGQgBAFAFACQAEADAGAAQAGAAAFgDQAFgEAAgHIAAgFIANAAIABAGQAAANgLAIQgJAIgOABIgHAAQgJAAgIgEgAgSg9IADACQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQABAFgFACIAHgBIAjAAQgFgJgLgDQgFgCgJAAQgJABgEABg");
	this.shape_27.setTransform(-244.3,185.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFD54F").s().p("Ag1ArQgQgOAAgZQAAgJAEgJQAFgJAGgEQgGgDAAgJIABgGQAFgOAaAAQAUAAAIALQAJgLAWAAQAKAAAIAEQAKAGAAAJQACAHgEAHQAGAEADAIQAEAJAAAJQAAAZgSAPQgTAQghAAQgiAAgTgRgAgwgNQgFAGABAKQAAAPASAHQAOAGAUAAQATAAAOgGQATgHAAgPQABgKgGgGQgFgGgKAAIhBAAQgJAAgGAGgAAPgqQgGAEgBAGIAhAAIAAgDQAAgFgDgDQgFgDgFAAQgHAAgGAEgAglgtQADACABAEQABAEgCADIAZAAQgBgLgPgDIgFAAQgEAAgDABg");
	this.shape_28.setTransform(-263.7,184);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFD54F").s().p("Ag1ArQgQgOAAgZQAAgTAPgIQgHgDAAgKQAAgEACgEQAHgPAYAAQAUAAAIAMQAJgMAWAAQALAAAJAGQAKAHgBALQAAAJgCAEQAMAMAAAOQAAAZgSAPQgTAQghAAQgiAAgTgRgAgzADQAAAPASAHQAOAGATAAQAVAAANgGQATgHAAgPQAAgHgEgGQgIAFgPAEQAHACAAAIQAAAHgLAEQgIAEgLAAQgNAAgKgEQgKgFAAgIQAAgPAaAAIAMgBQARgDAEgCIg8AAQgWAAACARgAgLAHQAAAFAMAAQAMAAAAgFQABgFgNAAQgMAAAAAFgAAOgoQgFAFgBAHIAiAAIABgFQAAgDgBgCQgEgHgKAAQgIAAgGAFgAglgsQAFACABAFQAAAFgDAEIAbAAQAAgMgPgFIgHgBQgEAAgEACg");
	this.shape_29.setTransform(-278.5,184);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFD54F").s().p("AAnAbQAKgGAAgJIgBgDQgCgIgPgCQgNgEgSAAQgRAAgNAEQgPADgCAHIgBADQAAAKAJAFIgNAAQgLgDAAgQQAAgHACgCQAHgPASgGQAOgEAWAAQAzgBALAaQACADAAAGQAAAPgLAEg");
	this.shape_30.setTransform(-293.4,176.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFD54F").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNIACgJQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape_31.setTransform(-293.7,184);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFD54F").s().p("AAnAbQAKgGAAgJIgBgDQgCgIgPgCQgNgEgSAAQgRAAgNAEQgPADgCAHIgBADQAAAKAJAFIgNAAQgLgDAAgQQAAgHACgCQAHgPASgGQAOgEAWAAQAzgBALAaQACADAAAGQAAAPgLAEg");
	this.shape_32.setTransform(-333.2,176.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFD54F").s().p("Ag6AKQgEgHAAgIQAAgHAEgFIAMAAQgCADAAAEQAAAFAEAEQAMAMAgAAQASAAAOgFQAIgDAEgEQAFgEAAgIQAAgIgHgHIAKgGQALAJAAARQAAAIgCAFQgMAegxAAQgrAAgPgZg");
	this.shape_33.setTransform(-333.2,190.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFD54F").s().p("AgaA8QgXAAgOgSQgLgQAAgYQAAgdAOgPQAGgHAHgEQAKgGALAAIALABIAAANQAPgPAXABQAZABAOAQQANAQAAAZQABAZgPASQgPASgYAAIgIAAIAAgNQgKAHgGADQgIADgMAAIgEAAgAAQAtIAFABQAJAAABgFIABgDIgEgOQgCALgKAKgAApARIACAJQADAHAAAEQAJgJAAgMQAAgGgHAAQgHAAAAAHgAgtgjQgJAJgBAOQgCAQAIANQAJANASABQAMAAAJgFQAJgGABgKQABgGgDgEQgEgGgFAAQgFAAgDAEQgEACAAAGIABAFIgOAAQgDgOACgOQABgNALgOIgHgBQgNAAgJAKgAAIgTQASAGABAPQAEgGAJgBQAIgBAFAEQgCgMgJgIQgJgKgOgBQgMAAgHAEQgLAEgCAKQAEgFAKAAIAHABg");
	this.shape_34.setTransform(-333.6,183.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFD54F").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_35.setTransform(-344.4,183.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFD54F").s().p("AAUBJIAAgaQgSABgNAAQgbgBgQgMQgSgOAAgXQAAgMAHgKQAGgJAJgFQgEgBgDgEIgBgHQAAgHAGgGQAMgJAVABQAgAAAIASQADgIAHgFQAHgFAIAAIAOAAIAAANIgCgBQgFAAAAAFQAAACAEADIAKAJQAFAGABAIQAAAOgNAIQgLAFgOgBIAAAfIAMgEIAKgDIAAANIgKAEIgMADIAAAdgAghgVQgHAJAAAMQAAAaAdAGIAPACIAQgBIAAg/IghAAQgMAAgIAJgAAjgkIAAAYIADAAQAFAAAEgDQAFgEAAgFQAAgHgGgIQgGgIAAgEIAAgDQgFACAAAQgAgcg4QADADAAAEQAAAEgDACIADgBIADAAIAfAAQgDgIgKgEQgHgCgGAAQgGAAgFACg");
	this.shape_36.setTransform(-354.5,185.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFD54F").s().p("AgxBBQgSgRABgeQABgUAOgNQAPgNAVAAIAQAAIAAAOIgMAAQgRAAgKAHQgKAHAAANQgBATAQAMQAOAJAUAAQAUAAAOgLQAPgOgBgUQAAgYgNgLQgLgJgRAAIgsAAQgKgBgFgEQgGgFAAgHQAAgPAQgIQANgFAbAAQAgAAAPANQAPANgCANIgPAAIAAgCQAAgIgKgGQgOgJgUAAQgaABgGAGQgBABAAAAQAAABgBABQAAAAAAABQAAAAAAABQAAAFAKAAIAmAAQAPAAAOAIQAYAQAAAjQAAAggSAVQgSAUgfAAQgfAAgSgRg");
	this.shape_37.setTransform(-376.3,181.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFD54F").s().p("AgZAxQgLgJAAgRQAAgNAEgJQAEgFAIgKQAJgKAYgTIguAAIAAgOIBEAAIAAANIgQAMQgIAGgGAGQgJAMgDAEQgGAJABAKQAAAHAFAEQAFADAFABQANgBADgKIACgHQAAgEgDgEIANAAQAHAKgBAMQgBAOgLAJQgLAJgOAAQgPAAgKgJg");
	this.shape_38.setTransform(-387.3,184.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AABBQQgMAOgWAAQgXAAgPgRQgPgSABgXQABgbATgLIgZAAIAAgRIAfAAQgGgDAAgJQAAgEACgEQAJgQAaAAIAMABQATABAMAQQAMAOgBAVIg2AAQgOAAgKAIQgKAJgBAOQAAAMAJAIQAIAJAOAAQAKAAAIgHQAHgGAAgLIAAgUIARAAIAAAVQAAAKAHAGQAHAHALAAQAdgBADgzQACgggUgZQgUgageAAQgtAAgQAcIgUAAQAWgtA6ABQAjAAAYAVQASAPAIAYQAJAXgBAYQgCAhgMAVQgPAagbAAQgXAAgLgOgAggglQADAGAAAEQAAAFgDADIAkAAQgEgMgIgEQgHgCgIAAIgJAAg");
	this.shape_39.setTransform(-298.7,146.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgiBGIAAgSQAGAEAJABQAJgBAGgRQAGgRAAgUQAAgUgFgRQgHgTgIgBQgJAAgHAFIAAgSQAJgGAPAAQAUgBANAbQAMAXAAAbQgBAcgMAVQgNAZgUAAQgNAAgKgGg");
	this.shape_40.setTransform(-311.4,148.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AARBKIAAgOQgMAJgIADQgLADgSAAQgegBgRgWQgOgUAAgdQAAgyAigUQALgHATAAIAJABIAAARQAVgTAbABQAgABARAVQAQATABAgQAAAggRAVQgTAXgfAAIgKgBgAALgYQAaAHgBAfQAAAYgQATQAQACAMgHQATgNgCggQAAgSgMgOQgMgPgSgBQgigCgFAXQAIgFAKAAIAJABgAg8gqQgKAOAAATQAAAUALANQALAOAUABQAQABAMgIQALgHABgNQABgHgFgFQgEgHgHAAQgGABgEAEQgDADAAAGIABAHIgSAAQgEgSACgRQACgQANgSIgLgCQgSAAgLAPg");
	this.shape_41.setTransform(-324.4,148.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgvA2QgWgVAAggQAAghAWgVQAVgVAfAAQAjAAAPAPQALAJABAOIgRAAQgBgJgOgHQgMgEgQAAQgVAAgPANQgRAPAAAUQAAAVAPANQAOANAWAAQAQAAANgJQANgKAAgQQABgUgUgGQAIAHgBANQABAJgKAIQgJAHgKAAQgNAAgKgIQgJgIAAgOQAAgOAMgJQALgIAQAAQAXAAAQANQARAOAAAWQAAAcgTATQgUASgbAAQgeAAgVgVgAgDgRQgEADAAAGQAAAGAEACQADAEAFAAQAFAAAEgEQADgCAAgGQAAgFgDgEQgEgDgEAAQgGAAgDADg");
	this.shape_42.setTransform(-341.4,148.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgiBGIAAgSQAGAEAJABQAJgBAHgRQAFgRAAgUQAAgUgFgRQgHgTgIgBQgJAAgHAFIAAgSQAJgGAPAAQAUgBANAbQAMAXAAAbQgBAcgLAVQgOAZgUAAQgNAAgKgGg");
	this.shape_43.setTransform(-369.8,148.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgpBYQgSgJgLgPQgPgXAAgiQAAgVAMgTQANgUASACQAQAAAFAMQAJgOASAAQAQAAAMANQALAMABAQQACATgPAQQgQAPgTAAQgTAAgMgMQgNgMABgQQAAgGADgGIAEgKQABgEgCgCQgDgDgEABQgIABgFALQgFAKAAANQgBAZATAQQATAQAaAAQAaABATgUQATgUgBgaQAAghgQgTQgRgWggABQgoAAgQAdIgUAAQAJgXAVgMQAUgLAaAAQAoAAAWAWQAaAaABArQACArgYAdQgYAcgpAAQgWAAgSgIgAgZgHQAAAIAIAFQAIAGAJAAQALAAAJgHQAIgHAAgJIAAgFQgCAGgHAFQgHADgJAAQgUAAgEgSIgEANgAgEgcQAAAFADACQADAEAEAAQAEAAAEgDQADgDAAgFQAAgEgEgDQgDgDgEAAQgKAAAAAKg");
	this.shape_44.setTransform(-382.3,146.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgvA2QgWgVAAggQAAghAWgVQAVgVAfAAQAjAAAPAPQAMAJAAAOIgRAAQgBgJgOgHQgMgEgPAAQgWAAgPANQgRAPAAAUQAAAVAPANQAOANAWAAQAQAAANgJQANgKAAgQQABgUgUgGQAIAHgBANQABAJgKAIQgJAHgKAAQgNAAgKgIQgJgIAAgOQAAgOAMgJQALgIAQAAQAXAAAQANQARAOAAAWQAAAcgTATQgUASgbAAQgeAAgVgVgAgDgRQgEADAAAGQAAAGAEACQADAEAFAAQAFAAAEgEQADgCAAgGQAAgFgDgEQgDgDgFAAQgGAAgDADg");
	this.shape_45.setTransform(-398.6,148.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#110408").s().p("EhGJAm0QgxAAgkgjQgjgjAAgyMAAAhJ3QAAgyAjgjQAkgjAxAAMCMTAAAQAyAAAjAjQAjAjAAAyMAAABJ3QAAAygjAjQgjAjgyAAg");
	this.shape_46.setTransform(0.5,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFD358").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_47.setTransform(-131.2,190.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFD358").s().p("AgsBEQgTgMgBgVQgCgXATgJQgFgBgEgDQgDgEAAgGIABgFQAGgQAfACQASABAIANQAJALgBARIgoAAQgTAAAAAPQAAALAPAGQAMAFASAAQAUAAANgLQAOgKABgQIABgJQAAgQgLgMQgJgLgQAAIgvAAQgWAAAAgPQAAgaA5AAQAiAAANAKQAJAHAAAJQAAAJgIAGQASAVAAAfQgBAcgUARQgUARgbAAQgZAAgRgKgAgggQQADACABAFQAAAFgDAEIAcAAQgCgIgFgEQgGgFgKAAIgGABgAAag6QgCACAAAEQAAADACADQADACADAAQAEAAACgCQADgDAAgDQAAgEgDgCQgCgCgEAAQgDAAgDACgAggg2QgBAFAKAAIAhAAQgCgHAEgGIgOgBQgdAAgBAJg");
	this.shape_48.setTransform(-145.3,190.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFD358").s().p("AADBJQgLgFAAgLQgBgKAHgEQgZABgRgOQgRgOABgVQACgZAVgMQgEgBgCgEQgDgEAAgEIABgFQAFgKALgEQAIgCAPAAQAcAAAMAOQAJALABAQIgrAAQgMAAgJAJQgJAIgBAMQgBANAMALQALAKAPAAQAYABARgJIAAAOQgIAEgIACQgPAEAAAGQgBAFAFACQAEADAGAAQAGAAAFgDQAFgEAAgHIAAgFIANAAIABAGQAAANgLAIQgJAIgOABIgHAAQgJAAgIgEgAgSg9IADACQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQABAFgFACIAHgBIAjAAQgFgJgLgDQgFgCgJAAQgJABgEABg");
	this.shape_49.setTransform(-157.5,194.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFD358").s().p("Ag1ArQgQgOAAgZQAAgJAEgJQAFgJAHgEQgIgDABgJIAAgGQAGgOAaAAQAVAAAHALQAJgLAWAAQAKAAAIAEQAKAGAAAJQACAHgEAHQAFAEAEAIQAEAJAAAJQAAAZgRAPQgUAQghAAQghAAgUgRgAgwgNQgFAGABAKQAAAPASAHQAOAGAUAAQAUAAANgGQATgHAAgPQABgKgGgGQgFgGgKAAIhBAAQgJAAgGAGgAAPgqQgGAEgBAGIAhAAIAAgDQAAgFgDgDQgFgDgFAAQgHAAgGAEgAglgtQADACABAEQABAEgCADIAZAAQgBgLgPgDIgFAAQgEAAgDABg");
	this.shape_50.setTransform(-171.2,192.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFD358").s().p("AAnAbQAKgGAAgJIgBgEQgCgHgPgDQgNgCgSAAQgRgBgNAEQgPADgCAGIgBAEQAAAKAJAFIgNAAQgLgDAAgRQAAgFACgDQAHgPASgFQAOgFAWgBQAzAAALAaQACADAAAFQAAAQgLAEg");
	this.shape_51.setTransform(-186.1,184.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFD358").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNQAAgEACgFQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape_52.setTransform(-186.4,192.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFD358").s().p("Ag5A+QgMgOABgTQABgWAPgIIgUAAIAAgOIAZAAQgFgCAAgHQAAgEACgCQAHgOAVAAIAJABQAQABAJAMQAJAMAAARIgrAAQgMAAgIAGQgIAHAAALQAAAKAHAGQAHAHALAAQAIAAAGgFQAGgFAAgIIAAgQIANAAIAAAQQAAAIAFAFQAGAFAJAAQAXAAADgpQACgZgRgUQgQgVgXAAQgkgBgNAXIgRAAQASgkAvABQAcAAATARQAOAMAHATQAHASgBATQgCAbgKARQgLAUgWAAQgSAAgJgLQgJALgTAAIgBAAQgRAAgMgNgAgagdQADAFAAADQAAADgCADIAcAAQgDgKgGgDQgGgCgHAAIgHABg");
	this.shape_53.setTransform(-209.1,190.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFD358").s().p("AgVAzQgIAJgTAAQgLAAgHgHQgIgHAAgLQgBgMAGgIIgJAAIAAgJIAHgJIALgLIgDAAQgHAAgDgFQgEgEAAgHQAAgLAIgIQAIgJALgBQAUgBAJANQATgNAZABQAXACAQARQAQARABAXQABAngdARQgHAEgNAAIgLgBIAAgJQgJAKgLAAIgBAAQgLAAgJgJgAgJgkQASACALANQAOAPAAAUQAAAUgKALIAGABQAJAAAJgIQAIgIACgKIAAgJQAAgXgPgOQgNgMgXAAQgIAAgIACgAgngQQgLAKgKAKIAJAAQgFAIABAJQAAAEAEADQAEAEAGAAQAJAAADgKQACgGAAgQIANAAQAAARABAGQACAIAJAAQAHAAAFgIQAEgGAAgKQAAgKgGgJQgHgJgJgBIgGAAQgQAAgJAGgAg3gpQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_54.setTransform(-224.4,192.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFD358").s().p("AgpBMQgagBgQgQQgPgQgBgYQAAgQAGgMQAGgMAOgMIAagZQAJgLAEgGIAYAAQgEAJgIAIQgHAIgMAHQASgBANAFQAjANAAAqQAAAbgTARQgRAQgaAAIgEAAgAhGgQQgLAMAAAOQAAAPAKAJQANANAVAAQARAAALgJQAPgLAAgTQAAgIgDgHQgDgLgMgGQgKgGgNAAQgXAAgMAOgAAkBAQgIgNAAgUQAAgZAUgZQAIgKAIgGIgkAAIAAgNIAegMIg9AAIAAgOIBeAAIAAANIgeAMIAeAAIAAAMQgMAGgJAJQgJAIgGALQgHANAAANQAAAYAUAAQAHAAAFgDQAFgEAAgHQAAgFgCgGIAMAAQAFAGAAAOQAAAPgKAKQgLAJgPAAQgSAAgKgMg");
	this.shape_55.setTransform(-242.9,190.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFD358").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_56.setTransform(-263.6,192.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFD358").s().p("AARAuQANAAAJgFQAKgGAEgLQAEgMAAgMQgCgNgJgJQgJgLgOAAQgWACgFATQAEgDAFAAQAHAAAEADQANAJgBAVQAAATgRAMQgPALgVAAQgWAAgPgNQgOgOAAgUQAAgWAQgLQgFgBgDgEQgDgFAAgGQAAgLAKgGQAIgGAMAAQAKAAAIAGQAHAHADAKQANgWAbgBQAWAAAPATQANARAAAYQAAAZgQARQgRARgbAAgAg0gHQgGAGAAAIQAAAMALAHQAKAGAPAAQAMAAAKgGQAJgFABgKQABgEgCgFQgDgDgEABQgDAAgCABQgDADABAEIgOAAIAAgLIABgKIgSAAQgJAAgHAGgAgwgrQADACACAFQACAFgCAFIAPAAQAAgJgFgFQgEgEgHAAIgEABg");
	this.shape_57.setTransform(-274.2,192.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgJQAEgHACgFIAABRg");
	this.shape_58.setTransform(-296.8,198.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFD358").s().p("Ag3BGQgQgGgFAAQgGAAgHAIIgLgHIAWgfQgKgLAAgTQAAgGACgGQAFgTARgKQAMgHASAAIAIAAIAVgeIAPAAIgUAfIAKAFIAJAGIAGgHQAHgFAIAAQAKAAAHAHQAJgIALABQAgABACA3QAAAZgJARQgLAUgXABQgPAAgIgJQgIgIAAgOIABgGIAOAAIAAAEQAAASAPgBQAKAAAEgPQACgIAAgVQABgPgDgMQgDgPgGAAQgKAAAAAOIAAAcIgPAAIAAgcQAAgPgIAAQgEgBgDADQgDADAAADQAMAOgBAWQgBAbgQAQQgPAPgbABIgCAAQgLAAgNgFgAg6A2QALAFALAAIAKgBQAPgCAJgMQAIgLAAgPQAAgOgJgKQgJgLgPgBIgMASIAHAAQAIAAAGAEQAGAFABAIQABAKgFAIQgFAHgJADQgHACgIAAQgOAAgKgFIgHALIAFgBQAGAAAGACgAg7AeQACACAFABQAJACADgCQgIgCgCgFQgCgEABgFgAgnATQAAAHAHAAQAGAAAAgHQAAgHgGAAQgHAAAAAHgAhBgFQgFAGAAAKIABAJIAagmQgPADgHAKg");
	this.shape_59.setTransform(-299.9,191);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgJQAEgHACgFIAABRg");
	this.shape_60.setTransform(-316.9,198.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFD358").s().p("AARAuQANAAAJgFQAKgGAEgLQAEgMAAgMQgCgNgJgJQgJgLgOAAQgWACgFATQAEgDAFAAQAHAAAEADQANAJgBAVQAAATgRAMQgPALgVAAQgWAAgPgNQgOgOAAgUQAAgWAQgLQgFgBgDgEQgDgFAAgGQAAgLAKgGQAIgGAMAAQAKAAAIAGQAHAHADAKQANgWAbgBQAWAAAPATQANARAAAYQAAAZgQARQgRARgbAAgAg0gHQgGAGAAAIQAAAMALAHQAKAGAPAAQAMAAAKgGQAJgFABgKQABgEgCgFQgDgDgEABQgDAAgCABQgDADABAEIgOAAIAAgLIABgKIgSAAQgJAAgHAGgAgwgrQADACACAFQACAFgCAFIAPAAQAAgJgFgFQgEgEgHAAIgEABg");
	this.shape_61.setTransform(-317.7,192.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgJQAEgHACgFIAABRg");
	this.shape_62.setTransform(-333,198.3);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFD358").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_63.setTransform(-333.2,190.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFD358").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_64.setTransform(-350.5,184.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFD358").s().p("AAhA8QgVAAgKgOQgIAOgXAAQgRAAgMgNQgMgNABgSQAAgTANgKIgSAAIAAgOIASAAQgEgCAAgHIABgGQAHgRAZAAQAWAAAHAOQAJgOAVAAQAMAAAIAFQAJAGABAKQACAKgEAGIgDAAQASAOAAAWQAAATgMANQgMAOgRAAIgBAAgAAKAEIAAANQAAAHAHAEQAHADAIAAQAKAAAHgHQAIgHAAgKQgBgIgGgGQgHgGgJAAIg9AAQgKAAgHAGQgHAHAAAIQgBAKAHAHQAHAGALAAQAJAAAGgDQAIgEAAgHIAAgNgAAdgtQgIAAgFAGQgFAFgBAHIAgAAQACgCAAgFQgBgLgNAAIgBAAgAgigrQAEADAAAFQABAEgDAEIAaAAQgBgJgHgGQgFgDgHAAQgEAAgEACg");
	this.shape_65.setTransform(-355.5,192.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFD358").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_66.setTransform(-370,190.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFD358").s().p("AgmArQgRgRAAgZQAAgbARgRQARgQAZAAQAbAAANALQAJAJABALIgPAAQAAgIgLgFQgKgFgNABQgQAAgNALQgNALAAARQAAAQAMAKQAMALAQAAQANAAALgHQALgIAAgNQAAgRgQgDQAGAFAAALQAAAHgHAFQgHAHgJAAQgKgBgIgGQgHgHAAgKQAAgLAKgIQAIgGAMAAQATAAAOALQANALAAARQAAAXgQAOQgPAPgWAAQgYAAgRgRgAgDgOQgDADAAAEQAAAFADACQADADADAAQAFAAADgDQADgCAAgEQAAgFgDgDQgDgCgEAAQgEAAgDACg");
	this.shape_67.setTransform(-383.1,192.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgiBGIAAgRQAGADAJAAQAJAAAHgRQAFgRAAgUQAAgTgFgSQgHgTgJAAQgJgBgGAFIAAgSQAJgHAOAAQAVAAANAbQALAWAAAcQABAbgMAWQgOAZgUgBQgNAAgKgFg");
	this.shape_68.setTransform(-258.9,157.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgwA2QgVgVAAggQAAghAVgVQAWgVAfAAQAjAAAQAOQAKALABAOIgRAAQgBgKgOgGQgMgFgQgBQgUAAgRAOQgQAPAAAVQAAATAPAOQAPANAUAAQARAAAMgJQAOgKABgQQgBgVgSgFQAGAGAAAOQAAAKgJAGQgIAIgMAAQgNAAgIgIQgKgJAAgMQAAgPAMgJQALgIAPAAQAYAAAQAOQARANAAAWQAAAdgUARQgTATgbAAQgeAAgWgVgAgDgRQgEADABAGQgBAGAEACQADADAEAAQAGAAAEgDQADgDAAgEQAAgGgDgDQgEgFgFAAQgFAAgDAEg");
	this.shape_69.setTransform(-288.9,157.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgTAkIAAgeIgNAAIAAgOIANAAIAAgTIgOAAIAAgNIAnAAQAMAAAHAFQAJAIAAAMQAAAKgKAFQgLAHgPgBIAAAjgAgCgbIAAATQAOAAAAgJQAAgKgMAAIgCAAg");
	this.shape_70.setTransform(-299.7,147.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AghBLQgagBgRgQQgQgRgBgYQgBgWAHgPIARAAQgBAFACADQACACAGAAQAIAAAHgEQAOgJADgKIABgHQAAgMgIgDQgHgDgHADQALADAAAOQAAAHgFAFQgFAFgIAAIgHgCQgOgEAAgQQAAgGACgFQAIgUAbAAQALAAAIAFQAIAFADAJQAPgUAfABQAgACAQAeQAMAUgBAYQAAAyglASQgLAFgNAAQgIAAgKgCIAAgMQgMAJgJADQgJACgNAAIgFAAgAARgTQARAOAAAXQAAAXgOAPIAMABQAMAAAHgFQATgOAAgdQgBgSgMgPQgNgOgSgBQgYgBgKAOIACgBQANAAAKAIgAhKALQAAAMAGAIQAKAMAVAAIAMgBQAPgCAJgKQAHgKAAgLQAAgHgDgFQgEgIgMAAQgGgBgKAGQgMAIgDABQgIADgJAAQgIAAgEgDIgBAIg");
	this.shape_71.setTransform(-306.2,157.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("Ag/BCQgXgcAAgpQABgoAZgZQAYgaApAAQAjAAAYAXQAXAXAAAjQAAAbgPAUQgRAVgaABQgaAAgQgQQgQgRADgYQACgWARgNIgUAAIAAgRIBDAAIAAARIgNAAQgMAAgJAIQgIAIAAAMQAAAMAIAIQAJAIAOAAQARgBAKgNQAJgLgBgSQgBgVgKgNQgQgTghAAQgbAAgQAXQgOAVAAAdQAAAcAOAUQAOAXAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgagdg");
	this.shape_72.setTransform(-325,159.2);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AADBdQgKgFgIgLQgDAIgIAFQgIAGgMAAQgVAAgMgWQgLgTAAgbQAAgfARgYQATgZAgAAQAWAAAPAOQAOAPAAAWQAAAQgNAMIgIAHQgEAEAAAFQAAARAXAAQASAAALgSQAKgRAAgZQAAgegSgYQgTgXglAAQgSAAgPAIQgRAHgHAPIgUAAQAIgYAWgMQAVgMAaAAQAsAAAZAaQAMALAJAVQAIAVABASQABAsgTAaQgSAYgbAAQgQAAgJgDgAg1A6QgDACAAAGQAAAEADADQAEADAEABQAFgBADgDQAEgDAAgEQAAgGgEgCQgDgEgFAAQgEAAgEAEgAhBAFQgFALAAAOQAAALADAJQAHgNARgCQAOgBALAJQABgKAJgIQAKgKAAgLQgBgKgHgGQgHgIgLAAQgeAAgLAZg");
	this.shape_73.setTransform(-343.4,155.2);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("Ag7BQQgXgXAAgiQAAgZAKgQQALgTAUAAQATgBADARQAGgTAUABQAPAAALAMQAKANAAAPQAAATgNAOQgMANgSAAQgQAAgMgKQgMgJAAgOQAAgFAEgSQABgKgKAAQgHAAgFALQgEAKAAALQAAAYATAPQARAOAZAAQAaAAARgRQASgSAAgaQAAgUgIgNQgLgRgSAAIhkAAIAAgQQAaACAAgDQAAAAgGgDQgFgCAAgGQAAgTAWgHQALgDAbAAQAWAAAOAEQATAFAMAPQAKALAAAQIgRAAIAAAAIAAAAIAAAAQAYATAAAtQAAAngXAaQgXAZglAAQgjAAgYgXgAgUADQAAAIAGAGQAGAGAJABQAKAAAHgHQAHgGAAgKQgHALgNABQgPAAgJgPIgBAFgAAAgUQgCADAAADQAAADACADQACADAEAAQADAAADgDQADgDAAgDQAAgDgDgDQgCgDgEAAQgEAAgCADgAA7gzQgDgQgRgIQgQgIgWABQgQAAgHABQgKAEAAAJQAAACADADQADACADAAIAvAAQAPAAALAFIAJAFIAAAAgAA7gzIAAAAIAAAAgAA7gzIAAAAgAAyg4QgLgFgPAAIgvAAQgDAAgDgCQgDgDAAgCQAAgJAKgEQAHgBAQAAQAWgBAQAIQARAIADAQIgJgFg");
	this.shape_74.setTransform(-361.5,154.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAZBbIAAghQgWACgQgBQgigBgUgPQgXgRAAgeQAAgOAJgNQAHgLAMgGQgGgBgDgFQgCgEAAgFQAAgJAIgHQAPgLAaAAQApABAKAXQADgLAJgGQAIgHALAAIASAAIAAARIgDgBQgGAAgBAGQAAACAGAFQAJAGADAEQAGAIABAKQABASgQAJQgOAHgRgBIAAAmIAPgEIALgFIAAARIgMAFIgOAEIAAAkgAgpgbQgJAMAAAPQAAAgAlAIIASACQANAAAHgCIAAhOIgpAAQgPAAgKALgAAtgtIAAAdIADAAQAGAAAGgEQAFgEAAgHQAAgIgHgKQgIgLAAgFIABgDQgGACAAAVgAALg3QgDgKgNgFQgQgFgOAEQADAEAAAFQAAAFgDACIADAAIAEAAIAnAAIAAAAg");
	this.shape_75.setTransform(-379.6,158.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFD358").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_76.setTransform(471.8,537.2);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFD358").s().p("AgsBEQgTgMgBgVQgCgXATgJQgFgBgEgDQgDgEAAgGIABgFQAGgQAfACQASABAIANQAJALgBARIgoAAQgTAAAAAPQAAALAPAGQAMAFASAAQAUAAANgLQAOgKABgQIABgJQAAgQgLgMQgJgLgQAAIgvAAQgWAAAAgPQAAgaA5AAQAiAAANAKQAJAHAAAJQAAAJgIAGQASAVAAAfQgBAcgUARQgUARgbAAQgZAAgRgKgAgggQQADACABAFQAAAFgDAEIAcAAQgCgIgFgEQgGgFgKAAIgGABgAAag6QgCACAAAEQAAADACADQADACADAAQAEAAACgCQADgDAAgDQAAgEgDgCQgCgCgEAAQgDAAgDACgAggg2QgBAFAKAAIAhAAQgCgHAEgGIgOgBQgdAAgBAJg");
	this.shape_77.setTransform(457.7,537.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFD358").s().p("AADBJQgLgFAAgLQgBgKAHgEQgZABgRgOQgRgOABgVQACgZAVgMQgEgBgCgEQgDgEAAgEIABgFQAFgKALgEQAIgCAPAAQAcAAAMAOQAJALABAQIgrAAQgMAAgJAJQgJAIgBAMQgBANAMALQALAKAPAAQAYABARgJIAAAOQgIAEgIACQgPAEAAAGQgBAFAFACQAEADAGAAQAGAAAFgDQAFgEAAgHIAAgFIANAAIABAGQAAANgLAIQgJAIgOABIgHAAQgJAAgIgEgAgSg9IADACQAAABAAAAQABABAAAAQAAABAAAAQABABAAAAQABAFgFACIAHgBIAjAAQgFgJgLgDQgFgCgJAAQgJABgEABg");
	this.shape_78.setTransform(445.5,540.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFD358").s().p("Ag1ArQgQgOAAgZQAAgJAEgJQAFgJAHgEQgIgDABgJIAAgGQAGgOAaAAQAVAAAHALQAJgLAWAAQAKAAAIAEQAKAGAAAJQACAHgEAHQAFAEAEAIQAEAJAAAJQAAAZgRAPQgUAQghAAQghAAgUgRgAgwgNQgFAGABAKQAAAPASAHQAOAGAUAAQAUAAANgGQATgHAAgPQABgKgGgGQgFgGgKAAIhBAAQgJAAgGAGgAAPgqQgGAEgBAGIAhAAIAAgDQAAgFgDgDQgFgDgFAAQgHAAgGAEgAglgtQADACABAEQABAEgCADIAZAAQgBgLgPgDIgFAAQgEAAgDABg");
	this.shape_79.setTransform(431.8,538.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFD358").s().p("AAnAcQAKgHAAgJIgBgDQgCgIgPgCQgNgEgSAAQgRAAgNAEQgPADgCAHIgBADQAAAKAJAGIgNAAQgLgEAAgQQAAgHACgCQAHgPASgGQAOgEAWAAQAzgBALAaQACADAAAGQAAAPgLAFg");
	this.shape_80.setTransform(416.9,531.3);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFD358").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNIACgJQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape_81.setTransform(416.6,538.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFD358").s().p("Ag5A+QgMgOABgTQABgWAPgIIgUAAIAAgOIAZAAQgFgCAAgHQAAgEACgCQAHgOAVAAIAJABQAQABAJAMQAJAMAAARIgrAAQgMAAgIAGQgIAHAAALQAAAKAHAGQAHAHALAAQAIAAAGgFQAGgFAAgIIAAgQIANAAIAAAQQAAAIAFAFQAGAFAJAAQAXAAADgpQACgZgRgUQgQgVgXAAQgkgBgNAXIgRAAQASgkAvABQAcAAATARQAOAMAHATQAHASgBATQgCAbgKARQgLAUgWAAQgSAAgJgLQgJALgTAAIgBAAQgRAAgMgNgAgagdQADAFAAADQAAADgCADIAcAAQgDgKgGgDQgGgCgHAAIgHABg");
	this.shape_82.setTransform(393.9,537.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFD358").s().p("AgVAzQgIAJgTAAQgLAAgHgHQgIgHAAgLQgBgMAGgIIgJAAIAAgJIAHgJIALgLIgDAAQgHAAgDgFQgEgEAAgHQAAgLAIgIQAIgJALgBQAUgBAJANQATgNAZABQAXACAQARQAQARABAXQABAngdARQgHAEgNAAIgLgBIAAgJQgJAKgLAAIgBAAQgLAAgJgJgAgJgkQASACALANQAOAPAAAUQAAAUgKALIAGABQAJAAAJgIQAIgIACgKIAAgJQAAgXgPgOQgNgMgXAAQgIAAgIACgAgngQQgLAKgKAKIAJAAQgFAIABAJQAAAEAEADQAEAEAGAAQAJAAADgKQACgGAAgQIANAAQAAARABAGQACAIAJAAQAHAAAFgIQAEgGAAgKQAAgKgGgJQgHgJgJgBIgGAAQgQAAgJAGgAg3gpQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_83.setTransform(378.6,538.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFD358").s().p("AgpBMQgagBgQgQQgPgQgBgYQAAgQAGgMQAGgMAOgMIAagZQAJgLAEgGIAYAAQgEAJgIAIQgHAIgMAHQASgBANAFQAjANAAAqQAAAbgTARQgRAQgaAAIgEAAgAhGgQQgLAMAAAOQAAAPAKAJQANANAVAAQARAAALgJQAPgLAAgTQAAgIgDgHQgDgLgMgGQgKgGgNAAQgXAAgMAOgAAkBAQgIgNAAgUQAAgZAUgZQAIgKAIgGIgkAAIAAgNIAegMIg9AAIAAgOIBeAAIAAANIgeAMIAeAAIAAAMQgMAGgJAJQgJAIgGALQgHANAAANQAAAYAUAAQAHAAAFgDQAFgEAAgHQAAgFgCgGIAMAAQAFAGAAAOQAAAPgKAKQgLAJgPAAQgSAAgKgMg");
	this.shape_84.setTransform(360.1,537.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFD358").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_85.setTransform(339.4,538.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFD358").s().p("AARAuQANAAAJgFQAKgGAEgLQAEgMAAgMQgCgNgJgJQgJgLgOAAQgWACgFATQAEgDAFAAQAHAAAEADQANAJgBAVQAAATgRAMQgPALgVAAQgWAAgPgNQgOgOAAgUQAAgWAQgLQgFgBgDgEQgDgFAAgGQAAgLAKgGQAIgGAMAAQAKAAAIAGQAHAHADAKQANgWAbgBQAWAAAPATQANARAAAYQAAAZgQARQgRARgbAAgAg0gHQgGAGAAAIQAAAMALAHQAKAGAPAAQAMAAAKgGQAJgFABgKQABgEgCgFQgDgDgEABQgDAAgCABQgDADABAEIgOAAIAAgLIABgKIgSAAQgJAAgHAGgAgwgrQADACACAFQACAFgCAFIAPAAQAAgJgFgFQgEgEgHAAIgEABg");
	this.shape_86.setTransform(328.8,538.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgKQAEgFACgGIAABRg");
	this.shape_87.setTransform(306.2,544.8);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFD358").s().p("Ag3BGQgQgGgFAAQgGAAgHAIIgLgHIAWgfQgKgLAAgTQAAgGACgGQAFgTARgKQAMgHASAAIAIAAIAVgeIAPAAIgUAfIAKAFIAJAGIAGgHQAHgFAIAAQAKAAAHAHQAJgIALABQAgABACA3QAAAZgJARQgLAUgXABQgPAAgIgJQgIgIAAgOIABgGIAOAAIAAAEQAAASAPgBQAKAAAEgPQACgIAAgVQABgPgDgMQgDgPgGAAQgKAAAAAOIAAAcIgPAAIAAgcQAAgPgIAAQgEgBgDADQgDADAAADQAMAOgBAWQgBAbgQAQQgPAPgbABIgCAAQgLAAgNgFgAg6A2QALAFALAAIAKgBQAPgCAJgMQAIgLAAgPQAAgOgJgKQgJgLgPgBIgMASIAHAAQAIAAAGAEQAGAFABAIQABAKgFAIQgFAHgJADQgHACgIAAQgOAAgKgFIgHALIAFgBQAGAAAGACgAg7AeQACACAFABQAJACADgCQgIgCgCgFQgCgEABgFgAgnATQAAAHAHAAQAGAAAAgHQAAgHgGAAQgHAAAAAHgAhBgFQgFAGAAAKIABAJIAagmQgPADgHAKg");
	this.shape_88.setTransform(303.1,537.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgKQAEgFACgGIAABRg");
	this.shape_89.setTransform(286.1,544.8);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFD358").s().p("AARAuQANAAAJgFQAKgGAEgLQAEgMAAgMQgCgNgJgJQgJgLgOAAQgWACgFATQAEgDAFAAQAHAAAEADQANAJgBAVQAAATgRAMQgPALgVAAQgWAAgPgNQgOgOAAgUQAAgWAQgLQgFgBgDgEQgDgFAAgGQAAgLAKgGQAIgGAMAAQAKAAAIAGQAHAHADAKQANgWAbgBQAWAAAPATQANARAAAYQAAAZgQARQgRARgbAAgAg0gHQgGAGAAAIQAAAMALAHQAKAGAPAAQAMAAAKgGQAJgFABgKQABgEgCgFQgDgDgEABQgDAAgCABQgDADABAEIgOAAIAAgLIABgKIgSAAQgJAAgHAGgAgwgrQADACACAFQACAFgCAFIAPAAQAAgJgFgFQgEgEgHAAIgEABg");
	this.shape_90.setTransform(285.3,538.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFD358").s().p("Ag7ApIAAgXIBqAAIAAglIAHgKQAEgFACgGIAABRg");
	this.shape_91.setTransform(270,544.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFD358").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_92.setTransform(269.8,537.2);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFD358").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_93.setTransform(252.5,530.7);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFD358").s().p("AAhA8QgVAAgKgOQgIAOgXAAQgRAAgMgNQgMgNABgSQAAgTANgKIgSAAIAAgOIASAAQgEgCAAgHIABgGQAHgRAZAAQAWAAAHAOQAJgOAVAAQAMAAAIAFQAJAGABAKQACAKgEAGIgDAAQASAOAAAWQAAATgMANQgMAOgRAAIgBAAgAAKAEIAAANQAAAHAHAEQAHADAIAAQAKAAAHgHQAIgHAAgKQgBgIgGgGQgHgGgJAAIg9AAQgKAAgHAGQgHAHAAAIQgBAKAHAHQAHAGALAAQAJAAAGgDQAIgEAAgHIAAgNgAAdgtQgIAAgFAGQgFAFgBAHIAgAAQACgCAAgFQgBgLgNAAIgBAAgAgigrQAEADAAAFQABAEgDAEIAaAAQgBgJgHgGQgFgDgHAAQgEAAgEACg");
	this.shape_94.setTransform(247.5,538.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFD358").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_95.setTransform(233,537.2);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFD358").s().p("AgmArQgRgRAAgaQAAgZARgSQARgQAZAAQAbAAANAMQAJAHABAMIgPAAQAAgIgLgFQgKgEgNgBQgQAAgNALQgNANAAAQQAAAQAMALQAMAKAQAAQANAAALgHQALgIAAgNQAAgRgQgEQAGAGAAALQAAAGgHAHQgHAFgJAAQgKAAgIgGQgHgHAAgKQAAgLAKgIQAIgGAMAAQATAAAOALQANAKAAASQAAAWgQAPQgPAPgWAAQgYAAgRgRgAgDgNQgDACAAAEQAAAFADACQADADADAAQAFAAADgDQADgCAAgEQAAgEgDgDQgDgEgEAAQgEAAgDAEg");
	this.shape_96.setTransform(219.9,538.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AABBQQgMAOgWAAQgXAAgPgRQgPgSABgXQABgbATgLIgZAAIAAgRIAfAAQgGgDAAgJQAAgEACgEQAJgQAaAAIAMABQATABAMAQQAMAOgBAVIg2AAQgOAAgKAIQgKAJgBAOQAAAMAJAIQAIAJAOAAQAKAAAIgHQAHgGAAgLIAAgUIARAAIAAAVQAAAKAHAGQAHAHALAAQAdgBADgzQACgggUgZQgUgageAAQgtAAgQAcIgUAAQAWgtA6ABQAjAAAYAVQASAPAIAYQAJAXgBAYQgCAhgMAVQgPAagbAAQgXAAgLgOgAggglQADAGAAAEQAAAFgDADIAkAAQgEgMgIgEQgHgCgIAAIgJAAg");
	this.shape_97.setTransform(356.8,501.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AgiBGIAAgRQAGADAJAAQAJAAAHgRQAFgRAAgUQAAgUgFgRQgHgTgJgBQgJAAgGAFIAAgSQAJgGAOAAQAVgBANAbQALAXAAAbQABAcgMAVQgOAZgUAAQgNAAgKgGg");
	this.shape_98.setTransform(344.1,503.7);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AARBKIAAgOQgMAJgIADQgLADgSAAQgegBgRgWQgOgUAAgdQAAgyAigUQALgHATAAIAJABIAAARQAVgTAbABQAgABARAVQAQATABAgQAAAggRAVQgTAXgfAAIgKgBgAALgYQAaAHgBAfQAAAYgQATQAQACAMgHQATgNgCggQAAgSgMgOQgMgPgSgBQgigCgFAXQAIgFAKAAIAJABgAg8gqQgKAOAAATQAAAUALANQALAOAUABQAQABAMgIQALgHABgNQABgHgFgFQgEgHgHAAQgGABgEAEQgDADAAAGIABAHIgSAAQgEgSACgRQACgQANgSIgLgCQgSAAgLAPg");
	this.shape_99.setTransform(331.1,503.7);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AgwA2QgVgVAAggQAAghAVgVQAWgVAfAAQAiAAARAOQAKAKABAOIgRAAQgBgJgOgHQgMgEgQAAQgUAAgRANQgQAPAAAUQAAAVAPANQAPANAUAAQARAAAMgJQAOgKABgQQgBgUgSgGQAGAHAAAOQAAAJgJAHQgIAHgMAAQgNAAgIgIQgKgIAAgOQAAgOAMgJQALgIAPAAQAYAAAQANQARAOAAAWQAAAdgUARQgTATgbAAQgeAAgWgVgAgDgRQgEADABAGQgBAFAEADQADAEAEAAQAGAAAEgEQADgDAAgFQAAgFgDgEQgEgDgFAAQgFgBgDAEg");
	this.shape_100.setTransform(314.1,503.7);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AgTAkIAAgeIgNAAIAAgNIANAAIAAgUIgOAAIAAgOIAnAAQAMAAAHAHQAJAGAAANQAAAKgKAGQgLAFgPAAIAAAjgAgCgaIAAATQAOAAAAgJQAAgLgMAAIgCABg");
	this.shape_101.setTransform(303.3,493.5);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AghBLQgagBgRgQQgQgRgBgYQgBgWAHgPIARAAQgBAFACADQACACAGAAQAIAAAHgEQAOgJADgKIABgHQAAgMgIgDQgHgDgHADQALADAAAOQAAAHgFAFQgFAFgIAAIgHgCQgOgEAAgQQAAgGACgFQAIgUAbAAQALAAAIAFQAIAFADAJQAPgUAfABQAgACAQAeQAMAUgBAYQAAAyglASQgLAFgNAAQgIAAgKgCIAAgMQgMAJgJADQgJACgNAAIgFAAgAARgTQARAOAAAXQAAAXgOAPIAMABQAMAAAHgFQATgOAAgdQgBgSgMgPQgNgOgSgBQgYgBgKAOIACgBQANAAAKAIgAhKALQAAAMAGAIQAKAMAVAAIAMgBQAPgCAJgKQAHgKAAgLQAAgHgDgFQgEgIgMAAQgGgBgKAGQgMAIgDABQgIADgJAAQgIAAgEgDIgBAIg");
	this.shape_102.setTransform(296.8,503.7);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("Ag/BBQgXgcAAgoQABgnAZgaQAYgaApAAQAjAAAYAXQAXAXAAAiQAAAcgPAUQgRAWgaAAQgaAAgQgRQgQgQADgYQACgWARgMIgUAAIAAgSIBDAAIAAASIgNAAQgMAAgJAHQgIAIAAAMQAAALAIAJQAJAIAOAAQARgBAKgNQAJgMgBgRQgBgWgKgNQgQgSghAAQgbAAgQAYQgOATAAAeQAAAcAOAVQAOAWAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgageg");
	this.shape_103.setTransform(278,505.7);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AADBcQgKgEgIgMQgDAJgIAGQgIAFgMAAQgVAAgMgWQgLgTAAgbQAAgfARgXQATgaAgAAQAWAAAPAPQAOAOAAAWQAAAQgNAMIgIAHQgEAEAAAGQAAAQAXAAQASAAALgTQAKgQAAgZQAAgfgSgXQgTgXglAAQgSAAgPAHQgRAJgHANIgUAAQAIgWAWgNQAVgMAaAAQAsAAAZAZQAMAMAJAVQAIAVABARQABAtgTAaQgSAYgbAAQgQAAgJgEgAg1A5QgDADAAAFQAAAFADADQAEAEAEgBQAFABADgEQAEgDAAgFQAAgFgEgDQgDgDgFAAQgEAAgEADgAhBAEQgFAMAAAOQAAALADAJQAHgOARgBQAOgCALAKQABgKAJgHQAKgLAAgMQgBgJgHgHQgHgGgLAAQgegBgLAYg");
	this.shape_104.setTransform(259.6,501.6);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("Ag7BQQgXgXAAgjQAAgXAKgSQALgTAUAAQATAAADAQQAGgRAUgBQAPABALANQAKALAAAQQAAAUgNAMQgMAOgSAAQgQAAgMgJQgMgKAAgPQAAgEAEgSQABgKgKAAQgHAAgFAMQgEAIAAAMQAAAYATAPQARAOAZAAQAaAAARgSQASgRAAgaQAAgUgIgOQgLgQgSAAIhkAAIAAgPQAaABAAgCQAAgBgGgDQgFgDAAgFQAAgUAWgFQALgEAbAAQAWAAAOAEQATAFAMAPQAKALAAARIgRAAIAAgBQgDgQgRgJQgQgGgWgBQgQAAgHADQgKADAAAJQAAADADACQADACADAAIAvAAQAPAAALAFIAJAFIAAABIAAAAQAYASAAAtQAAAmgXAaQgXAaglAAQgjAAgYgXgAgUADQAAAIAGAGQAGAGAJAAQAKABAHgGQAHgHAAgJQgHAKgNABQgPAAgJgQIgBAGgAAAgVQgCADAAAEQAAADACAEQACACAEAAQADAAADgCQADgEAAgDQAAgEgDgDQgCgCgEAAQgEAAgCACgAA7gyIAAgBIAAABgAA7gzIAAAAgAA7gzIAAAAgAA7gzIAAAAgAAyg4QgLgFgPAAIgvAAQgDAAgDgCQgDgCAAgDQAAgJAKgDQAHgDAQAAQAWABAQAGQARAJADAQIgJgFg");
	this.shape_105.setTransform(241.5,501);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AAZBbIAAghQgWACgQgBQgigBgUgPQgXgRAAgeQAAgOAJgNQAHgLAMgGQgGgBgDgFQgCgEAAgFQAAgJAIgHQAPgLAaAAQApABAKAXQADgLAJgGQAIgHALAAIASAAIAAARIgDgBQgGAAgBAGQAAACAGAFQAJAGADAEQAGAIABAKQABASgQAJQgOAHgRgBIAAAmIAPgEIALgFIAAARIgMAFIgOAEIAAAkgAgpgbQgJAMAAAPQAAAgAlAIIASACQANAAAHgCIAAhOIgpAAQgPAAgKALgAAtgtIAAAdIADAAQAGAAAGgEQAFgEAAgHQAAgIgHgKQgIgLAAgFIABgDQgGACAAAVgAALg3QgDgKgNgFQgQgFgOAEQADAEAAAFQAAAFgDACIADAAIAEAAIAnAAIAAAAg");
	this.shape_106.setTransform(223.4,505.3);

	this.instance_6 = new lib.Bitmap18();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-391,-227,1.297,1.297);

	this.instance_7 = new lib.Bitmap17();
	this.instance_7.parent = this;
	this.instance_7.setTransform(115,-227,1.209,1.209);

	this.instance_8 = new lib.Bitmap16();
	this.instance_8.parent = this;
	this.instance_8.setTransform(115,-17,0.63,0.63);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26,p:{x:0}},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23,p:{x:-384.8,y:153}},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:-268,y:185.3}},{t:this.shape_4,p:{x:-258,y:187}},{t:this.shape_3,p:{x:-247.2,y:187}},{t:this.shape_2,p:{x:-242,y:178.8}},{t:this.shape_1,p:{x:-230.9,y:187}},{t:this.shape},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_23,p:{x:-357.4,y:149.9}},{t:this.shape_42},{t:this.shape_41,p:{x:-324.4,y:148.7}},{t:this.shape_40},{t:this.shape_39,p:{x:-298.7,y:146.9}},{t:this.shape_5,p:{x:-398.8,y:182.2}},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_1,p:{x:-317.3,y:183.9}},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_2,p:{x:-273.7,y:175.8}},{t:this.shape_28},{t:this.shape_4,p:{x:-253.7,y:183.9}},{t:this.shape_27},{t:this.shape_3,p:{x:-231.1,y:184}},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3}]},1).to({state:[{t:this.shape_26,p:{x:0.5}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_41,p:{x:-271.9,y:157.2}},{t:this.shape_68},{t:this.shape_39,p:{x:-246.2,y:155.4}},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-461,-248.4,922,496.9);


(lib.mov = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFD54F").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNIACgJQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape.setTransform(-312.8,192);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFD54F").s().p("AgZAxQgLgJAAgRQAAgMAEgKQAEgFAIgKQAJgKAYgTIguAAIAAgOIBEAAIAAANIgQAMQgIAGgGAGQgJAMgDAEQgGAJABAKQAAAHAFAEQAFADAFABQANgBADgKIACgGQAAgFgDgEIANAAQAHAKgBAMQgBAOgLAJQgLAJgOAAQgPAAgKgJg");
	this.shape_1.setTransform(-324.7,192.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFD54F").s().p("AhFAiQgDgLADgJIgLAAIAAgLIAIgJIALgLQgHAAgEgFQgFgFAAgHQAAgLAJgHQAIgGALgBQASgBAJANQARgMAZAAIAIAAQAYACAPASQAQATgCAXQgBAYgPAQQgQARgYAAIgIAAIAAgMQgPAMgYAAIgDAAQglAAgHgagAgGgiQAGABAHAEQAIAEAFAFQALANAAAOQABAXgNAPIAFABQALAAAIgFQAPgKABgUQACgNgHgMQgGgLgNgGQgJgFgQAAQgKAAgGACgAgogPQgKAGgNANIAJAAQgEAJADAIQAFAQAZgBQAOgBAKgIQAJgIABgNQAAgLgIgIQgHgJgMAAQgMAAgKAHgAgxgtIgGADQAGABACADQACAEgCAFIAHgFIAGgFQgDgEgFgCIgEAAIgDAAg");
	this.shape_2.setTransform(-337.1,191.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD54F").s().p("AgrBEQgTgMgCgVQgCgXATgJQgFgBgEgDQgDgEAAgGIABgFQAGgQAfACQASABAJANQAJALgBARIgpAAQgTAAAAAPQAAALAPAGQAMAFASAAQAUAAAOgLQANgLABgQQABgWgLgOQgJgLgQAAIgvAAQgWAAAAgPQAAgaA5AAQAWAAARAJQATAKABARIgPAAQAAgKgQgGQgNgFgRAAQgdAAgBAIQgBAFAKAAIAnAAQAPAAANAKQAWARAAAnQAAAbgVARQgUARgbAAQgZAAgQgKgAgggQQAEACAAAFQAAAFgDAEIAcAAQgCgIgFgEQgGgFgKAAIgGABg");
	this.shape_3.setTransform(-352.3,190.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFD54F").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_4.setTransform(-362.1,191.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFD54F").s().p("AgaA8QgXAAgOgSQgLgQAAgYQAAgnAbgRQAJgFAPAAIAIABIAAANQAQgPAWABQAZABAOAQQANAQAAAZQABAZgPASQgPASgYAAIgIAAIAAgMQgKAHgGACQgHADgNAAIgEAAgAAJgTQAUAGAAAYQAAAUgNAOQANADAJgGQAQgLgCgZQAAgPgKgLQgJgMgOgBQgbgBgEASQAGgEAIAAIAHABgAgwghQgIALAAAPQAAAPAJALQAJALAQABQANABAJgGQAJgGABgKQAAgGgDgEQgEgGgGABQgEAAgDAEQgDACAAAFIABAFIgOAAQgDgOABgNQACgOAKgOIgJgBQgOAAgJAMg");
	this.shape_5.setTransform(-372.5,191.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFD54F").s().p("AgmArQgRgRAAgaQAAgZARgSQARgQAZAAQAbAAANAMQAJAHABAMIgPAAQAAgIgLgFQgKgEgMgBQgRABgNAKQgNANAAAQQAAAQAMALQAMAKAQAAQANAAALgHQALgIAAgNQAAgQgQgFQAGAGAAALQAAAGgHAHQgHAFgJAAQgKAAgIgGQgHgHAAgKQAAgLAKgIQAIgGAMAAQAUAAANALQANAKAAASQAAAWgQAPQgPAPgWAAQgYAAgRgRgAgCgNQgEADAAADQAAAFAEACQACADADAAQAFAAADgDQADgCAAgEQAAgEgDgDQgDgEgEAAQgEAAgCAEg");
	this.shape_6.setTransform(-386.1,192);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFD54F").s().p("AgsBEQgTgMgBgVQgCgXATgJQgFgBgEgDQgDgEAAgGIABgFQAGgQAfACQASABAIANQAJALgBARIgoAAQgTAAAAAPQAAALAPAGQAMAFASAAQAUAAANgLQAOgKABgQIABgJQAAgQgLgMQgJgLgQAAIgvAAQgWAAAAgPQAAgaA5AAQAiAAANAKQAJAHAAAJQAAAJgIAGQASAVAAAfQgBAcgUARQgUARgbAAQgZAAgRgKgAgggQQADACABAFQAAAFgDAEIAcAAQgCgIgFgEQgGgFgKAAIgGABgAAag6QgCACAAAEQAAADACADQADACADAAQAEAAACgCQADgDAAgDQAAgEgDgCQgCgCgEAAQgDAAgDACgAggg2QgBAFAKAAIAhAAQgCgHAEgGIgOgBQgdAAgBAJg");
	this.shape_7.setTransform(-406.2,190.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgGBfQgggBgTgUQgUgUAAgfQgBgUAIgPQAIgPARgPQALgJAWgWQALgOADgIIAfAAQgGALgJALQgIAJgOAKQAVgBAQAFQAtARAAA1QAAAhgYAWQgYAVghAAIgDgBgAgqgVQgNAPAAASQAAASAMAMQARARAaAAQAUAAAPgMQATgOAAgXQAAgKgEgKQgFgOgOgHQgNgHgRAAQgbAAgQARg");
	this.shape_8.setTransform(-191.8,154.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AghBGIAAgRQAEADAKAAQAJAAAGgRQAGgRAAgUQAAgUgFgRQgHgTgJgBQgIAAgGAFIAAgSQAHgGAPAAQAVgBANAbQAMAXgBAbQAAAcgMAVQgNAZgVAAQgMAAgJgGg");
	this.shape_9.setTransform(-203.7,156.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhCA2QgVgSAAgeQAAgMAGgLQAFgLAJgGQgJgEAAgLQAAgEABgDQAHgSAhAAQAZAAAKAOQALgOAbAAQAOAAAKAGQALAGACAMQABAIgFAJQAHAFAFALQAFALAAAMQAAAfgWASQgZAUgpAAQgqAAgYgVgAg8gQQgHAIABAMQABASAWAKQASAHAZAAQAYAAASgHQAWgKABgTQABgLgHgIQgHgIgMAAIhRAAQgMAAgHAIgAATg1QgIAFgBAIIApAAIABgEQAAgGgFgDQgGgEgGAAIgBAAQgIAAgHAEgAgug4QAEADABAEQABAFgDAEIAfAAQgBgNgSgEIgHgBQgFAAgDACg");
	this.shape_10.setTransform(-216.3,156.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ag7BQQgXgXAAgjQAAgXAKgSQALgTAUAAQATAAADAQQAGgRAUgBQAPABALANQAKALAAAQQAAAUgNAMQgMAOgSAAQgQAAgMgJQgMgKAAgPQAAgEAEgSQABgKgKAAQgHAAgFAMQgEAIAAAMQAAAYATAPQARAOAZAAQAaAAARgSQASgRAAgaQAAgUgIgOQgLgQgSAAIhkAAIAAgPQAaABAAgCQAAgBgGgDQgFgDAAgFQAAgUAWgFQALgEAbAAQAWAAAOAEQATAGAMAOQAKALAAARIgRAAIAAgBQgDgQgRgJQgQgGgWgBQgQAAgHADQgKADAAAJQAAADADACQADACADAAIAvAAQAPAAALAFIAJAFIAAABIAAAAQAYASAAAtQAAAmgXAaQgXAaglAAQgjAAgYgXgAgUADQAAAIAGAGQAGAGAJAAQAKABAHgGQAHgHAAgJQgHAKgNABQgPAAgJgQIgBAGgAAAgVQgCADAAAEQAAADACAEQACACAEAAQADAAADgCQADgEAAgDQAAgEgDgDQgCgCgEAAQgEAAgCACgAA7gyIAAgBIAAABgAA7gzIAAAAgAA7gzIAAAAgAA7gzIAAAAgAAyg4QgLgFgPAAIgvAAQgDAAgDgCQgDgCAAgDQAAgJAKgDQAHgDAQAAQAWABAQAGQARAJADAQIgJgFg");
	this.shape_11.setTransform(-234.1,154);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZBbIAAghQgWACgQgBQgigBgUgPQgXgRAAgeQAAgOAJgNQAHgLAMgGQgGgBgDgFQgCgEAAgFQAAgJAIgHQAPgLAaAAQApABAKAXQADgLAJgGQAIgHALAAIASAAIAAARIgDgBQgGAAgBAGQAAACAGAFQAJAGADAEQAGAIABAKQABASgQAJQgOAHgRgBIAAAmIAPgEIALgFIAAARIgMAFIgOAEIAAAkgAgpgbQgJAMAAAPQAAAgAlAIIASACQANAAAHgCIAAhOIgpAAQgPAAgKALgAAtgtIAAAdIADAAQAGAAAGgEQAFgEAAgHQAAgIgHgKQgIgLAAgFIABgDQgGACAAAVgAALg3QgDgKgNgFQgQgFgOAEQADAEAAAFQAAAFgDACIADAAIAEAAIAnAAIAAAAg");
	this.shape_12.setTransform(-252.2,158.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRAeQAOgJAAgIIgBgBIgIgFQgIgHAAgIQAAgHAEgGQAGgIAKAAQAHAAAGAEQAIAGAAALQAAAHgDAHQgDAGgGAHQgIAIgEADg");
	this.shape_13.setTransform(-274.4,163.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAAA7QgMAQgUAAQgdAAgPgVQgNgUABgiQAAgjAZgWQAQgPAWgCQAJgBAHACIAAARQgKgEgPAFQgOAFgKAOQgJAOAAAQQAAAtAfAAQAMAAAHgGQAIgHAAgLIAAgPIARAAIAAAPQAAALAIAHQAIAGALAAQAOAAAJgJQAJgJAAgPQAAgMgJgJQgKgIgNAAIgrAAQgBgbALgNQALgLAVAAQAoABAAAfIgBAHIgOAAQAOAGAIANQAIANAAAPQAAAcgOARQgOASgYAAQgaAAgLgQgAAMgnIAaAAIAKABIABgFQAAgNgRAAQgSAAgCARg");
	this.shape_14.setTransform(-288.7,156.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgGBfQgggBgTgUQgUgUAAgfQgBgUAIgPQAIgPARgPQALgJAWgWQALgOADgIIAfAAQgGALgJALQgIAJgOAKQAVgBAQAFQAtARAAA1QAAAhgYAWQgYAVghAAIgDgBgAgqgVQgNAPAAASQAAASAMAMQARARAaAAQAUAAAPgMQATgOAAgXQAAgKgEgKQgFgOgOgHQgNgHgRAAQgbAAgQARg");
	this.shape_15.setTransform(-306.1,154.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AhLAzIAAgcICFAAIAAgvIAJgMIAIgOIAABlg");
	this.shape_16.setTransform(-323.3,164.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhCA2QgVgSAAgeQAAgMAGgLQAFgLAJgGQgJgEAAgLQAAgEABgDQAHgSAhAAQAZAAAKAOQALgOAbAAQAOAAAKAGQALAGACAMQABAIgFAJQAHAFAFALQAFALAAAMQAAAfgWASQgZAUgpAAQgqAAgYgVgAg8gQQgHAIABAMQABASAWAKQASAHAZAAQAYAAASgHQAWgKABgTQABgLgHgIQgHgIgMAAIhRAAQgMAAgHAIgAATg1QgIAFgBAIIApAAIABgEQAAgGgFgDQgGgEgGAAIgBAAQgIAAgHAEgAgug4QAEADABAEQABAFgDAEIAfAAQgBgNgSgEIgHgBQgFAAgDACg");
	this.shape_17.setTransform(-323.7,156.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgDBSQgLARgXAAQgagBgNgVQgNgUABghQACgkAWgUQARgPAjAAIAKAAIAAARIgZABQgNACgIAFQgRANgBAYQAAAUAGAMQAIAOAPAAQANABAHgIQAGgHAAgLIAAgXIARAAIAAAXQAAAZAaAAQASAAAJgUQAJgUgCgcQgBgdgOgTQgUgagigBQgUABgRAHQgQAJgHANIgTAAQAGgVAVgMQAWgMAdAAQAXAAATAJQAOAGAKAKQAaAdABA0QABAlgPAaQgQAbgdAAQgXAAgKgRg");
	this.shape_18.setTransform(-342.1,154.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AghBGIAAgRQAEADAKAAQAJAAAGgRQAGgRAAgUQAAgUgFgRQgHgTgJgBQgIAAgGAFIAAgSQAHgGAPAAQAVgBANAbQALAXAAAbQAAAcgMAVQgNAZgVAAQgMAAgJgGg");
	this.shape_19.setTransform(-355,156.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgGBfQgggBgTgUQgUgUAAgfQgBgUAIgPQAIgPARgPQALgJAWgWQALgOADgIIAfAAQgGALgJALQgIAJgOAKQAVgBAQAFQAtARAAA1QAAAhgYAWQgYAVghAAIgDgBgAgqgVQgNAPAAASQAAASAMAMQARARAaAAQAUAAAPgMQATgOAAgXQAAgKgEgKQgFgOgOgHQgNgHgRAAQgbAAgQARg");
	this.shape_20.setTransform(-366.3,154.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AhKAzIAAgcICEAAIAAgvIAJgMIAJgOIAABlg");
	this.shape_21.setTransform(-383.6,164.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AghBLQgagBgRgQQgQgRgBgYQgBgWAHgPIARAAQgBAFACADQACACAGAAQAIAAAHgEQAOgJADgKIABgHQAAgMgIgDQgHgDgHADQALADAAAOQAAAHgFAFQgFAFgIAAIgHgCQgOgEAAgQQAAgGACgFQAIgUAbAAQALAAAIAFQAIAFADAJQAPgUAfABQAgACAQAeQAMAUgBAYQAAAyglASQgLAFgNAAQgIAAgKgCIAAgMQgMAJgJADQgJACgNAAIgFAAgAARgTQARAOAAAXQAAAXgOAPIAMABQAMAAAHgFQATgOAAgdQgBgSgMgPQgNgOgSgBQgYgBgKAOIACgBQANAAAKAIgAhKALQAAAMAGAIQAKAMAVAAIAMgBQAPgCAJgKQAHgKAAgLQAAgHgDgFQgEgIgMAAQgGgBgKAGQgMAIgDABQgIADgJAAQgIAAgEgDIgBAIg");
	this.shape_22.setTransform(-384.4,156.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAZBbIAAghQgWACgQgBQgigBgUgPQgXgRAAgeQAAgOAJgNQAHgLAMgGQgGgBgDgFQgCgEAAgFQAAgJAIgHQAPgLAaAAQApABAKAXQADgLAJgGQAIgHALAAIASAAIAAARIgDgBQgGAAgBAGQAAACAGAFQAJAGADAEQAGAIABAKQABASgQAJQgOAHgRgBIAAAmIAPgEIALgFIAAARIgMAFIgOAEIAAAkgAgpgbQgJAMAAAPQAAAgAlAIIASACQANAAAHgCIAAhOIgpAAQgPAAgKALgAAtgtIAAAdIADAAQAGAAAGgEQAFgEAAgHQAAgIgHgKQgIgLAAgFIABgDQgGACAAAVgAALg3QgDgKgNgFQgQgFgOAEQADAEAAAFQAAAFgDACIADAAIAEAAIAnAAIAAAAg");
	this.shape_23.setTransform(-403.6,158.3);

	this.instance = new lib.Image_2();
	this.instance.parent = this;
	this.instance.setTransform(126,-39,0.362,0.362);

	this.instance_1 = new lib.Image_0();
	this.instance_1.parent = this;
	this.instance_1.setTransform(125.2,-220.6,0.439,0.411);

	this.instance_2 = new lib.Image_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-415,-221,0.233,0.233);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#110408").s().p("EhGJAm0QgxAAgkgjQgjgjAAgyMAAAhJ3QAAgyAjgjQAkgjAxAAMCMTAAAQAyAAAjAjQAjAjAAAyMAAABJ3QAAAygjAjQgjAjgyAAg");

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFD358").s().p("AAAAwQgJAMgQAAQgXAAgMgRQgLgPABgbQABgcATgSQANgMASgCQAHAAAFABIAAAOQgIgDgLAEQgMADgHAMQgIALAAANQAAAkAZAAQAJAAAGgFQAGgFAAgKIAAgMIAOAAIAAAMQAAAKAHAFQAFAFAKAAQALAAAHgIQAHgHAAgMQAAgJgHgHQgIgHgLAAIgiAAQgBgVAJgLQAJgJARAAQAgABAAAZIgBAGIgLAAQALAEAGALQAHAKAAAMQAAAWgLAOQgMAOgTAAQgVAAgJgMgAAKgfIAVAAIAIABIABgEQAAgLgOAAQgPAAgBAOg");
	this.shape_25.setTransform(-264.8,189.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFD358").s().p("AhFAiQgDgLADgJIgLAAIAAgLIAIgJIALgLQgHAAgEgFQgFgFAAgHQAAgLAJgHQAIgGALgBQASgBAJANQARgMAZAAIAIAAQAYACAPASQAQATgCAXQgBAYgPAQQgQARgYAAIgIAAIAAgMQgPAMgYAAIgDAAQglAAgHgagAgGgiQAGABAHAEQAIAEAFAFQALANAAAOQABAXgNAPIAFABQALAAAIgFQAPgKABgUQACgNgHgMQgGgLgNgGQgJgFgQAAQgKAAgGACgAgogPQgKAGgNANIAJAAQgEAJADAIQAFAQAZgBQAOgBAKgIQAJgIABgNQAAgLgIgIQgHgJgMAAQgMAAgKAHgAgxgtIgGADQAGABACADQACAEgCAFIAHgFIAGgFQgDgEgFgCIgEAAIgDAAg");
	this.shape_26.setTransform(-280.6,189.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFD358").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_27.setTransform(-291.7,181.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFD358").s().p("AgaA8QgVgBgNgNQgOgNAAgUQgBgRAGgNIANAAQgBAFACACQACACAEAAQAHAAAFgEQAMgGACgJIABgFQAAgKgHgCQgFgDgGADQAJACAAALQAAAGgEAEQgEAEgGAAIgGgBQgLgEAAgNIACgJQAGgPAWAAQAIAAAHAEQAGAEACAHQAMgQAZABQAaABANAYQAJAQgBATQAAAogdAOQgJAFgLAAIgOgCIAAgKQgJAHgHADQgHACgLAAIgEAAgAAOgPQANALAAASQAAATgLAMIAJAAQAKAAAGgEQAPgLAAgXQgBgPgKgLQgKgMgPgBQgSgBgIAMIACAAQAKAAAIAGgAg7AIQAAAKAFAGQAHAKASAAIAJgBQAMgBAHgJQAGgHAAgKQAAgFgDgEQgDgGgKAAQgEgBgIAFIgMAHQgHADgGAAQgHAAgDgDIgBAGg");
	this.shape_28.setTransform(-296.9,189.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFD358").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_29.setTransform(-307.7,189.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFD358").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_30.setTransform(-317.7,187.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFD358").s().p("AgCBRQgagBgQgQQgPgQgBgZQAAgQAGgMIACgDQAFgHAGgIIADgCIAAAAIAEgEQAJgHARgRQAIgLAEgHIAYAAQgEAJgHAIQgIAIgKAIQARgBAMAEQAkAOAAAqQAAAagTASQgSAQgaAAIgDAAgAgfgMQgLAMAAAOQAAAPAKAKQANANAUAAQARAAAMgJQAPgMAAgSQAAgIgDgIQgEgLgMgGQgKgFgNAAQgWAAgMANgAg0grIgLAAIAAgLIALAAIAAgQIgLAAIAAgKIAfAAQAKAAAGAEQAHAGAAAKQAAAJgIAFQgJAEgMAAIAAARIAAAAIgDACIADgCIgDACQgGAIgFAHgAgmhFIAAAPQAMAAAAgHQAAgIgKAAIgCAAgAgpgXIAAAAg");
	this.shape_31.setTransform(-331.6,187.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFD358").s().p("AgVAzQgIAJgTAAQgLAAgHgHQgIgHAAgLQgBgMAGgIIgJAAIAAgJIAHgJIALgLIgDAAQgHAAgDgFQgEgEAAgHQAAgLAIgIQAIgJALgBQAUgBAJANQATgNAZABQAXACAQARQAQARABAXQABAngdARQgHAEgNAAIgLgBIAAgJQgJAKgLAAIgBAAQgLAAgJgJgAgJgkQASACALANQAOAPAAAUQAAAUgKALIAGABQAJAAAJgIQAIgIACgKIAAgJQAAgXgPgOQgNgMgXAAQgIAAgIACgAgngQQgLAKgKAKIAJAAQgFAIABAJQAAAEAEADQAEAEAGAAQAJAAADgKQACgGAAgQIANAAQAAARABAGQACAIAJAAQAHAAAFgIQAEgGAAgKQAAgKgGgJQgHgJgJgBIgGAAQgQAAgJAGgAg3gpQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_32.setTransform(-346.1,189.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFD358").s().p("AgEBMQgagBgPgQQgQgQgBgYQAAgQAHgMQAGgMANgMIAbgZQAJgLACgGIAZAAQgFAJgHAIQgHAIgLAHQARgBANAFQAkANAAAqQAAAbgUARQgSAQgZAAIgEAAgAghgQQgKAMAAAOQAAAOAJAKQANANAVAAQAQAAAMgJQAPgLAAgTQAAgIgDgHQgEgLgLgGQgLgGgNAAQgWAAgMAOg");
	this.shape_33.setTransform(-368.1,187.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFD358").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_34.setTransform(-381.8,187.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFD358").s().p("AgDBDQgJAPgUAAQgRAAgMgQQgKgOAAgTQAAgiAUgQQAMgJAiAAIAAANQgcAAgIAGQgHAFgDAGQgDAGAAALQAAAOAGAIQAGAJALAAQAVAAAAgQIAAgSIANAAIAAARQAAASAYAAQANAAAIgOQAGgLAAgQQAAgZgPgOQgKgIgOAAIg4AAQgTgBAAgQQAAgdA7AAQApAAAOANQAFAGAAAKQAAANgKABIAFAEQASATAAAiQAAAagLASQgMATgXAAQgUAAgJgPgAAcg8QgDADAAAEQAAAIAJAAQADAAADgCQADgDAAgEQAAgDgDgDQgDgDgDABQgEgBgCADgAgYg/QgMACAAAHQgBAFAJAAIAoAAQgCgCAAgEQAAgJADgCIgMgBQgPABgKADg");
	this.shape_35.setTransform(-396.5,187.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AhWArQgEgOADgLIgOAAIAAgOIALgMIAOgOQgKAAgFgGQgFgGAAgJQAAgNALgJQAJgIAPgBQAXgBAKAQQAWgPAeAAIALAAQAeACATAXQATAYgBAdQgCAegTAUQgUAWgdAAIgLgBIAAgPQgTAPgeABIgDAAQguAAgJghgAgIgrQAIABAJAFQAKAGAFAGQAOAQABASQAAAdgPATIAGABQAOAAAJgHQATgNACgYQACgRgIgOQgIgPgQgIQgMgGgUAAQgMAAgIADgAgzgTQgLAIgRAQIAMAAQgFALADALQAHAUAfgCQARgBAMgKQAMgKABgRQABgNgKgLQgKgLgPAAIgCAAQgOAAgMAJgAg+g4QgFABgCACQAHACACAEQADAFgCAGIAIgHIAIgGQgEgFgGgCIgEgBIgFABg");
	this.shape_36.setTransform(-287.6,154.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AghBGIAAgSQAEAEAKABQAJgBAGgRQAGgRAAgUQAAgUgFgRQgHgTgJgBQgIAAgGAFIAAgSQAHgGAPAAQAVgBANAbQAMAXgBAbQAAAcgMAVQgNAZgVAAQgMAAgJgGg");
	this.shape_37.setTransform(-301.8,154.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgpBYQgSgJgLgPQgPgXAAgiQAAgVAMgTQANgUASACQAQAAAFANQAJgPASAAQAQAAAMANQALAMABAQQACATgPAQQgQAPgTAAQgTAAgMgMQgNgMABgRQAAgFADgGIAEgKQABgEgCgCQgDgDgEABQgIABgFALQgFAKAAANQgBAZATAQQATAQAaAAQAaABATgUQATgUgBgaQAAghgQgTQgRgWggABQgoAAgQAdIgUAAQAJgXAVgMQAUgLAaAAQAoAAAWAWQAaAaABArQACArgYAdQgYAcgpAAQgWAAgSgIgAgZgHQAAAIAIAFQAIAGAJAAQALAAAJgHQAIgHAAgJIAAgFQgCAGgHAFQgHADgJAAQgUAAgEgSIgEANgAgEgcQAAAFADACQADAEAEAAQAEAAAEgDQADgDAAgFQAAgEgEgDQgDgDgEAAQgKAAAAAKg");
	this.shape_38.setTransform(-314.3,152);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgRAeQAOgJAAgIIgBgBIgHgFQgJgGAAgJQAAgHAEgGQAGgIAKAAQAHAAAGAEQAIAGAAALQAAAHgDAHQgDAGgGAHQgIAIgEADg");
	this.shape_39.setTransform(-336.2,160.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("Ag/BBQgXgbAAgoQABgoAZgaQAZgaAoAAQAjAAAYAXQAXAXAAAiQAAAcgPAUQgRAWgaAAQgaABgQgSQgQgQADgYQACgWARgMIgUAAIAAgSIBDAAIAAASIgNAAQgMAAgJAHQgIAIAAAMQAAALAIAJQAJAIAOgBQARAAAKgNQAJgMgBgRQgBgWgKgNQgQgSghAAQgbAAgQAYQgOATAAAfQAAAbAOAUQAOAXAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgageg");
	this.shape_40.setTransform(-350.3,156.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgTAkIAAgeIgNAAIAAgOIANAAIAAgTIgOAAIAAgOIAnAAQAMABAHAFQAJAIAAALQAAAMgKAFQgLAFgPAAIAAAkgAgCgaIAAASQAOABAAgKQAAgKgMAAIgCABg");
	this.shape_41.setTransform(-362.8,144);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("Ag/BBQgXgbAAgoQABgoAZgaQAZgaAoAAQAjAAAYAXQAXAXAAAiQAAAcgPAUQgRAWgaAAQgaABgQgSQgQgQADgYQACgWARgMIgUAAIAAgSIBDAAIAAASIgNAAQgMAAgJAHQgIAIAAAMQAAALAIAJQAJAIAOgBQAQAAALgNQAJgMgBgRQgBgWgKgNQgQgSghAAQgbAAgQAYQgOATAAAfQAAAbAOAUQAOAXAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgageg");
	this.shape_42.setTransform(-368.5,156.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgiBGIAAgSQAGAEAJABQAJgBAHgRQAFgRAAgUQAAgUgFgRQgHgTgJgBQgJAAgGAFIAAgSQAJgGAOAAQAVgBANAbQALAXAAAbQABAcgMAVQgOAZgUAAQgNAAgKgGg");
	this.shape_43.setTransform(-381.3,154.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AARBKIAAgOQgMAJgIADQgLADgSAAQgegBgRgWQgOgUAAgdQAAgyAigUQALgHATAAIAJABIAAARQAVgTAbABQAgABARAVQAQATABAgQAAAggRAVQgTAXgfAAIgKgBgAALgYQAaAHgBAfQAAAYgQATQAQACAMgHQATgNgCggQAAgSgMgOQgMgPgSgBQgigCgFAXQAIgFAKAAIAJABgAg8gqQgKAOAAATQAAAUALANQALAOAUABQAQABAMgIQALgHABgNQABgHgFgFQgEgHgHAAQgGABgEAEQgDADAAAGIABAHIgSAAQgEgSACgRQACgQANgSIgLgCQgSAAgLAPg");
	this.shape_44.setTransform(-394.3,154.1);

	this.instance_3 = new lib.Bitmap3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(62,-19,0.709,0.709);

	this.instance_4 = new lib.Bitmap2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(61,-196,1.12,1.12);

	this.instance_5 = new lib.Bitmap1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-406,-196,1.178,1.178);

	this.instance_6 = new lib.Bitmap6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-423,-201,1.299,1.299);

	this.instance_7 = new lib.Bitmap5();
	this.instance_7.parent = this;
	this.instance_7.setTransform(85,-201,0.977,0.977);

	this.instance_8 = new lib.Bitmap4();
	this.instance_8.parent = this;
	this.instance_8.setTransform(85,-45,0.76,0.76);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFD54F").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_45.setTransform(-245.4,192.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFD54F").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_46.setTransform(-280.9,192.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFD54F").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_47.setTransform(-298.2,185.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFD54F").s().p("AgyA1QgTgXAAggQABggAUgUQAUgVAgAAQAcAAATASQATATAAAbQAAAXgNAQQgNAQgVABQgVAAgMgOQgNgNACgSQACgSANgLIgQAAIAAgNIA2AAIAAANIgKAAQgKAAgHAHQgGAGgBAKQAAAJAGAHQAIAGALgBQANAAAJgLQAHgIgBgOQgBgRgIgLQgMgPgbAAQgVAAgNATQgLARAAAXQAAAWALARQALARAUADIALABQAZAAANgUIARAAQgRAmgvAAQggAAgUgXg");
	this.shape_48.setTransform(-302.8,195.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFD54F").s().p("AghBGQgOgHgJgMQgMgSAAgbQAAgSAKgPQAKgPAOABQAOAAAEAKQAGgLAPAAQANAAAJAKQAJAJABAOQABAPgMAMQgMAMgQAAQgOAAgKgJQgKgKAAgNQAAgFACgEQAEgGAAgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgCgCgEAAQgGABgEAJQgEAIAAAKQgBAUAQANQAPANAUAAQAVAAAPgPQAPgQAAgVQgBgagMgQQgOgRgZAAQghAAgMAYIgQAAQAHgSARgKQAQgJAVAAQAfAAASASQAVAVABAiQABAigTAXQgTAXghAAQgRAAgPgHgAgUgFQABAGAGAEQAGAEAHABQAIAAAIgGQAHgFAAgIIgBgEQgBAFgGAEQgGADgGAAQgQAAgEgOIgDAKgAgDgWQAAADADADQABACAEAAQADAAADgCQACgCAAgEQAAgDgCgDQgDgCgDAAQgIAAAAAIg");
	this.shape_49.setTransform(-317.4,192.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFD54F").s().p("AgbA4IAAgOQAEADAIAAQAHAAAFgOQAFgNAAgQQAAgQgFgOQgFgPgHAAQgHgBgFAEIAAgOQAHgFAMAAQAQAAAKAVQAKASAAAWQAAAWgKARQgLATgQAAQgKAAgIgEg");
	this.shape_50.setTransform(-335,193.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFD54F").s().p("AARAuQANAAAJgFQAKgGAEgLQAEgMAAgMQgCgNgJgJQgJgLgOAAQgWACgFATQAEgDAFAAQAHAAAEADQANAJgBAVQAAATgRAMQgPALgVAAQgWAAgPgNQgOgOAAgUQAAgWAQgLQgFgBgDgEQgDgFAAgGQAAgLAKgGQAIgGAMAAQAKAAAIAGQAHAHADAKQANgWAbgBQAWAAAPATQANARAAAYQAAAZgQARQgRARgbAAgAg0gHQgGAGAAAIQAAAMALAHQAKAGAPAAQAMAAAKgGQAJgFABgKQABgEgCgFQgDgDgEABQgDAAgCABQgDADABAEIgOAAIAAgLIABgKIgSAAQgJAAgHAGgAgwgrQADACACAFQACAFgCAFIAPAAQAAgJgFgFQgEgEgHAAIgEABg");
	this.shape_51.setTransform(-345.6,193.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFD54F").s().p("AgjBCQgSgMgCgXQgCgUANgLIAKgGIgGgEQgDgFACgFQgEgBgGAEQgFAEgDAGQgGAKAAAPQAAAPAIAQQAHAQAPAMIgTAAQgUgQgFgeIgBgNQAAgPAFgLQAFgMAIgGQALgJANAAIAEABIAAAEQAKgHARACQAUACAJAMQAFAIABAQIgiAAQgcAAgCASQgCAOAQAIQAOAHASgBQAUgBANgMQAOgLAEgSIABgKQAAgQgKgPQgJgOgNgGIgGAHQgFAEgDABQgKAFgJAAQgPgBgFgHQgEgFADgKQABgDgIAAIgGABIAAgNQAMgCAMAAQAPAAALADQAcAFASATQAWAWAAAiQAAAegUAVQgUAVggAAQgbAAgRgMgAgQgVQADABAAAEQABAEgDACIAZAAQgCgHgKgDIgIgBIgGAAgAgKg4QABAFAIAAQALAAAFgIQgKgEgQAAIABAHg");
	this.shape_52.setTransform(-369.4,192.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFD54F").s().p("AgxA/QgUgOABgaQABgWASgIQgJgCAAgLQAAgMANgGQAKgFANABQAQABAJAMQALANgBAQIgpAAQgJAAgGAFQgHAFABAJQABAVAbAGQAJACAMAAQAXAAAPgQQANgOAAgVQABgYgOgRQgOgSgbAAQgfgBgQAXIgPAAQAHgRARgKQARgJAVAAQAfAAAUAWQAVAXAAAfQAAAggSAVQgUAYgfAAQgeAAgTgOgAghgbQADACACAEQABAFgCAEIAaAAQgCgHgFgEQgFgGgJAAIgJACg");
	this.shape_53.setTransform(-384.9,192.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFD54F").s().p("AgtA2QgVgRAAgbQAAgZATgOQgCgMAFgJQALgUAVACQALABAHAHQAHgJAPABQAOAAAIAJQAIALAAAXIg6AAIgYABQgBAQAMAKQALALAPgCQALgBAJgGQAJgGACgKIAOAAQgEARgNAKQgOALgSAAQgjABgOgZIgBAJQAAARAPAMQANALASAAQAaAAAOgQQAIgKgBgMIAPAAQABAPgLAPQgSAaglAAQgbAAgUgPgAgVgyQgDAFAAAHIALgBIAMAAQAAgPgLAAQgGAAgDAEgAANgnIAYAAQgBgPgLAAQgMAAAAAPg");
	this.shape_54.setTransform(-399,194.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFD54F").s().p("AgxBDQgTgNAAgWQAAgVANgIIgNAAIAAgMIASAAQgEgCAAgGQAAgIAGgEQAKgIASACQAVACAIAMQAGAIAAAQIghAAQgdAAgCASQgBAOAQAIQAOAHATgBQATgBANgMQANgLAEgSIABgKQAAgQgJgPQgJgOgOgGIgGAHQgEAEgEABQgIAFgLAAQgPgBgFgHQgEgFADgKQABgDgIAAIgFABIAAgNQALgCANAAQAQAAAKADQAbAFATATQAVAWAAAiQABAegVAVQgUAVgfAAQgcAAgSgLgAgcgVQACABABAEQAAAEgDACIAaAAQgBgHgLgDIgIgBIgGAAgAgXg4QABAFAJAAQALAAAFgIQgJgEgRAAIAAAHg");
	this.shape_55.setTransform(-413,192.2);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAwAiQANgIAAgLIgBgEQgDgJgSgEQgRgEgWAAQgVAAgRAEQgTAEgCAJIgBAEQAAAMALAHIgQAAQgOgEAAgVQAAgHACgEQAJgSAXgIQARgFAcAAQBAAAANAfQADAEAAAHQAAAUgPAFg");
	this.shape_56.setTransform(-340.5,149.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("Ag4BDQgagVAAgiQAAgfAWgRQgBgQAGgLQANgZAbACQANABAJAKQAJgLATAAQASABAJALQAKANAAAdIhIAAIgfACQAAATAOANQAOAOATgCQAOgBALgJQALgHACgNIASAAQgEAWgRANQgRAOgXAAQgrAAgSgfIgBALQAAAWARAPQARAOAWAAQAigBARgUQAKgMgBgPIASAAQACATgNASQgYAiguAAQgiAAgYgUgAgbg+QgEAFABAJIANgBIAPAAQAAgTgOAAQgHAAgEAGgAAQgxIAdAAQAAgTgNAAQgQAAAAATg");
	this.shape_57.setTransform(-339.8,159.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("Ag9BTQgYgQAAgcQAAgZAQgLIgPAAIAAgPIAVAAQgFgDAAgHQAAgJAIgGQANgJAWABQAbADAKAPQAHAKAAAUIgqAAQgkAAgCAXQgCASAUAKQASAIAXgBQAZgCAQgOQARgOAEgXQACgGAAgGQAAgUgMgTQgLgRgRgIIgIAJIgJAGQgMAGgNAAQgSgBgHgIQgFgHAEgMQABgFgKAAIgGABIAAgQQAOgBAQAAQATAAANACQAjAIAXAXQAaAbABArQAAAmgZAaQgZAagnAAQgkAAgWgOgAgkgbQAEACABAEQAAAFgEADIAhAAQgCgJgOgEIgJgCIgJABgAgchHQABAGAKAAQAPAAAFgKQgLgFgVAAIABAJg");
	this.shape_58.setTransform(-357.4,156.6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("Ag+BPQgZgSACghQABgcAXgKQgMgCAAgNQAAgQAQgIQANgGAQACQAUABAMAPQANARgBAUIgzAAQgLAAgIAFQgIAIAAAKQABAbAjAHQALACAPAAQAdAAASgTQARgSAAgbQABgegRgVQgSgVgjgBQgmgBgUAcIgTAAQAJgVAWgMQAVgLAaAAQAnAAAZAbQAaAdAAAnQAAAogXAaQgYAegnAAQgmAAgYgRgAgpgiQADADACAEQACAGgDAGIAgAAQgBgJgHgGQgHgGgKAAIgLACg");
	this.shape_59.setTransform(-391.5,156.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhKAzIAAgcICEAAIAAgvIAJgMIAIgOIAABlg");
	this.shape_60.setTransform(-409.5,166.1);

	this.instance_9 = new lib.Bitmap9();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-395,-214,1.303,1.303);

	this.instance_10 = new lib.Bitmap8();
	this.instance_10.parent = this;
	this.instance_10.setTransform(116,-214,1.003,1.003);

	this.instance_11 = new lib.Bitmap7();
	this.instance_11.parent = this;
	this.instance_11.setTransform(116,-42,0.682,0.682);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFD54F").s().p("AAAAwQgJAMgQAAQgXAAgMgRQgLgPABgbQABgcATgSQANgMASgCQAHAAAFABIAAAOQgIgDgLAEQgMADgHAMQgIALAAANQAAAkAZAAQAJAAAGgFQAGgFAAgKIAAgMIAOAAIAAAMQAAAKAHAFQAFAFAKAAQALAAAHgIQAHgHAAgMQAAgJgHgHQgIgHgLAAIgiAAQgBgVAJgLQAJgJARAAQAgABAAAZIgBAGIgLAAQALAEAGALQAHAKAAAMQAAAWgLAOQgMAOgTAAQgVAAgJgMgAAKgfIAVAAIAIABIABgEQAAgLgOAAQgPAAgBAOg");
	this.shape_61.setTransform(-233.8,182.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFD54F").s().p("AgCBRQgagBgQgQQgPgQgBgZQAAgQAGgMIACgDQAFgHAHgIIACgCIAAAAIgCACQgHAIgFAHIAAgjIgLAAIAAgLIALAAIAAgQIgLAAIAAgKIAfAAQAKAAAGAEQAHAGAAAKQAAAJgIAFQgJAEgMAAIAAARIAEgEQAJgHARgRQAIgLAEgHIAYAAQgEAJgHAIQgIAIgKAIQARgBAMAEQAkAOAAAqQAAAagTASQgSAQgaAAIgDAAgAgfgMQgLAMAAAOQAAAPAKAKQANANAUAAQARAAAMgJQAPgMAAgSQAAgIgDgIQgEgLgMgGQgKgFgNAAQgWAAgMANgAgmhFIAAAPQAMAAAAgHQAAgIgKAAIgCAAgAgmgZIAAAAIgCACIACgCg");
	this.shape_62.setTransform(-300.6,180.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFD54F").s().p("AgVAzQgIAJgTAAQgLAAgHgHQgIgHAAgLQgBgMAGgIIgJAAIAAgJIAHgJIALgLIgDAAQgHAAgDgFQgEgEAAgHQAAgLAIgIQAIgJALgBQAUgBAJANQATgNAZABQAXACAQARQAQARABAXQABAngdARQgHAEgNAAIgLgBIAAgJQgJAKgLAAIgBAAQgLAAgJgJgAgJgkQASACALANQAOAPAAAUQAAAUgKALIAGABQAJAAAJgIQAIgIACgKIAAgJQAAgXgPgOQgNgMgXAAQgIAAgIACgAgngQQgLAKgKAKIAJAAQgFAIABAJQAAAEAEADQAEAEAGAAQAJAAADgKQACgGAAgQIANAAQAAARABAGQACAIAJAAQAHAAAFgIQAEgGAAgKQAAgKgGgJQgHgJgJgBIgGAAQgQAAgJAGgAg3gpQAFABABAGQACAFgDAFIAIgHIAIgHQgCgGgIAAIgCAAQgHAAgCADg");
	this.shape_63.setTransform(-315.1,182.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFD54F").s().p("AgPAdIAAgYIgLAAIAAgLIALAAIAAgPIgLAAIAAgLIAfAAQAJAAAGAFQAHAFAAAKQAAAJgIAEQgIAEgMAAIAAAcgAgBgVIAAAPQALAAAAgHQAAgIgKAAIgBAAg");
	this.shape_64.setTransform(-333.4,174.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFD54F").s().p("AgyA0QgTgWAAggQABgfAUgWQAUgUAgAAQAcAAATASQATASAAAcQAAAWgNARQgNAQgVAAQgVABgMgOQgNgMACgUQACgRANgLIgQAAIAAgNIA2AAIAAANIgKAAQgKABgHAGQgGAGgBAKQAAAJAGAHQAIAGALgBQANABAJgMQAHgIgBgOQgBgRgIgLQgMgPgbAAQgVAAgNATQgLAQAAAYQAAAWALARQALASAUADIALAAQAZAAANgTIARAAQgRAlgvAAQggAAgUgYg");
	this.shape_65.setTransform(-338,184.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFD54F").s().p("AgsBEQgTgMgBgVQgCgXATgJQgFgBgEgDQgDgEAAgGIABgFQAGgQAfACQASABAIANQAJALgBARIgoAAQgTAAAAAPQAAALAPAGQAMAFASAAQAUAAAOgLQANgLABgQQABgWgLgOQgJgLgQAAIhHAAIAAgLQAQAAABgCQAAgBgEgCQgFgCAAgEQgCgTA2AAQAWAAASAJQATAKABARIgQAAQAAgKgPgGQgNgFgRAAQgZAAgCAIQgBAFALAAIAhAAQAPAAANAKQAWARAAAnQAAAbgVARQgUARgbAAQgZAAgRgKgAgggQQADACABAFQAAAFgDAEIAcAAQgCgIgFgEQgGgFgKAAIgGABg");
	this.shape_66.setTransform(-374.4,181.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFD54F").s().p("AgmArQgRgRAAgaQAAgZARgSQARgQAZAAQAbAAANAMQAJAHAAAMIgNAAQgBgIgLgFQgKgEgNgBQgQABgMAKQgOANAAAQQAAAQAMALQAMAKARAAQANAAAJgHQAMgIAAgNQAAgQgPgFQAFAGAAALQAAAGgHAHQgHAFgJAAQgKAAgHgGQgIgHAAgKQAAgLAJgIQAKgGAMAAQASAAANALQAOAKAAASQAAAWgPAPQgQAPgWAAQgYAAgRgRgAgDgNQgCADAAADQAAAFACACQADADAEAAQAEAAADgDQADgCAAgEQAAgEgDgDQgDgEgEAAQgEAAgDAEg");
	this.shape_67.setTransform(-387.1,183);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhGBYQgTgHgGAAQgJAAgIAKIgNgJIAagnQgLgOAAgXQAAgIACgIQAGgYAVgMQAQgJAVAAIALAAIAagmIATAAIgZAnQAJADAEADQAGAEAFAEQADgFAEgEQAJgGALAAQAMAAAJAJQAKgKAPABQAoABACBFQAAAggLAVQgOAagcAAQgTAAgKgLQgKgKAAgRIAAgIIASAAIAAAFQAAAWATAAQAMgBAFgTQACgKABgZQABgUgDgPQgEgTgIAAQgMAAAAASIAAAkIgSAAIAAgkQAAgSgLgBQgFAAgEADQgDAEgBADQAPASgBAcQgBAigUATQgTATgiABIgCAAQgNAAgSgGgAhJBFQAOAFANAAIANgBQATgDALgOQAKgOAAgTQAAgSgLgNQgMgOgSgBIgQAXIAJAAQAKAAAIAFQAIAHABAKQABAMgGAKQgHAJgLAEQgJADgKAAQgRAAgMgHIgKANIAHAAQAIAAAHADgAhLAmQADADAGABQALACAFgCQgKgCgDgHQgDgFACgGgAgxAYQAAAIAIAAQAIAAAAgIQAAgIgIAAQgIAAAAAIgAhSgGQgGAHAAANQAAAGACAFIAfgvQgTADgIANg");
	this.shape_68.setTransform(-273.7,145.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AADBbQgNgGgBgOQAAgMAIgFQggABgVgSQgVgRACgbQACgfAagPQgFgBgDgFQgDgFAAgFIABgGQAGgNAOgFQAKgDAUAAQAiAAAPASQAMAOABAUIg2AAQgQAAgLALQgLAKgBAPQgCARAQANQAOAMATABQAeACAVgMIAAASQgLAFgJACQgTAFAAAIQgBAFAFAEQAGADAHAAQAJAAAGgEQAGgFAAgIIgBgGIARAAIABAHQAAAQgNAKQgMAKgSABIgGABQgNAAgLgGgAgXhMIAEACQACACAAADQABAHgFACIAIgBIAsAAQgGgLgNgFQgHgBgLAAQgLAAgGACg");
	this.shape_69.setTransform(-292.6,149.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgwA2QgVgVAAggQAAghAVgVQAWgVAfAAQAiAAARAOQAKAKABAOIgRAAQgBgJgOgHQgMgEgQAAQgUAAgRANQgQAPAAAUQAAAVAPANQAPANAUAAQARAAAMgJQAOgKABgQQgBgUgSgGQAGAHAAAOQAAAIgJAIQgIAHgMAAQgNAAgIgIQgKgIAAgOQAAgOAMgJQALgIAPAAQAYAAAQANQARAOAAAWQAAAdgUARQgTATgbAAQgeAAgWgVgAgDgRQgEADABAGQgBAFAEADQADAEAEAAQAGAAAEgEQADgDAAgFQAAgFgDgEQgDgDgGAAQgFgBgDAEg");
	this.shape_70.setTransform(-307.8,147.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgTAkIAAgeIgNAAIAAgNIANAAIAAgUIgOAAIAAgOIAnAAQAMAAAHAHQAJAGAAAMQAAALgKAGQgLAFgPAAIAAAjgAgCgaIAAATQAOAAAAgJQAAgLgMAAIgCABg");
	this.shape_71.setTransform(-318.6,137.6);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("Ag/BBQgXgcAAgoQABgoAZgZQAZgaAoAAQAjAAAYAXQAXAXAAAiQAAAcgPAUQgRAWgaAAQgaAAgQgRQgQgQADgYQACgWARgMIgUAAIAAgSIBDAAIAAASIgNAAQgMAAgJAHQgIAIAAAMQAAALAIAJQAJAIAOgBQARAAAKgNQAJgMgBgRQgBgWgKgNQgQgSghAAQgbAAgQAYQgOATAAAeQAAAcAOAVQAOAWAZAEIANABQAgAAARgZIAVAAQgVAvg8AAQgnAAgageg");
	this.shape_72.setTransform(-324.3,149.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AggA+QgOgMAAgVQAAgPAGgNQAEgHAKgLQAMgNAegYIg6AAIAAgRIBVAAIAAAQIgTAOQgKAIgIAIQgMAPgDAFQgIAMABAMQABAJAGAFQAFAEAHAAQAQAAAFgNIABgIQAAgGgDgGIARAAQAJANgCAQQgBARgOALQgNALgSABQgUAAgMgLg");
	this.shape_73.setTransform(-338.3,148);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("Ag+BPQgZgTACggQABgbAXgLQgMgDAAgNQAAgPAQgHQANgHAQABQAUABAMAQQANAQgBAVIgzAAQgLAAgIAGQgIAHAAALQABAaAjAIQALABAPAAQAdAAASgTQARgSAAgbQABgegRgVQgSgWgjAAQgmgCgUAdIgTAAQAJgVAWgMQAVgLAaAAQAnAAAZAbQAaAcAAAoQAAAogXAbQgYAdgnAAQgmAAgYgRgAgpgjQADADACAFQACAGgDAGIAgAAQgBgJgHgGQgHgGgKAAIgLABg");
	this.shape_74.setTransform(-352.4,145.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("Ag3BUQgYgNgCgbQgCgdAYgMQgHAAgEgFQgEgFAAgHIABgHQAIgTAnACQAWABALAQQALAPgBAVIgzAAQgYAAAAASQABAPASAHQAOAGAYAAQAZAAARgNQARgOABgUQACgcgPgSQgLgNgUAAIhZAAIAAgOQAUAAABgCQAAgCgFgDQgGgCAAgFQgCgYBDABQAcgBAWALQAYANABAWIgUAAQABgNgTgHQgRgHgVAAQggAAgCAJQgBAIANAAIAqAAQATAAAQAMQAbAVAAAxQAAAigaAVQgZAWgiAAQgfAAgVgOgAgogVQAEAEAAAFQABAHgFAEIAkAAQgCgKgHgFQgIgFgMgBIgHABg");
	this.shape_75.setTransform(-369.9,145.6);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgwA2QgVgVAAggQAAghAVgVQAWgVAfAAQAjAAAQAOQAKAKABAOIgRAAQgBgJgOgHQgMgEgQAAQgUAAgRANQgQAPAAAUQAAAVAPANQAPANAUAAQARAAAMgJQAOgKABgQQgBgUgSgGQAGAHAAAOQAAAIgJAIQgIAHgMAAQgNAAgIgIQgKgIAAgOQAAgOAMgJQALgIAPAAQAYAAAQANQARAOAAAWQAAAdgUARQgTATgbAAQgeAAgWgVgAgDgRQgEADABAGQgBAFAEADQADAEAEAAQAGAAAEgEQADgDAAgFQAAgFgDgEQgEgDgFAAQgFgBgDAEg");
	this.shape_76.setTransform(-385.6,147.8);

	this.instance_12 = new lib.Bitmap12();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-411,-214,1.343,1.343);

	this.instance_13 = new lib.Bitmap11();
	this.instance_13.parent = this;
	this.instance_13.setTransform(112,-214,1.004,1.004);

	this.instance_14 = new lib.Bitmap10();
	this.instance_14.parent = this;
	this.instance_14.setTransform(112,-43,0.708,0.708);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFD54F").s().p("AAOBJQgLgFgBgLQAAgKAHgEQgZABgRgOQgRgOACgVQABgZAVgMQgIgCABgLQgIADgIANQgHAOAAAPQAAAQAJAQQAIANAKAGIgTAAQgHgEgHgMQgJgRAAgTQgBgPAHgPQAFgMAIgHQAOgNANgCIAAAEQAKgFAUAAQAcAAAMAOQAKALAAAQIgrAAQgMAAgJAJQgJAIgBAMQgBANAMALQALAKAQAAQAYABARgJIAAAOQgJAEgHACQgPAEgBAGQAAAFAEACQAEADAGAAQAHAAAFgDQAFgEAAgHIgBgFIAOAAIABAGQAAANgLAIQgKAIgOABIgGAAQgKAAgIgEgAgHg9QAEACAAAEQABAFgDACIAFgBIAkAAQgEgJgLgDQgGgCgJAAQgIABgFABg");
	this.shape_77.setTransform(-323.4,185.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFD54F").s().p("AgmArQgRgRAAgZQAAgbARgQQARgRAZAAQAbAAANALQAJAJABAKIgPAAQAAgHgLgFQgKgFgMABQgRAAgNALQgNAMAAAQQAAAQAMAKQAMALAQAAQANAAALgHQALgIAAgNQAAgRgQgDQAGAFAAALQAAAHgHAGQgHAFgJABQgKAAgIgHQgHgHAAgKQAAgLAKgHQAIgHAMAAQAUAAANALQANALAAARQAAAXgQAOQgPAPgWAAQgYAAgRgRgAgCgOQgEAEAAAEQAAAEAEACQACADADAAQAFAAADgDQADgCAAgEQAAgFgDgDQgDgCgEAAQgEAAgCACg");
	this.shape_78.setTransform(-365,183.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFD54F").s().p("Ag7ApIAAgXIBqAAIAAglIAHgKQAEgGACgFIAABRg");
	this.shape_79.setTransform(-385.5,189.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFD54F").s().p("AgxBAQgWgRACgYQABgTAPgKQgKgCAAgLQAAgMAMgFQAJgEANAAQATAAAIALQAFgKARgBIAKAAIAHgGIAFgHQAIgMAAgNIAUAAIgEAOQgDAIgEAGQgFAHgHAGQAKAFACAMQABAGgDAFIgLAAQAFACAGAGQAKAJAAARQAAAYgVAPQgUAPgbAAQgbAAgVgPgAgtAGQgGAGAAAJQABANAQAJQAPAHASAAQAUAAANgHQARgIABgPQAAgIgGgHQgGgFgNAAIg3AAQgJAAgGAGgAARgaQgGABgDADQgFAEAAAFIAXAAQAIAAAFACQACgDgCgFQgDgIgOAAIgFABgAglgZQAEACABADQABAEgDADIAZAAQgBgJgJgDQgEgCgEAAQgFAAgFACg");
	this.shape_80.setTransform(-385.3,181.5);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgpBYQgSgJgLgPQgPgXAAghQAAgXAMgSQANgUASACQAQAAAFAMQAJgOASAAQAQAAAMAMQALANABAQQACAUgPAOQgQAPgTABQgTAAgMgMQgNgMABgRQAAgGADgGIAEgJQABgEgCgDQgDgCgEAAQgIABgFAMQgFAKAAANQgBAZATAQQATAQAaAAQAaAAATgTQATgUgBgbQAAgggQgUQgRgUggAAQgoAAgQAcIgUAAQAJgWAVgNQAUgLAaAAQAoAAAWAXQAaAaABArQACAqgYAdQgYAdgpAAQgWABgSgJgAgZgHQAAAIAIAFQAIAGAJAAQALAAAJgHQAIgGAAgKIAAgFQgCAGgHAEQgHAEgJAAQgUABgEgTIgEANgAgEgcQAAAEADADQADAEAEAAQAEAAAEgDQADgDAAgEQAAgFgEgDQgDgDgEAAQgKAAAAAKg");
	this.shape_81.setTransform(-342.1,146);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AhKAzIAAgcICEAAIAAgvIAJgLIAJgPIAABlg");
	this.shape_82.setTransform(-379.6,155.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("Ag9BQQgbgVABgeQACgYATgMQgNgDAAgPQAAgOAQgHQALgFAQAAQAYAAAJAPQAHgNAWgCIAMABIAJgIIAGgIQAJgPABgQIAZAAIgFAQQgEAKgFAIQgGAJgJAIQANAGACAOQABAIgDAHIgOAAQAGACAIAHQAMALAAAWQAAAegbATQgZASghAAQgiAAgagSgAg4AHQgIAIAAALQABARAVAKQASAJAXAAQAYAAASgIQAVgKABgTQAAgKgHgIQgIgHgRAAIhEAAQgLAAgIAHgAAVghQgHACgEADQgFAFAAAHIAcAAQAJAAAHACQADgEgDgGQgEgJgRAAIgHAAgAguggQAFADABAEQABAFgDAEIAfAAQgCgMgKgEQgGgCgGAAQgGAAgFACg");
	this.shape_83.setTransform(-379.5,145.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AAaBJIAAgLQgLAMgPABQgPAAgLgMQgKAMgXAAQgOAAgKgJQgKgJAAgOQAAgPAHgJIgMAAIAAgMIAJgLIAOgPIgEABQgIAAgEgGQgFgGAAgIQAAgOAKgKQAKgLAOgBQAZgCALARQAYgRAfACQAdACAUAVQAUAWABAdQABAwgkAWQgIAFgQAAQgJAAgFgCgAgLgtQAXABAOARQARATAAAZQAAAZgNAOIAIABQALAAALgKQALgKACgNIAAgKQAAgegTgRQgQgPgdAAQgKAAgKADgAgwgUQgOALgMAOIALAAQgHAKABALQABAFAFAEQAFAEAHAAQALAAAEgMQADgHAAgUIAQAAQgBAVACAHQADALALgBQAJAAAGgJQAFgJAAgLQAAgNgIgMQgIgLgLgBIgJgBQgUAAgKAJgAhEg0QAGACABAGQACAHgDAGIAJgJIAKgJQgCgHgKAAIgCAAQgJAAgCAEg");
	this.shape_84.setTransform(-398.8,148.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24,p:{y:0,x:0}},{t:this.instance_2},{t:this.instance_1},{t:this.instance},{t:this.shape_23},{t:this.shape_22,p:{x:-384.4,y:156.7}},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14,p:{x:-288.7,y:156.7}},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8,p:{x:-191.8,y:154.8}},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:-372.5,y:191.9}},{t:this.shape_4,p:{x:-362.1,y:191.9}},{t:this.shape_3},{t:this.shape_2,p:{x:-337.1,y:191.9}},{t:this.shape_1,p:{x:-324.7,y:192.2}},{t:this.shape,p:{x:-312.8,y:192}}]}).to({state:[{t:this.shape_24,p:{y:-5,x:0}},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.shape_44,p:{x:-394.3,y:154.1}},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_8,p:{x:-268.9,y:152.2}},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25}]},1).to({state:[{t:this.shape_24,p:{y:0,x:0.5}},{t:this.shape_22,p:{x:-410.4,y:158.7}},{t:this.shape_60},{t:this.shape_59},{t:this.shape_8,p:{x:-374.3,y:156.8}},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56,p:{x:-340.5,y:149.3}},{t:this.shape_14,p:{x:-321.8,y:158.7}},{t:this.shape_55},{t:this.shape_54,p:{x:-399,y:194.9}},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49,p:{x:-317.4,y:192.2}},{t:this.shape_48},{t:this.shape_47,p:{x:-298.2,y:185.8}},{t:this.shape_46},{t:this.shape_5,p:{x:-266,y:193.9}},{t:this.shape_4,p:{x:-255.6,y:193.9}},{t:this.shape_45,p:{x:-245.4,y:192.3}},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6}]},1).to({state:[{t:this.shape_24,p:{y:0,x:0.5}},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_56,p:{x:-270.2,y:138.3}},{t:this.shape_14,p:{x:-251.5,y:147.7}},{t:this.shape_67},{t:this.shape_66},{t:this.shape_45,p:{x:-360.4,y:181.3}},{t:this.shape_1,p:{x:-349.2,y:183.2}},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63,p:{x:-315.1,y:182.9}},{t:this.shape_62},{t:this.shape_49,p:{x:-286.7,y:181.2}},{t:this.shape_4,p:{x:-276.7,y:182.9}},{t:this.shape,p:{x:-265.9,y:183}},{t:this.shape_47,p:{x:-260.7,y:174.8}},{t:this.shape_2,p:{x:-249.6,y:182.9}},{t:this.shape_61},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9}]},1).to({state:[{t:this.shape_24,p:{y:0,x:0.5}},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_44,p:{x:-360.8,y:148.1}},{t:this.shape_81},{t:this.shape_63,p:{x:-400.8,y:183.3}},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_45,p:{x:-351.9,y:181.7}},{t:this.shape_54,p:{x:-337.8,y:184.3}},{t:this.shape_77},{t:this.shape_4,p:{x:-314.4,y:183.3}},{t:this.shape_49,p:{x:-304.1,y:181.6}},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-461,-248.4,922,496.9);


(lib.home_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAwDBIAAh5QAAgJgHgIQgHgHgKAAIgvAAQgKAAgHAHQgHAIAAAJIAAB5IhIAAQgKAAgIgHQgHgHAAgKIAAiRIgYAAQgKAAgHgHQgHgHAAgKQAAgGAEgGIAJgLICcibIAKgJQAHgFAGAAQAIAAAGAFIAKAJIAwAwIAAgOQAAgKAHgHQAIgHAKAAIAYAAQAKAAAHAHQAHAIAAAJIAABWIAjAjIAJALQAEAGAAAGQAAAKgHAHQgHAHgKAAIgYAAIAACRQAAAKgHAHQgHAHgKAAg");
	this.shape.setTransform(-7.6,259.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EF5350").s().p("AjXGwQhUAAg9g7Qg8g8AAhWIAAnFQAAhWA8g7QA8g9BVAAIJ8AAIAANgg");
	this.shape_1.setTransform(0,258.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EF5350").s().p("EgCZA4fMAAAhw9IEzAAMAAABw9g");
	this.shape_2.setTransform(26.7,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.1,-361.5,84.2,723);


(lib.Path_1_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egr+AGGQg4AAgngnQgogoAAg4IAAn9QAAg4AognQAngoA4AAMBX9AAAQA5AAAnAoQAnAnAAA4IAAH9QAAA4gnAoQgoAng4AAg");
	this.shape.setTransform(295,39);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_2, new cjs.Rectangle(0,0,590,78), null);


(lib.Path_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egr+AGGQg4AAgngnQgogoAAg4IAAn9QAAg4AognQAngoA4AAMBX9AAAQA5AAAnAoQAnAnAAA4IAAH9QAAA4gnAoQgoAng4AAg");
	this.shape.setTransform(295,39);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_1, new cjs.Rectangle(0,0,590,78), null);


(lib.Path_1_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egr+AGGQg4AAgngnQgogoAAg4IAAn9QAAg4AognQAngoA4AAMBX9AAAQA5AAAnAoQAnAnAAA4IAAH9QAAA4gnAoQgoAng4AAg");
	this.shape.setTransform(295,39);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_0, new cjs.Rectangle(0,0,590,78), null);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egr+AGGQg4AAgngnQgogoAAg4IAAn9QAAg4AognQAngoA4AAMBX9AAAQA5AAAnAoQAnAnAAA4IAAH9QAAA4gnAoQgoAng4AAg");
	this.shape.setTransform(295,39);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1, new cjs.Rectangle(0,0,590,78), null);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(0,0,0,0)"],[0,1],0,-23.5,0,23.5).s().p("EhO4ADrIAAnVMCdxAAAIAAHVg");
	this.shape.setTransform(504.9,23.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0,0,1009.8,47), null);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#000000","rgba(0,0,0,0)"],[0,1],0,-23.5,0,23.5).s().p("EhO4ADrIAAnVMCdxAAAIAAHVg");
	this.shape_1.setTransform(504.9,23.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2, new cjs.Rectangle(0,0,1009.8,47), null);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#000000","rgba(0,0,0,0)"],[0,1],0,-23.5,0,23.5).s().p("EhO4ADrIAAnVMCdxAAAIAAHVg");
	this.shape_2.setTransform(504.9,23.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(0,0,1009.8,47), null);


(lib.close = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhghTIAOgNICzC0IgNANg");
	this.shape.setTransform(0,0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhgBUIC0i0IANANIizC0g");
	this.shape_1.setTransform(0,0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#AD2B2B").s().p("AjmEEIAAmVQAAgvAhghQAhgiAvAAIFcAAIAAGWQAAAughAiQghAhgvAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.1,-25.9,46.3,52);


(lib.back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABCBxIiDhxICDhw");
	this.shape.setTransform(-3.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABCBxIiDhxICDhw");
	this.shape_1.setTransform(3.3,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#FF7300","#F1C500"],[0,1],0,18.8,0,-18.7).s().p("AigC8QgbAAgUgUQgTgTAAgbIAAjzQAAgbATgTQAUgUAbAAIFBAAQAbAAAUAUQATATAAAbIAADzQAAAbgTATQgUAUgbAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.7,-18.7,45.4,37.6);


(lib.location = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(-0.9,-0.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHIgPAVQgLAYASAZg");
	this.shape_1.setTransform(-3.6,-5.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(3.4,0);

	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(0.1,0,1,1,0,0,0,8.6,11.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(7).to({_off:false},0).wait(1).to({regX:8.5,scaleX:1.06,scaleY:1.06,x:0,y:0.1},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.17,scaleY:1.17},0).wait(1).to({scaleX:1.23,scaleY:1.23,y:0},0).wait(1).to({scaleX:1.28,scaleY:1.28},0).wait(1).to({scaleX:1.34,scaleY:1.34},0).wait(1).to({scaleX:1.4,scaleY:1.4,y:0.1},0).wait(1).to({x:0.1,y:0.2},0).wait(1).to({scaleX:1.34,scaleY:1.34},0).wait(1).to({scaleX:1.28,scaleY:1.28},0).wait(1).to({scaleX:1.23,scaleY:1.23,x:0.2},0).wait(1).to({scaleX:1.17,scaleY:1.17,x:0.1},0).wait(1).to({scaleX:1.11,scaleY:1.11},0).wait(1).to({scaleX:1.06,scaleY:1.06,x:0.2},0).wait(1).to({scaleX:1,scaleY:1,x:0.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.5,-11.3,17.1,22.6);


(lib.btnm7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(-1,11.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(-1.9,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgaIANgFIAJgCQAKABADAHIgPAVQgLAYASAag");
	this.shape_1.setTransform(-4.6,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(2.4,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgCAyQgRgBgLgLQgKgKAAgQQAAgKAEgIQAEgHAJgIIARgQIAHgMIAQAAQgDAGgFAFQgEAFgHAFQALAAAIADQAYAIAAAbQAAASgNALQgMALgRAAIgBAAgAgVgKQgHAHAAAJQAAAKAGAGQAJAJANAAQAKAAAIgGQAKgIAAgMQAAgFgCgFQgDgHgHgEQgHgDgJAAQgNAAgIAJg");
	this.shape_3.setTransform(12.1,-10.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgtAWQgCgHACgGIgHAAIAAgHIAFgGIAIgHQgFAAgDgDQgDgDAAgFQAAgHAGgFQAFgEAIAAQALgBAGAIQALgHAQAAIAFAAQAQABAKAMQAKAMgBAPQgBAPgKALQgKALgQAAIgFAAIAAgIQgKAIgPAAIgBAAQgZAAgFgRgAgEgWQAEAAAFADQAFADADADQAHAIAAAKQAAAPgIAJIAEABQAHAAAFgEQAKgGABgNQAAgIgEgIQgEgHgIgEQgGgEgLAAQgGAAgEACgAgagKIgPAMIAGAAQgCAGABAGQAEAKAQgBQAJAAAHgGQAFgFABgIQAAgHgFgGQgFgFgIAAIgBAAQgHAAgGAEgAgggdIgEACQAEABABACQACACgBADIAEgDIAEgDQgCgDgDgBIgCAAIgDAAg");
	this.shape_4.setTransform(2.4,-9.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgFAAQgEAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgGALQgIANgKAAQgGAAgFgDg");
	this.shape_5.setTransform(-5,-9.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgVAuQgKgFgFgIQgIgLAAgSQAAgLAGgKQAHgKAJAAQAJABACAGQAFgHAJgBQAIABAGAGQAGAHABAJQABAJgIAIQgIAIgKAAQgKgBgGgFQgHgHABgIIABgGIADgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAgBAAQgEABgDAGQgCAFAAAGQgBAOAKAIQAKAJAOAAQANAAAKgLQAKgKgBgOQAAgQgIgLQgJgLgQABQgVgBgJAPIgKAAQAFgLALgHQAKgFAOAAQAUAAALALQAOAOABAWQABAWgNAPQgMAPgVAAQgMAAgJgEgAgNgDQABADADADQAEADAFAAQAFABAFgEQAFgEAAgEIgBgDQgBADgDACQgEACgEAAQgKABgDgKIgCAHgAgCgOIACAEIADABQABAAAAAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAQgFABAAAFg");
	this.shape_6.setTransform(-11.6,-10.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFB300").s().p("Ai2CCQgVAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAVAAIFtAAQAVAAAQAQQAPAPAAAWIAACaQAAAVgPAPQgQAQgVAAg");
	this.shape_7.setTransform(0,-10.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4DD0E1").s().p("Ai2CCQgVAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAVAAIFtAAQAVAAAQAQQAPAPAAAWIAACaQAAAVgPAPQgQAQgVAAg");
	this.shape_8.setTransform(0,-10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_8},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.5,-23.1,47,46.2);


(lib.btnm6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(2,11.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(1.1,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgrIAKgbIANgGIAJAAQAKAAADAHIgPAUQgLAZASAag");
	this.shape_1.setTransform(-1.6,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(5.4,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgCAyQgRgBgLgLQgKgKAAgQQAAgKAEgIQAEgHAJgIIARgQIAHgMIAQAAQgDAGgFAFQgEAFgHAFQALAAAIADQAYAIAAAbQAAASgNALQgMALgRAAIgBAAgAgVgKQgHAHAAAJQAAAKAGAGQAJAJANAAQAKAAAIgGQAKgIAAgMQAAgFgCgFQgDgHgHgEQgHgDgJAAQgNAAgIAJg");
	this.shape_3.setTransform(16.1,-10.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgFAAQgEAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgGALQgIANgKAAQgGAAgFgDg");
	this.shape_4.setTransform(9.9,-9.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgiAcQgLgJAAgQQAAgGADgFQADgGAEgDQgEgCAAgGIAAgEQAEgJARAAQANAAAFAHQAFgHAPAAQAHAAAFADQAGADABAGQABAFgDAEQAEADACAFQADAGAAAGQAAAQgMAKQgNAKgVAAQgWAAgMgLgAgfgIQgDAEAAAGQAAAKAMAFQAJADANAAQANAAAJgDQALgFABgKQAAgGgEgEQgDgEgGAAIgqAAQgGAAgEAEgAAKgbQgEACgBAFIAWAAIAAgDQAAgDgDgBQgDgCgDAAIgBAAQgEAAgDACgAgYgdQABABAAAAQABAAAAABQAAABAAAAQABABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABIAQAAQAAgHgKgCIgEgBIgEABg");
	this.shape_5.setTransform(3.3,-9.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgeAqQgMgMAAgSQAAgMAFgJQAGgKAKAAQAKAAACAIQADgJAKAAQAIAAAFAHQAGAGAAAHQAAALgHAHQgHAGgJAAQgIAAgGgEQgHgFAAgIIACgLQABgFgFAAQgEAAgCAGQgCAEAAAFQAAANAJAIQAKAHAMAAQANAAAKgJQAJgJAAgNQAAgKgFgHQgFgJgJAAIg0AAIAAgIQANABAAgCIgDgBQgCgCAAgDQAAgKALgDQAGgCAOAAQALAAAHACQAKADAHAIQAFAFAAAJIgJAAQgCgIgJgFQgIgEgLAAQgIAAgEACQgFABAAAFIABADIAEABIAXAAQAIAAAGACIAFADQAMAKAAAXQAAAUgMANQgMAOgTAAQgSAAgMgMgAgKACQAAAEADADQADADAEAAQAGAAADgDQAEgDAAgFQgEAFgGAAQgIABgFgIIAAADgAAAgKQAAAAAAABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAABQAAAAABAAQAAABAAAAQABAAAAAAQABAAAAAAIADgBQABgBAAAAQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAAAAAQgBAAAAABgAAfgaIAAAAgAAagdQgGgCgIAAIgXAAIgEgBIgBgDQAAgFAFgBQAEgCAIAAQALAAAIAEQAJAFACAIIgFgDg");
	this.shape_6.setTransform(-6,-11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AANAvIAAgRIgUABQgRgBgLgHQgMgKAAgOQAAgIAFgHQAEgGAGgDQgDAAgCgDIgBgFQAAgFAEgDQAIgGAOABQAVAAAFALQABgFAFgDQAEgEAGABIAJAAIAAAIIgBAAQgBAAAAAAQgBAAAAABQgBAAAAABQAAAAgBABIADADIAGAGQAEAEAAAFQABAJgJAGQgHACgJAAIAAATIAIgCIAGgCIAAAJIgGACIgIACIAAATgAgVgOQgFAHAAAHQAAARATAEIAJAAIALAAIAAgoIgVAAQgIAAgFAFgAAXgXIAAAPIACAAQADAAADgCQADgCAAgEQAAgEgEgGQgEgFAAgDIAAgBQgDABAAALgAgSglQACADAAACQAAABAAAAQAAABgBAAQAAABAAAAQgBABAAAAIACAAIACAAIATAAQgBgGgGgCQgFgCgFAAIgGABg");
	this.shape_7.setTransform(-15.3,-8.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFB300").s().p("AkaCCQgVAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAVAAII0AAQAXAAAOAQQAQAPAAAWIAACaQAAAVgQAPQgPAQgWAAg");
	this.shape_8.setTransform(0,-10.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4DD0E1").s().p("AkaCCQgVAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAVAAII0AAQAXAAAOAQQAQAPAAAWIAACaQAAAVgQAPQgPAQgWAAg");
	this.shape_9.setTransform(0,-10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_9},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.5,-23.1,67,46.2);


(lib.btnm5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(32,-1.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(31.1,-2.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgrIAKgbIANgGIAJAAQAKAAADAHIgPAUQgLAZASAag");
	this.shape_1.setTransform(28.4,-7.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(35.3,-1.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgVAuQgKgFgFgIQgIgMAAgRQAAgLAGgKQAHgKAJABQAJAAACAHQAFgIAJAAQAIgBAGAHQAGAGABAJQABAKgIAIQgIAHgKABQgKAAgGgHQgHgGABgIIABgGIADgGQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBAAgBABQgEAAgDAGQgCAFAAAGQgBANAKAJQAKAJAOgBQANAAAKgKQAKgKgBgOQAAgRgIgKQgJgKgQgBQgVABgJAPIgKAAQAFgMALgGQAKgHAOAAQAUAAALAMQAOAOABAWQABAWgNAPQgMAPgVAAQgMABgJgFgAgNgEQABAEADADQAEADAFAAQAFAAAFgEQAFgDAAgFIgBgCQgBADgDACQgEADgEAAQgKgBgDgIIgCAFgAgCgOIACAEIADACQABAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBAAAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAAAgBAAQgFgBAAAGg");
	this.shape_3.setTransform(8.1,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgQAnQgQAAgJgMQgHgKAAgQQAAgZASgLQAGgDAJAAIAFAAIAAAJQAKgKAPABQAQAAAJALQAJAKAAARQAAAQgJALQgKAMgQAAIgFAAIAAgIQgGAFgEABQgEACgHAAIgEAAgAAGgMQANADAAAQQAAANgJAJQAJACAGgEQAKgHgBgQQAAgKgGgHQgHgIgJAAQgRgBgDAMQAEgDAFAAIAFABgAgfgVQgFAHAAAJQAAAKAFAHQAGAIALAAQAIAAAGgDQAGgEAAgHQABgEgDgCQgCgDgDAAQgDAAgCACQgCABAAAEIABADIgKAAQgCgJABgIQABgJAHgKIgGAAQgJAAgGAIg");
	this.shape_4.setTransform(-1.7,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgmAbIAAgPIBFAAIAAgYIAEgGIAFgIIAAA1g");
	this.shape_5.setTransform(-11.5,4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgfApQgPgKABgQQABgMAKgHQgHgBAAgIQAAgHAIgEQAGgCAJAAQAMAAAFAIQADgHALgBIAHAAIAEgEIAEgEQAEgIABgJIANAAIgDAJIgEAJIgIAJQAGADABAIQABAEgCADIgHAAQADABAEAFQAHAFAAALQAAAQgOAKQgNAKgRgBQgSAAgNgKgAgdADQgEAFAAAFQABAKAKAFQAKAFAMgBQAMABAJgFQALgFABgKQAAgGgEgEQgEgDgJAAIgjAAQgGAAgEADgAALgQQgEAAgCACQgDADAAADIAPAAIAJABQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgDgFgJAAIgDABgAgYgQQABAAAAABQABAAAAAAQABABAAAAQAAABAAAAQABABAAABQAAAAAAABQAAAAgBABQAAAAAAABIAQAAQgBgGgGgCIgGgBIgGABg");
	this.shape_6.setTransform(-11.4,-0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgNAiQgFAFgNAAQgHAAgFgEQgFgFAAgHQAAgIADgFIgGAAIAAgGIAFgFIAHgIIgCAAQgEAAgCgDQgDgDAAgEQAAgHAFgGQAGgFAHgBQANgBAGAJQAMgJAQABQAPABALAMQAKALAAAOQABAagTALQgEACgIAAIgIAAIAAgGQgFAGgIAAIAAAAQgIAAgFgFgAgGgXQAMABAHAJQAJAKAAAMQAAANgGAIIAEAAQAGAAAFgFQAGgFABgHIAAgFQAAgPgKgJQgIgIgPAAIgLABgAgJAAQAAAMABADQACAGAFAAQAEgBADgEQADgFAAgGQAAgGgEgGQgEgGgFgBIgFAAQgKAAgGAEQgHAGgHAHIAGAAQgDAFAAAGQABADACACQADACAEAAQAGAAACgGIABgPgAgjgaQADABABADQABAEgCADIAFgFIAFgEQgBgEgGAAIgBgBQgEAAgBADg");
	this.shape_7.setTransform(-21.5,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFB300").s().p("AkZCCQgWAAgPgQQgQgPAAgWIAAiaQAAgVAQgPQAPgQAWAAIIzAAQAWAAAQAQQAPAPAAAVIAACaQAAAWgPAPQgQAQgWAAg");
	this.shape_8.setTransform(-6.9,0.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4DD0E1").s().p("AkZCCQgWAAgPgQQgQgPAAgWIAAiaQAAgVAQgPQAPgQAWAAIIzAAQAWAAAQAQQAPAPAAAVIAACaQAAAWgPAPQgQAQgWAAg");
	this.shape_9.setTransform(-6.9,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_9},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.4,-13.2,81,26.4);


(lib.btnm4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(1,11.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(0.1,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHIgPAVQgLAYASAZg");
	this.shape_1.setTransform(-2.6,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(4.4,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAAAfQgGAIgKAAQgPAAgIgLQgHgKABgRQAAgTANgLQAIgIAMgBQAEAAAEABIAAAJQgFgCgIACQgIACgEAIQgFAHAAAJQAAAXAQAAQAGAAAEgEQAEgDAAgGIAAgIIAIAAIAAAIQAAAGAFADQADAEAGAAQAIAAAEgFQAFgFAAgHQAAgGgFgFQgFgFgHAAIgWAAQAAgNAFgHQAGgGALAAQAVAAAAARIgBAEIgHAAQAHACAEAHQAFAHAAAHQAAAPgIAJQgHAJgNAAQgNAAgGgIgAAGgUIAOAAIAFABIABgDQAAgHgJAAQgKAAgBAJg");
	this.shape_3.setTransform(33.1,-9.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAZASQAHgEAAgGIgBgCQgBgFgKgCQgIgCgMAAQgKAAgJADQgKABgBAFIgBACQAAAGAGAEIgIAAQgIgDAAgKQAAgEACgBQAEgKAMgEQAJgDAOAAQAiAAAGARQACABAAAEQAAAKgIADg");
	this.shape_4.setTransform(23.4,-14.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAIAmIAAgGQgGAEgEACQgGABgJAAQgNAAgJgJQgIgJgBgMQAAgLADgIIAJAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAEAAAEgCQAHgFACgFIAAgDQAAgHgEgCQgEgBgDABQAGACAAAHQAAAEgDADQgDACgEAAIgEgBQgHgCAAgIIABgGQAEgKAPAAQAFAAAEACQAFADABAEQAHgKARABQAQAAAJAQQAGALgBAMQAAAagTAJQgGADgHAAIgJgBgAAJgJQAJAHAAALQAAAMgIAIIAHABQAGAAAEgDQAKgHAAgPQgBgJgGgIQgHgIgJAAQgNgBgEAIIABAAQAGAAAFAEgAgmAGQAAAGADAEQAFAGALAAIAHgBQAHAAAFgGQAEgFAAgGQAAgDgCgCQgCgEgGgBQgDAAgFADIgIAEQgEACgFAAQgEAAgCgCIgBAFg");
	this.shape_5.setTransform(23.1,-9.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AACAwQgHgEAAgHQAAgGAEgDQgQABgMgJQgLgKABgNQABgQAOgIQgCAAgCgDQgCgCAAgDIABgEQADgGAHgDQAGgBAKAAQARAAAIAJQAGAHABALIgcAAQgIAAgGAFQgGAGAAAHQgBAJAIAHQAHAGAKABQAPAAAMgGIAAAKIgLADQgKADAAAEQAAADACACQADABAEAAQAEAAAEgCQADgCAAgFIgBgDIAJAAIABAEQAAAIgHAGQgGAFgKAAIgDAAQgHAAgFgCgAgLgnIABABQABAAAAABQAAAAAAAAQABABAAAAQAAAAAAABQAAADgDABIAFAAIAWAAQgDgGgHgCIgJgBIgIABg");
	this.shape_6.setTransform(14.5,-8.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgZAcQgKgLgBgRQABgQAKgLQALgLARAAQASAAAHAHQAHAFAAAIIgJAAQAAgFgIgDQgGgDgIAAQgKAAgJAHQgJAIABALQgBAKAIAGQAIAHAKAAQAJAAAGgEQAIgGAAgIQAAgKgKgDQADADAAAIQAAAEgEAEQgFADgGAAQgGAAgFgEQgFgEABgHQAAgHAFgFQAHgEAHAAQAMAAAJAHQAJAHgBALQAAAPgKAJQgKAKgOAAQgPAAgMgLgAgBgIQAAAAgBABQAAAAgBABQAAAAAAABQAAAAAAABQAAABAAAAQAAABAAABQABAAAAABQABAAAAAAQAAAAABABQAAAAAAAAQAAAAABABQABAAABAAQAAAAABAAQAAgBABAAQAAAAABAAQABgBAAAAQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABAAAAQAAAAgBABg");
	this.shape_7.setTransform(6.6,-9.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgJATIAAgQIgIAAIAAgHIAIAAIAAgKIgIAAIAAgHIAUAAQAGAAAEAEQAFADAAAGQAAAGgGACQgFADgIAAIAAASgAgBgOIAAAKQAHABAAgFQAAgGgGAAIgBAAg");
	this.shape_8.setTransform(0.9,-15.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AggAiQgNgPABgUQAAgVANgNQANgNAVAAQASAAAMALQAMAMAAASQAAAOgIAKQgIAMgOAAQgOAAgHgIQgJgJACgMQABgLAIgIIgKAAIAAgJIAjAAIAAAJIgHAAQgGABgFAEQgEAEAAAGQAAAGAEAEQAFAEAHAAQAJAAAFgHQAFgFgBgJQAAgMgGgHQgIgJgRAAQgOAAgIAMQgHAKAAAQQAAAOAHALQAHALANACIAHABQAQAAAJgNIALAAQgLAYgfABQgUAAgNgQg");
	this.shape_9.setTransform(-2.1,-8.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgQAgQgHgGAAgLQAAgIADgHQACgCAFgHQAGgHAPgMIgdAAIAAgJIAsAAIAAAIIgKAJIgJAIIgIAKQgEAFAAAHQABAEADADQADACADAAQAIAAADgHIAAgEQAAgDgCgCIAJAAQAFAGgBAIQAAAJgHAGQgIAGgJAAQgKAAgGgGg");
	this.shape_10.setTransform(-9.4,-9.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AggApQgNgJABgRQABgPAMgEQgHgCAAgHQAAgIAJgEQAGgDAJABQALAAAFAJQAHAIAAAKIgbAAQgGAAgEADQgEAEAAAGQABANASAEIANABQAPAAAKgKQAIgJABgOQAAgQgJgKQgJgMgSAAQgUgBgKAPIgKAAQAFgLALgGQALgGAOAAQAUAAANAOQANAPAAAUQAAAVgMAOQgNAPgUAAQgTAAgNgJgAgVgRIADAEQABADgCADIARAAQgBgFgDgDQgEgDgFAAIgGABg");
	this.shape_11.setTransform(-16.7,-10.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgcAsQgNgHgBgOQgBgPAMgGQgDgBgCgBQgCgDAAgEIAAgDQAFgLAUABQALABAGAJQAFAHAAALIgaAAQgNAAAAAJQAAAIAKAEQAIADAMAAQANAAAIgHQAJgHABgLQABgOgIgJQgGgHgKAAIguAAIAAgHQAKAAABgCQAAAAAAAAQgBgBAAAAQAAAAgBAAQAAgBgBAAQAAAAgBgBQgBAAAAAAQAAgBgBgBQAAAAAAgBQgBgMAiAAQAPAAALAGQANAHAAALIgKAAQABgHgKgEQgJgDgLAAQgQAAgBAFQgBADAHAAIAVAAQAKAAAJAHQAOALAAAZQAAARgOAMQgNALgRAAQgQAAgLgHgAgVgKQADABAAADQAAAEgCACIASAAQgBgFgDgDQgEgDgHAAIgEABg");
	this.shape_12.setTransform(-25.8,-10.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgYAcQgLgLAAgRQAAgQALgLQALgLAQAAQARAAAJAHQAFAFABAIIgJAAQgBgFgGgDQgHgDgJAAQgKAAgIAHQgIAIAAALQAAAKAHAGQAIAHAKAAQAJAAAHgEQAGgGAAgIQAAgKgKgDQAFADAAAIQgBAEgFAEQgEADgGAAQgGAAgFgEQgEgEgBgHQAAgHAHgFQAFgEAIAAQAMAAAJAHQAJAHAAALQAAAPgLAJQgKAKgOAAQgPAAgLgLgAgBgIQAAAAgBABQAAAAAAABQgBAAAAABQAAAAAAABQAAABAAAAQAAABABABQAAAAAAABQABAAAAAAQAAAAABABQAAAAAAAAQAAAAABABQABAAABAAQAAAAABAAQAAgBABAAQAAAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABAAAAQAAAAgBABg");
	this.shape_13.setTransform(-34.1,-9.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFB300").s().p("AnhCCQgWAAgQgPQgPgQAAgVIAAiaQAAgWAPgQQAQgPAWAAIPEAAQAVAAAQAPQAPAQAAAWIAACaQAAAVgPAQQgQAPgVAAg");
	this.shape_14.setTransform(0,-10.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#4DD0E1").s().p("AnhCCQgWAAgQgPQgPgQAAgVIAAiaQAAgWAPgQQAQgPAWAAIPEAAQAVAAAQAPQAPAQAAAWIAACaQAAAVgPAQQgQAPgVAAg");
	this.shape_15.setTransform(0,-10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_15},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.5,-23.1,107,46.2);


(lib.btnm3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(1,11.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(0.1,11.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgrIAKgbIANgGIAJgBQAKABADAHIgPAUQgLAZASAZg");
	this.shape_1.setTransform(-2.6,6.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(4.4,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAAAfQgGAIgKAAQgPAAgIgLQgHgKABgRQAAgTANgLQAIgIAMgBQAEAAAEABIAAAJQgFgCgIACQgIACgEAIQgFAHAAAJQAAAXAQAAQAGAAAEgEQAEgDAAgGIAAgIIAIAAIAAAIQAAAGAFADQADAEAGAAQAIAAAEgFQAFgFAAgHQAAgGgFgFQgFgFgHAAIgWAAQAAgNAFgHQAGgGALAAQAVAAAAARIgBAEIgHAAQAHACAEAHQAFAHAAAHQAAAPgIAJQgHAJgNAAQgNAAgGgIgAAGgUIAOAAIAFABIABgDQAAgHgJAAQgKAAgBAJg");
	this.shape_3.setTransform(23.2,-9.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAZASQAHgEAAgGIgBgCQgBgFgKgCQgIgCgMAAQgKAAgJADQgKABgBAFIgBACQAAAGAGAEIgIAAQgIgDAAgKQAAgEACgBQAEgKAMgEQAJgDAOAAQAiAAAGARQACABAAAEQAAAKgIADg");
	this.shape_4.setTransform(13.5,-14.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgdAjQgOgLAAgRQAAgQAMgJQgBgJADgFQAHgOAOACQAHAAAEAFQAFgGAKABQAJAAAFAGQAFAHAAAPIglAAIgQABQAAAKAHAGQAIAHAJgBQAHAAAGgFQAGgDABgHIAJAAQgCALgJAHQgJAHgMAAQgWAAgJgQIgBAGQAAALAJAIQAJAHALAAQASAAAJgKQAFgHAAgIIAJAAQABAKgHAKQgMARgZAAQgRAAgMgKgAgOggQgCADAAAFIAHgBIAIAAQAAgKgHAAQgEAAgCADgAAIgZIAPAAQAAgKgHAAQgIAAAAAKg");
	this.shape_5.setTransform(13.9,-9.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgfArQgNgIAAgOQAAgOAJgFIgIAAIAAgIIALAAQgDgBAAgEQAAgFAEgDQAHgFAMABQANABAFAJQAEAFAAAKIgVAAQgTAAgBALQgBAKAKAFQAKAEAMAAQAMgBAIgHQAJgIADgMIAAgGQAAgKgGgKQgGgJgIgEIgFAFIgFADQgFADgHAAQgJAAgEgFQgCgDABgHQABgCgFAAIgDAAIAAgIIAPgBQAKAAAHACQASADAMANQAOAOAAAWQAAAUgNANQgNAOgUAAQgTAAgLgIgAgSgNQAAAAABAAQAAAAAAABQABAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQgBAAAAABQAAAAgBABIARAAQgBgFgHgCIgEgBIgFABgAgOgkQAAADAGAAQAIAAACgFQgGgDgLAAIABAFg");
	this.shape_6.setTransform(4.7,-10.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgCAyQgRgBgLgLQgKgKAAgQQAAgKAEgIQAEgHAJgIIARgQIAHgMIAQAAQgDAGgFAFQgEAFgHAFQALAAAIADQAYAIAAAbQAAASgNALQgMALgRAAIgBAAgAgVgKQgHAHAAAJQAAAKAGAGQAJAJANAAQAKAAAIgGQAKgIAAgMQAAgFgCgFQgDgHgHgEQgHgDgJAAQgNAAgIAJg");
	this.shape_7.setTransform(-4.2,-10.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AggApQgNgJABgRQABgPAMgEQgHgCAAgHQAAgIAJgEQAGgDAJABQALAAAFAJQAHAIAAAKIgbAAQgGAAgEADQgEAEAAAGQABANASAEIANABQAPAAAKgKQAIgJABgOQAAgQgJgKQgJgMgSAAQgUgBgKAPIgKAAQAFgLALgGQALgGAOAAQAUAAANAOQANAPAAAUQAAAVgMAOQgNAPgUAAQgTAAgNgJgAgVgRIADAEQABADgCADIARAAQgBgFgDgDQgEgDgFAAIgGABg");
	this.shape_8.setTransform(-13.1,-10.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgmAbIAAgPIBEAAIAAgYIAFgGIAEgIIAAA1g");
	this.shape_9.setTransform(-22.5,-6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAIAmIAAgGQgGAEgEACQgGABgJAAQgNAAgJgJQgIgJgBgMQAAgLADgIIAJAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAEAAAEgCQAHgFACgFIAAgDQAAgHgEgCQgEgBgDABQAGACAAAHQAAAEgDADQgDACgEAAIgEgBQgHgCAAgIIABgGQAEgKAPAAQAFAAAEACQAFADABAEQAHgKARABQAQAAAJAQQAGALgBAMQAAAagTAJQgGADgHAAIgJgBgAAJgJQAJAHAAALQAAAMgIAIIAHABQAGAAAEgDQAKgHAAgPQgBgJgGgIQgHgIgJAAQgNgBgEAIIABAAQAGAAAFAEgAgmAGQAAAGADAEQAFAGALAAIAHgBQAHAAAFgGQAEgFAAgGQAAgDgCgCQgCgEgGgBQgDAAgFADIgIAEQgEACgFAAQgEAAgCgCIgBAFg");
	this.shape_10.setTransform(-23,-9.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFB300").s().p("AkaCCQgVAAgQgQQgPgPAAgWIAAiaQAAgVAPgPQAQgQAVAAII0AAQAXAAAOAQQAQAPAAAVIAACaQAAAWgQAPQgPAQgWAAg");
	this.shape_11.setTransform(0,-10.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#4DD0E1").s().p("AkaCCQgVAAgQgQQgPgPAAgWIAAiaQAAgVAPgPQAQgQAVAAII0AAQAXAAAOAQQAQAPAAAVIAACaQAAAWgQAPQgPAQgWAAg");
	this.shape_12.setTransform(0,-10.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_12},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.5,-23.1,67,46.2);


(lib.btnm2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(17.5,9.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(16.6,9.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgrIAKgbIANgGIAJgBQAKABADAHIgPAUQgLAZASAag");
	this.shape_1.setTransform(13.9,4.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(20.9,9.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AggAiQgNgPABgUQAAgVANgNQANgOAVAAQASAAAMAMQAMAMAAASQAAAOgIAKQgIALgOABQgOAAgHgIQgJgKACgLQABgLAIgIIgKAAIAAgJIAjAAIAAAJIgHAAQgGAAgFAFQgEAEAAAGQAAAFAEAFQAFAEAHAAQAJAAAFgHQAFgFgBgKQAAgLgGgHQgIgKgRAAQgOAAgIANQgHALAAAPQAAAOAHAKQAHAMANADIAHAAQAQAAAJgNIALAAQgLAZgfgBQgUAAgNgPg");
	this.shape_3.setTransform(9.2,-6.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgJASIAAgPIgIAAIAAgHIAIAAIAAgKIgIAAIAAgGIAUAAQAGAAAEADQAFADAAAGQAAAGgGACQgFAEgIgBIAAASgAgBgOIAAAKQAHABAAgFQAAgGgGAAIgBAAg");
	this.shape_4.setTransform(2.7,-13.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AggAiQgNgPABgUQAAgVANgNQANgOAVAAQASAAAMAMQAMAMAAASQAAAOgIAKQgIALgOABQgOAAgHgIQgJgKACgLQABgLAIgIIgKAAIAAgJIAjAAIAAAJIgHAAQgGAAgFAFQgEAEAAAGQAAAFAEAFQAFAEAHAAQAJAAAFgHQAFgFgBgKQAAgLgGgHQgIgKgRAAQgOAAgIANQgHALAAAPQAAAOAHAKQAHAMANADIAHAAQAQAAAJgNIALAAQgLAZgfgBQgUAAgNgPg");
	this.shape_5.setTransform(-0.3,-6.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgEAAQgFAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgHALQgHANgKAAQgGAAgFgDg");
	this.shape_6.setTransform(-7,-7.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgQAnQgQAAgJgMQgHgKAAgQQAAgZASgLQAGgDAJAAIAFAAIAAAJQAKgKAPABQAQAAAJALQAJAKAAARQAAAQgJALQgKAMgQAAIgFAAIAAgIQgGAFgEABQgEACgHAAIgEAAgAAGgMQANADAAAQQAAANgJAJQAJACAGgEQAKgHgBgQQAAgKgGgHQgHgIgJAAQgRgBgDAMQAEgDAFAAIAFABgAgfgVQgFAHAAAJQAAAKAFAHQAGAIALAAQAIAAAGgDQAGgEAAgHQABgEgDgCQgCgDgDAAQgDAAgCACQgCABAAAEIABADIgKAAQgCgJABgIQABgJAHgKIgGAAQgJAAgGAIg");
	this.shape_7.setTransform(-13.8,-7.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFB300").s().p("Ai1CCQgWAAgPgPQgQgQAAgWIAAiaQAAgVAQgQQAPgPAWAAIFrAAQAWAAAQAPQAPAQAAAVIAACaQAAAWgPAQQgQAPgWAAg");
	this.shape_8.setTransform(-2.5,-8.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4DD0E1").s().p("Ai1CCQgWAAgPgPQgQgQAAgWIAAiaQAAgVAQgQQAPgPAWAAIFrAAQAWAAAQAPQAPAQAAAVIAACaQAAAWgPAQQgQAPgWAAg");
	this.shape_9.setTransform(-2.5,-8.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_9},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-26,-21.1,52.1,42.2);


(lib.btnm1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(0,11.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuQgTAYgRAOQgTgVgRgdgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(-0.9,11);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHIgPAVQgLAYASAZg");
	this.shape_1.setTransform(-3.6,6.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIABgOQARgVARgcQAhg2ACggQACgggcgWIgdgPQA9gCATAsQAKAWgDAWQgDAzgkAwQgQAZgSAOg");
	this.shape_2.setTransform(3.4,11.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAAAfQgGAIgKAAQgPAAgIgLQgHgKABgRQAAgTANgLQAIgIAMgBQAEAAAEABIAAAJQgFgCgIACQgIACgEAIQgFAHAAAJQAAAXAQAAQAGAAAEgEQAEgDAAgGIAAgIIAIAAIAAAIQAAAGAFADQADAEAGAAQAIAAAEgFQAFgFAAgHQAAgGgFgFQgFgFgHAAIgWAAQAAgNAFgHQAGgGALAAQAVAAAAARIgBAEIgHAAQAHACAEAHQAFAHAAAHQAAAPgIAJQgHAJgNAAQgNAAgGgIgAAGgUIAOAAIAFABIABgDQAAgHgJAAQgKAAgBAJg");
	this.shape_3.setTransform(29.9,-9.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgCAyQgRgBgLgLQgKgKAAgQQAAgKAEgIQAEgHAJgIIARgQIAHgMIAQAAQgDAGgFAFQgEAFgHAFQALAAAIADQAYAIAAAbQAAASgNALQgMALgRAAIgBAAgAgVgKQgHAHAAAJQAAAKAGAGQAJAJANAAQAKAAAIgGQAKgIAAgMQAAgFgCgFQgDgHgHgEQgHgDgJAAQgNAAgIAJg");
	this.shape_4.setTransform(20.9,-10.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgmAbIAAgPIBFAAIAAgYIAEgGIAFgIIAAA1g");
	this.shape_5.setTransform(11.9,-5.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgiAcQgLgJAAgQQAAgGADgFQADgGAEgDQgEgCAAgGIAAgEQAEgJARAAQANAAAFAHQAFgHAPAAQAHAAAFADQAGADABAGQABAFgDAEQAEADACAFQADAGAAAGQAAAQgMAKQgNAKgVAAQgWAAgMgLgAgfgIQgDAEAAAGQAAAKAMAFQAJADANAAQANAAAJgDQALgFABgKQAAgGgEgEQgDgEgGAAIgqAAQgGAAgEAEgAAKgbQgEACgBAFIAWAAIAAgDQAAgDgDgBQgDgCgDAAIgBAAQgEAAgDACgAgYgdQABABAAAAQABAAAAABQAAAAAAABQABABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABIAQAAQAAgHgKgCIgEgBIgEABg");
	this.shape_6.setTransform(11.7,-9.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgBArQgGAIgLABQgOAAgHgMQgHgKABgRQABgTAKgKQAKgIASAAIAFAAIAAAJIgNAAQgGABgFAEQgIAGgBAMQAAALADAGQAEAIAJgBQAGABAEgFQADgEAAgFIAAgMIAIAAIAAAMQAAANAOAAQAJAAAFgLQAEgJAAgQQgBgOgHgKQgLgNgRgBQgLAAgIAFQgIADgEAIIgLAAQAEgLALgHQALgGAQgBQALAAAKAGQAHADAFAFQAOAPAAAaQABAVgIAMQgIAPgPAAQgMgBgFgIg");
	this.shape_7.setTransform(2,-10.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgFAAQgEAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgGALQgIANgKAAQgGAAgFgDg");
	this.shape_8.setTransform(-4.7,-9.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgCAyQgRgBgLgLQgKgKAAgQQAAgKAEgIQAEgHAJgIIARgQIAHgMIAQAAQgDAGgFAFQgEAFgHAFQALAAAIADQAYAIAAAbQAAASgNALQgMALgRAAIgBAAgAgVgKQgHAHAAAJQAAAKAGAGQAJAJANAAQAKAAAIgGQAKgIAAgMQAAgFgCgFQgDgHgHgEQgHgDgJAAQgNAAgIAJg");
	this.shape_9.setTransform(-10.6,-10.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgmAbIAAgPIBFAAIAAgYIAEgGIAFgIIAAA1g");
	this.shape_10.setTransform(-19.6,-5.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAIAmIAAgGQgGAEgEACQgGABgJAAQgNAAgJgJQgIgJgBgMQAAgLADgIIAJAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAEAAAEgCQAHgFACgFIAAgDQAAgHgEgCQgEgBgDABQAGACAAAHQAAAEgDADQgDACgEAAIgEgBQgHgCAAgIIABgGQAEgKAPAAQAFAAAEACQAFADABAEQAHgKARABQAQAAAJAQQAGALgBAMQAAAagTAJQgGADgHAAIgJgBgAAJgJQAJAHAAALQAAAMgIAIIAHABQAGAAAEgDQAKgHAAgPQgBgJgGgIQgHgIgJAAQgNgBgEAIIABAAQAGAAAFAEgAgmAGQAAAGADAEQAFAGALAAIAHgBQAHAAAFgGQAEgFAAgGQAAgDgCgCQgCgEgGgBQgDAAgFADIgIAEQgEACgFAAQgEAAgCgCIgBAFg");
	this.shape_11.setTransform(-20.1,-9.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AANAwIAAgSIgUABQgRgBgLgIQgMgIAAgQQAAgHAFgGQAEgHAGgCQgDgBgCgDIgBgEQAAgFAEgEQAIgGAOAAQAVABAFAMQABgGAFgDQAEgEAGAAIAJAAIAAAJIgBAAQgBAAAAAAQgBAAAAABQgBAAAAABQAAAAgBABIADADIAGAGQAEAEAAAFQABAKgJAEQgHAEgJgBIAAAUIAIgCIAGgDIAAAJIgGADIgIACIAAATgAgVgOQgFAGAAAIQAAAQATAFIAJAAIALAAIAAgpIgVAAQgIABgFAFgAAXgXIAAAPIACAAQADAAADgCQADgCAAgEQAAgEgEgFQgEgGAAgCIAAgCQgDABAAALgAgSglQACACAAADQAAABAAAAQAAABgBAAQAAABAAAAQgBABAAAAIACgBIACAAIATAAQgBgFgGgCQgFgCgFAAIgGABg");
	this.shape_12.setTransform(-30,-8.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFB300").s().p("AnhCCQgWAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAWAAIPEAAQAVAAAQAQQAPAPAAAWIAACaQAAAVgPAPQgQAQgVAAg");
	this.shape_13.setTransform(0,-9.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#4DD0E1").s().p("AnhCCQgWAAgQgQQgPgPAAgVIAAiaQAAgWAPgPQAQgQAWAAIPEAAQAVAAAQAQQAPAPAAAWIAACaQAAAVgPAPQgQAQgVAAg");
	this.shape_14.setTransform(0,-9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_14},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.5,-22.6,107,45.2);


(lib.btnb3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(0,11.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuIgkAmgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(-0.9,12);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHQgKAIgFANQgLAYASAag");
	this.shape_1.setTransform(-3.6,7.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIACgOIAhgxQAhg2ACggQACgggcgWQgOgLgPgEQA9gCAUAsQAGAOABAQIAAAOQgDAzgkAwIgiAng");
	this.shape_2.setTransform(3.3,12.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAAAqQgFAHgMAAQgMAAgIgJQgHgJAAgMQABgOAJgFIgMAAIAAgKIAQAAQgDgBAAgFIABgEQAFgIANAAIAGAAQAKABAGAIQAGAIAAALIgcAAQgIAAgFADQgFAFAAAHQAAAGAEAFQAFAEAHAAQAFAAAEgDQAEgEAAgFIAAgKIAIAAIAAAKQAAAFAEAEQAEADAFAAQAQAAABgbQABgQgKgNQgLgNgPgBQgXAAgJAPIgKAAQALgYAfABQARAAANALQAJAIAFAMQAEAMgBAMQgBASgGALQgIANgNAAQgMAAgHgHgAgQgTIACAFIgCAEIASAAQgCgGgEgCIgIgBIgEAAg");
	this.shape_3.setTransform(34.8,-11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgFAAQgEAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgGALQgIANgKAAQgGAAgFgDg");
	this.shape_4.setTransform(28.2,-10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgQAnQgQAAgJgMQgHgKAAgQQAAgZASgLQAGgDAJAAIAFAAIAAAJQAKgKAPABQAQAAAJALQAJAKAAARQAAAQgJALQgKAMgQAAIgFAAIAAgIQgGAFgEABQgEACgHAAIgEAAgAAGgMQANADAAAQQAAANgJAJQAJACAGgEQAKgHgBgQQAAgKgGgHQgHgIgJAAQgRgBgDAMQAEgDAFAAIAFABgAgfgVQgFAHAAAJQAAAKAFAHQAGAIALAAQAIAAAGgDQAGgEAAgHQABgEgDgCQgCgDgDAAQgDAAgCACQgCABAAAEIABADIgKAAQgCgJABgIQABgJAHgKIgGAAQgJAAgGAIg");
	this.shape_5.setTransform(21.4,-10.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgZAcQgLgLABgRQgBgQALgLQAMgLAPAAQASAAAJAHQAFAFABAIIgJAAQgBgFgGgDQgHgDgJAAQgKAAgIAHQgJAIAAALQABAKAHAGQAIAHAKAAQAJAAAHgEQAGgGABgIQAAgKgLgDQAFADAAAIQAAAEgGAEQgEADgGAAQgGAAgFgEQgFgEAAgHQABgHAFgFQAGgEAIAAQAMAAAJAHQAJAHAAALQAAAPgLAJQgKAKgOAAQgQAAgLgLgAgBgIQgBAAAAABQAAAAAAABQgBAAAAABQAAAAAAABQAAABAAAAQAAABABABQAAAAAAABQAAAAABAAQAAAAABABQAAAAAAAAQAAAAABABQABAAAAAAQABAAABAAQAAgBABAAQAAAAABAAQAAgBABAAQAAAAABAAQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABAAAAQgBAAAAABg");
	this.shape_6.setTransform(12.5,-10.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgJATIAAgQIgIAAIAAgHIAIAAIAAgKIgIAAIAAgHIAUAAQAGAAAEAEQAFADAAAGQAAAGgGACQgFADgIAAIAAASgAgBgOIAAAKQAHABAAgFQAAgGgGAAIgBAAg");
	this.shape_7.setTransform(6.8,-15.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAIAmIAAgGQgGAEgEACQgGABgJAAQgNAAgJgJQgIgJgBgMQAAgLADgIIAJAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAEAAAEgCQAHgFACgFIAAgDQAAgHgEgCQgEgBgDABQAGACAAAHQAAAEgDADQgDACgEAAIgEgBQgHgCAAgIIABgGQAEgKAPAAQAFAAAEACQAFADABAEQAHgKARABQAQAAAJAQQAGALgBAMQAAAagTAJQgGADgHAAIgJgBgAAJgJQAJAHAAALQAAAMgIAIIAHABQAGAAAEgDQAKgHAAgPQgBgJgGgIQgHgIgJAAQgNgBgEAIIABAAQAGAAAFAEgAgmAGQAAAGADAEQAFAGALAAIAHgBQAHAAAFgGQAEgFAAgGQAAgDgCgCQgCgEgGgBQgDAAgFADIgIAEQgEACgFAAQgEAAgCgCIgBAFg");
	this.shape_8.setTransform(3.4,-10.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AggAiQgNgPABgUQAAgVANgNQANgNAVAAQASAAAMALQAMAMAAASQAAAOgIAKQgIAMgOAAQgOAAgHgIQgJgJACgMQABgLAIgIIgKAAIAAgJIAjAAIAAAJIgHAAQgGABgFAEQgEAEAAAGQAAAGAEAEQAFAEAHAAQAJAAAFgHQAFgFgBgJQAAgMgGgHQgIgJgRAAQgOAAgIAMQgHAKAAAQQAAAOAHALQAHALANACIAHABQAQAAAJgNIALAAQgLAYgfABQgUAAgNgQg");
	this.shape_9.setTransform(-6.4,-9.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AABAwQgEgCgFgGQgBAEgEADQgEADgHAAQgLAAgGgLQgGgKAAgOQAAgQAJgMQAKgOARAAQALAAAIAIQAHAHAAAMQAAAIgHAGIgEAEQgCACAAADQAAAIAMAAQAJAAAGgJQAFgJAAgNQAAgQgJgMQgKgMgTAAQgJAAgIAEQgJAEgEAIIgKAAQAEgMAMgHQALgGANAAQAXAAANANQAGAGAFALQAEALAAAJQABAXgKAOQgJAMgPAAQgIAAgFgCgAgbAeQAAAAgBABQAAAAAAABQAAAAAAABQgBABAAAAIACAEQAAABABAAQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAQgBAAAAABgAgiACQgCAGAAAIQAAAFABAFQAEgHAJgBQAHAAAGAEQABgFAEgEQAFgFAAgGQgBgFgDgDQgEgEgFAAQgQAAgGAMg");
	this.shape_10.setTransform(-15.9,-11.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgeAqQgMgMAAgSQAAgMAFgJQAGgKAKAAQAKAAACAIQADgJAKAAQAIAAAFAHQAGAGAAAHQAAALgHAHQgHAGgJAAQgIAAgGgEQgHgFAAgIIACgLQABgFgFAAQgEAAgCAGQgCAEAAAFQAAANAJAIQAKAHAMAAQANAAAKgJQAJgJAAgNQAAgKgFgHQgFgJgJAAIg0AAIAAgIQANABAAgCIgDgBQgCgCAAgDQAAgKALgDQAGgCAOAAQALAAAHACQAKADAHAIQAFAFAAAJIgJAAQgCgIgJgFQgIgEgLAAQgIAAgEACQgFABAAAFIABADIAEABIAXAAQAIAAAGACIAFADQAMAKAAAXQAAAUgMANQgMAOgTAAQgSAAgMgMgAgKACQAAAEADADQADADAEAAQAGAAADgDQAEgDAAgFQgEAFgGAAQgIABgFgIIAAADgAAAgKQAAAAAAABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAABQAAAAABAAQAAABAAAAQABAAAAAAQABAAAAAAIADgBQABgBAAAAQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAAAAAQgBAAAAABgAAfgaIAAAAgAAagdQgGgCgIAAIgXAAIgEgBIgBgDQAAgFAFgBQAEgCAIAAQALAAAIAEQAJAFACAIIgFgDg");
	this.shape_11.setTransform(-25.4,-11.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AANAvIAAgRIgUABQgRgBgLgHQgMgKAAgOQAAgIAFgHQAEgGAGgDQgDAAgCgDIgBgFQAAgFAEgDQAIgGAOABQAVAAAFALQABgFAFgDQAEgEAGABIAJAAIAAAIIgBAAQgBAAAAAAQgBAAAAABQgBAAAAABQAAAAgBABIADADIAGAGQAEAEAAAFQABAJgJAGQgHACgJAAIAAATIAIgCIAGgCIAAAJIgGACIgIACIAAATgAgVgOQgFAHAAAHQAAARATAEIAJAAIALAAIAAgoIgVAAQgIAAgFAFgAAXgXIAAAPIACAAQADAAADgCQADgCAAgEQAAgEgEgGQgEgFAAgDIAAgBQgDABAAALgAgSglQACADAAACQAAABAAAAQAAABgBAAQAAABAAAAQgBABAAAAIACAAIACAAIATAAQgBgGgGgCQgFgCgFAAIgGABg");
	this.shape_12.setTransform(-34.7,-9.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFB300").s().p("Al+CCQgVAAgQgPQgPgQAAgVIAAibQAAgVAPgQQAQgPAVAAIL8AAQAWAAAQAPQAPAQAAAVIAACbQAAAVgPAQQgQAPgWAAg");
	this.shape_13.setTransform(0,-10.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#4DD0E1").s().p("Al+CCQgVAAgQgPQgPgQAAgVIAAibQAAgVAPgQQAQgPAVAAIL8AAQAWAAAQAPQAPAQAAAVIAACbQAAAVgPAQQgQAPgWAAg");
	this.shape_14.setTransform(0,-10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_14},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-43.5,-23.6,87,47.2);


(lib.btnb2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(34.1,-0.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuIgkAmgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(32.1,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgsIAKgbIANgEIAJgBQAKAAADAHQgKAIgFANQgLAYASAag");
	this.shape_1.setTransform(29.4,-5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIACgOIAhgxQAhg2ACggQACgggcgWQgOgLgPgEQA9gCAUAsQAGAOABAQIAAAOQgDAzgkAwIgiAng");
	this.shape_2.setTransform(36.3,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAAAqQgFAHgMAAQgMAAgIgJQgHgJAAgMQABgOAJgFIgMAAIAAgKIAQAAQgDgBAAgFIABgEQAFgIANAAIAGAAQAKABAGAIQAGAIAAALIgcAAQgIAAgFADQgFAFAAAHQAAAGAEAFQAFAEAHAAQAFAAAEgDQAEgEAAgFIAAgKIAIAAIAAAKQAAAFAEAEQAEADAFAAQAQAAABgbQABgQgKgNQgLgNgPgBQgXAAgJAPIgKAAQALgYAfABQARAAANALQAJAIAFAMQAEAMgBAMQgBASgGALQgIANgNAAQgMAAgHgHgAgQgTIACAFIgCAEIASAAQgCgGgEgCIgIgBIgEAAg");
	this.shape_3.setTransform(17.7,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAkIAAgJQADACAEAAQAFAAAEgJQACgJAAgKQAAgKgCgJQgEgKgEAAQgFAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgHALQgHANgKAAQgGAAgFgDg");
	this.shape_4.setTransform(11.1,0.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgQAnQgQAAgJgMQgHgKAAgQQAAgZASgLQAGgDAJAAIAFAAIAAAJQAKgKAPABQAQAAAJALQAJAKAAARQAAAQgJALQgKAMgQAAIgFAAIAAgIQgGAFgEABQgEACgHAAIgEAAgAAGgMQANADAAAQQAAANgJAJQAJACAGgEQAKgHgBgQQAAgKgGgHQgHgIgJAAQgRgBgDAMQAEgDAFAAIAFABgAgfgVQgFAHAAAJQAAAKAFAHQAGAIALAAQAIAAAGgDQAGgEAAgHQABgEgDgCQgCgDgDAAQgDAAgCACQgCABAAAEIABADIgKAAQgCgJABgIQABgJAHgKIgGAAQgJAAgGAIg");
	this.shape_5.setTransform(4.3,0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYAcQgMgLAAgRQAAgQAMgLQAKgLARAAQARAAAJAHQAGAFAAAIIgJAAQAAgFgIgDQgGgDgIAAQgKAAgJAHQgJAIABALQgBAKAIAGQAIAHAKAAQAJAAAGgEQAIgGgBgIQABgKgLgDQAEADAAAIQAAAEgEAEQgFADgGAAQgGAAgFgEQgEgEAAgHQgBgHAHgFQAFgEAIAAQAMAAAJAHQAJAHgBALQABAPgLAJQgKAKgOAAQgQAAgKgLgAgBgIQAAAAgBABQAAAAgBABQAAAAAAABQAAABAAAAQAAABAAAAQAAABAAABQABAAAAABQABAAAAAAQAAAAABABQAAAAAAAAQAAAAABABQABAAABAAQAAAAABAAQAAgBABAAQAAAAABAAQABgBAAAAQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABAAAAQAAAAgBABg");
	this.shape_6.setTransform(-4.6,0.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgdAjQgOgLAAgRQAAgQAMgJQgBgJADgFQAHgOAOACQAHAAAEAFQAFgGAKABQAJAAAFAGQAFAHAAAPIglAAIgQABQAAAKAHAGQAIAHAJgBQAHAAAGgFQAGgDABgHIAJAAQgCALgJAHQgJAHgMAAQgWAAgJgQIgBAGQAAALAJAIQAJAHALAAQASAAAJgKQAFgHAAgIIAJAAQABAKgHAKQgMARgZAAQgRAAgMgKgAgOggQgCADAAAFIAHgBIAIAAQAAgKgHAAQgEAAgCADgAAIgZIAPAAQAAgKgHAAQgIAAAAAKg");
	this.shape_7.setTransform(-12.9,1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgRAkIAAgJQACACAFAAQAFAAADgJQADgJAAgKQAAgKgDgJQgDgKgFAAQgEAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgGALQgIANgKAAQgGAAgFgDg");
	this.shape_8.setTransform(-19.4,0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgVAuQgKgFgFgIQgIgMAAgRQAAgLAGgKQAHgKAJAAQAJABACAHQAFgJAJAAQAIABAGAGQAGAHABAIQABAKgIAIQgIAIgKgBQgKAAgGgFQgHgHABgIIABgGIADgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAAAgBQgBAAAAAAQgBAAgBAAQgEABgDAGQgCAFAAAGQgBAOAKAIQAKAJAOAAQANAAAKgLQAKgKgBgNQAAgRgIgKQgJgLgQAAQgVAAgJAOIgKAAQAFgLALgHQAKgFAOAAQAUgBALAMQAOAOABAWQABAWgNAPQgMAQgVAAQgMgBgJgEgAgNgDQABAEADACQAEADAFAAQAFABAFgFQAFgCAAgGIgBgCQgBADgDADQgEABgEAAQgKABgDgKIgCAHgAgCgOIACAEIADABQABAAAAAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAAAgBAAQgFAAAAAFg");
	this.shape_9.setTransform(-26,-0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYAcQgLgLAAgRQAAgQALgLQALgLAQAAQARAAAJAHQAFAFABAIIgJAAQgBgFgGgDQgHgDgJAAQgKAAgIAHQgIAIAAALQAAAKAHAGQAIAHAKAAQAJAAAHgEQAGgGAAgIQAAgKgKgDQAFADAAAIQgBAEgFAEQgEADgGAAQgGAAgFgEQgEgEgBgHQAAgHAHgFQAFgEAIAAQAMAAAJAHQAJAHAAALQAAAPgLAJQgKAKgOAAQgPAAgLgLgAgBgIQAAAAgBABQAAAAAAABQgBAAAAABQAAABAAAAQAAABAAAAQAAABABABQAAAAAAABQABAAAAAAQAAAAABABQAAAAAAAAQAAAAABABQABAAABAAQAAAAABAAQAAgBABAAQAAAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABAAAAQAAAAgBABg");
	this.shape_10.setTransform(-34.5,0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFB300").s().p("AkaCCQgVAAgPgQQgQgPAAgWIAAiaQAAgVAQgPQAPgQAVAAII1AAQAVAAAQAQQAPAPAAAVIAACaQAAAWgPAPQgQAQgVAAg");
	this.shape_11.setTransform(-8,0);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#4DD0E1").s().p("AkaCCQgVAAgPgQQgQgPAAgWIAAiaQAAgVAQgPQAPgQAVAAII1AAQAVAAAQAQQAPAPAAAVIAACaQAAAWgPAPQgQAQgVAAg");
	this.shape_12.setTransform(-8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_12},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-41.5,-13,84.1,26);


(lib.btnb1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.location();
	this.instance.parent = this;
	this.instance.setTransform(-29.4,8.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EF4E55").s().p("AgtA8Qgkg6AGgrQAGguAggPQAVgKAVAEQAbADATAOQAgAYgIAvQgJAugnAuIgkAmgAgYhJQgKAKgCAPQgBAPAIALQAIAMANABQAMACAKgKQALgJACgPQABgPgIgMQgIgLgMgCIgEAAQgKAAgKAIg");
	this.shape.setTransform(-30.3,8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#BD3D44").s().p("AgRAmIgFgrIAKgbIANgGIAJgBQAKABADAHQgKAIgFAMQgLAZASAag");
	this.shape_1.setTransform(-33,3.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BD3D44").s().p("AgzBrIACgOIAhgxQAhg2ACggQACgggcgWQgOgLgPgEQA9gCAUAsQAGAOABAQIAAAOQgDAzgkAwIgiAng");
	this.shape_2.setTransform(-26.1,8.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AggApQgNgJABgRQABgPAMgEQgHgCAAgHQAAgIAJgEQAGgDAJABQALAAAFAJQAHAIAAAKIgbAAQgGAAgEADQgEAEAAAGQABANASAEIANABQAPAAAKgKQAIgJABgOQAAgQgJgKQgJgMgSAAQgUgBgKAPIgKAAQAFgLALgGQALgGAOAAQAUAAANAOQANAPAAAUQAAAVgMAOQgNAPgUAAQgTAAgNgJgAgVgRIADAEQABADgCADIARAAQgBgFgDgDQgEgDgFAAIgGABg");
	this.shape_3.setTransform(30.8,-7.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAkIAAgJQADACAEAAQAFAAAEgJQACgJAAgKQAAgKgCgJQgEgKgEAAQgFAAgDACIAAgJQAEgDAIAAQAKAAAHANQAGAMAAAOQAAAOgHALQgHANgKAAQgGAAgFgDg");
	this.shape_4.setTransform(24.2,-6.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AggAiQgNgOABgVQAAgVANgNQANgOAVAAQASAAAMAMQAMAMAAASQAAAOgIALQgIAKgOABQgOAAgHgJQgJgJACgLQABgMAIgGIgKAAIAAgJIAjAAIAAAJIgHAAQgGgBgFAFQgEAEAAAGQAAAGAEAEQAFAEAHAAQAJAAAFgHQAFgGgBgJQAAgLgGgHQgIgKgRAAQgOAAgIANQgHALAAAPQAAAOAHAKQAHAMANADIAHAAQAQAAAJgNIALAAQgLAZgfgBQgUAAgNgPg");
	this.shape_5.setTransform(17.7,-5.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgtAWQgCgHACgGIgHAAIAAgHIAFgGIAIgHQgFAAgDgDQgDgDAAgFQAAgHAGgFQAFgEAIAAQALgBAGAIQALgHAQAAIAFAAQAQABAKAMQAKAMgBAPQgBAPgKALQgKALgQAAIgFAAIAAgIQgKAIgPAAIgBAAQgZAAgFgRgAgEgWQAEAAAFADQAFADADADQAHAIAAAKQAAAPgIAJIAEABQAHAAAFgEQAKgGABgNQAAgIgEgIQgEgHgIgEQgGgEgLAAQgGAAgEACgAgagKIgPAMIAGAAQgCAGABAGQAEAKAQgBQAJAAAHgGQAFgFABgIQAAgHgFgGQgFgFgIAAIgBAAQgHAAgGAEgAgggdIgEACQAEABABACQACACgBADIAEgDIAEgDQgCgDgDgBIgCAAIgDAAg");
	this.shape_6.setTransform(7.5,-6.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgVAuQgKgFgFgIQgIgMAAgRQAAgLAGgKQAHgKAJABQAJAAACAHQAFgIAJAAQAIgBAGAHQAGAGABAJQABAKgIAIQgIAHgKAAQgKABgGgHQgHgGABgIIABgGIADgGQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBAAgBABQgEAAgDAGQgCAFAAAGQgBANAKAJQAKAJAOgBQANAAAKgKQAKgKgBgOQAAgRgIgKQgJgKgQgBQgVABgJAPIgKAAQAFgMALgGQAKgHAOAAQAUAAALAMQAOAOABAWQABAWgNAPQgMAPgVAAQgMABgJgFgAgNgEQABAEADADQAEADAFAAQAFAAAFgEQAFgDAAgFIgBgCQgBADgDACQgEADgEAAQgKgBgDgIIgCAFgAgCgOIACAEIADACQABAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBAAAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAAAgBAAQgFgBAAAGg");
	this.shape_7.setTransform(-2.8,-7.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAZASQAHgEAAgGIgBgCQgBgFgKgCQgIgCgMAAQgKAAgJADQgKABgBAFIgBACQAAAGAGAEIgIAAQgIgDAAgKQAAgEACgBQAEgKAMgEQAJgDAOAAQAiAAAGARQACABAAAEQAAAKgIADg");
	this.shape_8.setTransform(-12.4,-11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgdAjQgOgLAAgRQAAgQAMgJQgBgJADgFQAHgOAOACQAHAAAEAFQAFgGAKABQAJAAAFAGQAFAHAAAPIglAAIgQABQAAAKAHAGQAIAHAJgBQAHAAAGgFQAGgDABgHIAJAAQgCALgJAHQgJAHgMAAQgWAAgJgQIgBAGQAAALAJAIQAJAHALAAQASAAAJgKQAFgHAAgIIAJAAQABAKgHAKQgMARgZAAQgRAAgMgKgAgOggQgCADAAAFIAHgBIAIAAQAAgKgHAAQgEAAgCADgAAIgZIAPAAQAAgKgHAAQgIAAAAAKg");
	this.shape_9.setTransform(-12,-5.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAZASQAHgEAAgGIgBgCQgBgFgKgCQgIgCgMAAQgKAAgJADQgKABgBAFIgBACQAAAGAGAEIgIAAQgIgDAAgKQAAgEACgBQAEgKAMgEQAJgDAOAAQAiAAAGARQACABAAAEQAAAKgIADg");
	this.shape_10.setTransform(-21.5,-11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgiAcQgLgJAAgQQAAgGADgFQADgGAEgDQgEgCAAgGIAAgEQAEgJARAAQANAAAFAHQAFgHAPAAQAHAAAFADQAGADABAGQABAFgDAEQAEADACAFQADAGAAAGQAAAQgMAKQgNAKgVAAQgWAAgMgLgAgfgIQgDAEAAAGQAAAKAMAFQAJADANAAQANAAAJgDQALgFABgKQAAgGgEgEQgDgEgGAAIgqAAQgGAAgEAEgAAKgbQgEACgBAFIAWAAIAAgDQAAgDgDgBQgDgCgDAAIgBAAQgEAAgDACgAgYgdQABABAAAAQABABAAAAQAAAAAAABQABABAAAAQAAABAAAAQAAABAAABQAAAAgBABQAAAAAAABIAQAAQAAgHgKgCIgEgBIgEABg");
	this.shape_11.setTransform(-21.5,-6.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFB300").s().p("AkZCCQgWAAgPgQQgQgPAAgVIAAiaQAAgWAQgQQAPgPAWAAIIzAAQAWAAAPAPQAQAQAAAWIAACaQAAAVgQAPQgPAQgWAAg");
	this.shape_12.setTransform(4.6,-6.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#4DD0E1").s().p("AkZCCQgWAAgPgQQgQgPAAgVIAAiaQAAgWAQgQQAPgPAWAAIIzAAQAWAAAPAPQAQAQAAAWIAACaQAAAVgQAPQgPAQgWAAg");
	this.shape_13.setTransform(4.6,-6.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_13},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38,-19.6,76.1,39.2);


(lib.btn4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape.setTransform(140.6,-2.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhBAsIAAgYIB0AAIAAgpIAIgKQAEgHADgFIAABXg");
	this.shape_1.setTransform(124.9,5.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgcBBQgXAAgOgPQgPgOgBgVQAAgTAGgOIAPAAQgBAFACACQABACAFAAQAIAAAFgDQANgIACgJIABgFQAAgLgHgDQgGgCgGACQAKADAAAMQAAAGgFAEQgEAEgHAAIgGgBQgMgDAAgPQAAgFACgEQAGgRAYAAQAJAAAHAEQAIAEACAIQANgRAbABQAcABAOAaQAKASgBAUQAAAsggAPQgKAFgLAAIgPgBIAAgLQgLAIgIACQgHACgMAAIgEAAgAAPgQQAPAMAAATQAAAVgNANIALABQALAAAGgFQAQgMAAgZQAAgQgLgNQgLgMgQgBQgVgBgIAMIABAAQAMAAAIAHgAhAAJQAAAKAGAHQAIALASAAIALgBQAMgBAIgJQAHgIAAgLQAAgGgDgEQgDgGgLgBQgFAAgIAFIgOAHQgHADgHAAQgHAAgEgDIgBAHg");
	this.shape_2.setTransform(124.2,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAWBPIAAgcQgUABgOgBQgdAAgRgNQgUgPAAgaQAAgNAHgLQAHgJAKgFQgFgCgCgEQgDgDAAgFQAAgHAIgGQAMgKAXAAQAkAAAIAVQADgKAIgGQAHgFAJAAIAQAAIAAAOIgDAAQgFAAgBAFQAAACAFAEIALAJQAFAHABAIQABAQgOAIQgMAGgQgBIAAAhIAOgDIAKgFIAAAPIgLAEIgNAEIAAAfgAgjgXQgJAKAAANQABAcAfAHIAQABIASgBIAAhEIgkAAQgNAAgIAKgAAmgnIAAAZIADABQAGgBAFgDQAEgEAAgGQAAgHgGgJQgHgJAAgFIABgBQgGABAAASgAAJgwQgCgJgLgEQgOgEgMADQACAEAAAEQAAAFgDABIADAAIAEAAIAhAAIAAAAg");
	this.shape_3.setTransform(107.5,0.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_4.setTransform(83.2,-1.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag8BMQgRgGgGAAQgHAAgHAJIgLgIIAXgiQgKgMAAgUQAAgHABgHQAFgVATgKQAOgIASAAIAKAAIAWghIAQAAIgVAiIALAFIAKAHQADgFADgDQAHgFAKAAQALAAAHAIQAJgJANABQAjABABA8QABAbgKATQgMAWgZAAQgQAAgJgJQgIgJAAgPIAAgHIAPAAIAAAEQAAAUARAAQALgBAEgQQACgJAAgWQABgRgDgNQgDgQgHAAQgKAAAAAPIAAAfIgQAAIAAgfQAAgQgKgBQgEAAgDADQgDADAAADQAMAQAAAYQgBAdgSARQgQAQgeABIgCABQgLAAgPgGgAg/A7QAMAFAMAAIALgBQAQgDAKgMQAJgMgBgQQAAgQgJgLQgLgMgPgBIgOAUQAEgBAEAAQAJAAAGAFQAHAGABAJQABAKgFAJQgGAIgKADQgHACgJAAQgPAAgLgFIgIALIAGAAQAGAAAHACgAhBAhQADACAFABQAKACAEgCQgJgCgDgFQgCgFABgFgAgrAUQAAAIAIAAQAHAAAAgIQAAgHgHAAQgIAAAAAHgAhHgGQgFAHAAALQAAAFABAFIAcgpQgRACgHALg");
	this.shape_5.setTransform(64,-2.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAJAAQAHAAAGgPQAEgPABgRQgBgRgEgPQgGgRgHAAQgIgBgFAEIAAgPQAGgFAOAAQASAAALAWQAKAUAAAYQAAAXgKATQgNAVgRAAQgLAAgIgEg");
	this.shape_6.setTransform(50.2,-1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgkBNQgQgJgJgNQgNgTAAgeQAAgSALgRQALgQAPAAQAPABAEAKQAHgMAQAAQAOAAAKAKQAKALABAPQABAQgNAOQgNANgRAAQgQAAgLgKQgLgLABgOQAAgGACgEIAEgJQABgEgCgCQgCgBgEAAQgHABgFAJQgEAKAAAKQgBAWARAOQARAOAWAAQAWAAARgRQARgRgBgWQAAgdgOgRQgPgSgcAAQgjAAgOAZIgQAAQAHgUATgKQARgKAWAAQAjAAATATQAWAYACAlQABAlgVAZQgVAZgjAAQgTAAgQgHgAgWgGQABAHAGAFQAHAEAIAAQAJAAAIgFQAHgGAAgJIAAgEQgCAGgGAEQgGADgHAAQgSAAgDgQQgEAJAAACgAgDgYQAAAEACACQACADAEABQAEgBADgDQACgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_7.setTransform(39.3,-2.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag/AMQgFgJAAgIQAAgIAEgGIAPAAQgDAEAAAEQAAAFAEAFQANANAjAAQAUAAAPgGQAIgEAFgDQAFgFAAgIQAAgJgHgHIAKgIQANAKAAATQAAAJgDAFQgNAig1AAQgvAAgQgbg");
	this.shape_8.setTransform(23.3,6.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_9.setTransform(23.2,-1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgNAAIAAgMIAiAAQALAAAGAFQAHAGAAALQABAJgKAFQgJAFgNgBIAAAfgAgCgWIAAAQQAMAAABgIQgBgJgJAAIgDABg");
	this.shape_10.setTransform(4.3,-9.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_11.setTransform(-1,-1.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgpAvQgTgTAAgbQAAgdATgSQASgSAcAAQAdAAAOAMQAKAJABAMIgQAAQAAgIgMgFQgLgFgOAAQgSAAgOAMQgOANAAASQAAARANALQANAMASAAQAOAAALgIQAMgIAAgPQAAgRgRgFQAHAGAAAMQAAAHgIAHQgIAGgKAAQgLAAgHgHQgJgHABgMQAAgMAKgIQAJgHAOAAQAUAAAPAMQAOAMAAATQAAAYgRAQQgRAQgXAAQgbAAgSgSgAgDgPQgCADAAAFQAAAFACACQADADAEAAQAFAAADgDQADgCAAgFQAAgEgDgDQgDgEgEAAQgFAAgDADg");
	this.shape_12.setTransform(-15.4,-1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgkBNQgQgJgJgNQgNgTAAgeQAAgSALgRQALgQAPAAQAPABAEAKQAHgMAQAAQAOAAAKAKQAKALABAPQABAQgNAOQgNANgRAAQgQAAgLgKQgLgLABgOQAAgGACgEIAEgJQABgEgCgCQgCgBgEAAQgHABgFAJQgEAKAAAKQgBAWARAOQARAOAWAAQAWAAARgRQARgRgBgWQAAgdgOgRQgPgSgcAAQgjAAgOAZIgQAAQAHgUATgKQARgKAWAAQAjAAATATQAWAYACAlQABAlgVAZQgVAZgjAAQgTAAgQgHgAgWgGQABAHAGAFQAHAEAIAAQAJAAAIgFQAHgGAAgJIAAgEQgCAGgGAEQgGADgHAAQgSAAgDgQQgEAJAAACgAgDgYQAAAEACACQACADAEABQAEgBADgDQACgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_13.setTransform(-29.9,-2.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("Ag/AMQgFgJAAgIQAAgIAEgGIAPAAQgDAEAAAEQAAAFAEAFQANANAjAAQAUAAAPgGQAIgEAFgDQAFgFAAgIQAAgJgHgHIAKgIQANAKAAATQAAAJgDAFQgNAig1AAQgvAAgQgbg");
	this.shape_14.setTransform(-45.9,6.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgcBBQgZAAgPgUQgNgRAAgaQAAgfAPgRQAHgIAIgEQALgGAMAAIALAAIAAAPQARgQAZABQAbAAAPATQAOAQABAcQAAAbgPATQgQAUgbAAIgJAAIAAgOQgLAIgGADQgJADgNAAIgEAAgAASAxIAFABQAJAAACgGIABgDIgFgPQgCAMgKALgAAtATQAAADACAGQADAHAAAFQAKgKAAgNQAAgGgIAAQgHAAAAAIgAgxgnQgKALgBAOQgCATAJANQAKAPATABQANAAAKgGQAKgHABgLQABgGgEgFQgEgGgFAAQgGABgDADQgEADAAAGIABAGIgPAAQgEgQACgOQACgPALgPIgHgBQgOAAgKAKgAAJgVQATAHACAQQAEgGAKgCQAJgBAEAEQgCgNgJgJQgKgKgQgBQgMgBgIAFQgLAFgDALQAEgGALAAIAIABg");
	this.shape_15.setTransform(-46.3,-1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgcBBQgXAAgOgPQgPgOgBgVQAAgTAGgOIAPAAQgBAFACACQABACAFAAQAIAAAFgDQANgIACgJIABgFQAAgLgHgDQgGgCgGACQAKADAAAMQAAAGgFAEQgEAEgHAAIgGgBQgMgDAAgPQAAgFACgEQAGgRAYAAQAJAAAHAEQAIAEACAIQANgRAbABQAcABAOAaQAKASgBAUQAAAsggAPQgKAFgLAAIgPgBIAAgLQgLAIgIACQgHACgMAAIgEAAgAAPgQQAPAMAAATQAAAVgNANIALABQALAAAGgFQAQgMAAgZQAAgQgLgNQgLgMgQgBQgVgBgIAMIABAAQAMAAAIAHgAhAAJQAAAKAGAHQAIALASAAIALgBQAMgBAIgJQAHgIAAgLQAAgGgDgEQgDgGgLgBQgFAAgIAFIgOAHQgHADgHAAQgHAAgEgDIgBAHg");
	this.shape_16.setTransform(-71.3,-1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgEBTQgcgBgRgSQgRgRgBgbQAAgRAHgNQAGgNAPgNQAKgIATgTQAKgMACgHIAbAAQgFAKgIAJQgHAIgMAIQASgBAOAFQAnAPAAAtQAAAdgVATQgUASgbAAIgEAAgAgkgSQgLANAAAQQAAAPAKALQAPAOAWAAQASAAAMgKQARgMAAgUQAAgJgDgIQgFgMgMgHQgLgGgPAAQgYAAgNAPg");
	this.shape_17.setTransform(-86.9,-2.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgXA4QgIAJgVAAQgMAAgIgHQgJgIAAgMQAAgNAGgIIgKAAIAAgKIAIgKIAMgMIgEAAQgHAAgEgFQgEgFAAgIQAAgLAJgJQAJgKAMgBQAWgBAKAOQAUgOAbABQAaACARATQARASABAZQABArggASQgHAEgOAAIgMAAIAAgKQgJAKgNAAIgBAAQgMAAgKgJgAgKgnQAUACAMAOQAPARAAAVQAAAWgLAMIAHABQAKAAAJgIQAKgJABgLIABgJQAAgagRgPQgOgNgZAAQgJAAgJACgAgqgRQgMAKgLAMIAKAAQgGAIABAKQAAAFAFADQAEAEAHAAQAJAAAEgLQACgGAAgSIAOAAQAAATABAGQADAJAJAAQAIAAAFgIQAFgIAAgKQAAgLgIgKQgHgJgJgCIgIAAQgRAAgJAHgAg8gsQAGABABAGQACAFgDAGIAIgIIAJgIQgCgGgJAAQgJAAgDAEg");
	this.shape_18.setTransform(-102.8,-1.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_19.setTransform(-119.8,-1.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgcA2QgMgKAAgTQAAgOAFgKQAEgGAJgKQAKgMAZgUIgxAAIAAgPIBKAAIAAAOIgRANQgJAHgHAGQgKAMgDAGQgHAKABAKQABAIAFAEQAFAEAGAAQANAAAFgLIABgIQAAgEgDgFIAPAAQAHALgBANQgBAPgMAKQgMAKgPAAQgRAAgLgJg");
	this.shape_20.setTransform(-132.2,-0.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_21.setTransform(-144.4,-2.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["#C4291D","#E26D45"],[0,1],-293,0,293,0).s().p("EgrqAFoQg4AAgngoQgognAAg4IAAnBQAAg4AognQAngoA4AAMBXVAAAQA4AAAoAoQAnAnAAA4IAAHBQAAA4gnAnQgoAog4AAg");
	this.shape_22.setTransform(-2,-3);

	this.instance = new lib.Path_1_2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,295,39);
	this.instance.alpha = 0.359;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-295,-39,590,78);


(lib.btn3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape.setTransform(123.4,-1.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag/AMQgFgJAAgIQAAgIAEgGIAPAAQgDAEAAAEQAAAFAEAFQANANAjAAQAUAAAPgGQAIgEAFgDQAFgFAAgIQAAgJgHgHIAKgIQANAKAAATQAAAJgDAFQgNAig1AAQgvAAgQgbg");
	this.shape_1.setTransform(107.3,6.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_2.setTransform(106.2,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgNAAIAAgMIAiAAQAKAAAHAFQAIAGAAALQgBAJgIAFQgKAFgNgBIAAAfgAgCgWIAAAQQANAAAAgIQAAgJgKAAIgDABg");
	this.shape_3.setTransform(94.1,-9.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgVAQgJQgHgDAAgLQAAgFABgEQAJgQAagBQAWAAAIAOQAKgOAYAAQAMAAAKAIQAKAHAAAMQAAAJgEAGQAOAMAAAQQAAAbgTAQQgVASgkAAQgkgBgVgSgAg4ADQABAQATAIQAPAHAVgBQAWABAPgHQAUgIABgQQAAgHgFgIQgIAGgQAEQAHADAAAJQAAAIgLAEQgKAEgMAAQgOAAgKgEQgLgFAAgKQAAgPAcAAQAHAAAGgCQATgCADgDIhBAAQgXAAABASgAgMAHQAAAGANAAQAOAAAAgFQAAgGgOABQgNAAAAAEgAAQgsQgHAGAAAIIAlAAQABgCAAgEQAAgDgBgCQgEgIgLAAQgJAAgGAFgAgngwQAEADABAFQABAGgEAEIAdAAQAAgOgQgFIgHgBQgFAAgDACg");
	this.shape_4.setTransform(88.8,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgqAvQgSgTAAgbQAAgdASgSQATgSAbAAQAeAAAOAMQAJAJABAMIgOAAQgCgIgMgFQgKgFgOAAQgRAAgOAMQgPANAAASQAAARANALQANAMARAAQAQAAAKgIQAMgIAAgPQABgRgSgFQAHAGgBAMQAAAHgHAHQgIAGgKAAQgKAAgJgHQgHgHgBgMQAAgMALgIQAKgHAMAAQAVAAAPAMQAOAMAAATQAAAYgRAQQgRAQgYAAQgaAAgTgSgAgDgPQgDADAAAFQAAAFADACQADADAEAAQAFAAADgDQADgCAAgFQAAgEgDgDQgDgEgFAAQgEAAgDADg");
	this.shape_5.setTransform(74.5,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgNAAIAAgMIAiAAQALAAAGAFQAIAGAAALQgBAJgIAFQgKAFgNgBIAAAfgAgCgWIAAAQQAMAAAAgIQAAgJgJAAIgDABg");
	this.shape_6.setTransform(65.1,-9.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgXA4QgIAJgVAAQgMAAgIgHQgJgIAAgMQAAgNAGgIIgKAAIAAgKIAIgKIAMgMIgEAAQgHAAgEgFQgEgFAAgIQAAgLAJgJQAJgKAMgBQAWgBAKAOQAUgOAbABQAaACARATQARASABAZQABArggASQgHAEgOAAIgMAAIAAgKQgJAKgNAAIgBAAQgMAAgKgJgAgKgnQAUACAMAOQAPARAAAVQAAAWgLAMIAHABQAKAAAJgIQAKgJABgLIABgJQAAgagRgPQgOgNgZAAQgJAAgJACgAgqgRQgMAKgLAMIAKAAQgGAIABAKQAAAFAFADQAEAEAHAAQAJAAAEgLQACgGAAgSIAOAAQAAATABAGQADAJAJAAQAIAAAFgIQAFgIAAgKQAAgLgIgKQgHgJgJgCIgIAAQgRAAgJAHgAg8gsQAGABABAGQACAFgDAGIAIgIIAJgIQgCgGgJAAQgJAAgDAEg");
	this.shape_7.setTransform(59.1,-1.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgcBBQgXAAgOgPQgPgOgBgVQAAgTAGgOIAPAAQgBAFACACQABACAFAAQAIAAAFgDQANgIACgJIABgFQAAgLgHgDQgGgCgGACQAKADAAAMQAAAGgFAEQgEAEgHAAIgGgBQgMgDAAgPQAAgFACgEQAGgRAYAAQAJAAAHAEQAIAEACAIQANgRAbABQAcABAOAaQAKASgBAUQAAAsggAPQgKAFgLAAIgPgBIAAgLQgLAIgIACQgHACgMAAIgEAAgAAPgQQAPAMAAATQAAAVgNANIALABQALAAAGgFQAQgMAAgZQAAgQgLgNQgLgMgQgBQgVgBgIAMIABAAQAMAAAIAHgAhAAJQAAAKAGAHQAIALASAAIALgBQAMgBAIgJQAHgIAAgLQAAgGgDgEQgDgGgLgBQgFAAgIAFIgOAHQgHADgHAAQgHAAgEgDIgBAHg");
	this.shape_8.setTransform(33.6,-1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_9.setTransform(17.3,-2.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgNAAIAAgMIAiAAQALAAAGAFQAIAGAAALQgBAJgIAFQgKAFgNgBIAAAfgAgCgWIAAAQQANAAAAgIQAAgJgKAAIgDABg");
	this.shape_10.setTransform(-1.4,-9.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_11.setTransform(-7.7,-1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_12.setTransform(-24.9,-1.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAWBPIAAgcQgUABgOgBQgdAAgRgNQgUgPAAgaQAAgNAHgLQAHgJAKgFQgFgCgCgEQgDgDAAgFQAAgHAIgGQAMgKAXAAQAkAAAIAVQADgKAIgGQAHgFAJAAIAQAAIAAAOIgDAAQgFAAgBAFQAAACAFAEIALAJQAFAHABAIQABAQgOAIQgMAGgQgBIAAAhIAOgDIAKgFIAAAPIgLAEIgNAEIAAAfgAgjgXQgJAKAAANQABAcAfAHIAQABIASgBIAAhEIgkAAQgNAAgIAKgAAmgnIAAAZIADABQAGgBAFgDQAEgEAAgGQAAgHgGgJQgHgJAAgFIABgBQgGABAAASgAAJgwQgCgJgLgEQgOgEgMADQACAEAAAEQAAAFgDABIADAAIAEAAIAhAAIAAAAg");
	this.shape_13.setTransform(-41.1,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_14.setTransform(-65.1,-2.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAJAAQAHAAAGgPQAEgPAAgRQAAgRgEgPQgGgRgHAAQgIgBgFAEIAAgPQAGgFAOAAQASAAALAWQAKAUAAAYQAAAXgLATQgMAVgRAAQgLAAgIgEg");
	this.shape_15.setTransform(-76.2,-1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_16.setTransform(-87.1,-1.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAqAeQALgHAAgKIgBgEQgCgHgQgEQgPgDgTAAQgSAAgOAEQgRADgCAHIgBAEQAAALAKAGIgOAAQgMgEAAgSQAAgGACgDQAHgQAUgHQAPgFAYAAQA4AAALAcQACADAAAGQAAARgMAFg");
	this.shape_17.setTransform(-103.4,-9.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgXA4QgIAJgVAAQgMAAgIgHQgJgIAAgMQAAgNAGgIIgKAAIAAgKIAIgKIAMgMIgEAAQgHAAgEgFQgEgFAAgIQAAgLAJgJQAJgKAMgBQAWgBAKAOQAUgOAbABQAaACARATQARASABAZQABArggASQgHAEgOAAIgMAAIAAgKQgJAKgNAAIgBAAQgMAAgKgJgAgKgnQAUACAMAOQAPARAAAVQAAAWgLAMIAHABQAKAAAJgIQAKgJABgLIABgJQAAgagRgPQgOgNgZAAQgJAAgJACgAgqgRQgMAKgLAMIAKAAQgGAIABAKQAAAFAFADQAEAEAHAAQAJAAAEgLQACgGAAgSIAOAAQAAATABAGQADAJAJAAQAIAAAFgIQAFgIAAgKQAAgLgIgKQgHgJgJgCIgIAAQgRAAgJAHgAg8gsQAGABABAGQACAFgDAGIAIgIIAJgIQgCgGgJAAQgJAAgDAEg");
	this.shape_18.setTransform(-104.1,-1.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AguBTQgbgBgSgSQgQgRgBgbQAAgRAHgNQAGgNAPgNQAKgIATgTQAJgMAEgHIAbAAQgFAKgIAJQgHAIgOAIQAUgBAOAFQAmAPAAAtQAAAdgVATQgSASgdAAIgFAAgAhNgSQgLANAAAQQAAAQALAKQANAOAYAAQARAAANgJQAQgNAAgUQAAgJgDgIQgEgMgMgHQgLgGgPAAQgZAAgNAPgAAnBFQgJgNAAgWQAAgcAWgjQAKgQAQgVIhRAAIAAgOIBlAAIAAANQgRAUgLARQgSAeAAAZQAAAaAWAAQAHAAAGgEQAFgEABgHQAAgGgEgGIAOAAQAGAHAAAPQAAAQgMALQgLAKgRAAQgUAAgKgOg");
	this.shape_19.setTransform(-124.2,-2.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#C4291D","#E26D45"],[0,1],-293,0,293,0).s().p("EgrqAFoQg4AAgngnQgogoAAg4IAAnBQAAg4AognQAngoA4AAMBXVAAAQA4AAAoAoQAnAnAAA4IAAHBQAAA4gnAoQgoAng4AAg");
	this.shape_20.setTransform(-2,-3);

	this.instance = new lib.Path_1_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,295,39);
	this.instance.alpha = 0.359;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-295,-39,590,78);


(lib.btn2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAIAAQAIAAAGgPQAFgPgBgRQABgRgFgPQgGgRgHAAQgIgBgFAEIAAgPQAHgFANAAQARAAAMAWQAKAUAAAYQAAAXgLATQgLAVgSAAQgLAAgIgEg");
	this.shape.setTransform(141.1,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_1.setTransform(130.2,-1.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAqAeQALgHAAgKIgBgEQgCgHgQgEQgPgDgTAAQgSAAgOAEQgRADgCAHIgBAEQAAALAKAGIgOAAQgMgEAAgSQAAgGACgDQAHgQAUgHQAPgFAYAAQA4AAALAcQACADAAAGQAAARgMAFg");
	this.shape_2.setTransform(113.9,-9.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgXA4QgIAJgVAAQgMAAgIgHQgJgIAAgMQAAgNAGgIIgKAAIAAgKIAIgKIAMgMIgEAAQgHAAgEgFQgEgFAAgIQAAgLAJgJQAJgKAMgBQAWgBAKAOQAUgOAbABQAaACARATQARASABAZQABArggASQgHAEgOAAIgMAAIAAgKQgJAKgNAAIgBAAQgMAAgKgJgAgKgnQAUACAMAOQAPARAAAVQAAAWgLAMIAHABQAKAAAJgIQAKgJABgLIABgJQAAgagRgPQgOgNgZAAQgJAAgJACgAgqgRQgMAKgLAMIAKAAQgGAIABAKQAAAFAFADQAEAEAHAAQAJAAAEgLQACgGAAgSIAOAAQAAATABAGQADAJAJAAQAIAAAFgIQAFgIAAgKQAAgLgIgKQgHgJgJgCIgIAAQgRAAgJAHgAg8gsQAGABABAGQACAFgDAGIAIgIIAJgIQgCgGgJAAQgJAAgDAEg");
	this.shape_3.setTransform(113.2,-1.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgtBTQgcgBgRgSQgRgRgBgbQAAgRAHgNQAHgNAOgNQAKgIATgTQAJgMAEgHIAbAAQgFAKgIAJQgIAIgMAIQATgBAOAFQAmAPAAAtQAAAdgVATQgSASgdAAIgEAAgAhNgSQgLANAAAQQAAAQAKAKQAPAOAWAAQATAAANgJQAPgNAAgUQAAgJgEgIQgDgMgMgHQgMgGgOAAQgZAAgNAPgAAnBFQgKgNAAgWQAAgcAYgjQAJgQARgVIhSAAIAAgOIBmAAIAAANQgRAUgLARQgTAeAAAZQAAAaAVAAQAJAAAFgEQAGgEgBgHQAAgGgCgGIANAAQAGAHAAAPQAAAQgLALQgMAKgRAAQgTAAgLgOg");
	this.shape_4.setTransform(93.1,-2.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgcBBQgXAAgOgPQgPgOgBgVQAAgTAGgOIAPAAQgBAFACACQABACAFAAQAIAAAFgDQANgIACgJIABgFQAAgLgHgDQgGgCgGACQAKADAAAMQAAAGgFAEQgEAEgHAAIgGgBQgMgDAAgPQAAgFACgEQAGgRAYAAQAJAAAHAEQAIAEACAIQANgRAbABQAcABAOAaQAKASgBAUQAAAsggAPQgKAFgLAAIgPgBIAAgLQgLAIgIACQgHACgMAAIgEAAgAAPgQQAPAMAAATQAAAVgNANIALABQALAAAGgFQAQgMAAgZQAAgQgLgNQgLgMgQgBQgVgBgIAMIABAAQAMAAAIAHgAhAAJQAAAKAGAHQAIALASAAIALgBQAMgBAIgJQAHgIAAgLQAAgGgDgEQgDgGgLgBQgFAAgIAFIgOAHQgHADgHAAQgHAAgEgDIgBAHg");
	this.shape_5.setTransform(65.3,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag1BJQgVgOAAgYQAAgXAOgJIgNAAIAAgNIASAAQgEgDAAgGQAAgIAHgFQALgIAUABQAWACAJAOQAGAJABARIglAAQgfAAgCATQgCAQASAJQAPAHAVgBQAVgBAOgNQAOgMAEgUIACgKQAAgSgLgPQgJgQgPgHIgHAIQgEAEgEACQgKAEgLAAQgQAAgGgIQgEgFADgLQABgEgJAAIgFABIAAgOQAMgCAOAAQARAAALADQAeAHAUAUQAXAYAAAkQABAigWAXQgWAVgiAAQgfABgTgMgAgfgXQADACABADQAAAEgDADIAcAAQgBgIgMgEIgIgBIgIABgAgZg9QABAGAKgBQAMAAAFgJQgKgEgSAAIAAAIg");
	this.shape_6.setTransform(49.1,-2.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_7.setTransform(24.3,-1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_8.setTransform(7.3,-2.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgcA2QgMgKAAgTQAAgOAFgKQAEgGAJgKQAKgMAZgUIgxAAIAAgPIBKAAIAAAOIgRANQgJAHgHAGQgKAMgDAGQgHAKABAKQABAIAFAEQAFAEAGAAQANAAAFgLIABgIQAAgEgDgFIAPAAQAHALgBANQgBAPgMAKQgMAKgPAAQgRAAgLgJg");
	this.shape_9.setTransform(-4.8,-0.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_10.setTransform(-17.3,-1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_11.setTransform(-41.1,-2.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgXA4QgIAJgVAAQgMAAgIgHQgJgIAAgMQAAgNAGgIIgKAAIAAgKIAIgKIAMgMIgEAAQgHAAgEgFQgEgFAAgIQAAgLAJgJQAJgKAMgBQAWgBAKAOQAUgOAbABQAaACARATQARASABAZQABArggASQgHAEgOAAIgMAAIAAgKQgJAKgNAAIgBAAQgMAAgKgJgAgKgnQAUACAMAOQAPARAAAVQAAAWgLAMIAHABQAKAAAJgIQAKgJABgLIABgJQAAgagRgPQgOgNgZAAQgJAAgJACgAgqgRQgMAKgLAMIAKAAQgGAIABAKQAAAFAFADQAEAEAHAAQAJAAAEgLQACgGAAgSIAOAAQAAATABAGQADAJAJAAQAIAAAFgIQAFgIAAgKQAAgLgIgKQgHgJgJgCIgIAAQgRAAgJAHgAg8gsQAGABABAGQACAFgDAGIAIgIIAJgIQgCgGgJAAQgJAAgDAEg");
	this.shape_12.setTransform(-57.7,-1.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgDBWQgcgBgRgRQgRgSgBgaQAAgYAOgRQgDgCgFgFQgEgFgCgFQgCgHAAgGQAAgMALgNQALgNAUAAQAGAAAGABQANAEABAKIAHgMIAbAAQgGALgKALQgJAKgLAHQASgBAOAFQAnAOAAAuQAAAdgVATQgTARgdAAIgDAAgAgjgOQgLAMAAAQQAAAQALAKQAOAPAWAAQASAAANgKQAQgNAAgUQAAgJgDgIQgFgMgMgGQgLgGgPAAQgXAAgOAPgAgsg2QgFAIAAAGQAAAJAGADIAFgFIAegbQgCgFgIgBIgEAAQgOAAgIAMg");
	this.shape_13.setTransform(-73.8,-3.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_14.setTransform(-88.9,-1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgzBFQgVgUAAgdQAAgVAJgPQALgQAQAAQARAAACANQAGgPASAAQANAAAIALQAKALgBANQABARgMALQgKAMgQAAQgNAAgLgIQgLgIAAgNIADgUQACgIgJAAQgGAAgEAKQgEAHAAAKQAAAVARANQAOAMAWAAQAWAAAPgPQAQgPAAgWQAAgSgIgMQgIgOgQAAIhXAAIAAgNQAXABgBgCQAAgBgEgCQgFgDAAgEQAAgRATgGQAKgCAYAAQATAAALADQARAFAKAMQAJAKAAAOIgPAAIAAAAQgCgOgPgHQgOgHgTAAQgOAAgFACQgKADAAAIQAAACACACQAEACADAAIAnAAQAOAAAJAEIAIAFIAAAAIAAAAQAVAQAAAnQABAigVAWQgUAWggAAQgfAAgUgUgAgSADQABAHAFAFQAFAFAHABQAJAAAHgGQAFgFAAgJQgFAKgMAAQgNAAgIgNIgBAFgAAAgSQgCADAAADQAAADACADQACACADAAQADAAACgCQADgDAAgDQAAgDgDgDQgCgCgDAAQgDAAgCACgAAzgsIAAAAIAAAAgAAzgsIAAAAgAAzgsIAAAAgAAzgsIAAAAgAArgxQgJgEgOAAIgnAAQgDAAgEgCQgCgCAAgCQAAgIAKgDQAFgCAOAAQATAAAOAHQAPAHACAOIgIgFg");
	this.shape_15.setTransform(-104.3,-3.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgEBTQgcgBgRgSQgRgRgBgbQAAgRAHgNQAGgNAPgNQAKgIATgTQAKgMACgHIAbAAQgFAKgIAJQgHAIgMAIQASgBAOAFQAnAPAAAtQAAAdgVATQgUASgbAAIgEAAgAgkgSQgLANAAAQQAAAPAKALQAPAOAWAAQASAAAMgKQARgMAAgUQAAgJgDgIQgFgMgMgHQgLgGgPAAQgYAAgNAPg");
	this.shape_16.setTransform(-119,-2.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAIAAQAIAAAGgPQAFgPgBgRQABgRgFgPQgGgRgHAAQgIgBgFAEIAAgPQAHgFANAAQARAAAMAWQAKAUAAAYQAAAXgLATQgLAVgSAAQgLAAgIgEg");
	this.shape_17.setTransform(-129.3,-1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_18.setTransform(-140.3,-1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#C4291D","#E26D45"],[0,1],-293,0,293,0).s().p("EgrqAFoQg4AAgngnQgogoAAg4IAAnBQAAg4AogoQAngnA4AAMBXVAAAQA4AAAoAnQAnAoAAA4IAAHBQAAA4gnAoQgoAng4AAg");
	this.shape_19.setTransform(-2,-3);

	this.instance = new lib.Path_1_0();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,295,39);
	this.instance.alpha = 0.359;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-295,-39,590,78);


(lib.btn1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgNAAIAAgMIAiAAQAKAAAHAFQAHAGABALQAAAJgJAFQgKAFgNgBIAAAfgAgCgWIAAAQQAMAAABgIQAAgJgKAAIgDABg");
	this.shape.setTransform(170.2,-9.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_1.setTransform(164,-1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_2.setTransform(146.7,-1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgzBFQgVgUABgdQgBgVAKgPQAKgQAQAAQARAAADANQAFgPASAAQAMAAAJALQAKALAAANQAAARgLALQgMAMgPAAQgOAAgKgIQgLgIAAgNIADgUQACgIgJAAQgGAAgEAKQgEAHAAAKQAAAVARANQAPAMAVAAQAXAAAPgPQAPgPAAgWQAAgSgHgMQgJgOgQAAIhXAAIAAgNQAXABAAgCQAAgBgGgCQgEgDAAgEQAAgRATgGQAKgCAYAAQASAAAMADQARAFAKAMQAJAKAAAOIgPAAIAAAAQgCgOgPgHQgOgHgTAAQgOAAgFACQgKADAAAIQAAACADACQACACAEAAIAnAAQAOAAAJAEIAIAFIAAAAIAAAAQAWAQAAAnQAAAigVAWQgUAWggAAQgfAAgUgUgAgRADQgBAHAGAFQAFAFAIABQAIAAAHgGQAFgFAAgJQgFAKgMAAQgNAAgHgNIgBAFgAAAgSQgCADAAADQAAADACADQACACADAAQADAAADgCQACgDAAgDQAAgDgCgDQgDgCgDAAQgDAAgCACgAAzgsIAAAAIAAAAgAAzgsIAAAAgAAzgsIAAAAgAAzgsIAAAAgAArgxQgJgEgOAAIgnAAQgEAAgCgCQgDgCAAgCQAAgIAKgDQAFgCAOAAQATAAAOAHQAPAHACAOIgIgFg");
	this.shape_3.setTransform(131.3,-3.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAkBBQgWAAgLgPQgJAPgaAAQgSAAgNgOQgMgOAAgUQAAgVAOgKIgTAAIAAgPIATAAQgEgCAAgIQAAgDACgDQAGgTAcAAQAYAAAHAPQAKgPAXAAQANAAAJAFQAKAHABALQABAKgEAGIgDAAQAUAQgBAYQAAAUgMAPQgNAPgTAAIgBAAgAALAEIAAAOQAAAIAIAEQAHAEAKAAQAKAAAIgIQAIgIAAgLQAAgJgIgGQgHgHgKAAIhCAAQgLAAgIAHQgHAHgBAJQAAALAIAIQAHAHAMAAQAJAAAHgEQAJgEAAgIIAAgOgAAfgxQgIAAgFAGQgGAGAAAIIAiAAQADgDgBgGQgBgLgOAAIgCAAgAglgvQAEADABAGQAAAFgCAEIAcAAQgBgLgIgFQgGgEgHAAQgFAAgEACg");
	this.shape_4.setTransform(115.3,-1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAJAAQAHAAAGgPQAEgPABgRQgBgRgEgPQgGgRgHAAQgIgBgFAEIAAgPQAGgFAOAAQASAAALAWQAKAUAAAYQAAAXgKATQgNAVgRAAQgLAAgIgEg");
	this.shape_5.setTransform(96.1,-1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AATAyQANABALgHQAKgGAFgMQAEgNgBgMQgBgOgKgLQgKgMgPABQgYABgFAVQADgDAGAAQAHAAAFADQAOAKAAAWQgBAVgSANQgQAMgYAAQgYAAgPgOQgQgOAAgXQAAgXARgMQgFgBgDgFQgDgGAAgGQAAgLAKgIQAKgGAMAAQAMAAAIAHQAIAHADAMQAOgZAdgBQAYAAAQAUQAPAUAAAZQAAAcgSASQgSASgdAAgAg4gHQgHAGAAAJQAAANAMAHQALAHAQAAQANAAALgGQAKgGABgKQABgFgDgFQgCgEgFABQgDAAgDACQgCADAAAEIgPAAIAAgMIABgLIgTAAQgKAAgHAHgAg1gvQAEACADAGQACAGgCAFIAQAAQgBgKgFgFQgFgFgHAAIgFABg");
	this.shape_6.setTransform(84.5,-1.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgdA9IAAgPQAEADAIAAQAIAAAGgPQAFgPgBgRQABgRgFgPQgGgRgHAAQgIgBgFAEIAAgPQAHgFANAAQARAAAMAWQAKAUAAAYQAAAXgLATQgMAVgRAAQgLAAgIgEg");
	this.shape_7.setTransform(64.5,-1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_8.setTransform(52.6,-1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgkBNQgQgJgJgNQgNgTAAgeQAAgSALgRQALgQAPAAQAPABAEAKQAHgMAQAAQAOAAAKAKQAKALABAPQABAQgNAOQgNANgRAAQgQAAgLgKQgLgLABgOQAAgGACgEIAEgJQABgEgCgCQgCgBgEAAQgHABgFAJQgEAKAAAKQgBAWARAOQARAOAWAAQAWAAARgRQARgRgBgWQAAgdgOgRQgPgSgcAAQgjAAgOAZIgQAAQAHgUATgKQARgKAWAAQAjAAATATQAWAYACAlQABAlgVAZQgVAZgjAAQgTAAgQgHgAgWgGQABAHAGAFQAHAEAIAAQAJAAAIgFQAHgGAAgJIAAgEQgCAGgGAEQgGADgHAAQgSAAgDgQQgEAJAAACgAgDgYQAAAEACACQACADAEABQAEgBADgDQACgCAAgEQAAgDgDgDQgCgDgEAAQgIAAAAAJg");
	this.shape_9.setTransform(35.4,-2.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgVAQgJQgHgDAAgLQAAgFABgEQAJgQAagBQAWAAAIAOQAKgOAYAAQAMAAAKAIQAKAHAAAMQAAAJgEAGQAOAMAAAQQAAAbgTAQQgVASgkAAQgkgBgVgSgAg4ADQABAQATAIQAPAHAVgBQAWABAPgHQAUgIABgQQAAgHgFgIQgIAGgQAEQAHADAAAJQAAAIgLAEQgKAEgMAAQgOAAgKgEQgLgFAAgKQAAgPAcAAQAHAAAGgCQATgCADgDIhBAAQgXAAABASgAgMAHQAAAGANAAQAOAAAAgFQAAgGgOABQgNAAAAAEgAAQgsQgHAGAAAIIAlAAQABgCAAgEQAAgDgBgCQgEgIgLAAQgJAAgGAFgAgngwQAEADABAFQABAGgEAEIAdAAQAAgOgQgFIgHgBQgFAAgDACg");
	this.shape_10.setTransform(19.3,-1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgvBKQgVgNgCgXQgCgZAUgKQgFgBgEgDQgEgEAAgHIACgGQAHgRAhACQATABAKAOQAKANgBASIgsAAQgWAAABAQQAAANAQAGQANAFAUAAQAWAAAPgMQAOgLABgSQABgYgMgPQgKgMgRAAIgzAAQgZAAAAgRQAAgcA/AAQAXAAATAKQAVALABATIgRAAQAAgLgRgHQgOgFgSgBQggAAgBAIQgBAGALABIAqAAQARAAANALQAYASAAAqQAAAdgXATQgVASgeAAQgaABgSgLgAgjgRQAEADAAAEQABAGgEAEIAfAAQgCgJgGgFQgHgEgKAAIgHABg");
	this.shape_11.setTransform(4,-2.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgcBBQgXAAgOgPQgPgOgBgVQAAgTAGgOIAPAAQgBAFACACQABACAFAAQAIAAAFgDQANgIACgJIABgFQAAgLgHgDQgGgCgGACQAKADAAAMQAAAGgFAEQgEAEgHAAIgGgBQgMgDAAgPQAAgFACgEQAGgRAYAAQAJAAAHAEQAIAEACAIQANgRAbABQAcABAOAaQAKASgBAUQAAAsggAPQgKAFgLAAIgPgBIAAgLQgLAIgIACQgHACgMAAIgEAAgAAPgQQAPAMAAATQAAAVgNANIALABQALAAAGgFQAQgMAAgZQAAgQgLgNQgLgMgQgBQgVgBgIAMIABAAQAMAAAIAHgAhAAJQAAAKAGAHQAIALASAAIALgBQAMgBAIgJQAHgIAAgLQAAgGgDgEQgDgGgLgBQgFAAgIAFIgOAHQgHADgHAAQgHAAgEgDIgBAHg");
	this.shape_12.setTransform(-20,-1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAqAeQALgHAAgKIgBgEQgCgHgQgEQgPgDgTAAQgSAAgOAEQgRADgCAHIgBAEQAAALAKAGIgOAAQgMgEAAgSQAAgGACgDQAHgQAUgHQAPgFAYAAQA4AAALAcQACADAAAGQAAARgMAFg");
	this.shape_13.setTransform(-36.7,-9.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhLAlQgEgMADgJIgMAAIAAgNIAKgKIAMgMQgIAAgFgFQgFgGAAgHQAAgMAKgIQAIgHANAAQAUgCAJAOQASgMAbAAIAJAAQAaABARAVQARAUgCAZQgBAagRASQgRASgaAAIgJAAIAAgNQgQANgaAAIgDAAQgoAAgIgcgAgHglQAHAAAIAFQAIAFAFAFQAMAOABAQQAAAZgNAQIAFABQAMAAAIgGQARgLABgVQACgPgHgMQgHgNgOgGQgKgGgSAAQgKAAgHADgAgsgRQgKAHgPAOIAKAAQgEAJADAKQAGARAbgBQAPgBALgJQAJgJABgOQABgMgJgJQgIgJgNAAIgCAAQgMAAgKAHgAg2gxQgEABgCACQAGACACADQADAEgCAFIAHgGIAHgFQgEgEgFgCIgDAAIgFAAg");
	this.shape_14.setTransform(-37.7,-1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_15.setTransform(-54.6,-2.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_16.setTransform(-70.5,-1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("Ag1BFQgWgQABgcQABgYAUgIQgKgDAAgMQAAgNAOgHQALgFAOABQASABAKAOQALAOAAARIgtAAQgKAAgHAFQgHAGABAKQABAXAeAGQAJACANAAQAaAAAPgRQAPgPAAgYQABgagPgSQgQgTgegBQghAAgRAYIgRAAQAIgSATgKQASgKAXAAQAigBAWAYQAWAZAAAiQAAAjgUAXQgVAagigBQghAAgUgOgAgjgeQADADABAEQACAFgCAFIAbAAQgBgHgFgFQgHgGgJAAQgFAAgEABg");
	this.shape_17.setTransform(-94.3,-2.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAAAzQgKAOgSAAQgZAAgNgSQgLgRABgdQAAgfAVgTQAPgNATgCQAIgBAFACIAAAPQgIgDgNAEQgNAEgIAMQgIAMAAAPQAAAmAbAAQAKAAAHgFQAHgGAAgKIAAgNIAPAAIAAANQAAAKAHAGQAGAFAKAAQAMAAAIgIQAIgIAAgMQAAgLgIgHQgJgIgMAAIglAAQgBgXAKgLQAJgKATAAQAjAAAAAcIgBAGIgMAAQAMAFAHAMQAHALAAAMQAAAZgMAPQgMAPgWAAQgWAAgKgOgAALgiIAWAAIAJABIABgEQAAgMgPAAQgQAAgBAPg");
	this.shape_18.setTransform(-110.2,-1.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Ag7AdQgJgEAAgRQAAgGACgDQAJgbA6AAQAcAAAPAFQAWAGACARQACAMgIAGQgIAHgMAAQgKAAgHgFQgIgGAAgJQAAgGACgEIgSgBQgsAAgHAOIgBAFQAAAKAJAGgAAiACQAAADACADQADACAEAAQAIAAAAgIQAAgDgCgCQgDgDgEAAQgIAAAAAIg");
	this.shape_19.setTransform(-126.4,-9.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgcBBQgZAAgPgUQgNgRAAgaQAAgfAPgRQAHgIAIgEQALgGAMAAIALAAIAAAPQARgQAZABQAbAAAPATQAOAQABAcQAAAbgPATQgQAUgbAAIgJAAIAAgOQgLAIgGADQgJADgNAAIgEAAgAASAxIAFABQAJAAACgGIABgDIgFgPQgCAMgKALgAAtATQAAADACAGQADAHAAAFQAKgKAAgNQAAgGgIAAQgHAAAAAIgAgxgnQgKALgBAOQgCATAJANQAKAPATABQANAAAKgGQAKgHABgLQABgGgEgFQgEgGgFAAQgGABgDADQgEADAAAGIABAGIgPAAQgEgQACgOQACgPALgPIgHgBQgOAAgKAKgAAJgVQATAHACAQQAEgGAKgCQAJgBAEAEQgCgNgJgJQgKgKgQgBQgMgBgIAFQgLAFgDALQAEgGALAAIAIABg");
	this.shape_20.setTransform(-126.8,-1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQAfIAAgaIgMAAIAAgLIAMAAIAAgRIgMAAIAAgMIAhAAQAKAAAHAFQAHAGAAALQABAJgKAFQgIAFgNgBIAAAfgAgBgWIAAAQQAMAAgBgIQABgJgKAAIgCABg");
	this.shape_21.setTransform(-138.2,-9.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AADBQQgMgGAAgMQgBgKAIgFQgcABgSgPQgTgQACgWQACgcAWgNQgEAAgCgFQgDgEAAgFIABgFQAFgLAMgEQAJgDARAAQAeAAANAQQAKAMABASIgvAAQgOAAgJAIQgKAJgBANQgBAPANAMQANAKAQABQAaABATgKIAAAQIgSAGQgQAEgBAHQAAAFAEADQAFADAGAAQAIAAAFgEQAFgEAAgHIAAgGIAPAAIAAAHQAAAOgLAJQgLAIgPABIgGAAQgLAAgJgEgAgUhCIADACQACACABADQAAAFgEACIAHAAIAmAAQgFgKgMgEQgGgCgJAAQgKABgFABg");
	this.shape_22.setTransform(-141.2,0.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgpAvQgTgTAAgbQAAgdATgSQASgSAcAAQAdAAAOAMQAKAJABAMIgQAAQAAgIgMgFQgLgFgOAAQgSAAgOAMQgOANAAASQAAARANALQANAMASAAQAOAAALgIQAMgIAAgPQAAgRgRgFQAHAGAAAMQAAAHgIAHQgIAGgKAAQgLAAgHgHQgJgHABgMQAAgMAKgIQAJgHAOAAQAUAAAPAMQAOAMAAATQAAAYgRAQQgRAQgXAAQgbAAgSgSgAgDgPQgCADAAAFQAAAFACACQADADAEAAQAFAAADgDQADgCAAgFQAAgEgDgDQgDgEgEAAQgFAAgDADg");
	this.shape_23.setTransform(-154.4,-1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("Ag/AMQgFgJAAgIQAAgIAEgGIAPAAQgDAEAAAEQAAAFAEAFQANANAjAAQAUAAAPgGQAIgEAFgDQAFgFAAgIQAAgJgHgHIAKgIQANAKAAATQAAAJgDAFQgNAig1AAQgvAAgQgbg");
	this.shape_24.setTransform(-169,6.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag5AvQgSgQAAgaQAAgKAFgJQAFgLAHgEQgIgEAAgJIABgHQAHgQAcAAQAWAAAIANQAJgNAZAAQALAAAJAGQAKAFABAKQABAIgEAHQAGAFAEAJQAEAKAAAKQAAAbgTAQQgVASgkAAQgkgBgVgSgAg0gOQgGAHABAKQABAQATAIQAPAHAWgBQAVABAPgHQAUgIABgQQAAgKgGgHQgGgGgKgBIhHAAQgKABgGAGgAAQguQgGAEgBAHIAkAAIAAgEQAAgEgEgEQgFgCgFAAIgCgBQgHAAgGAEgAgogxQADADABAEQABAEgCADIAbAAQgBgLgQgDIgHgBQgDAAgDABg");
	this.shape_25.setTransform(-169.1,-1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["#C4291D","#E26D45"],[0,1],-293,0,293,0).s().p("EgrqAFoQg4AAgngoQgognAAg4IAAnBQAAg4AogoQAngnA4AAMBXVAAAQA4AAAoAnQAnAoAAA4IAAHBQAAA4gnAnQgoAog4AAg");
	this.shape_26.setTransform(-2,-3);

	this.instance = new lib.Path_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,295,39);
	this.instance.alpha = 0.359;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-295,-39,590,78);


(lib.slider2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		
		var clip = this;
		var count = 3; // Define the number of buttons |Sanka|
		
		
		
		this.home_btn.visible = false;
		this.slider_mov.visible = false;
		
		
		
		
		for (var i=1; i<=count; i++){
			//var btn = clip["btn"+i];
			//btn.mouseEnabled= false;
		}
		
		
		
		
		
		var btn = []; //Define the array switchyes
		
		for (var i=1; i<=count; i++){
			var btnn = clip["btn"+i];
			btnn.name = i;
			btn[i-1] = clip["btn"+i];
			btn[i-1].addEventListener("click", btn_clk.bind(this));
		}
		
		
		function btn_clk(evt)
		{
		
			for (var i=1; i<=count; i++){
			var btn = clip["btn"+i];
			btn.mouseEnabled= false;
			}
			this.home_btn.visible = true;
			var item = evt.currentTarget.name;  // Selecting (current target) button name |Sanka|
		
			//var mov = clip["mov"+item];
			this.slider_mov.visible = true;
		
			this.slider_mov.gotoAndStop(item-1);
			
			
			
		
		}
		
		this.home_btn.addEventListener("click", close_clk.bind(this));
		
		function close_clk()
		{
			this.slider_mov.visible = false;
			for (var i=1; i<=count; i++)
			{
				var btn = clip["btn"+i];
				btn.mouseEnabled= true;
			}
			
			this.home_btn.visible = false;
			
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// close
	this.home_btn = new lib.close();
	this.home_btn.parent = this;
	this.home_btn.setTransform(437.4,224.5);
	new cjs.ButtonHelper(this.home_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.home_btn).wait(1));

	// slider
	this.slider_mov = new lib.mov2();
	this.slider_mov.parent = this;
	this.slider_mov.setTransform(-0.5,4);

	this.timeline.addTween(cjs.Tween.get(this.slider_mov).wait(1));

	// btn
	this.btn3 = new lib.btnb3();
	this.btn3.parent = this;
	this.btn3.setTransform(192,174.6);
	new cjs.ButtonHelper(this.btn3, 0, 1, 1);

	this.btn2 = new lib.btnb2();
	this.btn2.parent = this;
	this.btn2.setTransform(211,24);
	new cjs.ButtonHelper(this.btn2, 0, 1, 1);

	this.btn1 = new lib.btnb1();
	this.btn1.parent = this;
	this.btn1.setTransform(286.5,9.6);
	new cjs.ButtonHelper(this.btn1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.btn1},{t:this.btn2},{t:this.btn3}]}).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#689F38").s().p("AAQFVQgPgIgVAAQgUAAgLgFQgKgFgQAAQgTAAgMgFQgOgHgNgQQgNgOAAgJQgBgEAEgDQADgCAIAAQAFAAADgEQAFgHACgOQACgOgFgFQgJgHgUgWQgagbgDgIQgCgGgFgFQgKgNAEgLQAEgLAAgEQABgFgDgFIgEgHQgHgKAEgJQAFgHAPgGIAKgFQAKgFACgGQABgEgCgCIgEgHQgJgOAAgHQAAgDADgDQAEgDgCgLQAAgJgGgKIgKgPIgHgJQgJgNADgIQADgGAMgCQAHgCAHgDQAEgDALgCQAJgCANAEQAJACACgCQACgEAAgCQABgFACgBQADgDAJgDIAHgDQAPgFABgEQACgEgGgJIgGgJQgJgNgDgHQgFgKgBgNQgCgIACgLQABgEgBgBIgFgFQgdgVgGgOQgCgGAAgGQAAgFgCgEQgCgFgHgDQgJgHgDgGIgBgDQgDgFgCgCIgKgGQgLgFgHgKQgHgKACgKIAIABQgCAHAGAHQAFAJAJAEIANAIQACACAEAHIABACQACAEAHAFQAJAHADAFQADAGAAAIQAAAEACAEQAEALAcAVQAGAEACAEQABADgBAGQgCAKABAHQACAMAEAIIALATIAGAKQAJANgDAHQgDAIgSAFIgIADQgJADAAACQgBAHgDADQgDAEgGABIgKgCQgLgDgHABIgNAFQgJAEgIABQgHABgBADQgCADAHALIAGAIIALAQQAGALABAMQACAOgGAFIgBABQAAAEAIANIAEAIQADAFgCAGQgDAJgNAHIgKAFQgMAFgEAFQgCAFAFAGIAFAIQADAHgBAGQgBAGgEALQgCAIAIAKQAEAFADAGQAFANAzAxQAIAHgCASQgCAPgHALQgFAHgKgBQgEgBgCADQgBADAMAOQAMAPAOAGQAJAFASAAQASAAAMAFQAJAFASAAQAZABAPAIQAMAHAIgBQAFgBAJgGIANgHIANgEIALACQAHADAMACQAKACAIgDIANgIQAKgIALgEQAXgHAZAAIAAAIQgYAAgVAHQgIACgMAJQgJAGgGADQgKADgMgCIgTgFIgJgCQgDAAgHADQgFACgHAFQgMAHgIAAQgJAAgNgGg");
	this.shape.setTransform(245,98.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#689F38").s().p("ABdAqIgBgCQgKgQgEgFQgJgHgKADQgaAFgHAJQgEAGgHACQgFACgLAAQg4AAgGgEQgDgBgHgNQgJgRgGgEQgTgLgXgDQgbgDgMgKQgNgKgBgJIgKAAIgKACIgOAHIgDgHIANgHQADgCALgBQAKgBAEADQADABAAADQAAAGANAJQAJAIAZADQAaAEAUANQAIAFAJARIAHALQAEADA2AAIAOgBQAEgBADgEQAIgLAdgHQAPgEAKAKQAHAGAJARIACACQAFAIAGABQAFAAAIgCIADgBQACgDgEgLQgKgcAKgMQAMgRAtgNIAlgLIACAHIgkAMQgrAMgKAOQgJALAJAWQAGANgDAHQgCADgEABIgCABQgIADgFAAQgNAAgIgNg");
	this.shape_1.setTransform(197.5,173.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#689F38").s().p("AAdAlQgOAAgSgXQgJgJgKgCQgTgDgGgDQgGgDADgLIABgHQgBgEgEAAIAAgIQAKAAACAKQACADgDAJIgBAEQAIADAPADQAHABAEACQAFACAGAJQARATALAAIAZAAIAAAIg");
	this.shape_2.setTransform(363.6,155.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#689F38").s().p("AmULaQAfgLAEAAQAFAAAUgMQASgKAggKQAFgCACgDQACgEgDgHIgGgGQgIgFgBgGQgDgIAJgNIABgCQAEgHAJAAQAFAAAKgIQAFgEAAgIIgBgDQAAgFAEgCQAFgEANAEQAGACAKgDIASgFQAPgDAfAGIALACQAKABADAJQABAFAAAKIAAADQAAABAAABQAAABABAAQAAABAAAAQAAAAAAAAQACACAHgDQAHgCAEgJIADgGQACgEAFgCQAKgBANALQAGAEAQAbIAOAYQAIAOAFAEQADADAEAAQACgBAFABIACgBQADgDADgIQACgHAAgGQAAgLAOAAIAJgBQAEgBAQgBQASgBAGgCQAdgJAYgPQAIgEAGgLIAGgJQAHgFAIAFIAFAEQAFADAFgBQAEgBAHgEQAHgDAGAAIAOADIAFABQAJADAFgCQADgBAEgGQADgCAAgKIAAgIQAAgFADgEQACgEABgHQACgIgGgKIgEgIQgEgJAQgLQAHgEAEgEQAfgeAUgOQANgJANgCIAKgCQAPgCAMgIIAEgCQAGgDAKgDIgBgGQgCgFgCgkIAAgLQgEg4AEgoIADgjQAFg+AEgeQAGgmACgtQABgpgCgZQgDgUgLgJQgEgDAAgDQAAgDACgGIABgKIgDgXQgCgJADgLQACgJAAgGQAAgLgDgDQgCgCgKgBQgtgHgPgIQgLgFgVgTIgGgGQgHgGgDgRQgCgOgEgIQgFgKABgFQAAgEAFgFIADgDIABgGQABgKAHgHQARgRALgPQAEgFgCgJQgCgJgGgFQgGgGgRgkIgJgUQgEgIgGgFQgGgEgFgJQgFgMgBgGQgBgFAAgJIAAgDQABgIgFgHIgDgEQgCgEgCgOIgCgNIgFgLIgDgGQgDgJALgGIADgCQACgCAAgDQABgEgDgEQgEgEgQAIIgPANQgaAXgLAHIgQAJQgJAEgCACQgCADgCAJIgCAMQgDAIgMACQgDABgDACIgEAGQgEAKgIAHQgHAGgKAEIgDABIgCAGQgEAMAAADIgEA3IgBAEQgEADgJgCIgKAAIADAFIAEAFIgGACQgMADgKgDQgKgDABgLIAAgHQgBgGgIgDIgFgDQgFgEABgJQABgFgCgFQgDgOgSgcQgFgIgCgBIgDADQAAABAAAAQgBABAAAAQAAABgBAAQAAAAgBABQgDAEgFAAQgHAAgNgOQgHgIgGgZQgEgSgEgHQgJgTAEgdQAAgHAEgMQAEgOAAgEQAAgVAIgTQAEgKAAgFQAAgFgEgKIgEgOQgCgGgCgDQgFgGgQABIgDAAQgFAAgKAHQgLAHgHAHQgFAGgJAFIgMAJQgFADgFAHIgJAKQgGAFgHABQgMACgJgEQgGgCgKgIQgIgIgWgeIgFgHQgHgKABgYQABgIgBgEQgBgCgJgEQgJgEgMgDIACgHQAiAIADAKQABAFgBAJQgBAWAFAHIAGAHQAUAeAJAHQAIAHAFACQAHADAJgCQAJgBAJgMIALgMIANgIIANgKQAHgIAKgHQAOgJAHAAIADAAQAUAAAHAIQAEAGACAHIADAMQAEALAAAHQAAAHgEALQgHAQAAAVQAAAEgEARQgEALgBAFQgCAdAHAQQAEAIAEASQAFAWAGAHQAMAMADAAIADgBIACgEQAFgGAEAAQAGAAAIANQASAdAEAPIABANQgBAFACAAIAEADQALAFABAJIABAKIAAAEIABABQAIACAHgBQgCgGABgDQACgEAFAAIALABIAGAAIADg1QAAgFAFgNIABgFQAAgEAIgDQAKgDAFgFQAGgFAEgJQADgHADgCQACgCAHgCQAIgCABgEIADgKQACgMADgEQADgEAKgEIAQgIIAkgeIAQgNQAVgNAIAKQAGAGgBAHQgBAIgEADIgEADQgGACABACIADAGIAFANQACAEABAKQABALACAEIACAEQAHAIgBALIAAAEIAAALQABAGAFAKQAFAIAEADQAHAGAFAJIAJAUQAPAiAGAGQAJAHACAMQACAMgFAIQgOARgQAQQgFAFAAAGQAAAGgDAEQgBADgDABQgCADAAACQgBAEAEAHQAFAJACAOQACAPAFAEIAGAGQAVASAJAFQAOAHAsAHQANABAEAFQAFAFAAAOQAAAGgCALQgDAJACAIIACAQIABAFQABADgDAMIgBAGIABABQAOALACAYQADAYgBApQgCAugGAoQgGAlgDA2IgDAjQgFAkAEA6IABALIADAoIAEAOIgEABQgJABgJAFIgEACQgNAIgQADIgLACQgKABgNAJQgTANgeAdQgHAGgFADQgLAHABAEIAEAIQAHANgCAJQgBAHgDAHIgCAGIAAAHQAAANgFAFQgGAGgEACQgHADgMgDIgFgBIgNgDQgEAAgEACIgNAGQgIABgHgEIgJgGIgDACIgFAGQgGAMgKAGQgZAPgeAJQgIADgSABQgLAAgGACIgMABQgGAAAAADQAAAHgDAJQgDAKgFAEQgEAEgFgCIgFAAQgGABgFgEQgGgFgKgQIgOgYQgPgZgFgEQgKgJgGABIgCACIgDAFQgGANgJADQgLAEgFgEQgFgDAAgIIAAgDIgBgMQgBgEgFgBIgMgCQgdgGgOADIgRAEIgKADQgFABgGgCQgIgEgCACIgBABIAAADQABALgIAHQgMAKgIAAQgFAAgBADIgCACQgGALABAEQABADAFADQAHAFACAFQAEALgDAGQgDAHgJACQgkANgMAHQgVANgIAAQgDAAgeAKg");
	this.shape_3.setTransform(319.4,109.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#689F38").s().p("AGEM8IgMgDQgUgFgIgDQgIgDgNgNQgMgMgHgCQgKgCgLAAQgMAAgFAEQgJAGgegGQgLgBgJgMIgDgEQgFgGgZgHIgRgGIgdgIIgPgEIgDAAIAAgbIgJAAQgHABgEgBQgGgCADgLQADgKgCgDQgCgEgVAHIgJACIgTAAIgEAAIADgUQAAgHAIgJQAGgJgGgIQgDgDgQAEQgLADgGAAIgUgCIgWAAQgCABgQAVIgHAKQgHAJgcAAIgCAAQgEAAgDgCQgBgCAAgGIAAgDQgBgIgRgDQgngGgRgLQgIgFgSgOQgRgOgJgJQgNgNggg9IgaguQgUgXgDgFQgBgFACgEQAFgHALABIACAAIgGgQQgDgKgcgRQgJgFgIgPQgHgLgEgKQgDgJgcghIgGgHQgIgJgCgKQgCgGAAgOQAAgIgIgTIgNgiQgJgZACgFQADgGAMABQAIAAADgBIABgCQABgEgEgJQgEgJgGgGQgHgJgDgPQgBgNACgIQABgHAQgMQAPgLAFgCQACgCgBgNIAAAAIABgBQABgJgBgDQgDgEACgOIAAgDQAAgEgFgDQgDgCACgHIABgFIgMABQgQADgDgEQgEgEgLguQgLguABgIQABgOAqg1IACgDQAAgCgGgCIgIgCQgGAAgEgDQgCgBgDADQgDADgJAAQgKAAgDgDQgDgDgDgWIgCgaQAAgOAYgBIACgBQAHAAAFgOQABgDABgPIACgPIALgiIAIADIgMAiIgBAMQgBASgCADQgGASgMABIgCABQgRABAAAGIACAaQACARACADQABABAGgBQAFAAABgBQAHgIAJAFIAPAFQAKADABAGQACAFgFAFQgIAJgPAWQgSAZAAAHQgBAHAKAqQAKAoAEAJIALgCQASgCACAFQACADgBAIIgBACQAHAGAAAGIAAAEIAAAOQAEAGgEAKQACATgIADQgFACgNAKQgOALgBAEQgBAHABALQACANAGAHQAHAHAEAKQAFALgBAHQgCAGgEABQgFADgJgBQgIAAAAABQgBACAJAVIAHATIAGAPQAIAWAAAIIABASQACAIAHAIIAGAHQAdAiAEAKQAKAcAOAJQAfAUAEALIACAGQAFANgDAFQgDADgFAAQgHgBgCADQABAFAUAYIAbAvQAhA9AKAKQAKAKAQANQARAOAIAFQAOAIAoAIQANACAFAEQAFAEABAHIAAAGIACAAQAYAAAFgGIAHgKQARgYAGgBIAXABIAUABQAGAAAJgDQAWgFAFAHQAJANgJANQgGAIAAAEQAAAFgCAHIAOAAIAHgCQAbgJAFALQADAHgDALIgBAEIAGgBQANAAADADQAAABABAAQAAABAAAAQABABAAABQAAAAAAABIAAAUQAZAGARAGIAQAFQAcAJAGAHIADAEQAIAJAHACQAbAFAHgFQAHgEAOgBQANAAALADQAIACAOAOQANAMAFABIAbAIIANAEIAFABIAAgEQAAgHgEgMQgIgWgOghIgRgjIgJgEQgSgGgJgJIgJgIQgKgIgDgGQgGgLgCgVIgGg2QgGg0ABgHQABgGAKgNIAGgJIADgIIAMgeIASgvQACgEAAgGQgBgIAIgJQAHgIAGgPQAEgNABgHIAAgEIgDABIgIAEQgTALgGAAQgEAAgFADQgEADgEAAQgHAAgDgIIAAAAIgCgBQgTgDgEgIQgCgGAAgEIgDgHQgJgNgCAAIgDgEIADgCIAJgGQAKgFAJAAQAPAAABgCIAAgJIgDgKQgDgEgIgFIgPgMQgDgBgHgGQgLgJADgLIAAgCIgCgGIgBgEIABgFIADgNQAAgGADgIQgDgCgBgCQAAgFAEgDIARgJIAAgEQAAgHgGgMIAAgBQgLgYgCgPQgBgHgDgDQgDgCgHgCIgIgBQgRgBgDgGQgCgFAHgIQADgEAPgCQANgBAHgEQALgdAPgHQAPgHAlgMQAMgDAIgKQAFgGAHgPIAIgOQAHgNATgRIAJgKQAEgJACgKQACgPgEgHQgHgNAEgJQACgHAGgBIgFgFQgJgCgCgGQgDgHAFgJIAEgFQAEgHABgEQAAgEgEgGQgRgXAEgOQACgHAQgCIAMgCIAFgBQAJgDAGgGQAFgFAEgDQAGgDAEgFQAQgRgDgHIgFgHQgbgigCggQgBgUgDgZQgDgbAAgLIADghQAFg1gFgNIgEgMQgHgPgBgHQgCgLADgMQACgIADgHQAEgKABgSQACgQAGgNQAGgNACgKQADgNAFgLQAGgSgBgPQgCgaAAgUIAIAAIABAtQACASgHATIgHAWQgEAPgFAKQgFALgCAQQgCATgEALQgCAFgCAJQgDAKACAKIAHAUIAFAMQAFAOgFA4QgDAUAAAMQAAAKADAbIAEAtQABATAKASQAFAKALAPIAGAIQAGAMgVAWQgGAHgFACIgHAGQgFAGgEACIgKADIgRAEQgMABAAADQgDAJAOAUQAGAJAAAIQgBAEgFAJIgEAFQgDAGABADQABACAFACQAEABADAEQAEAEAAADQAAAGgFABQgBAAgBABQAAAAgBABQAAAAAAABQgBAAAAABQgCAFAFAKQAGAKgEAUIgFASQgBACgLALQgSARgHALIgHAOQgIAQgFAGQgJALgOAEQggAKgUAJQgGAEgHAMQgGAKgDAIIAAABIgCABQgHAEgRACQgLABgCACIgEAFQACACALAAIAKABQAJADAEADQAGAFABAKQABANALAXIAAACQAMAZgIAHIgIAEIgJAFIABAAIAFABIgCAFQgEAHAAAHQAAAGgDAJIgBADIABACQACAFAAADIgBADQgBAIAHAFIAGAEIADgBIABADIAKAJIAEADQAKAHADAEQADAEACAKQABAKgCAEQgEAGgTAAQgIAAgLAGIAJAMQADAGAAAEIACAIQADAFAOABIAGACQACABABADQAAABAAABQABAAAAABQAAAAAAAAQABAAAAAAIAEgCQAHgDAFgBQAEAAASgJIAJgFQAHgCAEACQADACAAAJQgBAIgFAOQgGAQgIAJQgGAHABAEQAAAIgCAGIgSAvQgEALgIATIgEAIQgCAFgFAGQgJAMAAADQgBAIALBnQADAUAEAIQADAGAJAHIAJAIQAIAIARAGQALADACAEIARAkQAOAhAIAWQAJAZgHAGQgCADgEAAQgDAAgEgCg");
	this.shape_4.setTransform(241.3,108.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#689F38").s().p("Ag7EWQgFgKgNgHQgMgHgEgIQAAgCgGgBQgJgBgTAIQgQAIgEAEQgXAcgagOQgMgGgKgLQgHgIgJgBQgNgBgMALIgGAEIgKAJIgHAHQgGAFgVAEIgDABIgHAEQgKAKgLAAQgTAAgJgCQgHgCghABIAAgJIASABIAXABQAJACASAAQAHAAAJgHQAGgGAEgBIAEAAQATgGAEgDIAHgGIALgKIAFgDQAPgNAQABQAMACAJAKQAJAKAKAFQAWALASgXQAFgFASgJQAUgIAMABQAIABAEAFQACAGALAGQAPAIAGANQANAaAYAAQASABAcgcQAMgMAggNQAYgKAYgGQAPgEAEgJQADgGAAgLIAAgEIgCgIQgDgLABgFQACgJALgEQAOgFAVACQAUADAGAJIACADQAQAWAJAEQAHABAOgFIAIgDIgBgCQgDgJABgFQACgOAbgNQAFgCAVAJIAMAGIACAAQADgDAEgPIAAgEQACgEALgEIAIgCQABAAAAgMQgBgIABgIQACgNAXgLIADgCQAFgBAAgFQAAgDgCgIIgCgMQAAgQgLgTIgDgFIgFgKQgCgHAFgFIAMgJQAKgGAAgCIAAgCQABgFgBgKIAAgKQABgNgFgLQgGgOgRgPIgFgEQgDgFABgHQADgLAGgEQACgCgBgMQgBgLgCgDQgBgCgEgEIgJgLQgFgHADgMIABgGQABgDgGgHIgIgJQgDgFgFgMIgEgIIgEgDQgNgPgBgJQgBgHABgKIAAgJIAIABQgBAPABAJQABAIALALIAEAFIAFAJQADAIAFAHIAHAIQAIAKgBAGIgBAIQgDAIAEAEIAIALIAGAHQADAFABANQABAQgGAEQgEADgBAHQgBAFACAAIADAEQATAPAGARQAGAOgBANQgBAGABADQABALgBAGIAAACQgBAHgNAHIgLAIQgDADAHAIIADAGQAMAUAAATIACAKQACAKAAAFQgBAHgIAEIgDACQgUAKgBAIIgBAOQABARgGADIgJADIgHACIgBAEQgDASgHAEQgEACgEgBIgOgGQgRgHgCAAQgJAFgHAFQgIAGAAAFQgBADADAHQACAHgCADQgCACgDABIgHACQgQAHgKgDQgMgEgRgZIgCgDQgEgGgSgCQgSgDgMAFQgHADgBAEQgBADACAJIACALIABADQAAAOgDAHQgGANgTAEQgZAHgXAJQgeANgLALQgdAdgWAAQgdABgPggg");
	this.shape_5.setTransform(200.6,43.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#689F38").s().p("AJqLIIgVgJIgVgIQgJgDgSgNQgOgLgHgHQgSgRgagEQgKgBgSABIgRABIgKAAQgnAAgOAEQgPAEgZgBQgZgBgOgGQgIgCgCAAQgCABgDAGIgHAMQgUAZgMgGIgFgDQgKgHgFAAQgFAAgIAHQgHAGgEABQgOAHgOgHIgLAAQgPgBgDgEQgIgIgEgbQgBgKAAgeIAAgiQACgPAQAAQAGABAJgEQAIgDAEgFQAFgHABhFQABgcAJgZQAHgXAKgHQAGgEADgHQAEgEgCgCQAAAAAAgBQgBAAAAAAQAAAAgBAAQgBgBAAAAIgIgBQgIgEgFgGIgCgCQgDgEgCAAQgCACAAAEQgBAKgEAFIgFAEIgCABIgBgBIgHgCQgHgEgDgFQgCgCgFgPQgFgTgEgCQgHgFgnAPQgLAEgOAOQgHAHgIAFQgHAGgLADQgIADgDADQgEAEgCAJIgCANQgFARgUAXQgHAIgNAWQgCAOgIAGQgGAEgLgDIgJgEQgHgBgeAPIgUAJQgUAIgPgBIgCAAQgEgBgIgFQgKgFgDgIQgEgHADgfQABgVgBgBQgBgEgHgHQgGgFgCgEQgCgEgCgRIgCgPQgCgGgZgKIgDgBQgIgEgCgKQgBgFAAgMIAAgHQAAgEgFgFQgEgGgBgDQgBgFADgKIACgGQACgEgKgHQgPgIgNgDQgGgCgHgGQgFgHgJgGIgSgMQgegWgSgHQgxgSgYgQQgLgHgUABIgHAAQgLAAgIgKQgGgGgFgEQgOgKgmgGQgCAAgJgGIgbgRIg/goQgEgCgIAKIgDADQgEAEgIABQgIABgHgGQgHgGgJgaQgJgZAAgNQAAgWgJgcQgDgGgIgNQgNgVAAgFQAAgQAMgIIAFgDIAPgLIAEgHIAJgQIABgBQAHgRAEgFQADgDAJgEIAHgCQADgCAMgaIAEgHQAFgLAAgQQAAgDgFgMIAHgEQAGANAAAGQAAARgGANIgDAHQgPAcgDADIgJAEIgJAEQgDAFgHAPIAAABIgKASIgEAGQgEAGgPAIIgEADQgIAEAAANQAAADALATQALAQABAFQAJAcAAAYQAAAMAKAYQAJAYAFAEQADAEAGgBQAEAAADgDIACgCQANgOAIAEIBAAnQARALALAIIAHAFQAoAFAQAMIAMALQAGAHAHAAIAHAAIARABQAKABAJAGQAWAPAwARQASAHAgAXIASANQAKAGAGAHQAEAFAEABQAIABALAGIAMAFQAPAJgDALIgCAHQgDAGABADIAEAHQAGAIAAAFIAAAIQgBAUAHAEIADAAQAdANADAIIACARQABAPACADIAHAHQAIAJACAEQABAFgBAWQgDAdADAFQACAFAHAFQAGADAEABIABAAQAQAAAQgHIAUgIQAhgQAJACIAKADQAHACADgCQAFgDACgMIAAgBIAHgLQAHgMAHgJQATgVAEgRIACgMQADgLAFgGQAEgDALgFQAJgDAHgEIAOgMQARgQAKgEQArgQAKAIQAGADAHAWQACAKADAEQAFAGAGACQAEgDABgIQAAgKAHgDQAHgCAHAIIACACQAEAFAGACIAFABQAGAAAEAGQAEAFgFAIQgEAIgJAHQgHAGgHATQgJAXAAAcQgBBIgHAJQgFAFgKAFQgKAFgIgBQgJgBgBAJIAAAhQAAAdABAKQADAZAHAGQABABALABIAOABQAMAFAKgFIAJgGQAJgJAJAAQAJAAAKAIIAFADQAGADAPgVIAHgKQAFgIAEgCQAFgCALAEQAMAEAYACQAYABANgDQASgGAnABIAJAAIAQgBQAQgCAOACQAdAFATATQAgAcANADQAFABARAHIAWAKQAMAGACgBQADgBACgGIAPgoQAHgNgBgEIgCgHIgCgLQAAgFACgFIADgFIAAgBQACgGgBgDQgBgGgJgIQgSgPARg9IAGgqQAEgiACgJQADgPALgJQANgMATABIAcAEQAYAEABgCIACgEQAEgKAAgGQABgKgGgKIgNgPQgJgKgDgGQgFgJAAgMQAAgSADgfQADgfADgOQAJgjgBgfQgBgRACgWIACgWQAAgRgCgJQgVAAgGgDQgFgCgCACQAAAEgFAHIgBADIAAACQAAAKgEAGQgEAHgKAEQgHADgFgBIgEAGQgDADgBAEIgCAGQgCAFAAAIIAAADQAAANgCAFQgDALgJAFQgJAEgKABQgLADgHgEQgGgCgCgFQgCgFAHgOQAIgLgBgIIAAgDQgBgQgDgJQgFgPgMgKQgKgIgMgHQgMgIgFgGQgGgIgCAAIgFAGQgIALgJgDIgEgBQghgNgMgBQgVAAgOgLIgEgDQgEgEgDgBQgDAAgJADIgKAFIgLAEQgFABgEgDQgHgDgcg1QgagSgEgUQgHgtATgeIAPgWQAPgaAGgGIAJgGQAFgDABgDQgBgEgGgEIgDAAQgJAAgCgIIgBgFQAAgCgFgEQgFgCgEAAIgIgFQgFgDAAgIIAAgNQABgMgEgHIgKgNQgTgXAAgMQAAgKgDgNQgDgOgEgIIgFgIQgHgLACgHIAFgIQAGgGABgDQADgHgCgKQgCgKgOggQgRgngBgOQgCgRAKgWQAHgRAKgLQADgDAAgKQgCgTgQgNQgJgIgkgXQghgUgEgFQgIgHgigEIABgIQAlAEAJAKQAHAGAdARQAlAXAJAJQAUAQABAWQABALgGAHQgJAKgHAQQgJAUACAPQABAKAQAoQAOAiADAKQACANgDAJQgDAFgGAHQgDACgBADQAAACAFAJIAFAJQAFAIADAOQADAOAAAMQAAAJARAVIALAOQAGAKgCAPIgBALQABAEACACIAGACQAGABAEADQAJAFAAAHIABADQAAABAAAAQAAAAABABQAAAAABAAQABAAABAAIAGABQAJAGACAHQABADgCADQgCAEgHAEIgIAGQgEAEgQAZIgOAYQgSAbAHApQACANANALIAMAKIABAAIABACQAZAxAHAEQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAIgEIALgFQALgEAGAAQAFABAGAFIADADQAMAKASAAQAPABAhAOIAEABQADABAFgHIACgDQAFgGAEAAQAGAAAIALQAEAFAMAHQAOAJAIAIQAOALAFAQQADAJACASIAAADQABAKgJAOQgFAKABACIADADQAEACAKgBQAJgCAIgEQAGgDACgHQABgFAAgLIAAgDQAAgKADgGIACgFQABgFAEgHQAGgGAEgBIAJgCQAHgCADgFQADgFAAgHIABgFIABgDIAEgJQAAgEAEgCQAGgEAJAFQADACAVgBIABAAQAEAAACAEQACAHAAAXQAAAMgCALQgCAWABAPQABAdgJAoQgDANgDAgQgDAeAAARQAAAKAEAIIALAOIANAQQANATgLAXIgBAFQgCAGgKAAQgGAAgRgDQgQgDgKgBQgQAAgKAJQgJAHgEANIgFAqIgGArQgQA4APANQAKAIACAJQABAGgCAHIgBABIgDAGIgBAHIACAIIACAIQABAHgIAQIgDAHIgMAgQgDAJgGACIgEABQgGAAgLgGg");
	this.shape_6.setTransform(248.6,-38.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#689F38").s().p("AmrEDQgFgCgIgIQgIgIgFgIQgDgHgpAEQgRABgDgCIgIgCQgcgIgDgIIAHgDQACAEAYAHQAIACACACIAPgBQAYgCAKABQAPACADAGQAEAHAJAIQAGAGADABIABgFQABgGAIgGQAGgFABgCQACgFAAghIABgkIgEgcQgEgbABgDQABgDADgBQAJgDAQAHQAcANALgCIApgLQBVgWAKgBQALgCAGAEQADAEAAAMIABAQQAEAWAfAQQANAGAGAjIACAFQACAIAbAHIAEABQAKADAIgGIAEgEQAIgLAMgFQAOgHAIgJIACgCQgCgCAAgDIACgIIADgPQAAgHALgMIADgCIAAgBQAAgFADgHQACgEASgRQATgRAGgFQAIgEAOABQAOACAFAHQADAGALAEQAMAFAJgBQASgDAQgNQAGgEB4h7QACgBACgIQABgHgCgGQgCgGgNgPQgNgPgEgCIgHgDQgGgEgGAAQgJgCgDgHQgCgGADgDQADgFANACIAFABIA5AAQAIAAASgHIAWgHQARgEA8glIAAgKQAAgKAcgFIAKgCQAkgHATgCQAWgCAMgIQAHgEACgDIAHADQgDAFgHAFQgPAKgXACQgZACgeAHIgLACQgSADgDAEIAAAPIgBABQhBAngRAEIgVAHQgVAHgIAAIgSAAQgeABgJgBIgGgBQgIgBgBABIAAABQABADAGABQAHABAIAEIAFADQAGADAOAQQAOAQACAHQADAIgCAJQgBALgEADQh1B4gJAHQgTAPgTACQgMACgNgGQgOgFgEgIQgDgEgMgBQgLgBgFADQgGAEgRAQQgRAQgCAEIgDAJIAAADIgEAFQgJAJAAAEQAAAFgEANIgCAFQAFAGgFAGQgKALgQAHQgJAEgIAKIgFAFQgKAIgPgEIgFgBQgOgDgHgDQgMgGgBgJIgBgFQgHgfgIgEQgjgTgFgZIgBgRQAAgIgBgCQgDgBgHAAQgNAChQAWIgsALQgLACgggOQgPgHgDADQAAADAEAXIAEAdIgBAkQAAAhgCAHQgCAFgHAGQgHAFAAACQgBAJgFACIgDABg");
	this.shape_7.setTransform(222.7,-89.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgJACQACgIAIgBQAIgBABAIQgCAIgJAAQgIAAAAgGg");
	this.shape_8.setTransform(197.5,-186.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgEAQQgGgGAAgKIABgQQAAAAABgBQAAAAAAgBQABAAAAgBQABAAAAgBQADgBADAAQAFACAAAHIABALQAAAGAEAEQAEAFgHAFQgEADgDAAQgDAAgBgGg");
	this.shape_9.setTransform(151.7,-187.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAAAmIgHgIIgJgHQgWABgCgWQgBgFgFgSQgHgeAeAGQADABAHAEQAHADACAHIAGAJQAKAKANgHQALgGAEAKQAEAIABAPQABASACAHQACAFgHAEQgPAEgHAAQgNAAgIgJg");
	this.shape_10.setTransform(141.8,-177);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgRARQgFgGABgHIAFgLQAUgSASAGIAAAIQgBAFgHgBQgFgBgBAHQgBAEgEADIgHAEIgDADIgEAJQgCgEgEgBg");
	this.shape_11.setTransform(153.1,-194.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgGABQABgIAGAAQAHAAAAAGQAAAIgIABQgHAAABgHg");
	this.shape_12.setTransform(154.6,-42.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgEAFIgBgIQgCgGAGAAQAGABACAKQACAIgHAAQgFAAgBgFg");
	this.shape_13.setTransform(153.9,-39.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgDABQgEgFACgFQADgGAFAFQAIAIgJAPIgFgMg");
	this.shape_14.setTransform(152.5,-35.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgCAIQgIgGAGgKIAGABQAEAAABAFQACAEgFAFQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAAAIgDgBg");
	this.shape_15.setTransform(165.2,-113);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AADA3QgCgfgMgcQgGgKAEgLQAEgKAAgVQAAgOAJAGQAHAGAAAJQABAGgEAKQgEALAFAPQAKAeABAfIAAAIQgBAFgFAAIAAAAQgGAAgBgMg");
	this.shape_16.setTransform(152.5,-58.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AACASIgEgJQgFgCgCgFQgEgGAEgIQADgHAFAAQAFABABAHIABAMQABAIADAFQADAEgFACQgEAAgCgCg");
	this.shape_17.setTransform(153.6,-48.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AADALQgMgBgBgMQAAgJAJAAQAKAAACAOQABAEgDACQgCADgDAAIgBgBg");
	this.shape_18.setTransform(178.9,-172.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAyBjQgFgHAEgEIAJgHQgVAHgNgPQgSgJgFABQgEABgLAOQgEAEgFAAIgKgBQgFgCgDgFIgGgKIgEgIQgDgFgGACQgBALgCADQgEAGgJgBIABgLQABgGgFgEIAAgGIAGABQAGAFADgBQgEgMAGgKQgQgBgDgEQgEgEACgQIABgGQgBgDgFgBQgIgCADgIQACgGAHgBQAMgBAKAFQACgIgGgDIgMgEIABAAQgEgDAAgCIAEgFQAGgGgBgIIgEgPIgDgIQgCgFAFgEQADgDAFABIAIACIAMAAQAGgBADgFQAFgIAKAEQAEACgDAGQgHAIgBAMIAAAVQgBASgDAGQgEAHgPAEIAAAAIABABQAeABACAfQAEgQAQAAQARAAAFANIAEANQACARAFAEQAEADARABIAKADQADABABAFQABAFADABIACgEQAIgJAJAFQAHAEgDAMIABAIQAAAFgDADIgHAHQgJACgJAAQgIAAgJgCg");
	this.shape_19.setTransform(168.9,-196.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgIAUIgFgIQgCgEACgEQADgEAEACIAHAEIgBgGQgBgDgDgCQgEgCABgEQABgFADgCIAFgFQAFgCADAGIgBAGIgBAEQAAAAAAABQABAAAAABQAAAAABABQABAAAAAAQAHACgFAIQgFAIgGgFIABAGIAAAHQAAADgDABIgCABQgDAAgDgFg");
	this.shape_20.setTransform(167.6,-109.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAKATIgCgGQgEgHgEAAIgLAEQgJADgCgFQgBgCAAgIQABgEAGgIQAGgLAJAEQAIADAAAHQAAAKAJACQAKAEgDAJIgCAGQgBADgEAAIgBAAQgDAAgCgEg");
	this.shape_21.setTransform(175.5,-151.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgIATQADgHgCgQQAAgJAHgFQACgBAEACQAGAFgFAJQgCADAAAGQACANgMAAIgDAAg");
	this.shape_22.setTransform(156.3,-198.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgNAeIgIgIQgFgEgHADQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQAAAAABAAQACgDgDgMQgDgIAKgEQADgBAAgPQAAgSAMANIAGAHQADADAGgBQAFAAACAGQABAIACADQADgYAYADIABAFQAKAMgJAHQgCANgCADIgBAMQgBAHgIACQgIACgHAAQgMAAgJgHg");
	this.shape_23.setTransform(157.7,-186);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("ABeBVQgCgBABgEIgUgHQgMgFgDgLQgKADAAgJQgGABgCgFIgDgIQgEgMgFgFQgHgHgKgBQgGAAgDgEQgKgNgcgKQgggLgKgIQgGgEgKAAIgSACQgMADgDgJIgFgPQgEgHABgEQABgDAGgFQAAgFAEgCIAHgDQAQgEAPAGQARAGARgEQASgEAQASIAJAEQAFABAEgCQAOgFANAFQAMAFAJANQAMADAKAJIAPATIAOAJIAGACQACACgBAEQAFgCgCAFQAGgDgDAGQAMgBABALQAFgCAEADQADADgBAEQAAAFgFACQgEACgFgCIgfgOQAHASATAJQAIADADADQAEAFgBAGQgBAJgPABQgFAAgEAEIgGAIIgGgBg");
	this.shape_24.setTransform(154.7,-122.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("EgDXAiTQgJAAgMgKQgJgIgPAAQgSAAgIgBQgQABgLgDIgEAEIggAAQgegDgFgYQgFgVgTAJQgUALgXgEQgRgCgYgMQgugVgZgDQgTgCgfgPQgNgHAIgJQAFgGgFgEQgIgHgEAEQgHAGgGgDIgLgHIgtghIgJgGQgFgDgBgFIgngpQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgFgCgFgJQgJgUgbgtQgYgpgKgZQgLgZACgSQABgIgCgQIgJgYQgEgNADgMQAAgCgCgEQgDgEACgCQgHAAgCgFIgDgJIgKgbQgFgRgBgMQgCgZgPgSQgFgHgCgKIAAgSIgEgPQAAgIAKgEQAEgBgCgGIg2ifQgNgmgfhLQgZhEgFgyQgBgRgPgXQgOgUAPgRQAHgIAEgNIAFgXIAAhHQgDgSgCgEQgLgagJgmIgQhCQgBgFAAgSQABgLALADQAEAJgBAEQgBARAKAaQANAfACAKIACAFQAPgigFgZQgFgbgegSQgEgJADgFQAMgYgBgfQgBgUgIgiIgRhCQgJgngCgcQAAgHgGgYQgFgTACgMQAAgCgFgEIgGgHQgRghAFggQADgPgFgXQgDgPAOgHQAIgEACgEQACgEgGgIIgCgMQAAgMgDgZQgCgVAFgPIgDgJQgFgDgBgHIABgKQACgogXgmQgNgWgHgrQgGgRgHglQgDgRgKgaIgQgqQgIgbgDg5QgCgZgHgPQgLgZAOgbQAHgPgFgHQgFgIgBgNIgCgXQgDgMAMAAQAPABADgRQAJgtAYgcQADgEACgGIACgLQADgSAKgHQAEgGAHAFQAEAHgDAHQgBAEgGAHQgEAEgDALQgCALgEAEIgEACQgEANAJAIQAFAFgCAAIgGAAQgBgHgDABQgFACAFAHIACAIQABAFAHgCQAFgCACAHQAEAKgIAQQgHAPgLAEQgKARAMAWQAAgMAKgBQANgBACgDQAGADgCACIgGAJQgEAFgHgCIgDAAQAAAAAAAAQgBABAAAAQAAAAABABQAAAAAAABQgHAFgHAMIgLARQgHAIADAMQADANAAAEIgBAXQAAAMAGAJQAMABACAMQABAJgLABQAAAAgBAAQgBABAAAAQgBABAAAAQAAABAAABIABAFQACAHABANIACAVQADAOAJAGQAGAFAOACQAJACANgEQATgFgCAVIgDAYQgCAOADAMIAThiQACgPgTgNQgKgGgJgOIgPgYQgCgEACgDQADgDAEACQAHAFAEgDQACgCADgHQAIgUARgTQAPgQgPgNQgEgEgEgIIgGgOIAAgEIAAgCQAKgHgDgGQgCgDgIgHIgLgNQgGgIgCgIQgCgHACgDQACgFAIAAQAGAAABgFIAAgJQABgeASgMQADgCABgDQAGgWAUgJQAAgGgCABQgMAJgCgHQgBgDACgKQADgTgBgcIAAguIgBgNQABgHAHgDIgBgCIABgCQALgGAFgPIAHgZQgBgJgCgGIgBgCIAAgDQgFgLAEgGQACgRAOgIQAFgEAWgIQALgEADgEQAGgGgBgMQgCgSAVgaQALgNgBgRQAAgHACgNQADgNgBgGQAAgNADgbQAAgXgKgQQgSgcACgXQABgFgCgKQgCgLgCgFIgBgKQgBgDACgIQgBgGgJAAQgEAAAAgRQAAgQAFgCQAEgCADACQADACAAAEIABAYQAIgOABgHQACgKgHgKQgHgJAEgHQALgIAEgMQACgIABgRQgLgEgFAEIAAgGQAIgIAEgBQAGgBAFALIAKgRQAFgIADgCQAFgEAIAGQAHAFAIgGQAIgHgBgJQgBgfAcgLIACgCQAQAEAKgMIAOgWIAGgEQAMgSABgcIAEgaQADgQAAgKQAAgKAHgCQAJgBADgGQACgEAAgKQgBgMALgOQALgPADgVQABgNgCgZIgBgPQgBgKgDgGQgDgFADgHIAHgLIAFgMQgJAAgJgIIgOgOIgIgFQgEgCgFADQgFAEgHgEIgLgFQgHAAABgHIADgMIABgYQAAgPADgJQAEgLADgBQAGgCAagQQAUgMARAFQAIACAGgFIAKgKQAAgGAIgDIAMgIQAHgEAEgIIAIgTQADgKgDgKQgIgEgGgKIgJgSQgRgCgCgRIgUgEIgHgCQgEgCACgFQgFACACgGQgCADgEgEQgPgUgagIIgNgGIgJgGQgFgFADgGQAEgLAQAGQAHADABgBQADgBAAgHQABgIAKAFQgBgEgIgGQgHgFAAgGQgMgFgjgEQgSgCgTgeIgIgMQgGgGgJgBQgDAAgEgFIgIgIQgCAAgCAGQgCAFgEgCQgFAHgEAAQgDAAgHgHQgKgFADgKQACgMgGgMIgPgUQgIgLgBgDQgDgJAIgIIABAAQAJgKAFgDQAIgGAKAAQgCgIAIABQADgOATgBQAKAAASgFQATgFAIgBQAdgDAMAAQAWAAASAIQAGADAJAAIAQgCQARgCAAAOQgBAIAEALIAGASQAAAFAFADIAJAEQAEACABgEIABgGQADgFALACQAKACAFAHQAFAHADABQAEABAIgFQAHgFAOAEQANADAEAHQAKAEAGALIAJAUQAPAHACAQQAFgCgCAFQAWAFAHAYQAFgCgCAFQANABAGAPQAFgBgCAFQAUADAGAQQASAEAIAPQAJABAMAIQANALAFACIAPAHQABAAABABQABAAAAgBQABAAAAAAQAAgBAAAAIAAgFQAAAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgHgCADgEIgTgLQgKgHgGgIQgUgEgGgPQgFABACgFQgKACAEgKQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAIgBABQgEgEgYgRQgRgNgHgMIgEgEIgCgBIgGgLIgGgGQgTgMgFgLQgEABgDgCQgDgCgBgDQgBgGgJgJQgJgIAAgIQgBgGgFAAQgGgBgFAHQgIANgJgMIgcgHQgRgDgIgKQgDgEgKgBQgPgCAAgRQAAgPAFgEQAFgFAPADQAiAHAmgFQAJgCAVAFQAKABgCAIQASAOAIAYQADgBACADIABAEIAFAIQAJACgEAHQAJAFAHANIANAVIAHAIQAEAFgBAGQAMACABAOQAFgDgCAGQAEgBgBAEQAFgBgCAFQAOADACANQAFgDACAGIAJAEQAEADAAAGQAFgBgCAEQAFgCgCAFQAFgBgCAFQAFgCgBAFQAFgCgCAFQAWAGAKAUQAGgBAHAFIAJAIIAZARQAPAKAJAJQAMgBABALQAHgCgBAIQAPAHAEAJQAHAAAFAEQADACAFAHQAHACAJALQABAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIAAAAIAAAAQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAABIACAEQABAAABAAQAAAAABAAQAAABAAAAQAAABAAABIAKAGQAFAEABAGQAJgCADAKIAMAJQAWABAVAWIADADQAGACAJAMQAIAKAIACQAGADAAABQABAIAHAEIANAGIAGAEIAHAGQAJAFADAJIAHAJIADAEIAHAJIALAKQAIACAIALQAFADABAIQACAHAIAKQAKALACAFQAKAKAHACQAKADAAAEQABAKALALQAMAMACAHQAHATAQAPQAJAKACAPQABAPAHAWIAMAlIgBAAQAEACAAAEQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABIAFAFIADAGQABADgDADQgEADgGgHIgEgFQgFgEgJgLQgIgIgLADIgIACQgEACACAEIADALQADAFAHgBQAUgCAGANQACADACAAIAFgBQAVABAAAQQAAAUAVASQAJAHAAADQAAAFgPACQAOAGALAQQAOAUAGAFIgDADQgdgTgNgLQgHgFgCABQgDABgEAHQgGAMgBAGQAAAIAKAHIAIAFQAEABAFgDQADgDAEAAQAEAAAAAFQgBAGADAJIAFAPQACALgHAAQgKgDgEABQAGARAAAGQAAAMASAJIADAAIACgBQABgJAJgCIAPgDQADgWgGgOQgCgGACgCQADgCAGADIANAFQAGAEADAGQACALANAOQAOAPADAIIAHAOIAJAOQADAFgCACQAZASASAYQAGgJAJABQAKACAFALQANAbAOAfIAFANQADAHAGAEQAGAFgDAFQgBADgJgBIgHgBQgEAAgBAEQgBAFAMACIAKAFQAFACAEgKQABgEAGADQAFACACAEIATAcQADAFgDAEQgEAEgGgFIgLgKQgHgGgIABQAAAJACACQADAFAIABQAQACAIAVIABAEQABADACABQACgCgBgDIgCgFIABgHQAAgDAEgBQAGgCAFAIQAFAIADAIQAIAbARAJQAMAHAAAKQAAAHgFANQgGAkATAZQALANgBARIgBAKQgCAFgGABQgGABgDgGIgEgKQgCgDACgEQADgGgBgCIgIgBQgQgBgCACQgCACACARIABAQQABAJADAGQADAFgBAFQgCAHgHACIgIABQgFABABAEQACARAIAGQAVAPALAEQATAHATgGQAEgBAIACQAIAAACgJQAEgQgEgKIAAgCQgCgGAQgRQAQgRAHABQAMABgBAKQgBAGAEAJIAGAOQADgGgBgHQgBgKAGAAQAFAAADADQADACgBAEQgCAEAGAMQAEAKgHAGIACAEQAPARAGAbIAIAwQAEAYAIAKIADADQAJAIgGAFQgEAEgFgDQgDgBgFgFQgQgUgCgTQgBgGgDgGQgFgIgJADQgGADAFAKIAJAQIAFAHQACAEgEAEQgDAEAEAGQAFAGgBADQALgDACABQACABgBALQgBAEACACQAFABACgDQAEgJAEADQACABADAHQAGAOAAAJQACArAWBAQACAFAAAIQgBAIABAEQAFgCADAEIACAGIAHAcIABAGIACADQAUANAHAZIAHAsQAIArAHARQADAHgBACIgBACIAJAFIALAFQAHADADAFIABAAQAEABABAHQACAHADgBQAMAAAMgIQAIgFAFAFQAGAGgFAHQgIAIgDANIgFAXIAEADIAEgCQACgKAIAAQAHAAACAKQABAPAOAAQAEAAACADQADADgBAEQgBAIgIAAQgIgBgCAFIgDAMQgDAUAMARQAEAHAVAWQAIAIAIgIQAEgEAEABQADACAAAFQgBALAFAOIAJAXQAGgCgDAFQAFgDAGAGQAJAKARAEQAYAHAEACQAOAHAGAIIAEAGQAIAWAQAZQA0A9APA6QAKAaALA6QAKA3ALAcIAIAoQADAQAMAdQAKAXAEAOQAFAVgCASQAAABAAAAQgBAAAAABQAAAAABABQAAAAAAAAQAMAKgEASQgFAWACAGQgLAOAAAXQADAZgBANQAAAFgEADIgHADIAHAHIABAFQADADAAAFIAAAZIAAAPQAAAJgDAGQgGANAFANIALAXQAFALABASIACAcQACAIgHAAIgMgBIABASQAAALAHAHQAFAFgBAHQgBAIgLACQgEAAAAADIAAAGQABAPgHARIgQAeIgDAEQgBACABADQALATgKAXIAAAEQAGAFgCAEIgBACQgGAMgFAEQgJAHgBAMIADAWQACALgEALQgCAGgJAOIgMARIgDAEQAAAAAAABQAAAAAAABQABABAAAAQAAABABAAQAJAHgDAFQgCAEgGgCQgGgCgDADQAGAJgEAFQgDAEgLABQgKAKABAMQABAJAHAOQAJAPgRAFQABAFgEAFQgGAFABADQgFAFAAAIIgCALQgCAHgJgHIgIAAQgCABAEAGIAEAIQAAAEgIAAQAHAMAAADQAAAHgPAAQgEAAgCAEQgDAFACAEQAEAPgGAPQgEAKgMAPQgNARgWAMQgHADgBAEQgBAPgMAJIgYAOIgNAJQgGAGgCAIQgCAHgFgEQgBAHgGAEIgMAGQgTAMgHAMQgHALgOAEIgaAGIgRADQgNADAHAJQAEAGgFAEIgZAXQgPANgQADQgFABgEAEIgGAIIgJAMQgGAFgJgDQgEgBgBAFQAAANgIAJQgHAGgNAGIgQAHQgjAMgOATQgLAQgUACQgMACgWAHQgXAIgLACQgRAEgdAPIgJAEQgGABgDgGQgDgHANgHIAFgEQgCAAgGgFQgFgEgEAFIgJAJQgFAGAHAHQADAEgCAEQgCADgEACIgYAJIgRAFQgJADgGAKQgIAPglACIgpAAQgZAAgQAHQgKAEgOAJIgYANIgLAFQgGACgHgCQgMgFgUAJQgLAFgRAFIgdAHQABAFgFgCQAAATgSgFIgRgEIgCAAIgLADIgLAHQgGAEgDAEQgTAcglgBQgDAVgEAEQgEAEgWAEQgBAAAAAAQgBAAgBABQAAAAAAAAQgBABAAAAQgHAPgQADIgdAFQgcAGgKAEQgUAGgPALQgKAIgJAAIgCAAgACcfUIgCAHQABADADABQAGAAAEgFIAHgKIgLgCIAAAAQgGAAgCAGgASiPuQABAHgDAHIAMgHQACgHgGgFIgKgIIAEANgASWNxQgDANAIALQAJAMgEAMQgBAEACACQABABAFgDQAFgGAAgIIgBgPIgGgQQgEgHgJAAIgCAAgASjMJIgPAKQgJAGAFAMQABADgEACQgFABgBACQAKADACAJQABAFgBALQAHACADgEIAEgJQADgJADgMIAEgWQgDgLgDAAIgCABgARZHKQAEAEgCAJQgBAJAGANQAHAPAAAHQAAAFAEgBQADgBABgEQAAgFgCgGIgDgJIgEgQQgBgIAFgHQAHgPABgEQAAgEgFgJQgCABgCAGQgCAEgEABQgEAAgCgEIgDgHQgRgLALgOQAEgFgGgDQAAAAAAAAQAAABgBAAQAAAAAAAAQgBAAAAAAIgDAAIgBABIgBAAQgDAEgIAAQgJAAgDAEQAGAFAGAKIALARQADAFgFAFQgGAFAAADIgBAGQABACAFgDIAGgHQADgCADAAQADAAACADgAw8GLQAJAUABAHQAFgggWgZQgCANAJARgARIF9QAEABAHALQAFgBgBgEIgBgHQgEgGAAgJIAAgQQAAgJgCgDQgDgEgJAGIgPAFIAFADQAIACAEAIQADAGgCAGQgCAHgGgDIgKgFIgEACIAGAQQALgGAFAAIABAAgAQWD/QAHABADAKQABAEAEAFIAHAJIAHAGQAFAEACAGQADAHgDACQgJAEAEADIAGAFIAHADQAFACAEgBQgCgEgJggQgGgVgPgJQgZgogNgwQgDgLgLgPQgBAGADAIIAFAMQADAKgGAEQgHAGgGgJIgEAHQgCAFgFAAQgIAAgGgLQgDgFgCgLQgCgLgCgFIgDgDIgUgLQgOgHgHgGIgHgDQgMgHgHADQgGADgGAMIgEARQgCAKgEAGQAHgBAJAEIAOAGIAJACQAEgBgBgIIgCgHQAAgEAEgDQAIgEAHAJQAIADALABQAHABAEAEQAEAEgDAGQgCAFABADQABABAFACQAGADAAAGIAAAAQAJASALAHQAIAFAFAHQAGAIgDAFQgHALAHAMQAIAQAAAEQAFgGgCgJIgDgKQAAgGAHgCQAHgDAEAAIADABgAxTAuIAAAGQAEAGAAAIIAAAOQgBAXAQANQAEAEABgBQAEAAAAgGIABgPQgBgIgCgHQgIgWgCglIgDgVQgLAYgCATgAMrgkQgDAMADAIIADARQACALAIAEQADADACAGIADAJQAHAJgCAEQgBAEgLACIgIABQgEABgCAFIAUADQAKABAFgGQAJgLgQgWIgCgCQgRgNgBgYIgDgpQAAAEgFAPgALDkBQAAAJAFADQAVALgIAYQgDAFAHADQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAAAQgBgHAEgRQAEgPgDgJIgCABQgCAJgGAAQgEAAgGgIQgCgDgDgJQgDgHgHgCQAAgMgJgYQgIgYABgNQgDAAgFACQgEABgCAAQgHgCgBAEQAAACACAGIAAAMQADABAAAEIAAAFIADAIQABAFgEADQgEAEgEgDIgHgFQgMgGgCAIQgCAFAEACIAIACIA0APQACAEABALgAyWmUQgCAIAEAAQAEABADgEIAFgHIADgGIgGgCQgHAAgEAKgAIvqHQAAAAgBABQAAAAgBABQAAAAAAABQAAAAAAABIACADQAMAHAEAGQAIgCgCgMQgFACgGgGQgDgDgEAAIgEABgAGSqqQgLgPAIgHIAQgJQAFgGAJAGQANAIADAAIABgLQAAgHgBAAQgFgCgIgFIgNgIIgCAAIgDACQgKAOgYASQgFADAAAEQAAAEAGADIAVAIIAAAAgAG1tzQAAAIAFAAQAGAAAAgIQABgNgMgWgADfyYQAJAFADAAQADAAAGgFQgHgEgDAAIgCAAQgEAAgFAEgAgt5QQgDADAAAFIAGAjQAQADASAKIAdAUQADgJgGgOQgHgPACgIIgDgBQgHABgDgFIgHgLQgHgNgXgDIgCAAQgDAAgDACgAhB54QAJAPAPAEQAGABABgCQABgCgDgFIgHgFQgFgDAAgEQgEABABgFQgJAAACgJQgDAAgCgCIgEgFIgIgEQgEgCgEABQABgEgDgFIgFgIQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAAAIABAAIgKgIQgCAJAGAIIANANIAJAFQAHAGALAUgAh163QgBgGgCgGQgCgHgDgBQgGgBgKgEIgPgGQgGgBAAADIADAGQARARAZAGIAAAAgAlV8oQAKAEASANQARANAMACQAOACAJALQAQATAPgGQAWgJAPAFQAFABABgEQADgDgEgFQgHgHgEgDQgHgEgJABQgSADgLgQQgFABgCgEIgGAAQgDgBgCgCQgEgBgBgDIAAgGQgLgIgSgHIgggKIgMgEQgHgDgEgEQgNgNgOAOIAIAFQAFADgCAFQgCAFgFABQgFABgDgDQgKgKgPAEQgPAGgIAAQgNACgIgEQgJgEgGgMQgPgBAAgOIgTgLIgEgCQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgFABACgEQgFABACgFQgEACgDgDQgUgVghgLQgFgDAAgCQgJACAAgJQgFADgCgGIgrgWQgGgEABgDQgLABgCgLQgCACgDgBIgFgDIgFgCQgEgBgCACQgEADADAHQAEALgLAGQAEAFALACQAKACAEAGIAEAGQABADgDACQgEAFgGgHQgJgKgNAGQgGADgDAAQgFAAgBgIQgGAAADgFQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAQgFgBgggTQghgTgDgEIABgDIgKABQgCgJAIgJQAIgKABgGQAEgGAQgFQAEgBABgCIgBgFQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAAAIgEABQgKAAgHAIQgIAKgFADQgOAKAHAQQAAAAABAAQAAAAAAABQAAAAAAABQAAABAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQgDAGAGAGQAHAHAAADIAGAGQADAEAEAAIARADQAIAEADAJQAHgDACAJQABAHgIAAQgUABgHgLQgCACgFgCQgFgDgDADQgBAFADAFIAFAIQALgJAEAKQAGANADABIAEADQACADACAAQATAEAUAXQAKAIAHACQAKADALgGQAEgCAFABQAGACABAEQADAHAFACQACACAJABQANADAGAIQAHALANAHQAIAFARAGIAIADQAEACAAAFQABAFgGABIgIACIAAAEQACAFgEACQgBACgGABQgFAAgBAEQArAUAhgXQAHgFAEAHQAGAJAGgCIAMgIQAEgDASgcQANgUAVADIABgDQAEgHAFACQACAAAFAGIAHAIQAEAEAFgCQAKgFAKAAQAHAAAHADgEgLlggEQgFADACADIAFAFQACACAAAEIABAGIATgMIgGgEQgEgBgBgDQgBgFgFAAQgCAAgFACgEgLOggUIgDAFQAAAAAAABQAAAAABAAQAAAAABAAQABAAABAAQAJgBAEgHIAUgJQAMgDALAHQACACgCgIIgDgDQgGgKgLAEQgOAGgDgBQgEAAgCAFQgCAEABAGIgDAAQgFAAgFACgAhT6SIAAAAg");
	this.shape_25.setTransform(264.8,7.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#C92B2B").s().p("AjWDXIAAmtIGtAAIAAGtg");
	this.shape_26.setTransform(66,149.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#824A4A").ss(8).p("EAdOAlvMg6bAAAMAAAhLdMA6bAAAg");
	this.shape_27.setTransform(253.5,8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("A7zAKIAAgTMA3nAAAIAAATg");
	this.shape_28.setTransform(-261.5,-86);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFCC80").s().p("AgJANQgGgFAAgIQAAgEADgFQAFgGAHAAQAGAAAEADQAGAFAAAHQAAAGgDAEQgFAGgIAAQgEAAgFgDg");
	this.shape_29.setTransform(-174.4,-9.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_30.setTransform(-185,-13.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFCC80").s().p("AAlAbQAKgHgBgJIAAgDQgCgGgOgEQgNgDgRAAQgQABgNADQgOADgCAGIgBADQAAAKAJAGIgMAAQgLgFAAgPQAAgFACgDQAGgOARgGQANgFAWABQAxgBALAZQABADAAAFQAAAPgLAFg");
	this.shape_31.setTransform(-206.3,-20.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_32.setTransform(-206.9,-13.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgJQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgDIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_33.setTransform(-218.6,-13.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFCC80").s().p("AARAsQALAAAJgFQAKgFADgLQAFgMgBgKQgBgNgJgJQgJgLgNABQgVABgFATQAEgDAEAAQAHAAAEADQAMAIAAAUQAAASgRAMQgOAKgVAAQgVAAgOgMQgNgNAAgUQAAgUAPgLQgEgBgDgEQgCgFgBgFQABgKAIgHQAJgFAKAAQALAAAGAGQAIAGACAKQANgWAaAAQAVgBAOASQANARAAAXQAAAYgQAQQgQAQgZAAgAgygHQgGAGAAAIQAAALALAHQAKAFANAAQANAAAJgFQAIgFACgJQAAgFgCgEQgDgDgEABQgCAAgDABQgCADAAAEIgMAAIAAgLIAAgKIgQAAQgJAAgHAGgAgugpQADACACAFQACAFgCAEIAOAAQgBgJgEgEQgEgEgGAAIgEABg");
	this.shape_34.setTransform(-230.1,-13.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_35.setTransform(-252,-13.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFCC80").s().p("AAlAbQAJgHAAgJIAAgDQgCgGgPgEQgMgDgRAAQgQABgMADQgPADgCAGIgBADQABAKAIAGIgMAAQgLgFAAgPQAAgFACgDQAGgOARgGQANgFAWABQAxgBALAZQABADAAAFQAAAPgLAFg");
	this.shape_36.setTransform(-266.2,-20.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_37.setTransform(-266.9,-13.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgJQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgDIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_38.setTransform(-278.5,-13.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgFAAQgKAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_39.setTransform(-287.5,-12.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFCC80").s().p("AgrA0QgUgRAAgZQAAgYASgNQgCgMAFgJQAKgTAVACQAKABAGAHQAHgJAPABQANAAAIAJQAIAKAAAWIg4AAIgXABQAAAPAKAKQALAKAOgBQALgBAJgGQAIgGACgJIAOAAQgEAQgNAKQgNAKgSAAQggABgOgYIgBAIQAAARAOAMQANAKAQAAQAaAAANgQQAIgJgBgMIAOAAQABAPgKAOQgSAagkAAQgZAAgTgPgAgUgvQgDAEAAAHQAEgBAGAAIAMAAQAAgPgLAAQgFAAgDAFgAAMglIAXAAQgBgPgKAAQgMAAAAAPg");
	this.shape_40.setTransform(-307.3,-12.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFCC80").s().p("AgUAUQgIgHgDgLIgBgFQAAgOAJgIIAAAOIAFAAQAAAIAGAFQAGAGAHAAQAIAAAGgHQAFgGgBgIIAOAAIAAAGQAAAOgJAKQgKAJgOAAQgLAAgJgGg");
	this.shape_41.setTransform(-317.4,-6.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_42.setTransform(-322,-13.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_43.setTransform(-344.2,-13.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_44.setTransform(-359,-13.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFCC80").s().p("AgOAcIAAgYIgKAAIAAgJIAKAAIAAgQIgLAAIAAgKIAdAAQAJABAHAEQAGAFAAAJQAAAJgIAEQgIAEgLgBIAAAbgAgBgUIAAAPQAKAAAAgIQAAgHgJAAIgBAAg");
	this.shape_45.setTransform(-369.4,-21.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFCC80").s().p("AgZA1IAAgNQADADAIAAQAGAAAFgNQAFgNAAgQQgBgPgDgNQgGgOgGAAQgHgBgEAEIAAgOQAFgEANAAQAPAAAKATQAIASABAUQgBAWgIAQQgLASgPABQgKAAgHgFg");
	this.shape_46.setTransform(-369.8,-13.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFCC80").s().p("AgwAyQgSgWAAgdQABgfATgUQATgUAfABQAbAAASARQASARAAAbQAAAUgMAQQgNAQgUABQgUAAgMgNQgMgNACgRQABgRANgKIgPAAIAAgOIAzAAIAAAOIgJAAQgKAAgHAGQgFAGgBAJQAAAIAGAHQAHAGALAAQANgBAHgKQAIgIgBgNQgBgRgIgKQgMgOgZAAQgVAAgMASQgLAPAAAXQAAAVALAQQALARATADIAKABQAYAAANgUIAQAAQgQAlgtgBQgfABgTgXg");
	this.shape_47.setTransform(-379.2,-12.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgZQAAgZARgPQAQgRAYABQAagBAMALQAJAJAAAKIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMALQgNAKAAARQAAAPALAJQAMALAPAAQANAAAKgHQAKgHAAgNQAAgQgOgEQAFAFAAALQAAAHgHAFQgHAGgIAAQgKAAgHgHQgHgGAAgJQAAgMAJgGQAJgHALABQASAAANAKQANAKAAAQQAAAXgPANQgPAPgVAAQgXAAgQgRgAgCgMQgDACAAAEQAAAEADACQACADAEAAQAEAAADgDQADgBAAgEQAAgFgDgCQgDgDgEgBQgEABgCADg");
	this.shape_48.setTransform(-391.7,-13.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFCC80").s().p("Ag2A7QgMgNABgSQABgVAOgIIgTAAIAAgNIAYAAQgEgCAAgHIABgGQAHgMAUAAIAJAAQAOABAKAMQAIALAAAQIgpAAQgLAAgIAGQgIAHAAAKQAAAJAGAHQAHAGALAAQAIAAAFgEQAGgFAAgIIAAgQIAMAAIAAAQQAAAIAGAFQAFAFAIgBQAXAAACgnQACgYgPgTQgQgUgXAAQgigBgMAWIgQAAQARgjAtABQAaABATAQQANALAHASQAGASgBASQgBAZgJARQgMATgUAAQgSAAgJgLQgIALgRAAIgBAAQgRAAgLgNgAgZgcQADAFAAADQAAADgCADIAbAAQgDgJgGgDQgFgCgHAAIgHAAg");
	this.shape_49.setTransform(-411.6,-15.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgJQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgDIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_50.setTransform(-422.3,-13.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgWASgGQgJgDAAgKQAAgMAMgGQAKgEAMABQAQAAAJAMQAJANAAAPIgnAAQgJAAgGAEQgGAGAAAJQABATAbAGQAIACALAAQAXAAANgPQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdAAgSgOgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_51.setTransform(-433.1,-15.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgFAAQgKAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_52.setTransform(-140.6,-39.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFCC80").s().p("AgOAcIAAgYIgLAAIAAgJIALAAIAAgQIgLAAIAAgKIAeAAQAIABAGAEQAHAFAAAJQAAAJgIAEQgIAEgLgBIAAAbgAgBgUIAAAPQAKAAAAgIQAAgHgJAAIgBAAg");
	this.shape_53.setTransform(-156.3,-49);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_54.setTransform(-161.3,-41.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgVASgHQgJgDAAgKQAAgMAMgGQAKgEAMABQAQAAAJAMQAJANAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAUAbAGQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdAAgSgOgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_55.setTransform(-175.7,-42.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFCC80").s().p("AgaA1IAAgNQAFADAGAAQAHAAAGgNQADgNAAgQQAAgPgDgNQgGgOgGgBQgHAAgFAEIAAgOQAGgFAMABQAPAAALATQAJASAAAUQAAAWgJAQQgLASgPABQgKgBgIgEg");
	this.shape_56.setTransform(-185.4,-41.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_57.setTransform(-195,-41.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC80").s().p("AAlAbQAJgHAAgIIAAgEQgCgGgPgEQgMgDgRAAQgQABgMADQgPADgCAGIAAAEQAAAJAIAGIgNAAQgKgFAAgPQAAgGABgCQAIgOAQgGQAOgFAVABQAxAAALAYQABADAAAFQAAAPgLAFg");
	this.shape_58.setTransform(-209.3,-48.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_59.setTransform(-209.9,-41.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFCC80").s().p("AgoBJQgYgBgPgQQgQgPAAgXQAAgQAGgLQAGgLANgMIAZgXQAJgLADgGIAXAAQgEAIgHAIQgGAIgMAHQARgBAMAEQAiANAAAoQAAAagTAQQgQAQgaAAIgDAAgAhEgQQgKAMABANQAAAOAJAKQAMAMAVAAQAQAAALgIQAOgMgBgRQAAgIgCgHQgDgLgMgGQgKgFgMAAQgWAAgMANgAAiA9QgIgMAAgTQAAgZAUgfQAJgOAOgSIhIAAIAAgNIBaAAIAAAMQgPARgKAQQgQAaAAAWQAAAXATAAQAGAAAFgEQAFgDABgHQAAgEgDgGIAMAAQAFAGAAANQAAAPgLAJQgKAJgPAAQgQAAgKgMg");
	this.shape_60.setTransform(-227.6,-42.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_61.setTransform(-252,-41.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFCC80").s().p("AgvBAQgSgMAAgWQAAgTANgJIgNAAIAAgLIARAAQgEgCAAgGQAAgHAGgEQAKgHARABQAUACAIAMQAFAIAAAOIgfAAQgbAAgDASQgBAOAQAHQANAHASgCQATgBAMgKQAMgLAEgSIACgJQAAgPgKgOQgIgOgNgGIgGAHQgEADgEACQgIAEgKAAQgOAAgFgHQgEgFADgJQABgEgIAAIgFABIAAgMIAXgBQAPAAAKACQAaAFASASQAVAVAAAhQgBAdgSAUQgTATgfAAQgaAAgSgKgAgbgUQACABABADQAAAEgCACIAYAAQgBgGgLgEIgHgBIgGABgAgWg2QACAFAHAAQAMAAAEgIQgJgEgQAAIAAAHg");
	this.shape_62.setTransform(-266.3,-42.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_63.setTransform(-288.2,-41.3);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgVASgHQgJgDAAgKQAAgMAMgGQAKgEAMABQAQAAAJAMQAJANAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAUAbAGQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdAAgSgOgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_64.setTransform(-303.1,-42.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgPQAAgNAFgJQADgGAIgIQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPAMIgNALIgMAQQgGAIABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgEgCgEIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_65.setTransform(-313.8,-41);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_66.setTransform(-324.8,-41.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgVASgHQgJgDAAgKQAAgMAMgGQAKgEAMABQAQAAAJAMQAJANAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAUAbAGQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdAAgSgOgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_67.setTransform(-345.7,-42.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_68.setTransform(-360.3,-41.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFCC80").s().p("AgDBMQgZgBgPgQQgOgPgBgXQAAgVANgPQgEgCgEgEQgDgFgCgEQgCgGAAgGQAAgKAJgMQAKgLASAAQAFAAAFABQAMADABAJQADgDADgHIAXAAQgEAJgJAKQgIAJgKAGQAQgBANAEQAiANAAAoQAAAagSAQQgSAQgZAAIgDAAgAgfgNQgJAMgBANQAAAOAKAJQANANASAAQAQAAALgJQAQgLAAgRQAAgIgEgHQgEgLgKgGQgKgFgNAAQgVAAgMANgAgngwQgEAHAAAGQAAAIAGACIAEgEIAagYQgCgEgHgBIgDAAQgNAAgHAKg");
	this.shape_69.setTransform(-374.4,-43.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_70.setTransform(-387.7,-41.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFCC80").s().p("AgtA9QgSgRAAgbQAAgSAIgNQAJgOAPAAQAPAAACAMQAFgOAPAAQALAAAIAKQAIAJAAAMQAAAPgKAKQgJAKgOAAQgMAAgJgHQgJgHAAgLIACgSQACgHgIAAQgFAAgEAJQgDAGAAAJQAAATAOALQANALATAAQAUAAANgOQAOgNAAgUQAAgPgHgLQgHgMgOAAIhNAAIAAgMQAUABAAgCQAAgBgEgBQgEgCAAgEQAAgPAQgFQAJgDAVAAQAQAAALAEQAOAEAKALQAHAIAAANIgNAAQgCgNgNgGQgMgGgRAAQgMAAgFACQgIACAAAHQAAABAAAAQAAABAAAAQABABAAAAQABABAAAAQABABAAAAQABAAAAAAQABABABAAQAAAAABAAIAjAAQAMAAAIAEQAaALAAApQAAAegSATQgSAUgcAAQgaAAgTgSgAgPADQAAAGAFAEQAEAFAHAAQAHAAAGgEQAFgFAAgIQgFAIgKABQgMAAgGgLIgBAEgAAAgPQgBACAAADQAAADABACQACACADAAQAAAAABAAQABgBAAAAQABAAAAAAQABgBAAAAQADgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_71.setTransform(-401.4,-43.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFCC80").s().p("AgEBJQgYgBgPgQQgPgPgBgXQAAgQAGgLQAGgLANgMIAZgXQAJgLACgGIAYAAQgFAIgHAIQgGAIgLAHQAQgBANAEQAiANAAAoQAAAagTAQQgRAQgZAAIgDAAgAgfgQQgKAMAAANQAAAOAJAJQANANATAAQAQAAALgJQAPgLAAgRQAAgIgEgHQgDgLgLgGQgKgFgNAAQgVAAgLANg");
	this.shape_72.setTransform(-414.2,-42.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFCC80").s().p("AgZA1IAAgNQADADAIAAQAGAAAFgNQAFgNAAgQQgBgPgDgNQgGgOgGgBQgGAAgFAEIAAgOQAFgFANABQAPAAAKATQAIASABAUQgBAWgIAQQgLASgPABQgKgBgHgEg");
	this.shape_73.setTransform(-423.3,-41.2);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_74.setTransform(-432.9,-41.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_75.setTransform(-237.3,-113.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgEByQgQAXgfAAQgkAAgTgdQgSgcABgtQACg0AfgbQAYgWAwABIAOAAIAAAXQgYAAgLABQgQACgMAJQgYASgBAhQgBAcAJARQALAUAWAAQARAAAJgLQAJgJAAgRIAAgeIAYAAIAAAeQAAAlAlAAQAYAAANgdQAMgbgCgoQgBgogUgbQgbgkgxgBQgcABgWAKQgXAMgJATIgcAAQAIgdAfgRQAegSAoAAQAgAAAbAMQAUAKANAOQAlAoABBIQACA1gVAjQgXAlgoAAQggAAgOgXg");
	this.shape_76.setTransform(-264.4,-116.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_77.setTransform(-282.4,-113.4);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AA6BpQglAAgRgZQgPAZgpAAQgdAAgUgXQgVgXABgfQABgiAWgRIgfAAIAAgYIAfAAQgGgEAAgMQAAgFACgFQALgfAsAAQAmAAAMAaQAQgaAkAAQAVAAAOAJQARALACASQACARgHAJIgFAAQAfAZAAAnQAAAhgVAYQgUAYgeAAIgBAAgAARAGIAAAXQAAANANAGQALAGAQAAQAQAAANgMQAOgNgBgRQgBgPgLgLQgLgKgRAAIhrAAQgRAAgMALQgNALAAAQQAAARAMAMQAMALASAAQAQAAAKgFQAOgHAAgMIAAgYgAAyhPQgOABgIAJQgJAJgBANIA3AAQAEgFAAgIQgCgTgXAAIgCAAgAg8hMQAGAGACAIQAAAIgEAHIAtAAQgBgRgOgJQgIgGgLAAQgJAAgGADg");
	this.shape_78.setTransform(-300.3,-113.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AguBpQgkgCgXgXQgYgXgBghQgBgfAKgWIAYAAQgCAIADADQADAEAIAAQAMAAAJgGQATgMAFgPIABgJQAAgQgLgFQgKgEgJAEQAPAEAAATQAAALgHAGQgHAHgLAAQgGAAgEgCQgTgGAAgXQAAgHADgIQAKgbAmAAQAPAAALAHQAMAHAEAMQAVgcArACQAtACAWAqQAQAcgBAhQAABGgzAZQgQAIgSAAQgLAAgOgDIAAgRQgQAMgNAEQgNAEgUAAIgFAAgAAYgaQAYATAAAgQAAAggUAVIAQACQASAAAJgHQAbgUgBgoQAAgagSgVQgRgUgagBQgggCgPAUIADgBQATAAANAMgAhnAPQAAAQAJALQANARAdAAIASgBQATgCANgPQALgNAAgQQAAgKgFgHQgGgLgRgBQgIAAgNAIQgRALgEACQgMAFgMAAQgLAAgGgFIgBALg");
	this.shape_79.setTransform(-339.5,-113.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_80.setTransform(-358.4,-113.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AhdBLQgcgYAAgrQAAgRAIgPQAHgQANgHQgOgGAAgPQABgGACgFQAJgZAuAAQAjAAAOAVQAPgVAmAAQATAAAOAJQAQAIACAQQACAMgHAMQAKAIAGAOQAHAQAAARQAAArgfAaQghAcg6AAQg7AAgigegAhUgXQgJALAAARQACAaAgANQAXAKAjAAQAjAAAYgKQAggNABgaQAAgRgKgLQgJgKgQAAIhyAAQgQAAgKAKgAAahKQgKAHgCALIA5AAIABgGQAAgIgHgFQgHgFgJAAIgCAAQgLAAgKAGgAhBhPQAGAEABAHQABAHgEAFIAsAAQgBgTgagFIgKgBQgGAAgFACg");
	this.shape_81.setTransform(-375.8,-113.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AhYBbQghgnABg3QABg4AigkQAjgkA5AAQAxAAAhAgQAgAfAAAxQAAAngVAbQgXAeglABQglAAgVgXQgWgXADgiQACgfAYgRIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMAMQANALATgBQAXAAAOgTQANgQgBgZQgBgegPgSQgWgagtAAQgnAAgWAiQgUAcAAApQAAAmAUAdQAUAfAjAGIASABQAsABAYgjIAdAAQgeBBhSAAQg3AAgkgpg");
	this.shape_82.setTransform(-401.2,-110.6);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AAECAQgOgGgLgQQgDAMgMAIQgMAIgRAAQgcAAgRgfQgQgbAAglQAAgsAZghQAbgjArAAQAgAAAUAUQAUAUAAAfQAAAYgTAQIgKAJQgFAGgBAHQgBAYAhAAQAZAAAQgaQANgYAAgiQAAgrgZggQgaghg0AAQgZAAgVALQgXALgLATIgbAAQALgfAfgSQAcgRAlAAQA9AAAkAkQAQAQAMAdQAMAdAAAZQACA+gbAkQgYAigmAAQgWAAgNgGgAhJBQQgFAEAAAHQAAAGAFAFQAFAEAGAAQAHAAAEgEQAFgFAAgGQAAgHgFgEQgFgFgGAAQgGAAgFAFgAhaAGQgIAQAAAUQAAAPAEANQALgUAYgCQASgBAQANQACgOALgLQAPgOAAgQQgBgOgKgJQgKgJgPAAQgqAAgPAhg");
	this.shape_83.setTransform(-426.8,-116.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AhXBmQggggABg0QABgkAagZQAagaAlAAIAcAAIAAAYIgYAAQgcAAgRARQgRARgBAYQgBAhAbAUQAaASAjgBQAmAAAZgbQAYgbAAgmQAAgngZgeQgZgdgmgCQgbAAgXAMQgXAMgKAUIgYAAQAMggAcgSQAcgSAoAAQA1AAAiAoQAgAmAAA4QAAA5ghAlQgjAng2AAQg1AAgfggg");
	this.shape_84.setTransform(-101.7,-166.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("Ag6B6QgZgMgOgVQgWggAAguQAAggARgaQASgbAYABQAYACAHARQALgVAaAAQAXAAAQASQAQARABAYQACAbgVAUQgVAWgbgBQgaAAgSgQQgRgRABgXQAAgIADgJQAGgKABgDQABgGgDgDQgEgEgGABQgLACgHAPQgGAPgBARQgBAkAbAWQAaAXAkAAQAkAAAbgbQAbgcgBglQgBgtgWgbQgYgegtAAQg4AAgWApIgbAAQANggAdgRQAbgPAkAAQA4gBAeAgQAlAkACA8QACA7ghApQgiApg5AAQgeAAgagNgAgjgKQAAALALAIQALAHANAAQAPABAMgKQAMgJAAgOIgBgHQgCAJgKAGQgKAGgMAAQgcAAgGgaQgFAOAAAEgAgGgnQAAAGAEAEQAEAEAGABQAGAAAFgFQAEgDAAgHQAAgFgEgFQgFgFgGABQgOAAAAAOg");
	this.shape_85.setTransform(-127,-166.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgHAAgJQAAgFACgFQALgbA1ACQAfADAQAWQAQAUgCAeIhHAAQgiAAABAaQAAAUAZAKQAVAIAhAAQAjAAAXgSQAXgSADgcIABgQQAAgcgSgWQgQgSgcAAIhTAAQgnAAAAgaQAAgtBkgBQA8AAAWARQAQANAAAQQAAAQgOAKQAgAkgBA4QgBAvgkAeQgiAdgwABQgrgBgdgRgAg5gdQAGAFABAIQABAJgHAGIAyAAQgEgOgJgHQgLgIgQAAQgGAAgFABgAAuhmQgEAEAAAGQAAAHAEAEQAEAEAGAAQAGAAAEgEQAFgEAAgHQAAgGgFgEQgEgDgGAAQgGAAgEADgAg5hfQgCAJATAAIA5AAQgDgMAGgLIgYgBQgzAAgCAPg");
	this.shape_86.setTransform(-151.4,-166.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("ABDAwQASgMgBgQIgBgFQgDgNgagFQgXgGgfAAQgdAAgXAGQgbAGgEAMIgBAGQABARAPAKIgWAAQgUgHAAgdQAAgKADgFQANgaAfgKQAYgIAnAAQBZAAATAsQADAGAAAJQAAAcgUAIg");
	this.shape_87.setTransform(-189.1,-176.6);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_88.setTransform(-190.8,-163.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgXAIgQQAGgKAOgRQAQgTAqggIhQAAIAAgYIB3AAIAAAWIgcAVQgNAKgLAMQgRAUgFAIQgKAQABASQABAMAIAGQAIAGAKAAQAWAAAHgTQACgFAAgGQAAgHgGgIIAYAAQAMASgBAVQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_89.setTransform(-212.3,-163);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AAiB+IAAgtQgeADgXgBQgvgCgcgVQgggXABgqQAAgVALgRQALgQAQgHQgIgDgEgGQgDgGAAgHQAAgNALgJQAUgQAlABQA6ABANAfQAFgOAMgJQAMgJAPAAIAYAAIAAAXIgEgBQgIAAgBAIQAAADAIAGQAMAKAEAFQAKALAAAOQABAYgWAOQgTAKgYgCIAAA1IAVgGIAQgGIAAAXIgRAHIgUAFIAAAygAg5glQgOAPAAAVQABAuAzALQAJACARAAQASAAAJgCIAAhtIg5AAQgVAAgNAQgAA+g/IAAAoIAEABQAKAAAHgGQAHgGAAgKQAAgLgKgOQgKgPAAgGIAAgEQgIACAAAdgAgxhjQAEAGAAAHQAAAGgEAEIAEgBIAGAAIA2AAQgEgOgSgHQgNgEgLAAQgJAAgJADg");
	this.shape_90.setTransform(-232.4,-161.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_91.setTransform(-263.1,-177.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_92.setTransform(-272.7,-163.4);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_93.setTransform(-299.8,-163.5);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACguQACgmAfgOQgQgEAAgSQAAgWAWgLQASgIAXABQAcACARAWQASAXgBAcIhIAAQgQAAgLAIQgLAKABAPQACAlAwAKQAPADAVAAQAogBAZgaQAYgZAAgmQABgpgYgdQgZgfgwgCQg1gBgcAoIgaAAQANgeAegQQAdgPAlAAQA2gBAjAmQAkAoAAA3QAAA3ggAmQgiApg2AAQg2AAgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgMgJgJQgKgJgOAAQgIABgHACg");
	this.shape_94.setTransform(-325.1,-166.3);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_95.setTransform(-342.3,-177.6);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_96.setTransform(-352.3,-163.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AgEBxQgQAYgfAAQgkAAgTgdQgSgcABgtQACg0AfgbQAYgWAwAAIAOAAIAAAYQgYAAgLABQgQACgMAJQgYASgBAhQgBAcAJARQALAUAWAAQARAAAJgLQAJgJAAgRIAAgeIAYAAIAAAeQAAAlAlAAQAYAAANgdQAMgagCgpQgBgogUgbQgbgkgxgBQgcABgWAKQgXAMgJATIgcAAQAIgdAfgRQAegSAoAAQAgAAAbAMQAUAKANAOQAlAoABBIQACA1gVAjQgXAlgoAAQggAAgOgYg");
	this.shape_97.setTransform(-379.8,-166.6);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgHAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAUgCAeIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgSQAXgTACgcQACgogUgZQgQgSgbAAIhTAAQgnAAAAgaQAAgtBkgBQAmABAeAPQAiASABAdIgbAAQABgRgcgLQgXgIgdgBQgzAAgCAOQgCAJATAAIBDAAQAbAAAWASQAmAeAABDQAAAvglAeQgjAdgvABQgrgBgdgRgAg5gdQAGAFABAIQABAJgHAGIAyAAQgDgOgKgHQgKgIgRAAQgGAAgFABg");
	this.shape_98.setTransform(-404.4,-166.5);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgHAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAUgCAeIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgSQAXgTACgcQACgogUgZQgQgSgbAAIhTAAQgnAAAAgaQAAgtBkgBQAmABAeAPQAiASABAdIgbAAQABgRgcgLQgXgIgdgBQgzAAgCAOQgCAJATAAIBDAAQAbAAAWASQAmAeAABDQAAAvglAeQgjAdgvABQgrgBgdgRgAg5gdQAGAFABAIQABAJgHAGIAyAAQgDgOgKgHQgKgIgRAAQgGAAgFABg");
	this.shape_99.setTransform(-428.1,-166.5);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARAAAKAIQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAxgAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_100.setTransform(-237,-227.6);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_101.setTransform(-245.3,-213.5);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AhCBLQgegeAAgsQAAguAegdQAdgdAsgBQAwABAWATQAPAOABAUIgYAAQgBgNgTgJQgRgHgWAAQgdAAgWASQgXAVAAAeQAAAbAUATQAVASAdAAQAXAAASgNQATgNAAgXQAAgdgbgIQAKAKAAATQAAANgMAKQgNALgPgBQgSAAgNgLQgNgLAAgTQAAgVARgLQAPgLAWAAQAgAAAYASQAWATAAAfQAAAngbAaQgbAZgmAAQgqAAgdgdgAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgIgFgFQgFgFgHAAQgIAAgEAFg");
	this.shape_102.setTransform(-268.5,-213.4);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_103.setTransform(-292.9,-213.5);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARAAAKAIQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAxgAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_104.setTransform(-311.9,-227.6);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_105.setTransform(-312.5,-213.5);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AhYBbQghgnABg4QABg3AigkQAjgkA5AAQAxAAAhAgQAgAfAAAxQAAAngVAbQgXAeglAAQglACgVgYQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMAMQANAKATABQAXgBAOgTQANgQgBgZQgBgegPgRQgWgagtAAQgngBgWAiQgUAcAAApQAAAnAUAcQAUAgAjAFIASABQAsAAAYgiIAdAAQgeBBhSAAQg3AAgkgpg");
	this.shape_106.setTransform(-329.6,-210.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("AhCBLQgegeAAgsQAAguAegdQAdgdAsgBQAwABAWATQAPAOABAUIgYAAQgBgNgTgJQgRgHgWAAQgdAAgWASQgXAVAAAeQAAAbAUATQAVASAdAAQAXAAASgNQATgNAAgXQAAgdgbgIQAKAKAAATQAAANgMAKQgNALgPgBQgSAAgNgLQgNgLAAgTQAAgVARgLQAPgLAWAAQAgAAAYASQAWATAAAfQAAAngbAaQgbAZgmAAQgqAAgdgdgAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgIgFgFQgFgFgHAAQgIAAgEAFg");
	this.shape_107.setTransform(-352.5,-213.4);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AhkBrQgUgYABggQACgmAagQIgiAAIAAgYIArAAQgIgDAAgNQAAgGACgFQANgWAkAAIAQAAQAcADARAVQAPAUgBAdIhLAAQgUAAgOAMQgOAMgBATQAAARAMAMQAMAMAUAAQAOAAAKgJQALgJAAgPIAAgbIAXAAIAAAcQAAAOAJAJQAKAJAPAAQAqgBAEhHQADgtgcgjQgcgjgqgBQg/gBgXAoIgcAAQAfg/BRABQAxABAiAdQAYAVAMAhQAMAggCAiQgCAugRAdQgVAkglAAQggAAgQgTQgQATggAAIgBAAQgfAAgVgYgAgtgzQAFAIAAAGQgBAGgEAEIAzAAQgHgRgLgEQgKgEgMAAIgLABg");
	this.shape_108.setTransform(-388.5,-216.1);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgWAIgRQAGgKAOgRQAQgTAqggIhQAAIAAgYIB3AAIAAAWIgcAVQgNAKgLAMQgRAUgFAIQgKAQABASQABAMAIAGQAIAGAKAAQAWAAAHgTQACgGAAgFQAAgHgGgIIAYAAQAMASgBAVQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_109.setTransform(-408,-213.1);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACguQACgmAfgOQgQgFAAgRQAAgXAWgKQASgIAXABQAcACARAXQASAVgBAdIhIAAQgQAAgLAIQgLAKABAPQACAlAwAKQAPADAVAAQAogBAZgaQAYgZAAgmQABgpgYgdQgZgfgwgCQg1gBgcAoIgaAAQANgeAegQQAdgPAlAAQA2gBAjAmQAkAoAAA3QAAA3ggAmQgiAog2AAQg2AAgggXgAg5gwQAFAEACAGQADAIgEAJIAsAAQgCgNgJgHQgKgKgOABQgIgBgHADg");
	this.shape_110.setTransform(-427.4,-216.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.lf(["#C4291D","#E26D45"],[0,1],-504.9,0,504.9,0).s().p("EhO4AAtIAAhZMCdxAAAIAABZg");
	this.shape_111.setTransform(-0.7,-279.5);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#2B0912").s().p("EhO4AsYMAAAhYvMCdxAAAMAAABYvg");
	this.shape_112.setTransform(-0.7,0);

	this.instance = new lib.Path();
	this.instance.parent = this;
	this.instance.setTransform(-0.7,282.5,1,1,0,0,0,504.9,23.5);
	this.instance.alpha = 0.539;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_112},{t:this.shape_111}]}).wait(1));

	// Layer 1
	this.instance_1 = new lib.Bitmap1_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-640,-360);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.slider2, new cjs.Rectangle(-640,-360,1280,720), null);


(lib.slide1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		
		var clip = this;
		var count = 7; // Define the number of buttons |Sanka|
		
		
		
		this.home_btn.visible = false;
		this.slider_mov.visible = false;
		
		
		
		
		for (var i=1; i<=count; i++){
			//var btn = clip["btn"+i];
			//btn.mouseEnabled= false;
		}
		
		
		
		
		
		var btn = []; //Define the array switchyes
		
		for (var i=1; i<=count; i++){
			var btnn = clip["btn"+i];
			btnn.name = i;
			btn[i-1] = clip["btn"+i];
			btn[i-1].addEventListener("click", btn_clk.bind(this));
		}
		
		
		function btn_clk(evt)
		{
		
			for (var i=1; i<=count; i++){
			var btn = clip["btn"+i];
			btn.mouseEnabled= false;
			}
			this.home_btn.visible = true;
			var item = evt.currentTarget.name;  // Selecting (current target) button name |Sanka|
		
			//var mov = clip["mov"+item];
			this.slider_mov.visible = true;
			
			//alert(item)
			if(item <= 5){
			this.slider_mov.gotoAndStop(item-1);
			}
			if(item == 6)
			{
				this.slider_mov.gotoAndStop(0);
			}
			if(item == 7)
			{
				this.slider_mov.gotoAndStop(1);
			}
			
		
		}
		
		this.home_btn.addEventListener("click", close_clk.bind(this));
		
		function close_clk()
		{
			this.slider_mov.visible = false;
			for (var i=1; i<=count; i++)
			{
				var btn = clip["btn"+i];
				btn.mouseEnabled= true;
			}
			
			this.home_btn.visible = false;
			
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// close
	this.home_btn = new lib.close();
	this.home_btn.parent = this;
	this.home_btn.setTransform(437.4,224.5);
	new cjs.ButtonHelper(this.home_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.home_btn).wait(1));

	// slider
	this.slider_mov = new lib.mov();
	this.slider_mov.parent = this;
	this.slider_mov.setTransform(-0.5,2);

	this.timeline.addTween(cjs.Tween.get(this.slider_mov).wait(1));

	// btns
	this.btn7 = new lib.btnm7();
	this.btn7.parent = this;
	this.btn7.setTransform(245.8,198.1);
	new cjs.ButtonHelper(this.btn7, 0, 1, 1);

	this.btn6 = new lib.btnm6();
	this.btn6.parent = this;
	this.btn6.setTransform(364.8,54.1);
	new cjs.ButtonHelper(this.btn6, 0, 1, 1);

	this.btn5 = new lib.btnm5();
	this.btn5.parent = this;
	this.btn5.setTransform(144.8,77.8);
	new cjs.ButtonHelper(this.btn5, 0, 1, 1);

	this.btn4 = new lib.btnm4();
	this.btn4.parent = this;
	this.btn4.setTransform(188.8,53.1);
	new cjs.ButtonHelper(this.btn4, 0, 1, 1);

	this.btn3 = new lib.btnm3();
	this.btn3.parent = this;
	this.btn3.setTransform(248.8,84.1);
	new cjs.ButtonHelper(this.btn3, 0, 1, 1);

	this.btn2 = new lib.btnm2();
	this.btn2.parent = this;
	this.btn2.setTransform(191.3,193.1);
	new cjs.ButtonHelper(this.btn2, 0, 1, 1);

	this.btn1 = new lib.btnm1();
	this.btn1.parent = this;
	this.btn1.setTransform(233.8,-63.4);
	new cjs.ButtonHelper(this.btn1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.btn1},{t:this.btn2},{t:this.btn3},{t:this.btn4},{t:this.btn5},{t:this.btn6},{t:this.btn7}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#689F38").s().p("AAQFVQgPgIgVAAQgUAAgLgFQgKgFgQAAQgTAAgMgFQgOgHgNgQQgNgOAAgJQgBgEAEgDQADgCAIAAQAFAAADgEQAFgHACgOQACgPgFgEQgJgHgUgWQgagbgDgIQgCgGgFgEQgKgOAEgLQAEgLAAgEQABgFgDgGIgEgGQgHgKAEgJQAFgHAPgGIAKgFQAKgFACgGQABgEgCgCIgEgHQgJgOAAgHQAAgDADgDQAEgDgCgLQAAgJgGgKIgKgPIgHgJQgJgNADgIQADgGAMgCQAHgBAHgEQAEgDALgCQAJgCANAEQAJACACgCQACgEAAgCQABgFACgBQADgDAJgDIAHgDQAPgFABgDQACgFgGgJIgGgJQgJgOgDgGQgFgKgBgNQgCgIACgLQABgEgBgBIgFgFQgdgVgGgOQgCgGAAgGQAAgFgCgEQgCgFgHgEQgJgFgDgHIgBgDQgDgGgCgBIgKgGQgLgFgHgKQgHgKACgLIAIACQgCAHAGAIQAFAHAJAFIANAHQACADAEAHIABACQACAEAHAFQAJAIADAEQADAGAAAHQAAAFACAEQAEALAcAVQAGAEACAEQABAEgBAFQgCAKABAHQACAMAEAJIALATIAGAJQAJANgDAHQgDAHgSAHIgIACQgJADAAACQgBAHgDADQgDAEgGABIgKgCQgLgDgHACIgNAEQgJAEgIACQgHABgBACQgCADAHALIAGAIIALAQQAGALABAMQACAOgGAFIgBABQAAAFAIALIAEAJQADAFgCAGQgDAJgNAHIgKAGQgMAEgEAGQgCADAFAHIAFAIQADAGgBAHQgBAGgEALQgCAHAIAKQAEAGADAGQAFANAzAxQAIAHgCASQgCAQgHAJQgFAIgKAAQgEgCgCADQgBADAMAOQAMAPAOAGQAJAFASAAQASAAAMAFQAJAFASAAIATACQAKACALAFQAMAHAIgBQAFgBAJgFIANgIIANgEIALADQAHACAMACQAKACAIgDIANgIQAKgIALgEQAXgGAZgBIAAAHQgYACgVAGQgIACgMAJQgJAGgGADQgKADgMgCIgTgFIgJgCQgDAAgHAEQgFABgHAFQgMAHgIAAQgJAAgNgGg");
	this.shape.setTransform(244.7,95.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#689F38").s().p("ABdAqIgBgCQgJgQgGgFQgIgHgKADQgaAFgGAJQgFAGgHACQgFACgLAAQg4AAgGgEQgDgBgHgNQgJgRgFgEQgUgLgXgDQgagDgMgKQgOgKgCgJIgKAAIgJACIgOAHIgDgHIANgHQADgCAKgBQALgBAEADQADABAAADQAAAGAMAJQAKAIAZADQAZAEAVANQAIAFAJARIAHALQAFADA1AAIAOgBQAEgBADgEQAJgLAdgHQAOgEALAKQAGAGAKARIABACQAFAIAGABQAFAAAIgCIAEgBQABgDgEgLQgKgcALgMQALgRAugNIAkgLIACAHIgkAMQgqAMgMAOQgIALAJAWQAGANgDAHQgCADgDABIgDABQgHADgHAAQgMAAgIgNg");
	this.shape_1.setTransform(197.3,170.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#689F38").s().p("AAeAlQgPAAgTgXQgIgJgKgCQgTgEgGgCQgGgDADgLIABgHQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAgBAAAAIgCgBIAAgHQAJgBADAKQABADgCAJIgBAEQAGADAPADQAIABADACQAGADAHAIQAQASAMAAIAZAAIAAAJg");
	this.shape_2.setTransform(363.3,152.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#689F38").s().p("AmULaQAfgLAEAAQAFAAAUgMQASgKAggKQAFgCACgDQACgEgDgHIgGgGQgIgFgBgGQgDgIAJgNIABgCQAEgHAJAAQAFAAAKgIQAFgEAAgIIgBgDQAAgFAEgCQAFgEANAEQAGACAKgDIASgFQAPgDAfAGIALACQAKABADAJQABAFAAAKIAAADQAAABAAABQAAABAAAAQABABAAAAQAAAAAAAAQACACAHgDQAHgCAEgJIADgGQACgEAFgCQAKgBANALQAGAEAQAbIAOAYQAIAOAFAEQADADAEAAQACgBAFABIACgBQADgDADgIQACgHAAgGQAAgLAOAAIAJgBQAEgBAQgBQASgBAGgCQAdgJAYgPQAIgEAGgLIAGgJQAHgFAIAFIAFAEQAFADAFgBQAEgBAHgEQAHgDAGAAIAOADIAFABQAJADAFgCQADgBAEgGQADgCAAgKIAAgIQAAgFADgEQACgEABgHQACgIgGgKIgEgIQgEgJAQgLQAHgEAEgEQAfgeAUgOQANgJANgCIAKgCQAPgCAMgIIAEgCQAGgDAKgDIgBgGQgCgFgCgkIAAgLQgEg4AEgoIADgjQAFg+AEgeQAGgmACgtQABgpgCgZQgDgUgLgJQgEgDAAgDQAAgDACgGIABgKIgDgXQgCgJADgLQACgJAAgGQAAgLgDgDQgCgCgKgBQgtgHgPgIQgLgFgVgTIgGgGQgHgGgDgRQgCgOgEgIQgFgKABgFQAAgEAFgFIADgDIABgGQABgKAHgHQARgRALgPQAEgFgCgJQgCgJgGgFQgGgGgRgkIgJgUQgEgIgGgFQgGgEgFgJQgFgMgBgGQgBgFAAgJIAAgDQABgIgFgHIgDgEQgCgEgCgOIgCgNIgFgLIgDgGQgDgJALgGIADgCQACgCAAgDQABgEgDgEQgEgEgQAIIgPANQgaAXgLAHIgQAJQgJAEgCACQgCADgCAJIgCAMQgDAIgMACQgDABgDACIgEAGQgEAKgIAHQgHAGgKAEIgDABIgCAGQgEAMAAADIgEA3QAAAAAAABQAAABAAAAQAAABgBAAQAAABAAAAQgEADgJgCIgKAAIADAFIAEAFIgGACQgMADgKgDQgKgDABgLIAAgHQgBgGgIgDIgFgDQgFgEABgJQABgFgCgFQgDgOgSgcIgHgJIgDADQAAABAAAAQgBABAAAAQAAABgBAAQAAAAgBABQgDAEgFAAQgHAAgNgOQgHgIgGgZQgEgSgEgHQgJgTAEgdQAAgHAEgMQAEgOAAgEQAAgVAIgTQAEgKAAgFQAAgFgEgKIgEgOQgCgGgCgDQgFgGgQABIgDAAQgFAAgKAHQgLAHgHAHQgFAGgJAFIgMAJQgFADgFAHIgJAKQgGAFgHABQgMACgJgEQgGgCgKgIQgIgIgWgeIgFgHQgHgKABgYQABgIgBgEQgBgCgJgEQgJgEgMgDIACgHQAiAIADAKQABAFgBAJQgBAWAFAHIAGAHQAUAeAJAHQAIAHAFACQAHADAJgCQAJgBAJgMIALgMIANgIIANgKQAHgIAKgHQAOgJAHAAIADAAQAUAAAHAIQAEAGACAHIADAMQAEALAAAHQAAAHgEALQgHAQAAAVQAAAEgEARQgEALgBAFQgCAdAHAQQAEAIAEASQAFAWAGAHQAMAMADAAIADgBIACgEQAFgGAEAAQAGAAAIANQASAdAEAPIABANQgBAFACAAIAEADQALAFABAJIABAKIAAAEIABABQAIACAHgBQgCgGABgDQACgEAFAAIALABIAGAAIADg1QAAgFAFgNIABgFQAAgEAIgDQAKgDAFgFQAGgFAEgJQADgHADgCQACgCAHgCQAIgCABgEIADgKQACgMADgEQADgEAKgEIAQgIIAkgeIAQgNQAVgNAIAKQAGAGgBAHQgBAIgEADIgEADQgGACABACIADAGIAFANQACAEABAKQABALACAEIACAEQAHAIgBALIAAAEIAAALQABAGAFAKQAFAIAEADQAHAGAFAJIAJAUQAPAiAGAGQAJAHACAMQACAMgFAIQgOARgQAQQgFAFAAAGQAAAGgDAEQgBADgDABQgCADAAACQgBAEAEAHQAFAJACAOQACAPAFAEIAGAGQAVASAJAFQAOAHAsAHQANABAEAFQAFAFAAAOQAAAGgCALQgDAJACAIIACAQIABAFQABADgDAMIgBAGIABABQAOALACAYQADAYgBApQgCAugGAoQgGAlgDA2IgDAjQgFAkAEA6IABALIADAoIAEAOIgEABIgGABIgMAFIgEACQgNAIgQADIgLACQgKABgNAJQgTANgeAdQgHAGgFADQgLAHABAEIAEAIQAHANgCAJQgBAHgDAHIgCAGIAAAHQAAANgFAFQgGAGgEACQgHADgMgDIgFgBIgNgDQgEAAgEACIgNAGQgIABgHgEIgJgGIgDACIgFAGQgGAMgKAGQgZAPgeAJQgIADgSABQgLAAgGACIgMABQgGAAAAADQAAAHgDAJQgDAKgFAEQgEAEgFgCIgFAAQgGABgFgEQgGgFgKgQIgOgYQgPgZgFgEQgKgJgGABIgCACIgDAFQgGANgJADQgLAEgFgEQgFgDAAgIIAAgDIgBgMQgBgEgFgBIgMgCQgdgGgOADIgRAEIgKADQgFABgGgCQgIgEgCACIgBABIAAADQABALgIAHQgMAKgIAAQgFAAgBADIgCACQgGALABAEQABADAFADQAHAFACAFQAEALgDAGQgDAHgJACQgkANgMAHQgVANgIAAQgDAAgeAKg");
	this.shape_3.setTransform(319.1,106.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#689F38").s().p("AGEM8IgMgDQgUgFgIgDQgIgDgNgNQgMgMgHgCQgKgCgLAAQgMAAgFAEQgJAGgegGQgLgBgJgMIgDgEQgFgGgZgHIgRgGIgdgIIgPgEIgDAAIAAgbIgJAAQgHABgEgBQgGgCADgLQADgKgCgDQgCgEgVAHIgJACIgTAAIgEAAIADgUQAAgHAIgJQAGgJgGgIQgDgDgQAEQgLADgGAAIgUgCIgWAAQgCABgQAVIgHAKQgHAJgcAAIgCAAQgEAAgDgCQgBgCAAgGIAAgDQgBgIgRgDQgngGgRgLQgIgFgSgOQgRgOgJgJQgNgNggg9IgaguQgUgXgDgFQgBgFACgEQAFgHALABIACAAIgGgQQgDgKgcgRQgJgFgIgPQgHgLgEgKQgDgJgcghIgGgHQgIgJgCgKQgCgGAAgOQAAgIgIgTIgNgiQgJgZACgFQADgGAMABQAIAAADgBIABgCQABgEgEgJQgEgJgGgGQgHgJgDgPQgBgNACgIQABgHAQgMQAPgLAFgCQACgCgBgNIAAAAIABgBQABgJgBgDQgDgEACgOIAAgDQAAgEgFgDQgDgCACgHIABgFIgMABQgQADgDgEQgEgEgLguQgLguABgIQABgJAUgdQARgYAFgFIACgDQAAgCgGgCIgIgCQgGAAgEgDQgCgBgDADQgDADgJAAQgKAAgDgDQgDgDgDgWIgCgaQAAgOAYgBIACgBQAHAAAFgOQABgDABgPIACgPIALgiIAIADIgMAiIgBAMQgBASgCADQgGASgMABIgCABQgRABAAAGIACAaQACARACADQABABAGgBQAFAAABgBQAHgIAJAFIAPAFQAKADABAGQACAFgFAFQgIAJgPAWQgSAZAAAHQgBAHAKAqQAKAoAEAJIALgCQASgCACAFQACADgBAIIgBACQAHAGAAAGIAAAEIAAAOQAEAGgEAKQACATgIADQgFACgNAKQgOALgBAEQgBAHABALQACANAGAHQAHAHAEAKQAFALgBAHQgCAGgEABQgFADgJgBQgIAAAAABQgBACAJAVIAHATIAGAPQAIAWAAAIIABASQACAIAHAIIAGAHQAdAiAEAKQAKAcAOAJQAfAUAEALIACAGQAFANgDAFQgDADgFAAQgHgBgCADQABAFAUAYIAbAvQAhA9AKAKQAKAKAQANQARAOAIAFQAOAIAoAIQANACAFAEQAFAEABAHIAAAGIACAAQAYAAAFgGIAHgKQARgYAGgBIAXABIAUABQAGAAAJgDQAWgFAFAHQAJANgJANQgGAIAAAEQAAAFgCAHIAOAAIAHgCQAbgJAFALQADAHgDALIgBAEIAGgBQANAAADADQAAABABAAQAAABAAAAQABABAAABQAAAAAAABIAAAUQAZAGARAGIAQAFQAcAJAGAHIADAEQAIAJAHACQAbAFAHgFQAHgEAOgBQANAAALADQAIACAOAOQANAMAFABIAbAIIANAEIAFABQADgEgHgTQgIgWgOghIgRgjIgJgEQgSgGgJgJIgJgIQgKgIgDgGQgGgLgCgVQgNhmACgLQABgGAKgNIAGgJIADgIIAMgeIASgvQACgEAAgGQgBgIAIgJQAHgIAGgPQAEgNABgHIAAgEIgDABIgIAEQgTALgGAAQgEAAgFADQgEADgEAAQgHAAgDgIIAAAAIgCgBQgTgDgEgIQgCgGAAgEIgDgHQgJgNgCAAIgDgEIADgCIAJgGQAKgFAJAAQAPAAABgCIAAgJIgDgKQgDgEgIgFIgFgEQgGgEgEgEQgDgBgHgGQgLgJADgLIAAgCIgCgGIgBgEIABgFIADgNQAAgGADgIQgDgCgBgCQAAgFAEgDIARgJQADgDgJgUIAAgBQgLgYgCgPQgBgHgDgDQgDgCgHgCIgIgBQgRgBgDgGQgCgFAHgIQADgEAPgCQANgBAHgEQALgdAPgHQAPgHAlgMQAMgDAIgKQAFgGAHgPIAIgOQAHgNATgRIAJgKQAEgJACgKQACgPgEgHQgHgNAEgJQACgHAGgBIgFgFQgJgCgCgGQgDgHAFgJIAEgFQAEgHABgEQAAgEgEgGQgRgXAEgOQACgHAQgCIAMgCIAFgBQAJgDAGgGQAFgFAEgDQAGgDAEgFQAQgRgDgHIgFgHQgbgigCggQgBgUgDgZQgDgbAAgLIADghQAFg1gFgNIgEgMQgHgPgBgHQgCgLADgMQACgIADgHQAEgKABgSQACgQAGgNQAGgNACgKQADgNAFgLQAGgSgBgPIgCguIAIAAIABAtQACASgHATIgHAWQgEAPgFAKQgFALgCAQQgCATgEALQgCAFgCAJQgDAKACAKIAHAUIAFAMQAFAOgFA4QgDAUAAAMQAAAKADAbIAEAtQABATAKASQAFAKALAPIAGAIQAGAMgVAWQgGAHgFACIgHAGQgHAIgMADIgRAEQgMABAAADQgDAJAOAUQAGAJAAAIQgBAEgFAJIgEAFQgDAGABADQABACAFACQAEABADAEQAEAEAAADQAAAGgFABQgBAAgBABQAAAAgBABQAAAAAAABQgBAAAAABQgCAFAFAKQAJARgMAfQgBACgLALQgSARgHALIgHAOQgIAQgFAGQgJALgOAEQggAKgUAJQgGAEgHAMQgGAKgDAIIAAABIgCABQgHAEgRACQgLABgCACQgEAEAAABQACACALAAIAKABQAJADAEADQAGAFABAKQABANALAXIAAACQAMAZgIAHIgIAEIgJAFIABAAIAFABIgEAJQgCAFAAAFQAAAGgDAJIgBADIABACQACAFAAADIgBADQgBAIAHAFIAGAEIADgBIABADIAKAJIAEADQAKAHADAEQADAEACAKQABAKgCAEQgEAGgTAAQgIAAgLAGIAJAMQADAGAAAEIACAIQADAFAOABIAGACQACABABADQAAABAAABQABAAAAAAQAAABAAAAQABAAAAAAIAEgCQAHgDAFgBQAEAAASgJIAJgFQAHgCAEACQADACAAAJQgBAIgFAOQgGAQgIAJQgGAHABAEQAAAIgCAGIgSAvQgEALgIATIgEAIQgCAFgFAGQgJAMAAADQgBAIALBnQADAUAEAIQADAGAJAHIAJAIQAIAIARAGQALADACAEIARAkQAOAhAIAWQAJAZgHAGQgCADgEAAQgDAAgEgCg");
	this.shape_4.setTransform(241,105.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#689F38").s().p("Ag7EXQgFgLgNgHQgMgHgEgIQAAgCgGAAQgJgBgTAHQgQAIgEAEQgXAcgagOQgMgGgKgLQgHgIgJgBQgNgBgMALIgGAEIgKAJIgHAHQgGAFgVAEIgDABIgHAEQgKALgLAAQgTAAgJgDQgHgBghgBIAAgIIASABIAXABQAJADASAAQAHAAAJgIQAGgGAEgBIAEgBQATgEAEgEIAHgGIALgKIAFgDQAPgNAQACQAMABAJAKQAJALAKAEQAWALASgXQAFgGASgHQAUgKAMACQAIAAAEAHQACAFALAGQAPAIAGANQANAbAYgBQASABAcgcQAMgMAggNQAYgKAYgGQAPgEAEgJQADgGAAgLIAAgEIgCgIQgDgLABgFQACgIALgFQAOgFAVACQAUADAGAJIACADQAQAWAJAEQAHABAOgFIAIgDIgBgCQgDgJABgFQACgOAbgNQAFgDAVAKIAMAGIACAAQADgDAEgPIAAgEQACgEALgDIAIgDQABAAAAgMQgBgIABgIQACgMAXgMIADgCQAFgCAAgDQAAgEgCgIIgCgMQAAgQgLgUIgDgEIgFgKQgCgHAFgFIAMgJQAKgFAAgDIAAgCQABgFgBgKIAAgKQABgMgFgMQgGgOgRgPIgFgEQgDgEABgIQADgLAGgEQACgCgBgMQgBgLgCgDQgBgDgEgDIgJgLQgFgHADgMIABgHQABgCgGgHIgIgJQgDgFgFgMIgEgIIgEgDQgNgPgBgJQgBgHABgKIAAgJIAIABIAAAYQABAHALAMIAEAFIAFAJQADAIAFAHIAHAIQAIAJgBAHIgBAIQgDAIAEAEIAIALIAGAHQADAFABANQABAQgGAFQgEADgBAGQgBAFACAAIADAEQATAPAGARQAGAOgBANQgBAGABADQABAKgBAHIAAACQgBAGgNAIIgLAIQgDADAHAIIADAGQAMAUAAATIACAKQACAKAAAFQgBAHgIAEIgDACQgUAKgBAIIgBAOQABARgGADIgJADIgHADIgBACQgDATgHAEQgEACgEgBIgOgGQgRgHgCAAQgJAFgHAFQgIAHAAAEQgBADADAHQACAHgCADQgCACgDABIgHACQgQAHgKgDQgMgEgRgZIgCgDQgEgGgSgCQgSgCgMAFQgHACgBAEQgBAEACAIIACALIABADQAAAOgDAHQgGANgTAEQgZAHgXAJQgeAMgLAMQgdAdgWAAQgdAAgPgeg");
	this.shape_5.setTransform(200.4,40.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#689F38").s().p("AJqLIIgVgJIgVgIQgJgCgSgOQgOgLgHgHQgSgQgagFQgKgBgSABIgRABIgKAAQgnAAgOAEQgPAEgZgBQgZgBgOgGQgIgCgCAAQgCABgDAGIgHALQgUAagMgGIgFgDQgKgHgFAAQgFAAgIAHQgHAHgEABQgOAFgOgFIgLgBQgPgBgDgEQgIgJgEgaQgBgKAAgeIAAgjQACgPAQABQAGABAJgEQAIgEAEgEQAFgHABhFQABgcAJgZQAHgWAKgHQAGgGADgGQAEgFgCgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgBgBAAAAIgIgBQgIgDgFgHIgCgCQgDgEgCAAQgCADAAADQgBAOgJAFIgCABIgBgBQgMgDgFgIQgCgCgFgQQgFgSgEgCQgHgFgnAPQgLAEgOAOQgHAHgIAFQgHAGgLADQgIADgDADQgEAEgCAJIgCANQgFARgUAXQgHAIgNAWQgCAOgIAGQgGAEgLgDIgJgEQgHgBgeAPIgUAJQgUAIgPgBIgCAAQgEAAgIgGQgKgGgDgGQgEgIADgfQABgVgBgCQgBgDgHgHQgGgFgCgEQgCgEgCgRIgCgPQgCgGgZgKIgDgBQgIgEgCgKQgBgFAAgMIAAgHQAAgEgFgFQgEgGgBgDQgBgFADgKIACgGQACgFgKgGQgPgJgNgCQgGgCgHgGQgFgHgJgFIgSgNQgegXgSgGQgxgSgYgQQgLgHgUABIgHAAQgLAAgIgJQgGgHgFgEQgJgHgVgFQgRgDgFAAQgCAAgJgHIgbgSIg/gnQgEgCgIAKIgDADQgEAFgIAAQgIABgHgGQgHgGgJgaQgJgZAAgNQAAgWgJgcQgDgGgIgNQgNgVAAgFQAAgPAMgJIAFgDIAPgLIAEgHIAJgQIABgBQAHgRAEgFQADgDAJgEIAHgCQADgDAMgZIAEgHQAFgMAAgPQAAgDgFgMIAHgEQAGAOAAAEQAAASgGAOIgDAGQgPAcgDADIgJAEIgJAFQgDAEgHAPIAAABIgKASIgEAGQgEAGgPAIIgEADQgIAEAAANQAAADALATQALAQABAEQAJAdAAAYQAAAMAKAYQAJAXAFAFQADAEAGgBQAEAAADgDIACgCQANgPAIAFIBAAnQARALALAIIAHAFQAGAAARAEQAXAGAKAHIAMALQAGAHAHAAIAHAAIARABQAKABAJAGQAWAOAwASQASAHAgAYIASAMQAKAGAGAHQAEAFAEABQAIABALAGIAMAFQAPAJgDAMIgCAGQgDAGABADIAEAGQAGAJAAAGIAAAIQgBATAHADIADABQAdAMADAKIACAQQABAQACACIAHAHQAIAJACAFQABADgBAXQgDAdADAFQACAFAHAFQAGADAEABIABAAQAQAAAQgHIAUgIQAhgPAJABIAKADQAHACADgCQAFgDACgMIAAgBIAHgLQAHgNAHgHQATgWAEgRIACgMQADgMAFgFQAEgDALgFQAJgDAHgEQAFgDAJgJQARgQAKgEQArgQAKAHQAGAEAHAWQACAKADAEQAFAHAGABQAEgEABgHQAAgKAHgDQAHgCAHAIIACACQAEAGAGABIAFABQAGAAAEAFQAEAGgFAJQgEAHgJAHQgHAGgHATQgJAXAAAcQgBBJgHAIQgFAGgKAEQgKAFgIgBQgJAAgBAHIAAAiQAAAeABAJQADAZAHAHQABABALAAIAOACQAMAEAKgEIAJgHQAJgJAJAAQAJAAAKAIIAFADQAGADAPgUIAHgLQAFgIAEgCQAFgBALADQAMAEAYACQAYABANgDQASgGAnABIAJAAIAQgBQAQgCAOACQAdAGATASQAgAbANAEQAFACARAGIAWAKQAMAGACgBQADgBACgGIAPgnQAHgNgBgFIgCgHIgCgLQAAgFACgEIADgGIAAgCQACgFgBgEQgBgFgJgHQgSgRARg8IAGgqQAEgiACgJQADgPALgJQANgLATABIAcADQAYAFABgDIACgEQAEgJAAgHQABgKgGgJIgNgQQgJgKgDgFQgFgKAAgMQAAgSADgeQADggADgNQAJgkgBgfQgBgQACgXQACgLAAgLQAAgRgCgJQgVABgGgEQgFgCgCACQAAADgFAJIgBACIAAABQAAALgEAHQgEAGgKAEQgHACgFAAIgEAFQgDAEgBAEIgCAGQgCAFAAAIIAAADQAAANgCAFQgDALgJAEQgJAFgKACQgLACgHgEQgGgCgCgFQgCgFAHgNQAIgMgBgIIAAgDQgBgQgDgJQgFgPgMgJQgKgKgMgGQgMgIgFgGQgGgIgCAAIgFAFQgIAMgJgDIgEgBQghgNgMgBQgVAAgOgMIgEgCQgEgEgDgBQgDAAgJAEIgKAEIgLAFQgFAAgEgDQgHgDgcg1QgagSgEgUQgHguATgdIAPgXQAPgZAGgGIAJgGQAFgDABgDQgBgEgGgEIgDAAQgJAAgCgIIgBgFQAAgCgFgEQgFgCgEAAQgGgDgCgCQgFgDAAgIIAAgMQABgMgEgIIgKgNQgTgXAAgMQAAgKgDgNQgDgOgEgIIgFgIQgHgLACgHIAFgIQAGgGABgDQADgHgCgKQgCgKgOggQgRgngBgOQgCgRAKgWQAHgRAKgKQADgEAAgKQgCgTgQgNQgJgIgkgXQghgUgEgFQgIgHgigEIABgIIAVAEQAUAEAFAGQAHAGAdARQAlAYAJAIQAUAQABAWQABALgGAHQgJAKgHAQQgJAUACAPQABAKAQApQAOAhADAKQACANgDAJQgDAGgGAGQgDACgBADQAAACAFAJIAFAJQAFAIADAOQADAOAAAMQAAAJARAWIALANQAGAKgCAPIgBALQABAEACACIAGACQAGABAEADQAJAFAAAHIABADQAAABAAAAQAAAAABABQAAAAABAAQABAAABAAIAGABQAJAGACAHQABADgCAEQgCADgHAEIgIAGQgEAFgQAYIgOAYQgSAaAHAqQAEAVAXANIABAAIABACQAZAxAHAFQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAIgEIALgFQALgFAGABQAFACAGAEIADADQAMAKASAAQAPACAhAMIAEACQADABAFgHIACgDQAFgGAEABQAGAAAIAKQAEAGAMAGQAOAJAIAHQAOAMAFAQQADAJACASIAAADQABALgJANQgFAKABACIADADQAEACAKgBQAJgCAIgEQAGgCACgIQABgEAAgMIAAgDQAAgJADgHIACgEQABgGAEgHQAGgGAEAAIAJgDQAHgCADgFQADgEAAgJIABgDIABgEIAEgJQAAgEAEgCQAGgEAJAFQADABAVAAIABAAQAEAAACAEQACAHAAAXQAAAMgCALQgCAWABAPQABAdgJAoQgDANgDAgQgDAeAAARQAAAKAEAIIALAOIANAQQANATgLAXIgBAFQgCAGgKAAQgGAAgRgDQgQgDgKAAQgQgBgKAJQgJAIgEAMIgFAqIgGArQgQA4APANQAKAJACAIQABAFgCAIIgBABIgDAGIgBAHIACAIIACAJQABAFgIARIgDAHIgMAgQgDAJgGACIgEABQgGAAgLgGg");
	this.shape_6.setTransform(248.3,-41.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#689F38").s().p("AmrEDQgFgCgIgIQgIgIgFgIQgEgHgpAEQgQABgDgCIgIgCQgdgIgCgIIAIgDQABAEAYAHQAIACADACIAOgBQAYgCAKABQAPACADAGQAFAHAHAIQAHAGACABIADgFQABgGAIgGQAFgFABgCQACgFAAghIACgkIgFgcQgFgbACgDQABgDADgBQAJgDAQAHQAcANAKgCIAqgLQBVgWAKgBQALgCAFAEQAFAEAAAMIABAQQADAWAfAQQANAGAHAjIAAAFQACAIAbAHIAFABQALADAHgGIAEgEQAIgLAMgFQAOgHAJgJIABgCQgCgCAAgDIACgIIADgPQAAgHAMgMIACgCIAAgBQAAgFADgHQACgEASgRQASgRAHgFQAIgEAOABQAOACAFAHQADAGALAEQAMAFAKgBQAQgDASgNQAFgEB5h7QACgBAAgIQACgHgCgGQgCgGgNgPQgNgPgEgCIgGgDQgHgEgGAAQgIgCgEgHQgCgGADgDQAEgFANACIAEABIA5AAQAJAAARgHIAWgHQARgEA9glIAAgKQAAgKAbgFIALgCQAigHAVgCQAVgCANgIIAIgHIAHADQgDAFgHAFQgOAKgZACQgZACgdAHIgKACQgTADgCAEIAAAPIgDABQg/AngTAEIgUAHQgVAHgIAAIgRAAQgfABgKgBIgFgBQgHgBgBABIAAABQABADAFABQAIABAGAEIAGADQAHADANAQQAOAQADAHQABAIAAAJQgCALgEADQh1B4gKAHQgSAPgUACQgLACgOgGQgMgFgGgIQgDgEgKgBQgLgBgGADQgGAEgSAQQgQAQgCAEIgCAJIgBADIgEAFQgJAJAAAEQAAAFgDANIgCAFQAEAGgFAGQgKALgQAHQgIAEgJAKIgFAFQgKAIgPgEIgFgBQgOgDgGgDQgMgGgCgJIgBgFQgHgfgJgEQgigTgFgZIAAgRQgBgIgBgCQgDgBgHAAQgNAChRAWIgrALQgLACgfgOQgQgHgDADQgBADAFAXIAFAdIgCAkQAAAhgCAHQgBAFgJAGQgFAFgBACQgBAJgEACIgEABg");
	this.shape_7.setTransform(222.5,-92.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgJACQACgIAIgBQAIgBABAIQgCAIgJAAQgIAAAAgGg");
	this.shape_8.setTransform(197.3,-189.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgEAQQgGgGAAgKIABgQQAAAAABgBQAAAAAAgBQABAAAAgBQABAAAAgBQADgBADAAQAFACAAAHIABALQAAAGAEAEQAEAFgHAFQgEADgDAAQgDAAgBgGg");
	this.shape_9.setTransform(151.4,-190.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAAAmIgHgIIgJgHQgWABgCgWQgBgFgFgSQgHgeAeAGQADABAHAEQAHADACAHIAGAJQAKAKANgHQALgGAEAKQAEAIABAPQABASACAHQACAFgHAEQgPAEgHAAQgNAAgIgJg");
	this.shape_10.setTransform(141.6,-180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgRARQgFgGABgHIAFgLQAUgSASAGIAAAIQgBAFgHgBQgFgBgBAHQgBAEgEADIgHAEIgDADIgEAJQgCgEgEgBg");
	this.shape_11.setTransform(152.9,-197.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgGABQABgIAGAAQAHAAAAAHQAAAHgIABQgHAAABgHg");
	this.shape_12.setTransform(154.3,-45.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgEAFIgBgIQgCgGAGAAQAGABACAKQACAIgHAAQgFAAgBgFg");
	this.shape_13.setTransform(153.7,-42.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgDABQgEgFACgFQADgGAFAFQAIAIgJAPIgFgMg");
	this.shape_14.setTransform(152.2,-38.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgCAIQgIgGAGgKIAGABQAEAAABAFQACAEgFAFQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAAAIgDgBg");
	this.shape_15.setTransform(165,-116);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AADA3QgCgfgMgcQgGgKAEgLQAEgKAAgVQAAgOAJAGQAHAGAAAJQABAGgEAKQgEALAFAPQAKAeABAfIAAAIQgBAFgFAAIAAAAQgGAAgBgMg");
	this.shape_16.setTransform(152.2,-61.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AACASIgEgJQgFgCgCgFQgEgGAEgIQADgHAFAAQAFABABAHIABAMQABAIADAFQADAEgFACQgEAAgCgCg");
	this.shape_17.setTransform(153.3,-51.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AADALQgMgBgBgMQAAgJAJAAQAKAAACAOQABAEgDACQgCADgDAAIgBgBg");
	this.shape_18.setTransform(178.7,-175.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAyBjQgFgHAEgEIAJgHQgVAHgNgPQgSgJgFABQgEABgLAOQgEAEgFAAIgKgBQgFgCgDgFIgGgKIgEgIQgDgFgGACQgBALgCADQgEAGgJgBIABgLQABgGgFgEIAAgGIAGABQAGAFADgBQgEgMAGgKQgQgBgDgEQgEgEACgQIABgGQgBgDgFgBQgIgCADgIQACgGAHgBQAMgBAKAFQACgIgGgDIgMgEIABAAQgEgDAAgCIAEgFQAGgGgBgIIgEgPIgDgIQgCgFAFgEQADgDAFABIAIACIAMAAQAGgBADgFQAFgIAKAEQAEACgDAGQgHAIgBAMIAAAVQgBASgDAGQgEAHgPAEIAAAAIABABQAeABACAfQAEgQAQAAQARAAAFANIAEANQACARAFAEQAEADARABIAKADQADABABAFQABAFADABIACgEQAIgJAJAFQAHAEgDAMIABAIQAAAFgDADIgHAHQgJACgJAAQgIAAgJgCg");
	this.shape_19.setTransform(168.6,-199.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgIAUIgFgIQgCgEACgEQADgEAEACIAHAEIgBgGQgBgDgDgCQgEgCABgEQABgFADgCIAFgFQAFgCADAGIgBAGIgBAEQAAAAAAABQABAAAAABQAAAAABABQABAAAAAAQAHACgFAIQgFAIgGgFIABAGIAAAHQAAADgDABIgCABQgDAAgDgFg");
	this.shape_20.setTransform(167.3,-112.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAKATIgCgGQgEgHgEAAIgLAEQgJADgCgFQgBgCAAgIIADgGIAEgGQAGgLAJAEQAIADAAAHQAAAKAJACQAKAEgDAJIgCAGQgBADgEAAIgBAAQgDAAgCgEg");
	this.shape_21.setTransform(175.3,-154.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgIATQADgHgCgQQAAgJAHgFQACgBAEACQAGAFgFAJQgCADAAAGQACANgMAAIgDAAg");
	this.shape_22.setTransform(156,-201.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgNAeIgIgIQgFgEgHADQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQAAAAABAAQACgDgDgMQgDgIAKgEQADgBAAgPQAAgSAMANIAGAHQADADAGgBQAFAAACAGIADALQADgYAYADIABAFQAKAMgJAHQgCANgCADQgCAEABAIQgBAHgIACQgIACgHAAQgMAAgJgHg");
	this.shape_23.setTransform(157.4,-189);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("ABeBVQgCgBABgEIgUgHQgMgFgDgLQgKADAAgJQgGABgCgFIgDgIQgEgMgFgFQgHgHgKgBQgGAAgDgEQgKgNgcgKQgggLgKgIQgGgEgKAAIgSACQgMADgDgJQgDgLgCgEQgEgHABgEQABgDAGgFQAAgFAEgCIAHgDQAQgEAPAGQARAGARgEQASgEAQASIAJAEQAFABAEgCQAOgFANAFQAMAFAJANQAMADAKAJIAPATIAOAJIAGACQACACgBAEQAFgCgCAFQAGgDgDAGQAMgBABALQAFgCAEADQADADgBAEQAAAFgFACQgEACgFgCIgfgOQAHASATAJQAIADADADQAEAFgBAGQgBAJgPABQgFAAgEAEIgGAIIgGgBg");
	this.shape_24.setTransform(154.5,-125.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("EgDXAiTQgJAAgMgKQgJgIgPAAQgSAAgIgBQgQABgLgDIgEAEIggAAQgegDgFgYQgFgVgTAJQgUALgXgEQgRgCgYgMQgugVgZgDQgTgCgfgPQgNgHAIgJQAFgGgFgEQgIgHgEAEQgHAGgGgDIgLgHIgtghIgJgGQgFgDgBgFIgngpQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQAAAAgBAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgFgCgFgJQgJgUgbgtQgYgpgKgZQgLgZACgSIgBgYIgJgYQgEgNADgMQAAgCgCgEQgDgEACgCQgHAAgCgFIgDgJIgKgbQgFgRgBgMQgCgZgPgSQgFgHgCgKIAAgSQgEgKAAgFQAAgIAKgEQAEgBgCgGIg2ifIgshxQgZhEgFgyQgBgRgPgXQgOgUAPgRQAHgIAEgNIAFgXIAAhHQgDgSgCgEQgLgagJgmIgQhCQgBgFAAgSQABgLALADIACAHQACADgBADQgBARAKAaQANAfACAKIACAFQAehHg3ghQgEgJADgFQAMgYgBgfQgBgUgIgiIgRhCQgJgngCgcQAAgHgGgYQgFgTACgMQAAgCgFgEIgGgHQgRghAFggQADgPgFgXQgDgPAOgHQAIgEACgEQACgEgGgIIgCgMIgDglQgCgVAFgPIgDgJQgFgDgBgHIABgKQACgogXgmQgNgWgHgrQgGgRgHglQgDgRgKgaIgQgqQgIgbgDg5QgCgZgHgPQgLgZAOgbQAHgPgFgHQgFgIgBgNQgBgPgBgIQgDgMAMAAQAPABADgRQAJgtAYgcQADgEACgGIACgLQADgSAKgHQAEgGAHAFQAEAHgDAHQgBAEgGAHQgEAEgDALQgCALgEAEIgEACQgEANAJAIQAFAFgCAAIgGAAQgBgHgDABQgFACAFAHIACAIQABAFAHgCQAFgCACAHQAEAKgIAQQgHAPgLAEQgKARAMAWQAAgMAKgBQANgBACgDQABAAAAAAQAAAAABAAQAAAAABAAQAAABAAAAQABABAAAAQABABAAAAQAAABgBAAQAAABAAAAIgGAJQgEAFgHgCIgDAAQAAAAAAAAQgBABAAAAQAAAAABABQAAAAAAABQgHAFgHAMIgLARQgHAIADAMQADANAAAEIgBAXQAAAMAGAJQAMABACAMQABAJgLABQAAAAgBAAQgBABAAAAQgBABAAAAQAAABAAABIABAFQACAHABANIACAVQADAOAJAGQAGAFAOACQAJACANgEQATgFgCAVIgDAYQgCAOADAMIAThiQACgPgTgNQgKgGgJgOIgPgYQgCgEACgDQADgDAEACQAHAFAEgDQACgCADgHQAIgUARgTQAPgQgPgNQgEgEgEgIIgGgOIAAgEIAAgCQAKgHgDgGIgKgKIgLgNQgGgIgCgIQgCgHACgDQACgFAIAAQAGAAABgFIAAgJQABgeASgMQADgCABgDQAGgWAUgJQAAgGgCABQgMAJgCgHQgBgDACgKQADgTgBgcIAAguIgBgNQABgHAHgDIgBgCIABgCQALgGAFgPQAEgRADgIQgBgJgCgGIgBgCIAAgDQgFgLAEgGQACgRAOgIQAFgEAWgIQALgEADgEQAGgGgBgMQgCgSAVgaQALgNgBgRQAAgHACgNQADgNgBgGIADgoQAAgXgKgQQgSgcACgXQABgFgCgKQgCgLgCgFIgBgKIABgLQgBgGgJAAQgEAAAAgRQAAgQAFgCQAEgCADACQADACAAAEIABAYQAIgOABgHQACgKgHgKQgHgJAEgHQALgIAEgMQACgIABgRQgLgEgFAEIAAgGQAIgIAEgBQAGgBAFALIAKgRQAFgIADgCQAFgEAIAGQAHAFAIgGQAIgHgBgJQgBgfAcgLIACgCQAQAEAKgMQAIgQAGgGIAGgEQAMgSABgcIAEgaQADgQAAgKQAAgKAHgCQAJgBADgGQACgEAAgKQgBgMALgOQALgPADgVQABgNgCgZIgBgPQgBgKgDgGQgDgFADgHIAHgLIAFgMQgJAAgJgIIgOgOIgIgFQgEgCgFADQgFAEgHgEIgLgFQgHAAABgHQADgJAAgDIABgYQAAgPADgJQAEgLADgBQAGgCAagQQAUgMARAFQAIACAGgFQAGgHAEgDQAAgGAIgDIAMgIQAHgEAEgIIAIgTQADgKgDgKQgIgEgGgKQgFgNgEgFQgRgCgCgRIgUgEIgHgCQgEgCACgFQgFACACgGQgCADgEgEQgPgUgagIIgNgGIgJgGQgFgFADgGQAEgLAQAGQAHADABgBQADgBAAgHQABgIAKAFQgBgEgIgGQgHgFAAgGQgMgFgjgEQgSgCgTgeIgIgMQgGgGgJgBQgDAAgEgFIgIgIQgCAAgCAGQgCAFgEgCQgFAHgEAAQgDAAgHgHQgKgFADgKQACgMgGgMIgPgUQgIgLgBgDQgDgJAIgIIABAAQAJgKAFgDQAIgGAKAAQgCgIAIABQADgOATgBQAKAAASgFQATgFAIgBQAdgDAMAAQAWAAASAIQAGADAJAAIAQgCQARgCAAAOQgBAIAEALIAGASQAAAFAFADIAJAEQAEACABgEQAAgFABgBQADgFALACQAKACAFAHQAFAHADABQAEABAIgFQAHgFAOAEQANADAEAHQAKAEAGALIAJAUQAPAHACAQQAFgCgCAFQAWAFAHAYQAFgCgCAFQANABAGAPQAFgBgCAFQAUADAGAQQASAEAIAPQAJABAMAIQANALAFACIAPAHQABAAABABQABAAAAgBQABAAAAAAQAAgBAAAAIAAgFQAAAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgHgCADgEIgTgLQgKgHgGgIQgUgEgGgPQgFABACgFQgKACAEgKQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAIgBABQgEgEgYgRQgRgNgHgMIgEgEIgCgBIgGgLIgGgGQgTgMgFgLQgEABgDgCQgDgCgBgDQgBgGgJgJQgJgIAAgIQgBgGgFAAQgGgBgFAHQgIANgJgMIgcgHQgRgDgIgKQgDgEgKgBQgPgCAAgRQAAgPAFgEQAFgFAPADQAiAHAmgFQAJgCAVAFQAKABgCAIQASAOAIAYQADgBACADIABAEIAFAIQAJACgEAHQAJAFAHANIANAVIAHAIQAEAFgBAGQAMACABAOQAFgDgCAGQAEgBgBAEQAFgBgCAFQAOADACANQAFgDACAGIAJAEQAEADAAAGQAFgBgCAEQAFgCgCAFQAFgBgCAFQAFgCgBAFQAFgCgCAFQAWAGAKAUQAGgBAHAFIAJAIIAZARQAPAKAJAJQAMgBABALQAHgCgBAIQAPAHAEAJQAHAAAFAEQADACAFAHQAHACAJALQABAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIAAAAIAAAAQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAABIACAEQABAAABAAQAAAAABAAQAAABAAAAQAAABAAABIAKAGQAFAEABAGQAJgCADAKIAMAJQAWABAVAWIADADQAGACAJAMQAIAKAIACQAGADAAABQABAIAHAEIANAGIAGAEIAHAGQAJAFADAJIAHAJIADAEIAHAJIALAKQAIACAIALQAFADABAIQACAHAIAKQAKALACAFQAKAKAHACQAKADAAAEQABAKALALQAMAMACAHQAHATAQAPQAJAKACAPQABAPAHAWIAMAlIgBAAQAEACAAAEQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABIAFAFIADAGQABADgDADQgEADgGgHIgEgFIgOgPQgIgIgLADIgIACQgEACACAEIADALQADAFAHgBQAUgCAGANQABAAAAABQABABAAAAQABAAAAABQABAAAAAAIAFgBQAVABAAAQQAAAUAVASQAJAHAAADQAAAFgPACQAOAGALAQQAOAUAGAFIgDADQgdgTgNgLQgHgFgCABQgDABgEAHQgGAMgBAGQAAAIAKAHIAIAFQAEABAFgDQADgDAEAAQAEAAAAAFQgBAGADAJIAFAPQACALgHAAQgKgDgEABQAGARAAAGQAAAMASAJIADAAIACgBQABgJAJgCIAPgDQADgWgGgOQgCgGACgCQADgCAGADIANAFQAGAEADAGQACALANAOQAOAPADAIIAHAOIAJAOQADAFgCACQAZASASAYQAGgJAJABQAKACAFALQANAbAOAfIAFANQADAHAGAEQAGAFgDAFQgBADgJgBIgHgBQgEAAgBAEQgBAFAMACIAKAFQAFACAEgKQABgEAGADQAFACACAEIATAcQADAFgDAEQgEAEgGgFIgLgKQgHgGgIABQAAAJACACQADAFAIABQAQACAIAVIABAEQABADACABQACgCgBgDIgCgFIABgHQAAgDAEgBQAGgCAFAIQAFAIADAIQAIAbARAJQAMAHAAAKQAAAHgFANQgGAkATAZQALANgBARIgBAKQgCAFgGABQgGABgDgGIgEgKQgCgDACgEQADgGgBgCIgIgBQgQgBgCACQgCACACARIABAQQABAJADAGQADAFgBAFQgCAHgHACIgIABQgFABABAEQACARAIAGQAVAPALAEQATAHATgGQAEgBAIACQAIAAACgJQAEgQgEgKIAAgCQgCgGAQgRQAQgRAHABQAMABgBAKQgBAGAEAJIAGAOQADgGgBgHQgBgKAGAAQAFAAADADQADACgBAEQgCAEAGAMQAEAKgHAGIACAEQAPARAGAbIAIAwQAEAYAIAKIADADQAJAIgGAFQgEAEgFgDQgDgBgFgFQgQgUgCgTQgBgGgDgGQgFgIgJADQgGADAFAKIAJAQIAFAHQACAEgEAEQgDAEAEAGQAFAGgBADQALgDACABQACABgBALQgBAEACACQAFABACgDQAEgJAEADQACABADAHQAGAOAAAJQACArAWBAQACAFAAAIQgBAIABAEQAFgCADAEIACAGIAHAcIABAGIACADQAUANAHAZQAEAOADAeQAIArAHARQADAHgBACIgBACIAJAFIALAFQAHADADAFIABAAQAEABABAHQACAHADgBQAMAAAMgIQAIgFAFAFQAGAGgFAHQgIAIgDANIgFAXIAEADIAEgCQACgKAIAAQAHAAACAKQABAPAOAAQAEAAACADQADADgBAEQgBAIgIAAQgIgBgCAFIgDAMQgDAUAMARQAEAHAVAWQAIAIAIgIQAEgEAEABQADACAAAFQgBALAFAOIAJAXQAGgCgDAFQAFgDAGAGQAJAKARAEQAYAHAEACQAOAHAGAIIAEAGQAIAWAQAZQA0A9APA6QAKAaALA6QAKA3ALAcIAIAoQADAQAMAdQAKAXAEAOQAFAVgCASQAAABAAAAQgBAAAAABQAAAAABABQAAAAAAAAQAMAKgEASQgFAWACAGQgLAOAAAXIACAmQAAAFgEADIgHADIAHAHIABAFQADADAAAFIAAAZIAAAPQAAAJgDAGQgGANAFANIALAXQAFALABASIACAcQACAIgHAAIgMgBQABAGAAAMQAAALAHAHQAFAFgBAHQgBAIgLACQgEAAAAADIAAAGQABAPgHARIgQAeIgDAEQgBACABADQALATgKAXIAAAEQAGAFgCAEIgBACQgGAMgFAEQgJAHgBAMIADAWQACALgEALQgCAGgJAOQgDAGgJALIgDAEQAAAAAAABQAAAAAAABQABABAAAAQAAABABAAQAJAHgDAFQgCAEgGgCQgGgCgDADQALARgXACQgKAKABAMQABAJAHAOQAJAPgRAFQABAFgEAFQgGAFABADQgFAFAAAIQgCACAAAJQgCAHgJgHIgIAAQgCABAEAGIAEAIQAAAEgIAAQAHAMAAADQAAAHgPAAQgEAAgCAEQgDAFACAEQAEAPgGAPQgEAKgMAPQgNARgWAMQgHADgBAEQgBAPgMAJQgIAFgQAJIgNAJQgGAGgCAIQgCAHgFgEQgBAHgGAEIgMAGQgTAMgHAMQgHALgOAEIgaAGIgRADQgNADAHAJQAEAGgFAEIgZAXQgPANgQADQgFABgEAEIgGAIIgJAMQgGAFgJgDQgEgBgBAFQAAANgIAJQgHAGgNAGIgQAHQgjAMgOATQgLAQgUACQgMACgWAHQgXAIgLACQgRAEgdAPIgJAEQgGABgDgGQgDgHANgHIAFgEQgCAAgGgFQgFgEgEAFQgCADgHAGQgFAGAHAHQADAEgCAEQgCADgEACIgYAJIgRAFQgJADgGAKQgIAPglACIgpAAQgZAAgQAHQgKAEgOAJIgYANIgLAFQgGACgHgCQgMgFgUAJQgLAFgRAFIgdAHQABAFgFgCQAAATgSgFIgRgEIgCAAIgLADIgLAHQgGAEgDAEQgTAcglgBQgDAVgEAEQgEAEgWAEQgBAAAAAAQgBAAgBABQAAAAAAAAQgBABAAAAQgHAPgQADIgdAFQgcAGgKAEQgUAGgPALQgKAIgJAAIgCAAgACcfUIgCAHQABADADABQAGAAAEgFIAHgKIgLgCIAAAAQgGAAgCAGgASiPuQABAHgDAHIAMgHQACgHgGgFIgKgIIAEANgASWNxQgDANAIALQAJAMgEAMQgBAEACACQABABAFgDQAFgGAAgIIgBgPQgDgLgDgFQgEgHgJAAIgCAAgASjMJQgDAFgMAFQgJAGAFAMQABADgEACQgFABgBACQAKADACAJQABAFgBALQAHACADgEIAEgJQADgJADgMIAEgWQgDgLgDAAIgCABgARZHKQAEAEgCAJQgBAJAGANQAHAPAAAHQAAAFAEgBQADgBABgEQAAgFgCgGIgDgJIgEgQQgBgIAFgHQAHgPABgEQAAgEgFgJQgCABgCAGQgCAEgEABQgEAAgCgEIgDgHQgRgLALgOQAEgFgGgDQgBADgEgCIgBABIgBAAQgDAEgIAAQgJAAgDAEQAGAFAGAKIALARQADAFgFAFQgGAFAAADIgBAGQABACAFgDIAGgHQADgCADAAQADAAACADgAw8GLQAJAUABAHQAFgggWgZIgBgEIgBABQAAABAAAAQAAABABAAQAAABAAAAQABAAAAAAQgCANAJARgARIF9QAEABAHALQAFgBgBgEIgBgHQgEgGAAgJIAAgQQAAgJgCgDQgDgEgJAGIgPAFIAFADQAIACAEAIQADAGgCAGQgCAHgGgDIgKgFIgEACIAGAQQALgGAFAAIABAAgAQWD/QAHABADAKQABAEAEAFIAHAJIAHAGQAFAEACAGQADAHgDACQgJAEAEADIAGAFIAHADQAFACAEgBQgCgEgJggQgGgVgPgJQgZgogNgwQgBgGgFgIIgIgMQgBAGADAIIAFAMQADAKgGAEQgHAGgGgJIgEAHQgCAFgFAAQgIAAgGgLQgDgFgCgLQgCgLgCgFIgDgDIgUgLQgOgHgHgGIgHgDQgMgHgHADQgGADgGAMIgEARQgCAKgEAGQAHgBAJAEIAOAGIAJACQAEgBgBgIIgCgHQAAgEAEgDQAIgEAHAJQAIADALABQAHABAEAEQAEAEgDAGQgCAFABADQABABAFACQAGADAAAGIAAAAQAJASALAHQAIAFAFAHQAGAIgDAFQgHALAHAMQAIAQAAAEQAFgGgCgJIgDgKQAAgGAHgCQAHgDAEAAIADABgAxTAuIAAAGQAEAGAAAIIAAAOQgBAXAQANQAEAEABgBQAEAAAAgGIABgPQgBgIgCgHQgIgWgCglIgDgVQgLAYgCATgAMrgkQgDAMADAIIADARQACALAIAEQADADACAGIADAJQAHAJgCAEQgBAEgLACIgIABQgEABgCAFIAUADQAKABAFgGQAJgLgQgWIgCgCQgRgNgBgYQABgcgEgNQAAAEgFAPgALDkBQAAAJAFADQAVALgIAYQgDAFAHADQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAAAQgBgHAEgRQAEgPgDgJIgCABQgCAJgGAAQgEAAgGgIIgFgMQgDgHgHgCQAAgMgJgYQgIgYABgNQgDAAgFACQgEABgCAAQgHgCgBAEQAAACACAGIAAAMQADABAAAEIAAAFIADAIQABAFgEADQgEAEgEgDIgHgFQgMgGgCAIQgCAFAEACIAIACIA0APQACAEABALgAyWmUQgCAIAEAAQAEABADgEIAFgHIADgGIgGgCQgHAAgEAKgAIvqHQAAAAgBABQAAAAgBABQAAAAAAABQAAAAAAABIACADQAMAHAEAGQAIgCgCgMQgFACgGgGQgDgDgEAAIgEABgAGSqqQgLgPAIgHQANgFADgEQAFgGAJAGQANAIADAAIABgLQAAgHgBAAQgFgCgIgFIgNgIIgCAAIgDACQgKAOgYASQgFADAAAEQAAAEAGADIAVAIIAAAAgAG1tzQAAAIAFAAQAGAAAAgIQABgNgMgWgADfyYQAJAFADAAQADAAAGgFQgHgEgDAAIgCAAQgEAAgFAEgAgt5QQgDADAAAFIAGAjQAQADASAKIAdAUQADgJgGgOQgHgPACgIIgDgBQgHABgDgFIgHgLQgHgNgXgDIgCAAQgDAAgDACgAhB54QAJAPAPAEQAGABABgCQABgCgDgFIgHgFQgFgDAAgEQgEABABgFQgJAAACgJQgDAAgCgCIgEgFIgIgEQgEgCgEABQABgEgDgFIgFgIQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBgBAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAAAgBQgBAAgBAAQAAAAgBgBQAAAAAAgBQgBgBAAAAIABAAIgKgIQgCAJAGAIQADAFAKAIIAJAFQAHAGALAUgAh163QgBgGgCgGQgCgHgDgBQgGgBgKgEIgPgGQgGgBAAADIADAGQARARAZAGIAAAAgAlV8oQAKAEASANQARANAMACQAOACAJALQAQATAPgGQAWgJAPAFQAFABABgEQADgDgEgFQgHgHgEgDQgHgEgJABQgSADgLgQQgFABgCgEQgJgBgCgCQgEgBgBgDIAAgGQgLgIgSgHIgggKIgMgEQgHgDgEgEQgNgNgOAOIAIAFQAFADgCAFQgCAFgFABQgFABgDgDQgKgKgPAEQgPAGgIAAQgNACgIgEQgJgEgGgMQgPgBAAgOIgTgLIgEgCQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgFABACgEQgFABACgFQgEACgDgDQgUgVghgLQgFgDAAgCQgJACAAgJQgFADgCgGIgrgWQgGgEABgDQgLABgCgLQgCACgDgBIgFgDIgFgCQgEgBgCACQgEADADAHQAEALgLAGQAEAFALACQAKACAEAGIAEAGQABADgDACQgEAFgGgHQgJgKgNAGQgGADgDAAQgFAAgBgIQgGAAADgFQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAQgFgBgggTQghgTgDgEIABgDIgKABQgCgJAIgJQAIgKABgGQAEgGAQgFQAEgBABgCIgBgFQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAAAIgEABQgKAAgHAIIgNANQgOAKAHAQQAAAAABAAQAAAAAAABQAAAAAAABQAAABAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQgDAGAGAGQAHAHAAADIAGAGQADAEAEAAIARADQAIAEADAJQAHgDACAJQABAHgIAAQgUABgHgLQgCACgFgCQgFgDgDADQgBAFADAFIAFAIQALgJAEAKQAGANADABIAEADQACADACAAQATAEAUAXQAKAIAHACQAKADALgGQAEgCAFABQAGACABAEQADAHAFACQACACAJABQANADAGAIQAHALANAHQAIAFARAGIAIADQAEACAAAFQABAFgGABIgIACIAAAEQACAFgEACQgBACgGABQgFAAgBAEQArAUAhgXQAHgFAEAHQAGAJAGgCIAMgIQAEgDASgcQANgUAVADIABgDQAEgHAFACQACAAAFAGIAHAIQAEAEAFgCQAKgFAKAAQAHAAAHADgEgLlggEQgFADACADIAFAFQAEAEgBAIIATgMIgGgEQgEgBgBgDQgBgFgFAAQgCAAgFACgEgLOggUIgDAFQAAAAAAABQAAAAABAAQAAAAABAAQABAAABAAQAJgBAEgHIAUgJQAMgDALAHQACACgCgIIgDgDQgGgKgLAEQgOAGgDgBQgEAAgCAFQgCAEABAGIgDAAQgFAAgFACg");
	this.shape_25.setTransform(264.6,4.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#C92B2B").s().p("AjWDXIAAmtIGtAAIAAGtg");
	this.shape_26.setTransform(65.8,146.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#824A4A").ss(8).p("EAdOAlvMg6bAAAMAAAhLdMA6bAAAg");
	this.shape_27.setTransform(253.3,5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("A7zAKIAAgTMA3nAAAIAAATg");
	this.shape_28.setTransform(-261.7,-89);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFCC80").s().p("AgJANQgGgFAAgIQAAgEADgFQAFgGAHAAQAGAAAEADQAGAFAAAHQAAAGgDAEQgFAGgIAAQgEAAgFgDg");
	this.shape_29.setTransform(-151,15.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_30.setTransform(-161.6,10.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFCC80").s().p("AAlAbQAKgHAAgJIgBgDQgCgGgPgDQgMgEgRAAQgQAAgNAEQgOACgCAHIAAADQgBAKAJAGIgNAAQgKgEAAgQQAAgGABgCQAIgOARgGQANgEAVgBQAxAAAKAZQACADAAAFQAAAPgLAFg");
	this.shape_31.setTransform(-182.9,3.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_32.setTransform(-183.6,10.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgKQAIgKAXgSIgsAAIAAgMIBBAAIAAAMIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgEIANAAQAGAKAAALQgBAOgLAJQgKAJgOAAQgPAAgJgIg");
	this.shape_33.setTransform(-195.2,11);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFCC80").s().p("AAQAsQAMAAAJgFQAKgFAEgLQADgMAAgKQgBgNgJgJQgKgLgMABQgVABgEATQACgDAGAAQAGAAAEADQAMAIAAAUQAAASgRAMQgOAKgVAAQgUAAgOgMQgOgNAAgUQAAgUAPgLQgEgBgDgEQgCgFAAgFQAAgKAJgHQAHgFAMAAQAKAAAHAGQAHAGADAKQAMgWAZAAQAWgBANASQAOARAAAXQAAAYgQAQQgQAQgaAAgAgxgHQgHAGAAAIQAAALALAHQAKAFAOAAQAMAAAJgFQAIgFACgJQAAgFgCgEQgDgDgEABQgCAAgCABQgCADAAAEIgNAAIgBgLIABgKIgQAAQgJAAgGAGgAgvgpQAEACACAFQACAFgCAEIAOAAQgBgJgEgEQgEgEgGAAIgFABg");
	this.shape_34.setTransform(-206.7,10.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_35.setTransform(-228.6,10.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFCC80").s().p("AAlAbQAKgHAAgJIgBgDQgCgGgOgDQgNgEgRAAQgQAAgNAEQgOACgCAHIgBADQAAAKAJAGIgNAAQgKgEAAgQQAAgGABgCQAHgOASgGQAMgEAWgBQAxAAAKAZQACADAAAFQAAAPgLAFg");
	this.shape_36.setTransform(-242.9,3.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_37.setTransform(-243.5,10.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgKQAIgKAXgSIgsAAIAAgMIBBAAIAAAMIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgEIANAAQAGAKAAALQgBAOgLAJQgKAJgOAAQgPAAgJgIg");
	this.shape_38.setTransform(-255.2,11);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgFAAQgKAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_39.setTransform(-264.2,12.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgFAAQgKAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_40.setTransform(-282.4,12.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFCC80").s().p("AgOAcIAAgYIgKAAIAAgKIAKAAIAAgPIgLAAIAAgKIAdAAQAJAAAHAFQAGAFAAAJQAAAJgIAEQgIAEgLgBIAAAbgAgBgUIAAAOQAKABAAgIQAAgHgJAAIgBAAg");
	this.shape_41.setTransform(-298.1,3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_42.setTransform(-303.4,10.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgWASgGQgJgDAAgKQAAgMAMgFQAKgGAMABQAQACAJAMQAJAMAAAPIgnAAQgJAAgGAEQgGAGAAAJQABATAbAGQAIACALAAQAXAAANgPQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAVIgOAAQAHgQARgIQAPgJAVAAQAdgBAUAWQATAVAAAeQAAAegRAVQgTAXgeAAQgdgBgSgNgAgfgaIAEAGQACAFgCAEIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_43.setTransform(-318.1,9.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFCC80").s().p("AACBHQgIgEgGgJQgBAHgHAEQgGAEgJABQgQgBgKgRQgIgOAAgVQAAgXANgSQAPgUAYAAQARAAALALQALAMAAAQQAAANgKAIIgGAGQgDADAAAEQAAANASAAQANAAAJgPQAIgNAAgSQAAgXgPgSQgOgSgcAAQgNAAgMAGQgNAGgGALIgPAAQAGgSARgJQAQgJAUAAQAhAAAUATQAJAJAGAQQAHAQAAANQABAigPAUQgNATgVAAQgMAAgHgDgAgoAsQgDADAAADQAAADADADQACADAEgBQAEABACgDQADgDAAgDQAAgDgDgDQgDgDgDAAQgEAAgCADgAgyADQgEAKAAAKQAAAIADAIQAGgMANAAQAKgCAIAIQABgIAHgFQAIgJgBgJQAAgHgFgEQgGgGgIABQgXAAgJARg");
	this.shape_44.setTransform(-332.1,9.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFCC80").s().p("AAlAbQAKgHAAgJIgBgDQgCgGgOgDQgNgEgRAAQgQAAgNAEQgOACgCAHIgBADQAAAKAJAGIgNAAQgKgEAAgQQAAgGABgCQAHgOASgGQAMgEAWgBQAxAAAKAZQACADAAAFQAAAPgLAFg");
	this.shape_45.setTransform(-353.6,3.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_46.setTransform(-354.5,10.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgKQAIgKAXgSIgsAAIAAgMIBBAAIAAAMIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgEIANAAQAGAKAAALQgBAOgLAJQgKAJgOAAQgPAAgJgIg");
	this.shape_47.setTransform(-366.3,11);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFCC80").s().p("AATBGIAAgZQgRABgNAAQgZgBgPgMQgSgNAAgXQAAgLAGgKQAGgIAJgEQgEgCgCgDQgCgDAAgEQAAgHAHgFQAKgJAUAAQAgABAHARQADgHAGgFQAHgGAIAAIAOAAIAAAOIgDgCQgEAAgBAFQAAACAFAEIAJAHQAFAHAAAHQABANgMAIQgLAFgNgBIAAAdIALgDIAJgEIAAANIgJAEIgLADIAAAcgAgfgUQgHAJgBALQABAZAbAFQAGACAJAAIAPgBIAAg8IgfAAQgMAAgHAJgAAigiIAAAWIACAAQAGAAADgDQAFgDgBgGQAAgFgFgIQgGgIAAgFIAAgCQgEABAAARgAgbg2QADADAAAEQAAADgDACIADAAIADAAIAdAAQgCgIgKgDQgGgCgGAAIgLABg");
	this.shape_48.setTransform(-377.4,12);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFCC80").s().p("AgZA1IAAgNQADAEAIgBQAGAAAFgNQAFgNAAgPQgBgQgDgNQgGgOgGAAQgHgBgEAEIAAgOQAFgEANgBQAPABAKATQAIASABAVQgBAUgIARQgLATgPAAQgKAAgHgFg");
	this.shape_49.setTransform(-394.5,10.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFCC80").s().p("AgwA4QgRgSABgcQAAgUAOgOQAPgOAUAAIAPAAIAAAOIgNAAQgPAAgJAJQgKAJAAANQgBASAPALQAOAKATAAQAVgBAOgPQANgOAAgVQAAgVgOgQQgOgRgUgBQgPABgNAGQgMAHgGALIgNAAQAGgSAQgKQAPgKAWABQAdAAATAVQASAVAAAfQAAAfgTAVQgTAUgdABQgdAAgSgSg");
	this.shape_50.setTransform(-403.8,9.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFCC80").s().p("AAlAbQAKgHgBgJIAAgDQgCgGgOgDQgNgEgRAAQgQAAgNAEQgOACgCAHIgBADQAAAKAJAGIgMAAQgLgEAAgQQAAgGACgCQAGgOARgGQANgEAWgBQAxAAALAZQABADAAAFQAAAPgLAFg");
	this.shape_51.setTransform(-417.8,3.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFCC80").s().p("AARAsQALAAAKgFQAJgFADgLQAFgMgBgKQgBgNgJgJQgKgLgMABQgVABgFATQADgDAFAAQAHAAAEADQAMAIAAAUQAAASgRAMQgOAKgUAAQgWAAgOgMQgNgNAAgUQAAgUAPgLQgEgBgDgEQgCgFgBgFQAAgKAJgHQAIgFALAAQALAAAGAGQAIAGACAKQANgWAaAAQAVgBANASQAOARAAAXQAAAYgQAQQgRAQgYAAgAgygHQgGAGAAAIQAAALALAHQAKAFANAAQAMAAAKgFQAJgFABgJQAAgFgCgEQgDgDgDABQgDAAgDABQgCADAAAEIgMAAIAAgLIAAgKIgQAAQgJAAgHAGgAgvgpQAEACACAFQACAFgCAEIAOAAQgBgJgEgEQgEgEgGAAIgFABg");
	this.shape_52.setTransform(-418.3,10.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFCC80").s().p("AAlAbQAJgHAAgJIAAgDQgCgGgPgDQgMgEgRAAQgQAAgMAEQgPACgCAHIgBADQABAKAIAGIgMAAQgLgEAAgQQAAgGACgCQAGgOARgGQANgEAWgBQAxAAALAZQABADAAAFQAAAPgLAFg");
	this.shape_53.setTransform(-433.2,3.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_54.setTransform(-433.2,10.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgWASgGQgJgDAAgKQAAgMAMgGQAKgEAMAAQAQACAJALQAJANAAAPIgnAAQgJAAgGAEQgGAGAAAJQABAUAbAFQAIACALAAQAXAAANgPQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdgBgSgNgAgfgaIAEAGQACAEgCAFIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_55.setTransform(-256.7,-18.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFCC80").s().p("AgzAaQgIgEAAgPIABgHQAIgZAzAAQAZAAANAFQATAGACAOQACAKgHAGQgHAGgLAAQgJAAgGgEQgHgFAAgJQAAgEACgEIgQgBQgnAAgGAMIgBAFQAAAJAIAFgAAeACQAAADACACQADACADAAQAHAAAAgHQAAgDgCgCQgCgCgDAAQgIAAAAAHg");
	this.shape_56.setTransform(-270.7,-24.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgSAOgIQgGgCAAgLQAAgEABgDQAHgPAXAAQAUAAAHAMQAJgMAVAAQAKAAAJAHQAJAGAAALQAAAIgDAEQAMALAAAOQAAAYgRAOQgSAQggAAQggAAgSgRgAgxADQAAAOASAHQANAFASAAQAUAAANgFQARgHABgOQAAgHgFgGQgHAFgNAEQAGACAAAIQAAAGgKAEQgIAEgMAAQgMAAgJgEQgJgEAAgIQAAgOAZAAIALgBQAQgDADgDIg5AAQgUAAABARgAgLAHQAAAEAMAAQAMAAAAgEQAAgFgMAAQgMAAAAAFgAAOgmQgGAEAAAIIAgAAIACgFQAAgDgCgCQgDgHgKAAQgIAAgFAFgAgjgqQAEACABAFQABAFgEAEIAaAAQAAgNgOgEIgHgBQgEAAgDACg");
	this.shape_57.setTransform(-270.8,-16.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC80").s().p("AgOAcIAAgYIgKAAIAAgKIAKAAIAAgPIgLAAIAAgKIAdAAQAJABAHAEQAGAFAAAJQAAAJgIAEQgIAEgLgBIAAAbgAgBgUIAAAOQAKABAAgIQAAgHgJAAIgBAAg");
	this.shape_58.setTransform(-280.3,-24.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAgbANgPQAGgHAHgEQAKgFAKAAIAKAAIAAANQAPgOAWABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgMQgKAHgGACQgIADgLAAIgDAAgAAPArIAFABQAIAAACgFIABgCIgEgOQgCAKgKAKgAAnARIADAIQACAGAAAEQAJgIAAgMQAAgFgHAAQgHAAAAAHgAgrgiQgJAJgBANQgBAQAHAMQAJANARABQALAAAJgFQAJgGABgKQABgFgEgEQgDgGgFABQgFAAgDADQgDACAAAFIABAGIgOAAQgDgOACgNQABgMALgOIgHgBQgMAAgJAJgAAIgSQARAGABAOQAEgGAJgBQAHgBAEAEQgCgMgIgHQgJgJgNgBQgLgBgHAEQgKAFgCAJQADgFAKAAIAHABg");
	this.shape_59.setTransform(-285.2,-16.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgZQAAgYARgQQAQgQAYgBQAaAAAMALQAJAJAAAKIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMALQgNAKAAARQAAAPALAKQAMAJAPABQANAAAKgHQAKgHAAgNQAAgQgOgEQAFAFAAALQAAAGgHAGQgHAGgIAAQgKAAgHgGQgHgHAAgJQAAgMAJgGQAJgHALAAQASAAANALQANAKAAAQQAAAXgPANQgPAOgVABQgXAAgQgRgAgCgMQgDACAAAEQAAAEADACQACADAEAAQAEAAADgDQADgBAAgEQAAgFgDgCQgDgDgEAAQgEAAgCADg");
	this.shape_60.setTransform(-298.3,-16.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFCC80").s().p("AgpBBQgTgLgBgVQgCgVASgJQgFgBgDgDQgEgEAAgFQAAgDACgCQAGgPAdABQARABAIANQAJALgBAQIgnAAQgSAAAAAOQAAALAOAFQAMAFASAAQATgBAMgJQANgLABgPQABgWgLgNQgIgKgPAAIgtAAQgWAAAAgPQAAgZA3AAQAVAAAQAJQATAKAAAQIgOAAQAAgJgPgHQgNgEgQAAQgbAAgCAHQgBAGAKgBIAlAAQAPAAAMAKQAVAQAAAlQAAAagUAQQgTAQgaABQgYAAgPgKgAgegPQADACAAAEQABAGgEADIAbAAQgBgIgGgEQgFgEgJAAIgGABg");
	this.shape_61.setTransform(-310.4,-18.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFCC80").s().p("Ag4AnIAAgVIBkAAIAAglIAIgIQAEgGACgFIAABNg");
	this.shape_62.setTransform(-323.8,-11.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFCC80").s().p("AAfA5QgTAAgJgNQgIANgXAAQgQAAgMgMQgKgNAAgRQAAgSANgKIgRAAIAAgNIARAAQgEgCAAgGIABgGQAGgRAZAAQAUAAAHAOQAJgOATAAQAMAAAHAFQAKAGABAKQABAJgEAFIgDAAQARAOABAVQAAASgMANQgLAOgQAAIgCgBgAAKADIAAANQAAAHAGADQAHADAIAAQAJAAAIgGQAGgHAAgKQAAgIgGgFQgHgGgJAAIg6AAQgJAAgHAGQgHAGAAAIQAAAKAHAGQAGAGAKAAQAJAAAFgCQAIgEAAgHIAAgNgAAcgrQgIAAgFAFQgFAFAAAHIAeAAQACgDAAgEQgBgKgMAAIgBAAgAghgqQAEADAAAFQABAEgDAEIAaAAQgBgJgIgFQgEgDgGAAQgFAAgEABg");
	this.shape_63.setTransform(-324.3,-16.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgWASgGQgJgDAAgKQAAgMAMgGQAKgEAMAAQAQACAJALQAJANAAAPIgnAAQgJAAgGAEQgGAGAAAJQABAUAbAFQAIACALAAQAXAAANgPQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdgBgSgNgAgfgaIAEAGQACAEgCAFIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_64.setTransform(-345.3,-18.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFCC80").s().p("Ag5AnIAAgVIBlAAIAAglIAHgIQAFgGACgFIAABNg");
	this.shape_65.setTransform(-359.1,-11.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_66.setTransform(-359.7,-16.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFCC80").s().p("AATBFIAAgYQgRABgNAAQgZgBgPgMQgSgNAAgXQAAgLAGgKQAGgIAJgEQgEgBgCgEQgCgDAAgEQAAgHAGgFQALgIAUgBQAgABAHARQADgIAGgEQAHgGAIAAIAOAAIAAANIgDgBQgEAAgBAFQAAACAFADIAJAIQAFAHAAAHQABANgMAIQgLAFgNgBIAAAdIAMgDIAIgEIAAANIgJAEIgLADIAAAbgAgfgUQgIAJAAALQABAZAbAFQAGACAIAAIAQgBIAAg8IgfAAQgMAAgHAJgAAigiIAAAWIADAAQAEABAFgEQADgDAAgGQAAgFgFgJQgGgIAAgEIAAgBQgEAAAAARgAgbg2QADADAAAEQAAADgDACIADAAIADAAIAdAAQgCgHgKgEQgGgCgGAAIgLABg");
	this.shape_67.setTransform(-374.4,-15.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAgbANgPQAGgHAHgEQAKgFAKAAIAKAAIAAANQAPgOAWABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgMQgKAHgGACQgIADgLAAIgDAAgAAPArIAFABQAIAAACgFIABgCIgEgOQgCAKgKAKgAAnARIADAIQACAGAAAEQAJgIAAgMQAAgFgHAAQgHAAAAAHgAgrgiQgJAJgBANQgBAQAHAMQAJANARABQALAAAJgFQAJgGABgKQABgFgEgEQgDgGgFABQgFAAgDADQgDACAAAFIABAGIgOAAQgDgOACgNQABgMALgOIgHgBQgMAAgJAJgAAIgSQARAGABAOQAEgGAJgBQAHgBAEAEQgCgMgIgHQgJgJgNgBQgLgBgHAEQgKAFgCAJQADgFAKAAIAHABg");
	this.shape_68.setTransform(-396.1,-16.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFCC80").s().p("AgOAcIAAgYIgLAAIAAgKIALAAIAAgPIgLAAIAAgKIAeAAQAIABAGAEQAHAFAAAJQAAAJgIAEQgIAEgLgBIAAAbgAgBgUIAAAOQAKABAAgIQAAgHgJAAIgBAAg");
	this.shape_69.setTransform(-406.1,-24.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgFAAQgKAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_70.setTransform(-408.7,-15.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgZQAAgYARgQQAQgQAYgBQAaAAAMALQAJAJAAAKIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMALQgNAKAAARQAAAPALAKQAMAJAPABQANAAAKgHQAKgHAAgNQAAgQgOgEQAFAFAAALQAAAGgHAGQgHAGgIAAQgKAAgHgGQgHgHAAgJQAAgMAJgGQAJgHALAAQASAAANALQANAKAAAQQAAAXgPANQgPAOgVABQgXAAgQgRgAgCgMQgDACAAAEQAAAEADACQACADAEAAQAEAAADgDQADgBAAgEQAAgFgDgCQgDgDgEAAQgEAAgCADg");
	this.shape_71.setTransform(-420.4,-16.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFCC80").s().p("Ag4AKQgDgHAAgIQAAgHADgFIAMAAQgCAEAAAEQABAEADAEQALALAfAAQASAAANgEQAHgEAEgCQAFgFgBgHQABgIgHgHIAKgGQALAIgBARQAAAIgCAFQgLAdgvAAQgpAAgPgYg");
	this.shape_72.setTransform(-433.1,-10.3);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_73.setTransform(-433.2,-16.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFCC80").s().p("AgNAXQALgHAAgGIgBgBIgGgEQgGgFAAgGQAAgFADgEQAFgHAHAAQAFAAAFADQAGAEAAAJQAAAFgDAGQgBAEgGAGQgFAGgDACg");
	this.shape_74.setTransform(-152.1,-39.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFCC80").s().p("AgaA1IAAgNQAFADAGAAQAHAAAGgNQADgNAAgQQAAgPgDgNQgGgOgGAAQgHgBgFAEIAAgOQAGgEAMAAQAPAAALATQAJASAAAUQAAAWgJAQQgLASgPABQgKAAgIgFg");
	this.shape_75.setTransform(-158.9,-44.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_76.setTransform(-168.5,-44.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFCC80").s().p("AAlAbQAJgHAAgJIAAgDQgCgGgPgEQgMgDgRAAQgQABgMADQgPADgCAGIAAADQAAAKAIAGIgNAAQgKgFAAgPQAAgFABgDQAIgOAQgGQAOgFAVABQAxgBALAZQABADAAAFQAAAPgLAFg");
	this.shape_77.setTransform(-182.7,-51.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_78.setTransform(-183.4,-44.3);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFCC80").s().p("AgoBJQgYgBgPgQQgQgPAAgXQAAgQAGgLQAGgLANgMIAZgXQAJgLADgGIAXAAQgEAIgHAIQgGAIgMAHQARgBAMAEQAiANAAAoQAAAagTAQQgQAQgaAAIgDAAgAhEgQQgKAMABANQAAAOAJAKQAMAMAVAAQAQAAALgIQAOgMgBgRQAAgIgCgHQgDgLgMgGQgKgFgMAAQgWAAgMANgAAiA9QgIgMAAgTQAAgZAUgfQAJgOAOgSIhIAAIAAgNIBaAAIAAAMQgPARgKAQQgQAaAAAWQAAAXATAAQAGAAAFgEQAFgDABgHQAAgEgDgGIAMAAQAFAGAAANQAAAPgLAJQgKAJgPAAQgQAAgKgMg");
	this.shape_79.setTransform(-201,-45.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFCC80").s().p("AgtA9QgSgRAAgbQAAgSAIgNQAJgOAPAAQAPAAACAMQAFgOAPAAQALAAAIAKQAIAJAAAMQAAAPgKAKQgJAKgOAAQgMAAgJgHQgJgHAAgLIACgSQACgHgIAAQgFAAgEAJQgDAGAAAJQAAATAOALQANALATAAQAUAAANgOQAOgNAAgUQAAgPgHgLQgHgMgOAAIhNAAIAAgMQAUABAAgCQAAgBgEgBQgEgCAAgEQAAgPAQgFQAJgDAVAAQAQAAALAEQAOAEAKALQAHAIAAANIgNAAQgCgNgNgGQgMgGgRAAQgMAAgFACQgIACAAAHQAAABAAAAQAAABAAAAQABABAAAAQABABAAAAQABABAAAAQABAAAAAAQABABABAAQAAAAABAAIAjAAQAMAAAIAEQAaALAAApQAAAegSATQgSAUgcAAQgaAAgTgSgAgPADQAAAGAFAEQAEAFAHAAQAHAAAGgEQAFgFAAgIQgFAIgKABQgMAAgGgLIgBAEgAAAgPQgBACAAADQAAADABACQACACADAAQAAAAABAAQABgBAAAAQABAAAAAAQABgBAAAAQADgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_80.setTransform(-224.6,-46.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgJQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgDIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_81.setTransform(-235.1,-44.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_82.setTransform(-247,-44.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFCC80").s().p("AgYAwQgLgKAAgQQAAgMAFgJQADgFAIgJQAIgLAXgRIgsAAIAAgOIBBAAIAAANIgPALIgNAMIgMAPQgGAJABAJQABAHAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgDIANAAQAGAJAAALQgBAOgLAJQgKAIgOABQgPgBgJgHg");
	this.shape_83.setTransform(-258.8,-44.1);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFCC80").s().p("AATBFIAAgYQgRABgNAAQgZgBgPgMQgSgNAAgXQAAgLAHgJQAFgJAJgEQgEgBgCgEQgCgDAAgEQAAgHAGgFQALgIAUAAQAgAAAHASQADgJAGgFQAHgEAJAAIANAAIAAAMIgDgBQgEABgBAEQAAACAFADIAJAIQAFAHAAAHQABANgMAIQgLAFgNgBIAAAdIALgDIAJgEIAAANIgJAEIgLADIAAAbgAgfgUQgIAIAAAMQABAZAbAFQAGACAIAAIAQgBIAAg8IgfAAQgMAAgHAJgAAigiIAAAWIACAAQAFABAEgEQAEgDAAgFQAAgHgFgIQgGgHAAgFIAAgBQgEABAAAQgAgag2QACADAAAEQAAAEgDACIADgBIADAAIAdAAQgCgHgKgEQgGgCgGAAIgKABg");
	this.shape_84.setTransform(-269.9,-43.1);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_85.setTransform(-291.7,-44.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFCC80").s().p("AAlAbQAKgHAAgJIgBgDQgCgGgOgEQgNgDgRAAQgQABgNADQgOADgCAGIgBADQAAAKAJAGIgNAAQgKgFAAgPQAAgFABgDQAHgOASgGQAMgFAWABQAxgBAKAZQACADAAAFQAAAPgLAFg");
	this.shape_86.setTransform(-306.3,-51.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_87.setTransform(-307.2,-44.3);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFCC80").s().p("AgvA8QgTgNABgZQABgWASgGQgJgDAAgKQAAgMAMgGQAKgEAMABQAQAAAJAMQAJANAAAPIgnAAQgJAAgGAEQgGAGAAAJQABATAbAGQAIACALAAQAXAAANgPQANgOAAgUQABgXgNgQQgOgRgaAAQgdgBgQAVIgOAAQAHgPARgKQAPgIAVgBQAdAAAUAWQATAVAAAeQAAAegRAVQgTAWgeABQgdAAgSgOgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_88.setTransform(-322.2,-45.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_89.setTransform(-336.1,-44.3);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFCC80").s().p("AgqBBQgSgLgBgVQgCgWASgIQgFgBgDgDQgEgEAAgFIABgFQAHgQAdACQARABAIANQAJALgBAQIgnAAQgSAAAAAOQAAALAOAFQAMAEARABQAUAAAMgLQANgJABgQQABgWgLgNQgIgKgPAAIhEAAIAAgLQAPAAAAgCQAAAAAAAAQAAgBgBAAQAAgBgBAAQAAgBgBAAQgEgCgBgEQgCgRA0gBQAVAAARAJQATAKAAAQIgPAAQABgJgPgHQgNgEgQAAQgYAAgBAHQgBAFAKAAIAfAAQAPAAAMAKQAVAQAAAlQAAAagUARQgTAQgaAAQgYAAgQgKgAgegPQADACAAAEQABAGgEADIAbAAQgCgIgFgEQgGgEgJAAIgFABg");
	this.shape_90.setTransform(-356.7,-46);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgZQAAgZARgPQAQgRAYABQAagBAMALQAJAJAAAKIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMALQgNAKAAARQAAAPALAJQAMALAPAAQANAAAKgHQAKgHAAgNQAAgQgOgEQAFAFAAALQAAAHgHAFQgHAGgIAAQgKAAgHgHQgHgGAAgJQAAgMAJgGQAJgHALABQASAAANAKQANAKAAAQQAAAXgPANQgPAPgVAAQgXAAgQgRgAgCgMQgDACAAAEQAAAEADACQACADAEAAQAEAAADgDQADgBAAgEQAAgFgDgCQgDgDgEgBQgEABgCADg");
	this.shape_91.setTransform(-368.8,-44.3);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFCC80").s().p("AgaA1IAAgNQAFADAHAAQAHAAAFgNQADgNAAgQQABgPgEgNQgFgOgHAAQgGgBgGAEIAAgOQAGgEAMAAQAQAAAJATQAJASAAAUQAAAWgJAQQgKASgQABQgJAAgIgFg");
	this.shape_92.setTransform(-377.3,-44.3);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_93.setTransform(-387.5,-44.3);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFCC80").s().p("AgdAeQgMgMAAgSQAAgQAMgNQAMgMARAAQARAAAMAMQANAMAAARQAAARgMANQgMAMgSAAQgQAAgNgMgAgTgTQgFAHAAAIQAAAMAKAHQAGAFAIAAQANAAAHgKQAFgGAAgIQAAgMgKgIQgGgFgJAAQgLAAgIAKg");
	this.shape_94.setTransform(-399.9,-42.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFCC80").s().p("AgwAyQgSgWAAgdQABgfATgUQATgUAfABQAbAAASARQASARAAAbQAAAUgMAQQgNAQgUABQgUAAgMgNQgMgNACgRQABgRANgKIgPAAIAAgOIAzAAIAAAOIgJAAQgKAAgHAGQgFAGgBAJQAAAIAGAHQAHAGALAAQANgBAHgKQAIgIgBgNQgBgRgIgKQgMgOgZAAQgVAAgMASQgLAPAAAXQAAAVALAQQALARATADIAKABQAYAAANgUIAQAAQgQAlgtgBQgfABgTgXg");
	this.shape_95.setTransform(-411.2,-42.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFCC80").s().p("AgzAaQgIgEAAgPIABgHQAIgZAzAAQAZAAANAFQATAGACAOQACAKgHAGQgHAGgLAAQgJAAgGgEQgHgFAAgJQAAgEACgEIgQgBQgnAAgGAMIgBAFQAAAJAIAFgAAeACQAAADACACQADACADAAQAHAAAAgHQAAgDgCgCQgCgCgDAAQgIAAAAAHg");
	this.shape_96.setTransform(-432.4,-51.6);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFCC80").s().p("Ag4AKQgDgHAAgIQAAgHADgFIANAAQgCAEgBAEQAAAEAEAEQAMALAeAAQARAAANgEQAIgEAEgCQAEgFABgIQgBgHgGgGIAJgHQALAIAAARQAAAIgCAEQgLAegvAAQgqAAgOgYg");
	this.shape_97.setTransform(-432.3,-37.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAgbANgPQAGgHAHgEQAKgFAKAAIAKAAIAAANQAPgOAWABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgMQgKAHgGACQgIADgLAAIgDAAgAAPArIAFABQAIAAACgFIABgCIgEgOQgCAKgKAKgAAnARIADAIQACAGAAAEQAJgIAAgMQAAgFgHAAQgHAAAAAHgAgrgiQgJAJgBANQgBAQAHAMQAJANARABQALAAAJgFQAJgGABgKQABgFgEgEQgDgGgFABQgFAAgDADQgDACAAAFIABAGIgOAAQgDgOACgNQABgMALgOIgHgBQgMAAgJAJgAAIgSQARAGABAOQAEgGAJgBQAHgBAEAEQgCgMgIgHQgJgJgNgBQgLgBgHAEQgKAFgCAJQADgFAKAAIAHABg");
	this.shape_98.setTransform(-432.7,-44.3);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_99.setTransform(-237.5,-116.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AgEByQgQAXgfAAQgkAAgTgeQgSgbABgtQACg1AfgbQAYgVAwABIAOAAIAAAXQgYAAgLABQgQADgMAIQgYASgBAhQgBAcAJARQALAUAWAAQARAAAJgLQAJgJAAgQIAAgfIAYAAIAAAfQAAAkAlAAQAYAAANgdQAMgbgCgoQgBgogUgbQgbgkgxgBQgcAAgWAMQgXALgJATIgcAAQAIgdAfgRQAegSAoAAQAgAAAbAMQAUAKANAOQAlApABBHQACA1gVAjQgXAlgoAAQggAAgOgXg");
	this.shape_100.setTransform(-264.6,-119.6);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_101.setTransform(-282.7,-116.4);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AA6BpQglAAgRgZQgPAZgpAAQgdAAgUgXQgVgXABgfQABgiAWgRIgfAAIAAgYIAfAAQgGgEAAgMQAAgFACgFQALgfAsAAQAmAAAMAaQAQgaAkAAQAVAAAOAJQARALACASQACARgHAJIgFAAQAfAZAAAnQAAAhgVAYQgUAYgeAAIgBAAgAARAGIAAAXQAAANANAGQALAGAQAAQAQAAANgMQAOgNgBgRQgBgPgLgLQgMgKgQAAIhrAAQgRAAgMALQgNALAAAQQAAARAMAMQAMALASAAQAQAAAKgFQAOgHAAgMIAAgYgAAyhPQgOABgIAJQgJAJgBANIA3AAQAEgFAAgIQgCgTgXAAIgCAAgAg8hMQAGAGACAIQAAAIgEAHIAtAAQgBgRgOgJQgIgGgLAAQgJAAgGADg");
	this.shape_102.setTransform(-300.6,-116.4);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AguBpQgkgCgXgXQgYgXgBghQgBgfAKgWIAYAAQgCAIADADQADAEAIAAQAMAAAJgGQATgMAFgPIABgJQAAgQgLgFQgKgEgJAEQAPAEAAATQAAALgHAGQgHAHgLAAQgGAAgEgCQgTgGAAgXQAAgHADgIQAKgbAmAAQAPAAALAHQAMAHAEAMQAVgcArACQAtACAWAqQAQAcgBAhQAABGgzAZQgQAIgSAAQgLAAgOgDIAAgRQgQAMgNAEQgNAEgUAAIgFAAgAAYgaQAYATAAAgQAAAggUAVIAQACQASAAAJgHQAbgUgBgoQAAgagSgVQgRgUgagBQgggCgPAUIADgBQATAAANAMgAhnAPQAAAQAJALQANARAdAAIASgBQATgCANgPQALgNAAgQQAAgKgFgHQgGgLgRgBQgIAAgNAIQgRALgEACQgMAFgMAAQgLAAgGgFIgBALg");
	this.shape_103.setTransform(-339.8,-116.4);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_104.setTransform(-358.6,-116.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AhdBLQgcgYAAgrQAAgRAIgPQAHgQAMgHQgMgGAAgPQAAgGACgFQAKgZAsAAQAkAAAOAVQAPgVAnAAQASAAAOAJQAPAIADAQQACAMgHAMQAJAIAHAOQAHAQAAARQAAArgfAaQgiAcg5AAQg7AAgigegAhUgXQgJALAAARQACAaAgANQAYAKAiAAQAjAAAYgKQAfgNACgaQAAgRgJgLQgKgKgQAAIhyAAQgQAAgKAKgAAahKQgLAHgBALIA5AAIABgGQAAgIgHgFQgIgFgIAAIgCAAQgLAAgKAGgAhBhPQAGAEABAHQACAHgFAFIAtAAQgCgTgagFIgKgBQgGAAgFACg");
	this.shape_105.setTransform(-376.1,-116.4);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AhYBbQghgnABg3QABg4AigkQAjgkA5AAQAxAAAhAfQAgAgAAAxQAAAmgVAcQgXAeglABQglABgVgYQgWgXADgiQACgfAYgRIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMALQANAMATgBQAXgBAOgSQANgQgBgZQgBgegPgSQgWgagtAAQgnAAgWAiQgUAbAAAqQAAAnAUAcQAUAfAjAGIASABQAsABAYgjIAdAAQgeBBhSAAQg3AAgkgpg");
	this.shape_106.setTransform(-401.4,-113.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("AAECAQgOgGgLgQQgDAMgMAIQgMAIgRAAQgcAAgRgfQgQgbAAglQAAgsAZghQAbgjArAAQAgAAAUAUQAUAUAAAfQAAAYgTAQIgKAJQgFAGgBAHQgBAYAhAAQAZAAAQgaQANgYAAgiQAAgrgZggQgaghg0AAQgZAAgVALQgXALgLATIgbAAQALgfAfgSQAcgRAlAAQA9AAAkAkQAQAQAMAdQAMAdAAAZQACA+gbAkQgYAigmAAQgWAAgNgGgAhJBQQgFAEAAAHQAAAGAFAFQAFAEAGAAQAHAAAEgEQAFgFAAgGQAAgHgFgEQgFgFgGAAQgGAAgFAFgAhaAGQgIAQAAAUQAAAPAEANQALgUAYgCQASgBAQANQACgOALgLQAPgOAAgQQgBgOgKgJQgKgJgPAAQgqAAgPAhg");
	this.shape_107.setTransform(-427.1,-119.3);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AhXBmQggggABg0QABgkAagZQAagaAlAAIAcAAIAAAYIgYAAQgcAAgRARQgRARgBAYQgBAhAbAUQAaASAjgBQAmAAAZgbQAYgbAAgmQAAgngZgeQgZgdgmgCQgbAAgXAMQgXAMgKAUIgYAAQAMggAcgSQAcgSAoAAQA1AAAiAoQAgAmAAA4QAAA5ghAlQgjAng2AAQg1AAgfggg");
	this.shape_108.setTransform(-102,-169.4);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("Ag6B6QgZgMgOgVQgWggAAguQAAggARgaQASgbAYACQAYABAHARQALgUAagBQAXAAAQASQAQARABAYQACAbgVAUQgVAWgbAAQgagBgSgQQgRgRABgXQAAgIADgJQAGgJABgEQABgFgDgEQgEgEgGACQgLABgHAPQgGAOgBASQgBAjAbAXQAaAXAkAAQAkAAAbgcQAbgagBgmQgBgtgWgbQgYgdgtgBQg4AAgWApIgbAAQANgfAdgSQAbgPAkAAQA4gBAeAgQAlAkACA8QACA7ghApQgiApg5AAQgeAAgagNgAgjgKQAAALALAIQALAHANABQAPAAAMgKQAMgJAAgOIgBgHQgCAIgKAHQgKAGgMAAQgcAAgGgZQgFANAAAEgAgGgnQAAAGAEAEQAEAEAGABQAGAAAFgFQAEgDAAgHQAAgFgEgFQgFgFgGABQgOAAAAAOg");
	this.shape_109.setTransform(-127.2,-169.4);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgGAAgKQAAgFACgFQALgbA1ADQAfABAQAXQAQAUgCAeIhHAAQgiAAABAaQAAAUAZAKQAVAIAhAAQAjAAAXgSQAXgSADgcIABgQQAAgdgSgUQgQgTgcAAIhTAAQgnAAAAgaQAAgtBkAAQA8AAAWAQQAQAMAAARQAAAQgOAKQAgAkgBA4QgBAvgkAeQgiAegwAAQgrAAgdgSgAg5gdQAGAFABAIQABAJgHAGIAyAAQgEgPgJgGQgLgIgQAAQgGAAgFABgAAuhmQgEAFAAAFQAAAHAEAEQAEAEAGAAQAGAAAEgEQAFgEAAgHQAAgFgFgFQgEgDgGAAQgGAAgEADgAg5hfQgCAJATAAIA5AAQgDgMAGgLIgYgBQgzAAgCAPg");
	this.shape_110.setTransform(-151.6,-169.5);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("ABEAwQAQgMAAgQIgBgFQgDgNgagFQgXgGgfAAQgdAAgYAGQgaAGgEAMIAAAGQAAARAPAKIgWAAQgUgHAAgdQAAgKADgFQAMgaAggKQAXgIAoAAQBaAAARAsQAEAGAAAJQAAAcgUAIg");
	this.shape_111.setTransform(-189.4,-179.6);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_112.setTransform(-191,-166.5);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgXAIgQQAGgKAOgRQAQgTAqggIhQAAIAAgYIB3AAIAAAWIgcAWQgNAKgLALQgRAUgFAIQgKAQABASQABAMAIAGQAIAGAKAAQAWAAAHgSQACgGAAgGQAAgHgGgIIAYAAQAMARgBAWQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_113.setTransform(-212.6,-166.1);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFFF").s().p("AAiB+IAAgtQgeADgXgBQgvgCgcgVQgggXABgqQAAgVALgRQALgQAQgHQgIgDgEgGQgDgGAAgHQAAgNALgJQAUgQAlABQA6ABANAfQAFgOAMgJQAMgJAPAAIAYAAIAAAXIgEgBQgIAAgBAIQAAADAIAGQAMAKAEAFQAKALAAAOQABAYgWAOQgTAKgYgCIAAA1IAVgGIAQgGIAAAXIgRAHIgUAFIAAAygAg5glQgOAPAAAVQABAuAzALQAJACARAAQASAAAJgCIAAhtIg5AAQgVAAgNAQgAA+g/IAAAoIAEABQAKAAAHgGQAHgGAAgKQAAgLgKgOQgKgPAAgGIAAgEQgIACAAAdgAgxhjQAEAGAAAHQAAAGgEAEIAEgBIAGAAIA2AAQgEgOgSgHQgNgEgLAAQgJAAgJADg");
	this.shape_114.setTransform(-232.7,-164.3);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAaQAUABAAgNQAAgOgQAAIgEAAg");
	this.shape_115.setTransform(-263.3,-180.6);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_116.setTransform(-272.9,-166.5);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_117.setTransform(-300,-166.5);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACguQACgmAfgOQgQgFAAgRQAAgWAWgLQASgIAXABQAcACARAWQASAXgBAcIhIAAQgQAAgLAIQgLAJABARQACAkAwAKQAPADAVAAQAoAAAZgcQAYgYAAgmQABgqgYgcQgZgfgwgBQg1gCgcAoIgaAAQANgeAegQQAdgQAlAAQA2AAAjAmQAkAnAAA4QAAA3ggAmQgiApg2AAQg2gBgggXgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgMgJgJQgKgJgOAAQgIABgHACg");
	this.shape_118.setTransform(-325.3,-169.3);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAaQAUABAAgNQAAgOgQAAIgEAAg");
	this.shape_119.setTransform(-342.6,-180.6);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_120.setTransform(-352.6,-166.5);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFFFFF").s().p("AgEByQgQAXgfAAQgkAAgTgdQgSgcABgtQACg0AfgbQAYgWAwABIAOAAIAAAXQgYAAgLABQgQACgMAJQgYASgBAhQgBAcAJARQALAUAWAAQARAAAJgLQAJgJAAgRIAAgeIAYAAIAAAeQAAAlAlAAQAYAAANgdQAMgbgCgoQgBgogUgbQgbgkgxgBQgcABgWAKQgXAMgJATIgcAAQAIgdAfgRQAegSAoAAQAgAAAbAMQAUAKANAOQAlAoABBIQACA1gVAjQgXAlgoAAQggAAgOgXg");
	this.shape_121.setTransform(-380.1,-169.7);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgGAAgKQAAgFACgFQALgbA2ADQAeABAQAXQAQAUgCAeIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgTQAXgSACgcQACgpgUgXQgQgTgbAAIhTAAQgnAAAAgaQAAgtBkAAQAmAAAeAPQAiASABAeIgbAAQABgSgcgLQgXgIgdgBQgzAAgCAOQgCAJATAAIBDAAQAbAAAWASQAmAeAABDQAAAvglAeQgjAegvAAQgrAAgdgSgAg5gdQAGAFABAIQABAJgHAGIAyAAQgDgPgKgGQgKgIgRAAQgGAAgFABg");
	this.shape_122.setTransform(-404.7,-169.5);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgHQgGgGAAgKQAAgFACgFQALgbA2ADQAeABAQAXQAQAUgCAeIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgTQAXgSACgcQACgpgUgXQgQgTgbAAIhTAAQgnAAAAgaQAAgtBkAAQAmAAAeAPQAiASABAeIgbAAQABgSgcgLQgXgIgdgBQgzAAgCAOQgCAJATAAIBDAAQAbAAAWASQAmAeAABDQAAAvglAeQgjAegvAAQgrAAgdgSgAg5gdQAGAFABAIQABAJgHAGIAyAAQgDgPgKgGQgKgIgRAAQgGAAgFABg");
	this.shape_123.setTransform(-428.3,-169.5);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_124.setTransform(-237.3,-230.7);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_125.setTransform(-245.6,-216.6);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFFFFF").s().p("AhCBLQgegdAAgtQAAguAegdQAdgeAsAAQAwAAAWAVQAPANABAUIgYAAQgBgNgTgJQgRgHgWgBQgdABgWASQgXAVAAAeQAAAcAUASQAVASAdAAQAXAAASgMQATgOAAgWQAAgegbgIQAKAKAAAUQAAAMgMAKQgNAKgPAAQgSAAgNgLQgNgLAAgTQAAgUARgNQAPgLAWABQAgAAAYASQAWATAAAfQAAAngbAaQgbAagmAAQgqAAgdgegAgEgYQgGAFAAAHQAAAIAFAEQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_126.setTransform(-268.7,-216.5);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_127.setTransform(-293.2,-216.5);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgSIA2AAQARgBAKAJQAMAJAAARQAAAPgOAIQgPAIgVgBIAAAygAgDglIAAAbQAUgBAAgNQAAgNgQAAIgEAAg");
	this.shape_128.setTransform(-312.1,-230.7);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_129.setTransform(-312.8,-216.5);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AhYBbQghgnABg4QABg3AigkQAjgkA5AAQAxAAAhAgQAgAfAAAxQAAAngVAbQgXAeglABQglAAgVgXQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMAMQANALATgBQAXAAAOgTQANgQgBgZQgBgegPgRQgWgbgtAAQgnAAgWAiQgUAcAAApQAAAmAUAdQAUAgAjAFIASABQAsAAAYgiIAdAAQgeBBhSAAQg3AAgkgpg");
	this.shape_130.setTransform(-329.9,-213.8);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AhCBLQgegdAAgtQAAguAegdQAdgeAsAAQAwAAAWAVQAPANABAUIgYAAQgBgNgTgJQgRgHgWgBQgdABgWASQgXAVAAAeQAAAcAUASQAVASAdAAQAXAAASgMQATgOAAgWQAAgegbgIQAKAKAAAUQAAAMgMAKQgNAKgPAAQgSAAgNgLQgNgLAAgTQAAgUARgNQAPgLAWABQAgAAAYASQAWATAAAfQAAAngbAaQgbAagmAAQgqAAgdgegAgEgYQgGAFAAAHQAAAIAFAEQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_131.setTransform(-352.7,-216.5);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AhkBrQgUgYABggQACgmAagQIgiAAIAAgYIArAAQgIgDAAgNQAAgGACgFQANgWAkAAIAQAAQAcADARAVQAPAUgBAdIhLAAQgUAAgOAMQgOAMgBATQAAARAMAMQAMAMAUAAQAOAAAKgJQALgJAAgPIAAgbIAXAAIAAAcQAAAOAJAJQAKAJAPAAQAqgBAEhHQADgtgcgjQgcgjgqgBQg/gBgXAoIgcAAQAfg/BRABQAxABAiAdQAYAVAMAhQAMAggCAiQgCAugRAdQgVAkglAAQggAAgQgTQgQATggAAIgBAAQgfAAgVgYgAgtgzQAFAIAAAGQgBAGgEAEIAzAAQgHgRgLgEQgKgEgMAAIgLABg");
	this.shape_132.setTransform(-388.8,-219.2);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgXAIgQQAGgKAOgRQAQgTAqggIhQAAIAAgYIB3AAIAAAWIgcAVQgNAKgLAMQgRAUgFAIQgKAQABASQABAMAIAGQAIAGAKAAQAWAAAHgTQACgFAAgGQAAgHgGgIIAYAAQAMASgBAVQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_133.setTransform(-408.2,-216.1);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACguQACgmAfgOQgQgEAAgSQAAgWAWgLQASgIAXABQAcACARAWQASAXgBAcIhIAAQgQAAgLAIQgLAKABAPQACAlAwAKQAPADAVAAQAogBAZgaQAYgZAAgmQABgpgYgdQgZgfgwgCQg1gBgcAoIgaAAQANgeAegQQAdgPAlAAQA2gBAjAmQAkAoAAA3QAAA3ggAmQgiApg2AAQg2AAgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgMgJgJQgKgJgOAAQgIABgHACg");
	this.shape_134.setTransform(-427.7,-219.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.lf(["#C4291D","#E26D45"],[0,1],-504.9,0,504.9,0).s().p("EhO4AAtIAAhZMCdxAAAIAABZg");
	this.shape_135.setTransform(-0.7,-279.5);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#2B0912").s().p("EhO4AsYMAAAhYvMCdxAAAMAAABYvg");
	this.shape_136.setTransform(-0.7,0);

	this.instance = new lib.Path();
	this.instance.parent = this;
	this.instance.setTransform(-0.7,282.5,1,1,0,0,0,504.9,23.5);
	this.instance.alpha = 0.539;

	this.instance_1 = new lib.Bitmap1_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-640,-360);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.slide1, new cjs.Rectangle(-640,-360,1280,720), null);


(lib.slider_mov = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// main_slider
	this.instance = new lib.slide1();
	this.instance.parent = this;

	this.instance_1 = new lib.slider2();
	this.instance_1.parent = this;

	this.instance_2 = new lib.Bitmap27();
	this.instance_2.parent = this;
	this.instance_2.setTransform(187,115,1.073,1.073);

	this.instance_3 = new lib.Bitmap26();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-28,115,1.073,1.073);

	this.instance_4 = new lib.Bitmap25();
	this.instance_4.parent = this;
	this.instance_4.setTransform(211,-72,1.153,1.147);

	this.instance_5 = new lib.Bitmap24();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-28,-72,1.194,1.194);

	this.instance_6 = new lib.Bitmap23();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-28,-221,0.866,0.866);

	this.instance_7 = new lib.Bitmap22();
	this.instance_7.parent = this;
	this.instance_7.setTransform(163,-221);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgFgLgFgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape.setTransform(84.7,95.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1.setTransform(76.8,95.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAfAVQAHgFAAgGIAAgDQgCgGgMgCQgKgDgOAAQgMAAgLAEQgMACgCAFIAAADQAAAHAHAEIgKAAQgJgDAAgNQAAgEABgBQAGgMAOgFQAKgEASAAQAoAAAJAVQABACAAADQAAANgJADg");
	this.shape_2.setTransform(65.2,89.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_3.setTransform(64.6,95.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AghA8QgTgBgMgNQgNgMgBgTQABgNAEgJQAFgJALgKIAUgTQAHgJADgFIATAAQgDAHgGAHQgFAGgJAGQANgBAKAEQAbAKAAAhQAAAUgOAOQgOANgUAAIgEAAgAg3gMQgIAJAAALQAAALAIAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgCgJgJgFQgIgEgLAAQgRAAgKALgAAcAyQgGgKAAgQQAAgTAQgaQAHgLALgPIg6AAIAAgLIBJAAIAAAKQgMAOgIANQgOAVAAASQAAATAQAAQAFAAAFgDQAEgDAAgFQAAgEgDgEIAKAAQAFAEAAALQAAAMgJAHQgIAIgMAAQgOAAgIgKg");
	this.shape_4.setTransform(50.1,94.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgXAZQgKgKAAgPQgBgNAKgKQALgLANAAQAOAAAKAKQALAKAAAOQAAAOgKAKQgLALgOAAQgNAAgKgKgAgPgQQgFAGAAAHQAAAJAJAGQAFAEAGAAQAKAAAHgIQAEgFAAgGQAAgKgJgHQgFgEgHAAQgJAAgGAIg");
	this.shape_5.setTransform(32.6,96.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_6.setTransform(23.2,95.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAKAAAMIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_7.setTransform(11.7,94.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_8.setTransform(4,89.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_9.setTransform(0.1,95.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgHAKQgFgDAAgHQAAgDACgEQAEgGAGAAQAEAAAEAEQAFAEAAAFQAAAFgDADQgEAGgGgBQgEAAgDgDg");
	this.shape_10.setTransform(-13.8,99.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgeAzQgPgIAAgQQgBgHAFgFQAEgFAHAAQAHAAAEAEQAEADAAAHQAAAMgPABQAJAIAPAAQAKAAAHgFQAIgGAAgKQAAgNgJgFQgGgDgOAAIAAgMQARAAAGgGQAEgEAAgIQABgIgHgFQgGgGgHAAQgJAAgIAEIgFADQALADAAAJQAAAGgEAEQgEADgGAAQgHAAgEgEQgEgFAAgGQAAgOAQgIQANgGAPAAQAQAAALAGQAOAIAAAOQAAAMgIAGQgFAEgOAEQAOADAHAEQAJAHAAANQABARgRAJQgOAIgRAAQgRAAgMgHg");
	this.shape_11.setTransform(-22.4,95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_12.setTransform(110,-90.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgXALgMQAFgFAGgEQAHgEAJAAIAIABIAAAKQAMgLASAAQAUABAKANQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgKQgIAGgEABQgHADgJAAIgDAAgAANAjIAEABQAGAAACgEIAAgCIgDgLQgCAIgHAIgAAgAOIACAHQACAEAAAEQAHgHAAgJQAAgFgFAAQgGAAAAAGgAgjgbQgHAHgBAKQgBANAGAKQAHALAOAAQAJABAIgFQAHgFAAgIQABgEgDgDQgDgEgDAAQgEAAgDADQgCABAAAEIAAAFIgLAAQgCgLABgLQABgKAJgLIgGgBQgKAAgHAIgAAGgPQAOAFACAMQACgFAIgBQAGgBADADQgBgJgHgGQgHgIgLAAQgJgBgGADQgIAEgCAIQADgFAIAAIAFABg");
	this.shape_13.setTransform(98.1,-90.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgXAZQgLgKAAgPQABgNAJgKQALgLANAAQAOAAAKAKQAKAKAAAOQAAAOgJAKQgKALgPAAQgNAAgKgKgAgPgQQgEAGAAAHQAAAJAIAGQAFAEAGAAQAKAAAHgIQAEgFAAgGQAAgKgIgHQgGgEgHAAQgJAAgGAIg");
	this.shape_14.setTransform(88.4,-89.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_15.setTransform(78.9,-89.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_16.setTransform(60.8,-90.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAeAWQAIgGAAgHIgBgCQgBgGgMgCQgKgCgOAAQgMAAgLACQgMACgBAGIgBACQAAAIAHAFIgKAAQgJgEAAgMQAAgFACgCQAFgLAOgFQALgEARABQAoAAAIATQACACAAAFQAAAMgJAEg");
	this.shape_17.setTransform(48.6,-96.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgnApQgOgSAAgYQAAgZAPgQQARgRAYAAQAWAAAPAPQAPAOAAAWQAAARgKAMQgLAOgPAAQgSABgIgLQgLgLACgOQABgOAKgIIgMAAIAAgKIAqAAIAAAKIgIAAQgIAAgGAFQgEAFAAAHQAAAHAEAFQAGAGAJgBQALAAAGgJQAGgGgBgLQgBgOgGgHQgKgMgUgBQgRABgKAPQgJAMAAATQAAAQAJAOQAJAOAPACIAJABQATAAALgQIANAAQgNAeglgBQgZAAgQgSg");
	this.shape_18.setTransform(48.7,-89);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgfAVgNQAIgEALAAIAGAAIAAALQANgMARABQAUAAAKAOQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgJQgIAFgEACQgGACgJAAIgEAAgAAHgPQAQAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgVgCgDAPQAEgEAHAAIAFABgAglgaQgHAJAAALQAAANAHAIQAHAJANAAQAKABAHgFQAHgEABgIQAAgFgDgDQgDgEgEAAQgDAAgDADQgCACAAADIABAFIgLAAQgDgLACgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_19.setTransform(37,-90.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgLAXIAAgTIgJAAIAAgIIAJAAIAAgNIgJAAIAAgIIAXAAQAIAAAFAEQAFAEAAAHQAAAIgGACQgHAEgJAAIAAAWgAgBgQIAAAMQAIAAAAgGQAAgGgHAAIgBAAg");
	this.shape_20.setTransform(28.8,-96.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgIAAgGgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_21.setTransform(26.6,-88.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgOAtQgFgBgFgFQgDgFAAgHIAAgFIALAAIgBADQAAAFADADQAEACAFAAQAGAAAFgKQAEgJAAgLQAAgjgJAAQgEgBAAAFQAAABAAAAQAAAAAAABQABAAAAABQAAAAABABQAGAHgBAHQABARgSAAQgQAAAAgbQAAgOAFgNIAOAAQgEAEgDAHQgCAIAAAFQAAAOAIAAIADgCQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAIgCgEQgGgFAAgIQAAgDACgEQABgDADgDQAFgEAJAAQAMAAAIARQAEANAAARQABATgJANQgKAOgOAAQgIAAgEgCg");
	this.shape_22.setTransform(18.4,-90.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgpAiQgNgLAAgTQAAgIAEgGQADgIAGgCQgGgDAAgHIABgFQAEgLAVAAQAPAAAGAJQAHgJARAAQAJAAAGADQAHAFABAHQABAFgDAGQAEADADAGQADAIAAAHQAAATgOALQgPANgaAAQgaAAgPgNgAglgKQgEAFAAAIQABALAOAGQALAEAPAAQAPAAALgEQAOgGABgMQAAgHgEgFQgEgEgIAAIgzAAQgGAAgFAEgAAMggQgFACgBAFIAaAAIABgCQAAgEgDgCQgEgCgEgBQgGAAgEAEgAgdgjQADACAAADQABADgCACIAUAAQgBgIgLgDIgEAAIgGABg");
	this.shape_23.setTransform(9.3,-90.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgeAhQgNgMAAgVQAAgUANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgDgKgBQgMABgKAIQgLAKABANQgBAMAKAIQAJAIANAAQAKAAAIgGQAJgGAAgJQAAgNgMgDQAEADAAAJQAAAFgFAFQgGAEgHABQgIAAgFgGQgGgFAAgHQAAgKAIgFQAGgFAKAAQAOAAALAIQAKAIAAAOQAAASgNALQgMAMgRAAQgSAAgOgOgAgBgLQgDADAAADQAAADADACQABACADAAQADAAADgCQACgBAAgDQAAgEgCgCQgDgDgDABQgDAAgBABg");
	this.shape_24.setTransform(-1,-90.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgHAKQgFgEAAgGQAAgEACgDQAEgGAGABQAEAAAEACQAFAEAAAGQAAAEgDAEQgEAGgGAAQgEAAgDgEg");
	this.shape_25.setTransform(-13.8,-86.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAHA4IgHgDQgUgKgFAAQgIAAgCAMIgKAAQgBgUAHgLQAGgLASgIQAPgFAGgEQAKgIABgLQAAgKgIgGQgHgFgJgBQgPAAgIAPIAGgBQAGgBADAEQAEADAAAGQAAAGgFAEQgEAEgHAAQgHAAgEgGQgFgFAAgIQAAgPAOgKQANgIAQAAQAQAAANAJQAOAJABARQAAAQgJAGQgHAGgRAFQgSAEgFADQgLAFgHAMQAJgFAGAAQAFAAALADIATADQAKAAADgGQACgEABgLIAKAAQAAAqgbABQgGAAgGgCg");
	this.shape_26.setTransform(-22.4,-91);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_27.setTransform(216.3,-237.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgXALgMQAFgFAGgEQAHgEAJAAIAIABIAAAKQAMgLASAAQAUABAKANQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgKQgIAGgEABQgHADgJAAIgDAAgAANAjIAEABQAGAAACgEIAAgCIgDgLQgCAIgHAIgAAgAOIACAHQACAEAAAEQAHgHAAgJQAAgFgFAAQgGAAAAAGgAgjgbQgHAHgBAKQgBANAGAKQAHALAOAAQAJABAIgFQAHgFAAgIQABgEgDgDQgDgEgDAAQgEAAgDADQgCABAAAEIAAAFIgLAAQgCgLABgLQABgKAJgLIgGgBQgKAAgHAIgAAGgPQAOAFACAMQACgFAIgBQAGgBADADQgBgJgHgGQgHgIgLAAQgJgBgGADQgIAEgCAIQADgFAIAAIAFABg");
	this.shape_28.setTransform(204.4,-237.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgXAZQgLgKAAgPQAAgNALgKQAJgLAOAAQAOAAAKAKQAKAKAAAOQAAAOgJAKQgKALgPAAQgNAAgKgKgAgPgQQgEAGgBAHQABAJAIAGQAFAEAGAAQALAAAFgIQAFgFAAgGQAAgKgIgHQgGgEgHAAQgJAAgGAIg");
	this.shape_29.setTransform(194.7,-236.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_30.setTransform(185.2,-236.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgFAtIgLgEQgFgCgEAAQgJAAABAHIgLAAQgCgHAAgJQAAgJACgGQADgJAIgGIAOgLQAHgFABgIQAAgDgCgFQgDgDgFAAQgEAAgEADQgDADAAADQgBALAIAEIgPAAQgEgGAAgHIAAgDQABgJAGgFQAHgFAJAAQAKAAAGAGQAGAFAAAJQAAAJgFAIIgNAJQgJAIgCADQgGAHAAAIIAAADIAIgBQAFAAAJADQAJADAFAAIAKgBQAFgCAEgEQAGgHAAgIQABgHgGgFIgMgMQgGgGAAgJQAAgJAHgGQAGgEAJAAQAIAAAFAEQAGAFAAAIQABAFgDAFQgCAEgFAAQgEABgEgDQgDgCAAgFQAAgGAGgDQgCgCgEAAQgDAAgDACQgDADAAAEQAAAIAOAGQARAIAAARQAAAPgMAKQgLAJgPAAQgIAAgGgCg");
	this.shape_31.setTransform(168.7,-237.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAKgEQAHgDAKAAQAPAAAGAJQAEgIANgBIAIAAIAFgFIAEgFQAGgKAAgKIAQAAIgDALQgCAGgEAFIgJAKQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACg");
	this.shape_32.setTransform(158.3,-238.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQgBgMgCgLQgFgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_33.setTransform(150.2,-237.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgDA8QgUgBgMgNQgNgMAAgTQAAgNAFgJQAEgJALgKIAVgTQAHgJABgFIAUAAQgEAHgGAHQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgagMQgIAJAAALQABALAHAIQAKAKAQAAQAMAAAKgHQAMgJAAgPQAAgGgDgFQgDgJgJgFQgIgEgKAAQgRAAgKALg");
	this.shape_34.setTransform(143.1,-238.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgCAzQgGALgPAAQgQAAgJgNQgIgNABgUQABgXAOgMQAKgKAWAAIAHAAIAAALIgQAAQgIABgFAEQgLAIAAAPQgBAMAFAIQAEAJAKAAQAIAAAEgFQAEgEAAgIIAAgNIAKAAIAAANQAAARARAAQALAAAGgNQAFgMgBgSQAAgSgJgMQgNgQgVAAQgNAAgKAFQgKAFgEAIIgMAAQADgNAOgIQANgIATAAQAOAAAMAGQAJAEAFAGQARATABAfQAAAYgJAQQgKARgSAAQgPAAgGgLg");
	this.shape_35.setTransform(126.4,-238.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgBA/QgVgBgMgMQgMgNAAgTQgBgMAFgKIACgCQAFgIAJgIIAUgUQAGgIADgFIATAAQgDAHgGAGQgFAGgJAGQANgBAKAEQAcALAAAgQAAAVgPAOQgOANgUAAIgCgBgAgYgJQgIAJAAALQAAAMAHAHQALAKAPAAQANAAAJgHQAMgJAAgOQAAgGgCgGQgDgJgJgEQgIgFgLAAQgRAAgJALgAgoghIgJAAIAAgJIAJAAIAAgMIgJAAIAAgJIAZAAQAHAAAFAEQAFAEAAAIQAAAHgGAEQgHAEgKgBIAAAOQgGAGgEAHgAgeg2IAAAMQAKAAAAgGQAAgGgIAAIgCAAg");
	this.shape_36.setTransform(115.4,-238.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_37.setTransform(104.6,-236.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_38.setTransform(90.6,-237.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgEAAIgDABg");
	this.shape_39.setTransform(82.3,-237.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_40.setTransform(64.4,-237.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgXALgMQAFgFAGgEQAHgEAJAAIAIABIAAAKQAMgLASAAQAUABAKANQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgKQgIAGgEABQgHADgJAAIgDAAgAANAjIAEABQAGAAACgEIAAgCIgDgLQgCAIgHAIgAAgAOIACAHQACAEAAAEQAHgHAAgJQAAgFgFAAQgGAAAAAGgAgjgbQgHAHgBAKQgBANAGAKQAHALAOAAQAJABAIgFQAHgFAAgIQABgEgDgDQgDgEgDAAQgEAAgDADQgCABAAAEIAAAFIgLAAQgCgLABgLQABgKAJgLIgGgBQgKAAgHAIgAAGgPQAOAFACAMQACgFAIgBQAGgBADADQgBgJgHgGQgHgIgLAAQgJgBgGADQgIAEgCAIQADgFAIAAIAFABg");
	this.shape_41.setTransform(52.6,-237.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgXAZQgLgKAAgPQABgNAJgKQAKgLAOAAQAOAAAKAKQALAKAAAOQAAAOgKAKQgKALgPAAQgNAAgKgKgAgPgQQgFAGABAHQAAAJAIAGQAFAEAGAAQAKAAAHgIQAEgFAAgGQAAgKgJgHQgFgEgHAAQgJAAgGAIg");
	this.shape_42.setTransform(42.8,-236.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_43.setTransform(33.3,-236.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgFAtIgLgEQgFgCgEAAQgJAAABAHIgLAAQgCgHAAgJQAAgJACgGQADgJAIgGIAOgLQAHgFABgIQAAgDgCgFQgDgDgFAAQgEAAgEADQgDADAAADQgBALAIAEIgPAAQgEgGAAgHIAAgDQABgJAGgFQAHgFAJAAQAKAAAGAGQAGAFAAAJQAAAJgFAIIgNAJQgJAIgCADQgGAHAAAIIAAADIAIgBQAFAAAJADQAJADAFAAIAKgBQAFgCAEgEQAGgHAAgIQABgHgGgFIgMgMQgGgGAAgJQAAgJAHgGQAGgEAJAAQAIAAAFAEQAGAFAAAIQABAFgDAFQgCAEgFAAQgEABgEgDQgDgCAAgFQAAgGAGgDQgCgCgEAAQgDAAgDACQgDADAAAEQAAAIAOAGQARAIAAARQAAAPgMAKQgLAJgPAAQgIAAgGgCg");
	this.shape_44.setTransform(16.8,-237.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAKgEQAHgDAKAAQAPAAAGAJQAEgIANgBIAIAAIAFgFIAEgFQAGgKAAgKIAQAAIgDALQgCAGgEAFIgJAKQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACg");
	this.shape_45.setTransform(6.4,-238.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_46.setTransform(-1.7,-237.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJACgFIATAAIgKAOQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJABALQAAALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_47.setTransform(-8.8,-238.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgHAKQgFgDAAgHQAAgDACgEQAEgGAGAAQAEAAAEAEQAFAEAAAFQAAAFgDADQgEAGgGgBQgEAAgDgDg");
	this.shape_48.setTransform(-16.3,-233.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AghA3IAAgNIAKABQAHAAADgCQACgCAAgHIAAg5IgWAAIAAgNQANAAAGgDQAGgDAGgKIAOAAIAABWQAAAHACACQACACAHAAIALgBIAAANg");
	this.shape_49.setTransform(-23.6,-238.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#C92B2B").s().p("Ah8B9IAAj5ID5AAIAAD5g");
	this.shape_50.setTransform(-62,145.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#824A4A").ss(8).p("EAqCApkMhUDAAAMAAAhTHMBUDAAAg");
	this.shape_51.setTransform(211.5,2.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("A7zAKIAAgTMA3nAAAIAAATg");
	this.shape_52.setTransform(-261.5,-90);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFCC80").s().p("AgJANQgGgFAAgIQAAgEADgFQAFgGAHAAQAGAAAEADQAGAFAAAHQAAAGgDAEQgFAGgIAAQgEAAgFgDg");
	this.shape_53.setTransform(-131.1,-13.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_54.setTransform(-141.7,-17.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFCC80").s().p("AAlAaQAKgGAAgIIgBgEQgCgHgOgDQgNgCgRAAQgQgBgNAEQgOACgCAHIgBAEQAAAJAJAFIgMAAQgLgDAAgQQAAgGACgCQAGgOARgGQANgFAWAAQAxAAAKAZQACADAAAFQAAAPgLAEg");
	this.shape_55.setTransform(-163,-24.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_56.setTransform(-163.6,-17.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFCC80").s().p("AgYAvQgLgIAAgQQAAgNAFgJQADgGAIgJQAIgJAXgTIgsAAIAAgMIBBAAIAAAMIgPAMIgNALIgMAQQgGAIABAKQABAGAEAEQAFADAFAAQAMAAADgKIABgGQAAgEgCgFIANAAQAGAKAAAMQgBANgLAIQgKAKgOgBQgPABgJgJg");
	this.shape_57.setTransform(-175.3,-17.5);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFCC80").s().p("AARAsQALAAAJgFQAKgFAEgLQADgMAAgKQgBgNgJgJQgJgLgNABQgVABgFATQAEgDAEAAQAHAAAEADQAMAIAAAUQgBASgQAMQgOAKgVAAQgUAAgPgMQgNgNAAgUQAAgUAPgLQgEgBgDgEQgDgFAAgFQABgKAIgHQAJgFAKAAQALAAAGAGQAIAGACAKQANgWAaAAQAVgBAOASQANARAAAXQAAAYgQAQQgQAQgZAAgAgygHQgGAGAAAIQAAALALAHQAKAFANAAQANAAAJgFQAIgFACgJQAAgFgCgEQgDgDgEABQgCAAgDABQgCADAAAEIgMAAIAAgLIAAgKIgQAAQgJAAgHAGgAgugpQADACACAFQACAFgCAEIAOAAQgBgJgEgEQgEgEgGAAIgEABg");
	this.shape_58.setTransform(-186.8,-17.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_59.setTransform(-208.7,-17.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFCC80").s().p("AAlAaQAKgGgBgIIAAgEQgCgHgOgDQgNgCgRAAQgQgBgNAEQgOACgCAHIgBAEQAAAJAJAFIgMAAQgLgDAAgQQAAgGACgCQAGgOARgGQANgFAWAAQAxAAALAZQABADAAAFQAAAPgLAEg");
	this.shape_60.setTransform(-222.9,-24.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgGAAQgJAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_61.setTransform(-220.9,-16.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFCC80").s().p("AACBHQgIgEgGgIQgBAGgHAEQgGAFgJgBQgQABgKgSQgIgOAAgVQAAgXANgSQAPgUAYAAQARAAALAMQALALAAAQQAAANgKAIIgGAGQgDADAAAEQAAANASAAQANAAAJgOQAIgOAAgSQAAgXgPgSQgOgSgcAAQgNAAgMAGQgNAGgGALIgPAAQAGgRARgKQAQgKAUABQAhgBAUAUQAJAJAGAQQAHAQAAANQABAigPAUQgNASgVAAQgMAAgHgCgAgoAsQgDADAAADQAAADADADQACADAEAAQAEAAACgDQADgDAAgDQAAgDgDgDQgDgCgDAAQgEAAgCACgAgyADQgEAJAAALQAAAIADAHQAGgKANgCQAKAAAIAHQABgHAHgHQAIgHgBgKQAAgGgFgFQgGgFgIgBQgXAAgJASg");
	this.shape_62.setTransform(-234.2,-19.3);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgYQAAgaARgQQAQgQAYAAQAaAAAMAMQAJAHAAALIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMAKQgNAMAAAPQAAAPALAKQAMALAPgBQANAAAKgGQAKgIAAgMQAAgQgOgEQAFAGAAAKQAAAHgHAFQgHAGgIAAQgKAAgHgHQgHgGAAgKQAAgLAJgHQAJgFALgBQASAAANALQANAKAAARQAAAVgPAPQgPAOgVgBQgXAAgQgQgAgCgNQgDADAAAEQAAAEADACQACADAEAAQAEAAADgDQADgCAAgEQAAgEgDgDQgDgDgEAAQgEAAgCADg");
	this.shape_63.setTransform(-247,-17.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFCC80").s().p("AgwA4QgRgSABgdQAAgSAOgPQAPgOAUABIAPAAIAAANIgNAAQgPAAgJAJQgKAJAAANQgBASAPALQAOAKATgBQAVABAOgQQANgOAAgVQAAgVgOgQQgOgQgUgCQgPAAgNAHQgMAGgGAMIgNAAQAGgSAQgKQAPgJAWAAQAdgBATAWQASAVAAAeQAAAggTAVQgTAUgdAAQgdAAgSgRg");
	this.shape_64.setTransform(-266.5,-19.3);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_65.setTransform(-281.1,-17.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_66.setTransform(-296.4,-17.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFCC80").s().p("AgUAUQgJgHgCgLIgBgFQAAgOAJgIIAAAOIAFAAQAAAIAGAFQAGAGAHAAQAIAAAFgHQAGgGgBgIIANAAIABAGQAAAOgKAKQgJAJgOAAQgLAAgJgGg");
	this.shape_67.setTransform(-307.4,-10.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_68.setTransform(-312,-17.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFCC80").s().p("Ag4AKQgEgHAAgHQABgIADgEIANAAQgCADAAADQAAAGADADQAMAMAeAAQASgBAMgFQAIgCAEgEQAEgEABgIQgBgHgGgHIAJgGQALAJAAAQQAAAIgCAEQgLAegvAAQgqAAgOgYg");
	this.shape_69.setTransform(-334.1,-11.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_70.setTransform(-335.1,-17.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFCC80").s().p("AgOAbIAAgWIgLAAIAAgLIALAAIAAgOIgLAAIAAgKIAdAAQAKgBAFAFQAHAFAAAJQAAAJgIADQgIAFgLAAIAAAbgAgBgUIAAAOQAKAAAAgGQAAgIgJAAIgBAAg");
	this.shape_71.setTransform(-345.7,-25.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgSAOgIQgGgCAAgLQAAgEABgDQAHgPAXAAQAUAAAHAMQAJgMAVAAQAKAAAJAHQAJAGAAALQAAAIgDAEQAMALAAAOQAAAYgRAOQgSAQggAAQggAAgSgRgAgxADQAAAOASAHQANAFASAAQAUAAANgFQARgHABgOQAAgHgFgGQgHAFgNAEQAGACAAAIQAAAGgKAEQgIAEgMAAQgMAAgJgEQgJgEAAgIQAAgOAZAAIALgBQAQgDADgDIg5AAQgUAAABARgAgLAHQAAAEAMAAQAMAAAAgEQAAgFgMAAQgMAAAAAFgAAOgmQgGAEAAAIIAgAAIACgFQAAgDgCgCQgDgHgKAAQgIAAgFAFgAgjgqQAEACABAFQABAFgEAEIAaAAQAAgNgOgEIgHgBQgEAAgDACg");
	this.shape_72.setTransform(-350.4,-17.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgYQAAgaARgQQAQgQAYAAQAaAAAMAMQAJAHAAALIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMAKQgNAMAAAPQAAAPALAKQAMALAPgBQANAAAKgGQAKgIAAgMQAAgQgOgEQAFAGAAAKQAAAHgHAFQgHAGgIAAQgKAAgHgHQgHgGAAgKQAAgLAJgHQAJgFALgBQASAAANALQANAKAAARQAAAVgPAPQgPAOgVgBQgXAAgQgQgAgCgNQgDADAAAEQAAAEADACQACADAEAAQAEAAADgDQADgCAAgEQAAgEgDgDQgDgDgEAAQgEAAgCADg");
	this.shape_73.setTransform(-363,-17.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFCC80").s().p("AgOAbIAAgWIgLAAIAAgLIALAAIAAgOIgLAAIAAgKIAeAAQAIgBAGAFQAHAFAAAJQAAAJgIADQgIAFgLAAIAAAbgAgBgUIAAAOQAKAAAAgGQAAgIgJAAIgBAAg");
	this.shape_74.setTransform(-371.2,-25.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_75.setTransform(-376.4,-17.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_76.setTransform(-398.8,-17.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFCC80").s().p("AgZA2IAAgOQAEADAGAAQAIAAAEgNQAFgNgBgQQAAgOgDgNQgFgPgHgBQgHAAgEAEIAAgOQAFgFANAAQAOAAALAVQAIARABAUQgBAVgIARQgLASgPAAQgKAAgHgDg");
	this.shape_77.setTransform(-409.2,-17.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFCC80").s().p("AgCA+QgIANgSAAQgUAAgKgQQgKgPABgZQABgcARgPQANgMAbAAIAIAAIAAANIgUABQgJABgGAFQgOAKAAASQgBAPAFAJQAGALAMAAQAJAAAGgGQAFgFAAgJIAAgQIAMAAIAAAQQAAAUAVAAQANAAAHgQQAHgOgBgWQgBgWgLgPQgPgUgaAAQgQAAgMAGQgMAGgGAKIgPAAQAFgPAQgKQARgJAWAAQASAAAOAHQALAFAHAHQAVAXAAAnQABAdgLATQgNAUgWAAQgRAAgIgNg");
	this.shape_78.setTransform(-418.7,-19.5);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFCC80").s().p("Ag3AKQgFgHAAgHQAAgIAEgEIAMAAQgBADAAADQAAAGADADQALAMAfAAQASgBAMgFQAIgCAEgEQAFgEgBgIQABgHgHgHIAKgGQAKAJABAQQgBAIgCAEQgLAegvAAQgqAAgNgYg");
	this.shape_79.setTransform(-432.8,-11.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_80.setTransform(-432.9,-17.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFCC80").s().p("AgZA2IAAgOQADADAIAAQAGAAAFgNQAEgNABgPQAAgPgEgNQgGgPgGgBQgGAAgFAEIAAgOQAGgFAMAAQAPAAAJAVQAJARAAAVQAAAUgJARQgKASgQAAQgJAAgHgDg");
	this.shape_81.setTransform(-200.7,-45.2);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_82.setTransform(-210.3,-45.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFCC80").s().p("AAlAaQAKgGAAgJIgBgDQgCgHgOgCQgNgDgRAAQgQAAgNADQgOADgCAGIgBADQAAAKAJAFIgMAAQgLgDAAgQQAAgFACgDQAGgOASgGQAMgEAWgBQAxABAKAYQACADAAAFQAAAPgLAEg");
	this.shape_83.setTransform(-224.5,-52.5);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_84.setTransform(-225.2,-45.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFCC80").s().p("AgoBJQgZgBgPgQQgPgPAAgXQAAgQAGgLQAGgLANgMIAZgXQAIgLAEgGIAXAAQgEAIgHAIQgGAIgMAHQARgBAMAEQAiANAAAoQAAAagSAQQgRAQgaAAIgDAAgAhDgQQgLAMAAANQAAAOAKAKQANAMATAAQAQAAALgIQAOgMABgRQAAgIgEgHQgCgLgLgGQgKgFgNAAQgWAAgLANgAAiA9QgIgMAAgTQAAgZAUgfQAIgOAPgSIhHAAIAAgNIBZAAIAAAMQgQARgJAQQgQAaAAAWQAAAXASAAQAHAAAGgEQAEgDAAgHQABgEgDgGIAMAAQAFAGAAANQAAAPgKAJQgLAJgOAAQgRAAgKgMg");
	this.shape_85.setTransform(-242.8,-46.8);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFCC80").s().p("AgZA5QgUAAgNgNQgMgNgBgSQgBgRAGgMIANAAQgBAFACABQABACAFAAQAGAAAFgDQALgGACgJIABgEQAAgKgGgCQgGgDgFADQAJACAAAKQAAAGgEAEQgEADgGAAIgGAAQgKgEAAgMQAAgFABgEQAGgPAVAAQAIAAAHAEQAGAEACAHQALgQAYABQAYABANAYQAJAPgBASQAAAmgcAOQgJAEgKAAIgNgBIAAgKQgJAHgHACQgHADgKAAIgEgBgAANgOQANAKAAARQAAASgLAMIAJABQAKAAAFgEQAPgLgBgWQAAgOgKgLQgJgMgOAAQgSgBgIALIACAAQAKAAAHAGgAg4AIQAAAJAEAGQAIAJAQAAIAJgBQALgBAHgIQAGgHAAgJQAAgFgDgEQgCgFgKgBQgFAAgHAEIgLAHQgHACgGAAQgGAAgDgCIgBAGg");
	this.shape_86.setTransform(-267.3,-45.2);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFCC80").s().p("AAlAaQAKgGAAgJIgBgDQgCgHgPgCQgMgDgRAAQgQAAgNADQgOADgCAGIAAADQgBAKAJAFIgNAAQgKgDAAgQQAAgFABgDQAIgOARgGQANgEAVgBQAxABAKAYQACADAAAFQAAAPgLAEg");
	this.shape_87.setTransform(-281.9,-52.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFCC80").s().p("AhCAhQgDgLADgIIgLAAIAAgLIAIgJIALgKQgHAAgEgFQgEgFAAgGQAAgLAIgGQAHgHALAAQASgBAIAMQAQgLAYAAIAIAAQAXABAOASQAPASgBAWQgCAXgOAPQgPARgXAAIgIgBIAAgLQgPALgWABIgDAAQgjAAgHgZgAgGggQAGAAAHAEQAHAEAFAFQAKAMABAOQAAAWgMAPIAFAAQALAAAHgFQAOgKACgSQABgNgGgLQgGgLgMgGQgKgFgPAAQgJAAgGADgAgngOQgJAGgMAMIAJAAQgEAIACAJQAFAPAYgBQAOgBAJgIQAIgIABgMQABgKgIgJQgHgIgMAAQgLAAgKAHgAgvgrIgGADQAGABABADQADAEgCAEIAGgFIAGgEQgDgEgFgCIgDAAIgDAAg");
	this.shape_88.setTransform(-282.8,-45.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFCC80").s().p("AgvA9QgTgOABgZQABgWASgHQgJgCAAgKQAAgMAMgFQAKgGAMABQAQABAJANQAJAMAAAPIgnAAQgJAAgGAEQgGAGAAAIQABAUAbAGQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAWIgOAAQAHgRARgIQAPgKAVABQAdAAAUAUQATAWAAAeQAAAfgRAUQgTAXgegBQgdABgSgNgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_89.setTransform(-297.8,-46.8);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFCC80").s().p("AgyApQgQgNAAgYQAAgIAEgJQAFgIAGgFQgHgDAAgIIABgGQAGgOAYAAQAUAAAHAMQAIgMAWAAQAKAAAHAFQAJAFABAJQABAGgDAHQAFAEADAIQAEAJAAAIQAAAYgRAOQgSAQggAAQggAAgSgRgAgtgMQgGAGABAJQAAAOASAHQANAFATAAQATAAANgFQARgHABgOQAAgJgFgGQgFgGgJAAIg+AAQgJAAgFAGgAAOgoQgFADgBAHIAfAAIABgEQAAgEgEgDQgEgCgFgBQgHAAgGAEgAgjgrQADACABAEQAAAEgCADIAYAAQgBgLgOgDIgFAAQgEAAgCABg");
	this.shape_90.setTransform(-311.7,-45.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFCC80").s().p("AgOAbIAAgWIgLAAIAAgLIALAAIAAgOIgLAAIAAgKIAeAAQAIgBAGAFQAHAFAAAJQAAAJgIADQgIAFgLAAIAAAbgAgBgUIAAAOQAKAAAAgGQAAgIgJAAIgBAAg");
	this.shape_91.setTransform(-328.3,-53);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_92.setTransform(-332.9,-45.3);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgYQAAgaARgQQAQgQAYAAQAaAAAMAMQAJAHAAALIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMAKQgNAMAAAPQAAAPALALQAMAKAPgBQANAAAKgGQAKgIAAgMQAAgPgOgFQAFAGAAAKQAAAHgHAFQgHAGgIAAQgKAAgHgGQgHgHAAgKQAAgLAJgHQAJgFALgBQASAAANALQANAKAAARQAAAVgPAPQgPAOgVgBQgXAAgQgQgAgCgNQgDADAAAEQAAAEADACQACADAEAAQAEAAADgDQADgCAAgEQAAgEgDgDQgDgCgEAAQgEAAgCACg");
	this.shape_93.setTransform(-345.6,-45.2);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFCC80").s().p("AgUAxQgIAIgSAAQgKAAgIgGQgHgHAAgKQAAgMAFgHIgJAAIAAgJIAHgIIAKgMIgCABQgHAAgDgFQgEgEAAgHQAAgKAIgIQAIgIAKgBQAUgCAIANQASgMAYABQAWACAQAQQAPAQAAAWQABAlgcARQgGADgMAAIgKAAIAAgJQgJAJgLAAIgBAAQgLAAgIgIgAgJgiQARABALANQAOAPAAASQAAATgKALQACABAEAAQAIAAAJgHQAIgIABgKIABgIQAAgWgPgOQgMgLgWAAQgIAAgIACgAglgPQgLAJgJAKIAIAAQgFAIABAIQAAAEAEADQAEADAGAAQAIAAADgJQACgGAAgPIANAAQgBARACAFQACAIAIAAQAHAAAEgIQAFgGAAgJQAAgKgHgIQgGgJgIgBIgHAAQgPAAgIAGgAg1gnQAFABABAFQACAFgCAFIAHgHIAHgHQgCgFgHAAIgCAAQgGAAgDADg");
	this.shape_94.setTransform(-359,-45.3);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFCC80").s().p("AgOAbIAAgWIgKAAIAAgLIAKAAIAAgOIgLAAIAAgKIAdAAQAJgBAHAFQAGAFAAAJQAAAJgIADQgIAFgLAAIAAAbgAgBgUIAAAOQAKAAAAgGQAAgIgJAAIgBAAg");
	this.shape_95.setTransform(-369.4,-53);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFCC80").s().p("AgZA2IAAgOQADADAIAAQAGAAAFgNQAFgNAAgPQgBgPgDgNQgGgPgGgBQgHAAgEAEIAAgOQAFgFANAAQAPAAAKAVQAIARABAVQgBAUgIARQgLASgPAAQgKAAgHgDg");
	this.shape_96.setTransform(-369.8,-45.2);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFCC80").s().p("AgwAyQgSgWAAgeQABgeATgUQATgTAfgBQAbAAASASQASARAAAaQAAAWgMAPQgNAQgUAAQgUABgMgNQgMgMACgTQABgRANgJIgPAAIAAgNIAzAAIAAANIgJAAQgKAAgHAGQgFAGgBAJQAAAIAGAHQAHAGALAAQANAAAHgKQAIgJgBgOQgBgQgIgKQgMgOgZAAQgVAAgMASQgLAPAAAXQAAAVALAPQALASATADIAKABQAYAAANgUIAQAAQgQAkgtAAQgfAAgTgWg");
	this.shape_97.setTransform(-379.2,-43.7);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFCC80").s().p("AgkApQgRgQAAgYQAAgaARgQQAQgQAYAAQAaAAAMAMQAJAHAAALIgNAAQAAgHgLgFQgJgEgMAAQgQAAgMAKQgNAMAAAPQAAAPALALQAMAKAPgBQANAAAKgGQAKgIAAgMQAAgPgOgFQAFAGAAAKQAAAHgHAFQgHAGgIAAQgKAAgHgGQgHgHAAgKQAAgLAJgHQAJgFALgBQASAAANALQANAKAAARQAAAVgPAPQgPAOgVgBQgXAAgQgQgAgCgNQgDADAAAEQAAAEADACQACADAEAAQAEAAADgDQADgCAAgEQAAgEgDgDQgDgCgEAAQgEAAgCACg");
	this.shape_98.setTransform(-391.7,-45.2);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFCC80").s().p("Ag2A7QgMgNABgSQABgVAOgIIgTAAIAAgNIAYAAQgEgCAAgHIABgGQAHgMAUAAIAJAAQAOABAKAMQAIALAAAQIgpAAQgLAAgIAGQgIAHAAAKQAAAJAGAHQAHAGALAAQAIAAAFgEQAGgFAAgIIAAgQIAMAAIAAAQQAAAIAGAFQAFAFAIgBQAXAAACgnQACgYgPgTQgQgUgXAAQgigBgMAWIgQAAQARgjAtABQAaABATAQQANALAHASQAGASgBASQgBAZgJARQgMATgUAAQgSAAgJgLQgIALgRAAIgBAAQgRAAgLgNgAgZgcQADAFAAADQAAADgCADIAbAAQgDgJgGgDQgFgCgHAAIgHAAg");
	this.shape_99.setTransform(-411.6,-46.7);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFCC80").s().p("AgYAvQgLgIAAgRQAAgMAFgJQADgGAIgJQAIgJAXgTIgsAAIAAgMIBBAAIAAAMIgPALIgNAMIgMAPQgGAJABAKQABAGAEAEQAFADAFAAQAMAAADgKIABgGQAAgFgCgEIANAAQAGAKAAAMQgBANgLAIQgKAKgOgBQgPABgJgJg");
	this.shape_100.setTransform(-422.3,-45);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFCC80").s().p("AgvA9QgTgOABgZQABgWASgHQgJgCAAgKQAAgMAMgFQAKgGAMABQAQABAJANQAJAMAAAPIgnAAQgJAAgGAEQgGAGAAAIQABAUAbAGQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAWIgOAAQAHgRARgIQAPgKAVABQAdAAAUAUQATAWAAAeQAAAfgRAUQgTAXgegBQgdABgSgNgAgfgaIAEAGQACAEgCAFIAYAAQgBgGgFgFQgFgFgJAAIgIABg");
	this.shape_101.setTransform(-433.1,-46.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_102.setTransform(-237.3,-117.4);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AgEBxQgQAYgfAAQgkAAgTgeQgSgbABguQACg0AfgbQAYgUAwgBIAOAAIAAAZQgYgBgLACQgQABgMAJQgYASgBAiQgBAbAJARQALAUAWAAQARAAAJgKQAJgLAAgQIAAgeIAYAAIAAAeQAAAlAlAAQAYAAANgcQAMgcgCgpQgBgngUgaQgbglgxAAQgcgBgWALQgXAMgJASIgcAAQAIgcAfgSQAegRAoAAQAgAAAbANQAUAJANANQAlApABBIQACA1gVAjQgXAlgoAAQggAAgOgYg");
	this.shape_103.setTransform(-264.4,-120.6);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_104.setTransform(-282.4,-117.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AA6BpQglAAgRgZQgPAZgpAAQgdAAgUgXQgVgXABgfQABgiAWgRIgfAAIAAgYIAfAAQgGgEAAgMQAAgFACgFQALgfAsAAQAmAAAMAaQAQgaAkAAQAVAAAOAJQARALACASQACARgHAJIgFAAQAfAZAAAnQAAAhgVAYQgUAYgeAAIgBAAgAARAGIAAAXQAAANANAGQALAGAQAAQAQAAANgMQAOgNgBgRQgBgPgLgLQgLgKgRAAIhrAAQgRAAgMALQgNALAAAQQAAARAMAMQAMALASAAQAQAAAKgFQAOgHAAgMIAAgYgAAyhPQgOABgIAJQgJAJgBANIA3AAQAEgFAAgIQgCgTgXAAIgCAAgAg8hMQAGAGACAIQAAAIgEAHIAtAAQgBgRgOgJQgIgGgLAAQgJAAgGADg");
	this.shape_105.setTransform(-300.3,-117.4);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AguBpQgkgCgXgXQgYgXgBghQgBgfAKgWIAYAAQgCAIADADQADAEAIAAQAMAAAJgGQATgMAFgPIABgJQAAgQgLgFQgKgEgJAEQAPAEAAATQAAALgHAGQgHAHgLAAQgGAAgEgCQgTgGAAgXQAAgHADgIQAKgbAmAAQAPAAALAHQAMAHAEAMQAVgcArACQAtACAWAqQAQAcgBAhQAABGgzAZQgQAIgSAAQgLAAgOgDIAAgRQgQAMgNAEQgNAEgUAAIgFAAgAAYgaQAYATAAAgQAAAggUAVIAQACQASAAAJgHQAbgUgBgoQAAgagSgVQgRgUgagBQgggCgPAUIADgBQATAAANAMgAhnAPQAAAQAJALQANARAdAAIASgBQATgCANgPQALgNAAgQQAAgKgFgHQgGgLgRgBQgIAAgNAIQgRALgEACQgMAFgMAAQgLAAgGgFIgBALg");
	this.shape_106.setTransform(-339.5,-117.4);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_107.setTransform(-358.4,-117.4);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AhdBLQgcgYAAgrQAAgRAIgPQAHgQANgHQgOgGAAgPQABgGACgFQAJgZAuAAQAjAAAOAVQAPgVAmAAQATAAAOAJQAQAIACAQQACAMgHAMQAKAIAGAOQAHAQAAARQAAArgfAaQghAcg6AAQg7AAgigegAhUgXQgJALAAARQACAaAgANQAXAKAjAAQAjAAAYgKQAggNABgaQAAgRgKgLQgJgKgQAAIhyAAQgQAAgKAKgAAahKQgKAHgCALIA5AAIABgGQAAgIgHgFQgHgFgJAAIgCAAQgLAAgKAGgAhBhPQAGAEABAHQABAHgEAFIAsAAQgBgTgagFIgKgBQgGAAgFACg");
	this.shape_108.setTransform(-375.8,-117.3);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AhYBcQghgoABg4QABg3AigkQAjgkA5AAQAxAAAhAfQAgAhAAAvQAAAngVAdQgXAdglAAQglABgVgXQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMALQANAMATAAQAXgCAOgSQANgQgBgYQgBgegPgSQgWgagtAAQgnAAgWAgQgUAcAAAqQAAAmAUAdQAUAfAjAGIASACQAsgBAYgiIAdAAQgeBBhSAAQg3AAgkgog");
	this.shape_109.setTransform(-401.2,-114.6);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AAECAQgOgGgLgQQgDAMgMAIQgMAIgRAAQgcAAgRgfQgQgbAAglQAAgsAZghQAbgjArAAQAgAAAUAUQAUAUAAAfQAAAYgTAQIgKAJQgFAGgBAHQgBAYAhAAQAZAAAQgaQANgYAAgiQAAgrgZggQgaghg0AAQgZAAgVALQgXALgLATIgbAAQALgfAfgSQAcgRAlAAQA9AAAkAkQAQAQAMAdQAMAdAAAZQACA+gbAkQgYAigmAAQgWAAgNgGgAhJBQQgFAEAAAHQAAAGAFAFQAFAEAGAAQAHAAAEgEQAFgFAAgGQAAgHgFgEQgFgFgGAAQgGAAgFAFgAhaAGQgIAQAAAUQAAAPAEANQALgUAYgCQASgBAQANQACgOALgLQAPgOAAgQQgBgOgKgJQgKgJgPAAQgqAAgPAhg");
	this.shape_110.setTransform(-426.8,-120.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("AhXBmQggggABg0QABgkAagZQAagaAlAAIAcAAIAAAYIgYAAQgcAAgRARQgRARgBAYQgBAhAbAUQAaASAjgBQAmAAAZgbQAYgbAAgmQAAgngZgeQgZgdgmgCQgbAAgXAMQgXAMgKAUIgYAAQAMggAcgSQAcgSAoAAQA1AAAiAoQAgAmAAA4QAAA5ghAlQgjAng2AAQg1AAgfggg");
	this.shape_111.setTransform(-101.7,-170.3);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFFFF").s().p("Ag6B6QgZgMgOgVQgWggAAgvQAAgfARgaQASgbAYABQAYACAHARQALgVAaABQAXAAAQARQAQARABAXQACAbgVAWQgVAUgbAAQgaAAgSgQQgRgRABgXQAAgJADgHQAGgKABgFQABgEgDgEQgEgDgGAAQgLACgHAQQgGANgBATQgBAjAbAWQAaAXAkAAQAkAAAbgcQAbgbgBgmQgBgsgWgcQgYgcgtAAQg4gBgWAoIgbAAQANgeAdgSQAbgQAkAAQA4ABAeAfQAlAkACA7QACA9ghAoQgiAog5AAQgeAAgagMgAgjgKQAAALALAHQALAJANgBQAPABAMgKQAMgJAAgOIgBgGQgCAIgKAFQgKAHgMAAQgcAAgGgaQgFAOAAAEgAgGgnQAAAGAEAEQAEAFAGgBQAGAAAFgDQAEgEAAgGQAAgHgEgEQgFgEgGgBQgOAAAAAPg");
	this.shape_112.setTransform(-127,-170.4);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA1ACQAfADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAZAKQAVAIAhAAQAjAAAXgSQAXgSADgdIABgOQAAgegSgVQgQgSgcAAIhTAAQgnAAAAgaQAAguBkAAQA8ABAWARQAQALAAARQAAAQgOAKQAgAkgBA4QgBAvgkAfQgiAcgwAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgEgPgJgGQgLgIgQAAQgGAAgFACgAAuhlQgEADAAAHQAAAGAEAEQAEAEAGAAQAGAAAEgEQAFgEAAgGQAAgHgFgDQgEgFgGAAQgGAAgEAFgAg5hgQgCAKATAAIA5AAQgDgMAGgKIgYgCQgzAAgCAOg");
	this.shape_113.setTransform(-151.4,-170.5);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFFF").s().p("ABDAwQASgMgBgQIgBgFQgDgNgagFQgXgGgfAAQgdAAgXAGQgbAGgEAMIgBAGQABARAPAKIgWAAQgUgHAAgdQAAgKADgFQANgaAfgKQAYgIAnAAQBZAAATAsQADAGAAAJQAAAcgUAIg");
	this.shape_114.setTransform(-189.1,-180.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_115.setTransform(-190.8,-167.4);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgWAIgRQAGgLAOgQQAQgSAqghIhQAAIAAgYIB3AAIAAAXIgcAUQgNALgLALQgRATgFAJQgKAQABARQABAMAIAHQAIAGAKAAQAWAAAHgTQACgFAAgFQAAgJgGgHIAYAAQAMARgBAWQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_116.setTransform(-212.3,-167);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AAiB+IAAgtQgeADgXgBQgvgCgcgVQgggXABgqQAAgVALgRQALgQAQgHQgIgDgEgGQgDgGAAgHQAAgNALgJQAUgQAlABQA6ABANAfQAFgOAMgJQAMgJAPAAIAYAAIAAAXIgEgBQgIAAgBAIQAAADAIAGQAMAKAEAFQAKALAAAOQABAYgWAOQgTAKgYgCIAAA1IAVgGIAQgGIAAAXIgRAHIgUAFIAAAygAg5glQgOAPAAAVQABAuAzALQAJACARAAQASAAAJgCIAAhtIg5AAQgVAAgNAQgAA+g/IAAAoIAEABQAKAAAHgGQAHgGAAgKQAAgLgKgOQgKgPAAgGIAAgEQgIACAAAdgAgxhjQAEAGAAAHQAAAGgEAEIAEgBIAGAAIA2AAQgEgOgSgHQgNgEgLAAQgJAAgJADg");
	this.shape_117.setTransform(-232.4,-165.2);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAaQAUAAAAgNQAAgNgQAAIgEAAg");
	this.shape_118.setTransform(-263.1,-181.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_119.setTransform(-272.7,-167.4);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_120.setTransform(-299.8,-167.5);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACgtQACgnAfgOQgQgEAAgSQAAgXAWgJQASgJAXACQAcABARAXQASAVgBAdIhIAAQgQAAgLAIQgLAJABAQQACAlAwAKQAPADAVAAQAogBAZgbQAYgYAAgmQABgqgYgcQgZgfgwgCQg1AAgcAnIgaAAQANgdAegRQAdgPAlgBQA2AAAjAnQAkAnAAA3QAAA3ggAmQgiAog2AAQg2ABgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgNgJgHQgKgJgOAAQgIAAgHACg");
	this.shape_121.setTransform(-325.1,-170.3);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAaQAUAAAAgNQAAgNgQAAIgEAAg");
	this.shape_122.setTransform(-342.3,-181.6);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_123.setTransform(-352.3,-167.4);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFFFFF").s().p("AgEBxQgQAYgfAAQgkAAgTgeQgSgbABguQACg0AfgbQAYgUAwgBIAOAAIAAAZQgYgBgLACQgQACgMAIQgYASgBAiQgBAbAJARQALAUAWAAQARAAAJgKQAJgLAAgPIAAgfIAYAAIAAAfQAAAkAlAAQAYAAANgcQAMgcgCgpQgBgngUgaQgbglgxAAQgcAAgWALQgXALgJASIgcAAQAIgcAfgSQAegRAoAAQAgAAAbANQAUAJANANQAlAqABBHQACA1gVAjQgXAlgoAAQggAAgOgYg");
	this.shape_124.setTransform(-379.8,-170.6);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgTQAXgSACgdQACgngUgZQgQgSgbAAIhTAAQgnAAAAgaQAAguBkAAQAmAAAeAQQAiASABAdIgbAAQABgRgcgKQgXgKgdAAQgzAAgCANQgCAKATAAIBDAAQAbAAAWARQAmAfAABDQAAAvglAfQgjAcgvAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgDgPgKgGQgKgIgRAAQgGAAgFACg");
	this.shape_125.setTransform(-404.4,-170.5);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgTQAXgSACgdQACgngUgZQgQgSgbAAIhTAAQgnAAAAgaQAAguBkAAQAmAAAeAQQAiASABAdIgbAAQABgRgcgKQgXgKgdAAQgzAAgCANQgCAKATAAIBDAAQAbAAAWARQAmAfAABDQAAAvglAfQgjAcgvAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgDgPgKgGQgKgIgRAAQgGAAgFACg");
	this.shape_126.setTransform(-428.1,-170.5);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAaQAUAAAAgMQAAgOgQAAIgEAAg");
	this.shape_127.setTransform(-237,-231.6);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_128.setTransform(-245.3,-217.5);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFFFFF").s().p("AhCBLQgegeAAgsQAAguAegdQAdgeAsABQAwAAAWATQAPAPABATIgYAAQgBgOgTgIQgRgIgWAAQgdAAgWAUQgXAUAAAdQAAAcAUASQAVATAdAAQAXAAASgNQATgNAAgXQAAgdgbgHQAKAJAAATQAAANgMALQgNAJgPABQgSAAgNgMQgNgMAAgSQAAgVARgMQAPgKAWgBQAgAAAYATQAWATAAAfQAAAngbAZQgbAagmAAQgqAAgdgdgAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_129.setTransform(-268.5,-217.4);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_130.setTransform(-292.9,-217.5);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgTIATAAIAAgbIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAaQAUAAAAgMQAAgOgQAAIgEAAg");
	this.shape_131.setTransform(-311.9,-231.6);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_132.setTransform(-312.5,-217.5);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("AhYBcQghgoABg3QABg4AigkQAjgkA5AAQAxAAAhAfQAgAhAAAvQAAAogVAcQgXAdglAAQglACgVgYQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMALQANALATAAQAXgBAOgSQANgQgBgYQgBgfgPgSQgWgagtAAQgnABgWAgQgUAcAAAqQAAAnAUAcQAUAgAjAFIASACQAsAAAYgjIAdAAQgeBBhSAAQg3AAgkgog");
	this.shape_133.setTransform(-329.6,-214.7);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AhCBLQgegeAAgsQAAguAegdQAdgeAsABQAwAAAWATQAPAPABATIgYAAQgBgOgTgIQgRgIgWAAQgdAAgWAUQgXAUAAAdQAAAcAUASQAVATAdAAQAXAAASgNQATgNAAgXQAAgdgbgHQAKAJAAATQAAANgMALQgNAJgPABQgSAAgNgMQgNgMAAgSQAAgVARgMQAPgKAWgBQAgAAAYATQAWATAAAfQAAAngbAZQgbAagmAAQgqAAgdgdgAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_134.setTransform(-352.5,-217.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFFFFF").s().p("AhkBrQgUgYABggQACgmAagQIgiAAIAAgYIArAAQgIgDAAgNQAAgGACgFQANgWAkAAIAQAAQAcADARAVQAPAUgBAdIhLAAQgUAAgOAMQgOAMgBATQAAARAMAMQAMAMAUAAQAOAAAKgJQALgJAAgPIAAgbIAXAAIAAAcQAAAOAJAJQAKAJAPAAQAqgBAEhHQADgtgcgjQgcgjgqgBQg/gBgXAoIgcAAQAfg/BRABQAxABAiAdQAYAVAMAhQAMAggCAiQgCAugRAdQgVAkglAAQggAAgQgTQgQATggAAIgBAAQgfAAgVgYgAgtgzQAFAIAAAGQgBAGgEAEIAzAAQgHgRgLgEQgKgEgMAAIgLABg");
	this.shape_135.setTransform(-388.5,-220.1);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgWAIgRQAGgLAOgQQAQgSAqghIhQAAIAAgYIB3AAIAAAXIgcAVQgNAKgLALQgRATgFAJQgKAQABARQABAMAIAHQAIAGAKAAQAWAAAHgSQACgHAAgEQAAgJgGgHIAYAAQAMASgBAVQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_136.setTransform(-408,-217.1);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACgtQACgnAfgOQgQgEAAgSQAAgXAWgJQASgJAXACQAcABARAWQASAXgBAcIhIAAQgQAAgLAIQgLAJABARQACAkAwAKQAPAEAVgBQAogBAZgbQAYgYAAgmQABgpgYgdQgZgfgwgBQg1gBgcAnIgaAAQANgeAegQQAdgQAlAAQA2AAAjAnQAkAmAAA4QAAA3ggAmQgiAog2AAQg2ABgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgNgJgIQgKgIgOgBQgIAAgHADg");
	this.shape_137.setTransform(-427.4,-220.3);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.lf(["#C4291D","#E26D45"],[0,1],-504.9,0,504.9,0).s().p("EhO4AAtIAAhZMCdxAAAIAABZg");
	this.shape_138.setTransform(-0.5,-280.5);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#2B0912").s().p("EhO4AsYMAAAhYvMCdxAAAMAAABYvg");
	this.shape_139.setTransform(-0.5,-1);

	this.instance_8 = new lib.Path_3();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-0.4,281.5,1,1,0,0,0,504.9,23.5);
	this.instance_8.alpha = 0.539;

	this.instance_9 = new lib.Bitmap33();
	this.instance_9.parent = this;
	this.instance_9.setTransform(223,-29,0.895,1.084);

	this.instance_10 = new lib.Bitmap34();
	this.instance_10.parent = this;
	this.instance_10.setTransform(223,120,1.215,1.084);

	this.instance_11 = new lib.Bitmap32();
	this.instance_11.parent = this;
	this.instance_11.setTransform(31,120,0.9,0.921);

	this.instance_12 = new lib.Bitmap31();
	this.instance_12.parent = this;
	this.instance_12.setTransform(31,-29,0.895,0.913);

	this.instance_13 = new lib.Bitmap30();
	this.instance_13.parent = this;
	this.instance_13.setTransform(310,-218,1,1.184);

	this.instance_14 = new lib.Bitmap29();
	this.instance_14.parent = this;
	this.instance_14.setTransform(147,-218,1,1.084);

	this.instance_15 = new lib.Bitmap28();
	this.instance_15.parent = this;
	this.instance_15.setTransform(-28,-218);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FDD835").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_140.setTransform(424,-84.3);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FDD835").s().p("AAfAVQAHgFAAgGIgBgDQgBgGgMgCQgKgDgOAAQgMAAgLAEQgMACgBAFIgBADQAAAHAHAEIgKAAQgJgDAAgNQAAgEACgBQAFgMAOgFQAKgEASAAQAoAAAJAVQABABAAAEQAAANgJADg");
	this.shape_141.setTransform(412.4,-90.1);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_142.setTransform(411.6,-84.2);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_143.setTransform(402.9,-90.6);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_144.setTransform(398.4,-84.2);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FDD835").s().p("AgMAvIgJgCIAAgLIAEABQAFAAAAgFQgBgCgEgGQgFgGAAgGQAAgHAEgEQAEgEAHgBQAGgBAEADQgBgOgCgGQgBgKgIgBQgGgCgEAEIAAgLQAGgDAJABQAOABAHAPQAGANAAARQAAATgKAOQgJAOgNAAIgDAAgAgFAEQgFABAAAGIABADIAEAHQACAEAAAEQADgEABgIQABgGAAgFQgCgCgDAAIgCAAg");
	this.shape_145.setTransform(389.3,-84.2);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FDD835").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_146.setTransform(381.1,-85.5);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FDD835").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgLIA1AAIAAAKIgMAJQgGAFgFAFQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_147.setTransform(366.7,-84);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FDD835").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJACgFIATAAIgKAOQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJABALQAAALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_148.setTransform(358.4,-85.5);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FDD835").s().p("AgQARQgHgGgCgJIgBgEQAAgLAHgIIAAANIAFAAQAAAGAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAFQAAAMgIAHQgHAJgMgBQgJAAgHgEg");
	this.shape_149.setTransform(350.4,-78.5);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FDD835").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgfAVgNQAIgEALAAIAGAAIAAALQANgMARABQAUAAAKAOQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgJQgIAFgEACQgGACgJAAIgEAAgAAHgPQAQAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgVgCgDAPQAEgEAHAAIAFABgAglgaQgHAJAAALQAAANAHAIQAHAJANAAQAKABAHgFQAHgEABgIQAAgFgDgDQgDgEgEAAQgDAAgDADQgCACAAADIABAFIgLAAQgDgLACgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_150.setTransform(347.2,-84.2);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FDD835").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_151.setTransform(284.3,-84.3);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FDD835").s().p("AAeAVQAIgFAAgGIgBgDQgBgGgMgCQgKgDgOAAQgMAAgLAEQgMACgBAFIgBADQAAAHAHAEIgKAAQgJgDAAgNQAAgEACgBQAFgMAOgFQALgEARAAQAoAAAIAVQACABAAAEQAAANgJADg");
	this.shape_152.setTransform(272.7,-90.1);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_153.setTransform(271.9,-84.2);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_154.setTransform(263.2,-90.6);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_155.setTransform(258.7,-84.2);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FDD835").s().p("AgMAvIgJgCIAAgLIAEABQAFAAAAgFQgBgCgEgGQgFgGAAgGQAAgHAEgEQAEgEAHgBQAGgBAEADQgBgOgCgGQgBgKgIgBQgGgCgEAEIAAgLQAGgDAJABQAOABAHAPQAGANAAARQAAATgKAOQgJAOgNAAIgDAAgAgFAEQgFABAAAGIABADIAEAHQACAEAAAEQADgEABgIQABgGAAgFQgCgCgDAAIgCAAg");
	this.shape_156.setTransform(249.6,-84.2);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FDD835").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_157.setTransform(241.4,-85.5);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FDD835").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgLIA1AAIAAAKIgMAJQgGAFgFAFQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_158.setTransform(227,-84);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FDD835").s().p("AgDA8QgUgBgNgNQgLgMgBgTQAAgNAFgJQAEgJALgKIAVgTQAHgJACgFIATAAQgEAHgFAHQgGAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgIAJAAALQgBALAIAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_159.setTransform(218.7,-85.5);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_160.setTransform(211.5,-90.6);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FDD835").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_161.setTransform(207.4,-84.2);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FDD835").s().p("AgdAiQgOgOAAgUQAAgUAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgKAAQgCgGgIgEQgIgDgJAAQgNgBgKAKQgKAJAAAMQAAANAJAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgNgEQAFAFAAAJQAAAEgFAFQgGAFgHgBQgIAAgFgEQgGgFAAgJQAAgIAHgGQAHgFAKAAQAOAAALAIQAKAJAAANQAAASgMAMQgMALgSAAQgSAAgNgNgAgBgKQgDACAAADQAAADADACQABACADAAQADAAADgCQACgBAAgEQAAgDgCgCQgDgDgDAAQgDABgBACg");
	this.shape_162.setTransform(196.7,-84.2);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FDD835").s().p("AgmAyQgRgMABgTQABgQAMgHQgIgBAAgKQAAgIAJgFQgDgDAAgIIABgFQAEgKALgDQAIgDAOAAQAeAAAEASQABAEgDAEQADgCADgFQAGgLAAgJIAQAAIgDALQgCAGgEAFQgEAHgFAEQAIADACAKQABAJgDAGQAIAIAAAMQAAATgRAMQgQALgUAAQgWAAgQgMgAgjAFQgFAFAAAHQABAKANAHQALAFAPAAQAOAAALgFQANgGABgMQABgCgCgDQgDAFgFADQgGADgFAAQgEAAgDgBQgHgEAAgLIABgGIgdAAQgHAAgFAFgAAQAFQAAAGAFAAQAGAAAEgIQgDgDgMAAIAAAFgAAXgQQgDADgBAEQAMAAAFADQABgCAAgDQAAgIgIAAQgEAAgCADgAAGgRQgDAEAAAEIAFAAIACgGQABgDACgCQgEABgDACgAgdgTQAEACAAACQABADgCADIAUAAQgCgHgGgDIgIgBIgHABgAgBgWQAEgHANgCQACgDgCgFQgEgGgRAAQgVAAgEAHIgBAEQAAABAAAAQAAABAAAAQAAABAAAAQABABAAAAIAIgBQAPAAAGAJg");
	this.shape_163.setTransform(186.6,-85.7);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FDD835").s().p("AgXAZQgLgKAAgPQAAgNAKgKQALgLANAAQAOAAAKAKQALAKAAAOQAAAOgKAKQgLALgOAAQgNAAgKgKgAgPgQQgFAGABAHQgBAJAJAGQAFAEAGAAQAKAAAHgIQAEgFAAgGQAAgKgJgHQgFgEgHAAQgJAAgGAIg");
	this.shape_164.setTransform(177.2,-83.1);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FDD835").s().p("AgcA4QgMgHABgMQABgHAGgDQAFgDAIAAIAFABIAAAJIgDAAQgHAAAAAEQAAAHAYgBQAHAAAGgCQAIgCgBgDQAAgFgNABIgLABIAAgKQATgCALgHQANgJAAgOIgBgHQgDgMgLgGQgLgGgNAAQgPAAgLAGQgMAHAAANQgBALAIAGQAGAGALAAQAGAAAFgCQAFgDACgFQgEADgFAAQgHAAgFgEQgFgEAAgHQAAgJAFgFQAFgEAJAAQAKgBAGAHQAHAGgBALQAAAMgKAIQgIAHgNABQgSABgLgKQgLgKAAgRQAAgWAQgMQAPgMAXABQAYAAAOANQANANAAAUQAAAbgZAOQAIAFAAAIQAAAJgLAGQgKAGgPAAIgCAAQgQAAgKgFgAgIgSQgCACAAADQAAAEADACQACACADAAQACAAACgDQADgCAAgDQAAgDgDgCQgCgDgCAAQgDAAgDADg");
	this.shape_165.setTransform(168.1,-82.8);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FDD835").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgLIA1AAIAAAKIgMAJQgGAFgFAFQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_166.setTransform(308.9,249);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FDD835").s().p("AgDA8QgUgBgMgNQgNgMAAgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJABgFIAUAAQgEAHgGAHQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgagMQgHAJgBALQAAALAIAIQAKAKAQAAQAMAAAKgHQAMgJAAgPQAAgGgCgFQgEgJgJgFQgIgEgKAAQgRAAgKALg");
	this.shape_167.setTransform(300.7,247.5);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FDD835").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_168.setTransform(289.1,248.8);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FDD835").s().p("AgkAyQgPgPAAgVQAAgPAGgKQAHgMANAAQAMAAACAKQADgLANAAQAJgBAHAJQAGAIAAAJQAAAMgIAIQgIAJgLAAQgJAAgIgHQgIgFAAgKIADgNQABgGgHgBQgEAAgDAIQgDAFAAAHQAAAPAMAJQALAKAPgBQAQABALgLQALgMAAgPQAAgNgFgJQgGgJgMgBIg+AAIAAgJQAQABAAgCIgDgCQgEgCAAgDQAAgMAOgEQAHgDARAAQANAAAJADQAMADAIAJQAGAIAAAKIgLAAIAAAAQgCgKgLgGQgJgEgOAAIgOABQgHACAAAGQAAAAAAABQAAAAABAAQAAABAAAAQABABAAAAIAEABIAdAAQAJAAAHAEIAGADIAAAAIAAAAQAPALAAAcQAAAZgOAQQgPAQgXAAQgWAAgOgPgAgMACQAAAFAEAEQAEADAFABQAGAAAEgEQAFgEAAgGQgFAHgIAAQgJAAgGgJIAAADgAAAgNQAAABAAAAQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQABAAAAABQAAAAAAABQAAAAABABQAAAAAAAAQABAAABAAQAAAAABAAIAEgBQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgBgEQgBAAAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQAAABAAAAQgBAAAAAAgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAfgiQgHgEgJAAIgdAAIgEgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAgGAHgCIAOgBQAOAAAJAEQALAGACAKIgGgDg");
	this.shape_169.setTransform(277.3,247.1);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FDD835").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_170.setTransform(265.5,248.8);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_171.setTransform(133.5,242.4);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FDD835").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_172.setTransform(129.4,248.8);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FDD835").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_173.setTransform(117.3,248.8);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FDD835").s().p("AgkAyQgPgPAAgVQAAgPAGgKQAHgMANAAQAMAAACAKQADgLANAAQAJgBAHAJQAGAIAAAJQAAAMgIAIQgIAJgLAAQgJAAgIgHQgIgFAAgKIADgNQABgGgHgBQgEAAgDAIQgDAFAAAHQAAAPAMAJQALAKAPgBQAQABALgLQALgMAAgPQAAgNgFgJQgGgJgMgBIg+AAIAAgJQAQABAAgCIgDgCQgEgCAAgDQAAgMAOgEQAHgDARAAQANAAAJADQAMADAIAJQAGAIAAAKIgLAAIAAAAQgCgKgLgGQgJgEgOAAIgOABQgHACAAAGQAAAAAAABQAAAAABAAQAAABAAAAQABABAAAAIAEABIAdAAQAJAAAHAEIAGADIAAAAIAAAAQAPALAAAcQAAAZgOAQQgPAQgXAAQgWAAgOgPgAgMACQAAAFAEAEQAEADAFABQAGAAAEgEQAFgEAAgGQgFAHgIAAQgJAAgGgJIAAADgAAAgNQAAABAAAAQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQABAAAAABQAAAAAAABQAAAAABABQAAAAAAAAQABAAABAAQAAAAABAAIAEgBQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgBgEQgBAAAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQAAABAAAAQgBAAAAAAgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAfgiQgHgEgJAAIgdAAIgEgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAgGAHgCIAOgBQAOAAAJAEQALAGACAKIgGgDg");
	this.shape_174.setTransform(106.1,247.1);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FDD835").s().p("AgdAiQgOgOAAgUQAAgUAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgHgDgKAAQgNgBgKAKQgLAJABAMQgBANAKAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgNgEQAFAFAAAJQAAAEgGAFQgFAFgHgBQgHAAgHgEQgFgFAAgJQAAgIAHgGQAHgFAJAAQAPAAAKAIQALAJAAANQAAASgMAMQgMALgRAAQgTAAgNgNgAgCgKQgCACAAADQAAADACACQACACADAAQADAAADgCQACgBAAgEQAAgDgCgCQgCgDgDAAQgEABgCACg");
	this.shape_175.setTransform(96.1,248.8);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FDD835").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_176.setTransform(326.9,102.8);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FDD835").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_177.setTransform(319,102.7);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FDD835").s().p("AAfAWQAHgFAAgHIAAgDQgCgGgMgCQgKgCgOgBQgNAAgKADQgMACgCAGIAAADQAAAHAHAFIgKAAQgJgDAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAJATQABADAAADQAAANgJAEg");
	this.shape_178.setTransform(307.4,96.9);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FDD835").s().p("AgpAiQgNgLAAgUQAAgGAEgHQADgHAGgEQgGgDAAgGIABgFQAEgLAVAAQAPAAAGAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAHQAAAMAPAGQALAEAPAAQAQAAAKgEQAOgGABgMQAAgHgEgEQgEgGgIABIgyAAQgIgBgEAGgAAMghQgFADgBAGIAaAAIAAgDQAAgEgCgCQgEgCgEAAQgFAAgFACgAgdgjQADACABADQAAADgCADIAUAAQAAgJgMgCIgEgBIgGABg");
	this.shape_179.setTransform(307.4,102.8);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_180.setTransform(299.5,96.4);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FDD835").s().p("AgnApQgPgRAAgZQABgZAPgQQAQgQAagBQAVAAAPAPQAOAOAAAWQAAARgJAMQgKANgRABQgQAAgKgKQgJgKABgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQALAAAGgJQAGgGAAgLQgBgNgHgJQgKgLgUAAQgRAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAALgQIANAAQgNAegmAAQgYgBgQgSg");
	this.shape_181.setTransform(295.9,104);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FDD835").s().p("AAeAWQAIgFAAgHIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLADQgMACgBAGIgBADQAAAHAHAFIgKAAQgJgDAAgOQAAgEACgCQAFgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJAEg");
	this.shape_182.setTransform(284.4,96.9);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FDD835").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgXALgMQAFgFAGgEQAHgEAJAAIAIABIAAAKQAMgLASAAQAUABAKANQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgKQgIAGgEABQgHADgJAAIgDAAgAANAjIAEABQAGAAACgEIAAgCIgDgLQgCAIgHAIgAAgAOIACAHQACAEAAAEQAHgHAAgJQAAgFgFAAQgGAAAAAGgAgjgbQgHAHgBAKQgBANAGAKQAHALAOAAQAJABAIgFQAHgFAAgIQABgEgDgDQgDgEgDAAQgEAAgDADQgCABAAAEIAAAFIgLAAQgCgLABgLQABgKAJgLIgGgBQgKAAgHAIgAAGgPQAOAFACAMQACgFAIgBQAGgBADADQgBgJgHgGQgHgIgLAAQgJgBgGADQgIAEgCAIQADgFAIAAIAFABg");
	this.shape_183.setTransform(284.1,102.8);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FDD835").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_184.setTransform(266.6,107.4);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FDD835").s().p("AAAAyQgGAJgPAAQgOAAgJgLQgKgLABgOQABgRALgHIgPAAIAAgKIATAAQgDgCAAgGIABgEQAGgLAQAAIAHABQAMABAIAJQAHAJgBANIghAAQgJAAgGAFQgHAGAAAIQAAAIAFAFQAGAFAJAAQAGAAAFgEQAEgEAAgGIAAgNIAKAAIAAANQAAAGAEAEQAFAFAHgBQASAAACggQABgTgMgQQgNgQgSAAQgcgBgLASIgMAAQAOgcAkAAQAWABAPANQALAJAFAPQAGAOgBAPQgBAVgIANQgJAQgRAAQgOAAgIgJgAgUgWQACADAAADQAAADgCACIAXAAQgDgIgFgCQgEgCgFAAIgGABg");
	this.shape_185.setTransform(266.4,101.6);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FDD835").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_186.setTransform(255.1,101.5);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FDD835").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIABgEQAFgNAYABQAOACAGAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAEQAJAEAPAAQAPAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIgkAAQgSAAAAgLQAAgVAtAAQARAAANAHQAQAIAAANIgMAAQAAgIgMgFQgLgDgMAAQgXAAgBAFQgBAFAIAAIAeAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgDQgFgDgHAAIgFABg");
	this.shape_187.setTransform(109.4,101.4);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FDD835").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPAAAQQgBARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_188.setTransform(101.7,102.8);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FDD835").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgfAVgNQAIgEALAAIAGAAIAAALQANgMARABQAUAAAKAOQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgJQgIAFgEACQgGACgJAAIgEAAgAAHgPQAQAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgVgCgDAPQAEgEAHAAIAFABgAglgaQgHAJAAALQAAANAHAIQAHAJANAAQAKABAHgFQAHgEABgIQAAgFgDgDQgDgEgEAAQgDAAgDADQgCACAAADIABAFIgLAAQgDgLACgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_189.setTransform(93.6,102.8);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FDD835").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAHAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgKQAAgMgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_190.setTransform(83,102.8);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FDD835").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_191.setTransform(105.8,-84.3);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FDD835").s().p("AAeAVQAIgFAAgGIgBgDQgBgGgMgCQgKgDgOAAQgMAAgLAEQgMACgBAFIgBADQAAAHAHAEIgKAAQgJgDAAgNQAAgEACgBQAFgMAOgFQAKgEASAAQAoAAAIAVQACABAAAEQAAANgJADg");
	this.shape_192.setTransform(94.1,-90.1);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_193.setTransform(93.4,-84.2);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FDD835").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAHgGADQgHADgJgBIAAAXgAgBgQIAAALQAIABAAgGQAAgGgHAAIgBAAg");
	this.shape_194.setTransform(84.7,-90.6);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FDD835").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_195.setTransform(80.2,-84.2);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FDD835").s().p("AgMAvIgJgCIAAgLIAEABQAFAAAAgFQgBgCgEgGQgFgGAAgGQAAgHAEgEQAEgEAHgBQAGgBAEADQgBgOgCgGQgBgKgIgBQgGgCgEAEIAAgLQAGgDAJABQAOABAHAPQAGANAAARQAAATgKAOQgJAOgNAAIgDAAgAgFAEQgFABAAAGIABADIAEAHQACAEAAAEQADgEABgIQABgGAAgFQgCgCgDAAIgCAAg");
	this.shape_196.setTransform(71.1,-84.2);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FDD835").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_197.setTransform(62.9,-85.5);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FDD835").s().p("AgFAtIgLgEQgFgCgEAAQgJAAABAHIgLAAQgCgHAAgJQAAgJACgGQADgJAIgGIAOgLQAHgFABgIQAAgDgCgFQgDgDgFAAQgEAAgEADQgDADAAADQgBALAIAEIgPAAQgEgGAAgHIAAgDQABgJAGgFQAHgFAJAAQAKAAAGAGQAGAFAAAJQAAAJgFAIIgNAJQgJAIgCADQgGAHAAAIIAAADIAIgBQAFAAAJADQAJADAFAAIAKgBQAFgCAEgEQAGgHAAgIQABgHgGgFIgMgMQgGgGAAgJQAAgJAHgGQAGgEAJAAQAIAAAFAEQAGAFAAAIQABAFgDAFQgCAEgFAAQgEABgEgDQgDgCAAgFQAAgGAGgDQgCgCgEAAQgDAAgDACQgDADAAAEQAAAIAOAGQARAIAAARQAAAPgMAKQgLAJgPAAQgIAAgGgCg");
	this.shape_198.setTransform(46.7,-84.2);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FDD835").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_199.setTransform(37.6,-82.9);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FDD835").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_200.setTransform(26.9,-85.5);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FDD835").s().p("AgeAiQgNgOAAgUQAAgUANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgLAAQgBgGgIgEQgHgDgLAAQgMgBgKAKQgLAJAAAMQAAANAKAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgMgEQAEAFAAAJQAAAEgGAFQgFAFgHgBQgHAAgHgEQgFgFAAgJQAAgIAIgGQAGgFAKAAQAOAAAKAIQALAJAAANQAAASgNAMQgMALgQAAQgTAAgOgNgAgCgKQgCACAAADQAAADACACQACACADAAQAEAAACgCQACgBAAgEQAAgDgCgCQgDgDgCAAQgEABgCACg");
	this.shape_201.setTransform(16.8,-84.2);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FDD835").s().p("AgeAiQgNgOAAgUQAAgUANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgHgDgKAAQgNgBgKAKQgLAJAAAMQAAANAKAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgNgEQAFAFAAAJQAAAEgGAFQgFAFgHgBQgHAAgHgEQgFgFAAgJQAAgIAHgGQAHgFAJAAQAPAAAKAIQALAJAAANQAAASgMAMQgNALgQAAQgTAAgOgNgAgCgKQgCACAAADQAAADACACQACACADAAQADAAADgCQACgBAAgEQAAgDgCgCQgCgDgDAAQgEABgCACg");
	this.shape_202.setTransform(7.6,-84.2);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AAeAVQAIgFAAgGIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLAEQgMACgBAFIgBADQAAAHAHAEIgKAAQgJgDAAgNQAAgEACgBQAFgMAOgFQALgEARAAQAoAAAIAVQACACAAADQAAANgJADg");
	this.shape_203.setTransform(266.4,-55.1);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AghA8QgTgBgNgNQgMgMgBgTQAAgNAGgJQAEgJALgKIAVgTQAGgJADgFIAUAAQgEAHgFAHQgGAGgKAGQAOgBALAEQAbAKAAAhQgBAUgPAOQgNANgUAAIgEAAgAg3gMQgIAJAAALQAAALAHAIQALAKAQAAQANAAAKgHQALgJgBgPQABgGgDgFQgCgJgJgFQgIgEgKAAQgTAAgJALgAAdAyQgIgKAAgQQAAgTARgaQAHgLALgPIg6AAIAAgLIBJAAIAAAKQgNAOgHANQgNAVAAASQAAATAPAAQAGAAAEgDQAEgDAAgFQAAgEgCgEIAKAAQADAEAAALQABAMgJAHQgIAIgNAAQgNAAgHgKg");
	this.shape_204.setTransform(251.3,-50.5);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_205.setTransform(231.3,-49.2);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgCAAgIQAAgKAKgFQAIgEAKABQANAAAHALQAIAKAAAMIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgOQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOAQQgPATgZAAQgXAAgPgKgAgZgVQACACABACQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEgBgDACg");
	this.shape_206.setTransform(219.6,-50.5);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_207.setTransform(209.8,-47.9);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQgBgMgDgLQgDgLgGgBQgGAAgEADIAAgLQAFgEAKAAQAMAAAIAQQAIAPAAAQQAAARgIAOQgIAPgNAAQgIAAgGgDg");
	this.shape_208.setTransform(202.4,-49.2);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AgdAiQgOgOAAgTQAAgVAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNgBgKAKQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgKQAAgMgNgEQAFAFAAAJQAAAEgGAFQgFAFgHgBQgIAAgGgEQgFgGAAgIQAAgIAHgGQAHgFAJAAQAPAAALAIQAKAJAAANQAAASgMAMQgMALgSAAQgSAAgNgNgAgBgKQgDACAAADQAAADADACQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgDgCAAQgEAAgBADg");
	this.shape_209.setTransform(184.2,-49.2);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgLALAAQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAKgMgBQgMABgIgIQgIgIABgJIACgHIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAHQAAAQALALQAMAKAQgBQAQABAMgMQAMgNAAgQQAAgVgKgLQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaABQgNAAgMgGgAgPgEQAAAFAFADQAEAEAGAAQAGAAAGgFQAFgEAAgGIAAgDQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADABQABAAAAgBQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgCgDAAQgFABAAAGg");
	this.shape_210.setTransform(156.4,-50.5);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AgtAIQgDgFgBgGQABgGADgEIAKAAQgCACAAADQAAAEADADQAKAKAZAAQAOAAAKgFQAGgCAEgDQADgDABgGQgBgGgFgGIAHgFQAKAHgBAOQAAAGgBAEQgJAYgnAAQgiAAgLgUg");
	this.shape_211.setTransform(144.9,-43.9);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("AAfAVQAHgFAAgGIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLAEQgMACgCAFIAAADQAAAHAHAEIgKAAQgJgDAAgNQAAgEACgBQAFgMAOgFQAKgEASAAQAoAAAJAVQABACAAADQAAANgJADg");
	this.shape_212.setTransform(114.1,-55.1);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_213.setTransform(106,-49.2);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_214.setTransform(97.5,-49.2);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAGgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_215.setTransform(83.3,-55.6);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_216.setTransform(79.2,-49.2);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgeAiQgNgOAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMgBgKAKQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgKQAAgMgMgEQAEAFAAAJQAAAEgFAFQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgIAIgGQAGgFAKAAQAOAAAKAIQALAJAAANQAAASgNAMQgMALgRAAQgSAAgOgNgAgCgKQgCACAAADQAAADACACQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgDgDAAQgDAAgCADg");
	this.shape_217.setTransform(56.9,-49.2);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgCAAgIQAAgKAKgFQAIgEAKABQANAAAHALQAIAKAAAMIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgOQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOAQQgPATgZAAQgXAAgPgKgAgZgVQACACABACQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEgBgDACg");
	this.shape_218.setTransform(34.5,-50.5);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AAAA1QgHALgPAAQgNAAgJgKQgIgLABgNQAAgNAJgIIgNAAIAAgJIATAAQgEgCAAgHIABgFQAGgKAUAAQANAAAHAIQAIAJAAAMIAAAEIglAAQgGAAgFAFQgEAEAAAHQAAAGAFAFQAFAFAGAAQAHAAAEgDQAFgDAAgGIAAgNIAKAAIAAANQAAAGAGADQAFADAHAAQAKAAAFgKQAFgJAAgMQAAgRgKgLQgKgKgOAAIgpAAQgOAAAAgMQAAgXAuABQAZAAAMALQAJAIgBALIgKAAQAAgHgHgFQgKgIgSAAQgcAAAAAJQAAAFAKAAIAeAAQAQAAANANQAPAPAAAZQAAAUgJANQgJAQgQAAQgQAAgGgLgAgTgNQADACABAEQABAEgDADIASAAQAAgGgDgEQgEgEgHAAIgGABg");
	this.shape_219.setTransform(17.4,-50.9);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAIgBAJQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_220.setTransform(8.8,-49);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgCAAgIQAAgKAKgFQAIgEAKABQANAAAHALQAIAKAAAMIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgOQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOAQQgPATgZAAQgXAAgPgKgAgZgVQACACABACQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEgBgDACg");
	this.shape_221.setTransform(-0.1,-50.5);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AgHALQgFgEAAgHQAAgDACgEQAEgFAGgBQAEAAAEAEQAFAEAAAFQAAAFgDADQgEAGgGgBQgEAAgDgCg");
	this.shape_222.setTransform(-13.8,-45.7);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_223.setTransform(294.5,-237.2);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_224.setTransform(286.6,-237.3);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AAfAVQAHgFAAgGIAAgDQgCgGgMgCQgKgDgOAAQgNAAgKAEQgMACgCAFIAAADQAAAHAHAEIgKAAQgJgDAAgNQAAgEABgBQAGgMAOgFQALgEARAAQAoAAAJAVQABABAAAEQAAANgJADg");
	this.shape_225.setTransform(275,-243.1);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_226.setTransform(274.4,-237.2);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AggA8QgVgBgLgNQgNgMAAgTQAAgNAEgJQAGgJAKgKIAUgTQAHgJADgFIATAAQgDAHgGAHQgFAGgJAGQAOgBAJAEQAbAKAAAhQAAAUgOAOQgOANgUAAIgDAAgAg3gMQgIAJAAALQAAALAIAIQAKAKAQAAQANAAAJgHQALgJABgPQgBgGgCgFQgCgJgJgFQgIgEgLAAQgSAAgJALgAAcAyQgGgKAAgQQAAgTAQgaQAHgLAMgPIg7AAIAAgLIBJAAIAAAKQgMAOgIANQgOAVAAASQABATAPAAQAFAAAFgDQAEgDAAgFQAAgEgDgEIALAAQADAEABALQgBAMgIAHQgIAIgMAAQgOAAgIgKg");
	this.shape_227.setTransform(259.9,-238.5);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FFFFFF").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_228.setTransform(239.9,-237.2);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FFFFFF").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_229.setTransform(228.2,-238.5);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FFFFFF").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_230.setTransform(218.4,-235.9);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FFFFFF").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQADgLAAgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_231.setTransform(211,-237.2);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_232.setTransform(203.2,-237.3);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FFFFFF").s().p("AgeAiQgNgOAAgUQAAgUANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgDgKAAQgMgBgKAKQgLAJAAAMQAAANAKAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgMgEQAEAFAAAJQAAAEgFAFQgGAFgHgBQgIAAgFgEQgGgFAAgJQAAgIAIgGQAGgFAKAAQAOAAAKAIQALAJAAANQAAASgNAMQgMALgQAAQgTAAgOgNgAgCgKQgCACAAADQAAADACACQACACADAAQAEAAACgCQACgBAAgEQAAgDgCgCQgDgDgDAAQgDABgCACg");
	this.shape_233.setTransform(192.8,-237.2);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_234.setTransform(176.6,-237.3);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FFFFFF").s().p("AgaA3QgLgFgGgKQgKgOAAgWQAAgNAIgLQAIgMALAAQAKAAADAIQAGgJALAAQAKAAAHAIQAHAHABALQABALgKAKQgJAKgMAAQgMAAgIgIQgIgIABgJIACgHIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAHQAAAQALAKQAMALAQgBQAQABAMgMQAMgNAAgQQAAgVgKgLQgLgOgUAAQgZAAgKASIgMAAQAGgOANgIQAMgHAQAAQAZABAOAOQAQAQABAbQABAagPASQgPASgaABQgNAAgMgGgAgPgEQAAAEAFAEQAEADAGABQAGgBAGgEQAFgEAAgGIAAgDQgBAEgEADQgFACgFABQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABADADAAQABAAAAgBQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgCgDAAQgFABAAAGg");
	this.shape_235.setTransform(165,-238.5);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FFFFFF").s().p("AgtAIQgDgFAAgGQAAgGADgEIAKAAQgCACAAADQAAAEADADQAJAKAZAAQAPAAAKgFQAHgCADgDQAEgDgBgGQABgGgGgGIAIgFQAIAHABAOQAAAGgCAEQgKAYgmAAQgiAAgLgUg");
	this.shape_236.setTransform(153.5,-231.9);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FFFFFF").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_237.setTransform(135,-237.2);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FFFFFF").s().p("AAeAVQAIgFAAgGIgBgDQgBgGgMgCQgKgDgOAAQgNAAgKAEQgMACgBAFIgBADQAAAHAHAEIgKAAQgJgDAAgNQAAgEABgBQAGgMAOgFQALgEARAAQAoAAAIAVQACABAAAEQAAANgJADg");
	this.shape_238.setTransform(122.7,-243.1);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FFFFFF").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_239.setTransform(110.6,-237.2);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FFFFFF").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPAAAQQgBARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_240.setTransform(102.2,-237.2);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FFFFFF").s().p("AgaA3QgLgFgGgKQgKgOAAgWQAAgNAIgLQAIgMALAAQAKAAADAIQAGgJALAAQAKAAAHAIQAHAHABALQABALgKAKQgJAKgMAAQgMAAgIgIQgIgIABgJIACgHIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAHQAAAQALAKQAMALAQgBQAQABAMgMQAMgNAAgQQAAgVgKgLQgLgOgUAAQgZAAgKASIgMAAQAGgOANgIQAMgHAQAAQAZABAOAOQAQAQABAbQABAagPASQgPASgaABQgNAAgMgGgAgPgEQAAAEAFAEQAEADAGABQAGgBAGgEQAFgEAAgGIAAgDQgBAEgEADQgFACgFABQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABADADAAQABAAAAgBQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgCgDAAQgFABAAAGg");
	this.shape_241.setTransform(94.3,-238.5);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FFFFFF").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_242.setTransform(76.7,-237.2);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FFFFFF").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_243.setTransform(64.8,-237.3);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FFFFFF").s().p("AgdAiQgOgOAAgUQAAgUAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgLAAQAAgGgJgEQgIgDgKAAQgMgBgKAKQgKAJAAAMQAAANAJAIQAJAIANAAQAKAAAIgGQAJgFAAgLQAAgMgMgEQAEAFAAAJQAAAEgFAFQgGAFgHgBQgIAAgFgEQgGgFAAgJQAAgIAHgGQAHgFAKAAQAOAAALAIQAKAJAAANQAAASgNAMQgLALgSAAQgSAAgNgNgAgBgKQgDACAAADQAAADADACQABACADAAQADAAADgCQACgBAAgEQAAgDgCgCQgCgDgEAAQgDABgBACg");
	this.shape_244.setTransform(54.4,-237.2);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FFFFFF").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgXALgMQAFgFAGgEQAHgEAJAAIAIABIAAAKQAMgLASAAQAUABAKANQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgKQgIAGgEABQgHADgJAAIgDAAgAANAjIAEABQAGAAACgEIAAgCIgDgLQgCAIgHAIgAAgAOIACAHQACAEAAAEQAHgHAAgJQAAgFgFAAQgGAAAAAGgAgjgbQgHAHgBAKQgBANAGAKQAHALAOAAQAJABAIgFQAHgFAAgIQABgEgDgDQgDgEgDAAQgEAAgDADQgCABAAAEIAAAFIgLAAQgCgLABgLQABgKAJgLIgGgBQgKAAgHAIgAAGgPQAOAFACAMQACgFAIgBQAGgBADADQgBgJgHgGQgHgIgLAAQgJgBgGADQgIAEgCAIQADgFAIAAIAFABg");
	this.shape_245.setTransform(43.7,-237.2);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FFFFFF").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_246.setTransform(32,-238.5);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FFFFFF").s().p("AAAA1QgHALgPAAQgNAAgJgKQgIgLABgNQAAgNAJgIIgNAAIAAgJIATAAQgEgCAAgHIABgFQAGgKAUAAQANAAAHAIQAIAJAAAMIAAAEIglAAQgGAAgFAFQgEAEAAAHQAAAGAFAFQAFAFAGAAQAHAAAEgDQAFgDAAgGIAAgNIAKAAIAAANQAAAGAGADQAFADAHAAQAKAAAFgKQAFgJAAgMQAAgRgKgLQgKgKgOAAIgpAAQgOAAAAgMQAAgXAuABQAZAAAMALQAJAIgBALIgKAAQAAgHgHgFQgKgIgSAAQgcAAAAAJQAAAFAKAAIAeAAQAQAAANANQAPAPAAAZQAAAUgJANQgJAQgQAAQgQAAgGgLgAgTgNQADACABAEQABAEgDADIASAAQAAgGgDgEQgEgEgHAAIgGABg");
	this.shape_247.setTransform(14.9,-238.9);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FFFFFF").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgLIA1AAIAAAKIgMAJQgGAFgFAFQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_248.setTransform(6.3,-237);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FFFFFF").s().p("AgmAxQgPgLAAgVQABgRAOgFQgHgDAAgHQAAgLAKgEQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABARAWAEQAGABAJAAQATAAALgMQAKgLABgRQAAgSgLgNQgLgOgVgBQgYAAgNASIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQASAAAYQAAAZgOAQQgPATgZAAQgXAAgPgLgAgZgVQACABABAEQACADgCAEIAUAAQgBgGgEgDQgFgEgGAAQgEgBgDACg");
	this.shape_249.setTransform(-2.6,-238.5);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FFCC80").s().p("AAlAaQAJgGAAgIIAAgEQgCgHgPgDQgMgCgRAAQgQgBgMAEQgPACgCAHIAAAEQAAAJAIAFIgNAAQgKgDAAgQQAAgGABgCQAIgOAQgGQANgFAWAAQAxAAALAZQABADAAAFQAAAPgLAEg");
	this.shape_250.setTransform(-324.4,2.6);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FFCC80").s().p("AgYAvQgLgIAAgQQAAgNAFgJQADgGAIgIQAIgKAXgTIgsAAIAAgNIBBAAIAAANIgPAMIgNALIgMAQQgGAIABAKQABAGAEAEQAFADAFAAQAMAAADgKIABgGQAAgEgCgEIANAAQAGAJAAAMQgBANgLAIQgKAJgOAAQgPAAgJgIg");
	this.shape_251.setTransform(-336.7,10.1);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FFCC80").s().p("AAQAsQAMAAAKgFQAJgFADgLQAFgMgBgKQgBgNgJgJQgJgLgNABQgVABgFATQAEgDAEAAQAHAAAEADQAMAIAAAUQgBASgQAMQgOAKgUAAQgWAAgNgMQgOgNAAgUQAAgUAPgLQgEgBgDgEQgCgFgBgFQAAgKAJgHQAJgFAKAAQAKAAAHAGQAIAGACAKQANgWAZAAQAWgBANASQAOARAAAXQAAAYgQAQQgRAQgZAAgAgygHQgGAGAAAIQAAALALAHQAKAFANAAQAMAAAKgFQAJgFABgJQAAgFgCgEQgCgDgEABQgDAAgDABQgBADAAAEIgNAAIgBgLIABgKIgQAAQgJAAgHAGgAgvgpQAEACACAFQACAFgCAEIAOAAQAAgJgFgEQgEgEgGAAIgFABg");
	this.shape_252.setTransform(-348.2,9.8);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#FFCC80").s().p("AgrA0QgUgRAAgZQAAgYASgNQgCgMAFgJQAKgTAVACQAKABAGAHQAHgJAPABQANAAAIAJQAIAKAAAWIg4AAIgXABQAAAPAKAKQALAKAOgBQALgBAJgGQAIgGACgJIAOAAQgEAQgNAKQgNAKgSAAQggABgOgYIgBAIQAAARAOAMQANAKAQAAQAaAAANgQQAIgJgBgMIAOAAQABAPgKAOQgSAagkAAQgZAAgTgPgAgUgvQgDAEAAAHQAEgBAGAAIAMAAQAAgPgLAAQgFAAgDAFgAAMglIAXAAQgBgPgKAAQgMAAAAAPg");
	this.shape_253.setTransform(-369.6,10.7);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAglAagQQAJgFAOAAIAHAAIAAANQAQgOAVABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgLQgKAHgGACQgHACgLAAIgEAAgAAJgSQATAFAAAYQAAASgNAPQANACAJgGQAOgKgBgYQAAgOgJgLQgJgLgOgBQgagCgDASQAFgEAIAAIAHABgAguggQgHALAAAOQAAAPAIAKQAJALAPABQAMAAAJgFQAJgGABgJQAAgGgDgEQgEgFgFAAQgFABgCADQgDACAAAFIABAFIgOAAQgDgOACgMQABgNAKgOIgIgBQgOAAgJALg");
	this.shape_254.setTransform(-406.3,9.8);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#FFCC80").s().p("AgCBNQgZgBgPgPQgPgPAAgYQgBgPAGgMQAHgKANgMIAYgYQAIgKAEgHIAXAAQgFAJgGAIQgHAHgLAIQARgCAMAFQAiANAAAoQAAAZgSARQgRAQgaAAIgCgBgAgegLQgKALAAAOQAAAOAKAJQAMAMATAAQAQAAALgIQAPgLAAgSQAAgIgDgHQgEgKgLgGQgJgFgNAAQgWAAgLANgAgxgpIgLAAIAAgKIALAAIAAgQIgMAAIAAgKIAfAAQAJAAAGAFQAGAFAAAJQAAAJgIAFQgHAEgNAAIAAAQQgHAIgFAIgAglhCIAAAOQALAAAAgHQAAgHgIAAIgDAAg");
	this.shape_255.setTransform(-420,7.9);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#FFCC80").s().p("AgvA9QgTgOABgZQABgVASgIQgJgCAAgKQAAgMAMgGQAKgFAMACQAQABAJAMQAJAMAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAVAbAFQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAWIgOAAQAHgQARgKQAPgJAVAAQAdABAUAUQATAWAAAeQAAAfgRAUQgTAWgeAAQgdAAgSgMgAgfgaIAEAGQACAFgCAEIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_256.setTransform(-433.1,8.3);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#FFCC80").s().p("AgvA9QgTgOABgZQABgWASgHQgJgCAAgKQAAgMAMgFQAKgFAMAAQAQACAJAMQAJAMAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAVAbAFQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAWIgOAAQAHgRARgIQAPgKAVABQAdAAAUAUQATAWAAAeQAAAfgRAUQgTAXgegBQgdABgSgNgAgfgaIAEAGQACAFgCAEIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_257.setTransform(-175.4,-19.3);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#FFCC80").s().p("Ag4AnIAAgWIBkAAIAAgjIAHgJQAFgGACgFIAABNg");
	this.shape_258.setTransform(-189.2,-12.1);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#FFCC80").s().p("AATBGIAAgaQgRACgNgBQgZgBgPgLQgSgNABgWQAAgMAFgKQAGgIAJgEQgEgCgCgDQgCgDAAgDQAAgIAGgFQALgJAUAAQAgABAIARQACgIAHgFQAGgEAIgBIAOAAIAAAOIgDgBQgEgBgBAFQAAACAFAEIAJAIQAFAFAAAIQABAOgMAHQgLAFgNgBIAAAdIALgDIAJgDIAAAMIgJAEIgLADIAAAcgAgfgUQgIAJAAALQABAYAbAHQAGABAIAAIAQgBIAAg8IgfAAQgMAAgHAJgAAigiIAAAWIACABQAFgBAEgDQAFgDgBgGQAAgGgFgHQgGgJAAgDIABgDQgFACAAAQgAgbg2QADADAAAEQAAAEgDABIADAAIADAAIAdAAQgCgIgKgEQgGgBgGAAIgLABg");
	this.shape_259.setTransform(-204.5,-16.5);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#FFCC80").s().p("Ag1BDQgPgFgFAAQgGAAgGAHIgKgGIAUgeQgJgLAAgSIACgMQAEgSAQgJQANgHAQAAIAIAAIAUgdIAOAAIgTAeIAKAFQAFACADAEQADgFADgCQAGgFAJAAQAJAAAHAHQAIgIALABQAfABABA0QABAZgJAQQgLATgVABQgPAAgHgIQgIgIAAgNIAAgHIAOAAIAAAEQAAARAOAAQAKAAADgPQACgIABgTQAAgPgCgLQgDgPgGAAQgJAAAAAOIAAAbIgOAAIAAgbQAAgOgJgBQgEAAgCADQgDACAAADQALAOAAAVQgBAagPAOQgPAPgaABIgCAAQgKAAgNgFgAg4A0QALAFAKAAIAKgBQAPgDAIgKQAIgLgBgOQAAgOgIgKQgJgKgOgBIgMARIAHAAQAIAAAGAEQAGAFABAIQAAAJgFAHQgEAHgJADQgHACgIAAQgMAAgKgFIgHALIAFgBQAGAAAFACgAg5AdQACACAFABQAJABADgBQgIgCgCgFQgCgEABgEgAgmASQAAAHAHAAQAGAAAAgHQAAgGgGAAQgHAAAAAGgAg/gFQgEAGAAAJQAAAFABAEIAZgkQgPADgHAJg");
	this.shape_260.setTransform(-242.8,-19.1);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#FFCC80").s().p("AgZA2IAAgOQADADAHAAQAIAAAEgNQAFgNAAgQQgBgOgDgNQgGgPgGgBQgHAAgEAEIAAgOQAFgFANAAQAOAAALAVQAIARABAUQgBAVgIARQgLASgPAAQgKAAgHgDg");
	this.shape_261.setTransform(-254.9,-17.7);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#FFCC80").s().p("AgfBDQgOgHgIgLQgMgRAAgaQAAgRAKgOQAJgPAOAAQAMABAEAKQAHgLANAAQANAAAJAJQAIAJABANQABAPgLALQgMAMgPAAQgOAAgJgJQgKgJAAgNQABgEACgFIADgIQABAAAAgBQAAAAgBgBQAAAAAAgBQgBgBAAAAQgCgCgDABQgHAAgDAJQgEAHAAAKQgBATAPAMQAPANATAAQAUAAAOgPQAPgPAAgUQgBgYgMgPQgNgRgYABQgfAAgMAVIgPAAQAHgRAQgKQAPgIAUAAQAeAAARARQAUAVABAgQABAhgSAVQgTAXgfAAQgQAAgOgHgAgTgFQAAAFAGAFQAGAEAHABQAIAAAHgGQAGgEAAgIIAAgEQgBAEgGAEQgFAEgHgBQgPAAgDgNIgDAJgAgDgVQAAAEADACQABACADAAQAEAAACgCQADgCAAgEQAAgDgDgDQgCgCgEAAQgHAAAAAIg");
	this.shape_262.setTransform(-264.5,-19.3);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#FFCC80").s().p("Ag4AKQgDgHAAgHQgBgIAEgEIANAAQgDADAAADQAAAGAEADQAMAMAeAAQARgBAOgFQAHgCAEgEQAFgEAAgIQAAgHgHgHIAJgGQAMAJgBAQQABAIgDAEQgLAegvAAQgqAAgOgYg");
	this.shape_263.setTransform(-278.5,-11.2);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#FFCC80").s().p("AgfBDQgOgHgIgLQgMgRAAgaQAAgRAKgOQAJgPAOAAQAMABAEAKQAHgLANAAQANAAAJAJQAIAJABANQABAPgLALQgMAMgPAAQgOAAgJgJQgKgJAAgNQABgEACgFIADgIQABAAAAgBQAAAAgBgBQAAAAAAgBQgBgBAAAAQgCgCgDABQgHAAgDAJQgEAHAAAKQgBATAPAMQAPANATAAQAUAAAOgPQAPgPAAgUQgBgYgMgPQgNgRgYABQgfAAgMAVIgPAAQAHgRAQgKQAPgIAUAAQAeAAARARQAUAVABAgQABAhgSAVQgTAXgfAAQgQAAgOgHgAgTgFQAAAFAGAFQAGAEAHABQAIAAAHgGQAGgEAAgIIAAgEQgBAEgGAEQgFAEgHgBQgPAAgDgNIgDAJgAgDgVQAAAEADACQABACADAAQAEAAACgCQADgCAAgEQAAgDgDgDQgCgCgEAAQgHAAAAAIg");
	this.shape_264.setTransform(-325.3,-19.3);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#FFCC80").s().p("Ag4AKQgDgHAAgHQgBgIAEgEIANAAQgDADAAADQAAAGAEADQAMAMAeAAQARgBAOgFQAHgCAEgEQAFgEAAgIQAAgHgHgHIAJgGQAMAJgBAQQABAIgDAEQgLAegvAAQgqAAgOgYg");
	this.shape_265.setTransform(-339.3,-11.2);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAgbANgPQAGgHAHgEQAKgFAKAAIAKAAIAAANQAPgOAWABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgMQgKAHgGACQgIADgLAAIgDAAgAAPArIAFABQAIAAACgFIABgCIgEgOQgCAKgKAKgAAnARIADAIQACAGAAAEQAJgIAAgMQAAgFgHAAQgHAAAAAHgAgrgiQgJAJgBANQgBAQAHAMQAJANARABQALAAAJgFQAJgGABgKQABgFgEgEQgDgGgFABQgFAAgDADQgDACAAAFIABAGIgOAAQgDgOACgNQABgMALgOIgHgBQgMAAgJAJgAAIgSQARAGABAOQAEgGAJgBQAHgBAEAEQgCgMgIgHQgJgJgNgBQgLgBgHAEQgKAFgCAJQADgFAKAAIAHABg");
	this.shape_266.setTransform(-339.7,-17.7);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#FFCC80").s().p("AgEBJQgYgBgPgQQgPgPgBgXQAAgQAGgLQAGgLANgMIAZgXQAJgLACgGIAYAAQgFAIgHAIQgGAIgLAHQAQgBANAEQAiANAAAoQAAAagTAQQgRAQgZAAIgDAAgAgfgQQgKAMAAANQAAAOAJAJQANANATAAQAQAAALgJQAPgLAAgRQAAgIgEgHQgDgLgLgGQgKgFgNAAQgVAAgLANg");
	this.shape_267.setTransform(-375.4,-19.2);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#FFCC80").s().p("AgvA9QgTgOABgZQABgWASgHQgJgCAAgKQAAgMAMgFQAKgFAMAAQAQACAJAMQAJAMAAAPIgnAAQgJAAgGAFQgGAFAAAIQABAVAbAFQAIACALgBQAXAAANgOQANgOAAgUQABgXgNgQQgOgRgagBQgdAAgQAWIgOAAQAHgRARgIQAPgKAVABQAdAAAUAUQATAWAAAeQAAAfgRAUQgTAXgegBQgdABgSgNgAgfgaIAEAGQACAFgCAEIAYAAQgBgHgFgEQgFgFgJAAIgIABg");
	this.shape_268.setTransform(-433.1,-19.3);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_269.setTransform(-225,-45.3);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#FFCC80").s().p("AgYA5QgXAAgNgRQgLgPAAgXQAAglAagQQAJgFAOAAIAHAAIAAANQAQgOAVABQAYAAANAQQANAPAAAYQAAAYgNARQgPARgXAAIgHAAIAAgLQgKAHgGACQgHACgLAAIgEAAgAAJgSQATAFAAAYQAAASgNAPQANACAJgGQAOgKgBgYQAAgOgJgLQgJgLgOgBQgagCgDASQAFgEAIAAIAHABgAguggQgHALAAAOQAAAPAIAKQAJALAPABQAMAAAJgFQAJgGABgJQAAgGgDgEQgEgFgFAAQgFABgCADQgDACAAAFIABAFIgOAAQgDgOACgMQABgNAKgOIgIgBQgOAAgJALg");
	this.shape_270.setTransform(-253.4,-45.3);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#FFCC80").s().p("AgOAbIAAgWIgLAAIAAgLIALAAIAAgOIgLAAIAAgKIAdAAQAKgBAFAFQAHAFAAAJQAAAJgIADQgIAFgLAAIAAAbgAgBgUIAAAOQAKAAAAgGQAAgIgJAAIgBAAg");
	this.shape_271.setTransform(-263.4,-53);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgGAAQgJAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_272.setTransform(-266,-43.6);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#FFCC80").s().p("Ag5AnIAAgVIBlAAIAAgkIAHgJQAFgGACgFIAABNg");
	this.shape_273.setTransform(-279,-39.6);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#FFCC80").s().p("AgzAaQgIgEAAgPIABgHQAIgZAzAAQAZAAANAFQATAGACAOQACAKgHAGQgHAGgLAAQgJAAgGgEQgHgFAAgJQAAgEACgEIgQgBQgnAAgGAMIgBAFQAAAJAIAFgAAeACQAAADACACQADACADAAQAHAAAAgHQAAgDgCgCQgCgCgDAAQgIAAAAAHg");
	this.shape_274.setTransform(-300.4,-52.6);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#FFCC80").s().p("AADBGQgLgFAAgKQgBgJAHgFQgYABgRgNQgQgOACgTQABgZAUgLQgDgBgDgDQgCgEAAgEIABgFQAEgKALgDQAIgDAOAAQAbAAALAOQAJALABAPIgpAAQgMAAgJAIQgIAIgBALQgBANAMAKQALAKANAAQAYABAQgIIAAANIgPAGQgPADAAAGQAAAFADACQAEADAGAAQAHAAAEgEQAFgDAAgGIgBgFIAOAAIAAAGQAAAMgKAIQgJAHgOABIgGAAQgJAAgHgEgAgSg6IADACQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABQABAFgEABIAGAAIAiAAQgFgJgKgDQgGgBgIAAQgIAAgFABg");
	this.shape_275.setTransform(-298.4,-43.6);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#FFCC80").s().p("AgtA9QgSgRAAgbQAAgSAIgNQAJgOAPAAQAPAAACAMQAFgOAPAAQALAAAIAKQAIAJAAAMQAAAPgKAKQgJAKgOAAQgMAAgJgHQgJgHAAgLIACgSQACgHgIAAQgFAAgEAJQgDAGAAAJQAAATAOALQANALATAAQAUAAANgOQAOgNAAgUQAAgPgHgLQgHgMgOAAIhNAAIAAgMQAUABAAgCQAAgBgEgBQgEgCAAgEQAAgPAQgFQAJgDAVAAQAQAAALAEQAOAEAKALQAHAIAAANIgNAAQgCgNgNgGQgMgGgRAAQgMAAgFACQgIACAAAHQAAABAAAAQAAABAAAAQABABAAAAQABABAAAAQABABAAAAQABAAAAAAQABABABAAQAAAAABAAIAjAAQAMAAAIAEQAaALAAApQAAAegSATQgSAUgcAAQgaAAgTgSgAgPADQAAAGAFAEQAEAFAHAAQAHAAAGgEQAFgFAAgIQgFAIgKABQgMAAgGgLIgBAEgAAAgPQgBACAAADQAAADABACQACACADAAQAAAAABAAQABAAAAgBQABAAAAAAQABgBAAAAQADgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_276.setTransform(-318.2,-47.4);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#FFCC80").s().p("AgDBMQgZgBgPgQQgPgPAAgXQAAgVANgPQgEgCgEgEQgDgFgCgEQgCgGAAgGQAAgKAJgMQALgLARAAQAFAAAGABQALADABAJQACgDAEgHIAXAAQgEAJgJAKQgIAJgKAGQAQgBAMAEQAjANAAAoQAAAagSAQQgSAQgZAAIgDAAgAgegNQgLAMAAANQAAAOAKAJQANANASAAQAQAAALgJQAPgLABgRQAAgIgEgHQgDgLgLgGQgKgFgNAAQgVAAgLANgAgngwQgEAHAAAGQAAAIAGACIAEgEIAMgKQgHAAgEgDQgEgEAAgFIgDADgAgZg3QAAAHAHAAQAHAAAAgHQAAgHgHAAQgHAAAAAHg");
	this.shape_277.setTransform(-342.4,-47.1);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#FFCC80").s().p("AgZA2IAAgOQADADAIAAQAGAAAFgNQAFgNAAgPQAAgPgEgNQgGgPgGgBQgGAAgFAEIAAgOQAGgFAMAAQAPAAAJAVQAJARAAAVQAAAUgJARQgKASgQAAQgJAAgHgDg");
	this.shape_278.setTransform(-387.9,-45.2);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#FFCC80").s().p("AAAAtQgJAMgPAAQgWAAgMgQQgKgPABgaQAAgbATgQQANgMAQgBQAHgBAFABIAAAOQgHgDgLADQgMAEgHALQgHAKAAANQAAAiAYAAQAJAAAGgFQAGgFAAgJIAAgLIANAAIAAALQAAAJAGAFQAFAFAJAAQALAAAHgHQAHgHAAgLQAAgJgIgHQgHgHgKAAIghAAQgBgUAJgKQAIgJAQABQAfAAAAAYIgBAGIgLAAQALAEAGAKQAGAKAAALQAAAVgKAOQgLANgTAAQgTAAgJgMgAAJgeIAUAAIAIABIABgEQAAgKgOAAQgNAAgCANg");
	this.shape_279.setTransform(-397.5,-45.3);

	this.instance_16 = new lib.Path_2();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-0.4,281.5,1,1,0,0,0,504.9,23.5);
	this.instance_16.alpha = 0.539;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_8},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97,p:{x:-379.2}},{t:this.shape_96},{t:this.shape_95,p:{x:-369.4}},{t:this.shape_94,p:{x:-359}},{t:this.shape_93,p:{x:-345.6}},{t:this.shape_92,p:{x:-332.9,y:-45.3}},{t:this.shape_91},{t:this.shape_90,p:{x:-311.7}},{t:this.shape_89,p:{x:-297.8}},{t:this.shape_88},{t:this.shape_87,p:{x:-281.9}},{t:this.shape_86,p:{x:-267.3}},{t:this.shape_85,p:{x:-242.8}},{t:this.shape_84,p:{x:-225.2}},{t:this.shape_83,p:{x:-224.5}},{t:this.shape_82,p:{x:-210.3,y:-45.3}},{t:this.shape_81},{t:this.shape_80,p:{x:-432.9}},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76,p:{x:-398.8}},{t:this.shape_75,p:{x:-376.4}},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71,p:{x:-345.7}},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66,p:{x:-296.4}},{t:this.shape_65,p:{x:-281.1,y:-17.7}},{t:this.shape_64},{t:this.shape_63,p:{x:-247}},{t:this.shape_62},{t:this.shape_61,p:{x:-220.9}},{t:this.shape_60},{t:this.shape_59,p:{x:-208.7}},{t:this.shape_58},{t:this.shape_57,p:{x:-175.3}},{t:this.shape_56,p:{x:-163.6,y:-17.7}},{t:this.shape_55},{t:this.shape_54,p:{x:-141.7,y:-17.7}},{t:this.shape_53,p:{x:-131.1,y:-13.4}},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41,p:{x:52.6}},{t:this.shape_40,p:{x:64.4,y:-237.3}},{t:this.shape_39},{t:this.shape_38,p:{x:90.6,y:-237.2}},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28,p:{x:204.4,y:-237.2}},{t:this.shape_27,p:{x:216.3,y:-237.3}},{t:this.shape_26,p:{y:-91}},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16,p:{x:60.8,y:-90.2}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13,p:{x:98.1,y:-90.2}},{t:this.shape_12,p:{x:110,y:-90.3}},{t:this.shape_11},{t:this.shape_10,p:{x:-13.8,y:99.3}},{t:this.shape_9,p:{x:0.1,y:95.8}},{t:this.shape_8,p:{x:4,y:89.4}},{t:this.shape_7},{t:this.shape_6,p:{x:23.2,y:95.7}},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3,p:{x:64.6,y:95.8}},{t:this.shape_2},{t:this.shape_1,p:{x:76.8,y:95.7}},{t:this.shape},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2}]},1).to({state:[{t:this.instance_16},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_85,p:{x:-430}},{t:this.shape_94,p:{x:-412.4}},{t:this.shape_83,p:{x:-411.7}},{t:this.shape_279},{t:this.shape_278},{t:this.shape_89,p:{x:-378.2}},{t:this.shape_84,p:{x:-356.4}},{t:this.shape_87,p:{x:-355.7}},{t:this.shape_277},{t:this.shape_93,p:{x:-330.5}},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_90,p:{x:-279.2}},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_97,p:{x:-239}},{t:this.shape_269},{t:this.shape_86,p:{x:-210.4}},{t:this.shape_95,p:{x:-205.4}},{t:this.shape_268},{t:this.shape_57,p:{x:-422.3}},{t:this.shape_92,p:{x:-411.4,y:-17.7}},{t:this.shape_75,p:{x:-389.4}},{t:this.shape_267},{t:this.shape_76,p:{x:-361.7}},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_63,p:{x:-312.5}},{t:this.shape_82,p:{x:-299.8,y:-17.7}},{t:this.shape_71,p:{x:-295.2}},{t:this.shape_80,p:{x:-278.6}},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_59,p:{x:-225.9}},{t:this.shape_259},{t:this.shape_66,p:{x:-189.8}},{t:this.shape_258},{t:this.shape_257},{t:this.shape_61,p:{x:-155.4}},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_65,p:{x:-384,y:9.8}},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_56,p:{x:-325,y:9.8}},{t:this.shape_250},{t:this.shape_54,p:{x:-303.1,y:9.8}},{t:this.shape_53,p:{x:-292.5,y:14.1}},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_10,p:{x:-16.3,y:-233.7}},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_8,p:{x:80.8,y:-243.6}},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_9,p:{x:122.5,y:-237.2}},{t:this.shape_238},{t:this.shape_237},{t:this.shape_41,p:{x:153.2}},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_26,p:{y:-50}},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_28,p:{x:46.2,y:-49.2}},{t:this.shape_217},{t:this.shape_40,p:{x:67.3,y:-49.3}},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_27,p:{x:114.1,y:-49.3}},{t:this.shape_212},{t:this.shape_16,p:{x:126.4,y:-49.2}},{t:this.shape_13,p:{x:144.6,y:-49.2}},{t:this.shape_211},{t:this.shape_210},{t:this.shape_12,p:{x:168,y:-49.3}},{t:this.shape_209},{t:this.shape_6,p:{x:194.6,y:-49.3}},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_3,p:{x:265.8,y:-49.2}},{t:this.shape_203},{t:this.shape_1,p:{x:278,y:-49.3}},{t:this.shape_38,p:{x:285.9,y:-49.2}},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-640,-360,1280,720);


// stage content:
(lib.civic_86 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		
		var clip = this;
		var count = 4; // Define the number of buttons |Sanka|
		var item_chk;
		
		
		this.home_btn.visible = false;
		this.slider_mov.visible = false;
		
		
		
		
		for (var i=1; i<=count; i++){
			//var btn = clip["btn"+i];
			//btn.mouseEnabled= false;
		}
		
		
		
		
		
		var btn = []; //Define the array switchyes
		
		for (var i=1; i<=count; i++){
			var btnn = clip["btn"+i];
			btnn.name = i;
			btn[i-1] = clip["btn"+i];
			btn[i-1].addEventListener("click", btn_clk.bind(this));
		}
		
		
		function btn_clk(evt)
		{
		
			for (var i=1; i<=count; i++){
			var btn = clip["btn"+i];
			btn.mouseEnabled= false;
			}
			
			
			this.home_btn.visible = true;
			var item = evt.currentTarget.name;  // Selecting (current target) button name |Sanka|
		
			//var mov = clip["mov"+item];
			this.slider_mov.visible = true;
			this.slider_mov.gotoAndStop(item-1);
			
				item_chk = item;
				counta = item;
			
				next_back_fun();
				
			
		
			
		
		}
		
		
		this.home_btn.addEventListener("click", close_clk.bind(this));
		
		function close_clk()
		{
			this.slider_mov.visible = false;
			for (var i=1; i<=count; i++)
			{
				var btn = clip["btn"+i];
				btn.mouseEnabled= true;
			}
			
			this.home_btn.visible = false;
			
				clip.back.visible = false;
				clip.next.visible = false;
		}
		var clip = this; 
		this.stop();
		var gcounta = 4;
		
		this.back.visible = false;
		this.next.visible = false;
		
		
		///////////// back next
		var counta;
		var slider_slider_movx = clip["slider_mov"].x ;
		
		
		
		
		//var slider_slider_movount = gcounta - 1;
		
		this.back.visible = false;
		
		
		
		this.next.addEventListener("click", next_btn.bind(this));
		
		
		function next_btn()
		{
			
			
			
			console.log(counta);
			counta = counta + 1;
			//this.getStage().getChildAt(0).slider_mov.crnt_fun(counta);
			if(counta > 0)
			{
				this.back.visible = true;
				
				
			}
			
			
				if(counta == gcounta)
			{
				this.next.visible = false;
			}
			
		
			//alert(counta)
			
			//clip["slider_mov"].x = 301.45;
			clip["slider_mov"].gotoAndStop(counta-1);
			var crnt = clip["slider_mov"];
			//createjs.Tween.get(crnt).to({x:slider_slider_movx}, 350, crnt.transitionEase);
			
			
		
			
			//this.getStage().getChildAt(0).slider_mov.stop_sound_fun();
		
		}
		
		
		
		
		this.back.addEventListener("click", back_btn.bind(this));
		
		
		function back_btn()
		{
			
			this.next.visible = true;
			
			console.log(counta);
			
			
			
			
			counta = counta - 1;
			if(counta == 1)
			{
				this.back.visible = false;
				
			}
			
		
			
			//clip["slider_mov"].x = -348.55;
			clip["slider_mov"].gotoAndStop(counta-1);
			
			//this.getStage().getChildAt(0).slider_mov.crnt_fun(counta);
			
			//this.getStage().getChildAt(0).slider_mov.stop_sound_fun();
		
		}
		
		
		function next_back_fun()
		{
				
			
				clip.back.visible = false;
				clip.next.visible = false;
			
			
				
				if(item_chk < 4 ){
					clip.next.visible = true;
				}
				
				if(item_chk > 1 )
				{
					clip.back.visible = true;
				}
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// next back
	this.next = new lib.next();
	this.next.parent = this;
	this.next.setTransform(1123.7,794);
	new cjs.ButtonHelper(this.next, 0, 1, 1);

	this.back = new lib.back();
	this.back.parent = this;
	this.back.setTransform(1065.7,794);
	new cjs.ButtonHelper(this.back, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.back},{t:this.next}]}).wait(1));

	// home_btn
	this.home_btn = new lib.home_btn();
	this.home_btn.parent = this;
	this.home_btn.setTransform(1238.7,361.5);
	new cjs.ButtonHelper(this.home_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.home_btn).wait(1));

	// slder
	this.slider_mov = new lib.slider_mov();
	this.slider_mov.parent = this;
	this.slider_mov.setTransform(641,360);

	this.timeline.addTween(cjs.Tween.get(this.slider_mov).wait(1));

	// btn
	this.btn4 = new lib.btn4();
	this.btn4.parent = this;
	this.btn4.setTransform(643.6,563.6);
	new cjs.ButtonHelper(this.btn4, 0, 1, 1);

	this.btn3 = new lib.btn3();
	this.btn3.parent = this;
	this.btn3.setTransform(643.6,471.6);
	new cjs.ButtonHelper(this.btn3, 0, 1, 1);

	this.btn2 = new lib.btn2();
	this.btn2.parent = this;
	this.btn2.setTransform(643.6,379.6);
	new cjs.ButtonHelper(this.btn2, 0, 1, 1);

	this.btn1 = new lib.btn1();
	this.btn1.parent = this;
	this.btn1.setTransform(643.6,287.6);
	new cjs.ButtonHelper(this.btn1, 0, 1, 1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape.setTransform(1026.5,144.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEBxQgQAYgfAAQgkAAgTgeQgSgbABguQACg0AfgbQAYgUAwgBIAOAAIAAAZQgYgBgLACQgQABgMAJQgYASgBAiQgBAbAJARQALAUAWAAQARAAAJgKQAJgLAAgPIAAgfIAYAAIAAAfQAAAkAlAAQAYAAANgcQAMgcgCgpQgBgngUgaQgbglgxAAQgcgBgWALQgXAMgJASIgcAAQAIgcAfgSQAegRAoAAQAgAAAbANQAUAJANANQAlApABBIQACA1gVAjQgXAlgoAAQggAAgOgYg");
	this.shape_1.setTransform(999.4,140.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_2.setTransform(981.4,144.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AA6BpQglAAgRgZQgPAZgpAAQgdAAgUgXQgVgXABgfQABgiAWgRIgfAAIAAgYIAgAAQgHgEAAgMQAAgFACgFQALgfAsAAQAmAAAMAaQAQgaAkAAQAVAAAOAJQARALACASQACARgHAJIgFAAQAfAZAAAnQAAAhgVAYQgUAYgeAAIgBAAgAARAGIAAAXQAAANANAGQALAGAQAAQAQAAANgMQAOgNgBgRQgBgPgLgLQgMgKgQAAIhrAAQgRAAgMALQgNALAAAQQAAARAMAMQAMALASAAQAQAAAKgFQAOgHABgMIAAgYgAAyhPQgNABgJAJQgJAJAAANIA2AAQAEgFAAgIQgCgTgXAAIgCAAgAg8hMQAGAGACAIQAAAIgEAHIAtAAQgBgRgNgJQgJgGgLAAQgJAAgGADg");
	this.shape_3.setTransform(963.5,144.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AguBpQgkgCgXgXQgYgXgBghQgBgfAKgWIAYAAQgCAIADADQADAEAIAAQAMAAAJgGQATgMAFgPIABgJQAAgQgLgFQgKgEgJAEQAPAEAAATQAAALgHAGQgHAHgLAAQgGAAgEgCQgTgGAAgXQAAgHADgIQAKgbAmAAQAPAAALAHQAMAHAEAMQAVgcArACQAtACAWAqQAQAcgBAhQAABGgzAZQgQAIgSAAQgLAAgOgDIAAgRQgQAMgNAEQgNAEgUAAIgFAAgAAYgaQAYATAAAgQAAAggUAVIAQACQASAAAJgHQAbgUgBgoQAAgagSgVQgRgUgagBQgggCgPAUIADgBQATAAANAMgAhnAPQAAAQAJALQANARAdAAIASgBQATgCANgPQALgNAAgQQAAgKgFgHQgGgLgRgBQgIAAgNAIQgRALgEACQgMAFgMAAQgLAAgGgFIgBALg");
	this.shape_4.setTransform(924.3,144.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_5.setTransform(905.4,144.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhdBLQgcgYAAgrQAAgRAIgPQAHgQAMgHQgMgGAAgPQAAgGACgFQAKgZAsAAQAkAAAOAVQAPgVAnAAQASAAAOAJQAPAIADAQQACAMgHAMQAJAIAHAOQAHAQAAARQAAArgfAaQghAcg6AAQg7AAgigegAhUgXQgJALAAARQACAaAgANQAYAKAiAAQAjAAAYgKQAfgNACgaQAAgRgJgLQgKgKgQAAIhyAAQgQAAgKAKgAAahKQgLAHgBALIA5AAIABgGQAAgIgHgFQgIgFgIAAIgCAAQgLAAgKAGgAhBhPQAGAEABAHQACAHgFAFIAtAAQgDgTgZgFIgKgBQgGAAgFACg");
	this.shape_6.setTransform(888,144.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhYBcQghgoABg4QABg3AigkQAjgkA5AAQAxAAAhAfQAgAhAAAvQAAAngVAdQgXAdglAAQglABgVgXQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMALQANAMATAAQAXgCAOgSQANgQgBgYQgBgegPgSQgWgagtAAQgnAAgWAgQgUAcAAAqQAAAmAUAdQAUAfAjAGIASACQAsgBAYgiIAdAAQgeBBhSAAQg3AAgkgog");
	this.shape_7.setTransform(862.6,146.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAECAQgOgGgLgQQgDAMgMAIQgMAIgRAAQgcAAgRgfQgQgbAAglQAAgsAZghQAbgjArAAQAgAAAUAUQAUAUAAAfQAAAYgTAQIgKAJQgFAGgBAHQgBAYAhAAQAZAAAQgaQANgYAAgiQAAgrgZggQgaghg0AAQgZAAgVALQgXALgLATIgbAAQALgfAfgSQAcgRAlAAQA9AAAkAkQAQAQAMAdQAMAdAAAZQACA+gbAkQgYAigmAAQgWAAgNgGgAhJBQQgFAEAAAHQAAAGAFAFQAFAEAGAAQAHAAAEgEQAFgFAAgGQAAgHgFgEQgFgFgGAAQgGAAgFAFgAhaAGQgIAQAAAUQAAAPAEANQALgUAYgCQASgBAQANQACgOALgLQAPgOAAgQQgBgOgKgJQgKgJgPAAQgqAAgPAhg");
	this.shape_8.setTransform(837,141.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhXBmQggggABg0QABgkAagZQAagaAlAAIAcAAIAAAYIgYAAQgcAAgRARQgRARgBAYQgBAhAbAUQAaASAjgBQAmAAAZgbQAYgbAAgmQAAgngZgeQgZgdgmgCQgbAAgXAMQgXAMgKAUIgYAAQAMggAcgSQAcgSAoAAQA1AAAiAoQAgAmAAA4QAAA5ghAlQgjAng2AAQg1AAgfggg");
	this.shape_9.setTransform(798.7,141.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag6B6QgZgMgOgVQgWggAAgvQAAgfARgaQASgbAYABQAYACAHARQALgVAaABQAXAAAQARQAQARABAXQACAbgVAWQgVAUgbAAQgaAAgSgQQgRgQABgYQAAgJADgHQAGgKABgFQABgFgDgDQgEgDgGAAQgLACgHAPQgGAOgBATQgBAiAbAXQAaAXAkAAQAkAAAbgbQAbgbgBgnQgBgsgWgcQgYgcgtAAQg4gBgWAoIgbAAQANgfAdgRQAbgQAkAAQA4AAAeAgQAlAkACA7QACA9ghAoQgiAog5AAQgeAAgagMgAgjgKQAAALALAHQALAJANgBQAPABAMgKQAMgJAAgOIgBgGQgCAHgKAGQgKAHgMAAQgcAAgGgaQgFAOAAAEgAgGgnQAAAGAEAEQAEAFAGgBQAGAAAFgEQAEgEAAgFQAAgGgEgFQgFgEgGgBQgOAAAAAPg");
	this.shape_10.setTransform(773.5,141.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA1ACQAfADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAZAKQAVAIAhAAQAjAAAXgSQAXgSADgdIABgOQAAgdgSgWQgQgSgcAAIhTAAQgnAAAAgaQAAguBkAAQA8ABAWAQQAQAMAAARQAAAQgOAKQAgAkgBA4QgBAvgkAeQgiAdgwAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgEgOgJgHQgLgIgQAAQgGAAgFACgAAuhlQgEADAAAHQAAAGAEAEQAEAEAGAAQAGAAAEgEQAFgEAAgGQAAgHgFgDQgEgFgGAAQgGAAgEAFgAg5hgQgCAKATAAIA5AAQgDgMAGgKIgYgCQgzAAgCAOg");
	this.shape_11.setTransform(749.1,141);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("ABDAwQASgMgBgQIgBgFQgDgNgagFQgXgGgfAAQgdAAgXAGQgbAGgEAMIgBAGQAAARAQAKIgWAAQgUgHAAgdQAAgKADgFQANgaAfgKQAYgIAnAAQBZAAATAsQADAGAAAJQAAAcgTAIg");
	this.shape_12.setTransform(711.3,130.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_13.setTransform(709.7,144.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgXAIgQQAGgLAOgQQAQgSAqghIhQAAIAAgYIB3AAIAAAXIgcAUQgNALgLALQgRATgFAJQgKAQABARQABANAIAGQAIAGAKAAQAWAAAHgTQACgFAAgFQAAgJgGgHIAYAAQAMARgBAWQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_14.setTransform(688.1,144.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAiB+IAAgtQgeADgXgBQgvgCgcgVQgggXABgqQAAgVALgRQALgQAQgHQgIgDgEgGQgDgGAAgHQAAgNALgJQAUgQAlABQA6ABANAfQAFgOAMgJQAMgJAPAAIAYAAIAAAXIgEgBQgIAAgBAIQAAADAIAGQAMAKAEAFQAKALAAAOQABAYgWAOQgTAKgYgCIAAA1IAVgGIAQgGIAAAXIgRAHIgUAFIAAAygAg5glQgOAPAAAVQABAuAzALQAJACARAAQASAAAJgCIAAhtIg5AAQgVAAgNAQgAA+g/IAAAoIAEABQAKAAAHgGQAHgGAAgKQAAgLgKgOQgKgPAAgGIAAgEQgIACAAAdgAgxhjQAEAGAAAHQAAAGgEAEIAEgBIAGAAIA2AAQgEgOgSgHQgNgEgLAAQgJAAgJADg");
	this.shape_15.setTransform(668,146.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAbQAUAAAAgOQAAgNgQAAIgEAAg");
	this.shape_16.setTransform(637.4,129.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_17.setTransform(627.8,144.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_18.setTransform(600.7,144);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACgtQACgnAfgOQgQgFAAgRQAAgWAWgKQASgJAXABQAcACARAXQASAVgBAdIhIAAQgQAAgLAIQgLAJABAQQACAlAwAKQAPADAVAAQAoAAAZgbQAYgZAAgmQABgqgYgcQgZgfgwgCQg1gBgcAoIgaAAQANgdAegRQAdgPAlAAQA2AAAjAmQAkAnAAA3QAAA3ggAmQgiAog2AAQg2ABgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgMgJgIQgKgJgOAAQgIAAgHACg");
	this.shape_19.setTransform(575.4,141.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAbQAUAAAAgOQAAgNgQAAIgEAAg");
	this.shape_20.setTransform(558.1,129.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Ah5A7QgFgTAEgPIgTAAIAAgUIAPgQIATgUQgMAAgIgIQgHgJAAgMQAAgTAPgMQAOgLAUgCQAfgCAPAXQAegVArAAIAPABQApACAbAhQAbAggCApQgDAqgbAcQgbAegpAAIgOgBIAAgVQgbAVgqABIgEAAQhBAAgNgugAgLg8QALABANAIQANAHAIAIQATAWABAbQABAogWAaQADABAGAAQATAAANgJQAagSADgiQACgYgLgUQgKgUgXgLQgRgIgbAAQgRAAgLAEgAhHgbQgQALgYAXIARAAQgHAPAFAPQAJAcArgCQAYgCARgOQARgPABgWQABgTgOgPQgNgPgWgBQgVAAgRANgAhXhPQgGACgEADQAKACAEAFQADAHgCAJIALgJIALgJQgFgHgKgDIgFAAIgHAAg");
	this.shape_21.setTransform(548.1,144.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgEBxQgQAYgfAAQgkAAgTgeQgSgbABguQACg0AfgbQAYgUAwgBIAOAAIAAAZQgYgBgLACQgQABgMAJQgYASgBAiQgBAbAJARQALAUAWAAQARAAAJgKQAJgLAAgPIAAgfIAYAAIAAAfQAAAkAlAAQAYAAANgcQAMgcgCgpQgBgngUgaQgbglgxAAQgcgBgWALQgXAMgJASIgcAAQAIgcAfgSQAegRAoAAQAgAAAbANQAUAJANANQAlApABBIQACA1gVAjQgXAlgoAAQggAAgOgYg");
	this.shape_22.setTransform(520.6,140.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgSQAXgTACgdQACgogUgYQgQgSgbAAIhTAAQgnAAAAgaQAAguBkAAQAmAAAeAQQAiASABAdIgbAAQABgRgcgLQgXgJgdAAQgzAAgCANQgCAKATAAIBDAAQAbAAAWARQAmAfAABDQAAAvglAeQgjAdgvAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgDgOgKgHQgKgIgRAAQgGAAgFACg");
	this.shape_23.setTransform(496,141);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AhNB2QghgUgDglQgDgoAhgQQgJgBgGgGQgGgIAAgJQAAgFACgFQALgbA2ACQAeADAQAWQAQAVgCAdIhHAAQgiAAABAaQAAAUAaAKQAUAIAhAAQAjAAAYgSQAXgTACgdQACgogUgYQgQgSgbAAIhTAAQgnAAAAgaQAAguBkAAQAmAAAeAQQAiASABAdIgbAAQABgRgcgLQgXgJgdAAQgzAAgCANQgCAKATAAIBDAAQAbAAAWARQAmAfAABDQAAAvglAeQgjAdgvAAQgrAAgdgRgAg5gcQAGAEABAHQABAKgHAGIAyAAQgDgOgKgHQgKgIgRAAQgGAAgFACg");
	this.shape_24.setTransform(472.4,141);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAbQAUAAAAgOQAAgNgQAAIgEAAg");
	this.shape_25.setTransform(443,129.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAABSQgRAXgcAAQgoAAgVgeQgSgbABgvQABgyAigeQAXgVAegDQANgBAJACIAAAYQgOgFgUAGQgUAHgNATQgNAUAAAXQAAA+ArAAQAQAAALgIQAKgJAAgQIAAgVIAZAAIAAAUQAAARALAJQAKAIAQAAQATAAANgMQANgNAAgUQAAgRgNgNQgNgMgUAAIg8AAQgBglAQgSQAPgQAeABQA3ABAAArIgBAKIgUAAQAUAIALATQALASAAAVQAAAmgTAYQgTAagjAAQgjAAgQgXgAARg3IAkAAIAPABIAAgGQAAgTgYAAQgZAAgCAYg");
	this.shape_26.setTransform(434.7,144);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AhCBLQgegdAAgtQAAguAegdQAdgdAsgBQAwABAWATQAPAOABAUIgYAAQgBgNgTgJQgRgIgWABQgdgBgWAUQgXAUAAAdQAAAcAUATQAVASAdAAQAXAAASgNQATgNAAgXQAAgdgbgIQAKAKAAATQAAANgMALQgNAKgPgBQgSAAgNgLQgNgMAAgSQAAgVARgLQAPgMAWAAQAgABAYASQAWATAAAfQAAAngbAZQgbAagmAAQgqABgdgegAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_27.setTransform(411.5,144.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AglBZQgOAPggAAQgTAAgOgMQgNgMgBgTQAAgWAJgNIgQAAIAAgQIANgQQAJgLAKgJIgFABQgLAAgHgJQgGgIAAgLQAAgTAOgPQAOgOATgCQAjgDAQAYQAhgYArADQApADAcAeQAcAdABApQABBEgzAeQgLAGgWAAQgMAAgHgBIAAgQQgQARgUAAIgBAAQgVAAgPgPgAgQg/QAgADAUAWQAYAcAAAiQAAAigSAUQAFACAFAAQAQAAAQgOQAPgNACgTIABgPQAAgpgbgYQgWgVgoAAQgOAAgPAEgAhEgcQgTAQgRATIAPAAQgJAOABAQQABAHAHAFQAHAGALAAQAPAAAFgQQAEgLAAgcIAWAAQAAAeACAJQAEAQAQgBQAMgBAIgNQAIgLAAgQQAAgTgLgQQgMgPgPgCIgNgBQgbAAgPAMgAhghIQAJACACAJQACAJgEAJIANgNIAOgMQgDgJgOgBIgCAAQgNAAgEAGg");
	this.shape_28.setTransform(387.1,144.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgbAyIAAgqIgTAAIAAgSIATAAIAAgcIgTAAIAAgTIA2AAQARABAKAHQAMAKAAARQAAAQgOAHQgPAIgVgBIAAAxgAgDglIAAAbQAUAAAAgOQAAgNgQAAIgEAAg");
	this.shape_29.setTransform(368.1,129.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgvBhIAAgYQAHAGANAAQAMgBAKgXQAIgYAAgcQAAgcgIgYQgJgagMgBQgNgBgIAHIAAgZQALgJAVAAQAcAAATAlQAQAfAAAnQAAAmgRAeQgTAigdAAQgRAAgNgIg");
	this.shape_30.setTransform(367.5,144.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AhYBcQghgoABg4QABg3AigkQAjgkA5AAQAxAAAhAfQAgAhAAAvQAAAngVAdQgXAdglAAQglABgVgXQgWgXADgiQACgeAYgSIgcAAIAAgYIBeAAIAAAYIgRAAQgRAAgNALQgLALgBAQQAAARAMALQANAMATAAQAXgCAOgSQANgQgBgYQgBgegPgSQgWgagtAAQgnAAgWAgQgUAcAAAqQAAAmAUAdQAUAfAjAGIASACQAsgBAYgiIAdAAQgeBBhSAAQg3AAgkgog");
	this.shape_31.setTransform(350.4,146.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AhCBLQgegdAAgtQAAguAegdQAdgdAsgBQAwABAWATQAPAOABAUIgYAAQgBgNgTgJQgRgIgWABQgdgBgWAUQgXAUAAAdQAAAcAUATQAVASAdAAQAXAAASgNQATgNAAgXQAAgdgbgIQAKAKAAATQAAANgMALQgNAKgPgBQgSAAgNgLQgNgMAAgSQAAgVARgLQAPgMAWAAQAgABAYASQAWATAAAfQAAAngbAZQgbAagmAAQgqABgdgegAgEgYQgGAFAAAHQAAAHAFAFQAFAFAHAAQAHAAAFgFQAGgEAAgHQAAgHgFgGQgFgFgHAAQgIAAgEAFg");
	this.shape_32.setTransform(327.5,144.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AhkBrQgUgYABggQACgmAagQIgiAAIAAgYIArAAQgIgDAAgNQAAgGACgFQANgWAkAAIAQAAQAcADARAVQAPAUgBAdIhLAAQgUAAgOAMQgOAMgBATQAAARAMAMQAMAMAUAAQAOAAAKgJQALgJAAgPIAAgbIAXAAIAAAcQAAAOAJAJQAKAJAPAAQAqgBAEhHQADgtgcgjQgcgjgqgBQg/gBgXAoIgcAAQAfg/BRABQAxABAiAdQAYAVAMAhQAMAggCAiQgCAugRAdQgVAkglAAQggAAgQgTQgQATggAAIgBAAQgfAAgVgYgAgtgzQAFAIAAAGQgBAGgEAEIAzAAQgHgRgLgEQgKgEgMAAIgLABg");
	this.shape_33.setTransform(291.5,141.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgtBWQgTgQAAgdQAAgXAIgQQAGgLAOgQQAQgSAqghIhQAAIAAgYIB3AAIAAAXIgcAUQgNALgLALQgRATgFAJQgKAQABARQABANAIAGQAIAGAKAAQAWAAAHgTQACgFAAgFQAAgJgGgHIAYAAQAMARgBAWQgCAYgTAQQgTAQgZAAQgcAAgRgPg");
	this.shape_34.setTransform(272,144.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhWBuQgjgZACgtQACgnAfgOQgQgFAAgRQAAgWAWgKQASgJAXABQAcACARAXQASAVgBAdIhIAAQgQAAgLAIQgLAJABAQQACAlAwAKQAPADAVAAQAoAAAZgbQAYgZAAgmQABgqgYgcQgZgfgwgCQg1gBgcAoIgaAAQANgdAegRQAdgPAlAAQA2AAAjAmQAkAnAAA3QAAA3ggAmQgiAog2AAQg2ABgggYgAg5gwQAFAEACAGQADAJgEAIIAsAAQgCgMgJgIQgKgJgOAAQgIAAgHACg");
	this.shape_35.setTransform(252.6,141.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.btn1},{t:this.btn2},{t:this.btn3},{t:this.btn4}]}).wait(1));

	// bg
	this.instance = new lib.Bitmap1_1();
	this.instance.parent = this;
	this.instance.setTransform(1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(641,360,1280,812.8);
// library properties:
lib.properties = {
	width: 1280,
	height: 720,
	fps: 30,
	color: "#999999",
	opacity: 1.00,
	manifest: [
		{src:"images/civic_8.6_atlas_.png?1564964019433", id:"civic_8.6_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;