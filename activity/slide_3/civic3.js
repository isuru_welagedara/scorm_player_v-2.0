(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"civic3_atlas_", frames: [[0,0,2971,1821],[0,1823,1248,701],[1250,1823,1248,701],[2973,1635,909,543],[3884,483,159,159],[2500,1823,295,369],[2797,2180,299,197],[3884,161,159,159],[3098,2180,299,197],[2973,1090,909,543],[3884,322,159,159],[2973,545,909,543],[3884,0,159,159],[2973,0,909,543],[3399,2180,289,177]]}
];


// symbols:



(lib.Image = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Image_0 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Image_2 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1_1 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1copy = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap1copy2 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap2 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap2copy = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap3 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap3_1 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap4 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap4_1 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap5 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.Bitmap6 = function() {
	this.spriteSheet = ss["civic3_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#666666").s().p("EhHAAqbMAAAhU1MCOBAAAMAAABU1g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-454.5,-271.5,909,543), null);


(lib.playBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhLhWICWBWIiWBXg");
	this.shape.setTransform(2.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7CB342").s().p("AhtBtQgtgtAAhAQAAg/AtguQAugtA/AAQBAAAAuAtQAtAuAAA/QAABAgtAtQguAuhAAAQg/AAgugug");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.5,-15.5,31,31);


(lib.pauseBtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgSBSIAAijIAlAAIAACjg");
	this.shape.setTransform(3.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgSBSIAAijIAlAAIAACjg");
	this.shape_1.setTransform(-3.1,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7CB342").s().p("AhtBtQgtgtAAhAQAAg/AtguQAugtA/AAQBAAAAuAtQAtAuAAA/QAABAgtAtQguAuhAAAQg/AAgugug");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.5,-15.5,31,31);


(lib.next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhBhwICDBwIiDBx");
	this.shape.setTransform(3.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhBhwICDBwIiDBx");
	this.shape_1.setTransform(-3.3,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#00A9E4","#00D3D0"],[0,1],0,18.8,0,-18.7).s().p("AigC8QgcAAgSgUQgUgSAAgcIAAjzQAAgbAUgTQASgTAcAAIFCAAQAbAAATATQATATAAAbIAADzQAAAcgTASQgTAUgbAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.7,-18.7,45.4,37.5);


(lib.name5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D8936").s().p("AAOAmIAAgvQAAgHgDgEQgDgDgHAAQgJAAgFAJIAAA0IgRAAIAAhJIAQAAIABAIQAHgKANAAQAXAAABAbIAAAwg");
	this.shape.setTransform(41.9,10.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4D8936").s().p("AgIAzIAAhJIAQAAIAABJgAgGgjQgCgCgBgEQABgEACgDQACgCAEAAQAEAAADACQADADgBAEQABAEgDACQgDACgEABQgEgBgCgCg");
	this.shape_1.setTransform(36.2,9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4D8936").s().p("AgWAcQgKgKAAgQIAAgCQAAgKAFgJQAEgIAIgGQAHgEAJAAQAPgBAIALQAJAJAAASIAAAGIgwAAQABAIAFAGQAGAFAHAAQALAAAHgJIAJAJQgEAGgHAEQgIADgJAAQgPAAgKgKgAgJgTQgEAFgCAIIAfAAIAAgBQAAgJgEgDQgEgFgHAAQgGAAgEAFg");
	this.shape_2.setTransform(30.7,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4D8936").s().p("AgIAZIAAgoIgMAAIAAgNIAMAAIAAgSIAQAAIAAASIANAAIAAANIgNAAIAAAoQAAAEABACQACACAEAAIAGgBIAAANIgLACQgSAAAAgWg");
	this.shape_3.setTransform(24.4,9.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4D8936").s().p("AgPAjQgHgDgEgGQgEgGAAgGIARAAQAAAGAEADQAEADAGAAQAGAAAEgCQADgDAAgEQAAgEgDgDIgMgEQgIgCgGgDQgMgEAAgMQAAgKAIgGQAIgGALAAQAOAAAIAGQAIAHAAAKIgRAAQAAgEgDgDQgEgEgGAAQgEAAgDACQgEADAAAEQAAAEADADQADABAJACQAKADAGADQAFACADAEQADAEAAAGQAAAKgJAGQgIAHgNgBQgIABgIgEg");
	this.shape_4.setTransform(18.5,10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4D8936").s().p("AAOAmIAAgvQAAgHgDgEQgDgDgHAAQgJAAgFAJIAAA0IgQAAIAAhJIAPAAIABAIQAHgKANAAQAXAAABAbIAAAwg");
	this.shape_5.setTransform(11.1,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#4D8936").s().p("AgHAzIAAhJIAPAAIAABJgAgGgjQgDgCABgEQgBgEADgDQADgCADAAQAFAAACACQACADAAAEQAAAEgCACQgCACgFABQgDgBgDgCg");
	this.shape_6.setTransform(5.4,9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4D8936").s().p("AgfAyIAAhjIA/AAIAAAOIguAAIAAAcIAoAAIAAAMIgoAAIAAAfIAuAAIAAAOg");
	this.shape_7.setTransform(-0.1,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#4D8936").s().p("AgIAZIAAgoIgMAAIAAgNIAMAAIAAgSIAQAAIAAASIANAAIAAANIgNAAIAAAoQAAAEABACQACACAEAAIAGgBIAAANIgLACQgSAAAAgWg");
	this.shape_8.setTransform(-10.3,9.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4D8936").s().p("AgTAmIAAhJIARAAIAAAIQAFgKALAAIAFABIAAAQIgGgBQgMAAgDAJIAAAyg");
	this.shape_9.setTransform(-14.6,10.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#4D8936").s().p("AgWAcQgKgKAAgQIAAgCQAAgKAFgJQAEgIAIgGQAHgEAJAAQAPgBAIALQAJAJAAASIAAAGIgwAAQABAIAFAGQAGAFAHAAQALAAAHgJIAJAJQgEAGgHAEQgIADgJAAQgPAAgKgKgAgJgTQgEAFgCAIIAfAAIAAgBQAAgJgEgDQgEgFgHAAQgGAAgEAFg");
	this.shape_10.setTransform(-21.1,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4D8936").s().p("AgPAsIgBAIIgQAAIAAhoIARAAIAAAmQAIgJAKAAQAOAAAHAKQAJALgBARIAAABQAAARgHAKQgIAKgOAAQgMAAgGgJgAgPAAIAAAeQAFAKAKAAQAHAAAFgGQADgGABgMIAAgCQAAgMgEgFQgEgGgIAAQgKAAgFAJg");
	this.shape_11.setTransform(-28.7,8.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#4D8936").s().p("AgHA1IAAhpIAPAAIAABpg");
	this.shape_12.setTransform(-34.6,8.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#4D8936").s().p("AAbAyIgHgXIgmAAIgJAXIgSAAIAnhjIAOAAIAlBjgAgNANIAbAAIgOgpg");
	this.shape_13.setTransform(-41,9.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgKATIAAgQIgIAAIAAgHIAIAAIAAgLIgIAAIAAgHIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgFAEgJgBIAAAUgAgBgPIAAALQAIAAgBgFQABgGgHAAIgBAAg");
	this.shape_14.setTransform(65.1,-15.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAJAoIAAgGQgHAFgFABQgFACgKAAQgOgBgJgJQgKgJAAgOQgBgLAEgJIAKAAQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQAAABABAAQAAAAABAAQAAABABAAQABAAABAAQAEAAAEgDQAIgEACgGIAAgEQAAgHgEgCQgEgBgEABQAGACAAAIQAAAEgDADQgDACgEAAIgEAAQgIgDAAgJIABgGQAFgLAPAAQAGAAAEADQAFADABAEQAJgLARABQARABAKARQAGALAAANQgBAbgUAKQgGAEgIAAIgJgCgAgEgPQAIAAAGAFQAJAHAAANQAAANgIAIIAHABQAHAAAEgDQAKgIAAgQQAAgKgHgIQgHgIgLgBIgCAAQgLAAgFAHgAgoAGQAAAGADAFQAFAGAMAAIAHAAQAIgBAFgGQAEgFAAgHQAAgDgCgDQgCgEgHAAQgDAAgFADIgJAEQgEACgFAAQgFAAgCgCIAAAFg");
	this.shape_15.setTransform(61.4,-9.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAbATQAHgEAAgHIgBgCQgBgFgKgCQgKgCgMAAQgLAAgKADQgKABgBAFIgBACQAAAHAGAEIgJAAQgHgCgBgMQAAgEACgBQAFgLANgEQAIgDAQAAQAkAAAHASQABABAAAEQAAALgIADg");
	this.shape_16.setTransform(50.8,-15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAAAhQgGAJgMAAQgQAAgIgMQgHgLAAgTQABgTANgMQAJgJAMgBQAFAAAEABIAAAKQgFgDgIADQgJADgFAHQgFAIAAAKQAAAYARAAQAHAAAEgEQAEgDAAgHIAAgIIAKAAIAAAIQAAAHAEADQAEAEAGAAQAIAAAFgFQAFgFAAgIQAAgHgFgFQgFgEgIAAIgXAAQgBgPAGgHQAGgHAMAAQAWABAAARIAAAEIgIAAQAHADAFAIQAFAHAAAIQAAAPgIAKQgIAKgOAAQgOAAgGgJgAAHgVIAOAAIAGAAIAAgCQAAgIgKAAQgJAAgBAKg");
	this.shape_17.setTransform(50.8,-9.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgiAoQgNgMAAgVQABgOAKgKQAKgKAPAAIALAAIAAAKIgJAAQgLgBgHAIQgHAGAAAJQgBANALAIQALAIANgBQAPAAAKgLQAKgLAAgOQAAgQgKgMQgKgLgPgBQgLgBgJAGQgJAEgEAIIgJAAQAEgNALgGQAMgIAPAAQAVAAAOAQQANAQAAAVQAAAXgOAPQgNAQgWAAQgVgBgMgNg");
	this.shape_18.setTransform(40.7,-10.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgKATIAAgQIgHAAIAAgHIAHAAIAAgLIgIAAIAAgHIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGAEgIgBIAAAUgAgBgPIAAALQAHAAAAgFQAAgGgGAAIgBAAg");
	this.shape_19.setTransform(33.9,-15.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AACAgQgGAKgQAAQgMAAgIgJQgIgKAAgMQAAgNAJgGIgMAAIAAgKIAMAAQgCgCAAgEIAAgFQAFgMASAAQAPAAAFAKQAFgKAPAAQAIAAAGADQAGAFACAHQAAAHgDAEIgCAAQANAKAAAPQAAANgIAJQgJAKgMAAQgPAAgGgKgAAGACIAAAJQAAAFAGAEQAFABAFAAQAHAAAGgEQAEgGAAgHQAAgEgEgFQgFgEgHAAIgpAAQgHAAgGAEQgEAFAAAFQgBAIAFAEQAFAEAIAAQAGAAAEgBQAGgDgBgFIAAgKgAALgbQgEADAAAGIAWAAQACgCgBgEQAAgHgKAAQgFAAgEAEgAgYgeQADACAAAEQABADgCADIASAAQgBgHgFgEQgDgCgEAAQgEAAgDABg");
	this.shape_20.setTransform(30.4,-9.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgKATIAAgQIgHAAIAAgHIAHAAIAAgLIgIAAIAAgHIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGAEgHgBIAAAUgAAAgPIAAALQAGAAABgFQgBgGgGAAIAAAAg");
	this.shape_21.setTransform(23.4,-15.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AAJAoIAAgGQgHAFgFABQgFACgKAAQgOgBgJgJQgKgJAAgOQgBgLAEgJIAKAAQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQAAABABAAQAAAAABAAQAAABABAAQABAAABAAQAEAAAEgDQAIgEACgGIAAgEQAAgHgEgCQgEgBgEABQAGACAAAIQAAAEgDADQgDACgEAAIgEAAQgIgDAAgJIABgGQAFgLAPAAQAGAAAEADQAFADABAEQAJgLARABQARABAKARQAGALAAANQgBAbgUAKQgGAEgIAAIgJgCgAgEgPQAIAAAGAFQAJAHAAANQAAANgIAIIAHABQAHAAAEgDQAKgIAAgQQAAgKgHgIQgHgIgLgBIgCAAQgLAAgFAHgAgoAGQAAAGADAFQAFAGAMAAIAHAAQAIgBAFgGQAEgFAAgHQAAgDgCgDQgCgEgHAAQgDAAgFADIgJAEQgEACgFAAQgFAAgCgCIAAAFg");
	this.shape_22.setTransform(19.7,-9.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AAbATQAHgEAAgHIAAgCQgCgFgLgCQgJgCgMAAQgLAAgJADQgLABgCAFIAAACQAAAHAGAEIgIAAQgJgCAAgMQABgEABgBQAFgLAMgEQAJgDAQAAQAkAAAHASQABABAAAEQABALgJADg");
	this.shape_23.setTransform(9.1,-15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAAAhQgGAJgMAAQgQAAgIgMQgHgLAAgTQABgTANgMQAJgJAMgBQAFAAAEABIAAAKQgFgDgIADQgJADgFAHQgFAIAAAKQAAAYARAAQAHAAAEgEQAEgDAAgHIAAgIIAKAAIAAAIQAAAHAEADQAEAEAGAAQAIAAAFgFQAFgFAAgIQAAgHgFgFQgFgEgIAAIgXAAQgBgPAGgHQAGgHAMAAQAWABAAARIAAAEIgIAAQAHADAFAIQAFAHAAAIQAAAPgIAKQgIAKgOAAQgOAAgGgJgAAHgVIAOAAIAGAAIAAgCQAAgIgKAAQgJAAgBAKg");
	this.shape_24.setTransform(9.1,-9.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AAOAzIAAgTQgMACgJgBQgTgBgLgIQgNgJAAgQQAAgJAFgHQAEgGAHgDIgFgDIgBgFQAAgGAEgDQAIgHAPABQAWAAAGANQACgGAFgEQAEgDAGAAIAKAAIAAAJIgCgBQgBAAAAAAQgBAAAAABQgBAAAAABQAAABAAABIADADIAHAGQADAFABAFQAAAKgJAFQgIAEgJgBIAAAVIAIgCIAHgDIAAAKIgHACIgIACIAAAVgAgWgOQgGAGAAAIQABASAUAEIAKABIALAAIAAgsIgWAAQgJAAgFAHgAAZgZIAAARIACAAQADAAADgCQADgDAAgEQAAgEgEgGQgEgGAAgDIAAgBQgDABAAALgAgTgnQACACAAADQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAAAIACAAIACAAIAVAAQgBgGgHgCQgGgCgFAAIgGABg");
	this.shape_25.setTransform(-1.3,-8.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AghAtQgNgLABgVQAAgOAKgJQALgJAOAAIALAAIAAAKIgIAAQgMAAgHAFQgHAEAAAJQgBAOALAHQAKAHAOAAQAOAAAJgIQALgJAAgPQgBgPgIgIQgIgHgMAAIgwAAIAAgJQANAAAAgBIgCgCQgBAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAIgBgDQAAgNAjAAQAWAAALAJQAKAJgBAJIgLAAIAAgCQAAgFgHgEQgJgFgOAAQgPAAgEAEIgBADQAAAEAHAAIAWAAQALAAAJAGQARAKAAAYQAAAXgNAOQgMAOgWAAQgVAAgMgMg");
	this.shape_26.setTransform(-16.5,-11.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgBA4QgSgBgLgLQgLgLAAgRQAAgLAEgIQAEgIAKgJIASgRIAIgMIARAAIgIAMQgFAFgIAGQAMgBAJADQAZAJAAAdQAAATgOAMQgMAMgSAAIgCgBgAgVgIQgIAIAAAKQAAAKAHAHQAJAJAOAAQALAAAJgGQAKgIAAgNQAAgGgCgFQgDgHgIgFQgHgDgJAAQgPAAgIAJgAgkgdIgHAAIAAgIIAHAAIAAgLIgIAAIAAgIIAWAAQAHAAAEAEQAFADAAAHQAAAHgGADQgFADgJAAIAAAMQgGAFgEAGgAgagwIAAALQAIAAAAgFQAAgGgGAAIgCAAg");
	this.shape_27.setTransform(-25.9,-11.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AACAzQgGgCgEgGQgBAEgFADQgEADgHABQgMAAgHgNQgGgLAAgOQAAgSAKgNQALgNARAAQAMAAAIAIQAIAHAAANQAAAJgHAGIgEAEQgCACAAADQgBAKANAAQAKAAAHgLQAFgKAAgNQAAgQgKgNQgLgNgUgBQgKAAgIAFQgJAEgFAIIgLAAQAFgNAMgHQAMgGAOgBQAYABAOAOQAHAGAFAMQAEALABAJQAAAagKAOQgKAOgPAAQgJgBgFgCgAgdAgQAAAAAAABQgBAAAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABAAABQAAAAABABQAAAAAAAAQABABAAAAQABABAAAAQABAAABAAQAAABABAAQAAAAABgBQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQAAgBABAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAgBgBAAQgBAAAAAAQgBAAAAAAQgBAAgBABQAAAAgBAAQAAABgBAAgAgkACQgCAHAAAIQAAAFABAGQAFgIAJgBQAHAAAHAFQAAgGAFgEQAFgFAAgHQAAgFgDgEQgFgDgFAAQgRAAgHAMg");
	this.shape_28.setTransform(-35.6,-10.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgKATIAAgQIgIAAIAAgHIAIAAIAAgLIgIAAIAAgHIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgFAEgIgBIAAAUgAAAgPIAAALQAGAAABgFQgBgGgGAAIAAAAg");
	this.shape_29.setTransform(-42.7,-15.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgiAkQgOgPABgWQAAgWAOgOQAOgPAWAAQATAAAOANQANAMAAAUQAAAPgJALQgJALgPABQgPABgIgKQgIgKABgMQABgNAJgGIgLAAIAAgKIAlAAIAAAKIgHAAQgHgBgFAFQgEAEAAAHQAAAGAEAFQAFAEAIAAQAJgBAGgHQAFgGAAgKQgBgLgGgIQgIgKgTAAQgPAAgIANQgIAMAAAQQAAAOAIAMQAIANANACIAHAAQASAAAJgNIAMAAQgMAaghAAQgVAAgOgRg");
	this.shape_30.setTransform(-45.9,-8.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgSAiQgHgGAAgMQAAgIADgHQACgEAGgHQAHgGAQgOIggAAIAAgJIAvAAIAAAJIgLAIIgJAJIgJALQgEAFABAIQAAAFADACQADADAEAAQAJAAACgIIABgFQAAgDgCgCIAKAAQAEAGAAAJQgBAKgIAGQgHAGgKABQgLAAgHgHg");
	this.shape_31.setTransform(-53.7,-9.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AAOAzIAAgTQgMACgJgBQgTgBgLgIQgNgJAAgQQAAgJAFgHQAEgGAHgDIgFgDIgBgFQAAgGAEgDQAIgHAPABQAWAAAGANQACgGAFgEQAEgDAGAAIAKAAIAAAJIgCgBQgBAAAAAAQgBAAAAABQgBAAAAABQAAABAAABIADADIAHAGQADAFABAFQAAAKgJAFQgIAEgJgBIAAAVIAIgCIAHgDIAAAKIgHACIgIACIAAAVgAgWgOQgGAGAAAIQABASAUAEIAKABIALAAIAAgsIgWAAQgJAAgFAHgAAZgZIAAARIACAAQADAAADgCQADgDAAgEQAAgEgEgGQgEgGAAgDIAAgBQgDABAAALgAgTgnQACACAAADQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAAAIACAAIACAAIAVAAQgBgGgHgCQgGgCgFAAIgGABg");
	this.shape_32.setTransform(-61.8,-8.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.name5, new cjs.Rectangle(-69,-21.2,138.1,40.9), null);


(lib.name4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#558B2F").s().p("AgYAgQgHgHAAgIQAAgNAJgFQAJgGAPAAIAKAAIAAgFQAAgGgDgDQgDgDgGAAQgFgBgEADQgDADAAAEIgRAAQAAgFAEgGQAEgFAHgDQAHgDAHgBQANAAAIAHQAIAGAAAMIAAAhQAAAKADAGIAAABIgRAAIgCgHQgIAIgKABQgMgBgHgGgAgJAFQgFAEAAAFQAAAFADADQADADAGAAQAEAAAEgDQAEgCACgEIAAgOIgIAAQgJAAgEADg");
	this.shape.setTransform(22.8,11.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#558B2F").s().p("AAiAyIAAghIACgrIgdBMIgMAAIgdhMIABArIAAAhIgRAAIAAhjIAXAAIAcBMIAchMIAWAAIAABjg");
	this.shape_1.setTransform(12.9,10.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#558B2F").s().p("AANA0IgUgfIgIAIIAAAXIgRAAIAAhnIARAAIAAA7IAFgGIAUgXIAUAAIgbAfIAdAqg");
	this.shape_2.setTransform(0.1,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#558B2F").s().p("AgWAdQgJgLAAgRIAAgBQAAgRAJgKQAJgLAPAAQANABAIAHQAJAIAAANIgQAAQAAgHgEgEQgEgEgGAAQgHAAgFAGQgEAGAAAMIAAABQAAANAEAFQAEAGAIAAQAGAAAEgEQAEgDAAgGIAQAAQAAAIgEAGQgEAGgHADQgHADgIABQgPAAgJgKg");
	this.shape_3.setTransform(-7.6,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#558B2F").s().p("AgYAgQgHgHAAgIQAAgNAJgFQAJgGAPAAIAKAAIAAgFQAAgGgDgDQgDgDgGAAQgFgBgEADQgDADAAAEIgRAAQAAgFAEgGQAEgFAHgDQAHgDAHgBQANAAAIAHQAIAGAAAMIAAAhQAAAKADAGIAAABIgRAAIgCgHQgIAIgKABQgMgBgHgGgAgJAFQgFAEAAAFQAAAFADADQADADAGAAQAEAAAEgDQAEgCACgEIAAgOIgIAAQgJAAgEADg");
	this.shape_4.setTransform(-15.1,11.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#558B2F").s().p("AgXAqQgJgHAAgOIASAAQAAAHAEAEQADAEAHAAQAHABAEgFQAEgEABgJIAAhEIAQAAIAABFQAAAOgJAIQgIAIgPABQgPAAgIgJg");
	this.shape_5.setTransform(-23.1,10.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_6.setTransform(21,-9.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgWAxQgKgFgGgJQgJgMAAgTQAAgMAHgKQAHgLAKABQAJAAADAHQAFgIAJAAQAJAAAHAHQAGAGABAKQABAKgJAIQgIAJgLAAQgKAAgHgHQgHgGAAgJIACgHIACgFQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBAAAAAAQgFABgDAGQgCAGAAAGQgBAOALAJQALAJAOAAQAOAAAKgKQALgLAAgPQgBgSgIgLQgKgLgRAAQgXAAgJAQIgKAAQAFgNALgHQALgGAPAAQAVAAANANQAOAOABAYQABAXgNAQQgOARgWAAQgMAAgKgFgAgNgEQAAAEAEADQAEADAFABQAGAAAFgEQAFgDAAgGIgBgDQgBAEgEACQgEADgEAAQgLAAgCgLIgCAHgAgCgPQAAAAAAABQAAABABAAQAAAAAAABQABAAAAABQAAAAAAABQAAAAABAAQAAAAABABQABAAAAAAQABAAAAAAQABAAABgBQAAAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgFAAAAAGg");
	this.shape_7.setTransform(14,-10.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgHAAIAAAUgAAAgOIAAAKQAGAAAAgFQAAgGgGAAIAAABg");
	this.shape_8.setTransform(2,-15.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgOAkQgGAGgNAAQgHAAgGgFQgFgFAAgHQgBgJAEgFIgGAAIAAgHIAFgFIAIgIIgDAAQgEAAgDgEQgCgDAAgEQAAgIAFgGQAGgGAIAAQAOgCAGAKQANgJARABQAQABAMAMQALALAAAQQABAbgVAMQgEADgJAAIgIgBIAAgGQgGAHgIAAIAAAAQgIAAgGgGgAgGgYQAMABAIAJQAKALAAANQAAAOgHAHIAEABQAGAAAGgFQAGgGABgHIABgGQAAgQgLgKQgJgIgQAAQgFAAgGACgAgagKQgIAGgHAHIAGAAQgEAGABAGQAAADADACQADACAEAAQAGAAACgGQACgFAAgLIAJAAQAAAMABAEQABAGAGAAQAFAAADgGQADgEAAgHQAAgHgFgGQgEgGgGgBIgFAAQgLAAgFAFgAgmgcQAEABABAEQAAADgBAEIAFgFIAFgFQgBgEgFAAIgCAAQgEAAgCACg");
	this.shape_9.setTransform(-1.8,-9.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgSAjQgHgHAAgLQAAgKADgGQACgEAGgGQAHgIAQgNIggAAIAAgJIAvAAIAAAJIgLAIIgJAIIgJAMQgEAGABAGQAAAFADADQADACAEAAQAJABACgIIABgFQAAgCgCgEIAKAAQAEAIAAAIQgBAJgIAHQgHAHgKAAQgLgBgHgFg");
	this.shape_10.setTransform(-10.3,-9.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AghAsQgQgLABgRQABgOALgGQgHgBAAgIQAAgIAIgEQAHgDAJAAQANAAAFAIQADgHAMgBIAHAAIAFgEIAEgFQAFgIAAgJIAOAAIgDAJQgCAGgDAEIgIAJQAHAEABAIQABAEgCAEIgIAAQAEABAEAEQAHAGAAAMQAAARgPALQgOAKgSAAQgTAAgOgLgAgfAEQgEAEAAAGQABAKALAFQAKAFANAAQANAAAKgEQALgGABgKQAAgGgEgEQgEgEgJAAIgmAAQgGAAgFAEgAAGgPQgDACAAAEIAQAAIAIABQACgCgCgDQgCgFgKAAIgDAAQgEABgCACgAgZgRQAAAAABAAQAAABABAAQAAABABAAQAAABAAAAQABADgCACIASAAQgCgGgFgCIgHgCIgGACg");
	this.shape_11.setTransform(-18,-10.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.name4, new cjs.Rectangle(-28.7,-21.1,57.4,42.2), null);


(lib.name3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#558B2F").s().p("AgQAxQgIgDgEgGIAIgKQAIAJALAAQAHAAAFgEQAFgFAAgIIAAgFQgHAIgLAAQgNAAgIgLQgJgKAAgRQAAgSAIgKQAJgKANAAQAMAAAHAJIABgIIAPAAIAABHQAAAPgJAIQgKAIgOAAQgIAAgIgDgAgLgfQgEAHAAAMQAAALAEAGQAFAGAHAAQAKAAAFgJIAAgfQgFgIgKAAQgHAAgFAGg");
	this.shape.setTransform(34.9,13.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#558B2F").s().p("AAOAmIAAgvQAAgHgDgEQgDgDgHAAQgJAAgFAJIAAA0IgQAAIAAhJIAPAAIABAIQAHgKANAAQAXAAABAbIAAAwg");
	this.shape_1.setTransform(27.2,11.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#558B2F").s().p("AgHAzIAAhJIAPAAIAABJgAgGgjQgDgDABgDQgBgEADgCQADgDADAAQAFAAACADQACACABAEQgBADgCADQgCACgFAAQgDAAgDgCg");
	this.shape_2.setTransform(21.5,10.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#558B2F").s().p("AgHA0IAAhnIAPAAIAABng");
	this.shape_3.setTransform(17.8,10.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#558B2F").s().p("AAQAlIgQgyIgPAyIgNAAIgUhJIAPAAIANAxIAOgxIAMAAIAPAyIAMgyIARAAIgVBJg");
	this.shape_4.setTransform(10.9,11.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#558B2F").s().p("AgYAcQgJgKAAgSIAAAAQAAgKAEgJQAEgJAIgEQAIgFAJgBQAPAAAJALQAKAJAAAQIAAADQAAAKgEAJQgEAJgIAEQgIAGgKAAQgPgBgJgKgAgMgRQgFAGAAAMQAAALAFAHQAFAGAHAAQAJAAAEgGQAFgHAAgMQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_5.setTransform(1.7,11.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#558B2F").s().p("AATAyIgTgnIgTAAIAAAnIgSAAIAAhjIAjAAQARAAAJAIQAJAIAAAOQABAKgFAHQgFAGgJADIAXAqIAAABgAgTgBIARAAQAIAAAGgFQAEgEAAgIQABgIgFgEQgFgFgIAAIgSAAg");
	this.shape_6.setTransform(-6.3,10.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#558B2F").s().p("AgGAGQgDgCAAgEQAAgDADgDQADgCADAAQAFAAACACQADADAAADQAAAEgDACQgCADgFAAQgDAAgDgDg");
	this.shape_7.setTransform(-16.6,14.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#558B2F").s().p("AATAyIgdgrIgLAMIAAAfIgRAAIAAhjIARAAIAAAvIAKgMIAbgjIAVAAIglAsIAoA3g");
	this.shape_8.setTransform(-22.3,10.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#558B2F").s().p("AgGAGQgDgCAAgEQAAgDADgDQADgCADAAQAFAAACACQADADAAADQAAAEgDACQgCADgFAAQgDAAgDgDg");
	this.shape_9.setTransform(-29.3,14.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#558B2F").s().p("AgXAqQgJgHABgOIARAAQAAAHAEAEQAEAEAGAAQAHABAEgFQAFgEAAgJIAAhEIARAAIAABFQgBAOgIAIQgKAIgOABQgOAAgJgJg");
	this.shape_10.setTransform(-35.4,10.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgKAUIAAgRIgIAAIAAgHIAIAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgFADgJAAIAAAUgAgBgOIAAAKQAIAAgBgFQABgGgHAAIgBABg");
	this.shape_11.setTransform(45.4,-15.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAJAoIAAgGQgHAFgFABQgFACgKAAQgOgBgJgJQgKgJAAgOQgBgLAEgJIAKAAQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQAAABABAAQAAAAABAAQAAABABAAQABAAABAAQAEAAAEgDQAIgEACgGIAAgEQAAgHgEgCQgEgBgEABQAGACAAAIQAAAEgDADQgDACgEAAIgEAAQgIgDAAgJIABgGQAFgLAPAAQAGAAAEADQAFADABAEQAJgLARABQARABAKARQAGALAAANQgBAbgUAKQgGAEgIAAIgJgCgAgEgPQAIAAAGAFQAJAHAAANQAAANgIAIIAHABQAHAAAEgDQAKgIAAgQQAAgKgHgIQgHgIgLgBIgCAAQgLAAgFAHgAgoAGQAAAGADAFQAFAGAMAAIAHAAQAIgBAFgGQAEgFAAgHQAAgDgCgDQgCgEgHAAQgDAAgFADIgJAEQgEACgFAAQgFAAgCgCIAAAFg");
	this.shape_12.setTransform(41.7,-9.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAbATQAHgEAAgHIgBgCQgBgFgKgCQgKgCgMAAQgLAAgKADQgKABgBAFIgBACQAAAHAGAEIgJAAQgHgCAAgMQAAgEABgBQAFgLANgEQAIgDAQAAQAkAAAHASQABABAAAEQAAALgHADg");
	this.shape_13.setTransform(31.1,-14.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgiAlQgOgRABgVQAAgWAOgPQAOgOAWAAQATAAAOANQANANAAATQAAAPgJALQgJAMgPAAQgPAAgIgJQgIgKABgMQABgMAJgIIgLAAIAAgJIAlAAIAAAJIgHAAQgHAAgFAFQgEAEAAAHQAAAGAEAEQAFAFAIAAQAJgBAGgHQAFgFAAgLQgBgLgGgIQgIgKgTAAQgPAAgIANQgIAMAAAQQAAAOAIAMQAIAMANADIAHAAQASABAJgOIAMAAQgMAaghAAQgVAAgOgQg");
	this.shape_14.setTransform(31.2,-8.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgHAAIAAAUgAAAgOIAAAKQAGAAABgFQgBgGgGAAIAAABg");
	this.shape_15.setTransform(24.3,-15.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_16.setTransform(24,-9.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgCA1QgSgBgLgLQgLgLAAgRQgBgLAFgIQAEgIAKgJIASgRQAGgHACgFIARAAQgEAGgFAGQgEAFgIAGQAMgBAJADQAZAJAAAdQAAATgOAMQgMALgSAAIgCAAgAgXgLQgHAIAAAKQAAAKAHAHQAJAJAOAAQALAAAJgGQAKgIAAgNQAAgGgCgFQgDgHgIgFQgHgDgJAAQgPAAgJAJg");
	this.shape_17.setTransform(17.7,-10.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAIAIQAHAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAIAHALAAQAKAAAGgFQAJgFgBgJQABgLgLgDQADAEAAAIQAAAEgEAEQgGAEgGAAQgGAAgGgEQgEgFgBgHQAAgIAHgFQAGgEAJAAQANAAAIAHQAKAIAAALQAAAQgLAKQgLALgPAAQgQAAgMgMgAgBgJQgCACAAADQAAADACABQABACACAAQAEAAACgCQACgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_18.setTransform(9.1,-9.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgGAJQgFgDAAgGQAAgDACgDQAEgFAFAAQAEAAADADQAFADAAAFQAAAEgCADQgEAFgGAAQgDAAgDgDg");
	this.shape_19.setTransform(-2.2,-6.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgKAUIAAgRIgIAAIAAgHIAIAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgFADgIAAIAAAUgAAAgOIAAAKQAGAAABgFQgBgGgGAAIAAABg");
	this.shape_20.setTransform(-6.7,-15.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgOAkQgGAGgNAAQgHAAgGgFQgFgFAAgHQgBgJAEgFIgGAAIAAgHIAFgFIAIgIIgDAAQgEAAgDgEQgCgDAAgEQAAgIAFgGQAGgGAIAAQAOgCAGAKQANgJARABQAQABAMAMQALALAAAQQABAbgVAMQgEADgJAAIgIgBIAAgGQgGAHgIAAIAAAAQgIAAgGgGgAgGgYQAMABAIAJQAKALAAANQAAAOgHAHIAEABQAGAAAGgFQAGgGABgHIABgGQAAgQgLgKQgJgIgQAAQgFAAgGACgAgagKQgIAGgHAHIAGAAQgEAGABAGQAAADADACQADACAEAAQAGAAACgGQACgFAAgLIAJAAQAAAMABAEQABAGAGAAQAFAAADgGQADgEAAgHQAAgHgFgGQgEgGgGgBIgFAAQgLAAgFAFgAgmgcQAEABABAEQAAADgBAEIAFgFIAFgFQgBgEgFAAIgCAAQgEAAgCACg");
	this.shape_21.setTransform(-10.5,-9.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAIAIQAHAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAIAHALAAQAJAAAHgFQAJgFAAgJQgBgLgKgDQADAEAAAIQAAAEgEAEQgGAEgGAAQgGAAgFgEQgFgFgBgHQAAgIAHgFQAGgEAJAAQANAAAIAHQAKAIAAALQAAAQgLAKQgKALgQAAQgQAAgMgMgAgBgJQgDACAAADQABADACABQABACACAAQADAAADgCQACgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_22.setTransform(-20.3,-9.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgHAJQgEgDAAgGQAAgDACgDQAEgFAFAAQAEAAADADQAFADAAAFQAAAEgDADQgDAFgGAAQgDAAgEgDg");
	this.shape_23.setTransform(-26.5,-6.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AghAyQgQgLABgRQABgOALgHQgHgBAAgHQAAgIAIgEQAHgDAJAAQAMAAAGAIIADgEIAAgQIgHAAIAAgIIAHAAIAAgKIgHAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGADQgGAEgJgBIAAANIADgBIAHAAQAGgFAEgHQAEgIABgJIANAAQgBAKgFAJQgFAJgFADQAHADACAJQABAIgDAFQAHAHAAALQAAARgPALQgOAKgSAAQgTAAgOgLgAgfAKQgEAEAAAGQABAKALAFQAKAFANAAQANAAAKgEQALgGABgKQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAAAgBQgCAFgFACQgFADgFAAQgDAAgDgBQgHgEAAgJIABgGIgZAAQgGAAgFAFgAAOAKQAAAFAFAAQAFAAAEgHQgDgDgKAAIgBAFgAAUgJQgCADgBADQALAAAEADQAAAAAAgBQAAAAABgBQAAAAAAgBQAAgBAAAAQAAgIgHAAQgDAAgDADgAAGgJQgDADAAADIAEAAIACgEIADgFQgEAAgCADgAgZgLQAAAAABAAQAAABABAAQAAABABAAQAAABAAAAQABADgCACIAQAAQgBgGgFgDIgGgBIgGACgAALg0IAAAKQAIAAAAgFQAAgFgHAAIgBAAg");
	this.shape_24.setTransform(-34,-11.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAIAIQAHAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAIAHALAAQAKAAAGgFQAJgFgBgJQABgLgLgDQADAEAAAIQAAAEgEAEQgGAEgGAAQgGAAgGgEQgEgFgBgHQAAgIAHgFQAGgEAJAAQANAAAIAHQAKAIAAALQAAAQgLAKQgLALgPAAQgQAAgMgMgAgBgJQgCACAAADQAAADACABQABACACAAQAEAAACgCQACgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_25.setTransform(-43.1,-9.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.name3, new cjs.Rectangle(-49.3,-21.1,98.7,42.2), null);


(lib.name2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#558B2F").s().p("AgaAyIAAgMIADAAQAGAAADgDQAEgCACgFIACgGIgahJIARAAIAPAyIAQgyIARAAIgeBUQgFATgQAAIgIgCg");
	this.shape.setTransform(33.7,13.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#558B2F").s().p("AgWAdQgKgKAAgRIAAgCQAAgKAFgJQAEgJAIgEQAHgFAJgBQAPAAAIALQAJAJAAASIAAAFIgwAAQABAKAFAFQAGAFAHAAQALAAAHgJIAJAIQgEAHgHAEQgIAEgJAAQgPAAgKgKgAgJgTQgEAEgCAJIAfAAIAAgBQAAgIgEgFQgEgEgHAAQgGAAgEAFg");
	this.shape_1.setTransform(26.6,11.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#558B2F").s().p("AAOAmIAAgvQAAgHgDgEQgDgDgHAAQgJAAgFAJIAAA0IgRAAIAAhJIAQAAIABAIQAHgKANAAQAXAAABAbIAAAwg");
	this.shape_2.setTransform(18.9,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#558B2F").s().p("AgPAjQgHgDgEgGQgEgFAAgIIARAAQAAAHAEADQAEADAGAAQAGAAAEgDQADgCAAgEQAAgEgDgDIgMgEQgIgBgGgDQgMgGAAgLQAAgJAIgHQAIgHALAAQAOAAAIAHQAIAHAAAKIgRAAQAAgFgDgDQgEgDgGAAQgEAAgDACQgEADAAAFQAAADADADQADACAJABQAKADAGADQAFACADAEQADAEAAAGQAAAKgJAGQgIAGgNABQgIgBgIgDg");
	this.shape_3.setTransform(11.3,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#558B2F").s().p("AgHAzIAAhJIAPAAIAABJgAgGgjQgCgDAAgDQAAgEACgCQADgDADAAQAEAAADADQADACAAAEQAAADgDADQgDACgEAAQgDAAgDgCg");
	this.shape_4.setTransform(6,10.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#558B2F").s().p("AgkAyIAAhjIAdAAQAMAAALAGQAKAGAGALQAFALAAAOIAAAEQAAAOgFAKQgGALgLAGQgKAGgMAAgAgTAkIALAAQANAAAHgJQAIgIAAgQIAAgFQAAgQgHgIQgHgJgNAAIgMAAg");
	this.shape_5.setTransform(-0.3,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#558B2F").s().p("AgIAZIAAgoIgMAAIAAgNIAMAAIAAgSIAQAAIAAASIANAAIAAANIgNAAIAAAoQAAAEABACQACACAEAAIAGgBIAAANIgLACQgSAAAAgWg");
	this.shape_6.setTransform(-11,11.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#558B2F").s().p("AgHA0IAAhnIAPAAIAABng");
	this.shape_7.setTransform(-15,10.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#558B2F").s().p("AgYAgQgHgHAAgIQAAgNAJgFQAJgGAPAAIAKAAIAAgFQAAgGgDgDQgDgDgGAAQgFgBgEADQgDADAAAEIgRAAQAAgFAEgGQAEgFAHgDQAHgDAHgBQANAAAIAHQAIAGAAAMIAAAhQAAAKADAGIAAABIgRAAIgCgHQgIAIgKABQgMgBgHgGgAgJAFQgFAEAAAFQAAAFADADQADADAGAAQAEAAAEgDQAEgCACgEIAAgOIgIAAQgJAAgEADg");
	this.shape_8.setTransform(-20.5,11.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#558B2F").s().p("AATAyIgThIIgSBIIgQAAIgYhjIARAAIAQBKIAThKIAOAAIASBKIAPhKIARAAIgWBjg");
	this.shape_9.setTransform(-30.4,10.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAbATQAHgEAAgHIgBgCQgBgFgKgCQgJgCgNAAQgLAAgJADQgLABgBAFIgBACQAAAHAGAEIgJAAQgHgCgBgMQAAgEACgBQAFgLAMgEQAKgDAPAAQAkAAAHASQACABgBAEQABALgJADg");
	this.shape_10.setTransform(34,-14.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAJAoIAAgGQgHAFgFABQgFACgKAAQgOgBgJgJQgKgJAAgOQgBgLAEgJIAKAAQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQAAABABAAQAAAAABAAQAAABABAAQABAAABAAQAEAAAEgDQAIgEACgGIAAgEQAAgHgEgCQgEgBgEABQAGACAAAIQAAAEgDADQgDACgEAAIgEAAQgIgDAAgJIABgGQAFgLAPAAQAGAAAEADQAFADABAEQAJgLARABQARABAKARQAGALAAANQgBAbgUAKQgGAEgIAAIgJgCgAgEgPQAIAAAGAFQAJAHAAANQAAANgIAIIAHABQAHAAAEgDQAKgIAAgQQAAgKgHgIQgHgIgLgBIgCAAQgLAAgFAHgAgoAGQAAAGADAFQAFAGAMAAIAHAAQAIgBAFgGQAEgFAAgHQAAgDgCgDQgCgEgHAAQgDAAgFADIgJAEQgEACgFAAQgFAAgCgCIAAAFg");
	this.shape_11.setTransform(33.7,-9.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgHAAIAAAUgAAAgOIAAAKQAGAAABgFQAAgGgHAAIAAABg");
	this.shape_12.setTransform(26.4,-15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AABAgQgFAKgQAAQgMAAgIgJQgIgKAAgMQAAgNAJgHIgMAAIAAgKIANAAQgDgBAAgEIAAgFQAFgMASAAQAPAAAEAKQAHgKAOAAQAJAAAFADQAHAFABAHQAAAHgCAEIgDAAQANAJAAAPQAAAOgIAJQgJAKgMAAQgOAAgIgKgAAHADIAAAJQgBAEAGAEQAEACAGAAQAHAAAGgGQAEgFAAgGQAAgFgEgFQgFgEgGAAIgqAAQgIAAgFAFQgEAEAAAFQAAAHAEAFQAFAEAIABQAFAAAFgCQAGgDAAgFIAAgJgAALgbQgEAEAAAEIAWAAQACgBgBgDQAAgIgKAAQgGAAgDAEgAgYgeQADADABADQAAADgCACIASAAQAAgGgGgEQgEgCgDAAQgEAAgDABg");
	this.shape_13.setTransform(22.9,-9.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAAAvQgGAKgOAAQgLAAgIgJQgHgJAAgMQABgMAIgHIgLAAIAAgIIAQAAQgDgCAAgFIABgFQAEgJATAAQALAAAGAHQAHAIAAAKIAAAEIggAAQgGAAgEAEQgEAEAAAGQAAAGAEAEQAFAFAGAAQAFAAAEgDQAFgDAAgFIAAgLIAJAAIAAALQAAAFAFADQAEADAGAAQAJAAAFgKQAEgHAAgLQAAgPgJgJQgJgKgMAAIgkAAQgNAAAAgLQAAgUApABQAWAAALAKQAIAHgBAKIgJAAQAAgGgGgFQgJgHgQAAQgZAAAAAIQAAAEAJAAIAaAAQAPAAALAMQANANAAAXQAAARgHAMQgIAOgPAAQgNAAgGgKgAgRgLQADABAAAEQABADgCADIAQAAQAAgFgDgDQgDgEgGAAIgGABg");
	this.shape_14.setTransform(12.7,-11.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AghAtQgNgLABgVQAAgOAKgJQALgJAOAAIALAAIAAAKIgIAAQgMAAgHAFQgHAEAAAJQgBAOALAHQAKAHAOAAQAOAAAJgIQALgJAAgPQgBgPgIgIQgIgHgMAAIgwAAIAAgJQANAAAAgBIgCgCQgBgBgBAAQAAAAAAgBQgBAAAAAAQAAgBAAAAIgBgDQAAgNAjAAQAWAAALAJQAKAJgBAJIgLAAIAAgCQAAgFgHgEQgJgFgOAAQgPAAgEAEIgBADQAAAEAHAAIAWAAQALAAAJAGQARAKAAAYQAAAXgNAOQgMAOgWAAQgVAAgMgMg");
	this.shape_15.setTransform(-2.2,-11.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgIAAIAAAUgAgBgOIAAAKQAHAAAAgFQAAgGgGAAIgBABg");
	this.shape_16.setTransform(-8.9,-15.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgiAlQgOgRABgVQAAgWAOgPQAOgOAWAAQATAAAOANQANANAAATQAAAPgJALQgJAMgPAAQgPAAgIgJQgIgKABgMQABgMAJgIIgLAAIAAgJIAlAAIAAAJIgHAAQgHAAgFAFQgEAEAAAHQAAAGAEAEQAFAFAIAAQAJgBAGgHQAFgFAAgLQgBgLgGgIQgIgKgTAAQgPAAgIANQgIAMAAAQQAAAOAIAMQAIAMANADIAHAAQASABAJgOIAMAAQgMAaghAAQgVAAgOgQg");
	this.shape_17.setTransform(-12.1,-8.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgHAAIAAAUgAAAgOIAAAKQAGAAABgFQAAgGgHAAIAAABg");
	this.shape_18.setTransform(-19,-15.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_19.setTransform(-19.3,-9.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgiAsQgOgKABgTQABgPAMgFQgGgBAAgIQAAgJAJgDQAHgEAJABQALABAHAIQAHAJgBALIgcAAQgGAAgFADQgEAEAAAGQABAPATAEQAGACAIgBQAQAAAKgLQAKgKAAgOQAAgRgJgMQgKgMgTAAQgVAAgLAPIgLAAQAFgMAMgGQAMgHAPABQAVAAAOAPQAOAPAAAWQAAAWgNAPQgNAQgWABQgVAAgNgKgAgWgTIADAFQABADgCADIASAAQgBgFgDgDQgEgEgGAAIgGABg");
	this.shape_20.setTransform(-26.1,-10.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAJAIQAGAGABAHIgLAAQAAgFgHgDQgIgDgIAAQgLAAgJAHQgJAJAAALQAAALAIAHQAJAHALAAQAIAAAHgFQAIgFAAgJQAAgLgLgDQAEAEABAIQgBAEgFAEQgEAEgGAAQgHAAgGgEQgEgFAAgHQAAgIAGgFQAGgEAIAAQANAAAKAHQAJAIAAALQAAAQgLAKQgLALgPAAQgQAAgMgMgAgBgJQgDACABADQAAADABABQACACADAAQACAAACgCQADgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_21.setTransform(-35.2,-9.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.name2, new cjs.Rectangle(-41.3,-21.1,82.7,42.2), null);


(lib.name1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#558B2F").s().p("AgYAgQgHgHAAgIQAAgNAJgFQAJgGAPAAIAKAAIAAgFQAAgGgDgDQgDgDgGAAQgFgBgEADQgDADAAAEIgRAAQAAgFAEgGQAEgFAHgDQAHgDAHgBQANAAAIAHQAIAGAAAMIAAAhQAAAKADAGIAAABIgRAAIgCgHQgIAIgKABQgMgBgHgGgAgJAFQgFAEAAAFQAAAFADADQADADAGAAQAEAAAEgDQAEgCACgEIAAgOIgIAAQgJAAgEADg");
	this.shape.setTransform(44.5,11.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#558B2F").s().p("AgXArQgIgLAAgSQAAgQAIgKQAIgLANAAQALAAAHAJIAAgmIAQAAIAABoIgPAAIAAgIQgIAJgLAAQgNAAgIgKgAgKgDQgFAGAAANQAAALAFAGQAEAHAHAAQAKAAAFgKIAAgeQgFgJgKAAQgHAAgEAGg");
	this.shape_1.setTransform(36.6,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#558B2F").s().p("AAOAmIAAgvQAAgHgDgEQgDgDgHAAQgJAAgFAJIAAA0IgRAAIAAhJIAQAAIAAAIQAJgKAMAAQAXAAABAbIAAAwg");
	this.shape_2.setTransform(28.9,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#558B2F").s().p("AgYAcQgJgKAAgSIAAAAQAAgKAEgJQAEgJAIgEQAIgFAJgBQAPAAAJALQAKAJAAAQIAAADQAAAKgEAJQgEAJgIAEQgIAGgKAAQgPgBgJgKgAgMgRQgFAGAAAMQAAALAFAHQAFAGAHAAQAJAAAEgGQAFgHAAgMQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_3.setTransform(21,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#558B2F").s().p("AAXAyIAAgsIgsAAIAAAsIgSAAIAAhjIASAAIAAAqIAsAAIAAgqIARAAIAABjg");
	this.shape_4.setTransform(12.1,10.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#558B2F").s().p("AgYAcQgJgKAAgSIAAAAQAAgKAEgJQAEgJAIgEQAIgFAJgBQAPAAAJALQAKAJAAAQIAAADQAAAKgEAJQgEAJgIAEQgIAGgKAAQgPgBgJgKgAgMgRQgFAGAAAMQAAALAFAHQAFAGAHAAQAJAAAEgGQAFgHAAgMQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_5.setTransform(-0.4,11.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#558B2F").s().p("AgSAmIAAhJIAPAAIAAAIQAGgKAKAAIAGABIAAAQIgHgBQgLAAgDAJIAAAyg");
	this.shape_6.setTransform(-6.4,11.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#558B2F").s().p("AgHAzIAAhJIAPAAIAABJgAgGgjQgCgDAAgDQAAgEACgCQADgDADAAQAFAAACADQACACABAEQgBADgCADQgCACgFAAQgDAAgDgCg");
	this.shape_7.setTransform(-11,10.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#558B2F").s().p("AAOA0IAAgvQAAgHgEgDQgDgDgGAAQgJAAgFAIIAAA0IgQAAIAAhnIAQAAIAAAnQAIgKAMAAQAXAAAAAaIAAAwg");
	this.shape_8.setTransform(-16.7,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#558B2F").s().p("AgWAdQgJgLAAgRIAAgBQAAgRAJgKQAJgLAPAAQANABAIAHQAJAIAAANIgQAAQAAgHgEgEQgEgEgGAAQgHAAgFAGQgEAGAAAMIAAABQAAANAEAFQAEAGAIAAQAGAAAEgEQAEgDAAgGIAQAAQAAAIgEAGQgEAGgHADQgHADgIABQgPAAgJgKg");
	this.shape_9.setTransform(-24.2,11.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#558B2F").s().p("AgIAzIAAhJIAQAAIAABJgAgGgjQgDgDABgDQgBgEADgCQADgDADAAQAFAAACADQACACAAAEQAAADgCADQgCACgFAAQgDAAgDgCg");
	this.shape_10.setTransform(-29.7,10.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#558B2F").s().p("AgYAcQgJgKAAgSIAAAAQAAgKAEgJQAEgJAIgEQAIgFAJgBQAPAAAJALQAKAJAAAQIAAADQAAAKgEAJQgEAJgIAEQgIAGgKAAQgPgBgJgKgAgMgRQgFAGAAAMQAAALAFAHQAFAGAHAAQAJAAAEgGQAFgHAAgMQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_11.setTransform(-35.5,11.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#558B2F").s().p("AgRAvQgJgEgFgHQgFgHAAgJIARAAQAAAIAGAFQAFAEAKAAQAJAAAEgDQAFgEAAgGQAAgGgFgEQgEgEgLgDQgMgEgIgDQgNgJAAgOQAAgMAKgHQAKgIAOAAQALAAAIAEQAIAEAEAHQAFAHAAAIIgRAAQAAgIgFgEQgFgEgJAAQgHAAgFADQgFAEAAAGQAAAGAFAEQAFADALAEQAMADAHAEQAHAEADAGQADAFAAAIQAAAMgJAIQgKAHgQAAQgKAAgJgEg");
	this.shape_12.setTransform(-43.7,10.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_13.setTransform(56.6,-9.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAAAtQgGAIgMAAQgNAAgJgKQgIgKABgMQABgQAKgFIgOAAIAAgKIARAAQgDgBAAgFIABgFQAFgJAPAAIAGABQALAAAGAJQAHAIgBAMIgdAAQgIAAgGAEQgGAEAAAIQAAAGAFAGQAFAEAIAAQAFAAAEgDQAEgEAAgGIAAgLIAJAAIAAAMQAAAFAEAEQAEAEAGgBQAQAAACgcQABgRgLgPQgLgOgQAAQgZgBgJARIgMAAQANgaAgABQATAAAOAMQAJAIAFANQAFANgBANQgBASgHAMQgIAPgPAAQgMAAgHgIgAgSgUIACAGIgCAEIAUAAQgCgHgEgCIgJgBIgFAAg");
	this.shape_14.setTransform(49.7,-10.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgKAUIAAgRIgIAAIAAgHIAIAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgFADgJAAIAAAUgAgBgOIAAAKQAIAAgBgFQAAgGgGAAIgBABg");
	this.shape_15.setTransform(42.8,-15.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAJAoIAAgGQgHAFgFABQgFACgKAAQgOgBgJgJQgKgJAAgOQgBgLAEgJIAKAAQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQAAABABAAQAAAAABAAQAAABABAAQABAAABAAQAEAAAEgDQAIgEACgGIAAgEQAAgHgEgCQgEgBgEABQAGACAAAIQAAAEgDADQgDACgEAAIgEAAQgIgDAAgJIABgGQAFgLAPAAQAGAAAEADQAFADABAEQAJgLARABQARABAKARQAGALAAANQgBAbgUAKQgGAEgIAAIgJgCgAgEgPQAIAAAGAFQAJAHAAANQAAANgIAIIAHABQAHAAAEgDQAKgIAAgQQAAgKgHgIQgHgIgLgBIgCAAQgLAAgFAHgAgoAGQAAAGADAFQAFAGAMAAIAHAAQAIgBAFgGQAEgFAAgHQAAgDgCgDQgCgEgHAAQgDAAgFADIgJAEQgEACgFAAQgFAAgCgCIAAAFg");
	this.shape_16.setTransform(39.1,-9.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_17.setTransform(31.6,-9.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAMAgQAJAAAGgEQAHgEADgHQADgJgBgHQAAgJgHgHQgGgIgKABQgPABgDANQACgCAEAAQAEAAADACQAJAGAAAOQgBAOgLAIQgKAIgPAAQgQAAgKgJQgKgKAAgOQAAgPALgHQgDgBgCgDQgCgEAAgDQAAgIAHgEQAGgEAIAAQAHAAAFAEQAGAEABAIQAJgQATAAQAPgBAKANQAKANAAAQQAAARgMAMQgLAMgTAAgAgjgEQgFAEAAAFQAAAIAIAFQAHAEAKAAQAJAAAGgEQAHgDAAgHQABgDgCgDQgCgCgCAAQgBAAgBAAQAAAAAAAAQAAAAgBAAQAAABgBAAQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAABIgKAAIAAgIIABgHIgMAAQgHAAgEAFgAghgdQACABACAEQABADgBADIAKAAQAAgGgEgDQgDgDgEAAIgDABg");
	this.shape_18.setTransform(24.2,-9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAJAIQAGAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAJAHAKAAQAKAAAGgFQAJgFgBgJQABgLgLgDQADAEAAAIQAAAEgEAEQgGAEgFAAQgHAAgGgEQgEgFAAgHQgBgIAHgFQAGgEAJAAQANAAAIAHQAKAIAAALQAAAQgLAKQgLALgPAAQgQAAgMgMgAgBgJQgCACAAADQAAADACABQABACACAAQAEAAACgCQACgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_19.setTransform(14.6,-9.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgKAUIAAgRIgHAAIAAgHIAHAAIAAgKIgIAAIAAgIIAVAAQAHAAAEADQAFAEAAAHQAAAGgGACQgGADgHAAIAAAUgAAAgOIAAAKQAGAAABgFQAAgGgHAAIAAABg");
	this.shape_20.setTransform(3.5,-15.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_21.setTransform(3.2,-9.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgCA1QgSgBgLgLQgLgLAAgRQgBgLAFgIQAEgIAKgJIASgRQAGgHACgFIARAAQgEAGgFAGQgEAFgIAGQAMgBAJADQAZAJAAAdQAAATgOAMQgMALgSAAIgCAAgAgXgLQgHAIAAAKQAAAKAHAHQAJAJAOAAQALAAAJgGQAKgIAAgNQAAgGgCgFQgDgHgIgFQgHgDgJAAQgPAAgJAJg");
	this.shape_22.setTransform(-3.1,-10.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAIAIQAHAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAIAHALAAQAJAAAHgFQAJgFAAgJQgBgLgKgDQADAEAAAIQAAAEgEAEQgGAEgGAAQgGAAgFgEQgFgFgBgHQAAgIAHgFQAGgEAJAAQANAAAIAHQAKAIAAALQAAAQgLAKQgKALgQAAQgQAAgMgMgAgBgJQgDACAAADQABADACABQABACACAAQADAAADgCQACgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_23.setTransform(-11.7,-9.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgkAqQgIgJABgLQABgLAKgFIgQAAIAAgHIANAAQgDgEABgFQACgHAIgDQAGgDAJAAQAMABAGAJQAHAJAAAKIgXAAQgGAAgFAEQgGADAAAGQgBAHAJAFQAJAEAOAAQAhAAAAggQAAgLgHgJQgJgKgMAAIgmAAQgGAAgCgDQgDgDABgEQABgKANgEQAKgDARAAQAYAAANAOIADAFQABAEAAADIgLAAQAGADAGAKQAFALAAAJQAAAWgLAOQgMAOgWAAQgcAAgMgNgAgTgMIADAEQABADgCAEIARAAQAAgFgDgEQgEgDgGAAIgGABgAgVgmQAAACAHAAIAZAAQAHAAAIAEIAFADQABgEgFgFQgHgGgTAAQgXAAABAGg");
	this.shape_24.setTransform(-21,-10.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AAbATQAHgEAAgHIAAgCQgCgFgLgCQgIgCgNAAQgLAAgJADQgLABgCAFIAAACQAAAHAHAEIgJAAQgJgCAAgMQABgEABgBQAFgLAMgEQAKgDAPAAQAkAAAHASQABABABAEQAAALgJADg");
	this.shape_25.setTransform(-31.1,-14.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAAAhQgGAJgMAAQgQAAgIgMQgHgLAAgTQABgTANgMQAJgJAMgBQAFAAAEABIAAAKQgFgDgIADQgJADgFAHQgFAIAAAKQAAAYARAAQAHAAAEgEQAEgDAAgHIAAgIIAKAAIAAAIQAAAHAEADQAEAEAGAAQAIAAAFgFQAFgFAAgIQAAgHgFgFQgFgEgIAAIgXAAQgBgPAGgHQAGgHAMAAQAWABAAARIAAAEIgIAAQAHADAFAIQAFAHAAAIQAAAPgIAKQgIAKgOAAQgOAAgGgJgAAHgVIAOAAIAGAAIAAgCQAAgIgKAAQgJAAgBAKg");
	this.shape_26.setTransform(-31,-9.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgSAnIAAgJQADACAFAAQAFAAAEgKQACgJAAgMQAAgLgCgJQgEgLgFAAQgFAAgDACIAAgKQAEgDAJAAQALAAAHAOQAGANAAAPQAAAQgGAMQgIANgLAAQgHAAgFgDg");
	this.shape_27.setTransform(-38.3,-9.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AACAgQgGAKgQAAQgMAAgIgJQgIgKAAgMQABgNAIgHIgMAAIAAgKIAMAAQgCgBAAgEIABgFQAEgMASAAQAPAAAFAKQAFgKAPAAQAJAAAFADQAGAFABAHQABAHgDAEIgBAAQAMAJAAAPQAAAOgJAJQgIAKgMAAQgPAAgGgKgAAGADIAAAJQABAEAFAEQAFACAFAAQAHAAAFgGQAGgFAAgGQgBgFgFgFQgEgEgHAAIgqAAQgHAAgFAFQgEAEgBAFQAAAHAFAFQAFAEAHABQAHAAAEgCQAFgDAAgFIAAgJgAALgbQgDAEgBAEIAWAAQACgBAAgDQgBgIgKAAQgGAAgDAEgAgXgeQACADAAADQABADgCACIASAAQgBgGgFgEQgDgCgFAAQgDAAgCABg");
	this.shape_28.setTransform(-45.4,-9.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgaAeQgMgMAAgSQAAgSAMgLQAMgMARAAQATAAAJAIQAGAGAAAHIgKAAQAAgFgIgDQgGgDgJAAQgLAAgJAHQgJAJAAALQAAALAIAHQAIAHAMAAQAIAAAHgFQAJgFgBgJQABgLgMgDQAEAEAAAIQAAAEgEAEQgFAEgGAAQgHAAgGgEQgEgFAAgHQgBgIAHgFQAGgEAIAAQANAAAKAHQAJAIAAALQAAAQgLAKQgLALgPAAQgQAAgMgMgAgBgJQgDACABADQAAADABABQACACADAAQADAAABgCQADgBAAgDQAAgDgCgCQgDgCgCAAQgDAAgBACg");
	this.shape_29.setTransform(-54.6,-9.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.name1, new cjs.Rectangle(-60.8,-21.1,121.7,42.2), null);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(9,13,10,0.988)","rgba(17,25,19,0.953)","rgba(25,45,30,0.898)","rgba(37,73,46,0.82)","rgba(52,111,67,0.718)","rgba(57,123,74,0.686)"],[0,0.165,0.349,0.541,0.741,0.945,1],-454.5,0,454.5,0).s().p("EhHAAqbMAAAhU1MCOBAAAMAAABU1g");
	this.shape.setTransform(454.5,271.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0,0,909,543), null);


(lib.ClipGroup_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EhHAAqbMAAAhU1MCOBAAAMAAABU1g");
	mask.setTransform(536.1,271.5);

	// Layer 3
	this.instance = new lib.Image_0();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.796,0.796);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_0, new cjs.Rectangle(81.6,0,909,543), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAwDBIAAh5QAAgJgHgIQgHgHgKAAIgvAAQgKAAgHAHQgHAIAAAJIAAB5IhIAAQgKAAgIgHQgHgHAAgKIAAiRIgYAAQgKAAgHgHQgHgHAAgKQAAgGAEgGIAJgLICcibIAKgJQAHgFAGAAQAIAAAGAFIAKAJIAwAwIAAgOQAAgKAHgHQAIgHAKAAIAYAAQAKAAAHAHQAHAIAAAJIAABWIAjAjIAJALQAEAGAAAGQAAAKgHAHQgHAHgKAAIgYAAIAACRQAAAKgHAHQgHAHgKAAg");
	this.shape.setTransform(34.5,620.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#689F38").s().p("AhzGwQhUAAg9g7Qg8g9AAhUIAAnHQAAhVA8g7QA8g9BVAAIG0AAIAANgg");
	this.shape_1.setTransform(32.1,620.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#689F38").s().p("EgAzA4fMAAAhw9IBnAAMAAABw9g");
	this.shape_2.setTransform(58.5,361.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,64.2,723);


(lib.ClipGroup_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ar7LQIAA2fIX3AAIAAWfg");
	mask.setTransform(187.7,75.1);

	// Layer 3
	this.instance = new lib.Image_2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.224,0.224);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_2, new cjs.Rectangle(111.4,3.1,152.7,144), null);


(lib.btn4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqlGzQgwAAgigiQghghAAgwIAAp/QAAgwAhghQAigiAwAAIVLAAQAwAAAiAiQAhAhAAAwIAAJ/QAAAwghAhQgiAigwAAg");
	this.shape.setTransform(-0.4,71.1);

	this.instance = new lib.Bitmap4_1();
	this.instance.parent = this;
	this.instance.setTransform(-79.2,-114.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.7,-114.5,159.5,229.2);


(lib.btn3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqlGzQgxAAghgiQghghgBgwIAAp/QABgwAhghQAhgiAxAAIVMAAQAvAAAiAiQAiAhgBAwIAAJ/QABAwgiAhQgiAigvAAg");
	this.shape.setTransform(-0.4,71.1);

	this.instance = new lib.Bitmap3_1();
	this.instance.parent = this;
	this.instance.setTransform(-79.2,-114.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.7,-114.5,159.5,229.2);


(lib.btn2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqmGzQgvAAgigiQgighAAgwIAAp/QAAgwAighQAigiAvAAIVNAAQAvAAAiAiQAiAhAAAwIAAJ/QAAAwgiAhQgiAigvAAg");
	this.shape.setTransform(-0.4,71.1);

	this.instance = new lib.Bitmap2();
	this.instance.parent = this;
	this.instance.setTransform(-79.2,-114.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.7,-114.5,159.5,229.2);


(lib.btn1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqmGzQgvAAgigiQgighABgwIAAp/QgBgwAighQAigiAvAAIVMAAQAwAAAiAiQAhAhABAwIAAJ/QgBAwghAhQgiAigwAAg");
	this.shape.setTransform(-0.4,71.1);

	this.instance = new lib.Bitmap1_1();
	this.instance.parent = this;
	this.instance.setTransform(-79.2,-114.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.7,-114.5,159.5,229.2);


(lib.back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABCBxIiDhxICDhw");
	this.shape.setTransform(-3.3,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABCBxIiDhxICDhw");
	this.shape_1.setTransform(3.2,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#00A9E4","#00D3D0"],[0,1],0,-18.7,0,18.8).s().p("AihC8QgbAAgTgUQgTgSAAgcIAAjzQAAgbATgTQATgTAbAAIFCAAQAcAAASATQAUATAAAbIAADzQAAAcgUASQgSAUgcAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.7,-18.7,45.4,37.5);


(lib.slider_mov = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		var crnt;
		var aud1;
		var namee
		
		var chk_play = 0;
		
		this.crnt_fun = function(crnt_var) {
		
		crnt = crnt_var;
		
			
				namee = "audio"+crnt;
				
			chk_play = 0;
				
			
			
		}
		
		this.stop_sound_fun = function() {
		
		aud1 = createjs.Sound.stop("audio");
			
			
		}
		
		
		/*
		this.playBtn.addEventListener("click", playBtn_clk.bind(this));
		
		function playBtn_clk() {
			var namee = "audio"+crnt;
			var aud1 = createjs.Sound.stop("audio");
			 aud1 = createjs.Sound.play(namee);
			
			//alert(crnt);
		}
		
		*/
		
		
		this.playBtn.addEventListener("click", playBtn_clk.bind(this));
		
		function playBtn_clk() {
			
			
			if(chk_play == 0)
			{
				aud1 = createjs.Sound.stop("audio");
				aud1 = createjs.Sound.play(namee);
				chk_play = 1;
				aud1.on("complete", audComplete.bind(this));
				
			}else
			{
				aud1.paused = false;
			}
			
			
			
			//alert(crnt);
		}
		
		
		
		
		
		
		
		function audComplete(event) {
			chk_play = 0;
			//alert("aaaaa");
		}
		
		this.pauseBtn.addEventListener("click", pauseBtn_clk.bind(this));
		
		function pauseBtn_clk() {
			
			if(chk_play == 1)
			{
			//aud1 = createjs.Sound.pasued(namee);
			aud1.paused = true;
			}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// playbtn
	this.pauseBtn = new lib.pauseBtn();
	this.pauseBtn.parent = this;
	this.pauseBtn.setTransform(416.6,240.5);
	new cjs.ButtonHelper(this.pauseBtn, 0, 1, 1);

	this.playBtn = new lib.playBtn();
	this.playBtn.parent = this;
	this.playBtn.setTransform(369.3,240.5);
	new cjs.ButtonHelper(this.playBtn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.playBtn},{t:this.pauseBtn}]}).wait(5));

	// Layer 4
	this.instance = new lib.Bitmap6();
	this.instance.parent = this;
	this.instance.setTransform(264,13);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrAAIgfhWICVBWIiVBXg");
	this.shape.setTransform(-285.9,112.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_1.setTransform(65,243.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_2.setTransform(59.2,239.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_3.setTransform(50.1,239.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgIQAEgFABgFIAABGg");
	this.shape_4.setTransform(37,245);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_5.setTransform(37,238.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_6.setTransform(21.9,232.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_7.setTransform(16.9,239.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_8.setTransform(3.2,238.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_9.setTransform(-9.8,239.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_10.setTransform(-28.8,238.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_11.setTransform(-41.4,238.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAALgJQAMgJABgOIABgHQAAgOgJgLQgIgJgOAAIgpAAQgUAAAAgNQAAgXAyAAQAeAAALAJQAIAGAAAIQAAAIgHAFQAQASgBAbQAAAYgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABgAAXgyQgCACAAADQAAADACACQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDAAQgDAAgCACgAgcgvQgBAFAKAAIAcAAQgCgHADgFIgLgBQgZAAgCAIg");
	this.shape_12.setTransform(-53.6,238.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_13.setTransform(-64.2,241.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAGgFQgHgCABgIIAAgFQAFgNAXAAQASABAGAJQAHgJAUgBQAJAAAHAFQAIAEABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAWgPAMQgRAOgdABQgdgBgRgPgAgpgLQgFAGABAHQAAAOAPAGQAMAEASABQARgBAMgEQAQgGABgOQAAgHgFgGQgFgFgIgBIg4AAQgJABgEAFgAANgkQgFACgBAHIAdAAIAAgDQAAgEgEgDQgDgCgEAAIgCgBQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQgBgKgNgCIgFgBIgFABg");
	this.shape_14.setTransform(-76.2,239.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAHgNAPgFQAMgEATAAQAtAAAJAWQACADgBAEQABAOgLAEg");
	this.shape_15.setTransform(-89.1,233.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_16.setTransform(-89.5,239.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_17.setTransform(-109.1,239.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAAPgzIAGADIACgFQAAgFgJgDQgIgDgIAAQgKAAgIACQgKADAAAFIABAEIAFgBQAHgCANAAQAMAAAHACg");
	this.shape_18.setTransform(-120.3,240.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgMIA7AAIAAAMIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEABQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAIgMAAQgOABgIgIg");
	this.shape_19.setTransform(-129.2,240.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCgBgIIABgFQAFgNAXAAQARABAHAJQAHgJATgBQAKAAAHAFQAIAEABAIQABAGgDAGQAEAEAEAHQADAIAAAIQAAAWgQAMQgQAOgdABQgdgBgRgPgAgpgLQgFAGAAAHQABAOAQAGQAMAEARABQARgBAMgEQAPgGABgOQABgHgFgGQgFgFgIgBIg5AAQgIABgEAFgAANgkQgFACgBAHIAcAAIABgDQAAgEgDgDQgEgCgFAAIgBgBQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgNgCIgGgBIgFABg");
	this.shape_20.setTransform(-139.2,239.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgMIA7AAIAAAMIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEABQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAIgMAAQgOABgIgIg");
	this.shape_21.setTransform(-155.5,240.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_22.setTransform(-164.6,238.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSASQgHgGgEgKIAAgFQAAgMAIgIIAAANIAFAAQAAAIAFAEQAGAGAGgBQAIAAAEgFQAFgGAAgIIAMAAIAAAGQAAANgJAIQgIAJgNAAQgKAAgIgGg");
	this.shape_23.setTransform(-173.6,246.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_24.setTransform(-177.7,239.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_25.setTransform(-191.5,239.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_26.setTransform(-206.9,239.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_27.setTransform(-215.7,238.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_28.setTransform(-224.4,232.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_29.setTransform(-228.9,239.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_30.setTransform(-238.3,239.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_31.setTransform(-247.5,239.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_32.setTransform(-259.6,239.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLAOQgLAOgTABQgSAAgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_33.setTransform(175.3,216.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAQAAAPABAFQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_34.setTransform(161.9,214.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgHQAEgGABgFIAABGg");
	this.shape_35.setTransform(142,220);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgwA9QgOgFgEAAQgGAAgFAHIgKgGIATgbQgIgKAAgQQAAgFABgGQAEgQAPgJQALgGAPAAIAIAAIARgaIANAAIgRAbIAJAEIAIAFIAFgGQAGgEAHAAQAJAAAGAGQAHgHAKABQAcABABAvQABAXgIAOQgKASgTAAQgNAAgHgHQgHgHAAgMIAAgGIAMAAIAAAEQAAAPANAAQAJAAADgOQACgHAAgRQABgOgCgKQgDgNgFAAQgJAAAAAMIAAAZIgNAAIAAgZQAAgNgHAAQgEAAgCACQgCACgBADQALAMgBATQgBAYgOANQgMANgYABIgCAAQgJAAgMgEgAgzAwQAKAEAKAAIAJgBQANgCAHgKQAHgKAAgNQAAgNgHgIQgJgKgMgBIgLAQQADAAADAAQAHAAAFAEQAGAEABAHQAAAJgEAGQgFAHgHACQgGACgIAAQgLAAgJgEIgGAJIAEgBQAFAAAFADgAg0AaQACACAFABQAHABADgBQgGgBgDgFQgCgEACgEgAgiARQAAAFAGAAQAGAAAAgFQAAgGgGAAQgGAAAAAGgAg5gEQgEAFAAAIIABAIIAWggQgNACgGAJg");
	this.shape_36.setTransform(139.3,213.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgHQAEgGABgFIAABGg");
	this.shape_37.setTransform(124.5,220);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_38.setTransform(124.3,214.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAAPgzIAGADIACgFQAAgFgJgDQgIgDgIAAQgKAAgIACQgKADAAAFIABAEIAFgBQAHgCANAAQAMAAAHACg");
	this.shape_39.setTransform(113.2,215.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_40.setTransform(94.8,214.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgqA6QgRgLAAgTQAAgSAMgIIgLAAIAAgKIAOAAQgDgCAAgFQAAgGAFgEQAKgHAPACQASABAHALQAFAHAAANIgcAAQgZAAgCAQQgCANAPAHQAMAFAQgBQARgBALgJQAMgKADgQIABgIQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgHAEgJAAQgNgBgFgGQgDgEACgJQABgDgHAAIgEABIAAgLQAJgCALAAQAOAAAJACQAYAGAQAQQASATABAdQAAAbgSASQgRASgbAAQgZAAgPgKgAgYgSQAAAAABABQAAAAABAAQAAABAAABQAAAAAAABQABADgDACIAXAAQgBgFgKgEIgGgBIgGABgAgTgxQAAAFAIAAQAKAAADgHQgHgEgPAAIABAGg");
	this.shape_41.setTransform(82.2,213.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_42.setTransform(67.2,207.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_43.setTransform(62.7,214.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_44.setTransform(49.5,214.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_45.setTransform(37.9,214.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_46.setTransform(26.3,213.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_47.setTransform(13.5,220.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_48.setTransform(12.8,214.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_49.setTransform(-7.1,213.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_50.setTransform(-19.9,220.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_51.setTransform(-20.6,214.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgHQAEgGABgFIAABGg");
	this.shape_52.setTransform(-40.5,220);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAIAFACAJQABAJgEAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_53.setTransform(-40.9,214.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAFgDQgFgDAAgIIABgFQAFgNAWAAQASAAAGALQAHgLAUAAQAJABAHAEQAIAEABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAWgPANQgRAOgdgBQgdABgRgQgAgpgLQgFAGABAIQAAANAPAGQAMAFASgBQARABAMgFQAQgGABgNQAAgIgFgGQgFgGgIAAIg5AAQgHAAgFAGgAANglQgFAEgBAFIAdAAIAAgCQAAgFgEgCQgDgDgFABIgBgBQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_54.setTransform(-53.8,214.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_55.setTransform(-62.6,207.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_56.setTransform(-67.1,214.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgfA+QgOgHACgOQABgHAGgEQAGgDAIAAIAHAAIAAAKIgEAAQgIAAAAAFQAAAIAbgBQAIAAAHgCQAIgDAAgDQgBgGgOACIgMAAIAAgLQAVgBAMgJQAOgKAAgPIgBgIQgDgNgNgHQgLgGgPAAQgRAAgLAGQgOAIgBAOQAAAMAIAHQAIAHAMAAQAGAAAGgDQAGgDACgFQgFADgFAAQgIAAgGgEQgFgFgBgHQAAgKAGgGQAGgFAKAAQALAAAHAHQAHAHAAAMQgBANgKAJQgKAIgOABQgUACgNgMQgMgLAAgTQAAgYASgOQAQgMAaAAQAcAAAPAPQAOAOAAAXQAAAdgcAQQAKAFgBAJQAAALgMAGQgLAGgRABIgEAAQgQAAgLgGgAgIgVQgDADAAADQAAAEADADQACACAEAAQACAAADgDQACgCAAgEQAAgDgCgDQgDgCgCAAQgEAAgCACg");
	this.shape_57.setTransform(-80,216.5);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_58.setTransform(-95.1,218.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_59.setTransform(-100.9,214.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_60.setTransform(-109.5,213.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgFIANAAQgEAEAAAEQAAAHAKgBQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgKIAABGIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHABgGgCg");
	this.shape_61.setTransform(-122.3,220);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_62.setTransform(-122.1,213.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_63.setTransform(-133,216.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAFgDQgFgDgBgIIACgFQAEgNAXAAQARAAAHALQAHgLATAAQAKABAHAEQAIAEABAIQABAGgDAGQAFAEADAHQADAIAAAIQAAAWgQANQgQAOgdgBQgdABgRgQgAgpgLQgFAGAAAIQABANAPAGQANAFARgBQARABAMgFQAPgGABgNQABgIgFgGQgFgGgIAAIg5AAQgHAAgFAGgAANglQgFAEgBAFIAcAAIABgCQAAgFgEgCQgDgDgFABIgBgBQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgNgCIgGgBIgFABg");
	this.shape_64.setTransform(-145,214.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_65.setTransform(-157.9,208.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_66.setTransform(-158.3,214.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_67.setTransform(-173.9,207.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_68.setTransform(-178.7,214.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_69.setTransform(-192.2,214.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLAOQgLAOgTABQgSAAgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_70.setTransform(-205,216.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAQAAAPABAFQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_71.setTransform(-218.4,214.8);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_72.setTransform(-232,208.3);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCAAgGIABgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAJAFAAAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHAAgIQgBgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAGAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgEACg");
	this.shape_73.setTransform(-232.3,214.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_74.setTransform(-245.2,208.3);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_75.setTransform(-245.2,214.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_76.setTransform(-258.2,213.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_77.setTransform(218.1,191.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_78.setTransform(204.7,189.8);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_79.setTransform(191.7,188.3);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFAAAJQABAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_80.setTransform(179.1,189.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_81.setTransform(163.9,182.8);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_82.setTransform(159.1,189.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_83.setTransform(145.5,189.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_84.setTransform(133.5,188.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_85.setTransform(121.6,188.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_86.setTransform(113,182.8);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_87.setTransform(112.7,189.9);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_88.setTransform(104,188.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_89.setTransform(92.6,189.9);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgMAVQAKgGAAgGIAAgBIgGgDQgGgFAAgGQAAgEADgEQAEgGAHAAQAFAAAEACQAGAEAAAIQAAAGgDAEQgBADgFAGIgIAIg");
	this.shape_90.setTransform(78.5,194.6);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_91.setTransform(72.4,189.9);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AgyA1QgKgLABgRQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_92.setTransform(63.6,188.6);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_93.setTransform(54.9,182.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_94.setTransform(50.4,189.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_95.setTransform(41,189.9);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_96.setTransform(31.8,189.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_97.setTransform(19.7,189.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgDIANAAQgEACAAAGQAAAFAKAAQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgLIAABHIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHAAgGgCg");
	this.shape_98.setTransform(1.6,195);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_99.setTransform(1.8,188.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_100.setTransform(-10.9,189.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_101.setTransform(-24.3,189.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_102.setTransform(-36.4,189.9);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_103.setTransform(-48,188.4);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_104.setTransform(-59.4,189.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_105.setTransform(-77.4,189.8);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_106.setTransform(-90.7,189.9);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_107.setTransform(-104.8,189.9);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_108.setTransform(-114.5,182.8);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_109.setTransform(-119,189.9);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgOAMgGIgVAAIAAgKIARAAQgDgEABgHQACgJAKgDQAIgFALABQAOABAIALQAJAMAAANIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAGQAKAFASgBQAqAAAAgnQAAgNgKgMQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQASIAEAGQABAFAAAEIgOAAQAIADAHANQAGANABAMQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADABABADQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAFIAHADQABgEgHgGQgJgIgYAAQgcABAAAGg");
	this.shape_110.setTransform(-132.2,188.2);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("AguAXQgIgDAAgNIABgHQAIgWAuAAQAXAAAMADQARAGACANQABAJgGAGQgHAFgJAAQgIAAgGgEQgGgFAAgHQAAgEACgEIgPAAQgjAAgGAKIgBAFQAAAIAIAEgAAbABQAAADACACQACACADAAQAHAAAAgHQAAgCgCgCQgCgCgDAAQgHAAAAAGg");
	this.shape_111.setTransform(-151.2,183.2);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_112.setTransform(-151.1,191.3);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("AguAXQgIgDAAgNIABgHQAIgWAuAAQAXAAAMADQARAGACANQABAJgGAGQgHAFgJAAQgIAAgGgEQgGgFAAgHQAAgEACgEIgPAAQgjAAgGAKIgBAFQAAAIAIAEgAAbABQAAADACACQACACADAAQAHAAAAgHQAAgCgCgCQgCgCgDAAQgHAAAAAGg");
	this.shape_113.setTransform(-163.9,183.2);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPIADAIQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_114.setTransform(-164.2,189.9);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("AgwA9QgOgFgEAAQgGAAgFAHIgKgGIATgbQgIgKAAgQQAAgFABgGQAEgQAPgJQALgGAPAAIAIAAIARgaIANAAIgRAbIAJAEIAIAFIAFgGQAGgEAHAAQAJAAAGAGQAHgHAKABQAcABABAvQABAXgIAOQgKASgTAAQgNAAgHgHQgHgHAAgMIAAgGIAMAAIAAAEQAAAPANAAQAJAAADgOQACgHAAgRQABgOgCgKQgDgNgFAAQgJAAAAAMIAAAZIgNAAIAAgZQAAgNgHAAQgEAAgCACQgCACgBADQALAMgBATQgBAYgOANQgMANgYABIgCAAQgJAAgMgEgAgzAwQAKAEAKAAIAJgBQANgCAHgKQAHgKAAgNQAAgNgHgIQgJgKgMgBIgLAQQADAAADAAQAHAAAFAEQAGAEABAHQAAAJgEAGQgFAHgHACQgGACgIAAQgLAAgJgEIgGAJIAEgBQAFAAAFADgAg0AaQACACAFABQAHABADgBQgGgBgDgFQgCgEACgEgAgiARQAAAFAGAAQAGAAAAgFQAAgGgGAAQgGAAAAAGgAg5gEQgEAFAAAIIABAIIAWggQgNACgGAJg");
	this.shape_115.setTransform(-179.9,188.6);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_116.setTransform(-191,189.9);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_117.setTransform(-199.7,188.4);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFFFFF").s().p("AgCBGQgXgBgNgOQgOgNAAgWQAAgOAFgKQAFgKAMgLIAYgVIAJgPIAVAAQgDAHgHAHQgGAHgJAHQAPgBAKAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAOAAALgIQANgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgSAAgLAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAFAEQAHAFAAAJQAAAIgIAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_118.setTransform(-211.7,188.1);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAJAFIgLAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADABAEQAAAOgKAEg");
	this.shape_119.setTransform(-223.9,183.3);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_120.setTransform(-224.2,189.9);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_121.setTransform(-243.9,188.4);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_122.setTransform(-257.5,189.9);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_123.setTransform(159.6,157.8);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAJAFAAAJQABAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACABAFQAAADgCAEIAWAAQgBgJgGgEQgEgDgGAAQgEAAgDACg");
	this.shape_124.setTransform(155.2,164.9);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_125.setTransform(143.7,164.9);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_126.setTransform(136.2,157.8);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCAAgGIABgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAJAFAAAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHAAgIQgBgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAGAGQAFAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgEACg");
	this.shape_127.setTransform(131.8,164.9);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFFFFF").s().p("AAFBCQgEgCgDgFQgHAKgSAAQgWAAgJgYQgFgPABgXQABgQAJgLQAKgNAOAAQAHAAAEAEQAFAEgBAHQAAADgDAEQgDAEABAEQABAKAOABQAEAAAGgCQAFgDABgDQgDAAgEAAQgGAAgFgDQgFgEgBgGQgBgIAGgGQAFgGAIAAQAWABAAAYQAAAMgJAHQgIAIgMABQgKABgIgEQgJgFgDgJIgBgFQAAgHADgEQAAAAABgBQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAgBAAAAQAAgBgBAAQAAAAgBAAQgHgBgGAKQgGAJAAAKQAAANAGALQAHALALAAQARAAAAgTIALAAQgBAKAFAFQAFAEAJAAQAOAAAHgOQAGgLAAgRQAAgVgLgQQgOgSgXgBQgmAAgHAZIgNAAQADgQAQgKQAPgLAYABQAgAAARAYQAOAUAAAXQAAAdgJASQgLAWgXAAQgLAAgFgDgAAAgQQAAAHAHAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgHAAAAAHg");
	this.shape_128.setTransform(118.8,163.2);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_129.setTransform(103.4,157.8);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFFFFF").s().p("AgqA6QgRgLAAgTQAAgSAMgIIgLAAIAAgKIAOAAQgDgCAAgFQAAgGAFgEQAKgHAPACQASABAHALQAFAHAAANIgcAAQgZAAgCAQQgCANAPAHQAMAFAQgBQARgBALgJQAMgKADgQIABgIQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgHAEgJAAQgNgBgFgGQgDgEACgJQABgDgHAAIgEABIAAgLQAJgCALAAQAOAAAJACQAYAGAQAQQASATABAdQAAAbgSASQgRASgbAAQgZAAgPgKgAgYgSQAAAAABABQAAAAABAAQAAABAAABQAAAAAAABQABADgDACIAXAAQgBgFgKgEIgGAAIgGAAgAgTgxQAAAFAIAAQAKAAADgHQgHgEgPAAIABAGg");
	this.shape_130.setTransform(99.6,163.3);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_131.setTransform(84.4,168.8);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_132.setTransform(78.6,164.9);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_133.setTransform(69,164.9);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_134.setTransform(59.3,157.8);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_135.setTransform(54.3,164.9);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_136.setTransform(40.3,164.9);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_137.setTransform(24.5,164.9);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_138.setTransform(15.2,164.8);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_139.setTransform(2.1,163.4);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_140.setTransform(-10.4,163.4);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_141.setTransform(-25.6,164.9);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_142.setTransform(-34.4,163.6);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_143.setTransform(-43.1,157.8);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_144.setTransform(-47.6,164.9);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_145.setTransform(-57,164.9);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_146.setTransform(-66.2,164.8);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_147.setTransform(-78.3,164.9);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_148.setTransform(-96.1,163.4);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_149.setTransform(-108.9,163.4);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_150.setTransform(-128.4,158.3);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_151.setTransform(-129.1,164.8);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_152.setTransform(-139.6,165.1);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_153.setTransform(-150.1,164.8);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_154.setTransform(-159.7,164.9);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_155.setTransform(-168.8,164.9);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_156.setTransform(-180.7,164.9);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_157.setTransform(-198.7,164.8);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOAAgVQgBgTALgOQgCgBgEgEIgFgIIgBgLQAAgKAJgKQAIgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgJAFQAPgBALAEQAfAMABAkQgBAXgRAQQgPAOgXAAIgDAAgAgcgMQgJALABAMQAAANAHAIQAMALASAAQAOAAAKgHQANgLAAgQQAAgHgCgGQgEgJgKgGQgIgFgMAAQgTAAgLAMgAgjgrQgFAGAAAFQABAHAFADIAEgEIAYgWQgCgEgGgBIgDAAQgMAAgGAKg");
	this.shape_158.setTransform(-210.8,163.2);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_159.setTransform(-219.9,165.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_160.setTransform(-228.2,166.4);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgMACgQQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_161.setTransform(-246.4,166.3);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgYQAAgQAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgSQAAgOgHgJQgGgMgNAAIgvAAQgWAAAAgNQAAgOARgHQAOgGAVAAQAfAAAPARQAGAIABALIgMAAQgCgLgNgGQgKgFgQAAQgcAAAAAKQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAnAAQAKAAAIAEQAXAKAAAlQAAAbgQASQgRASgZAAQgYAAgRgQgAgOACQAAAGAFAEQAEAEAFABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAg");
	this.shape_162.setTransform(-258.7,162.9);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_163.setTransform(87.8,139.8);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_164.setTransform(74.3,139.8);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFFFFF").s().p("AgrA2QgQgPACgbQABgQAMgMQAMgLATAAIASAAIAAAMIgQAAQgNAAgJAHQgJAHAAAMQAAAPANAJQAMAJARgBQAUgBAKgMQALgNAAgSIAAgHQgDgYgQgNQgLAMgSABQgSABgHgLQgCgCAAgEQAAgFACgDIgHAAIAAgMIANAAQAeAAARAIQATAJAJAUQAKATgBAXQgBAZgQAQQgRARgZAAQgaAAgQgPgAgSg1QAAADADADQAEACADAAQAMABAFgHIgNgEIgNgBIgBADg");
	this.shape_165.setTransform(61.8,138.2);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_166.setTransform(50.1,138.5);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgNgNgBgWQAAgOAFgKQAFgKANgLIAXgVIAJgPIAVAAQgEAHgFAHQgHAHgJAHQAOgBALAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgDgKgKgFQgJgFgMAAQgSAAgLAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAGAEQAFAFAAAJQABAIgIAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_167.setTransform(32.3,138.1);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_168.setTransform(20.6,138.4);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_169.setTransform(12,132.8);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_170.setTransform(11.7,139.9);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_171.setTransform(2.9,138.4);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_172.setTransform(-8.5,139.9);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_173.setTransform(-26.2,138.4);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_174.setTransform(-35,139.9);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_175.setTransform(-43.7,139.8);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_176.setTransform(-57,139.9);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRAOgIQgJgCAAgKQAAgKALgFQAHgDAMAAQAQAAAHAKQAEgIAPgCIAJAAIAGgFIAEgGQAHgKAAgMIARAAIgDAMQgCAHgEAGQgFAGgFAFQAIAFACAKQAAAFgCAFIgKAAQAFABAFAFQAJAIAAAOQAAAWgTANQgRAMgXAAQgYABgSgNgAgnAFQgFAGAAAHQABAMAOAHQANAGAQAAQARAAAMgFQAOgIABgNQAAgHgFgGQgFgEgMAAIgwAAQgHAAgGAFgAAPgXQgFACgDACQgEADAAAFIAUAAQAHAAAFABQABgCgBgEQgDgHgNAAIgEAAgAgfgWQADACABADQAAADgCADIAWAAQgCgIgHgDIgIgBQgEAAgDABg");
	this.shape_177.setTransform(-69.9,138.3);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgLQANgMASAAIAOAAIAAAMIgKAAQgPAAgIAHQgJAFgBAMQAAAQAOAKQAMAJARgBQASAAAMgKQANgLgBgTQAAgTgLgKQgJgJgQABIg7AAIAAgMQAQABAAgCQAAAAgBgBQAAAAAAAAQgBAAAAgBQgBAAAAgBQgEgBAAgCIgBgEQAAgRAsAAQAbAAAOANQANAKgCAMIgOAAIABgCQAAgHgJgFQgLgHgSAAQgTAAgEAFQgBABAAAAQAAABgBAAQAAABAAAAQAAAAAAABQAAAFAJAAIAcAAQANAAAMAHQAUAOAAAdQAAAdgPARQgQATgbgBQgaAAgQgOg");
	this.shape_178.setTransform(-88.7,138);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_179.setTransform(-99.9,139.9);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_180.setTransform(-110.6,138.5);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_181.setTransform(-129.2,138.4);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_182.setTransform(-142.8,139.9);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgLQANgMASAAIAOAAIAAAMIgKAAQgPAAgIAHQgJAFgBAMQAAAQAOAKQAMAJARgBQASAAAMgKQANgLgBgTQAAgTgLgKQgJgJgQABIgmAAQgIAAgFgFQgFgDAAgHQAAgOAOgFQALgFAYgBQAbAAAOANQANAKgCAMIgOAAIABgCQAAgHgJgFQgMgHgSAAQgWAAgGAFQAAABgBAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAFAIAAIAiAAQANAAAMAHQAUAOAAAdQAAAdgPARQgQATgbgBQgaAAgQgOg");
	this.shape_183.setTransform(-162.5,138);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAIAFIgMAAQgJgDgBgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQAAAOgLAEg");
	this.shape_184.setTransform(-175.1,133.3);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_185.setTransform(-175.4,139.9);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_186.setTransform(-194.4,138.4);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_187.setTransform(-203.1,132.8);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_188.setTransform(-208.1,139.9);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_189.setTransform(-221.9,138.4);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FFFFFF").s().p("AgpA3QgQgPAAgYQAAgQAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAJQAAAOgJAKQgJAJgMAAQgLgBgIgGQgJgHAAgKIADgPQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQASAAAMgMQANgMAAgSQgBgOgGgKQgHgLgMAAIhGAAIAAgKQATABgBgDIgDgCQgEgBAAgEQAAgOAPgEQAIgDATAAQAOAAAKAEQANADAJAKQAGAHABAMIgMAAQgCgLgMgGQgLgFgQAAQgKAAgFACQgIACAAAGQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIAEQAXAKAAAlQAAAbgQASQgRARgZAAQgYABgRgRgAgOACQAAAGAFAEQADAEAGAAQAHAAAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAABg");
	this.shape_190.setTransform(-234.5,138);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_191.setTransform(-247.5,139.8);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_192.setTransform(-259.6,139.9);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_193.setTransform(213.4,107.8);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_194.setTransform(208.9,114.9);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_195.setTransform(195.7,114.8);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_196.setTransform(184.1,114.9);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AgCA5QgIAMgPgBQgTAAgIgOQgKgOABgXQABgZAQgOQAMgKAYgBIAGAAIAAAMIgRABQgIABgGAEQgMAJAAARQgBANAFAJQAEAKAMAAQAIAAAFgFQAFgFAAgIIAAgPIALAAIAAAPQAAASASAAQAMAAAHgPQAGgNgBgUQgBgUgJgNQgOgSgYAAQgOAAgLAFQgLAGgFAJIgOAAQAEgOAPgJQAPgIAUgBQAQABAOAGQAJAFAGAGQAUAVAAAjQABAbgLARQgLASgUAAQgQABgHgMg");
	this.shape_197.setTransform(172.5,113.3);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_198.setTransform(163.7,107.8);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_199.setTransform(161.3,116.4);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgIQAEgFABgFIAABGg");
	this.shape_200.setTransform(149.5,120);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_201.setTransform(149.3,114.8);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAQgGAAgIABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_202.setTransform(129.3,114.8);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_203.setTransform(119.8,107.8);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_204.setTransform(119.5,114.9);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgdARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAATgLAPQgLAOgTAAQgSABgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKAAQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_205.setTransform(110.9,116.3);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_206.setTransform(99.5,114.9);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_207.setTransform(85.4,118.8);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_208.setTransform(74.9,114.9);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgNIA7AAIAAAMIgNAKIgNALIgKAOQgFAIAAAIQABAGAEADQAEAEAEAAQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBAMgJAJQgKAHgMAAQgOABgIgIg");
	this.shape_209.setTransform(64.1,115.1);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_210.setTransform(53.8,114.9);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_211.setTransform(38.2,107.8);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_212.setTransform(34,114.8);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_213.setTransform(22.5,114.9);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgFIANAAQgEAEAAAEQAAAHAKAAQAJAAATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgKIAABGIgMAAQgHgLgOABQgLAAgJADQgSAJgPAAQgHAAgGgCg");
	this.shape_214.setTransform(10.8,120);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_215.setTransform(11,113.4);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_216.setTransform(-7.8,113.4);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_217.setTransform(-20.6,113.4);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgIQAEgFABgFIAABGg");
	this.shape_218.setTransform(-33.5,120);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCgBgGIACgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAIAFABAJQABAJgDAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAIAAAHgGQAHgHAAgIQgBgHgGgFQgFgGgIAAIg1AAQgIAAgHAGQgGAGgBAHQAAAIAHAGQAFAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgCgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_219.setTransform(-33.9,114.9);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCAAgIIABgFQAFgNAWAAQASABAGAJQAIgJASgBQAKAAAHAFQAIAEABAIQABAGgEAGQAFAEAEAHQADAIAAAIQAAAVgPANQgRAOgdABQgdgBgRgPgAgpgLQgFAGAAAHQABAOAPAGQANAEARAAQARAAAMgEQAPgGACgOQAAgHgFgGQgFgFgIgBIg5AAQgHABgFAFgAANgkQgFADgBAFIAcAAIABgCQAAgEgEgDQgDgCgFAAIgBgBQgGAAgEAEgAgggnQADACAAADQABAEgCACIAWAAQAAgJgOgCIgFgBIgFABg");
	this.shape_220.setTransform(-46.8,114.9);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_221.setTransform(-62.2,114.9);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAJAGAFQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_222.setTransform(-70.9,113.6);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_223.setTransform(-79.6,107.8);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_224.setTransform(-84.1,114.9);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_225.setTransform(-93.5,114.9);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_226.setTransform(-102.8,114.8);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_227.setTransform(-114.9,114.9);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_228.setTransform(-128.8,107.8);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_229.setTransform(-133.8,114.9);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAQgGAAgIABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_230.setTransform(-148.2,114.8);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_231.setTransform(-161.5,113.4);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_232.setTransform(-172.8,114.9);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_233.setTransform(-183.8,113.3);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAIAFIgMAAQgJgDgBgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQAAAOgLAEg");
	this.shape_234.setTransform(-196.3,108.3);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_235.setTransform(-196.5,114.9);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAJAFIgLAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADABAEQAAAOgKAEg");
	this.shape_236.setTransform(-209.5,108.3);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAQgGAAgIABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_237.setTransform(-210.1,114.8);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_238.setTransform(-226,107.8);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_239.setTransform(-231,114.9);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgIQAEgFABgFIAABGg");
	this.shape_240.setTransform(-244.7,120);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_241.setTransform(-244.9,113.4);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_242.setTransform(-258,114.9);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#FFFFFF").s().p("AgrAAIgfhWICVBWIiVBXg");
	this.shape_243.setTransform(-285.9,11.8);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_244.setTransform(-131.1,67.8);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_245.setTransform(-136.9,63.9);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_246.setTransform(-146,63.9);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_247.setTransform(-159.1,69);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_248.setTransform(-159.1,62.4);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_249.setTransform(-167.8,56.8);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_250.setTransform(-172.6,63.8);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAPQAKAPARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_251.setTransform(-186,65.3);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_252.setTransform(-204.9,62.4);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#FFFFFF").s().p("AAMBAQgKgFAAgKQgBgIAHgEQgVABgQgMQgOgMABgSQACgWARgKQgGgCAAgKQgHADgGALQgHAMAAANQAAAOAIAOQAHALAJAGIgRAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAFgLAHgGQAMgLALgCIAAAEQAIgFASAAQAZAAAKAMQAJAKAAAOIglAAQgLAAgHAHQgJAHAAALQgBALALAJQAJAJANABQAVABAPgIIAAAMQgIADgGACQgNADgBAGQAAAEAEACQAEACAFAAQAGAAAEgDQAEgDAAgGIgBgEIANAAIAAAFQAAALgJAIQgJAGgMABIgEAAQgJAAgIgDgAgGg1QADACABAEQAAAEgDABIAFAAIAfAAQgDgIgKgDQgFgBgIAAQgHAAgEABg");
	this.shape_253.setTransform(-216.7,65.4);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_254.setTransform(-229.7,62.4);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_255.setTransform(-238.6,63.9);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_256.setTransform(-247.7,63.9);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_257.setTransform(-259.6,63.9);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_258.setTransform(126.6,38.9);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_259.setTransform(118,37.4);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDAAgHIABgGQAFgMAWAAQASgBAGAKQAIgKASABQAKAAAHADQAIAFABAIQABAGgEAGQAFADAEAIQADAIAAAIQAAAVgPAOQgRAOgdAAQgdAAgRgPgAgpgLQgFAFAAAIQABANAPAHQANAFARAAQARAAAMgFQAPgHACgNQAAgIgFgFQgFgGgIABIg5AAQgHgBgFAGgAANgkQgFACgBAHIAcAAIABgEQAAgEgEgCQgDgCgFgBIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgOgDIgFAAIgFABg");
	this.shape_260.setTransform(105.3,38.9);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_261.setTransform(86.4,37.4);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgMACgQQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_262.setTransform(73.9,40.3);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_263.setTransform(61.3,37.4);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#FFFFFF").s().p("AgeA6QgQgLgCgUQgBgSALgJIAJgFQgEgBgBgDQgDgEABgEQgDgBgFAEQgEADgDAFQgFAJAAANQAAANAHAOQAGAOANAKIgRAAQgRgOgEgaIgBgMQAAgMAEgKQAEgKAHgGQAJgHAMAAIAEAAIAAAEQAJgGAOACQASABAHALQAFAHAAANIgdAAQgZAAgCAQQgBANAOAHQANAFAPgBQARgBAMgJQALgKAEgQIABgIQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgJAEgIAAQgNgBgEgGQgEgEADgJQABgDgHAAIgFABIAAgLQAKgCALAAQANAAAKACQAXAGAQAQQATATAAAdQABAbgSASQgRASgcAAQgXAAgPgKgAgOgSQABAAAAABQABAAAAAAQABABAAABQAAAAAAABQAAADgCACIAWAAQgCgFgIgEIgGAAIgHAAgAgJgxQABAFAIAAQAJAAAEgHQgJgEgNAAIAAAGg");
	this.shape_264.setTransform(41.4,37.3);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgJQAEgFABgEIAABGg");
	this.shape_265.setTransform(27.8,44);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgCgCgBgGIABgFQAGgPAWAAQATAAAGAMQAIgMASAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAFADAIAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgHAAgHAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEAAAHIAbAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgEgDgGAAQgEAAgEACg");
	this.shape_266.setTransform(27.3,38.9);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_267.setTransform(15.1,37.3);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#FFFFFF").s().p("AgeA6QgQgLgCgUQgBgSALgJIAJgFQgEgBgBgDQgDgEABgEQgDgBgFAEQgEADgDAFQgFAJAAANQAAANAHAOQAGAOANAKIgRAAQgRgOgEgaIgBgMQAAgMAEgKQAEgKAHgGQAJgHAMAAIAEAAIAAAEQAJgGAOACQASABAHALQAFAHAAANIgdAAQgZAAgCAQQgBANAOAHQANAFAPgBQARgBAMgJQALgKAEgQIABgIQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgJAEgIAAQgNgBgEgGQgEgEADgJQABgDgHAAIgFABIAAgLQAKgCALAAQANAAAKACQAXAGAQAQQATATAAAdQABAbgSASQgRASgcAAQgXAAgPgKgAgOgSQADABAAADQAAADgCACIAWAAQgCgFgIgEIgGAAIgHAAgAgJgxQABAFAIAAQAJAAAEgHQgJgEgNAAIAAAGg");
	this.shape_268.setTransform(-4.5,37.3);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgJQAEgFABgEIAABGg");
	this.shape_269.setTransform(-18.2,44);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCgBgGIACgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAIAFABAJQABAJgDAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAIAAAHgGQAHgHAAgIQgBgHgGgFQgFgGgIAAIg1AAQgIAAgHAGQgGAGgBAHQAAAIAHAGQAFAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgCgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_270.setTransform(-18.6,38.9);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#FFFFFF").s().p("AgrAxQgUgQAAgYQABgWANgMQAHgHALgEIgCgDQgCgDAAgDQAAgEACgCQAFgIAMgDIAVgBQATAAANAJQANAKACARIg0AAQgRAAgKALQgJAJgBAPQAAARANAMQANAMAPAAQANAAALgFQALgEAGgIQAIgLgCgJIAMAAIABAEQAAAJgGAKQgIAOgPAIQgPAHgSAAQgbAAgSgPgAgKgxQADADAAACQAAADgCACIAqAAQgHgJgPgCIgKgBQgGAAgFACg");
	this.shape_271.setTransform(-31.5,40.1);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_272.setTransform(-46.9,31.8);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_273.setTransform(-51.3,38.9);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_274.setTransform(-63.2,38.9);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_275.setTransform(-74.8,38.8);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_276.setTransform(-87.8,40);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#FFFFFF").s().p("AgCA5QgHALgQAAQgSAAgJgOQgJgOAAgXQABgZAQgNQAMgLAYAAIAHAAIAAALIgSABQgIABgGAFQgMAIAAARQgBANAEAJQAGAKALAAQAJAAAEgFQAEgFABgIIAAgQIALAAIAAAQQAAASASAAQANAAAGgOQAGgOgBgUQAAgTgKgOQgOgSgYAAQgOAAgLAGQgMAFgEAJIgOAAQAEgOAPgIQAPgKAVABQAPAAAOAGQAKAEAGAIQASAUABAjQABAbgLARQgLATgUgBQgQAAgHgLg");
	this.shape_277.setTransform(-107.2,37.3);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_278.setTransform(-119.4,37.3);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_279.setTransform(-131.2,37.3);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_280.setTransform(-150,37.4);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_281.setTransform(-158.9,38.9);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCAAgGIABgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAJAFAAAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHAAgIQgBgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAGAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgEACg");
	this.shape_282.setTransform(-167.9,38.9);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_283.setTransform(-180.9,32.3);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_284.setTransform(-181.2,38.9);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_285.setTransform(-200.7,37.4);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgEQAAgNAIgIIAAANIAFAAQAAAHAGAFQAEAFAHAAQAHABAFgHQAGgFgBgIIALAAIABAGQAAAMgJAKQgIAIgNAAQgKAAgIgGg");
	this.shape_286.setTransform(-210.2,45.2);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_287.setTransform(-214.3,38.9);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_288.setTransform(-224,31.8);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_289.setTransform(-228.7,38.8);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_290.setTransform(-240.8,38.9);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_291.setTransform(-254.7,31.8);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("#FFFFFF").s().p("AgqA6QgRgLAAgTQAAgSAMgIIgLAAIAAgKIAOAAQgDgCAAgFQAAgGAFgEQAKgHAPACQASABAHALQAFAHAAANIgcAAQgZAAgCAQQgCANAPAHQAMAFAQgBQARgBALgJQAMgKADgQIABgIQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgHAEgJAAQgNgBgFgGQgDgEACgJQABgDgHAAIgEABIAAgLQAJgCALAAQAOAAAJACQAYAGAQAQQASATABAdQAAAbgSASQgRASgbAAQgZAAgPgKgAgYgSQAAAAABABQAAAAABAAQAAABAAABQAAAAAAABQABADgDACIAXAAQgBgFgKgEIgGAAIgGAAgAgTgxQAAAFAIAAQAKAAADgHQgHgEgPAAIABAGg");
	this.shape_292.setTransform(-258.6,37.3);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_293.setTransform(157.8,13.9);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_294.setTransform(149,12.6);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_295.setTransform(140.3,6.8);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_296.setTransform(135.8,13.9);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_297.setTransform(126.4,13.9);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_298.setTransform(117.2,13.8);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_299.setTransform(105.1,13.9);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgEIANAAQgEAEAAAFQAAAFAKABQAJgBATgGQAOgFALgBIAEAAQAMACAFAHIAAgjQAGgFAGgKIAABGIgMAAQgHgLgOAAQgLAAgJAFQgSAHgPABQgHAAgGgDg");
	this.shape_300.setTransform(87,19);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_301.setTransform(87.2,12.4);

	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("#FFFFFF").s().p("AgSASQgIgGgCgKIgBgFQAAgMAIgIIAAANIAFAAQAAAIAGAEQAEAGAHgBQAHAAAGgFQAEgGgBgIIAMAAIABAGQAAAMgIAJQgJAJgNAAQgKAAgIgGg");
	this.shape_302.setTransform(71.3,20.2);

	this.shape_303 = new cjs.Shape();
	this.shape_303.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_303.setTransform(67.3,13.8);

	this.shape_304 = new cjs.Shape();
	this.shape_304.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_304.setTransform(53.8,13.8);

	this.shape_305 = new cjs.Shape();
	this.shape_305.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_305.setTransform(41,15.3);

	this.shape_306 = new cjs.Shape();
	this.shape_306.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_306.setTransform(28,13.9);

	this.shape_307 = new cjs.Shape();
	this.shape_307.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_307.setTransform(18.8,6.8);

	this.shape_308 = new cjs.Shape();
	this.shape_308.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_308.setTransform(16.4,15.4);

	this.shape_309 = new cjs.Shape();
	this.shape_309.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgFIAABHg");
	this.shape_309.setTransform(4.6,19);

	this.shape_310 = new cjs.Shape();
	this.shape_310.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAGgFQgHgCABgIIAAgFQAFgNAXAAQASAAAGAKQAHgKAUAAQAJAAAHAFQAIAEABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAVgPANQgRAOgdABQgdgBgRgPgAgpgLQgFAGABAHQAAANAPAHQAMAEASABQARgBAMgEQAQgHABgNQAAgHgFgGQgFgFgIgBIg4AAQgJABgEAFgAANgkQgFACgBAHIAdAAIAAgEQAAgEgEgCQgDgDgEAAIgCAAQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_310.setTransform(4.4,13.9);

	this.shape_311 = new cjs.Shape();
	this.shape_311.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgFIAABHg");
	this.shape_311.setTransform(-14.8,19);

	this.shape_312 = new cjs.Shape();
	this.shape_312.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_312.setTransform(-15,12.6);

	this.shape_313 = new cjs.Shape();
	this.shape_313.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_313.setTransform(-27.8,15);

	this.shape_314 = new cjs.Shape();
	this.shape_314.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_314.setTransform(-47.2,12.4);

	this.shape_315 = new cjs.Shape();
	this.shape_315.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_315.setTransform(-59.8,12.4);

	this.shape_316 = new cjs.Shape();
	this.shape_316.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_316.setTransform(-73.1,13.8);

	this.shape_317 = new cjs.Shape();
	this.shape_317.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_317.setTransform(-86.1,12.3);

	this.shape_318 = new cjs.Shape();
	this.shape_318.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_318.setTransform(-94.6,13.9);

	this.shape_319 = new cjs.Shape();
	this.shape_319.graphics.f("#FFFFFF").s().p("AARA0IAAgNIAGABQAMAAAIgKQAJgLAAgMQAAgNgHgKQgHgLgMgBIgFgBQgVAAgDATQACgCAEAAQAFgBAEABQAIADADAHQAEAHgBAKQgBAQgNALQgMAKgQAAQgZAAgNgOQgKgLAAgNQAAgIAEgGQAEgIAHgEIgOAAQgDAAgCACIAAgNIAOAAQgFgBAAgJIABgGQAFgMASAAQAMAAAGAEQAIAFABAJQAKgSAYAAQATgBANARQAMARgBAUQgBAUgOAPQgNAPgTAAIgFAAgAgogGQgGAFgBAGQAAAKAIAGQAIAGAOAAQAMAAAHgEQAKgFAAgLQAAgIgIAAQgDgBgCACQgDACAAAEIgLAAIAAgKIABgIIgMAAQgIAAgGAGgAgkgmQADABABAGQAAAEgCAEIAPAAQAAgIgEgEQgEgEgGAAIgDABg");
	this.shape_319.setTransform(-103.9,13.9);

	this.shape_320 = new cjs.Shape();
	this.shape_320.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_320.setTransform(-117.1,12.4);

	this.shape_321 = new cjs.Shape();
	this.shape_321.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_321.setTransform(-125.7,6.8);

	this.shape_322 = new cjs.Shape();
	this.shape_322.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCAAgGIABgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAJAFAAAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHAAgIQgBgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAGAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgEACg");
	this.shape_322.setTransform(-130.1,13.9);

	this.shape_323 = new cjs.Shape();
	this.shape_323.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_323.setTransform(-145.3,6.8);

	this.shape_324 = new cjs.Shape();
	this.shape_324.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_324.setTransform(-149.8,13.9);

	this.shape_325 = new cjs.Shape();
	this.shape_325.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_325.setTransform(-163.1,13.8);

	this.shape_326 = new cjs.Shape();
	this.shape_326.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_326.setTransform(-174.6,13.9);

	this.shape_327 = new cjs.Shape();
	this.shape_327.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAHgNAPgFQAMgEATAAQAtAAAJAWQACADgBAEQABAOgLAEg");
	this.shape_327.setTransform(-186.3,7.3);

	this.shape_328 = new cjs.Shape();
	this.shape_328.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_328.setTransform(-187.2,13.9);

	this.shape_329 = new cjs.Shape();
	this.shape_329.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_329.setTransform(-196.9,6.8);

	this.shape_330 = new cjs.Shape();
	this.shape_330.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_330.setTransform(-201.7,13.8);

	this.shape_331 = new cjs.Shape();
	this.shape_331.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPIADAIQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_331.setTransform(-215.5,13.9);

	this.shape_332 = new cjs.Shape();
	this.shape_332.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_332.setTransform(-234.3,12.5);

	this.shape_333 = new cjs.Shape();
	this.shape_333.graphics.f("#FFFFFF").s().p("AgCBFQgXgBgNgOQgOgOgBgVQAAgTALgOQgDgBgDgEIgFgIIgBgLQgBgKAKgKQAIgKAQAAIAKABQAKADABAHIAGgJIAVAAQgFAJgHAJQgIAIgIAFQAOgBALAEQAgAMgBAkQABAXgSAQQgPAOgXAAIgCAAgAgcgMQgIALAAAMQgBANAJAIQALALARAAQAPAAAKgHQANgLAAgQQAAgHgDgGQgDgJgJgGQgKgFgMAAQgSAAgLAMgAgkgrQgDAGAAAFQgBAHAGADIAEgEIALgJQgHAAgDgDQgFgEABgEIgDADgAgXgyQAAAGAGAAQAHAAAAgGQAAgHgHAAQgGAAAAAHg");
	this.shape_333.setTransform(-245.6,12.2);

	this.shape_334 = new cjs.Shape();
	this.shape_334.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPQAAACADAGQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_334.setTransform(-258,13.9);

	this.shape_335 = new cjs.Shape();
	this.shape_335.graphics.f("#FFFFFF").s().p("AgrAAIgghWICXBWIiXBXg");
	this.shape_335.setTransform(-285.9,-89.8);

	this.shape_336 = new cjs.Shape();
	this.shape_336.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_336.setTransform(-25.7,-33.9);

	this.shape_337 = new cjs.Shape();
	this.shape_337.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_337.setTransform(-31.5,-37.8);

	this.shape_338 = new cjs.Shape();
	this.shape_338.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_338.setTransform(-40,-39.2);

	this.shape_339 = new cjs.Shape();
	this.shape_339.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgFIANAAQgEADAAAFQAAAHAKAAQAJAAATgIQAOgEALgBIAEAAQAMADAFAGIAAgjQAGgFAGgKIAABGIgMAAQgHgLgOAAQgLABgJAEQgSAHgPABQgHAAgGgCg");
	this.shape_339.setTransform(-52.8,-32.6);

	this.shape_340 = new cjs.Shape();
	this.shape_340.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_340.setTransform(-52.6,-39.2);

	this.shape_341 = new cjs.Shape();
	this.shape_341.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_341.setTransform(-67.7,-44.9);

	this.shape_342 = new cjs.Shape();
	this.shape_342.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_342.setTransform(-72.5,-37.8);

	this.shape_343 = new cjs.Shape();
	this.shape_343.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_343.setTransform(-86,-37.8);

	this.shape_344 = new cjs.Shape();
	this.shape_344.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_344.setTransform(-97.6,-37.8);

	this.shape_345 = new cjs.Shape();
	this.shape_345.graphics.f("#FFFFFF").s().p("AgFAyIgMgEQgHgCgEAAQgKAAACAHIgNAAQgCgIAAgJQAAgKACgIQAEgJAJgHIAPgMQAIgHAAgHQABgFgDgEQgCgEgGAAQgFAAgEADQgEAEAAAEQgBALAKAFIgRAAQgGgHAAgHIABgEQABgKAHgGQAHgFALAAQAKAAAIAGQAGAHAAAKQAAAJgGAIIgNAMQgLAIgDAEQgFAHAAAJIAAAEQAEgCAEAAQAFAAALADQAKADAFAAIALgBQAGgBAFgGQAGgGAAgJQABgIgGgHIgOgMQgHgIAAgJQAAgKAIgGQAGgFALAAQAIAAAHAFQAGAFAAAIQACAGgEAFQgDAFgFABQgFAAgEgDQgDgCAAgFQgBgIAIgCQgDgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgMAKgRAAQgJAAgGgCg");
	this.shape_345.setTransform(-108.1,-37.8);

	this.shape_346 = new cjs.Shape();
	this.shape_346.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgQANgIQgGgCAAgKQAAgDACgDQAGgOAVAAQASAAAGALQAIgLATAAQAJAAAJAHQAHAFAAAKQAAAHgCAEQALAKAAANQAAAWgPAMQgRAOgdABQgdgBgRgPgAgtACQABAOAPAGQANAEAQAAQASAAAMgEQAQgGAAgOQAAgFgEgGQgHAFgMADQAFACABAHQgBAGgIADQgIADgKAAQgLABgIgEQgJgDAAgIQAAgNAXAAIAKgBQAPgCADgCIg0AAQgSAAAAAOgAgKAGQAAAFALgBQALAAAAgDQgBgFgKAAQgLAAAAAEgAAMgjQgEAEgBAHIAeAAIABgEQAAgBAAgBQAAgBAAAAQgBgBAAAAQAAgBAAAAQgEgGgJAAQgHAAgFAEgAgggmQAEACABAEQAAAFgDADIAYAAQAAgLgNgEIgGgBQgEAAgDACg");
	this.shape_346.setTransform(-120,-37.7);

	this.shape_347 = new cjs.Shape();
	this.shape_347.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAJAFIgLAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADABAEQAAAOgKAEg");
	this.shape_347.setTransform(-133,-44.4);

	this.shape_348 = new cjs.Shape();
	this.shape_348.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPQAAACADAGQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_348.setTransform(-133.2,-37.8);

	this.shape_349 = new cjs.Shape();
	this.shape_349.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKAAQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_349.setTransform(-152.7,-36.4);

	this.shape_350 = new cjs.Shape();
	this.shape_350.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_350.setTransform(-165.3,-39.2);

	this.shape_351 = new cjs.Shape();
	this.shape_351.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgNgNgBgWQAAgOAGgKQAEgKANgLIAWgVIAKgPIAWAAQgFAHgFAHQgHAHgJAHQAOgBAMAEQAfAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAOAAALgIQANgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgTAAgKAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAFAEQAHAFgBAJQAAAIgHAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_351.setTransform(-177.2,-39.6);

	this.shape_352 = new cjs.Shape();
	this.shape_352.graphics.f("#FFFFFF").s().p("AgoBFIAAgTIBLAAIAAgSQgQAFgXgCQgTgBgMgLQgNgLAAgRQAAgVASgKQgCgBgCgEQgDgDAAgFIABgEQACgJALgEQAIgCAPAAQAWAAALANQAIAKAAAPIgkAAQgKAAgJAHQgHAGgBAKQgBALAKAJQAKAKANAAQAYAAAPgIIAAA2gAgNg2QADABABAFQABAGgFACIAHgBIAgAAQgDgIgKgEQgFgCgIAAIgEAAIgJABg");
	this.shape_352.setTransform(-187.9,-36.1);

	this.shape_353 = new cjs.Shape();
	this.shape_353.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_353.setTransform(-202,-37.8);

	this.shape_354 = new cjs.Shape();
	this.shape_354.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_354.setTransform(-211.6,-37.8);

	this.shape_355 = new cjs.Shape();
	this.shape_355.graphics.f("#FFFFFF").s().p("AgfA+QgOgHACgOQABgHAGgEQAGgDAIAAIAHAAIAAAKIgEAAQgIAAAAAFQAAAIAbgBQAIAAAHgCQAIgDAAgDQgBgGgOACIgMAAIAAgLQAVgBAMgJQAOgKAAgPIgBgIQgDgNgNgHQgLgGgPAAQgRAAgLAGQgOAIgBAOQAAAMAIAHQAIAHAMAAQAGAAAGgDQAGgDACgFQgFADgFAAQgIAAgGgEQgFgFgBgHQAAgKAGgGQAGgFAKAAQALAAAHAHQAHAHAAAMQgBANgKAJQgKAIgOABQgUACgNgMQgMgLAAgTQAAgYASgOQAQgMAaAAQAcAAAPAPQAOAOAAAXQAAAdgcAQQAKAFgBAJQAAALgMAGQgLAGgRABIgEAAQgQAAgLgGgAgIgVQgDADAAADQAAAEADADQACACAEAAQACAAADgDQACgCAAgEQAAgDgCgDQgDgCgCAAQgEAAgCACg");
	this.shape_355.setTransform(-225,-36.2);

	this.shape_356 = new cjs.Shape();
	this.shape_356.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgIQAEgFABgFIAABGg");
	this.shape_356.setTransform(-243.9,-32.7);

	this.shape_357 = new cjs.Shape();
	this.shape_357.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_357.setTransform(-244.6,-37.8);

	this.shape_358 = new cjs.Shape();
	this.shape_358.graphics.f("#FFFFFF").s().p("AAFBCQgEgCgDgFQgHAKgSAAQgWAAgJgYQgFgPABgXQABgQAJgLQAKgNAOAAQAHAAAEAEQAFAEgBAHQAAADgDAEQgDAEABAEQABAKAOABQAEAAAGgCQAFgDABgDQgDAAgEAAQgGAAgFgDQgFgEgBgGQgBgIAGgGQAFgGAIAAQAWABAAAYQAAAMgJAHQgIAIgMABQgKABgIgEQgJgFgDgJIgBgFQAAgHADgEQAAAAABgBQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAgBAAAAQAAgBgBAAQAAAAgBAAQgHgBgGAKQgGAJAAAKQAAANAGALQAHALALAAQARAAAAgTIALAAQgBAKAFAFQAFAEAJAAQAOAAAHgOQAGgLAAgRQAAgVgLgQQgOgSgXgBQgmAAgHAZIgNAAQADgQAQgKQAPgLAYABQAgAAARAYQAOAUAAAXQAAAdgJASQgLAWgXAAQgLAAgFgDgAAAgQQAAAHAHAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgHAAAAAHg");
	this.shape_358.setTransform(-258.2,-39.4);

	this.shape_359 = new cjs.Shape();
	this.shape_359.graphics.f("#FFFFFF").s().p("AguAXQgIgDAAgNIABgHQAIgWAuAAQAXAAAMADQARAGACANQABAJgGAGQgHAFgJAAQgIAAgGgEQgGgFAAgHQAAgEACgEIgPAAQgjAAgGAKIgBAFQAAAIAIAEgAAbABQAAADACACQACACADAAQAHAAAAgHQAAgCgCgCQgCgCgDAAQgHAAAAAGg");
	this.shape_359.setTransform(186.6,-69.4);

	this.shape_360 = new cjs.Shape();
	this.shape_360.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_360.setTransform(188.4,-61.3);

	this.shape_361 = new cjs.Shape();
	this.shape_361.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLAOQgLAOgTABQgSAAgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_361.setTransform(176.6,-61.4);

	this.shape_362 = new cjs.Shape();
	this.shape_362.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_362.setTransform(164,-64.2);

	this.shape_363 = new cjs.Shape();
	this.shape_363.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgFQAAgMAIgIIAAAOIAFAAQAAAGAGAFQAEAGAHAAQAHgBAGgFQAEgGgBgIIAMAAIABAGQAAANgJAIQgIAJgNAAQgKAAgIgGg");
	this.shape_363.setTransform(148.1,-56.5);

	this.shape_364 = new cjs.Shape();
	this.shape_364.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_364.setTransform(144,-62.8);

	this.shape_365 = new cjs.Shape();
	this.shape_365.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAghIAHgHQAEgGABgFIAABGg");
	this.shape_365.setTransform(130.3,-57.7);

	this.shape_366 = new cjs.Shape();
	this.shape_366.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_366.setTransform(130.1,-62.8);

	this.shape_367 = new cjs.Shape();
	this.shape_367.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_367.setTransform(117.5,-64.2);

	this.shape_368 = new cjs.Shape();
	this.shape_368.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_368.setTransform(104.2,-62.8);

	this.shape_369 = new cjs.Shape();
	this.shape_369.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_369.setTransform(83.9,-62.8);

	this.shape_370 = new cjs.Shape();
	this.shape_370.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAFgDQgFgDAAgIIABgFQAFgNAWAAQASAAAGALQAIgLASAAQAKABAHAEQAIAEABAIQABAGgEAGQAFADAEAIQADAIAAAIQAAAWgPANQgRAOgdgBQgdABgRgQgAgpgLQgFAGAAAIQABANAPAGQANAFARgBQARABAMgFQAPgGACgNQAAgIgFgGQgFgGgIAAIg5AAQgHAAgFAGgAANglQgFAEgBAFIAcAAIABgCQAAgFgEgCQgDgDgFABIgBgBQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgNgCIgGgBIgFABg");
	this.shape_370.setTransform(70.7,-62.7);

	this.shape_371 = new cjs.Shape();
	this.shape_371.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_371.setTransform(61.7,-62.8);

	this.shape_372 = new cjs.Shape();
	this.shape_372.graphics.f("#FFFFFF").s().p("AgFAyIgMgEQgGgCgFAAQgKAAACAHIgNAAQgCgIAAgJQAAgKACgIQAEgJAJgHIAPgMQAIgHAAgHQABgFgDgEQgCgEgGAAQgFAAgEADQgDAEgBAEQgBALAKAFIgRAAQgFgHgBgHIABgEQABgKAHgGQAIgFAKAAQAKAAAIAGQAGAHAAAKQAAAJgGAIIgNAMQgKAIgEAEQgFAHAAAJIAAAEQAEgCAEAAQAGAAAKADQAKADAFAAIALgBQAGgBAFgGQAGgGAAgJQABgIgGgHIgOgMQgHgIAAgJQAAgKAIgGQAGgFALAAQAIAAAHAFQAGAFABAIQABAGgEAFQgDAFgFABQgEAAgFgDQgDgCAAgFQgBgIAIgCQgDgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgMAKgRAAQgJAAgGgCg");
	this.shape_372.setTransform(54,-62.8);

	this.shape_373 = new cjs.Shape();
	this.shape_373.graphics.f("#FFFFFF").s().p("AgCA5QgHAMgRAAQgRAAgKgPQgIgOAAgXQABgZAPgOQAMgKAYgBIAIAAIAAANIgSAAQgJABgFAEQgMAJAAARQgBANAFAJQAFAKAKAAQAKAAAEgGQAFgEgBgIIAAgQIAMAAIAAAQQAAASATAAQAMAAAGgPQAGgNgBgUQAAgUgLgNQgNgSgYAAQgOAAgLAFQgLAGgGAJIgNAAQAEgOAPgJQAPgIAVgBQAPAAANAHQALAFAGAGQATAVAAAjQAAAbgKARQgLASgUABQgQAAgHgMg");
	this.shape_373.setTransform(42.1,-64.4);

	this.shape_374 = new cjs.Shape();
	this.shape_374.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_374.setTransform(29.1,-61.7);

	this.shape_375 = new cjs.Shape();
	this.shape_375.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_375.setTransform(13.8,-69.9);

	this.shape_376 = new cjs.Shape();
	this.shape_376.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLAOQgLAOgTABQgSAAgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_376.setTransform(9.8,-61.4);

	this.shape_377 = new cjs.Shape();
	this.shape_377.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCAAgGIABgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAJAFAAAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHAAgIQgBgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAGAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgEACg");
	this.shape_377.setTransform(-3.2,-62.8);

	this.shape_378 = new cjs.Shape();
	this.shape_378.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_378.setTransform(-12.2,-62.8);

	this.shape_379 = new cjs.Shape();
	this.shape_379.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAGgDQgHgDAAgIIABgFQAGgNAWAAQARAAAHALQAHgLATAAQAKABAHAEQAIAEABAIQABAGgDAGQAEADAEAIQADAIAAAIQAAAWgQANQgQAOgdgBQgdABgRgQgAgpgLQgFAGAAAIQABANAQAGQAMAFARgBQARABAMgFQAPgGABgNQABgIgFgGQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFAEgBAFIAcAAIABgCQAAgFgDgCQgEgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQAAgJgNgCIgGgBIgFABg");
	this.shape_379.setTransform(-21,-62.7);

	this.shape_380 = new cjs.Shape();
	this.shape_380.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_380.setTransform(-36.2,-69.9);

	this.shape_381 = new cjs.Shape();
	this.shape_381.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_381.setTransform(-41.2,-62.8);

	this.shape_382 = new cjs.Shape();
	this.shape_382.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_382.setTransform(-54.7,-64.2);

	this.shape_383 = new cjs.Shape();
	this.shape_383.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgFIANAAQgEAEAAAEQAAAHAKgBQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgKIAABGIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHAAgGgBg");
	this.shape_383.setTransform(-67.5,-57.6);

	this.shape_384 = new cjs.Shape();
	this.shape_384.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_384.setTransform(-67.3,-64.2);

	this.shape_385 = new cjs.Shape();
	this.shape_385.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_385.setTransform(-82.4,-69.9);

	this.shape_386 = new cjs.Shape();
	this.shape_386.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_386.setTransform(-87.2,-62.8);

	this.shape_387 = new cjs.Shape();
	this.shape_387.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_387.setTransform(-100.7,-62.8);

	this.shape_388 = new cjs.Shape();
	this.shape_388.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_388.setTransform(-112.3,-62.8);

	this.shape_389 = new cjs.Shape();
	this.shape_389.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_389.setTransform(-124,-61.7);

	this.shape_390 = new cjs.Shape();
	this.shape_390.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgFIANAAQgEAEAAAEQAAAHAKgBQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgKIAABGIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHAAgGgBg");
	this.shape_390.setTransform(-143.5,-57.6);

	this.shape_391 = new cjs.Shape();
	this.shape_391.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_391.setTransform(-143.3,-64.2);

	this.shape_392 = new cjs.Shape();
	this.shape_392.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_392.setTransform(-151.9,-69.9);

	this.shape_393 = new cjs.Shape();
	this.shape_393.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAQAAAPABAFQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_393.setTransform(-156.7,-62.8);

	this.shape_394 = new cjs.Shape();
	this.shape_394.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgNIA7AAIAAAMIgNAKIgNALIgKAOQgFAIAAAIQABAGAEADQAEAEAEAAQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBAMgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_394.setTransform(-167.3,-62.6);

	this.shape_395 = new cjs.Shape();
	this.shape_395.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_395.setTransform(-175.5,-61.3);

	this.shape_396 = new cjs.Shape();
	this.shape_396.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_396.setTransform(-189.8,-69.9);

	this.shape_397 = new cjs.Shape();
	this.shape_397.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_397.setTransform(-194.6,-62.8);

	this.shape_398 = new cjs.Shape();
	this.shape_398.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_398.setTransform(-208,-64.2);

	this.shape_399 = new cjs.Shape();
	this.shape_399.graphics.f("#FFFFFF").s().p("AgoBFIAAgTIBLAAIAAgSQgPAFgYgCQgTgBgMgLQgNgLAAgRQAAgVASgKQgCgBgCgEQgDgDAAgFIABgEQACgJALgEQAIgCAPAAQAWAAALANQAIAKAAAPIgkAAQgKAAgJAHQgHAGgBAKQgBALAKAJQALAKAMAAQAYAAAPgIIAAA2gAgNg2QADABABAFQABAGgFACIAHgBIAgAAQgCgIgLgEQgFgCgIAAIgEAAIgJABg");
	this.shape_399.setTransform(-219.3,-61.1);

	this.shape_400 = new cjs.Shape();
	this.shape_400.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_400.setTransform(-226.8,-69.9);

	this.shape_401 = new cjs.Shape();
	this.shape_401.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_401.setTransform(-231.3,-62.8);

	this.shape_402 = new cjs.Shape();
	this.shape_402.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_402.setTransform(-244.9,-62.8);

	this.shape_403 = new cjs.Shape();
	this.shape_403.graphics.f("#FFFFFF").s().p("AgrAwQgUgPABgYQAAgVANgNQAHgHALgEIgCgDQgCgCAAgEQAAgEACgCQAFgJAMgCIAVgBQATAAANAJQANAJACASIg0AAQgRAAgKAKQgJAKgBAPQAAAQANANQANAMAPAAQANgBALgEQAMgEAFgIQAIgLgCgKIAMAAIABAFQAAAKgGAJQgIAOgPAHQgPAIgSAAQgbAAgSgQgAgKgwQADABAAADQAAAEgCABIAqAAQgHgJgPgCIgKgBQgGAAgFADg");
	this.shape_403.setTransform(-258.2,-61.6);

	this.shape_404 = new cjs.Shape();
	this.shape_404.graphics.f("#FFFFFF").s().p("AAAA7QgIAMgRAAQgOAAgKgLQgJgMABgPQABgOAKgJIgPAAIAAgLIAVAAQgEgBAAgIIABgGQAGgLAXAAQAOAAAIAJQAJAKAAANIgBAFIgoAAQgIAAgFAFQgEAFAAAHQAAAIAFAFQAGAGAHAAQAHAAAFgEQAFgDAAgHIAAgOIAMAAIAAAOQAAAHAGAEQAGADAHAAQALAAAHgMQAFgKAAgOQAAgSgMgMQgLgLgPAAIguAAQgQAAAAgOQAAgZA0ABQAcAAANAMQAKAJAAAMIgMAAQAAgHgIgGQgLgIgUAAQgfAAAAAJQAAAFALAAIAhAAQASAAAPAPQAQAQAAAdQAAAVgJAQQgLARgSAAQgRAAgHgMgAgVgOQADACABAEQABAEgDADIAVAAQgBgGgDgEQgEgEgIAAIgHABg");
	this.shape_404.setTransform(176.8,-89.7);

	this.shape_405 = new cjs.Shape();
	this.shape_405.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_405.setTransform(167.2,-87.6);

	this.shape_406 = new cjs.Shape();
	this.shape_406.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_406.setTransform(159,-86.3);

	this.shape_407 = new cjs.Shape();
	this.shape_407.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgLAAQgLgDAAgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADABAEQgBAOgJAEg");
	this.shape_407.setTransform(140.5,-94.4);

	this.shape_408 = new cjs.Shape();
	this.shape_408.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_408.setTransform(140,-87.8);

	this.shape_409 = new cjs.Shape();
	this.shape_409.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_409.setTransform(127.4,-89.2);

	this.shape_410 = new cjs.Shape();
	this.shape_410.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_410.setTransform(116.6,-87.8);

	this.shape_411 = new cjs.Shape();
	this.shape_411.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_411.setTransform(104.4,-87.8);

	this.shape_412 = new cjs.Shape();
	this.shape_412.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_412.setTransform(92.2,-87.8);

	this.shape_413 = new cjs.Shape();
	this.shape_413.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_413.setTransform(74.1,-81.9);

	this.shape_414 = new cjs.Shape();
	this.shape_414.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_414.setTransform(73.2,-87.8);

	this.shape_415 = new cjs.Shape();
	this.shape_415.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgDIANAAQgEACAAAGQAAAFAKAAQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgLIAABHIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHAAgGgCg");
	this.shape_415.setTransform(59.4,-82.6);

	this.shape_416 = new cjs.Shape();
	this.shape_416.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgGgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_416.setTransform(59.1,-87.8);

	this.shape_417 = new cjs.Shape();
	this.shape_417.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_417.setTransform(39.8,-81.9);

	this.shape_418 = new cjs.Shape();
	this.shape_418.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_418.setTransform(38.9,-87.8);

	this.shape_419 = new cjs.Shape();
	this.shape_419.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_419.setTransform(29.2,-94.9);

	this.shape_420 = new cjs.Shape();
	this.shape_420.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_420.setTransform(24.7,-87.8);

	this.shape_421 = new cjs.Shape();
	this.shape_421.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_421.setTransform(11.5,-87.8);

	this.shape_422 = new cjs.Shape();
	this.shape_422.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_422.setTransform(-7.8,-89.3);

	this.shape_423 = new cjs.Shape();
	this.shape_423.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_423.setTransform(-20.3,-89.2);

	this.shape_424 = new cjs.Shape();
	this.shape_424.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_424.setTransform(-33,-94.4);

	this.shape_425 = new cjs.Shape();
	this.shape_425.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAIAFACAJQABAJgEAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQAAAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGgBAHQABAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_425.setTransform(-33.3,-87.8);

	this.shape_426 = new cjs.Shape();
	this.shape_426.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_426.setTransform(-52.4,-86.4);

	this.shape_427 = new cjs.Shape();
	this.shape_427.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_427.setTransform(-65.8,-87.8);

	this.shape_428 = new cjs.Shape();
	this.shape_428.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_428.setTransform(-81.9,-87.8);

	this.shape_429 = new cjs.Shape();
	this.shape_429.graphics.f("#FFFFFF").s().p("AgyA1QgKgLABgRQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_429.setTransform(-90.6,-89.1);

	this.shape_430 = new cjs.Shape();
	this.shape_430.graphics.f("#FFFFFF").s().p("AgSASQgIgGgCgKIgBgEQAAgNAIgIIAAAOIAFAAQAAAGAGAFQAEAFAHABQAHAAAGgHQAEgFgBgIIAMAAIABAGQAAANgIAJQgJAIgNAAQgKAAgIgGg");
	this.shape_430.setTransform(-100.2,-81.5);

	this.shape_431 = new cjs.Shape();
	this.shape_431.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_431.setTransform(-104.1,-87.8);

	this.shape_432 = new cjs.Shape();
	this.shape_432.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_432.setTransform(-120.2,-87.8);

	this.shape_433 = new cjs.Shape();
	this.shape_433.graphics.f("#FFFFFF").s().p("AgyA1QgKgLABgRQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_433.setTransform(-129,-89.1);

	this.shape_434 = new cjs.Shape();
	this.shape_434.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_434.setTransform(-137.7,-94.9);

	this.shape_435 = new cjs.Shape();
	this.shape_435.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_435.setTransform(-142.2,-87.8);

	this.shape_436 = new cjs.Shape();
	this.shape_436.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_436.setTransform(-151.6,-87.8);

	this.shape_437 = new cjs.Shape();
	this.shape_437.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_437.setTransform(-160.8,-87.8);

	this.shape_438 = new cjs.Shape();
	this.shape_438.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_438.setTransform(-172.9,-87.8);

	this.shape_439 = new cjs.Shape();
	this.shape_439.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_439.setTransform(-186.9,-94.9);

	this.shape_440 = new cjs.Shape();
	this.shape_440.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_440.setTransform(-187.2,-87.8);

	this.shape_441 = new cjs.Shape();
	this.shape_441.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_441.setTransform(-195.1,-89.2);

	this.shape_442 = new cjs.Shape();
	this.shape_442.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_442.setTransform(-205.9,-87.8);

	this.shape_443 = new cjs.Shape();
	this.shape_443.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgOAMgGIgVAAIAAgKIARAAQgDgEABgHQACgJAKgDQAIgFALABQAOABAIALQAJAMAAANIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAGQAKAFASgBQAqAAAAgoQAAgMgKgMQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQASIAEAGQABAFAAAEIgOAAQAIADAHANQAGANABAMQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADABABADQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAEIAHAEQABgEgHgGQgJgIgYAAQgcABAAAGg");
	this.shape_443.setTransform(-217.4,-89.4);

	this.shape_444 = new cjs.Shape();
	this.shape_444.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAIAFIgKAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADAAAEQABAOgKAEg");
	this.shape_444.setTransform(-230.1,-94.4);

	this.shape_445 = new cjs.Shape();
	this.shape_445.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_445.setTransform(-230.1,-87.8);

	this.shape_446 = new cjs.Shape();
	this.shape_446.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_446.setTransform(-239.1,-87.8);

	this.shape_447 = new cjs.Shape();
	this.shape_447.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCgBgGIACgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAIAFABAJQABAJgDAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAIAAAHgGQAHgHAAgIQgBgHgGgFQgFgGgIAAIg1AAQgIAAgHAGQgGAGgBAHQAAAIAHAGQAFAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgCgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_447.setTransform(-248.1,-87.8);

	this.shape_448 = new cjs.Shape();
	this.shape_448.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_448.setTransform(-259.6,-87.8);

	this.shape_449 = new cjs.Shape();
	this.shape_449.graphics.f("#FFFFFF").s().p("AgrAAIgghWICXBWIiXBXg");
	this.shape_449.setTransform(-285.9,-188.8);

	this.shape_450 = new cjs.Shape();
	this.shape_450.graphics.f("#FFFFFF").s().p("AgIAMQgGgFAAgHQAAgEADgEQAFgGAGAAQAFAAAEADQAGAFAAAGQAAAFgDAEQgFAGgHAAQgEAAgEgDg");
	this.shape_450.setTransform(28.2,-132.9);

	this.shape_451 = new cjs.Shape();
	this.shape_451.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_451.setTransform(18.5,-143.4);

	this.shape_452 = new cjs.Shape();
	this.shape_452.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_452.setTransform(18.5,-136.8);

	this.shape_453 = new cjs.Shape();
	this.shape_453.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_453.setTransform(9.4,-136.8);

	this.shape_454 = new cjs.Shape();
	this.shape_454.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_454.setTransform(0.7,-136.8);

	this.shape_455 = new cjs.Shape();
	this.shape_455.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_455.setTransform(-11.4,-138.2);

	this.shape_456 = new cjs.Shape();
	this.shape_456.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_456.setTransform(-23.3,-138.2);

	this.shape_457 = new cjs.Shape();
	this.shape_457.graphics.f("#FFFFFF").s().p("AgOA0IgJgCIAAgLIAFABQAEAAAAgGQAAgDgFgGQgGgHAAgHQABgIAEgEQAFgEAHgCQAHgBAEADQgBgPgCgGQgCgLgHgCQgIgBgEADIAAgLQAGgEALABQAPABAIARQAGAOABATQAAAWgLAPQgLAPgPAAIgDAAgAgGAFQgFABAAAHIABADIAEAIQADAFAAAEQADgFABgJQACgGgBgGQgCgDgDAAIgDABg");
	this.shape_457.setTransform(-32.3,-136.8);

	this.shape_458 = new cjs.Shape();
	this.shape_458.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgDAAQgfAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_458.setTransform(-42.5,-136.8);

	this.shape_459 = new cjs.Shape();
	this.shape_459.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_459.setTransform(-52.4,-136.8);

	this.shape_460 = new cjs.Shape();
	this.shape_460.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_460.setTransform(-61.1,-138.3);

	this.shape_461 = new cjs.Shape();
	this.shape_461.graphics.f("#FFFFFF").s().p("AgCBGQgXgBgNgOQgOgNAAgWQAAgOAFgKQAFgKAMgLIAYgVIAJgPIAVAAQgDAHgHAHQgGAHgJAHQAPgBAKAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAOAAALgIQANgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgSAAgLAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAFAEQAHAFAAAJQAAAIgIAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_461.setTransform(-73.1,-138.6);

	this.shape_462 = new cjs.Shape();
	this.shape_462.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAJAFIgLAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADABAEQAAAOgKAEg");
	this.shape_462.setTransform(-85.3,-143.4);

	this.shape_463 = new cjs.Shape();
	this.shape_463.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_463.setTransform(-85.6,-136.8);

	this.shape_464 = new cjs.Shape();
	this.shape_464.graphics.f("#FFFFFF").s().p("AgoA4QgRgRAAgYQAAgPAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAJQAAAOgJAJQgJAKgMgBQgLAAgIgGQgJgHAAgKIADgPQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQARAAANgMQAMgMAAgSQAAgOgFgKQgIgLgNAAIhFAAIAAgKQASABABgCIgFgDQgDgBAAgEQAAgOAPgEQAIgCASgBQAQABAJADQANADAJAKQAHAHgBAMIgLAAQgCgLgNgGQgKgFgPAAQgLAAgFABQgHACAAAHQAAABAAAAQAAAAAAABQAAAAABABQAAAAABAAQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHADQAYALAAAlQAAAbgRASQgQARgZAAQgYAAgQgPgAgOACQAAAGAEAEQAFAEAGAAQAGABAFgFQAFgEAAgHQgFAIgJgBQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQgBABAAAAQAAABABABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_464.setTransform(-104.8,-138.7);

	this.shape_465 = new cjs.Shape();
	this.shape_465.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_465.setTransform(-116,-136.8);

	this.shape_466 = new cjs.Shape();
	this.shape_466.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_466.setTransform(-127.9,-136.8);

	this.shape_467 = new cjs.Shape();
	this.shape_467.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_467.setTransform(-137.3,-136.8);

	this.shape_468 = new cjs.Shape();
	this.shape_468.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_468.setTransform(-146,-138.3);

	this.shape_469 = new cjs.Shape();
	this.shape_469.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRAMgIIgQAAIAAgMIAQAAQgDgCgBgGIACgFQAFgPAWAAQATAAAGAMQAIgMARAAQALAAAHAEQAIAFABAJQABAJgDAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAIAAAHgGQAHgHAAgIQgBgHgGgFQgFgGgIAAIg1AAQgIAAgHAGQgGAGgBAHQAAAIAHAGQAFAGAKAAQAHAAAGgDQAGgDABgGIAAgMgAAOgiQgFAEAAAHIAcAAQACgDAAgEQgCgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_469.setTransform(-159,-136.8);

	this.shape_470 = new cjs.Shape();
	this.shape_470.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_470.setTransform(-174.4,-136.8);

	this.shape_471 = new cjs.Shape();
	this.shape_471.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_471.setTransform(-183.2,-138.1);

	this.shape_472 = new cjs.Shape();
	this.shape_472.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_472.setTransform(-191.9,-143.9);

	this.shape_473 = new cjs.Shape();
	this.shape_473.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_473.setTransform(-196.4,-136.8);

	this.shape_474 = new cjs.Shape();
	this.shape_474.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_474.setTransform(-205.8,-136.8);

	this.shape_475 = new cjs.Shape();
	this.shape_475.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_475.setTransform(-215,-136.8);

	this.shape_476 = new cjs.Shape();
	this.shape_476.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_476.setTransform(-227.1,-136.8);

	this.shape_477 = new cjs.Shape();
	this.shape_477.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_477.setTransform(-245.5,-136.8);

	this.shape_478 = new cjs.Shape();
	this.shape_478.graphics.f("#FFFFFF").s().p("AgqA3QgSgNABgWQABgUAQgGQgIgCAAgJQAAgLALgFQAJgFALABQAOABAIALQAJALAAAOIgkAAQgIAAgFAEQgGAFAAAIQABASAYAFQAIACAKgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAOgIATAAQAbAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgaAAgQgMgAgcgYQACACACAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgIAAIgHABg");
	this.shape_478.setTransform(-258.5,-138.2);

	this.shape_479 = new cjs.Shape();
	this.shape_479.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_479.setTransform(171.2,-168.9);

	this.shape_480 = new cjs.Shape();
	this.shape_480.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_480.setTransform(166.4,-161.8);

	this.shape_481 = new cjs.Shape();
	this.shape_481.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_481.setTransform(152.9,-161.8);

	this.shape_482 = new cjs.Shape();
	this.shape_482.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_482.setTransform(139.6,-161.8);

	this.shape_483 = new cjs.Shape();
	this.shape_483.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_483.setTransform(125.5,-161.8);

	this.shape_484 = new cjs.Shape();
	this.shape_484.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_484.setTransform(111.8,-161.8);

	this.shape_485 = new cjs.Shape();
	this.shape_485.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_485.setTransform(102.7,-161.8);

	this.shape_486 = new cjs.Shape();
	this.shape_486.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_486.setTransform(93.9,-160.7);

	this.shape_487 = new cjs.Shape();
	this.shape_487.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_487.setTransform(74.1,-161.8);

	this.shape_488 = new cjs.Shape();
	this.shape_488.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_488.setTransform(62.6,-160.3);

	this.shape_489 = new cjs.Shape();
	this.shape_489.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_489.setTransform(54.5,-161.8);

	this.shape_490 = new cjs.Shape();
	this.shape_490.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAGgDQgHgDABgIIAAgFQAFgNAXAAQASAAAGALQAHgLAUAAQAJABAHAEQAIAEABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAWgPANQgRAOgdgBQgdABgRgQgAgpgLQgFAGABAIQAAANAPAGQAMAFASgBQARABAMgFQAQgGABgNQAAgIgFgGQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFAEgBAFIAdAAIAAgCQAAgFgEgCQgDgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_490.setTransform(45.8,-161.7);

	this.shape_491 = new cjs.Shape();
	this.shape_491.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_491.setTransform(37,-168.9);

	this.shape_492 = new cjs.Shape();
	this.shape_492.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgQANgIQgGgCAAgJQAAgEABgDQAHgOAVAAQASAAAGALQAIgLATAAQAKABAIAFQAHAGABAKQgBAHgCAEQALAKAAANQAAAWgQANQgQAOgdgBQgdABgRgQgAgtADQABANAQAGQAMAFAQgBQASABAMgFQAQgGAAgNQAAgHgEgFQgGAEgNAEQAFACAAAHQABAGgKADQgHADgKABQgLAAgIgEQgJgEAAgHQAAgNAXAAIALgBQAOgCADgCIg0AAQgTgBABAQgAgJAGQAAAFAKgBQAKAAAAgEQABgEgMAAQgKAAABAEgAAMgjQgEAFAAAGIAdAAIABgFQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBAAAAQgEgGgIAAQgIAAgFAEgAgggmQAEACABAEQAAAEgCAEIAXAAQAAgLgNgEIgGgBQgEAAgDACg");
	this.shape_492.setTransform(32.8,-161.7);

	this.shape_493 = new cjs.Shape();
	this.shape_493.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgPAAgLADQgNADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADgBAEQABAOgLAEg");
	this.shape_493.setTransform(19.9,-168.4);

	this.shape_494 = new cjs.Shape();
	this.shape_494.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_494.setTransform(19.5,-161.8);

	this.shape_495 = new cjs.Shape();
	this.shape_495.graphics.f("#FFFFFF").s().p("AgrA2QgQgPACgbQABgQAMgMQAMgLATAAIASAAIAAAMIgQAAQgNAAgJAHQgJAHAAAMQAAAPANAJQAMAJARgBQAUgBAKgMQALgNAAgSIAAgHQgDgYgQgNQgLAMgSABQgSABgHgLQgCgCAAgEQAAgFACgDIgHAAIAAgMIANAAQAeAAARAIQATAJAJAUQAKATgBAXQgBAZgQAQQgRARgZAAQgaAAgQgPgAgSg1QAAADADADQAEACADAAQAMABAFgHIgNgEIgNgBIgBADg");
	this.shape_495.setTransform(0.3,-163.4);

	this.shape_496 = new cjs.Shape();
	this.shape_496.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_496.setTransform(-11.4,-163.2);

	this.shape_497 = new cjs.Shape();
	this.shape_497.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgNgNgBgWQAAgOAGgKQAEgKANgLIAWgVIAKgPIAWAAQgFAHgFAHQgHAHgJAHQAOgBAMAEQAfAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAOAAALgIQANgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgTAAgKAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAFAEQAHAFgBAJQAAAIgHAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_497.setTransform(-29.2,-163.6);

	this.shape_498 = new cjs.Shape();
	this.shape_498.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_498.setTransform(-40.9,-163.2);

	this.shape_499 = new cjs.Shape();
	this.shape_499.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_499.setTransform(-49.5,-168.9);

	this.shape_500 = new cjs.Shape();
	this.shape_500.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_500.setTransform(-49.8,-161.8);

	this.shape_501 = new cjs.Shape();
	this.shape_501.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_501.setTransform(-58.6,-163.3);

	this.shape_502 = new cjs.Shape();
	this.shape_502.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_502.setTransform(-70,-161.8);

	this.shape_503 = new cjs.Shape();
	this.shape_503.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_503.setTransform(-88.5,-161.8);

	this.shape_504 = new cjs.Shape();
	this.shape_504.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAJAFAAAJQABAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_504.setTransform(-102.2,-161.8);

	this.shape_505 = new cjs.Shape();
	this.shape_505.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAHgNAPgFQAMgEATAAQAtAAAJAWQACADgBAEQABAOgLAEg");
	this.shape_505.setTransform(-121.5,-168.4);

	this.shape_506 = new cjs.Shape();
	this.shape_506.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_506.setTransform(-119.8,-160.3);

	this.shape_507 = new cjs.Shape();
	this.shape_507.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgNIA7AAIAAAMIgNAKIgNALIgKAOQgFAIAAAIQABAGAEADQAEAEAEAAQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBAMgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_507.setTransform(-128.7,-161.6);

	this.shape_508 = new cjs.Shape();
	this.shape_508.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAGgDQgHgDABgIIAAgFQAFgNAXAAQASAAAGALQAHgLAUAAQAJABAHAEQAIAEABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAWgPANQgRAOgdgBQgdABgRgQgAgpgLQgFAGABAIQAAANAPAGQAMAFASgBQARABAMgFQAQgGABgNQAAgIgFgGQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFAEgBAFIAdAAIAAgCQAAgFgEgCQgDgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_508.setTransform(-138.7,-161.7);

	this.shape_509 = new cjs.Shape();
	this.shape_509.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgNIA7AAIAAAMIgNAKIgNALIgKAOQgFAIAAAIQABAGAEADQAEAEAEAAQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBAMgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_509.setTransform(-148.6,-161.6);

	this.shape_510 = new cjs.Shape();
	this.shape_510.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_510.setTransform(-157.7,-163.2);

	this.shape_511 = new cjs.Shape();
	this.shape_511.graphics.f("#FFFFFF").s().p("AgSASQgIgGgDgKIAAgFQAAgMAIgIIAAAOIAFAAQAAAGAFAFQAFAGAHAAQAHgBAGgFQAEgGgBgIIANAAIAAAGQAAANgIAIQgJAJgNAAQgKAAgIgGg");
	this.shape_511.setTransform(-166.7,-155.5);

	this.shape_512 = new cjs.Shape();
	this.shape_512.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_512.setTransform(-170.8,-161.8);

	this.shape_513 = new cjs.Shape();
	this.shape_513.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_513.setTransform(-184.6,-161.8);

	this.shape_514 = new cjs.Shape();
	this.shape_514.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_514.setTransform(-203.6,-163.2);

	this.shape_515 = new cjs.Shape();
	this.shape_515.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_515.setTransform(-216.9,-161.8);

	this.shape_516 = new cjs.Shape();
	this.shape_516.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_516.setTransform(-230.5,-155.9);

	this.shape_517 = new cjs.Shape();
	this.shape_517.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgJAGgDQgHgDABgIIAAgFQAFgNAXAAQASAAAGALQAHgLAUAAQAJABAHAEQAIAEABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAWgPANQgRAOgdgBQgdABgRgQgAgpgLQgFAGABAIQAAANAPAGQAMAFASgBQARABAMgFQAQgGABgNQAAgIgFgGQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFAEgBAFIAdAAIAAgCQAAgFgEgCQgDgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_517.setTransform(-230.5,-161.7);

	this.shape_518 = new cjs.Shape();
	this.shape_518.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_518.setTransform(-239.3,-168.9);

	this.shape_519 = new cjs.Shape();
	this.shape_519.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_519.setTransform(-239.6,-161.8);

	this.shape_520 = new cjs.Shape();
	this.shape_520.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLAOQgLAOgTABQgSAAgKgMQgMgMACgQQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_520.setTransform(-248.2,-160.4);

	this.shape_521 = new cjs.Shape();
	this.shape_521.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_521.setTransform(-259.6,-161.8);

	this.shape_522 = new cjs.Shape();
	this.shape_522.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_522.setTransform(168.2,-186.8);

	this.shape_523 = new cjs.Shape();
	this.shape_523.graphics.f("#FFFFFF").s().p("AgyA1QgKgLABgRQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_523.setTransform(159.4,-188.1);

	this.shape_524 = new cjs.Shape();
	this.shape_524.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_524.setTransform(150.7,-193.9);

	this.shape_525 = new cjs.Shape();
	this.shape_525.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_525.setTransform(146.2,-186.8);

	this.shape_526 = new cjs.Shape();
	this.shape_526.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_526.setTransform(136.8,-186.8);

	this.shape_527 = new cjs.Shape();
	this.shape_527.graphics.f("#FFFFFF").s().p("AAPAoQALABAIgFQAJgFADgKQAEgKgBgKQgBgMgIgIQgIgKgMABQgTABgEARQADgDAEAAQAGAAAEADQALAIAAASQgBAQgOAKQgNAKgTAAQgTAAgNgLQgMgMAAgSQAAgSAOgKQgEAAgDgFQgCgEAAgFQAAgJAIgGQAHgFAKAAQAKAAAGAGQAHAFACAJQALgTAYgBQATAAAMAQQAMAQAAAUQAAAWgOAPQgPAOgXAAgAgtgGQgFAGAAAGQAAALAKAGQAIAFANAAQALAAAIgFQAIgFABgIQABgEgDgEQgCgCgDAAQgCAAgDACQgCACAAADIgLAAIgBgKIABgIIgPAAQgIAAgGAFgAgqglQADABACAFQACAFgCAEIANAAQgBgIgEgEQgDgEgGAAIgEABg");
	this.shape_527.setTransform(127.6,-186.8);

	this.shape_528 = new cjs.Shape();
	this.shape_528.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_528.setTransform(115.5,-186.8);

	this.shape_529 = new cjs.Shape();
	this.shape_529.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_529.setTransform(101.5,-193.9);

	this.shape_530 = new cjs.Shape();
	this.shape_530.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_530.setTransform(101.2,-186.8);

	this.shape_531 = new cjs.Shape();
	this.shape_531.graphics.f("#FFFFFF").s().p("AgDBCQgXgBgNgOQgOgNAAgWQgBgOAGgKQAFgKAMgLIAXgVIAKgPIAVAAQgEAHgGAHQgGAHgKAHQAPgBALAEQAfALAAAlQAAAXgRAPQgPAOgWAAIgDAAgAgdgOQgJAKAAANQAAAMAJAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgKgFQgJgFgLAAQgTAAgLAMg");
	this.shape_531.setTransform(93.3,-188.2);

	this.shape_532 = new cjs.Shape();
	this.shape_532.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_532.setTransform(82.5,-186.8);

	this.shape_533 = new cjs.Shape();
	this.shape_533.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgOAMgGIgVAAIAAgKIARAAQgDgEABgHQACgJAKgDQAIgFALABQAOABAIALQAJAMAAANIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAGQAKAFASgBQAqAAAAgoQAAgMgKgMQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQASIAEAGQABAFAAAEIgOAAQAIADAHANQAGANABAMQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADABABADQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAEIAHAEQABgEgHgGQgJgIgYAAQgcABAAAGg");
	this.shape_533.setTransform(71,-188.4);

	this.shape_534 = new cjs.Shape();
	this.shape_534.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_534.setTransform(58.3,-193.4);

	this.shape_535 = new cjs.Shape();
	this.shape_535.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_535.setTransform(58.3,-186.8);

	this.shape_536 = new cjs.Shape();
	this.shape_536.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_536.setTransform(49.3,-186.8);

	this.shape_537 = new cjs.Shape();
	this.shape_537.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgIAAIg2AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_537.setTransform(40.3,-186.8);

	this.shape_538 = new cjs.Shape();
	this.shape_538.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_538.setTransform(28.8,-186.8);

	this.shape_539 = new cjs.Shape();
	this.shape_539.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_539.setTransform(12.5,-185.3);

	this.shape_540 = new cjs.Shape();
	this.shape_540.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_540.setTransform(0.7,-185.4);

	this.shape_541 = new cjs.Shape();
	this.shape_541.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_541.setTransform(-19.3,-186.8);

	this.shape_542 = new cjs.Shape();
	this.shape_542.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDgBgHIABgGQAFgMAXAAQARAAAHAKQAHgKATAAQAKAAAHADQAIAFABAIQABAGgDAGQAEAEAEAHQADAIAAAIQAAAWgQANQgQANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABANAQAGQAMAFARgBQARABAMgFQAPgGABgNQABgJgFgFQgFgGgIABIg5AAQgIgBgEAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgDgDQgEgCgFgBIgBAAQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgNgDIgGAAIgFABg");
	this.shape_542.setTransform(-33,-186.7);

	this.shape_543 = new cjs.Shape();
	this.shape_543.graphics.f("#FFFFFF").s().p("AgrAxQgUgQAAgYQABgWANgMQAHgHALgEIgCgDQgCgDAAgDQAAgEACgCQAFgJAMgCIAVgBQATAAANAJQANAJACASIg0AAQgRAAgKAKQgJAKgBAPQAAAQANANQANAMAPAAQANAAALgFQALgEAGgIQAIgLgCgJIAMAAIABAEQAAAJgGAKQgIAOgPAIQgPAHgSAAQgbAAgSgPgAgKgwQADACAAACQAAADgCACIAqAAQgHgJgPgCIgKgBQgGAAgFADg");
	this.shape_543.setTransform(-45.9,-185.6);

	this.shape_544 = new cjs.Shape();
	this.shape_544.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_544.setTransform(-66,-186.8);

	this.shape_545 = new cjs.Shape();
	this.shape_545.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_545.setTransform(-79.5,-188.3);

	this.shape_546 = new cjs.Shape();
	this.shape_546.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_546.setTransform(-92.5,-186.8);

	this.shape_547 = new cjs.Shape();
	this.shape_547.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgJAAgGgDg");
	this.shape_547.setTransform(-108.3,-186.8);

	this.shape_548 = new cjs.Shape();
	this.shape_548.graphics.f("#FFFFFF").s().p("AgyA1QgKgLABgRQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_548.setTransform(-117.1,-188.1);

	this.shape_549 = new cjs.Shape();
	this.shape_549.graphics.f("#FFFFFF").s().p("AgSASQgIgGgDgKIAAgEQAAgNAIgIIAAAOIAFAAQAAAGAFAFQAFAFAHABQAHAAAGgHQAEgFgBgIIANAAIAAAGQAAANgIAJQgJAIgNAAQgKAAgIgGg");
	this.shape_549.setTransform(-126.6,-180.5);

	this.shape_550 = new cjs.Shape();
	this.shape_550.graphics.f("#FFFFFF").s().p("AgSAtQgHAHgQAAQgKAAgGgGQgHgGAAgJQAAgLAEgHIgIAAIAAgIIAHgHQAEgGAFgEIgDAAQgFAAgDgEQgEgEAAgGQAAgJAIgHQAHgIAJgBQASgBAHAMQARgMAVABQAVACAOAPQAOAOAAAUQABAigaAPQgFADgLAAIgKAAIAAgIQgIAIgKAAIgBAAQgKAAgHgHgAgIgfQAQACAKALQAMAOAAAQQAAARgJAKIAFABQAIAAAIgHQAHgGACgKIAAgHQAAgUgNgMQgLgKgVAAIgOABgAghgNQgKAIgIAJIAHAAQgEAHAAAIQABADADADQAEADAFAAQAIAAACgIQACgGAAgOIALAAIABAUQACAIAIgBQAGAAAEgGQAEgGAAgIQAAgJgGgIQgGgIgHgBIgGAAQgOAAgHAGgAgvgjQAEABABAEQABAFgCAEIAHgGIAGgGQgBgFgHAAIgCAAQgGAAgBADg");
	this.shape_550.setTransform(-130.6,-186.8);

	this.shape_551 = new cjs.Shape();
	this.shape_551.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_551.setTransform(-146.5,-193.9);

	this.shape_552 = new cjs.Shape();
	this.shape_552.graphics.f("#FFFFFF").s().p("AAAApQgIALgOAAQgUAAgKgOQgJgOAAgXQABgZAQgPQAMgLAPgBQAGgBAFACIAAAMQgHgDgKADQgKAEgHAJQgGAKAAAMQAAAeAWAAQAIAAAFgEQAFgEAAgIIAAgLIAMAAIAAAKQAAAJAGAEQAFAEAIAAQAJAAAHgGQAGgGAAgKQAAgJgHgGQgGgGgKAAIgdAAQgBgSAHgJQAIgIAPAAQAcAAAAAWIgBAFIgKAAQAKAEAGAJQAFAKAAAJQAAAUgJAMQgKAMgRAAQgSAAgIgLgAAJgbIASAAIAHABIAAgEQAAgJgMAAQgMAAgBAMg");
	this.shape_552.setTransform(-150.6,-186.8);

	this.shape_553 = new cjs.Shape();
	this.shape_553.graphics.f("#FFFFFF").s().p("AghAlQgPgOAAgWQAAgXAPgPQAPgOAVAAQAYAAALAKQAIAHABAKIgMAAQgBgHgKgEQgIgEgLAAQgOAAgLAJQgMALAAAOQAAAOAKAJQALAJAOAAQAMAAAIgGQAKgHAAgLQAAgOgNgEQAEAFAAAJQAAAGgGAFQgGAFgIAAQgIAAgHgFQgGgGAAgJQAAgKAIgGQAIgGAKAAQARAAALAKQAMAJAAAPQAAAUgOAMQgNANgTAAQgVAAgPgPgAgCgMQgCADAAAEQAAADACACQACADADAAQAEAAADgDQACgBAAgEQAAgEgCgCQgDgDgDAAQgEAAgCACg");
	this.shape_553.setTransform(-162.2,-186.8);

	this.shape_554 = new cjs.Shape();
	this.shape_554.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_554.setTransform(-174.1,-186.8);

	this.shape_555 = new cjs.Shape();
	this.shape_555.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_555.setTransform(-183.5,-186.8);

	this.shape_556 = new cjs.Shape();
	this.shape_556.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgEQgHgDABgHIAAgGQAFgMAXAAQASAAAGAKQAHgKAUAAQAJAAAHADQAIAFABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAWgPANQgRANgdAAQgdAAgRgOgAgpgLQgFAFABAJQAAANAPAGQAMAFASgBQARABAMgFQAQgGABgNQAAgJgFgFQgFgGgIABIg4AAQgJgBgEAGgAANglQgFAEgBAFIAdAAIAAgDQAAgDgEgDQgDgCgEgBIgCAAQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgDIgFAAIgFABg");
	this.shape_556.setTransform(-192.3,-186.7);

	this.shape_557 = new cjs.Shape();
	this.shape_557.graphics.f("#FFFFFF").s().p("AgqA3QgTgOABgVQABgRAOgIQgJgCAAgKQAAgKALgEQAHgEAMAAQAQAAAHAKQAEgJAPgBIAJAAIAGgFIAEgGQAHgLAAgKIARAAIgDALQgCAHgEAFQgFAHgFAFQAIAEACAKQAAAGgCAEIgKAAQAFACAFAFQAJAHAAAQQAAAUgTAOQgRANgXAAQgYgBgSgNgAgnAFQgFAGAAAHQABAMAOAHQANAGAQAAQARAAAMgGQAOgGABgOQAAgHgFgFQgFgFgMAAIgwAAQgHAAgGAFgAAPgWQgFABgDACQgEADAAAFIAUAAQAHAAAFABQABgCgBgFQgDgGgNAAIgEABgAgfgVQADABABADQAAADgCADIAWAAQgCgIgHgDIgIgBQgEAAgDACg");
	this.shape_557.setTransform(-204.8,-188.4);

	this.shape_558 = new cjs.Shape();
	this.shape_558.graphics.f("#FFFFFF").s().p("AgnAwQgMgQgBgcQAAgcAPgTQAPgUAcgBQAOAAAMAGQAPAIAAAMQAAAIgFAEQgFAEgHABQgGAAgFgEQgEgDAAgGQAAgIAJgEQgHgFgLAAIgDAAQgNAAgHARQgEAMAAARQARgLAOAAQARAAANALQAMALAAARQABAUgRALQgOAKgVABQgZAAgPgRgAgPAIQgFAGAAALQAAAYAUABQALAAAGgHQAFgHAAgLQAAgZgWAAQgKAAgFAIg");
	this.shape_558.setTransform(-223.7,-187.7);

	this.shape_559 = new cjs.Shape();
	this.shape_559.graphics.f("#FFFFFF").s().p("AgpAtQgOgSAAgbQAAgaANgSQAPgTAbAAQAZAAAQATQAPARAAAbQABAbgOASQgQATgbAAQgZAAgQgTgAgZAAQABAwAYAAQALABAHgKQAHgKABgdQgBgWgDgJQgGgQgRgBQgXAAgBAwg");
	this.shape_559.setTransform(-236.3,-187.7);

	this.shape_560 = new cjs.Shape();
	this.shape_560.graphics.f("#FFFFFF").s().p("AgdA7QgPgHAAgMQAAgIAFgEQAEgFAIAAQAGAAAEAEQAEAEABAGQAAAHgJAEQAJAEAHAAQAQABAGgQQAGgLAAgUQgSALgPAAQgRAAgMgLQgMgLAAgRQAAgTAPgMQAOgKAVAAQAbgBAOASQANAQgBAcQAAAcgNASQgQAVgaAAQgOAAgMgGgAgRgpQgEAHAAALQgBALAHAGQAGAGAJAAQAJAAAGgGQAGgHAAgKQAAgLgGgHQgFgIgKAAQgKAAgHAIg");
	this.shape_560.setTransform(-248.9,-187.7);

	this.shape_561 = new cjs.Shape();
	this.shape_561.graphics.f("#FFFFFF").s().p("AglA9IAAgOIALABQAIAAADgDQADgCAAgIIAAg/IgZAAIAAgNQAOAAAHgFQAHgDAHgLIAPAAIAABfQAAAIADACQACADAIAAIAMgBIAAAOg");
	this.shape_561.setTransform(-260,-187.7);

	this.shape_562 = new cjs.Shape();
	this.shape_562.graphics.f("#FFFFFF").s().p("Ag0BFQgPgOAAgUQAAgZATgNQATgOAiAAIAVAAIAAgKQAAgMgGgIQgHgHgNAAQgNAAgHAGQgIAGAAAJIgkAAQAAgNAJgLQAJgLAPgHQAOgGASAAQAbAAARAOQARAOAAAZIAABHQAAAVAGANIAAACIgkAAQgDgEgBgLQgRASgYAAQgYAAgPgNgAgWAMQgJAHAAAMQAAAKAGAHQAHAGAMAAQAJAAAKgGQAJgFAEgIIAAgeIgTAAQgTAAgKAHg");
	this.shape_562.setTransform(340.6,-252.9);

	this.shape_563 = new cjs.Shape();
	this.shape_563.graphics.f("#FFFFFF").s().p("AgzBbQgRgWAAgnQAAgjARgWQAQgWAdgBQAZAAAPASIAAhRIAjAAIAADgIggAAIgBgRQgRAUgZAAQgcAAgRgXgAgXgHQgKANAAAcQAAAYAKAOQAJANAQAAQAWAAAKgUIAAhCQgKgTgWAAQgQAAgJANg");
	this.shape_563.setTransform(323.7,-256.1);

	this.shape_564 = new cjs.Shape();
	this.shape_564.graphics.f("#FFFFFF").s().p("AAeBRIAAhmQAAgPgGgHQgHgIgPAAQgUAAgLAUIAABwIgkAAIAAieIAiAAIABATQASgWAbAAQAyAAABA5IAABog");
	this.shape_564.setTransform(307.2,-253.1);

	this.shape_565 = new cjs.Shape();
	this.shape_565.graphics.f("#FFFFFF").s().p("Ag0A8QgVgWAAgmIAAAAQAAgYAKgSQAJgTAQgKQARgKAVAAQAgAAAUAUQAUAVACAiIAAAIQAAAXgJATQgJASgRAKQgRAKgWAAQggAAgUgWgAgbgnQgKAOAAAbQAAAYAKAOQAKAOARAAQASAAAKgOQAKgOAAgaQAAgYgKgPQgKgOgSAAQgRAAgKAOg");
	this.shape_565.setTransform(290.3,-252.9);

	this.shape_566 = new cjs.Shape();
	this.shape_566.graphics.f("#FFFFFF").s().p("AAwBrIAAhfIhfAAIAABfIglAAIAAjVIAlAAIAABZIBfAAIAAhZIAlAAIAADVg");
	this.shape_566.setTransform(271.1,-255.7);

	this.shape_567 = new cjs.Shape();
	this.shape_567.graphics.f("#FFFFFF").s().p("Ag0A8QgVgWAAgmIAAAAQAAgYAKgSQAJgTAQgKQARgKAVAAQAgAAAUAUQAUAVACAiIAAAIQAAAXgJATQgJASgRAKQgRAKgWAAQggAAgUgWgAgbgnQgKAOAAAbQAAAYAKAOQAKAOARAAQASAAAKgOQAKgOAAgaQAAgYgKgPQgKgOgSAAQgRAAgKAOg");
	this.shape_567.setTransform(244.4,-252.9);

	this.shape_568 = new cjs.Shape();
	this.shape_568.graphics.f("#FFFFFF").s().p("AgpBRIAAieIAiAAIABASQAMgVAXAAQAIAAAFACIgBAiIgOgBQgYAAgIASIAABsg");
	this.shape_568.setTransform(231.3,-253.1);

	this.shape_569 = new cjs.Shape();
	this.shape_569.graphics.f("#FFFFFF").s().p("AgRBtIAAidIAiAAIAACdgAgOhMQgFgFAAgIQAAgJAFgFQAFgFAJAAQAJAAAGAFQAFAFAAAJQAAAIgFAFQgGAFgJAAQgJAAgFgFg");
	this.shape_569.setTransform(221.6,-255.9);

	this.shape_570 = new cjs.Shape();
	this.shape_570.graphics.f("#FFFFFF").s().p("AAeBxIAAhnQAAgPgHgHQgHgHgOAAQgUAAgLATIAABxIgkAAIAAjgIAkAAIAABTQASgUAZAAQAzAAABA5IAABog");
	this.shape_570.setTransform(209.4,-256.2);

	this.shape_571 = new cjs.Shape();
	this.shape_571.graphics.f("#FFFFFF").s().p("AgwA9QgUgWAAgmIAAgCQAAglAUgVQATgWAgAAQAdAAASAQQARARABAbIghAAQgBgOgIgJQgJgJgOAAQgQAAgKANQgIAMgBAaIAAAFQAAAZAJANQAKANAQAAQANAAAKgIQAIgIABgLIAhAAQAAAPgJAMQgJANgOAIQgPAHgRAAQghAAgTgVg");
	this.shape_571.setTransform(193.4,-252.9);

	this.shape_572 = new cjs.Shape();
	this.shape_572.graphics.f("#FFFFFF").s().p("AgRBtIAAidIAjAAIAACdgAgOhMQgFgFAAgIQAAgJAFgFQAFgFAJAAQAJAAAGAFQAFAFAAAJQAAAIgFAFQgGAFgJAAQgJAAgFgFg");
	this.shape_572.setTransform(181.6,-255.9);

	this.shape_573 = new cjs.Shape();
	this.shape_573.graphics.f("#FFFFFF").s().p("Ag0A8QgVgWAAgmIAAAAQAAgYAKgSQAJgTAQgKQARgKAVAAQAgAAAUAUQAUAVACAiIAAAIQAAAXgJATQgJASgRAKQgRAKgWAAQggAAgUgWgAgbgnQgKAOAAAbQAAAYAKAOQAKAOARAAQASAAAKgOQAKgOAAgaQAAgYgKgPQgKgOgSAAQgRAAgKAOg");
	this.shape_573.setTransform(169.1,-252.9);

	this.shape_574 = new cjs.Shape();
	this.shape_574.graphics.f("#FFFFFF").s().p("AgmBlQgTgJgLgPQgLgPABgUIAlAAQAAASALAKQANAKAVAAQATAAAKgIQAJgIAAgMQAAgPgJgHQgKgIgZgHQgZgIgQgJQgdgSAAgeQAAgaAVgQQAVgQAhAAQAWgBARAJQARAHAKAQQALAPgBASIglAAQABgQgKgKQgLgJgUAAQgRAAgKAIQgKAHAAAOQAAAMALAIQALAIAYAHQAYAHAPAJQAQAKAGALQAIANgBARQAAAagUAQQgUAPgjAAQgWAAgUgIg");
	this.shape_574.setTransform(151.6,-255.6);

	this.shape_575 = new cjs.Shape();
	this.shape_575.graphics.f("#FFFFFF").s().p("AglAOIAAgbIBLAAIAAAbg");
	this.shape_575.setTransform(130.1,-254.1);

	this.shape_576 = new cjs.Shape();
	this.shape_576.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAKAAAGgSQAGgSAAgVQAAgVgFgSQgIgTgIgBQgKAAgGAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgMAWQgOAagWAAQgNAAgKgGg");
	this.shape_576.setTransform(111.7,-252.8);

	this.shape_577 = new cjs.Shape();
	this.shape_577.graphics.f("#FFFFFF").s().p("AABBUQgMAPgYAAQgYAAgQgSQgPgTABgYQACgcATgMIgaAAIAAgSIAgAAQgGgDAAgJQAAgFACgDQAKgRAbAAIAMABQAUABANAQQAMAQgBAVIg4AAQgPAAgLAJQgKAJgBAOQAAANAJAJQAJAJAPAAQAKAAAIgHQAIgHAAgLIAAgUIARAAIAAAVQAAAKAHAHQAIAHALAAQAfgBADg2QACgggVgbQgVgbgfAAQgvgBgRAeIgVAAQAXgvA9ABQAlAAAZAWQASAQAJAYQAJAZgCAZQgBAigNAXQgQAbgbAAQgYAAgMgPgAgigmQAEAGAAAEQAAAFgDADIAlAAQgEgMgJgEQgHgDgJAAIgJABg");
	this.shape_577.setTransform(98.5,-254.8);

	this.shape_578 = new cjs.Shape();
	this.shape_578.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHAAANQABALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_578.setTransform(85.6,-263.4);

	this.shape_579 = new cjs.Shape();
	this.shape_579.graphics.f("#FFFFFF").s().p("AgiBOQgbgBgSgRQgRgRgBgZQgBgXAIgQIASAAQgCAGACACQADACAFAAQAJAAAHgEQAPgJADgLIABgHQAAgMgIgEQgIgDgHADQAMAEAAAOQAAAIgGAFQgFAEgIAAIgIgBQgOgEAAgRQAAgGACgGQAIgUAdAAQALAAAIAFQAJAFADAJQAQgVAgACQAhABARAgQAMAVAAAZQgBA0gmASQgMAGgOAAQgIAAgKgCIAAgNQgMAKgKADQgJACgPAAIgEAAgAASgUQASAPAAAXQAAAZgPAQIAMABQANAAAIgGQATgOAAgfQAAgTgOgPQgNgPgTgBQgYgBgLAOIABAAQAPAAAKAIgAhNALQAAAMAHAJQAJAMAXAAIANgBQAOgBAKgLQAIgKAAgMQAAgHgEgFQgEgIgNgBQgGgBgKAHQgMAIgEABQgIADgJAAQgJAAgEgDIgBAIg");
	this.shape_579.setTransform(78.8,-252.8);

	this.shape_580 = new cjs.Shape();
	this.shape_580.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAJAAAHgSQAGgSAAgVQAAgVgGgSQgHgTgIgBQgJAAgHAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgNAWQgNAagWAAQgNAAgKgGg");
	this.shape_580.setTransform(64.7,-252.8);

	this.shape_581 = new cjs.Shape();
	this.shape_581.graphics.f("#FFFFFF").s().p("AAXA8QAQABAMgIQANgHAFgOQAGgQgBgPQgCgRgMgNQgMgPgSACQgcABgHAZQAEgDAHAAQAJAAAFADQARAMAAAbQgBAZgWAQQgTAOgdAAQgcAAgTgRQgTgRAAgbQAAgcAVgPQgGgBgEgGQgDgHAAgHQAAgOAMgIQALgIAPAAQAOAAAJAIQAKAJAEAOQARgeAjgBQAdAAATAYQASAXAAAfQAAAhgWAWQgWAWgiAAgAhEgJQgIAIAAAKQAAAQAPAJQANAIATAAQAQAAANgHQAMgHACgNQAAgHgDgEQgDgFgFAAQgEABgEACQgDAEAAAEIgRAAIgBgOIACgNIgXAAQgMAAgJAIgAg/g4QAFACACAHQACAHgCAGIATAAQgBgMgGgGQgFgFgJAAIgFABg");
	this.shape_581.setTransform(50.8,-252.8);

	this.shape_582 = new cjs.Shape();
	this.shape_582.graphics.f("#FFFFFF").s().p("AgxA4QgXgWAAghQAAgiAXgWQAWgWAhAAQAkAAAQAPQAMAKAAAPIgSAAQgBgKgOgHQgNgFgQAAQgWAAgQAOQgSAPAAAWQAAAVAQAOQAPAOAWAAQARAAANgKQAPgKAAgRQAAgVgUgGQAHAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQAAgPANgJQALgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgWgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQAEgDgBgFQABgGgEgDQgEgEgFAAQgGAAgDADg");
	this.shape_582.setTransform(32.7,-252.8);

	this.shape_583 = new cjs.Shape();
	this.shape_583.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHAAANQAAALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAABgJQgBgKgLAAIgDAAg");
	this.shape_583.setTransform(11.8,-263.4);

	this.shape_584 = new cjs.Shape();
	this.shape_584.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAJAAAHgSQAGgSAAgVQAAgVgGgSQgGgTgKgBQgIAAgHAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgNAWQgOAagVAAQgNAAgKgGg");
	this.shape_584.setTransform(11.4,-252.8);

	this.shape_585 = new cjs.Shape();
	this.shape_585.graphics.f("#FFFFFF").s().p("AgFBkQgigCgUgVQgVgVAAgfQgBgWAJgPQAIgQARgQQAMgJAXgXQAMgOADgJIAgAAQgGAMgJALQgJAKgPAKQAWgBARAGQAvARAAA3QAAAjgZAWQgYAWgiAAIgEAAgAgrgVQgOAPAAATQAAATANAMQARASAbAAQAVAAAQgMQAUgPAAgYQAAgLgEgKQgGgOgPgIQgNgHgRAAQgdAAgQASg");
	this.shape_585.setTransform(-0.5,-254.9);

	this.shape_586 = new cjs.Shape();
	this.shape_586.graphics.f("#FFFFFF").s().p("AgyA4QgWgWAAghQAAgiAWgWQAXgWAhAAQAjAAARAPQAMAKABAPIgSAAQgBgKgPgHQgNgFgQAAQgWAAgRAOQgRAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQAAgPAMgJQAMgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgXgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDAAgFQAAgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_586.setTransform(-16.7,-252.8);

	this.shape_587 = new cjs.Shape();
	this.shape_587.graphics.f("#FFFFFF").s().p("AhEBPQgQgRADgUQACgVATgJIggAAIAAgPIAaAAQgFgHACgKQADgNAOgGQAMgFARAAQAVACANAQQANAQAAAWIgrAAQgMgBgKAHQgLAHAAAKQAAAOARAJQAPAIAbAAQA/gBAAg8QAAgUgOgRQgQgTgXABIhIAAQgKAAgGgHQgFgHABgHQAEgSAYgIQASgFAhAAQAuAAAXAaQAEADACAHQACAGgBAFIgUAAQAMAHAKATQAKAUAAARQABAqgWAaQgXAbgpABQg1gBgWgYgAgkgYQADADACAFQACAGgDAHIAhAAQgBgKgHgGQgHgHgLAAQgGAAgFACgAgphJQAAAFAOAAIAwAAQANAAAPAGIAKAGQACgGgKgKQgNgLgkAAQgsAAABAKg");
	this.shape_587.setTransform(-34,-255.2);

	this.shape_588 = new cjs.Shape();
	this.shape_588.graphics.f("#FFFFFF").s().p("AAzAkQANgIAAgMIgBgFQgDgJgUgEQgRgEgXAAQgWAAgRAEQgUAEgDAJIAAAFQgBANAMAHIgRAAQgPgFAAgVQAAgIADgEQAKgTAXgIQASgGAdAAQBDAAAOAhQACAFABAHQgBAUgPAGg");
	this.shape_588.setTransform(-53,-262.7);

	this.shape_589 = new cjs.Shape();
	this.shape_589.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_589.setTransform(-52.9,-252.8);

	this.shape_590 = new cjs.Shape();
	this.shape_590.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAKAAAGgSQAGgSAAgVQAAgVgFgSQgIgTgIgBQgKAAgGAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgMAWQgOAagWAAQgNAAgKgGg");
	this.shape_590.setTransform(-66.5,-252.8);

	this.shape_591 = new cjs.Shape();
	this.shape_591.graphics.f("#FFFFFF").s().p("AADA8QgLATgfAAQgVAAgQgSQgPgRABgXQAAgaAQgMIgWAAIAAgSIAWAAQgEgDAAgJIABgIQAJgXAhAAQAcAAAJATQAMgTAbAAQAQAAAKAHQANAIACAOQABAMgFAHIgEAAQAYATAAAdQgBAZgPARQgQATgXAAQgbgBgNgSgAANAFIAAARQAAAJAJAFQAJAFALAAQAMAAALgKQAJgJAAgNQgBgLgIgIQgIgIgNAAIhQAAQgMAAgKAJQgJAIgBALQAAANAJAJQAKAJANAAQAMAAAIgEQAKgFAAgKIAAgRgAAmg7QgLAAgHAHQgGAHgBAKIAqAAQADgEgBgGQgBgOgQAAIgCAAgAgtg5QAFAEABAHQABAGgEAFIAiAAQgBgNgKgHQgHgEgHAAQgHAAgFACg");
	this.shape_591.setTransform(-79.9,-252.8);

	this.shape_592 = new cjs.Shape();
	this.shape_592.graphics.f("#FFFFFF").s().p("AgyA4QgWgWAAghQAAgiAWgWQAXgWAhAAQAjAAARAPQAMAKABAPIgSAAQgBgKgPgHQgNgFgQAAQgWAAgQAOQgSAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQABgPALgJQAMgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgXgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDAAgFQAAgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_592.setTransform(-97.1,-252.8);

	this.shape_593 = new cjs.Shape();
	this.shape_593.graphics.f("#FFFFFF").s().p("AgrAjQgMgFAAgNQAAgIAGgEIANAAQgEADAAAEQAAAHAKgBQAJABATgIQAOgEALAAIAEAAQAMACAFAGIAAgiQAGgGAGgLIAABHIgMAAQgHgKgOAAQgLAAgJADQgSAJgPgBQgHAAgGgBg");
	this.shape_593.setTransform(127.6,249.4);

	this.shape_594 = new cjs.Shape();
	this.shape_594.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_594.setTransform(94,242.7);

	this.shape_595 = new cjs.Shape();
	this.shape_595.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEAAAHIAbAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_595.setTransform(81,244.2);

	this.shape_596 = new cjs.Shape();
	this.shape_596.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAALg4QAAADADACQACACADAAQAFAAAAgFQAAgDgCgCQgDgCgDAAQgFAAAAAFgAgKg/QgUACAAAHIABAEIAFgBQAHgCANAAIAFAAQgBgDAAgDIABgFIgDAAIgIABg");
	this.shape_596.setTransform(63.5,244.4);

	this.shape_597 = new cjs.Shape();
	this.shape_597.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgMQANgLASAAIAOAAIAAAMIgKAAQgPAAgIAGQgJAGgBALQAAASAOAJQAMAIARAAQASAAAMgKQANgLgBgTQAAgTgLgKQgJgJgQAAIg7AAIAAgKQAQAAAAgCQAAAAgBgBQAAAAAAAAQgBAAAAgBQgBAAAAgBQgEgBAAgDIgBgDQAAgQAsAAQAbgBAOAMQANALgCAMIgOAAIABgDQAAgGgJgFQgLgHgSAAQgTAAgEAGQgBAAAAAAQAAABgBAAQAAABAAAAQAAABAAAAQAAAFAJAAIAcAAQANAAAMAIQAUANAAAeQAAAcgPASQgQASgbAAQgaAAgQgPg");
	this.shape_597.setTransform(-80.7,242.3);

	this.shape_598 = new cjs.Shape();
	this.shape_598.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgcQAAgcARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAAUgLANQgLAPgTABQgSAAgKgMQgMgLACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAPQAKAPARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_598.setTransform(-93.2,245.6);

	this.shape_599 = new cjs.Shape();
	this.shape_599.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_599.setTransform(-152.4,242.7);

	this.shape_600 = new cjs.Shape();
	this.shape_600.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgYQAAgQAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQANgMAAgSQgBgOgGgJQgHgMgMAAIgvAAQgWAAAAgNQAAgOARgHQAOgGAVAAQAeAAAQARQAGAIABALIgMAAQgCgLgMgGQgLgFgQAAQgcAAAAAKQAAABAAAAQAAABAAAAQAAABABAAQAAAAABABIAEABIAnAAQAKAAAIAEQAXAKAAAlQAAAbgQASQgRASgZAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAGABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_600.setTransform(-200.7,242.3);

	this.shape_601 = new cjs.Shape();
	this.shape_601.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAggIAHgIQAEgGABgEIAABFg");
	this.shape_601.setTransform(-258.1,249.3);

	this.shape_602 = new cjs.Shape();
	this.shape_602.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgIAGgDQgHgDAAgHIABgGQAFgMAXAAQASAAAGAKQAHgKAUAAQAJAAAHADQAIAFABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAWgQANQgQAOgdgBQgdABgRgPgAgpgLQgFAFABAJQAAANAQAGQALAFASgBQARABAMgFQAQgGAAgNQABgJgFgFQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFADgBAGIAdAAIAAgCQAAgEgDgDQgEgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_602.setTransform(-258.3,244.3);

	this.shape_603 = new cjs.Shape();
	this.shape_603.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgDIANAAQgEACAAAGQAAAFAKAAQAJAAATgGQAOgGALABIAEAAQAMACAFAGIAAgiQAGgGAGgLIAABHIgMAAQgHgKgOAAQgLgBgJAEQgSAIgPAAQgHAAgGgCg");
	this.shape_603.setTransform(66.1,224.4);

	this.shape_604 = new cjs.Shape();
	this.shape_604.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAAMgJQALgJABgOQABgUgKgMQgHgJgOAAIg+AAIAAgKQAOAAAAgBQAAgBAAAAQAAAAAAgBQgBAAAAgBQgBAAgBAAQgEgCAAgDQgCgRAvAAQATAAAPAIQARAJABAPIgOAAQABgJgOgFQgLgFgOAAQgXAAgBAHQgBAFAKAAIAcAAQANAAALAIQATAPAAAiQAAAXgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABg");
	this.shape_604.setTransform(16,217.7);

	this.shape_605 = new cjs.Shape();
	this.shape_605.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_605.setTransform(-11.3,220.6);

	this.shape_606 = new cjs.Shape();
	this.shape_606.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgEQgHgDABgHIAAgGQAFgMAXAAQASgBAGALQAHgLAUABQAJAAAHADQAIAFABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAVgPAOQgRANgdAAQgdAAgRgOgAgpgLQgFAFABAJQAAAMAPAHQAMAFASAAQARAAAMgFQAQgHABgMQAAgJgFgFQgFgGgIABIg4AAQgJgBgEAGgAANglQgFAEgBAFIAdAAIAAgDQAAgDgEgDQgDgCgEgBIgCAAQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgDIgFAAIgFABg");
	this.shape_606.setTransform(-166.6,219.3);

	this.shape_607 = new cjs.Shape();
	this.shape_607.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAAMgJQALgJABgOQABgUgKgMQgHgJgOAAIg+AAIAAgKQAOAAAAgBQAAgBAAAAQAAAAAAgBQgBAAAAgBQgBAAgBAAQgEgCAAgDQgCgRAvAAQATAAAPAIQARAJABAPIgOAAQABgJgOgFQgLgFgOAAQgXAAgBAHQgBAFAKAAIAcAAQANAAALAIQATAPAAAiQAAAXgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABg");
	this.shape_607.setTransform(-185.3,217.7);

	this.shape_608 = new cjs.Shape();
	this.shape_608.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOAAgVQgBgTAMgOQgDgBgEgEIgFgIIgCgLQABgKAIgKQAJgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgJAFQAPgBALAEQAfAMABAkQgBAXgQAQQgQAOgXAAIgDAAgAgcgMQgJALAAAMQABANAHAIQAMALASAAQAOAAAKgHQAOgLAAgQQAAgHgDgGQgEgJgKgGQgJgFgLAAQgTAAgLAMgAgjgrQgFAGAAAFQABAHAFADIAEgEIAYgWQgCgEgGgBIgDAAQgMAAgGAKg");
	this.shape_608.setTransform(-226.2,217.6);

	this.shape_609 = new cjs.Shape();
	this.shape_609.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAIQgKAJgMAAQgOgBgIgHg");
	this.shape_609.setTransform(-248.1,219.4);

	this.shape_610 = new cjs.Shape();
	this.shape_610.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgDIANAAQgEACAAAGQAAAFAKAAQAJAAATgGQAOgGALAAIAEAAQAMACAFAHIAAgjQAGgFAGgLIAABHIgMAAQgHgLgOAAQgLAAgJAFQgSAHgPAAQgHAAgGgCg");
	this.shape_610.setTransform(-225.4,178.6);

	this.shape_611 = new cjs.Shape();
	this.shape_611.graphics.f("#FFFFFF").s().p("AATBIQgTgBgIgLQgGAMgPAAQgQAAgJgRQgGgNAAgVQAAgUANgNQANgPAUAAQANAAAJAIQAMAJABAMQgBAHgCAHQgCAHgDACQgKADABAHQAAAMARgBQAMAAAHgNQAGgLAAgPQAAgQgMgNQgJgJgQAAIgxAAQgHAAgDgEQgEgEAAgFQAAgPAUgHQANgFASAAQAnAAALATQADAGAAAGIgBAFIgKAAQAWASAAAfQAAAXgLARQgLASgTAAIgBAAgAglAtQgCACAAADQAAAFAFADQACABADAAQAFAAACgEQACgCAAgDQAAgFgDgDQgDgBgDAAQgFAAgDAEgAgKAlQgBgJAGgFIAFgEQADgCABgDIAAgEQAAgHgHgEQgGgEgIAAQgUAAgHASQgDAGAAAHQAAAGACAFQAEgIANgBQAMAAAGAJIAAAAgAgggvQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAIAFABIAtAAQAFAAAGACIAFADIABgDQAAgHgMgFQgKgEgLAAQggAAgDAKg");
	this.shape_611.setTransform(-258.3,171.5);

	this.shape_612 = new cjs.Shape();
	this.shape_612.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOgBgVQAAgTAMgOQgEgBgDgEIgFgIIgCgLQABgKAIgKQAJgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgJAFQAPgBALAEQAgAMAAAkQAAAXgRAQQgQAOgXAAIgDAAgAgcgMQgJALAAAMQABANAHAIQAMALARAAQAPAAAKgHQAOgLAAgQQAAgHgEgGQgDgJgJgGQgKgFgLAAQgTAAgLAMgAgkgrQgDAGAAAFQAAAHAFADIAEgEIAYgWQgCgEgGgBIgDAAQgMAAgHAKg");
	this.shape_612.setTransform(27.4,146.8);

	this.shape_613 = new cjs.Shape();
	this.shape_613.graphics.f("#FFFFFF").s().p("AgmA4QgRgLAAgUIAAgFQAAgFAFgGQAEgGAFgDIgVAAIAAgKIARAAQgDgEABgGQACgJAKgFQAIgDALAAQAOABAIALQAJALAAAOIgfAAQgIAAgGAEQgHAFAAAIQAAAMANAHQALAGAQgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAPgIARAAQAcAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgZAAgQgLgAgZgYQADACABAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgHAAIgIABg");
	this.shape_613.setTransform(15.2,147);

	this.shape_614 = new cjs.Shape();
	this.shape_614.graphics.f("#FFFFFF").s().p("AgCA5QgHALgRAAQgRAAgKgOQgIgOAAgXQABgZAPgOQAMgLAYABIAIAAIAAALIgSABQgJABgFAFQgMAJAAAQQgBANAFAJQAFAKAKAAQAKAAAEgFQAFgFgBgIIAAgPIAMAAIAAAPQAAASATAAQAMAAAGgOQAGgOgBgUQAAgTgLgOQgNgSgYAAQgOAAgLAGQgLAFgGAJIgNAAQAEgOAPgJQAPgJAVAAQAPABANAGQALAFAGAGQATAVAAAjQAAAagKASQgLASgUAAQgQAAgHgLg");
	this.shape_614.setTransform(-55.6,146.8);

	this.shape_615 = new cjs.Shape();
	this.shape_615.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCAAgIIABgFQAFgNAWAAQASAAAGAKQAHgKAUAAQAJAAAHAFQAIAEABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAVgPANQgRAOgdABQgdgBgRgPgAgpgLQgFAGABAHQAAANAPAHQAMAEASABQARgBAMgEQAQgHABgNQAAgHgFgGQgFgFgIgBIg5AAQgHABgFAFgAANgkQgFACgBAHIAdAAIAAgEQAAgEgEgCQgDgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_615.setTransform(-92,148.5);

	this.shape_616 = new cjs.Shape();
	this.shape_616.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_616.setTransform(-105.2,148.4);

	this.shape_617 = new cjs.Shape();
	this.shape_617.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAJAFAAAJQABAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_617.setTransform(-141.3,148.4);

	this.shape_618 = new cjs.Shape();
	this.shape_618.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAATgLAOQgLAOgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_618.setTransform(-154,149.8);

	this.shape_619 = new cjs.Shape();
	this.shape_619.graphics.f("#FFFFFF").s().p("AADAXIAHgIQAEgFAAgCIAAgCIgGgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAGAEAAAIQAAAFgDAFIgHAKQgFAHgEADgAgeAXQADgDAEgFQAEgFAAgCIgBgCIgFgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAFAEAAAIQAAAFgCAFIgHAKQgGAHgDADg");
	this.shape_619.setTransform(-180.8,140.8);

	this.shape_620 = new cjs.Shape();
	this.shape_620.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgGgFQgFgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_620.setTransform(-192,148.4);

	this.shape_621 = new cjs.Shape();
	this.shape_621.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQAMgMAAgSQAAgOgFgJQgIgMgMAAIgvAAQgWAAAAgNQAAgOARgHQAOgGAVAAQAeAAAQARQAHAIAAALIgMAAQgCgLgMgGQgLgFgPAAQgdAAgBAKQAAABABAAQAAABAAAAQAAABABAAQAAAAABABIAEABIAnAAQAKAAAIAEQAXAKAAAlQAAAbgRASQgPASgaAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQAAgBgBAAQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAg");
	this.shape_621.setTransform(-250.4,146.5);

	this.shape_622 = new cjs.Shape();
	this.shape_622.graphics.f("#FFFFFF").s().p("AAVAXQgDgDgGgHQgGgHgBgDQgCgFAAgFQAAgIAFgEQAFgDAEAAQAIAAAEAGQADAEAAAFQAAAHgGADIgFADIgBACQAAACAEAFQAEAFADADgAgMAXIgJgKIgHgKQgDgFAAgFQAAgIAGgEQAEgDAFAAQAHAAAFAGQACAEAAAFQAAAHgFADIgGADIAAACQAAACAEAFQADAFAEADg");
	this.shape_622.setTransform(-260.8,140.8);

	this.shape_623 = new cjs.Shape();
	this.shape_623.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_623.setTransform(91.5,123.4);

	this.shape_624 = new cjs.Shape();
	this.shape_624.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAGgFQgHgCABgIIAAgFQAFgNAXAAQASABAGAJQAHgJAUgBQAJAAAHAFQAIAEABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAWgPAMQgRAOgdAAQgdAAgRgPgAgpgLQgFAGABAHQAAAOAPAGQAMAEASAAQARAAAMgEQAQgGABgOQAAgHgFgGQgFgFgIgBIg4AAQgJABgEAFgAANglQgFAEgBAFIAdAAIAAgCQAAgEgEgDQgDgCgEAAIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_624.setTransform(78.6,123.5);

	this.shape_625 = new cjs.Shape();
	this.shape_625.graphics.f("#FFFFFF").s().p("AADAXIAHgIQAEgFAAgCIAAgCIgGgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAGAEAAAIQAAAFgDAFIgHAKQgFAHgEADgAgeAXQADgDAEgFQAEgFAAgCIgBgCIgFgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAFAEAAAIQAAAFgCAFIgHAKQgGAHgDADg");
	this.shape_625.setTransform(-56.4,115.8);

	this.shape_626 = new cjs.Shape();
	this.shape_626.graphics.f("#FFFFFF").s().p("AgyAEQAAgTAZgHQAIgCALAAIAPAAIAXACQATAEAAAOQAAAIgKAEQgEACgFAAQgIAAgGgEQgFgDgBgFQgBgFADgEQgFgBgNAAQggABABAPQAAAHAHACIAAAMQgWgCAAgTgAAYgFQABAGAGAAQAGgBAAgEQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAAAgBgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgHAAAAAGg");
	this.shape_626.setTransform(-67.6,115.3);

	this.shape_627 = new cjs.Shape();
	this.shape_627.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgdARgRQASgSAcAAQAYAAAQAQQARAPAAAYQAAATgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKAAQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAPAAAUQAAATAKAPQAKAPARADIAJABQAWAAAMgSIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_627.setTransform(-67.2,124.8);

	this.shape_628 = new cjs.Shape();
	this.shape_628.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgLQANgMASAAIAOAAIAAAMIgKAAQgPAAgIAHQgJAFgBAMQAAARAOAJQAMAJARgBQASAAAMgKQANgLgBgTQAAgTgLgKQgJgIgQAAIg7AAIAAgLQAQAAAAgCQAAAAgBgBQAAAAAAAAQgBAAAAgBQgBAAAAgBQgEgBAAgCIgBgEQAAgQAsAAQAbAAAOAMQANALgCALIgOAAIABgDQAAgGgJgFQgLgHgSAAQgTAAgEAGQgBAAAAAAQAAABgBAAQAAABAAAAQAAABAAAAQAAAFAJAAIAcAAQANAAAMAHQAUAOAAAdQAAAdgPARQgQATgbgBQgaAAgQgOg");
	this.shape_628.setTransform(-97.7,121.5);

	this.shape_629 = new cjs.Shape();
	this.shape_629.graphics.f("#FFFFFF").s().p("AgoA4QgRgRAAgYQAAgPAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAJQgJAKgMgBQgLAAgIgGQgJgHAAgJIADgQQABgHgHAAQgFAAgDAIQgDAGAAAHQAAASANAKQAMAKARAAQARAAANgMQAMgMAAgSQABgOgHgKQgGgLgOAAIhFAAIAAgKQASABAAgCIgEgDQgDgBAAgEQAAgOAPgEQAIgCASAAQAPAAAKADQANADAJAKQAGAHAAAMIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFABQgHACAAAHQAAABAAAAQAAAAAAABQAAAAABABQAAAAABABQABAAAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHADQAYALAAAlQAAAbgRASQgPARgaAAQgYAAgQgPgAgOACQAAAGAEAEQAFAEAFAAQAHABAFgFQAFgEAAgHQgFAIgJgBQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQgBABAAAAQAAABABABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_629.setTransform(-144.5,121.5);

	this.shape_630 = new cjs.Shape();
	this.shape_630.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgLQANgMASAAIAOAAIAAAMIgKAAQgPAAgIAHQgJAFgBAMQAAARAOAJQAMAJARgBQASAAAMgKQANgLgBgTQAAgTgLgKQgJgIgQAAIgmAAQgIAAgFgFQgFgDAAgHQAAgNAOgHQAMgEAXAAQAcAAAOAJQALAHgBAJQAAAFgEACQgEAEgEAAQASAOAAAbQAAAdgPARQgQATgbgBQgaAAgQgOgAAagzQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAGAGABQAAAAABgBQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAAAQAAgBABAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABgAgcgzQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAFAIAAIAiAAQgCgDAAgDQAAgEAEgDQgGgBgJAAQgWgBgGAHg");
	this.shape_630.setTransform(-156.7,121.5);

	this.shape_631 = new cjs.Shape();
	this.shape_631.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_631.setTransform(-169.5,123.4);

	this.shape_632 = new cjs.Shape();
	this.shape_632.graphics.f("#FFFFFF").s().p("AAVAXQgDgDgGgHQgGgHgBgDQgCgFAAgFQAAgIAFgEQAFgDAEAAQAIAAAEAGQADAEAAAFQAAAHgGADIgFADIgBACQAAACAEAFQAEAFADADgAgMAXIgJgKIgHgKQgDgFAAgFQAAgIAGgEQAEgDAFAAQAHAAAFAGQACAEAAAFQAAAHgFADIgGADIAAACQAAACAEAFQADAFAEADg");
	this.shape_632.setTransform(-180.2,115.8);

	this.shape_633 = new cjs.Shape();
	this.shape_633.graphics.f("#FFFFFF").s().p("AgnAvQgSgPAAgXQAAgWAPgMQgBgLAEgHQAKgSASACQAKABAFAGQAHgIANABQAMAAAHAIQAHAJAAAUIgyAAIgVABQgBAOAKAIQAKAKAMgBQALgBAHgGQAIgFABgIIANAAQgDAOgMAJQgMAKgQAAQgeAAgMgVIgBAHQAAAQANAKQALAKAPAAQAYgBALgOQAIgIgBgLIANAAQAAAOgJAMQgQAXggAAQgXAAgRgNgAgTgrQgDAEABAGIAJgBIAKAAQABgNgKAAQgFAAgDAEgAALghIAUAAQAAgOgJAAQgLAAAAAOg");
	this.shape_633.setTransform(-245.8,124.2);

	this.shape_634 = new cjs.Shape();
	this.shape_634.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAGgFQgHgCAAgIIABgFQAFgNAXAAQASABAGAJQAHgJAUgBQAJAAAHAFQAIAEABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAWgQAMQgQAOgdAAQgdAAgRgPgAgpgLQgFAGABAHQAAAOAQAGQALAEASAAQARAAAMgEQAQgGAAgOQABgHgFgGQgFgFgIgBIg4AAQgJABgEAFgAANglQgFAEgBAFIAdAAIAAgCQAAgEgDgDQgEgCgEAAIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_634.setTransform(-258.3,123.5);

	this.shape_635 = new cjs.Shape();
	this.shape_635.graphics.f("#FFFFFF").s().p("AgrAAIgghWICXBWIiXBXg");
	this.shape_635.setTransform(-285.9,-27.6);

	this.shape_636 = new cjs.Shape();
	this.shape_636.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMAAQgOgBgIgHg");
	this.shape_636.setTransform(-55.6,74.6);

	this.shape_637 = new cjs.Shape();
	this.shape_637.graphics.f("#FFFFFF").s().p("AgSASQgIgGgCgKIgBgEQAAgNAIgIIAAAOIAFAAQAAAGAGAFQAEAFAHAAQAHABAGgHQAEgFgBgIIAMAAIABAGQAAAMgIAKQgJAIgNAAQgKAAgIgGg");
	this.shape_637.setTransform(-120,80.7);

	this.shape_638 = new cjs.Shape();
	this.shape_638.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQAMgMAAgSQAAgOgFgJQgIgMgMAAIgvAAQgWAAAAgNQAAgOARgHQAOgGAVAAQAQAAAKACQAPAEAHAKQADAFAAAGQABAKgIACQAOAOAAAbQAAAbgRASQgPASgaAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQAAgBgBAAQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAgAAYgzQgCACgBADQABADACADQADACACAAQAEAAACgCQACgDABgDQgBgDgCgCQgCgCgEAAQgCAAgDACgAgegvQAAABABAAQAAABAAAAQAAABABAAQAAAAABABIAEABIAhAAIgBgFQgBgHAEgDIgLAAQgfAAAAAKg");
	this.shape_638.setTransform(-174.8,72.5);

	this.shape_639 = new cjs.Shape();
	this.shape_639.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgNgNgBgWQAAgOAFgKQAFgKANgLIAXgVIAJgPIAVAAQgEAHgGAHQgGAHgJAHQAOgBALAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgDgKgKgFQgJgFgMAAQgSAAgLAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAGAEQAFAFAAAJQABAIgIAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_639.setTransform(5.8,47.6);

	this.shape_640 = new cjs.Shape();
	this.shape_640.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_640.setTransform(-18.6,50.8);

	this.shape_641 = new cjs.Shape();
	this.shape_641.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgMIA7AAIAAAMIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEAAQALABAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_641.setTransform(-76.1,49.6);

	this.shape_642 = new cjs.Shape();
	this.shape_642.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgEIANAAQgEAEAAAEQAAAGAKABQAJgBATgGQAOgFALgBIAEAAQAMACAFAHIAAgjQAGgFAGgKIAABGIgMAAQgHgLgOAAQgLAAgJAFQgSAHgPABQgHAAgGgDg");
	this.shape_642.setTransform(-107.2,54.6);

	this.shape_643 = new cjs.Shape();
	this.shape_643.graphics.f("#FFFFFF").s().p("AgCBFQgXgBgNgOQgOgOgBgVQAAgTAMgOQgDgBgEgEIgFgIIgCgLQAAgKAJgKQAJgKAQAAIAKABQAKADABAHIAGgJIAVAAQgFAJgHAJQgIAIgJAFQAPgBALAEQAgAMgBAkQAAAXgQAQQgQAOgXAAIgCAAgAgcgMQgJALAAAMQAAANAIAIQAMALARAAQAPAAAKgHQAOgLAAgQQgBgHgDgGQgDgJgJgGQgKgFgMAAQgSAAgLAMgAgkgrQgDAGAAAFQgBAHAGADIAEgEIAXgWQgBgEgGgBIgDAAQgMAAgHAKg");
	this.shape_643.setTransform(-125.3,47.8);

	this.shape_644 = new cjs.Shape();
	this.shape_644.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgMIA7AAIAAAMIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEAAQALABAEgKIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_644.setTransform(-134.4,49.6);

	this.shape_645 = new cjs.Shape();
	this.shape_645.graphics.f("#FFFFFF").s().p("AgCA5QgHALgQAAQgTAAgIgOQgJgOAAgXQABgZAPgOQANgLAXABIAHAAIAAALIgRABQgIABgGAFQgMAJgBAQQAAANAFAJQAFAKALAAQAIAAAFgFQAFgFAAgIIAAgPIALAAIAAAPQAAASATAAQAMAAAGgOQAGgOgBgUQAAgTgLgOQgOgSgXAAQgOAAgMAGQgKAFgFAJIgOAAQAEgOAPgJQAPgJAVABQAPAAANAGQALAFAFAHQAUAUAAAjQABAagLASQgLASgUAAQgQAAgHgLg");
	this.shape_645.setTransform(-240.3,47.8);

	this.shape_646 = new cjs.Shape();
	this.shape_646.graphics.f("#FFFFFF").s().p("AgCA5QgHAMgRgBQgRAAgKgOQgIgOAAgXQABgZAQgOQAMgKAYgBIAHAAIAAAMIgSABQgIABgGAEQgMAJAAARQgBANAEAJQAGAKAKAAQAKAAAEgGQAFgEgBgIIAAgPIAMAAIAAAPQAAASATAAQAMAAAGgPQAGgNgBgUQAAgUgKgNQgOgSgYAAQgOAAgLAFQgLAGgGAJIgNAAQAEgOAPgJQAPgIAVgBQAPABAOAGQAJAFAHAGQATAVAAAjQABAbgLARQgLASgUAAQgQABgHgMg");
	this.shape_646.setTransform(-3.9,22.8);

	this.shape_647 = new cjs.Shape();
	this.shape_647.graphics.f("#FFFFFF").s().p("AgSASQgIgGgDgKIAAgFQAAgMAIgIIAAANIAFAAQAAAIAFAEQAFAGAHgBQAHAAAGgFQAEgGgBgIIANAAIAAAGQAAANgIAIQgJAJgNAAQgKAAgIgGg");
	this.shape_647.setTransform(-38,30.7);

	this.shape_648 = new cjs.Shape();
	this.shape_648.graphics.f("#FFFFFF").s().p("AgoA4QgRgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQARAAANgMQAMgMAAgSQAAgOgFgJQgIgMgNAAIguAAQgWAAAAgNQAAgOARgHQAOgGAUAAQASAAAJACQAPAEAIAKQACAFAAAGQAAAKgHACQAOAOAAAbQAAAbgRASQgQASgZAAQgYAAgQgQgAgOACQAAAGAEAEQAFAEAGABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQAAgBgBAAQAAgBAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAgAAYgzQgDACABADQgBADADADQADACADAAQADAAACgCQADgDAAgDQAAgDgDgCQgCgCgDAAQgDAAgDACgAgegvQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAhAAIgCgFQAAgHAEgDIgLAAQgfAAAAAKg");
	this.shape_648.setTransform(-67.9,22.5);

	this.shape_649 = new cjs.Shape();
	this.shape_649.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgMQANgLASAAIAOAAIAAAMIgKAAQgPAAgIAHQgJAFgBAMQAAARAOAJQAMAJARgBQASAAAMgKQANgLgBgTQAAgTgLgKQgJgIgQAAIgmAAQgIAAgFgFQgFgDAAgHQAAgNAOgHQALgEAYAAQAbAAAOAMQANALgCALIgOAAIABgDQAAgGgJgFQgMgHgSgBQgWAAgGAHQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAFAIAAIAiAAQANAAAMAHQAUAOAAAdQAAAdgPARQgQATgbgBQgaAAgQgOg");
	this.shape_649.setTransform(-86.5,22.5);

	this.shape_650 = new cjs.Shape();
	this.shape_650.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAIAFIgMAAQgJgDgBgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQAAAOgLAEg");
	this.shape_650.setTransform(-99.1,17.8);

	this.shape_651 = new cjs.Shape();
	this.shape_651.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgGgFQgGgGgHAAIg1AAQgJAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAJAAQAHAAAGgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgDAEIAXAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_651.setTransform(-99.3,24.4);

	this.shape_652 = new cjs.Shape();
	this.shape_652.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAAMgJQALgJABgOQABgUgKgMQgHgJgOAAIg+AAIAAgKQAOAAAAgBQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBgBgBAAQgEgCAAgDQgCgRAvAAQATAAAPAIQARAJABAPIgOAAQABgJgOgFQgLgFgOAAQgXAAgBAHQgBAFAKAAIAcAAQANAAALAIQATAPAAAiQAAAXgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABg");
	this.shape_652.setTransform(-118,22.9);

	this.shape_653 = new cjs.Shape();
	this.shape_653.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_653.setTransform(-239.4,24.4);

	this.shape_654 = new cjs.Shape();
	this.shape_654.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_654.setTransform(106.3,-2.1);

	this.shape_655 = new cjs.Shape();
	this.shape_655.graphics.f("#FFFFFF").s().p("AgVArQgJgHAAgMQAAgNALgLQAIgHAMgGIgdAAIAAgMIAhgMIghAAIAAgNIA7AAIAAAOIgbAKIAbAAIAAALQgQAGgLAJIgIAGQgEAGABAGQABAJAJAAQAFAAAEgDQAEgEABgEQABgGgEgFIAMAAQAGAIgBAKQAAAMgJAIQgIAHgNABQgNAAgJgIg");
	this.shape_655.setTransform(96.7,-0.4);

	this.shape_656 = new cjs.Shape();
	this.shape_656.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgGgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_656.setTransform(86.6,-0.6);

	this.shape_657 = new cjs.Shape();
	this.shape_657.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgGgFQgFgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_657.setTransform(-3.9,-0.6);

	this.shape_658 = new cjs.Shape();
	this.shape_658.graphics.f("#FFFFFF").s().p("AgsAxQgSgQgBgYQABgVANgNQAIgHALgEIgEgDQgBgCAAgEQAAgEACgCQAFgJALgCIAWgBQATAAAMAJQAOAJADASIg1AAQgRAAgKAKQgJAKAAAPQgBAQAMANQANAMARAAQAMgBAKgEQAMgEAGgIQAHgLAAgKIALAAIAAAFQAAAJgFAKQgIAOgPAHQgPAIgTAAQgaAAgTgPgAgJgwQACABAAADQAAADgCACIArAAQgJgJgOgCIgKgBQgGAAgEADg");
	this.shape_658.setTransform(-31.4,0.6);

	this.shape_659 = new cjs.Shape();
	this.shape_659.graphics.f("#FFFFFF").s().p("AANBNQgKgFgBgKQAAgIAFgEQgVABgOgMQgPgMABgTQABgVATgKQgIgCACgKQgIADgHALQgGAMAAAMQAAAPAJAOQAGALAIAGIgQAAQgGgEgGgKQgIgPAAgRQAAgNAGgMQAEgLAHgGQAGgGAGgDQgCgCgBgHQAAgFACgDQAIgOAhAAQAeAAAIANQADAFAAAFQAAALgKADIACABQAHAKABAOIgmAAQgKAAgIAHQgHAHgBAKQgBAMAKAJQAJAJAOABQAVABAPgIIAAAMQgIADgGACQgNADAAAGQgBAEAEACQAEACAEAAQAHAAADgDQAFgDAAgGIAAgEIALAAIABAFQAAALgKAIQgHAGgNABIgFAAQgIAAgHgDgAgGgoQADACABAEQAAAEgCABIAEAAIAgAAQgFgIgJgDQgFgBgIAAQgHAAgEABgAAZgzIAFADQACgDABgCQAAgFgJgDQgIgDgJAAQgKAAgHACQgLADABAFIABAEIAFgBQAGgCAMAAQANAAAIACg");
	this.shape_659.setTransform(-77.2,-0.4);

	this.shape_660 = new cjs.Shape();
	this.shape_660.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgOAMgGIgVAAIAAgKIARAAQgDgEABgHQACgJAKgDQAIgFALABQAOABAIALQAJAMAAANIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAGQAKAFASgBQAqAAAAgnQAAgOgKgLQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQASIAEAGQABAFAAADIgOAAQAIAFAHAMQAGANABAMQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADACABACQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAFIAHADQABgEgHgGQgJgIgYAAQgcABAAAGg");
	this.shape_660.setTransform(-124.4,-2.2);

	this.shape_661 = new cjs.Shape();
	this.shape_661.graphics.f("#FFFFFF").s().p("AgzAjIAAgTIBbAAIAAggIAHgIQAEgGABgEIAABFg");
	this.shape_661.setTransform(-155.6,4.5);

	this.shape_662 = new cjs.Shape();
	this.shape_662.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_662.setTransform(-174.7,-2.1);

	this.shape_663 = new cjs.Shape();
	this.shape_663.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgIAGgDQgHgDAAgHIABgGQAFgMAXAAQASAAAGAKQAHgKAUAAQAJAAAHADQAIAFABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAWgQANQgQAOgdgBQgdABgRgPgAgpgLQgFAFABAJQAAANAQAGQALAFASgBQARABAMgFQAQgGAAgNQABgJgFgFQgFgGgIAAIg4AAQgJAAgEAGgAANglQgFADgBAGIAdAAIAAgCQAAgEgDgDQgEgDgEABIgCgBQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_663.setTransform(-258.3,-0.5);

	this.shape_664 = new cjs.Shape();
	this.shape_664.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAIQgKAJgMAAQgOgBgIgHg");
	this.shape_664.setTransform(91.4,-25.4);

	this.shape_665 = new cjs.Shape();
	this.shape_665.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgNAMgHIgVAAIAAgKIARAAQgDgEABgHQACgIAKgEQAIgFALABQAOABAIALQAJALAAAOIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAFQAKAGASAAQAqAAAAgpQAAgMgKgMQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQARIAEAHQABAFAAAEIgOAAQAIAEAHANQAGANABALQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADABABADQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAEIAHAFQABgFgHgGQgJgIgYABQgcAAAAAGg");
	this.shape_665.setTransform(47.3,-27.2);

	this.shape_666 = new cjs.Shape();
	this.shape_666.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARARAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_666.setTransform(1.9,-24.2);

	this.shape_667 = new cjs.Shape();
	this.shape_667.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_667.setTransform(-10.7,-20.5);

	this.shape_668 = new cjs.Shape();
	this.shape_668.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_668.setTransform(-56.7,-20.5);

	this.shape_669 = new cjs.Shape();
	this.shape_669.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAHgBAJQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_669.setTransform(-56.9,-26.9);

	this.shape_670 = new cjs.Shape();
	this.shape_670.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_670.setTransform(-69.6,-27.1);

	this.shape_671 = new cjs.Shape();
	this.shape_671.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAHgBAJQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgFAAIgIABg");
	this.shape_671.setTransform(-93.7,-26.9);

	this.shape_672 = new cjs.Shape();
	this.shape_672.graphics.f("#FFFFFF").s().p("AgSASQgIgGgDgKIAAgEQAAgNAIgIIAAAOIAFAAQAAAGAFAFQAFAFAHABQAHAAAGgHQAEgFgBgIIANAAIAAAGQAAAMgIAKQgJAIgNAAQgKAAgIgGg");
	this.shape_672.setTransform(-103.2,-19.3);

	this.shape_673 = new cjs.Shape();
	this.shape_673.graphics.f("#FFFFFF").s().p("AgrAiQgMgEAAgNQAAgJAGgDIANAAQgEACAAAGQAAAFAKAAQAJAAATgGQAOgGALAAIAEAAQAMADAFAGIAAgjQAGgFAGgLIAABHIgMAAQgHgLgOAAQgLAAgJAEQgSAIgPAAQgHAAgGgCg");
	this.shape_673.setTransform(-127.2,-20.4);

	this.shape_674 = new cjs.Shape();
	this.shape_674.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_674.setTransform(-160.6,-25.6);

	this.shape_675 = new cjs.Shape();
	this.shape_675.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAJAFAAAJQABAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQgBAIAHAGQAGAGAIAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_675.setTransform(-221.9,-25.6);

	this.shape_676 = new cjs.Shape();
	this.shape_676.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgJgDgBgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_676.setTransform(-257.6,-32.2);

	this.shape_677 = new cjs.Shape();
	this.shape_677.graphics.f("#FFFFFF").s().p("AgrAAIgghWICXBWIiXBXg");
	this.shape_677.setTransform(-285.9,-106.6);

	this.shape_678 = new cjs.Shape();
	this.shape_678.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAALgJQAMgJABgOIABgHQAAgOgJgLQgIgJgOAAIgpAAQgUAAAAgNQAAgXAyAAQAeAAALAJQAIAGAAAIQAAAIgHAFQAQASgBAbQAAAYgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABgAAXgyQgCACAAADQAAADACACQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDAAQgDAAgCACgAgcgvQgBAFAKAAIAcAAQgCgHADgFIgLgBQgZAAgCAIg");
	this.shape_678.setTransform(117.6,-81.1);

	this.shape_679 = new cjs.Shape();
	this.shape_679.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgEQAAgNAIgIIAAAOIAFAAQAAAGAGAFQAEAFAHABQAHAAAGgHQAEgFgBgIIAMAAIABAGQAAAMgJAKQgIAIgNAAQgKAAgIgGg");
	this.shape_679.setTransform(60.8,-73.3);

	this.shape_680 = new cjs.Shape();
	this.shape_680.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_680.setTransform(23.5,-74.5);

	this.shape_681 = new cjs.Shape();
	this.shape_681.graphics.f("#FFFFFF").s().p("AgoA3QgRgQAAgXQAAgRAHgMQAIgNAOAAQANAAACALQAEgMAOAAQALAAAHAJQAHAIAAAKQAAAOgJAKQgJAIgMABQgLAAgIgHQgJgGAAgLIADgPQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgTQAAgNgHgJQgGgMgOAAIhFAAIAAgLQASABAAgCIgDgCQgEgBAAgFQAAgNAPgFQAIgCASAAQAQAAAJADQANAEAJAKQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgIACABAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAgAAQALAAAHAEQAYAKAAAlQAAAbgQASQgQASgaAAQgYgBgQgQgAgOACQAAAGAEAEQAFAEAFABQAHgBAFgEQAFgEAAgHQgFAHgJABQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_681.setTransform(-9.7,-81.5);

	this.shape_682 = new cjs.Shape();
	this.shape_682.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_682.setTransform(-36.4,-74.5);

	this.shape_683 = new cjs.Shape();
	this.shape_683.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_683.setTransform(-36.8,-79.6);

	this.shape_684 = new cjs.Shape();
	this.shape_684.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDgBgHIABgGQAFgMAXAAQARgBAHALQAHgLATABQAKAAAHADQAIAFABAIQABAGgDAGQAEAEAEAHQADAIAAAIQAAAVgQAOQgQANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABAMAQAHQAMAFARAAQARAAAMgFQAPgHABgMQABgJgFgFQgFgGgIABIg5AAQgIgBgEAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgDgDQgEgCgFgBIgBAAQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgNgDIgGAAIgFABg");
	this.shape_684.setTransform(-63.6,-79.5);

	this.shape_685 = new cjs.Shape();
	this.shape_685.graphics.f("#FFFFFF").s().p("AgFAyIgNgEQgFgCgFAAQgKAAACAHIgNAAQgCgIAAgJQAAgKADgIQADgJAJgHIAPgMQAIgHABgHQAAgFgDgEQgCgEgGAAQgFAAgEADQgEAEAAAEQgBALAKAFIgSAAQgEgHgBgHIABgEQABgKAHgGQAIgFAKAAQAKAAAIAGQAGAHAAAKQAAAJgGAIIgNAMQgKAIgEAEQgFAHAAAJIAAAEQAEgCAEAAQAGAAAKADQAKADAFAAIALgBQAGgBAFgGQAGgGAAgJQACgIgHgHIgOgMQgHgIABgJQgBgKAIgGQAGgFALAAQAIAAAHAFQAGAFABAIQABAGgEAFQgDAFgFABQgEAAgEgDQgEgCAAgFQgBgIAIgCQgDgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgMAKgRAAQgIAAgHgCg");
	this.shape_685.setTransform(-81.8,-79.6);

	this.shape_686 = new cjs.Shape();
	this.shape_686.graphics.f("#FFFFFF").s().p("AgwA9QgOgFgEAAQgGAAgFAHIgKgGIATgbQgIgKAAgQQAAgFABgGQAEgQAPgJQALgGAPAAIAIAAIARgaIANAAIgRAbIAJAEIAIAFIAFgGQAGgEAHAAQAJAAAGAGQAHgHAKABQAcABABAvQABAXgIAOQgKASgTAAQgNAAgHgHQgHgHAAgMIAAgGIAMAAIAAAEQAAAPANAAQAJAAADgOQACgHAAgRQABgOgCgKQgDgNgFAAQgJAAAAAMIAAAZIgNAAIAAgZQAAgNgHAAQgEAAgCACQgCACgBADQALAMgBATQgBAYgOANQgMANgYABIgCAAQgJAAgMgEgAgzAwQAKAEAKAAIAJgBQANgCAHgKQAHgKAAgNQAAgNgHgIQgJgKgMgBIgLAQQADAAADAAQAHAAAFAEQAGAEABAHQAAAJgEAGQgFAHgHACQgGACgIAAQgLAAgJgEIgGAJIAEgBQAFAAAFADgAg0AaQACACAFABQAHABADgBQgGgBgDgFQgCgEACgEgAgiARQAAAFAGAAQAGAAAAgFQAAgGgGAAQgGAAAAAGgAg5gEQgEAFAAAIIABAIIAWggQgNACgGAJg");
	this.shape_686.setTransform(-156.4,-80.9);

	this.shape_687 = new cjs.Shape();
	this.shape_687.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_687.setTransform(-171.2,-74.5);

	this.shape_688 = new cjs.Shape();
	this.shape_688.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_688.setTransform(-171.5,-81);

	this.shape_689 = new cjs.Shape();
	this.shape_689.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAIAAAIQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAIQgKAJgMAAQgOgBgIgHg");
	this.shape_689.setTransform(-247.2,-79.4);

	this.shape_690 = new cjs.Shape();
	this.shape_690.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAHgNAPgFQAMgEATAAQAtAAAJAWQACADgBAEQABAOgKAEg");
	this.shape_690.setTransform(123.3,-111.2);

	this.shape_691 = new cjs.Shape();
	this.shape_691.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_691.setTransform(98,-106.1);

	this.shape_692 = new cjs.Shape();
	this.shape_692.graphics.f("#FFFFFF").s().p("AANBNQgLgFAAgKQAAgIAFgEQgVABgPgMQgOgMABgTQABgVATgKQgIgCABgKQgHADgHALQgGAMAAAMQAAAPAJAOQAGALAIAGIgQAAQgGgEgGgKQgIgPAAgRQAAgNAGgMQAEgLAHgGQAGgGAGgDQgCgCgBgHQAAgFACgDQAJgOAgAAQAfAAAHANQAEAFAAAFQAAALgLADQAKAKABAPIgmAAQgLAAgIAHQgHAHgBAKQgBAMAKAJQAKAJANABQAVABAPgIIAAAMQgIADgGACQgNADAAAGQgBAEAEACQADACAFAAQAHAAADgDQAFgDAAgGIAAgEIALAAIABAFQAAALgKAIQgHAGgNABIgFAAQgIAAgHgDgAgFgoQADACAAAEQAAAEgCABIAEAAIAgAAQgDgIgLgDQgFgBgIAAQgGAAgEABgAAVg4QAAADADACQABACADAAQAGAAAAgFQAAgDgCgCQgDgCgDAAQgFAAAAAFgAAAg/QgVACABAHIABAEIAFgBQAHgCAMAAIAGAAQgCgDAAgDIABgFIgDAAIgHABg");
	this.shape_692.setTransform(86.2,-104.4);

	this.shape_693 = new cjs.Shape();
	this.shape_693.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_693.setTransform(76.2,-104.4);

	this.shape_694 = new cjs.Shape();
	this.shape_694.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_694.setTransform(66.1,-103.5);

	this.shape_695 = new cjs.Shape();
	this.shape_695.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgNAMgHIgVAAIAAgKIARAAQgDgEABgHQACgIAKgFQAIgDALABQAOAAAIALQAJALAAAOIgcAAQgIAAgHAEQgHAEAAAIQAAAJALAFQAKAGASAAQAqgBAAgoQAAgNgKgLQgKgNgPAAIgwAAQgHAAgEgDQgDgFABgFQACgNAQgEQAMgEAWAAQAeAAAQARIAEAHQABAFAAAEIgOAAQAIADAHAOQAGANABALQAAAbgOASQgQASgbAAQgjAAgPgQgAgYgQQADACABAEQABAEgCAEIAWAAQgBgGgEgEQgFgEgHgBIgIABgAgbgvQAAACAJAAIAgAAQAJAAAKAEIAHAFQABgFgHgGQgJgHgYAAQgcgBAAAIg");
	this.shape_695.setTransform(32,-106.2);

	this.shape_696 = new cjs.Shape();
	this.shape_696.graphics.f("#FFFFFF").s().p("AgnAvQgSgPAAgXQAAgWAPgMQgBgLAEgHQAKgSASACQAKABAFAGQAHgIANABQAMAAAHAIQAHAJAAAUIgyAAIgVABQgBAOAKAIQAKAKAMgBQALgBAHgGQAIgFABgIIANAAQgDAOgMAJQgMAKgQAAQgeAAgMgVIgBAHQAAAQANAKQALAKAPAAQAYgBALgOQAIgIgBgLIANAAQAAAOgJAMQgQAXggAAQgXAAgRgNgAgTgrQgDAEABAGIAJgBIAKAAQABgNgKAAQgFAAgDAEgAALghIAUAAQAAgOgJAAQgLAAAAAOg");
	this.shape_696.setTransform(13.5,-103.8);

	this.shape_697 = new cjs.Shape();
	this.shape_697.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_697.setTransform(-43,-111.2);

	this.shape_698 = new cjs.Shape();
	this.shape_698.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgIQAEgIAGgEQgHgCAAgIIABgFQAGgNAWAAQARAAAHAKQAHgKATAAQAKAAAHAEQAIAFABAIQABAGgDAGQAEADAEAIQADAIAAAIQAAAVgQANQgQAPgdAAQgdAAgRgQgAgpgLQgFAFAAAIQABANAQAHQAMAEARABQARgBAMgEQAPgHABgNQABgIgFgFQgFgFgIAAIg4AAQgJAAgEAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgDgCQgEgDgEAAIgCAAQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_698.setTransform(-43,-104.5);

	this.shape_699 = new cjs.Shape();
	this.shape_699.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgGAAIgHABg");
	this.shape_699.setTransform(-81.9,-105.9);

	this.shape_700 = new cjs.Shape();
	this.shape_700.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_700.setTransform(-113.5,-106.1);

	this.shape_701 = new cjs.Shape();
	this.shape_701.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAIAFIgKAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADAAAEQABAOgKAEg");
	this.shape_701.setTransform(-138.7,-111.2);

	this.shape_702 = new cjs.Shape();
	this.shape_702.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_702.setTransform(-139,-104.6);

	this.shape_703 = new cjs.Shape();
	this.shape_703.graphics.f("#FFFFFF").s().p("AgnAvQgSgPAAgXQAAgWAPgMQgBgLAEgHQAKgSASACQAKABAFAGQAHgIANABQAMAAAHAIQAHAJAAAUIgyAAIgVABQgBAOAKAIQAKAKAMgBQALgBAHgGQAIgFABgIIANAAQgDAOgMAJQgMAKgQAAQgeAAgMgVIgBAHQAAAQANAKQALAKAPAAQAYgBALgOQAIgIgBgLIANAAQAAAOgJAMQgQAXggAAQgXAAgRgNgAgTgrQgDAEABAGIAJgBIAKAAQABgNgKAAQgFAAgDAEgAALghIAUAAQAAgOgJAAQgLAAAAAOg");
	this.shape_703.setTransform(-157.7,-103.8);

	this.shape_704 = new cjs.Shape();
	this.shape_704.graphics.f("#FFFFFF").s().p("AgyA1QgKgMABgQQABgTANgHIgRAAIAAgMIAVAAQgEgCAAgGQAAgDABgDQAHgLASAAIAIAAQANACAIAKQAIAKAAAPIglAAQgKAAgHAFQgHAGgBAKQAAAIAGAGQAGAGAKAAQAHAAAFgFQAFgEAAgHIAAgOIALAAIAAAOQAAAHAFAFQAFAEAIAAQAUAAADgkQABgWgOgRQgOgSgVAAQgfgBgLAUIgOAAQAPgfApAAQAYABARAOQAMALAGAQQAGAQgBARQgBAXgJAOQgKASgTAAQgQAAgIgKQgHAKgQAAIgBABQgPAAgLgNgAgXgZQADAEAAACQAAAEgCACIAZAAQgDgJgGgCQgEgCgGAAIgHABg");
	this.shape_704.setTransform(-195.8,-105.9);

	this.shape_705 = new cjs.Shape();
	this.shape_705.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgEQAAgNAIgIIAAANIAFAAQAAAIAGAEQAFAFAGAAQAIABAEgHQAGgFgBgIIALAAIABAGQAAAMgJAJQgIAJgNAAQgKAAgIgGg");
	this.shape_705.setTransform(-205.3,-98.3);

	this.shape_706 = new cjs.Shape();
	this.shape_706.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgFIAABHg");
	this.shape_706.setTransform(-229.1,-99.5);

	this.shape_707 = new cjs.Shape();
	this.shape_707.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_707.setTransform(-239.4,-104.6);

	this.shape_708 = new cjs.Shape();
	this.shape_708.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_708.setTransform(-248.2,-106.1);

	this.shape_709 = new cjs.Shape();
	this.shape_709.graphics.f("#FFFFFF").s().p("AgrAAIgghWICXBWIiXBXg");
	this.shape_709.setTransform(-285.9,-205.6);

	this.shape_710 = new cjs.Shape();
	this.shape_710.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgLADQgOADgCAFIgBAEQABAIAHAFIgKAAQgLgDABgPQgBgFACgCQAHgNAPgFQAMgEATAAQAtAAAJAWQACADgBAEQABAOgLAEg");
	this.shape_710.setTransform(128.7,-160.2);

	this.shape_711 = new cjs.Shape();
	this.shape_711.graphics.f("#FFFFFF").s().p("AgOA0IgJgCIAAgLIAEABQAGAAgBgGQAAgDgGgGQgEgHAAgHQgBgIAFgEQAEgEAIgCQAHgBAEADQgBgPgBgGQgDgLgIgCQgHgBgFADIAAgLQAIgEAKABQAPABAIARQAHAOgBATQAAAWgKAPQgLAPgPAAIgDAAgAgFAFQgGABAAAHIAAADIAGAIQACAFAAAEQADgFABgJQABgGAAgGQgCgDgDAAIgCABg");
	this.shape_711.setTransform(77.9,-153.6);

	this.shape_712 = new cjs.Shape();
	this.shape_712.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_712.setTransform(57.8,-153.6);

	this.shape_713 = new cjs.Shape();
	this.shape_713.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_713.setTransform(49.1,-155.1);

	this.shape_714 = new cjs.Shape();
	this.shape_714.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgOgNAAgWQAAgOAGgKQAEgKANgLIAWgVIAKgPIAWAAQgEAHgGAHQgHAHgJAHQAPgBALAEQAfAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgTAAgKAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAGAEQAFAFAAAJQAAAIgHAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_714.setTransform(37.1,-155.4);

	this.shape_715 = new cjs.Shape();
	this.shape_715.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDABgPQAAgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_715.setTransform(24.9,-160.2);

	this.shape_716 = new cjs.Shape();
	this.shape_716.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgZQAAgQAHgMQAIgNAOAAQANAAACALQAEgMAOAAQAKAAAIAJQAHAJAAAKQAAANgJAJQgJAJgMAAQgLABgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQANgMAAgTQgBgNgGgKQgHgLgMAAIhGAAIAAgLQATABgBgBIgDgDQgEgCAAgEQAAgNAPgFQAIgBATAAQAOAAAKACQANAEAJAKQAGAIABALIgMAAQgCgLgMgGQgLgFgPAAQgLAAgFABQgIADAAAGQAAABABAAQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIADQAXALAAAlQAAAbgQASQgRASgZAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAABgBQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBABAAAAg");
	this.shape_716.setTransform(5.4,-155.5);

	this.shape_717 = new cjs.Shape();
	this.shape_717.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_717.setTransform(-35.8,-155.1);

	this.shape_718 = new cjs.Shape();
	this.shape_718.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_718.setTransform(-48.8,-153.6);

	this.shape_719 = new cjs.Shape();
	this.shape_719.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_719.setTransform(-82.1,-153.6);

	this.shape_720 = new cjs.Shape();
	this.shape_720.graphics.f("#FFFFFF").s().p("AAAA7QgIAMgRAAQgOAAgKgLQgJgMABgPQABgOAKgJIgPAAIAAgLIAVAAQgEgBAAgIIABgGQAGgLAXAAQAOAAAIAJQAJAKAAANIgBAFIgoAAQgIAAgFAFQgEAFAAAHQAAAIAFAFQAGAGAHAAQAHAAAFgEQAFgDAAgHIAAgOIAMAAIAAAOQAAAHAGAEQAGADAHAAQALAAAHgMQAFgKAAgOQAAgSgMgMQgLgLgPAAIguAAQgQAAAAgOQAAgZA0ABQAcAAANAMQAKAJAAAMIgMAAQAAgHgIgGQgLgIgUAAQgfAAAAAJQAAAFALAAIAhAAQASAAAPAPQAQAQAAAdQAAAVgJAQQgLARgSAAQgRAAgHgMgAgVgOQADACABAEQABAEgDADIAVAAQgBgGgDgEQgEgEgIAAIgHABg");
	this.shape_720.setTransform(-94.9,-155.5);

	this.shape_721 = new cjs.Shape();
	this.shape_721.graphics.f("#FFFFFF").s().p("AgnAvQgSgPAAgXQAAgWAPgMQgBgLAEgHQAKgSASACQAKABAFAGQAHgIANABQAMAAAHAIQAHAJAAAUIgyAAIgVABQgBAOAKAIQAKAKAMgBQALgBAHgGQAIgFABgIIANAAQgDAOgMAJQgMAKgQAAQgeAAgMgVIgBAHQAAAQANAKQALAKAPAAQAYgBALgOQAIgIgBgLIANAAQAAAOgJAMQgQAXggAAQgXAAgRgNgAgTgrQgDAEABAGIAJgBIAKAAQABgNgKAAQgFAAgDAEgAALghIAUAAQAAgOgJAAQgLAAAAAOg");
	this.shape_721.setTransform(-113.4,-152.8);

	this.shape_722 = new cjs.Shape();
	this.shape_722.graphics.f("#FFFFFF").s().p("AgwA9QgOgFgEAAQgGAAgFAHIgKgGIATgbQgIgKAAgQQAAgFABgGQAEgQAPgJQALgGAPAAIAIAAIARgaIANAAIgRAbIAJAEIAIAFIAFgGQAGgEAHAAQAJAAAGAGQAHgHAKABQAcABABAvQABAXgIAOQgKASgTAAQgNAAgHgHQgHgHAAgMIAAgGIAMAAIAAAEQAAAPANAAQAJAAADgOQACgHAAgRQABgOgCgKQgDgNgFAAQgJAAAAAMIAAAZIgNAAIAAgZQAAgNgHAAQgEAAgCACQgCACgBADQALAMgBATQgBAYgOANQgMANgYABIgCAAQgJAAgMgEgAgzAwQAKAEAKAAIAJgBQANgCAHgKQAHgKAAgNQAAgNgHgIQgJgKgMgBIgLAQQADAAADAAQAHAAAFAEQAGAEABAHQAAAJgEAGQgFAHgHACQgGACgIAAQgLAAgJgEIgGAJIAEgBQAFAAAFADgAg0AaQACACAFABQAHABADgBQgGgBgDgFQgCgEACgEgAgiARQAAAFAGAAQAGAAAAgFQAAgGgGAAQgGAAAAAGgAg5gEQgEAFAAAIIABAIIAWggQgNACgGAJg");
	this.shape_722.setTransform(-161.9,-154.9);

	this.shape_723 = new cjs.Shape();
	this.shape_723.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_723.setTransform(-181.7,-155.1);

	this.shape_724 = new cjs.Shape();
	this.shape_724.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAIgMASAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAFADAIAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEAAAHIAbAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgEgDgGAAQgEAAgEACg");
	this.shape_724.setTransform(-240.4,-153.6);

	this.shape_725 = new cjs.Shape();
	this.shape_725.graphics.f("#FFFFFF").s().p("AgkBCQgWgBgOgOQgNgNgBgWQAAgOAGgKQAFgKAMgLIAXgVIAKgPIAWAAQgEAHgHAHQgGAHgKAHQAQgBALAEQAeALAAAlQAAAXgRAPQgPAOgXAAIgDAAgAg9gOQgJAKAAANQAAAMAIAJQAMALASAAQAPAAAKgIQAMgKAAgQQAAgHgDgGQgCgKgKgFQgJgFgMAAQgTAAgLAMgAAgA3QgIgKAAgSQAAgWASgcQAIgNANgQIhBAAIAAgMIBRAAIAAALQgOAPgIAOQgPAYAAAUQAAAVARAAQAGAAAFgDQAEgEAAgGQAAgEgCgFIALAAQAEAGAAALQAAAOgJAIQgJAIgOAAQgPAAgIgLg");
	this.shape_725.setTransform(-255.8,-155);

	this.shape_726 = new cjs.Shape();
	this.shape_726.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgEQgHgDAAgHIABgGQAGgMAWAAQARgBAHALQAIgLASABQAKAAAHADQAIAFABAIQABAGgDAGQAEAEADAHQAEAIAAAIQAAAVgQAOQgQANgdAAQgdAAgRgOgAgpgLQgFAFABAJQAAAMAQAHQAMAFARAAQARAAAMgFQAPgHABgMQABgJgFgFQgFgGgIABIg4AAQgJgBgEAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgDgDQgEgCgEgBIgCAAQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQAAgJgNgDIgGAAIgFABg");
	this.shape_726.setTransform(23,-178.5);

	this.shape_727 = new cjs.Shape();
	this.shape_727.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_727.setTransform(10.1,-172.7);

	this.shape_728 = new cjs.Shape();
	this.shape_728.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgNAMgHIgVAAIAAgKIARAAQgDgEABgHQACgIAKgEQAIgFALABQAOABAIALQAJALAAAOIgcAAQgIAAgHAEQgHAFAAAGQAAAKALAFQAKAGASAAQAqAAAAgpQAAgMgKgMQgKgMgPAAIgwAAQgHAAgEgFQgDgEABgFQACgMAQgGQAMgDAWAAQAeAAAQARIAEAHQABAFAAAEIgOAAQAIAEAHANQAGANABALQAAAcgOAQQgQATgbAAQgjAAgPgQgAgYgPQADABABADQABAFgCAEIAWAAQgBgGgEgEQgFgFgHABIgIABgAgbgwQAAADAJAAIAgAAQAJAAAKAEIAHAFQABgFgHgGQgJgIgYABQgcAAAAAGg");
	this.shape_728.setTransform(-4.5,-180.2);

	this.shape_729 = new cjs.Shape();
	this.shape_729.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgIQAEgGABgEIAABGg");
	this.shape_729.setTransform(-37,-173.5);

	this.shape_730 = new cjs.Shape();
	this.shape_730.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_730.setTransform(-36.9,-180);

	this.shape_731 = new cjs.Shape();
	this.shape_731.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_731.setTransform(-74.8,-180);

	this.shape_732 = new cjs.Shape();
	this.shape_732.graphics.f("#FFFFFF").s().p("AgyAJQgEgGAAgHQAAgGADgFIAMAAQgCADAAADQAAAFADADQALALAbAAQAQAAAMgFQAHgDAEgCQAEgEAAgHQAAgHgGgGIAIgGQAKAIAAAPQAAAHgCAEQgKAbgrAAQglAAgNgWg");
	this.shape_732.setTransform(-101.7,-172.7);

	this.shape_733 = new cjs.Shape();
	this.shape_733.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgEQgHgDAAgHIABgGQAFgMAXAAQASgBAGALQAHgLAUABQAJAAAHADQAIAFABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAVgQAOQgQANgdAAQgdAAgRgOgAgpgLQgFAFABAJQAAAMAQAHQALAFASAAQARAAAMgFQAQgHAAgMQABgJgFgFQgFgGgIABIg4AAQgJgBgEAGgAANglQgFAEgBAFIAdAAIAAgDQAAgDgDgDQgEgCgEgBIgCAAQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQgBgJgNgDIgFAAIgFABg");
	this.shape_733.setTransform(-101.7,-178.5);

	this.shape_734 = new cjs.Shape();
	this.shape_734.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_734.setTransform(-117.2,-178.6);

	this.shape_735 = new cjs.Shape();
	this.shape_735.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARARAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_735.setTransform(-125.8,-177.2);

	this.shape_736 = new cjs.Shape();
	this.shape_736.graphics.f("#FFFFFF").s().p("AgMAVQAKgGAAgGIAAgBIgGgDQgGgFAAgFQAAgFADgEQAEgGAHAAQAFAAAEACQAGAFAAAHQAAAFgDAFQgBADgFAGIgIAIg");
	this.shape_736.setTransform(-151.3,-173.9);

	this.shape_737 = new cjs.Shape();
	this.shape_737.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_737.setTransform(-161.4,-185.2);

	this.shape_738 = new cjs.Shape();
	this.shape_738.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgGgFQgFgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_738.setTransform(-175.3,-178.6);

	this.shape_739 = new cjs.Shape();
	this.shape_739.graphics.f("#FFFFFF").s().p("AAAA7QgIAMgRAAQgOAAgKgLQgJgMABgPQABgOAKgJIgPAAIAAgLIAVAAQgEgBAAgIIABgGQAGgLAXAAQAOAAAIAJQAJAKAAANIgBAFIgoAAQgIAAgFAFQgEAFAAAHQAAAIAFAFQAGAGAHAAQAHAAAFgEQAFgDAAgHIAAgOIAMAAIAAAOQAAAHAGAEQAGADAHAAQALAAAHgMQAFgKAAgOQAAgSgMgMQgLgLgPAAIguAAQgQAAAAgOQAAgZA0ABQAcAAANAMQAKAJAAAMIgMAAQAAgHgIgGQgLgIgUAAQgfAAAAAJQAAAFALAAIAhAAQASAAAPAPQAQAQAAAdQAAAVgJAQQgLARgSAAQgRAAgHgMgAgVgOQADACABAEQABAEgDADIAVAAQgBgGgDgEQgEgEgIAAIgHABg");
	this.shape_739.setTransform(-188.1,-180.5);

	this.shape_740 = new cjs.Shape();
	this.shape_740.graphics.f("#FFFFFF").s().p("AgrAzQgQgQAAgaQABgSANgMQANgNASAAIAOAAIAAAMIgMAAQgOAAgIAIQgIAJgBALQgBARAOAKQANAJARgBQATAAANgNQAMgOAAgSQAAgUgNgOQgNgPgSgBQgOAAgLAGQgMAGgFAKIgMAAQAGgQAOgJQAOgJAUAAQAaAAARAUQARATAAAbQAAAdgRATQgSATgaAAQgbAAgPgQg");
	this.shape_740.setTransform(-218.1,-180);

	this.shape_741 = new cjs.Shape();
	this.shape_741.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARARAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKAAQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_741.setTransform(-230.7,-177.2);

	this.shape_742 = new cjs.Shape();
	this.shape_742.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgIQAEgIAFgEQgFgCAAgIIABgFQAFgNAWAAQASAAAGAKQAHgKAUAAQAJAAAHAEQAIAFABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAVgPANQgRAPgdAAQgdAAgRgQgAgpgLQgFAFABAIQAAANAPAHQAMAEASABQARgBAMgEQAQgHABgNQAAgIgFgFQgFgFgIAAIg5AAQgHAAgFAFgAANgkQgFACgBAHIAdAAIAAgEQAAgEgEgCQgDgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_742.setTransform(101.9,-203.5);

	this.shape_743 = new cjs.Shape();
	this.shape_743.graphics.f("#FFFFFF").s().p("AgsAwQgSgPAAgYQAAgWANgMQAIgHAKgEIgDgDQgBgDAAgDQAAgDACgDQAFgIALgDIAWgBQATAAAMAJQAOAKADARIg1AAQgRAAgKALQgJAJAAAQQgBAPAMANQANAMAQAAQANAAAKgEQANgFAFgIQAIgLgBgJIALAAIABAEQgBAKgFAJQgIAOgPAIQgPAHgTAAQgaAAgTgQgAgJgxQACACAAADQAAADgCACIArAAQgJgJgOgCIgKgBQgGAAgEACg");
	this.shape_743.setTransform(89.1,-202.4);

	this.shape_744 = new cjs.Shape();
	this.shape_744.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_744.setTransform(68.7,-203.6);

	this.shape_745 = new cjs.Shape();
	this.shape_745.graphics.f("#FFFFFF").s().p("AgnAvQgSgPAAgXQAAgWAPgMQgBgLAEgHQAKgSASACQAKABAFAGQAHgIANABQAMAAAHAIQAHAJAAAUIgyAAIgVABQgBAOAKAIQAKAKAMgBQALgBAHgGQAIgFABgIIANAAQgDAOgMAJQgMAKgQAAQgeAAgMgVIgBAHQAAAQANAKQALAKAPAAQAYgBALgOQAIgIgBgLIANAAQAAAOgJAMQgQAXggAAQgXAAgRgNgAgTgrQgDAEABAGIAJgBIAKAAQABgNgKAAQgFAAgDAEgAALghIAUAAQAAgOgJAAQgLAAAAAOg");
	this.shape_745.setTransform(40.3,-202.8);

	this.shape_746 = new cjs.Shape();
	this.shape_746.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgIQAEgIAFgEQgFgCgBgIIABgFQAFgNAXAAQARAAAHAKQAHgKATAAQAKAAAHAEQAIAFABAIQABAGgDAGQAEADAEAIQADAIAAAIQAAAVgQANQgQAPgdAAQgdAAgRgQgAgpgLQgFAFAAAIQABANAQAHQAMAEARABQARgBAMgEQAPgHABgNQABgIgFgFQgFgFgIAAIg4AAQgJAAgEAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgDgCQgEgDgFAAIgBAAQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_746.setTransform(27.7,-203.5);

	this.shape_747 = new cjs.Shape();
	this.shape_747.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_747.setTransform(12.3,-203.6);

	this.shape_748 = new cjs.Shape();
	this.shape_748.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgGgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgDAEIAXAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_748.setTransform(3.3,-203.6);

	this.shape_749 = new cjs.Shape();
	this.shape_749.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQAEACAGAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_749.setTransform(-5.8,-203.6);

	this.shape_750 = new cjs.Shape();
	this.shape_750.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_750.setTransform(-14.5,-205.1);

	this.shape_751 = new cjs.Shape();
	this.shape_751.graphics.f("#FFFFFF").s().p("AgCBGQgXgBgNgOQgOgNAAgWQAAgOAFgKQAGgKALgLIAYgVIAJgPIAVAAQgDAHgHAHQgGAHgJAHQAPgBAKAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAPAAAJgIQAOgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgSAAgLAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAFAEQAGAFABAJQAAAIgIAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_751.setTransform(-36.8,-205.4);

	this.shape_752 = new cjs.Shape();
	this.shape_752.graphics.f("#FFFFFF").s().p("AgMAVQAKgHAAgFIAAgBIgGgEQgGgEAAgFQAAgFADgEQAEgGAHAAQAFAAAEACQAGAFAAAHQAAAGgDAEQgBAEgFAFIgIAIg");
	this.shape_752.setTransform(-65.2,-198.9);

	this.shape_753 = new cjs.Shape();
	this.shape_753.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgFIAABHg");
	this.shape_753.setTransform(-98.9,-198.5);

	this.shape_754 = new cjs.Shape();
	this.shape_754.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_754.setTransform(-115.4,-203.6);

	this.shape_755 = new cjs.Shape();
	this.shape_755.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_755.setTransform(-124.4,-203.6);

	this.shape_756 = new cjs.Shape();
	this.shape_756.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQAOAAAJASQAIAQAAASQAAATgIAPQgKARgOAAQgJAAgGgDg");
	this.shape_756.setTransform(-144,-203.6);

	this.shape_757 = new cjs.Shape();
	this.shape_757.graphics.f("#FFFFFF").s().p("AgtA1QgKgMABgNQACgNAMgHIgVAAIAAgKIARAAQgDgEABgHQACgIAKgFQAIgDALABQAOAAAIALQAJALAAAOIgcAAQgIAAgHAEQgHAEAAAIQAAAJALAFQAKAGASAAQAqgBAAgoQAAgNgKgLQgKgNgPAAIgwAAQgHAAgEgDQgDgFABgFQACgNAQgEQAMgEAWAAQAeAAAQARIAEAHQABAFAAAEIgOAAQAIADAHAOQAGANABALQAAAbgOASQgQASgbAAQgjAAgPgQgAgYgQQADACABAEQABAEgCAEIAWAAQgBgGgEgEQgFgEgHgBIgIABgAgbgvQAAACAJAAIAgAAQAJAAAKAEIAHAFQABgFgHgGQgJgHgYAAQgcgBAAAIg");
	this.shape_757.setTransform(-166.9,-205.2);

	this.shape_758 = new cjs.Shape();
	this.shape_758.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAAMgJQALgJABgOQABgUgKgMQgHgJgOAAIg+AAIAAgKQAOAAAAgBQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBgBgBAAQgEgCAAgDQgCgRAvAAQATAAAPAIQARAJABAPIgOAAQABgJgOgFQgLgFgOAAQgXAAgBAHQgBAFAKAAIAcAAQANAAALAIQATAPAAAiQAAAXgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABg");
	this.shape_758.setTransform(-185.3,-205.1);

	this.shape_759 = new cjs.Shape();
	this.shape_759.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_759.setTransform(-204.1,-203.6);

	this.shape_760 = new cjs.Shape();
	this.shape_760.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOAAgVQgBgTAMgOQgDgBgEgEIgFgIIgCgLQABgKAIgKQAJgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgJAFQAPgBALAEQAfAMABAkQgBAXgQAQQgQAOgXAAIgDAAgAgcgMQgJALAAAMQABANAHAIQAMALASAAQAOAAAKgHQAOgLAAgQQAAgHgDgGQgEgJgKgGQgJgFgLAAQgTAAgLAMgAgjgrQgFAGAAAFQABAHAFADIAEgEIAYgWQgCgEgGgBIgDAAQgMAAgGAKg");
	this.shape_760.setTransform(-226.2,-205.2);

	this.shape_761 = new cjs.Shape();
	this.shape_761.graphics.f("#FFFFFF").s().p("AgcA9QgNgGgHgLQgLgPAAgYQAAgPAJgNQAJgNAMAAQALABAEAJQAGgLAMAAQALAAAIAJQAIAJABALQABANgKALQgLAKgNAAQgNAAgJgIQgIgIAAgLIACgJIADgHQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQgCgCgDABQgGAAgDAIQgDAHAAAJQgBARAOAMQANALARAAQASAAAOgOQANgNAAgTQgBgWgLgOQgMgOgWAAQgcAAgLAUIgNAAQAGgQAOgIQAOgIASAAQAcAAAPAPQASATABAdQABAegRAUQgQAUgdAAQgOAAgNgGgAgRgFQAAAGAGADQAFAEAGAAQAHABAHgGQAGgEAAgGIgBgEQgBAEgFADQgFADgGAAQgNAAgDgMIgDAIgAgCgTQAAADACACQABACADAAQADAAADgCQACgCAAgDQAAgDgDgCQgCgCgDAAQgGAAAAAHg");
	this.shape_761.setTransform(-238.3,-205.1);

	this.shape_762 = new cjs.Shape();
	this.shape_762.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEADQAEADAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBAKQgBANgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_762.setTransform(-248.1,-203.4);

	this.shape_763 = new cjs.Shape();
	this.shape_763.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_763.setTransform(-258.2,-202.5);

	this.shape_764 = new cjs.Shape();
	this.shape_764.graphics.f("#FFFFFF").s().p("Ag5BtIAAgcIAGAAQANAAAIgEQAHgFAEgMIAEgOIg3icIAmAAIAgBrIAhhrIAmAAIg/C1QgOAogiAAQgIAAgJgCg");
	this.shape_764.setTransform(398.4,-249.7);

	this.shape_765 = new cjs.Shape();
	this.shape_765.graphics.f("#FFFFFF").s().p("AgvA9QgWgVAAgkIAAgEQAAgXAKgTQAJgSAQgLQARgKATAAQAhAAARAUQASAVAAAmIAAANIhnAAQACAUALALQAMAMAQAAQAZAAAPgUIATASQgJAPgQAHQgQAIgUAAQghAAgUgVgAgVgqQgJAKgCASIBDAAIAAgCQgBgSgJgJQgIgKgPAAQgOAAgJALg");
	this.shape_765.setTransform(383.2,-252.9);

	this.shape_766 = new cjs.Shape();
	this.shape_766.graphics.f("#FFFFFF").s().p("AggBLQgPgHgJgMQgJgMAAgPIAjAAQABANAIAHQAKAHANAAQAOAAAHgGQAIgFAAgJQAAgJgIgFQgIgFgRgEQgSgEgMgGQgbgMAAgYQAAgVARgNQARgOAaAAQAdAAARAOQARAOABAWIgkAAQAAgKgIgHQgHgHgNAAQgLAAgHAGQgIAFABAJQAAAJAGAEQAHAFAUAEQAVAFAMAGQAMAGAGAJQAGAJAAAMQgBAWgRANQgSANgcAAQgTAAgPgHg");
	this.shape_766.setTransform(350.5,-252.9);

	this.shape_767 = new cjs.Shape();
	this.shape_767.graphics.f("#FFFFFF").s().p("AhPBrIAAjVIA/AAQAcAAAWANQAVANANAXQAMAXAAAeIAAAKQAAAegMAXQgNAXgWAMQgWANgcAAgAgqBNIAZAAQAcAAAPgSQAQgSAAgiIAAgLQAAgjgPgSQgPgTgcAAIgaAAg");
	this.shape_767.setTransform(325.7,-255.7);

	this.shape_768 = new cjs.Shape();
	this.shape_768.graphics.f("#FFFFFF").s().p("AgnCFQAVgQANgkQAMgiAAgtIAAgDQAAgogKghQgMgigUgUIgEgFIAHgUQASAKARAXQARAXAKAdQAJAcABAeIABALQgBAggIAfQgKAegRAZQgSAZgTAJg");
	this.shape_768.setTransform(302.2,-253.5);

	this.shape_769 = new cjs.Shape();
	this.shape_769.graphics.f("#FFFFFF").s().p("AgvA9QgWgVAAgkIAAgEQAAgXAKgTQAJgSAQgLQARgKATAAQAhAAARAUQASAVAAAmIAAANIhnAAQACAUALALQAMAMAQAAQAZAAAPgUIATASQgJAPgQAHQgQAIgUAAQghAAgUgVgAgVgqQgJAKgCASIBDAAIAAgCQgBgSgJgJQgIgKgPAAQgOAAgJALg");
	this.shape_769.setTransform(279.1,-252.9);

	this.shape_770 = new cjs.Shape();
	this.shape_770.graphics.f("#FFFFFF").s().p("AgSA3IAAhZIgaAAIAAgaIAaAAIAAgnIAjAAIAAAnIAcAAIAAAaIgcAAIAABXQAAAKADAEQAEADAJAAQAGAAAGgBIAAAbQgMAEgLAAQgoAAAAgtg");
	this.shape_770.setTransform(265.5,-254.7);

	this.shape_771 = new cjs.Shape();
	this.shape_771.graphics.f("#FFFFFF").s().p("AgRBxIAAjgIAjAAIAADgg");
	this.shape_771.setTransform(257.1,-256.2);

	this.shape_772 = new cjs.Shape();
	this.shape_772.graphics.f("#FFFFFF").s().p("AApBrIgoibIgpCbIgjAAIgyjVIAlAAIAhCgIApigIAeAAIAoCgIAhigIAlAAIgxDVg");
	this.shape_772.setTransform(223.9,-255.7);

	this.shape_773 = new cjs.Shape();
	this.shape_773.graphics.f("#FFFFFF").s().p("AgDB4QgSgYgJgeQgJgfAAgjQAAgiAJgfQAJgeASgYQAQgYAUgKIAHAWQgVAPgMAhQgMAfgBAqIgBALQAAAtAMAiQAMAkAXARIgHAUQgUgJgQgYg");
	this.shape_773.setTransform(206,-253.5);

	this.shape_774 = new cjs.Shape();
	this.shape_774.graphics.f("#FFFFFF").s().p("AgSA3IAAhZIgaAAIAAgaIAaAAIAAgnIAjAAIAAAnIAcAAIAAAaIgcAAIAABXQAAAKADAEQAEADAJAAQAGAAAGgBIAAAbQgMAEgLAAQgoAAAAgtg");
	this.shape_774.setTransform(187.3,-254.7);

	this.shape_775 = new cjs.Shape();
	this.shape_775.graphics.f("#FFFFFF").s().p("AgRBxIAAjgIAjAAIAADgg");
	this.shape_775.setTransform(178.9,-256.2);

	this.shape_776 = new cjs.Shape();
	this.shape_776.graphics.f("#FFFFFF").s().p("Ag0BFQgPgOAAgUQAAgZATgNQATgOAiAAIAVAAIAAgKQAAgMgGgIQgHgHgNAAQgNAAgHAGQgIAGAAAJIgkAAQAAgNAJgLQAJgLAPgHQAOgGASAAQAbAAARAOQARAOAAAZIAABHQAAAVAGANIAAACIgkAAQgDgEgBgLQgRASgYAAQgYAAgPgNgAgWAMQgJAHAAAMQAAAKAGAHQAHAGAMAAQAJAAAKgGQAJgFAEgIIAAgeIgTAAQgTAAgKAHg");
	this.shape_776.setTransform(166.9,-252.9);

	this.shape_777 = new cjs.Shape();
	this.shape_777.graphics.f("#FFFFFF").s().p("AApBrIgoibIgpCbIgjAAIgyjVIAlAAIAhCgIApigIAeAAIAoCgIAhigIAlAAIgxDVg");
	this.shape_777.setTransform(145.7,-255.7);

	this.shape_778 = new cjs.Shape();
	this.shape_778.graphics.f("#FFFFFF").s().p("AAzAkQAMgIAAgMIAAgFQgDgJgTgEQgSgEgXAAQgWAAgRAEQgUAEgDAJIgBAFQAAANAMAHIgQAAQgPgFgBgVQABgIACgEQAJgTAYgIQASgGAdAAQBDAAAOAhQADAFAAAHQAAAUgPAGg");
	this.shape_778.setTransform(95.7,-262.7);

	this.shape_779 = new cjs.Shape();
	this.shape_779.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgPAAIAAgOIApAAQAMAAAJAGQAJAHgBANQAAALgKAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAABgJQgBgKgLAAIgDAAg");
	this.shape_779.setTransform(81.4,-263.4);

	this.shape_780 = new cjs.Shape();
	this.shape_780.graphics.f("#FFFFFF").s().p("AADA8QgLATgeAAQgXAAgPgSQgPgRAAgXQABgaARgMIgYAAIAAgSIAYAAQgFgDAAgJIACgIQAHgXAiAAQAcAAAJATQAMgTAbAAQAQAAALAHQAMAIABAOQACAMgFAHIgEAAQAYATgBAdQAAAZgPARQgQATgXAAQgbgBgNgSgAANAFIAAARQAAAJAKAFQAIAFAMAAQAMAAAJgKQALgJgBgNQAAgLgJgIQgJgIgMAAIhQAAQgNAAgIAJQgKAIAAALQAAANAJAJQAIAJAOAAQAMAAAIgEQALgFgBgKIAAgRgAAlg7QgKAAgHAHQgGAHAAAKIApAAQADgEAAgGQgCgOgQAAIgDAAgAgtg5QAFAEABAHQAAAGgDAFIAiAAQgBgNgKgHQgGgEgJAAQgGAAgFACg");
	this.shape_780.setTransform(74.9,-252.8);

	this.shape_781 = new cjs.Shape();
	this.shape_781.graphics.f("#FFFFFF").s().p("AAABYQgNASgZAAQgVAAgPgRQgOgRACgWQABgWAPgNIgWAAIAAgRIAfAAQgGgDAAgKQAAgFACgEQAJgSAiAAQAVAAAMAPQAOAOAAAUIgBAIIg9AAQgLAAgHAHQgIAHAAAMQAAALAIAIQAJAIALAAQALAAAHgFQAIgFAAgKIAAgVIASAAIAAAVQAAAKAJAFQAIAFAMAAQARAAAJgRQAHgPAAgVQAAgcgQgSQgRgRgXAAIhFAAQgYAAAAgUQAAgmBNABQAqAAAVATQAOANAAASIgSAAQABgLgMgJQgRgMgeAAQgvAAAAAOQAAAIAQAAIAyAAQAcAAAWAWQAYAZAAArQAAAggOAXQgQAagbAAQgZAAgLgSgAghgWQAGADABAGQABAGgEAFIAfAAQAAgJgGgGQgGgHgMAAIgLACg");
	this.shape_781.setTransform(55.7,-255.6);

	this.shape_782 = new cjs.Shape();
	this.shape_782.graphics.f("#FFFFFF").s().p("AgYBZQAOgXAFgTQAGgUAAgbQAAgagGgUQgFgTgOgYIARAAQARAWAIAVQAHAUAAAaQAAAcgHAUQgIAUgRAVg");
	this.shape_782.setTransform(33.2,-253.6);

	this.shape_783 = new cjs.Shape();
	this.shape_783.graphics.f("#FFFFFF").s().p("AgDBqQgigCgUgVQgVgVAAgfQgBgWAJgPQAHgPASgRQAMgJAXgXQAKgOAFgJIAgAAQgGAMgKALQgJAKgOAKQAWgBARAGQAvARAAA3QAAAjgaAWQgXAWgjAAIgDAAgAgpgPQgOAPAAATQAAATANAMQARASAaAAQAWAAAPgMQAVgPAAgYQAAgLgFgKQgFgOgPgIQgNgHgSAAQgcAAgQASgAhEg4IgOAAIAAgOIAOAAIAAgVIgPAAIAAgOIApAAQANAAAIAGQAJAHAAANQAAAMgLAGQgLAGgQAAIAAAWQgLAKgHAMgAgyhaIAAATQAPAAAAgJQAAgKgLAAIgEAAg");
	this.shape_783.setTransform(21.1,-255.5);

	this.shape_784 = new cjs.Shape();
	this.shape_784.graphics.f("#FFFFFF").s().p("AhBBMQgYgYABgnQAAgbAUgTQATgSAcAAIAVAAIAAASIgSAAQgVAAgMAMQgNAMgBATQgBAYAVAPQATAOAaAAQAdgBATgVQASgTAAgcQAAgegTgWQgTgWgcgBQgVgBgRAJQgRAJgHAPIgSAAQAIgYAVgNQAVgNAeAAQAoAAAZAdQAZAdAAAqQAAArgaAcQgZAdgpAAQgnAAgYgZg");
	this.shape_784.setTransform(3.4,-254.9);

	this.shape_785 = new cjs.Shape();
	this.shape_785.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHAAANQAAALgKAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_785.setTransform(-9.4,-263.4);

	this.shape_786 = new cjs.Shape();
	this.shape_786.graphics.f("#FFFFFF").s().p("AhBBEQgZgdAAgqQABgpAagbQAagbArAAQAkAAAZAYQAYAYAAAkQAAAdgQAVQgRAWgcABQgbAAgQgSQgRgRACgZQACgXASgNIgVAAIAAgSIBHAAIAAASIgOAAQgNAAgJAIQgIAIAAAMQgBAMAJAKQAJAIAPAAQASgBAKgOQAKgMgBgSQgBgXgLgNQgQgTgjgBQgcAAgRAZQgPAVAAAfQAAAdAPAWQAPAXAaAEIAOACQAgAAASgbIAXAAQgXAxg+AAQgpAAgagfg");
	this.shape_786.setTransform(-15.4,-250.7);

	this.shape_787 = new cjs.Shape();
	this.shape_787.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQANAAAIAGQAJAHAAANQAAALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_787.setTransform(-28.4,-263.4);

	this.shape_788 = new cjs.Shape();
	this.shape_788.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAJAAAHgSQAGgSAAgVQAAgVgGgSQgHgTgIgBQgJAAgHAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgMAWQgOAagWAAQgNAAgKgGg");
	this.shape_788.setTransform(-28.8,-252.8);

	this.shape_789 = new cjs.Shape();
	this.shape_789.graphics.f("#FFFFFF").s().p("AhABSQgagSABgjQACgcAXgLQgMgDAAgNQAAgRARgIQANgGARABQAVABANARQANARgBAVIg1AAQgMAAgIAGQgJAHABAMQABAcAkAHQAMACAPAAQAfAAASgUQASgTAAgcQABgfgSgWQgTgXgjgBQgogBgVAeIgUAAQAKgWAWgNQAWgLAcAAQAoAAAaAcQAbAeAAApQAAApgYAdQgZAegpAAQgoAAgYgSgAgqgkQADADACAFQACAGgDAGIAiAAQgCgJgHgGQgHgHgLAAQgGAAgFACg");
	this.shape_789.setTransform(-41.6,-254.9);

	this.shape_790 = new cjs.Shape();
	this.shape_790.graphics.f("#FFFFFF").s().p("AgxA4QgXgWAAghQAAgiAXgWQAWgWAgAAQAlAAAQAPQALAKABAPIgSAAQgBgKgOgHQgNgFgQAAQgWAAgQAOQgSAPAAAWQAAAVAQAOQAPAOAWAAQARAAANgKQAPgKAAgRQAAgVgUgGQAHAHAAAPQAAAJgKAHQgIAIgMAAQgNAAgKgJQgKgIAAgOQAAgPANgJQALgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgWgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDAAgFQAAgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_790.setTransform(-58.6,-252.8);

	this.shape_791 = new cjs.Shape();
	this.shape_791.graphics.f("#FFFFFF").s().p("AAIBZQgRgVgIgUQgHgUAAgcQAAgaAHgUQAIgVARgWIARAAQgOAYgGATQgFAUAAAaQAAAbAFAUQAGATAOAXg");
	this.shape_791.setTransform(-69.8,-253.6);

	this.shape_792 = new cjs.Shape();
	this.shape_792.graphics.f("#FFFFFF").s().p("AhABVQgXgWABgnQAAgaAUgRQATgSAbAAIAVAAIAAASIgQAAQgWAAgMAKQgNAJgBARQgCAZAWAPQASAMAaAAQAbAAARgPQAUgRAAgbQgBgegQgQQgPgMgXAAIhZAAIAAgQQAYAAAAgCQAAgCgFgDQgFgCgBgEIAAgFQAAgYBBAAQApAAAVARQATARgCARIgVAAIABgEQAAgJgNgIQgRgKgaAAQgdAAgHAIQgDADAAACQAAAHANAAIArAAQATAAASAMQAfAUAAAtQAAArgXAaQgYAbgpAAQgnAAgYgWg");
	this.shape_792.setTransform(-92.2,-255.6);

	this.shape_793 = new cjs.Shape();
	this.shape_793.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQANAAAIAGQAJAHAAANQAAALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_793.setTransform(-104.9,-263.4);

	this.shape_794 = new cjs.Shape();
	this.shape_794.graphics.f("#FFFFFF").s().p("AhBBEQgZgdAAgqQABgpAagbQAagbArAAQAkAAAZAYQAYAYAAAkQAAAdgQAVQgRAWgcABQgbAAgQgSQgRgRACgZQACgXASgNIgVAAIAAgSIBHAAIAAASIgOAAQgNAAgJAIQgIAIAAAMQgBAMAJAKQAJAIAPAAQASgBAKgOQAKgMgBgSQgBgXgLgNQgQgTgjgBQgcAAgRAZQgPAVAAAfQAAAdAPAWQAPAXAaAEIAOACQAgAAASgbIAXAAQgXAxg+AAQgqAAgZgfg");
	this.shape_794.setTransform(-110.9,-250.7);

	this.shape_795 = new cjs.Shape();
	this.shape_795.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgPAAIAAgOIAoAAQANAAAIAGQAJAHAAANQAAALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQAOAAgBgJQAAgKgLAAIgCAAg");
	this.shape_795.setTransform(-123.9,-263.4);

	this.shape_796 = new cjs.Shape();
	this.shape_796.graphics.f("#FFFFFF").s().p("AhABSQgagSABgjQACgcAXgLQgMgDAAgNQAAgRARgIQANgGARABQAVABANARQANARgBAVIg1AAQgMAAgIAGQgJAHABAMQABAcAkAHQAMACAPAAQAfAAASgUQASgTAAgcQABgfgSgWQgTgXgjgBQgogBgVAeIgUAAQAKgWAWgNQAWgLAcAAQAoAAAaAcQAbAeAAApQAAApgYAdQgZAegpAAQgoAAgYgSgAgqgkQADADACAFQACAGgDAGIAiAAQgCgJgHgGQgHgHgLAAQgGAAgFACg");
	this.shape_796.setTransform(-137.1,-254.9);

	this.shape_797 = new cjs.Shape();
	this.shape_797.graphics.f("#FFFFFF").s().p("AgxA4QgXgWAAghQAAgiAXgWQAWgWAhAAQAkAAAQAPQALAKABAPIgSAAQgBgKgOgHQgNgFgQAAQgWAAgQAOQgSAPAAAWQAAAVAQAOQAPAOAWAAQARAAANgKQAPgKAAgRQAAgVgUgGQAHAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQAAgPANgJQALgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgWgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQAEgDgBgFQABgGgEgDQgEgEgFAAQgGAAgDADg");
	this.shape_797.setTransform(-154.1,-252.8);

	this.shape_798 = new cjs.Shape();
	this.shape_798.graphics.f("#FFFFFF").s().p("AgrAAIgfhWICWBWIiWBXg");
	this.shape_798.setTransform(-285.9,121.2);

	this.shape_799 = new cjs.Shape();
	this.shape_799.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_799.setTransform(-121.7,173.4);

	this.shape_800 = new cjs.Shape();
	this.shape_800.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_800.setTransform(-142.3,173.4);

	this.shape_801 = new cjs.Shape();
	this.shape_801.graphics.f("#FFFFFF").s().p("AgeBFIAAgTIBKAAIAAgSQgOAFgYgCQgTgBgNgLQgMgLAAgRQAAgXATgKQgHgCABgKQgHADgHALQgGAMAAANQAAAOAIAOQAHALAIAGIgQAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAFgLAHgGQAMgLALgCIAAAEQAIgFASAAQAZAAAKAMQAIAKABAOIgmAAQgLAAgHAHQgHAHgBALQgBALAKAJQAJAKAOAAQAXAAAQgIIAAA2gAgCg3QACACABAEQAAAEgCABIAFAAIAfAAQgEgIgJgDQgFgBgIAAQgIAAgDABg");
	this.shape_801.setTransform(78.7,149.9);

	this.shape_802 = new cjs.Shape();
	this.shape_802.graphics.f("#FFFFFF").s().p("AgMAVQAKgHAAgFIAAgBIgGgDQgGgFAAgFQAAgFADgEQAEgGAHAAQAFAAAEACQAGAFAAAHQAAAGgDAEQgBAEgFAFIgIAIg");
	this.shape_802.setTransform(-9.7,152.9);

	this.shape_803 = new cjs.Shape();
	this.shape_803.graphics.f("#FFFFFF").s().p("AgCA5QgHALgRAAQgRAAgKgOQgIgOAAgXQABgZAPgNQAMgLAYAAIAIAAIAAALIgSABQgJABgFAFQgMAIAAARQgBANAFAJQAFAKAKAAQAKAAAEgFQAFgFgBgIIAAgQIAMAAIAAAQQAAASATAAQAMAAAGgOQAGgOgBgUQAAgTgLgOQgNgSgYAAQgOAAgLAGQgLAFgGAJIgNAAQAEgOAPgIQAPgKAVABQAPAAANAGQALAEAGAIQATAUAAAjQAAAbgKARQgLATgUgBQgQAAgHgLg");
	this.shape_803.setTransform(-60.5,146.6);

	this.shape_804 = new cjs.Shape();
	this.shape_804.graphics.f("#FFFFFF").s().p("AgpA3QgQgPAAgYQAAgRAHgLQAIgOAOAAQANAAACALQAEgMAOAAQAKAAAIAJQAHAIAAAKQAAAOgJAKQgJAIgMABQgLAAgIgHQgJgGAAgLIADgPQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQASAAAMgMQANgMAAgTQgBgNgFgJQgIgMgMAAIhGAAIAAgLQATABAAgCIgFgCQgDgBAAgFQAAgNAPgFQAIgCATAAQAOAAAKADQAOAEAIAKQAHAIAAALIgMAAQgCgLgMgGQgLgFgPAAQgLAAgFACQgIACAAAGQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIAEQAXAKAAAlQAAAbgRASQgPARgaAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGgBAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABAAABgBQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_804.setTransform(-79.3,146.3);

	this.shape_805 = new cjs.Shape();
	this.shape_805.graphics.f("#FFFFFF").s().p("AgpA3QgQgPAAgZQAAgPAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAJQAAAOgJAKQgJAJgMAAQgLgBgIgGQgJgHAAgKIADgPQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQASAAAMgMQANgMAAgSQgBgOgFgJQgIgMgMAAIhGAAIAAgKQATABAAgDIgFgCQgDgBAAgEQAAgOAPgEQAIgDATAAQAOAAAKAEQAOADAIAKQAHAHAAAMIgMAAQgCgLgMgGQgLgFgPAAQgLAAgFACQgIACAAAGQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAABAAIAgAAQAKAAAIAEQAXAKAAAlQAAAbgRASQgPARgaAAQgYABgRgRgAgOACQAAAGAFAEQADAEAHAAQAGAAAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBABgBQgBAAAAgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAABg");
	this.shape_805.setTransform(176.7,121.3);

	this.shape_806 = new cjs.Shape();
	this.shape_806.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_806.setTransform(80.9,121.6);

	this.shape_807 = new cjs.Shape();
	this.shape_807.graphics.f("#FFFFFF").s().p("AgoA4QgRgQAAgYQAAgQAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQAMgMAAgSQABgOgHgJQgGgMgOAAIguAAQgWAAAAgNQAAgOARgHQAOgGAUAAQAfAAAQARQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgdAAABAKQAAABAAAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAmAAQALAAAHAEQAYAKAAAlQAAAbgQASQgQASgaAAQgYAAgQgQgAgOACQAAAGAEAEQAEAEAGABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAg");
	this.shape_807.setTransform(48.4,121.3);

	this.shape_808 = new cjs.Shape();
	this.shape_808.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCgBgIIACgFQAEgNAXAAQARAAAHAKQAHgKATAAQAKAAAHAFQAIAEABAIQABAGgDAGQAFADADAIQADAIAAAIQAAAVgQANQgQAOgdABQgdgBgRgPgAgpgLQgFAGAAAHQABANAPAHQANAEARABQARgBAMgEQAPgHABgNQABgHgFgGQgFgFgIgBIg5AAQgHABgFAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgEgCQgDgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_808.setTransform(-21.7,123.3);

	this.shape_809 = new cjs.Shape();
	this.shape_809.graphics.f("#FFFFFF").s().p("AgoA4QgRgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQAMgMAAgSQABgOgHgJQgGgMgOAAIguAAQgWAAAAgNQAAgOARgHQAOgGAUAAQAfAAAQARQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgdAAAAAKQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAmAAQALAAAHAEQAYAKAAAlQAAAbgQASQgQASgaAAQgYAAgQgQgAgOACQAAAGAEAEQAEAEAGABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAg");
	this.shape_809.setTransform(-54.2,121.3);

	this.shape_810 = new cjs.Shape();
	this.shape_810.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_810.setTransform(-122.7,121.6);

	this.shape_811 = new cjs.Shape();
	this.shape_811.graphics.f("#FFFFFF").s().p("AgSASQgIgGgDgKIAAgFQAAgMAIgIIAAANIAFAAQAAAIAFAEQAFAGAHgBQAIAAAFgFQAEgGgBgIIANAAIAAAGQAAAMgIAJQgJAJgNAAQgKAAgIgGg");
	this.shape_811.setTransform(-150.8,129.5);

	this.shape_812 = new cjs.Shape();
	this.shape_812.graphics.f("#FFFFFF").s().p("AgoA3QgRgPAAgZQAAgPAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAJQAAAOgJAKQgJAJgMAAQgLgBgIgGQgJgHAAgKIADgPQABgHgHAAQgFAAgDAIQgDAGAAAHQAAASANAKQAMAKARAAQARAAANgMQAMgMAAgSQABgOgHgJQgGgMgOAAIhFAAIAAgKQASABAAgDIgEgCQgDgBAAgEQAAgOAPgEQAIgDASAAQAPAAAKAEQANADAJAKQAGAHAAAMIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgHACAAAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHAEQAYAKAAAlQAAAbgRASQgPARgaAAQgYABgQgRgAgOACQAAAGAEAEQAFAEAFAAQAHAAAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQgBABAAAAQAAABABABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAABg");
	this.shape_812.setTransform(-174.7,121.3);

	this.shape_813 = new cjs.Shape();
	this.shape_813.graphics.f("#FFFFFF").s().p("AgoBFIAAgTIBKAAIAAgSQgOAFgXgCQgVgBgMgLQgMgLAAgRQAAgVASgKQgCgBgDgEQgCgDAAgFIABgEQACgJAMgEQAHgCAOAAQAXAAALANQAIAKAAAPIgkAAQgKAAgIAHQgJAGAAAKQgBALAKAJQAKAKANAAQAYAAAPgIIAAA2gAgNg2QADABAAAFQABAGgDACIAGgBIAfAAQgCgIgKgEQgFgCgIAAIgEAAIgJABg");
	this.shape_813.setTransform(-197.6,124.9);

	this.shape_814 = new cjs.Shape();
	this.shape_814.graphics.f("#FFFFFF").s().p("AgsAAIgfhWICXBWIiXBXg");
	this.shape_814.setTransform(-285.9,12.2);

	this.shape_815 = new cjs.Shape();
	this.shape_815.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgFQAAgMAIgIIAAAOIAFAAQAAAGAGAFQAFAGAGAAQAIgBAEgFQAGgGgBgIIALAAIABAGQAAANgJAJQgIAIgNAAQgKAAgIgGg");
	this.shape_815.setTransform(128.1,70.5);

	this.shape_816 = new cjs.Shape();
	this.shape_816.graphics.f("#FFFFFF").s().p("AgCBFQgXgBgNgOQgOgOgBgVQAAgTALgOQgDgBgDgEIgFgIIgBgLQgBgKAKgKQAIgKAQAAIAKABQAKADABAHIAGgJIAVAAQgFAJgHAJQgIAIgIAFQAOgBALAEQAgAMgBAkQABAXgSAQQgPAOgXAAIgCAAgAgcgMQgIALAAAMQgBANAJAIQALALARAAQAPAAAKgHQANgLAAgQQAAgHgDgGQgDgJgJgGQgKgFgMAAQgSAAgLAMgAgkgrQgDAGAAAFQgBAHAGADIAEgEIAXgWQgBgEgGgBIgDAAQgMAAgHAKg");
	this.shape_816.setTransform(-35.5,62.6);

	this.shape_817 = new cjs.Shape();
	this.shape_817.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAALgJQAMgJABgOIABgHQAAgOgJgLQgIgJgOAAIgpAAQgUAAAAgNQAAgXAyAAQAeAAALAJQAIAGAAAIQAAAIgHAFQAQASgBAbQAAAYgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABgAAXgyQgCACAAADQAAADACACQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDAAQgDAAgCACgAgcgvQgBAFAKAAIAcAAQgCgHADgFIgLgBQgZAAgCAIg");
	this.shape_817.setTransform(-75.9,62.7);

	this.shape_818 = new cjs.Shape();
	this.shape_818.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAABAAAAQAAABAAAAQABABAAAAQAAAAABABIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_818.setTransform(-150.2,62.6);

	this.shape_819 = new cjs.Shape();
	this.shape_819.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgFQAAgMAIgIIAAAOIAFAAQAAAGAGAFQAFAGAGAAQAIgBAEgFQAGgGgBgIIALAAIABAGQAAANgJAJQgIAIgNAAQgKAAgIgGg");
	this.shape_819.setTransform(-178.3,70.5);

	this.shape_820 = new cjs.Shape();
	this.shape_820.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgZQAAgQAHgMQAIgNAOAAQANAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAKQAAANgJAJQgJAJgMAAQgLABgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQANgMAAgTQgBgNgFgKQgIgLgMAAIhGAAIAAgLQATABAAgBIgFgDQgDgCAAgEQAAgNAPgFQAIgBATAAQAOAAAKACQAOAEAIAKQAHAIAAALIgMAAQgCgLgMgGQgLgFgPAAQgLAAgFABQgIADAAAGQAAABABAAQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIADQAXALAAAlQAAAbgRASQgPASgaAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAABgBQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBABAAAAg");
	this.shape_820.setTransform(-202.2,62.3);

	this.shape_821 = new cjs.Shape();
	this.shape_821.graphics.f("#FFFFFF").s().p("AgqA5QgPgPAAgaQABgRAMgMQANgLASAAIAOAAIAAAMIgKAAQgPAAgIAGQgJAGgBAMQAAARAOAJQAMAIARAAQASAAAMgKQANgLgBgTQAAgTgLgKQgJgJgQAAIgmAAQgIAAgFgDQgFgFAAgFQAAgOAOgHQAMgEAXAAQAcgBAOAKQALAGgBALQAAADgEADQgEAEgEAAQASAOAAAcQAAAcgPASQgQASgbAAQgaAAgQgPgAAagzQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAGAGAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAAAQAAgBABAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABgAgcgzQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAFAIAAIAiAAQgCgDAAgEQAAgDAEgDQgGgBgJgBQgWAAgGAHg");
	this.shape_821.setTransform(-214.3,62.3);

	this.shape_822 = new cjs.Shape();
	this.shape_822.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgNIA7AAIAAAMIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEADAEAAQALAAAEgKIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_822.setTransform(-223.8,64.4);

	this.shape_823 = new cjs.Shape();
	this.shape_823.graphics.f("#FFFFFF").s().p("AAMBAQgKgFAAgKQgBgIAHgEQgVABgPgMQgPgMABgSQACgWARgKQgHgCACgKQgIADgHALQgGAMAAANQAAAOAIAOQAHALAJAGIgRAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAFgLAHgGQAMgLALgCIAAAEQAIgFASAAQAZAAAKAMQAJAKAAAOIgmAAQgKAAgHAHQgJAHAAALQgBALAKAJQAKAJANABQAVABAPgIIAAAMQgHADgHACQgNADAAAGQgBAEAEACQAEACAFAAQAFAAAEgDQAFgDAAgGIgBgEIANAAIAAAFQAAALgJAIQgJAGgMABIgFAAQgIAAgIgDgAgGg1QADACABAEQAAAEgDABIAFAAIAfAAQgDgIgKgDQgFgBgIAAQgHAAgEABg");
	this.shape_823.setTransform(-245.4,65.7);

	this.shape_824 = new cjs.Shape();
	this.shape_824.graphics.f("#FFFFFF").s().p("AASBIQgSgBgIgLQgGAMgOAAQgRAAgIgRQgIgNAAgVQAAgUAOgNQANgPATAAQAOAAAKAIQALAJAAAMQABAHgDAHQgCAHgEACQgIADgBAHQAAAMASgBQAMAAAHgNQAGgLAAgPQAAgQgMgNQgJgJgQAAIgxAAQgHAAgEgEQgDgEAAgFQAAgPATgHQAPgFARAAQAnAAAKATQAEAGAAAGIgBAFIgKAAQAWASABAfQgBAXgKARQgMASgTAAIgCAAgAgkAtQgCACAAADQAAAFADADQADABADAAQAFAAACgEQADgCAAgDQAAgFgFgDQgCgBgDAAQgFAAgCAEgAgKAlQAAgJAEgFIAGgEQACgCABgDIABgEQAAgHgGgEQgIgEgHAAQgUAAgHASQgCAGAAAHQAAAGACAFQADgIAOgBQALAAAGAJIAAAAgAgggvQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAIAFABIAsAAQAGAAAGACIAGADIAAgDQAAgHgLgFQgLgEgMAAQgfAAgDAKg");
	this.shape_824.setTransform(-258.3,62.3);

	this.shape_825 = new cjs.Shape();
	this.shape_825.graphics.f("#FFFFFF").s().p("AgoA3QgRgQAAgXQAAgRAHgMQAIgNANAAQAOAAACALQAEgMAOAAQALAAAHAJQAHAIAAAKQAAAOgJAKQgJAIgMABQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgTQAAgNgHgJQgGgMgOAAIhFAAIAAgLQASABAAgCIgDgCQgEgBAAgFQAAgNAPgFQAIgBASgBQAQAAAJADQANAEAJAKQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgIACABAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAgAAQALAAAHAEQAYAKAAAlQAAAbgQASQgQASgaAAQgYgBgQgQgAgOACQAAAGAEAEQAEAEAGABQAHgBAFgEQAFgEAAgHQgFAHgJABQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_825.setTransform(195.6,37.3);

	this.shape_826 = new cjs.Shape();
	this.shape_826.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAALgJQAMgJABgOIABgHQAAgOgJgLQgIgJgOAAIgpAAQgUAAAAgNQAAgXAyAAQAeAAALAJQAIAGAAAIQAAAIgHAFQAQASgBAbQAAAYgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABgAAXgyQgCACAAADQAAADACACQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDAAQgDAAgCACgAgcgvQgBAFAKAAIAcAAQgCgHADgFIgLgBQgZAAgCAIg");
	this.shape_826.setTransform(183.6,37.7);

	this.shape_827 = new cjs.Shape();
	this.shape_827.graphics.f("#FFFFFF").s().p("AgqA3QgTgOABgVQABgRAOgIQgJgCAAgKQAAgKALgEQAHgEAMAAQAQAAAHAKQAEgJAPgBIAJAAIAGgFIAEgGQAHgLAAgKIARAAIgDALQgCAHgEAFQgFAHgFAFQAIAEACAKQAAAGgCAEIgKAAQAFACAFAFQAJAIAAAPQAAAUgTAOQgRANgXAAQgYgBgSgNgAgnAFQgFAGAAAHQABAMAOAHQANAGAQAAQARAAAMgGQAOgGABgOQAAgHgFgFQgFgFgMAAIgwAAQgHAAgGAFgAAPgWQgFAAgDADQgEADAAAFIAUAAQAHAAAFABQABgCgBgFQgDgGgNAAIgEABgAgfgVQADABABADQAAADgCADIAWAAQgCgIgHgDIgHgBQgFAAgDACg");
	this.shape_827.setTransform(152.2,37.6);

	this.shape_828 = new cjs.Shape();
	this.shape_828.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDgBgHIACgGQAEgMAXAAQARgBAHALQAHgLATABQAKAAAHADQAIAFABAIQABAGgDAGQAFAEADAHQADAIAAAIQAAAVgQAOQgQANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABAMAQAHQAMAFARAAQARAAAMgFQAPgHABgMQABgJgFgFQgFgGgIABIg5AAQgHgBgFAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgEgDQgDgCgFgBIgBAAQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgNgDIgGAAIgFABg");
	this.shape_828.setTransform(123.2,39.3);

	this.shape_829 = new cjs.Shape();
	this.shape_829.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgRANgHQgGgCAAgKQAAgDACgDQAGgNAVAAQARAAAHAKQAIgKASAAQALAAAHAFQAJAGgBAKQABAHgDAEQALAKAAANQAAAVgQAOQgQANgdAAQgdAAgRgOgAgtADQABAMAPAHQANAFAQAAQASAAAMgFQAPgHABgMQAAgHgEgFQgHAEgMAEQAGACAAAHQgBAGgIAEQgIADgKAAQgLgBgIgDQgJgEAAgHQAAgMAXAAIAKgBQAPgDADgDIg0AAQgTABABAPgAgKAGQAAAEALABQALAAAAgFQAAgEgMAAQgJAAgBAEgAANgjQgGAEAAAHIAdAAIACgFQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBgBAAQgCgGgKAAQgHAAgEAEgAgfgnQADADABAEQAAAFgDADIAYAAQAAgLgNgEIgGgBQgEAAgCABg");
	this.shape_829.setTransform(-41.5,39.3);

	this.shape_830 = new cjs.Shape();
	this.shape_830.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgIAFgEQgFgCAAgHIABgGQAFgNAWAAQASAAAGAKQAIgKATAAQAJAAAHAEQAIAFABAIQABAGgEAGQAFADAEAIQADAIAAAIQAAAVgPANQgRAPgdAAQgdAAgRgPgAgpgLQgFAFAAAIQABANAPAHQAMAEASABQARgBAMgEQAPgHACgNQAAgIgFgFQgFgFgIAAIg5AAQgHAAgFAFgAANgkQgFACgBAHIAdAAIAAgEQAAgEgEgCQgDgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_830.setTransform(66.3,14.3);

	this.shape_831 = new cjs.Shape();
	this.shape_831.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRAOgIQgJgCAAgKQAAgKALgEQAHgEAMAAQAQAAAHAKQAEgJAPgBIAJAAIAGgFIAEgGQAHgKAAgLIARAAIgDALQgCAHgEAGQgFAGgFAFQAIAFACAJQAAAGgCAFIgKAAQAFABAFAFQAJAHAAAQQAAAUgTAOQgRAMgXAAQgYABgSgNgAgnAFQgFAFAAAIQABAMAOAHQANAGAQAAQARAAAMgFQAOgIABgNQAAgHgFgFQgFgFgMAAIgwAAQgHAAgGAFgAAPgWQgFABgDACQgEADAAAFIAUAAQAHAAAFABQABgCgBgEQgDgHgNAAIgEABgAgfgWQADACABADQAAADgCADIAWAAQgCgIgHgDIgHgBQgFAAgDABg");
	this.shape_831.setTransform(22.1,12.6);

	this.shape_832 = new cjs.Shape();
	this.shape_832.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgIAFgEQgFgCgBgHIABgGQAFgNAXAAQARAAAHAKQAHgKATAAQAKAAAHAEQAIAFABAIQABAGgDAGQAEADAEAIQADAIAAAIQAAAVgQANQgQAPgdAAQgdAAgRgPgAgpgLQgFAFAAAIQABANAQAHQAMAEARABQARgBAMgEQAPgHABgNQABgIgFgFQgFgFgIAAIg5AAQgIAAgEAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgDgCQgEgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_832.setTransform(-6.9,14.3);

	this.shape_833 = new cjs.Shape();
	this.shape_833.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_833.setTransform(-258.4,12.6);

	this.shape_834 = new cjs.Shape();
	this.shape_834.graphics.f("#FFFFFF").s().p("AgoBFIAAgTIBLAAIAAgSQgQAFgWgCQgVgBgLgLQgNgLAAgRQAAgVATgKQgDgBgDgEQgCgDAAgFIABgEQADgJAKgEQAIgCAPAAQAWAAALANQAIAKABAPIglAAQgLAAgIAHQgHAGgBAKQgBALAKAJQALAKAMAAQAXAAAQgIIAAA2gAgNg2QADABABAFQAAAGgDACIAGgBIAgAAQgCgIgLgEQgEgCgJAAIgEAAIgJABg");
	this.shape_834.setTransform(136,-40.1);

	this.shape_835 = new cjs.Shape();
	this.shape_835.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgPAAgLADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDABgPQgBgFACgCQAGgNAQgFQALgEAUAAQAtAAAJAWQACADgBAEQABAOgLAEg");
	this.shape_835.setTransform(124.3,-48.4);

	this.shape_836 = new cjs.Shape();
	this.shape_836.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_836.setTransform(72.4,-40.7);

	this.shape_837 = new cjs.Shape();
	this.shape_837.graphics.f("#FFFFFF").s().p("AgwBFIAAgQIBbAAIAAgLQgRAIgaAAQgbAAgQgRQgQgRAAgaQABgZARgRQATgQAYAAQAYAAARAQQAQAPAAAYQAAARgKANQgLANgRAAQgRAAgKgIQgMgKAAgPQAAgQANgJIgMAAIAAgMIArAAIABAMIgLAAQgGAAgFAEQgFAFAAAGQgBAJAFAFQAHAFAKAAQAKAAAGgHQAHgIAAgKQAAgRgMgKQgMgKgSAAQgSABgKANQgKANABARQAAAUAMAPQANAPAUAAQAVAAAOgNIAMAAIAAAsg");
	this.shape_837.setTransform(-158.7,-40.1);

	this.shape_838 = new cjs.Shape();
	this.shape_838.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_838.setTransform(49.3,-66.8);

	this.shape_839 = new cjs.Shape();
	this.shape_839.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_839.setTransform(24.1,-68.4);

	this.shape_840 = new cjs.Shape();
	this.shape_840.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_840.setTransform(-31.7,-66.8);

	this.shape_841 = new cjs.Shape();
	this.shape_841.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_841.setTransform(-65.5,-66.8);

	this.shape_842 = new cjs.Shape();
	this.shape_842.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_842.setTransform(-97.8,-66.8);

	this.shape_843 = new cjs.Shape();
	this.shape_843.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_843.setTransform(-111.6,-65.7);

	this.shape_844 = new cjs.Shape();
	this.shape_844.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_844.setTransform(-224.8,-66.8);

	this.shape_845 = new cjs.Shape();
	this.shape_845.graphics.f("#FFFFFF").s().p("AgCBGQgXgBgNgOQgOgNAAgWQAAgOAGgKQAFgKALgLIAXgVIAKgPIAWAAQgFAHgGAHQgGAHgJAHQAOgBAMAEQAfAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAJAJQAKALASAAQAPAAAKgIQANgKAAgQQAAgHgDgGQgDgKgKgFQgJgFgMAAQgTAAgKAMgAgtglIgJAAIAAgKIAJAAIAAgNIgKAAIAAgKIAcAAQAIAAAFAEQAHAFAAAJQgBAIgHAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_845.setTransform(57.9,-93.6);

	this.shape_846 = new cjs.Shape();
	this.shape_846.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAIAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgEACg");
	this.shape_846.setTransform(40.7,-91.8);

	this.shape_847 = new cjs.Shape();
	this.shape_847.graphics.f("#FFFFFF").s().p("AgmA7QgQgKgCgTQgBgUAQgHQgEgBgDgDQgDgDAAgFIABgFQAFgOAbACQAQABAHALQAIAKgBAOIgjAAQgRAAAAANQABAKAMAFQAKAEARAAQARAAALgJQAMgJABgOIABgHQAAgOgJgLQgIgJgOAAIgpAAQgUAAAAgNQAAgXAyAAQAeAAALAJQAIAGAAAIQAAAIgHAFQAQASgBAbQAAAYgSAPQgRAPgYAAQgVAAgPgJgAgbgOQACACABAEQAAAFgDADIAZAAQgCgHgFgEQgFgEgIAAIgFABgAAXgyQgCACAAADQAAADACACQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDAAQgDAAgCACgAgcgvQgBAFAKAAIAcAAQgCgHADgFIgLgBQgZAAgCAIg");
	this.shape_847.setTransform(-5.5,-93.3);

	this.shape_848 = new cjs.Shape();
	this.shape_848.graphics.f("#FFFFFF").s().p("AgqA4QgTgPABgVQABgRANgIQgJgCAAgKQAAgKAKgEQgDgEAAgJIABgGQAEgLANgDQAIgDAQAAQAiAAAEAUQABAEgDAFQADgCAEgGQAHgLAAgLIARAAQgEAQgGAJQgEAHgGAEQAJAFABAKQABAFgCAFIgKAAQAFABAFAFQAIAIAAAPQAAAVgSANQgSANgWAAQgYAAgSgNgAgnAFQgGAGABAHQAAAMAPAHQAMAGARAAQAQAAAMgFQAPgHAAgOQABgHgFgFQgGgFgLAAIgwAAQgIAAgFAFgAAOgWQgFABgCACQgEADAAAFIAUAAQAGAAAFACQACgDgCgEQgDgHgMAAIgFABgAgggVQADABABADQABADgCADIAWAAQgCgIgHgDIgIgBQgEAAgEACgAgCgZQADgEADgCQgHgCgCgFQgDgGADgGQgYAAgFAHQAAABAAAAQgBABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAIAIgBQARAAAGAKgAAHgzQgCADAAACQAAADACADQACACADAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACg");
	this.shape_848.setTransform(-17.5,-93.4);

	this.shape_849 = new cjs.Shape();
	this.shape_849.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAIAFIgKAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADAAAEQABAOgKAEg");
	this.shape_849.setTransform(-50.5,-98.4);

	this.shape_850 = new cjs.Shape();
	this.shape_850.graphics.f("#FFFFFF").s().p("AgMAVQAKgGAAgGIAAgBIgGgEQgGgEAAgGQAAgEADgEQAEgGAHAAQAFAAAEADQAGAEAAAHQAAAFgDAFQgBADgFAGIgIAIg");
	this.shape_850.setTransform(-137.1,-87.1);

	this.shape_851 = new cjs.Shape();
	this.shape_851.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAAPgzIAGADIACgFQAAgFgJgDQgIgDgIAAQgKAAgIACQgKADAAAFIABAEIAFgBQAHgCANAAQAMAAAHACg");
	this.shape_851.setTransform(-187,-91.6);

	this.shape_852 = new cjs.Shape();
	this.shape_852.graphics.f("#FFFFFF").s().p("AgqA6QgRgLAAgTQAAgSAMgIIgLAAIAAgKIAOAAQgDgCAAgFQAAgGAFgEQAKgHAPACQASABAHALQAFAHAAANIgcAAQgZAAgCAQQgCANAPAHQAMAFAQgBQARgBALgJQAMgKADgQQABgFAAgDQAAgOgIgNQgIgMgMgGIgFAGIgHAFQgHAEgJAAQgNgBgFgGQgDgEACgJQABgDgHAAIgEABIAAgLQAJgCALAAQAOAAAJACQAYAGAQAQQASATABAdQAAAbgSASQgRASgbAAQgZAAgPgKgAgYgSQAAAAABABQAAAAABAAQAAABAAABQAAAAAAABQABADgDACIAXAAQgBgFgKgEIgGgBIgGABgAgTgxQAAAFAIAAQAKAAADgHQgHgEgPAAIABAGg");
	this.shape_852.setTransform(-219.3,-93.3);

	this.shape_853 = new cjs.Shape();
	this.shape_853.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_853.setTransform(-258.4,-90.7);

	this.shape_854 = new cjs.Shape();
	this.shape_854.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOAAgVQgBgTALgOQgDgBgDgEIgFgIIgBgLQAAgKAJgKQAIgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgIAFQAOgBALAEQAfAMABAkQgBAXgRAQQgPAOgXAAIgDAAgAgcgMQgJALABAMQAAANAIAIQALALASAAQAOAAAKgHQANgLAAgQQAAgHgCgGQgEgJgKgGQgIgFgNAAQgSAAgLAMgAgjgrQgFAGAAAFQAAAHAGADIAEgEIAXgWQgBgEgGgBIgDAAQgMAAgGAKg");
	this.shape_854.setTransform(176.7,-152.4);

	this.shape_855 = new cjs.Shape();
	this.shape_855.graphics.f("#FFFFFF").s().p("AgOA0IgJgCIAAgLIAEABQAGAAgBgGQAAgDgGgGQgEgHAAgHQgBgIAFgEQAFgEAHgCQAHgBAEADQgBgPgBgGQgDgLgIgCQgHgBgFADIAAgLQAIgEAKABQAPABAIARQAHAOgBATQABAWgLAPQgLAPgPAAIgDAAgAgFAFQgGABAAAHIAAADIAGAIQACAFgBAEQAEgFABgJQABgGAAgGQgCgDgDAAIgCABg");
	this.shape_855.setTransform(155.8,-150.8);

	this.shape_856 = new cjs.Shape();
	this.shape_856.graphics.f("#FFFFFF").s().p("Ag8AdQgCgJACgIIgKAAIAAgKIAIgHIAJgKQgGAAgEgEQgDgFAAgGQAAgJAHgGQAHgGAKAAQAQgBAHALQAPgKAVAAIAIAAQAUABAOAQQANAQgBAVQgBAUgOAOQgOAPgUAAIgHAAIAAgLQgOALgUAAIgCAAQggAAgHgXgAgFgeQAFABAHAEQAGADAEAFQAKALAAAMQABAUgLANIAEABQAKAAAGgFQANgJACgRQABgLgGgKQgFgKgLgFQgJgFgNAAQgJAAgFACgAgjgNQgIAFgMALIAJAAQgEAIACAHQAFAOAWgBQAMAAAIgIQAIgHABgLQAAgJgHgIQgGgHgLAAIgBAAQgKAAgIAGgAgrgnIgFACQAFACACACQACAEgBAEIAFgFIAGgEQgDgEgFgBIgDAAIgDAAg");
	this.shape_856.setTransform(145.7,-150.8);

	this.shape_857 = new cjs.Shape();
	this.shape_857.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgOgNAAgWQAAgOAFgKQAFgKANgLIAWgVIAKgPIAVAAQgEAHgFAHQgHAHgJAHQAOgBALAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgDgKgKgFQgJgFgMAAQgTAAgKAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAGAEQAFAFAAAJQABAIgIAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_857.setTransform(115,-152.6);

	this.shape_858 = new cjs.Shape();
	this.shape_858.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_858.setTransform(102.9,-157.4);

	this.shape_859 = new cjs.Shape();
	this.shape_859.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgOgNAAgWQAAgOAGgKQAEgKANgLIAWgVIAKgPIAWAAQgEAHgGAHQgHAHgJAHQAPgBALAEQAfAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQALALASAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgTAAgKAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAGAEQAFAFAAAJQAAAIgHAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_859.setTransform(-20.1,-152.6);

	this.shape_860 = new cjs.Shape();
	this.shape_860.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDAAgHIABgGQAFgMAWAAQASAAAGAKQAIgKASAAQAKAAAHADQAIAFABAIQABAGgEAGQAFAEAEAHQADAIAAAIQAAAWgPANQgRANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABANAPAGQANAFARgBQARABAMgFQAPgGACgNQAAgJgFgFQgFgGgIABIg5AAQgHgBgFAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgEgDQgDgCgFgBIgBAAQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQAAgJgOgDIgFAAIgFABg");
	this.shape_860.setTransform(-49.5,-150.7);

	this.shape_861 = new cjs.Shape();
	this.shape_861.graphics.f("#FFFFFF").s().p("AgDBFQgWgBgOgOQgNgOAAgVQgBgTAMgOQgDgBgEgEIgFgIIgCgLQABgKAIgKQAJgKAQAAIAKABQAKADABAHIAFgJIAWAAQgEAJgJAJQgHAIgJAFQAPgBALAEQAfAMABAkQgBAXgQAQQgQAOgXAAIgDAAgAgcgMQgJALAAAMQABANAHAIQAMALASAAQAOAAAKgHQAOgLAAgQQAAgHgDgGQgEgJgKgGQgJgFgLAAQgTAAgLAMgAgjgrQgFAGAAAFQABAHAFADIAEgEIAKgJQgFAAgFgDQgEgEABgEIgCADgAgXgyQAAAGAHAAQAGAAAAgGQAAgHgGAAQgHAAAAAHg");
	this.shape_861.setTransform(-78.2,-152.4);

	this.shape_862 = new cjs.Shape();
	this.shape_862.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_862.setTransform(-87.3,-150.6);

	this.shape_863 = new cjs.Shape();
	this.shape_863.graphics.f("#FFFFFF").s().p("AgeBFIAAgTIBKAAIAAgSQgOAFgYgCQgTgBgNgLQgMgLAAgRQAAgXATgKQgHgCABgKQgHADgHALQgGAMAAANQAAAOAIAOQAHALAIAGIgQAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAFgLAHgGQAMgLALgCIAAAEQAIgFASAAQAZAAAKAMQAIAKABAOIgmAAQgLAAgHAHQgHAHgBALQgBALAKAJQAJAKAOAAQAXAAAQgIIAAA2gAgCg3QACACABAEQAAAEgCABIAFAAIAfAAQgEgIgJgDQgFgBgIAAQgIAAgDABg");
	this.shape_863.setTransform(-144.3,-149.1);

	this.shape_864 = new cjs.Shape();
	this.shape_864.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgPQAAgLAEgIQADgFAHgIQAIgJAVgQIgoAAIAAgMIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBALgJAJQgKAHgMABQgOAAgIgIg");
	this.shape_864.setTransform(-248.1,-150.6);

	this.shape_865 = new cjs.Shape();
	this.shape_865.graphics.f("#FFFFFF").s().p("AARA/IAAgXQgPACgLgBQgXgBgOgKQgQgMAAgUQAAgLAGgIQAFgIAIgEQgEgBgCgDIgBgGQAAgHAFgFQAKgHATAAQAcAAAHAQQACgHAGgFQAGgEAIAAIAMAAIAAALIgDAAQgDAAgBAEQAAABAEAEIAIAHQAFAGAAAGQABANgLAGQgKAFgMgBIAAAaIAKgDIAIgDIAAAMIgIADIgKADIAAAZgAgcgSQgHAIAAAKQABAWAZAGIANABIANgBIAAg2IgcAAQgKAAgHAIgAAfgfIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgHQgFgHAAgEIAAgCQgEABAAAPgAgYgxQACADAAADQAAAEgCABIACAAIADAAIAbAAQgCgHgJgDQgGgDgGAAIgJACg");
	this.shape_865.setTransform(-258.2,-149.7);

	this.shape_866 = new cjs.Shape();
	this.shape_866.graphics.f("#FFAB00").s().p("AgQAHIAAgNIAgAAIAAANg");
	this.shape_866.setTransform(24,241.7);

	this.shape_867 = new cjs.Shape();
	this.shape_867.graphics.f("#FFAB00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQgBgMgCgLQgFgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_867.setTransform(12.9,241);

	this.shape_868 = new cjs.Shape();
	this.shape_868.graphics.f("#FFAB00").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgLALAAQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAKgMgBQgMABgIgIQgIgIABgJIACgHIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAHQAAAQALALQAMAKAQgBQAQABAMgMQAMgNAAgQQAAgVgKgLQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaABQgNAAgMgGgAgPgEQAAAFAFADQAEAEAGAAQAGAAAGgFQAFgEAAgGIAAgDQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADABQABAAAAgBQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgCgDAAQgFABAAAGg");
	this.shape_868.setTransform(5.1,239.7);

	this.shape_869 = new cjs.Shape();
	this.shape_869.graphics.f("#FFAB00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAIQAAAGgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_869.setTransform(-8.5,234.7);

	this.shape_870 = new cjs.Shape();
	this.shape_870.graphics.f("#FFAB00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_870.setTransform(-12.8,241);

	this.shape_871 = new cjs.Shape();
	this.shape_871.graphics.f("#FFAB00").s().p("AgUAnQgIgIAAgMQAAgKADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCADQgFAHAAAIQABAGAEADQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAIgBAJQgBALgIAHQgJAIgLAAQgMgBgIgGg");
	this.shape_871.setTransform(-22.3,241.2);

	this.shape_872 = new cjs.Shape();
	this.shape_872.graphics.f("#FFAB00").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAKgEQAHgDAKAAQAPAAAGAJQAEgIANgBIAIAAIAFgFIAEgFQAGgKAAgKIAQAAIgDALQgCAGgEAFIgJAKQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACg");
	this.shape_872.setTransform(-31,239.6);

	this.shape_873 = new cjs.Shape();
	this.shape_873.graphics.f("#FFAB00").s().p("AgPAHIAAgNIAfAAIAAANg");
	this.shape_873.setTransform(-45.5,241.7);

	this.shape_874 = new cjs.Shape();
	this.shape_874.graphics.f("#FFFF00").s().p("AADAUIAGgGQAEgEAAgDIgBgBIgEgDQgGgDAAgGQAAgFADgDQAEgGAHABQAEgBAEADQAFAEAAAGQAAAFgCAEIgHAKQgFAGgDACgAgbAUQADgCADgEQAEgEAAgDIAAgBIgFgDQgGgDAAgGQAAgFADgDQAEgGAHABQAEgBAEADQAFAEAAAGQAAAFgCAEIgHAKQgFAGgDACg");
	this.shape_874.setTransform(237.1,213.4);

	this.shape_875 = new cjs.Shape();
	this.shape_875.graphics.f("#FFFF00").s().p("AgHALQgFgFAAgGQAAgEACgDQAEgFAGgBQAEAAAEADQAFAFAAAFQAAAEgDAEQgEAFgGAAQgEAAgDgCg");
	this.shape_875.setTransform(230.8,223.8);

	this.shape_876 = new cjs.Shape();
	this.shape_876.graphics.f("#FFFF00").s().p("AAeAVQAIgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgNAAgKADQgMACgBAGIgBADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJADg");
	this.shape_876.setTransform(222.1,214.3);

	this.shape_877 = new cjs.Shape();
	this.shape_877.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_877.setTransform(222.1,220.2);

	this.shape_878 = new cjs.Shape();
	this.shape_878.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_878.setTransform(210.6,219);

	this.shape_879 = new cjs.Shape();
	this.shape_879.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAIgFQAGgFAKAAQAOAAAKAJQALAHAAAOQAAASgNAMQgMALgRAAQgSAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgCACg");
	this.shape_879.setTransform(200.5,220.3);

	this.shape_880 = new cjs.Shape();
	this.shape_880.graphics.f("#FFFF00").s().p("AgCA+QgUgBgMgMQgNgNAAgTQAAgRAKgMQgDgBgDgEQgDgEgBgDIgCgKQAAgIAIgKQAIgJAOAAIAJABQAJADABAHIAFgJIATAAQgEAIgHAIQgHAHgHAFQANgBAKAEQAcALAAAgQAAAVgPAOQgPAMgUAAIgCAAgAgZgKQgIAJAAALQAAAMAIAHQAKAKAPAAQAOAAAJgHQAMgJAAgOQAAgGgDgGQgDgJgJgEQgIgFgKAAQgRAAgKALgAgfgnQgEAGAAAEQAAAHAFACIADgDIAWgUQgCgEgFAAIgEAAQgKAAgFAIg");
	this.shape_880.setTransform(184.9,218.7);

	this.shape_881 = new cjs.Shape();
	this.shape_881.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_881.setTransform(176.8,220.4);

	this.shape_882 = new cjs.Shape();
	this.shape_882.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_882.setTransform(167.8,218.9);

	this.shape_883 = new cjs.Shape();
	this.shape_883.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_883.setTransform(149.9,220.2);

	this.shape_884 = new cjs.Shape();
	this.shape_884.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_884.setTransform(141.7,213.9);

	this.shape_885 = new cjs.Shape();
	this.shape_885.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_885.setTransform(137.6,220.2);

	this.shape_886 = new cjs.Shape();
	this.shape_886.graphics.f("#FFFF00").s().p("AAeAVQAIgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgNAAgKADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJADg");
	this.shape_886.setTransform(125.7,214.3);

	this.shape_887 = new cjs.Shape();
	this.shape_887.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_887.setTransform(125.1,220.2);

	this.shape_888 = new cjs.Shape();
	this.shape_888.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_888.setTransform(114.5,221.6);

	this.shape_889 = new cjs.Shape();
	this.shape_889.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_889.setTransform(97.9,220.2);

	this.shape_890 = new cjs.Shape();
	this.shape_890.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_890.setTransform(85.7,220.2);

	this.shape_891 = new cjs.Shape();
	this.shape_891.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_891.setTransform(77.2,213.9);

	this.shape_892 = new cjs.Shape();
	this.shape_892.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQADgLAAgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_892.setTransform(76.9,220.2);

	this.shape_893 = new cjs.Shape();
	this.shape_893.graphics.f("#FFFF00").s().p("AgnApQgPgRAAgZQABgZAPgQQAQgQAagBQAVAAAPAPQAOAOAAAWQABARgKAMQgKANgRABQgRAAgJgKQgJgKABgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQAKAAAHgJQAGgGAAgLQgCgNgGgIQgKgMgUAAQgRAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAALgQIANAAQgOAegkAAQgZgBgQgSg");
	this.shape_893.setTransform(69.2,221.5);

	this.shape_894 = new cjs.Shape();
	this.shape_894.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgLAAQAAgGgJgEQgHgEgLABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAIgFQAGgFAJAAQAPAAAKAJQALAHAAAOQAAASgNAMQgMALgQAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgEAAgCACg");
	this.shape_894.setTransform(59,220.3);

	this.shape_895 = new cjs.Shape();
	this.shape_895.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_895.setTransform(52,220.2);

	this.shape_896 = new cjs.Shape();
	this.shape_896.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_896.setTransform(44.1,221.2);

	this.shape_897 = new cjs.Shape();
	this.shape_897.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_897.setTransform(30.3,213.9);

	this.shape_898 = new cjs.Shape();
	this.shape_898.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_898.setTransform(25.8,220.2);

	this.shape_899 = new cjs.Shape();
	this.shape_899.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_899.setTransform(13.6,219);

	this.shape_900 = new cjs.Shape();
	this.shape_900.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_900.setTransform(5.6,220.2);

	this.shape_901 = new cjs.Shape();
	this.shape_901.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_901.setTransform(-0.6,221.6);

	this.shape_902 = new cjs.Shape();
	this.shape_902.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_902.setTransform(-11.3,219);

	this.shape_903 = new cjs.Shape();
	this.shape_903.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_903.setTransform(-23.3,220.2);

	this.shape_904 = new cjs.Shape();
	this.shape_904.graphics.f("#FFFF00").s().p("AgWA6QgFgFAAgHQAAgEACgEQAFgFAHAAQAEAAAEADQAFAEAAAHQAAAFgDADQgEAGgHAAQgFAAgDgDgAgaAfIAAgFQAAgHAGgEQAEgDAJgDQAKgDAEgCQAQgHAAgOIgBgEQgBgJgHgFQgHgFgJAAQgJAAgHAFQgHAFAAAIQABAHAEAEQAEAFAGgBQAFAAADgCQADgDAAgEIgFACQgEAAgDgDQgCgCAAgEQAAgFADgDQADgCAFgBQAFAAAEAFQAEAFAAAHQABAIgHAHQgGAFgKAAQgNAAgHgHQgHgIAAgOQAAgOALgKQALgKAPAAQARAAALAKQAMAKAAARQAAAOgGAHQgEAFgLAGQgCACgKAEQgGADgCADQgDACAAAIg");
	this.shape_904.setTransform(-39.8,218.9);

	this.shape_905 = new cjs.Shape();
	this.shape_905.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_905.setTransform(-46.3,213.9);

	this.shape_906 = new cjs.Shape();
	this.shape_906.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_906.setTransform(-50.8,220.2);

	this.shape_907 = new cjs.Shape();
	this.shape_907.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgFgLgFgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_907.setTransform(-59.8,220.2);

	this.shape_908 = new cjs.Shape();
	this.shape_908.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_908.setTransform(-67.6,220.2);

	this.shape_909 = new cjs.Shape();
	this.shape_909.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMAAgKAJQgLAIABANQgBANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAIgFQAGgFAKAAQAOAAALAJQAKAHAAAOQAAASgNAMQgMALgRAAQgSAAgOgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgBACg");
	this.shape_909.setTransform(-78,220.3);

	this.shape_910 = new cjs.Shape();
	this.shape_910.graphics.f("#FFFF00").s().p("AgCA+QgUgBgMgMQgNgNAAgTQAAgRAKgMQgDgBgDgEQgDgEgBgDIgCgKQAAgIAIgKQAIgJAOAAIAJABQAJADABAHIAFgJIATAAQgEAIgHAIQgHAHgHAFQANgBAKAEQAcALAAAgQAAAVgPAOQgPAMgUAAIgCAAgAgZgKQgIAJAAALQAAAMAIAHQAKAKAPAAQAOAAAJgHQAMgJAAgOQAAgGgDgGQgDgJgJgEQgIgFgKAAQgRAAgKALgAgfgnQgEAGAAAEQAAAHAFACIADgDIAWgUQgCgEgFAAIgEAAQgKAAgFAIg");
	this.shape_910.setTransform(-87.8,218.7);

	this.shape_911 = new cjs.Shape();
	this.shape_911.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_911.setTransform(-95.9,220.4);

	this.shape_912 = new cjs.Shape();
	this.shape_912.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgFAAIgCABg");
	this.shape_912.setTransform(-105.3,220.2);

	this.shape_913 = new cjs.Shape();
	this.shape_913.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_913.setTransform(-124,220.2);

	this.shape_914 = new cjs.Shape();
	this.shape_914.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_914.setTransform(-136.4,221.2);

	this.shape_915 = new cjs.Shape();
	this.shape_915.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_915.setTransform(-153.9,220.2);

	this.shape_916 = new cjs.Shape();
	this.shape_916.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_916.setTransform(-166.2,220.2);

	this.shape_917 = new cjs.Shape();
	this.shape_917.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIABgEQAFgNAYABQAOACAGAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAPAAQAPAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIgkAAQgSAAAAgLQAAgVAtAAQARAAANAHQAQAIAAANIgMAAQAAgIgMgFQgLgDgMAAQgXAAgBAFQgBAFAIAAIAeAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_917.setTransform(-178.1,218.9);

	this.shape_918 = new cjs.Shape();
	this.shape_918.graphics.f("#FFFF00").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAJgEQgDgEAAgHIABgGQAEgKALgDQAIgCAOAAQAeAAAEASQABAEgDAEQADgCADgFQAGgKABgKIAPAAQgDAOgGAIQgEAGgFAEQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACgAgBgWQACgEADgCQgGgBgCgFQgDgGACgFQgUAAgFAGIgBAFQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAAAIAIAAQAPAAAGAJgAAGguQAAABgBAAQAAABAAAAQgBABAAABQAAAAAAABQAAADACACQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQACgCAAgDQAAgBAAAAQAAgBAAgBQgBAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgBAAgBAAQAAABgBAAQAAAAgBAAQAAABgBAAg");
	this.shape_918.setTransform(-188.9,218.8);

	this.shape_919 = new cjs.Shape();
	this.shape_919.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_919.setTransform(-206,218.9);

	this.shape_920 = new cjs.Shape();
	this.shape_920.graphics.f("#FFFF00").s().p("AAOAkQAJAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgIgJgKABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgOAJQgLAJgRAAQgRAAgLgLQgMgKAAgQQAAgRANgIQgEgBgCgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQARAAAMAPQAKANABATQgBATgNAOQgNANgUAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFALAAQAKAAAIgEQAHgFABgHQAAgEgBgDQgDgCgDAAQgBAAgCABQgCACgBADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIANAAQgCgHgDgEQgEgDgFAAIgCABg");
	this.shape_920.setTransform(-217.7,220.2);

	this.shape_921 = new cjs.Shape();
	this.shape_921.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgLAAQAAgGgJgEQgIgEgKABQgMAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAKAAQAOAAALAJQAKAHAAAOQAAASgNAMQgLALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgCgCgEgBQgDAAgBACg");
	this.shape_921.setTransform(-228.6,220.3);

	this.shape_922 = new cjs.Shape();
	this.shape_922.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_922.setTransform(-244.8,218.9);

	this.shape_923 = new cjs.Shape();
	this.shape_923.graphics.f("#FFFF00").s().p("AAEA8QgEgCgCgFQgGAJgRAAQgTAAgIgWQgFgNABgVQABgOAIgKQAJgMANABQAGAAAEADQAEAEgBAGQAAACgDAEQgCAEAAADQABAJANABQAEAAAEgCQAFgDACgCIgHAAQgFAAgFgDQgFgDAAgFQgBgIAFgFQAFgGAHAAQATABAAAWQAAAKgHAHQgIAHgKABQgKABgHgEQgIgEgDgIIgBgFQAAgGAEgEQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAgBAAQgGAAgFAIQgFAJAAAIQgBAMAGAJQAGALAKAAQAPAAAAgRIAKAAQAAAJAEAEQAEAEAIAAQANAAAHgNQAFgKAAgPQAAgTgKgOQgNgRgVAAQgiAAgGAWIgMAAQADgOAOgJQAOgKAWAAQAdABAPAWQAMARAAAVQAAAagIAQQgKAUgVAAQgJAAgFgCgAAAgOQAAAGAFAAQADAAACgCQABAAAAgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgGAAAAAHg");
	this.shape_923.setTransform(-256.7,218.8);

	this.shape_924 = new cjs.Shape();
	this.shape_924.graphics.f("#FFFF00").s().p("AgkAyQgPgOAAgWQAAgPAGgKQAHgMANAAQAMAAACAKQADgMANAAQAJAAAHAJQAGAIAAAIQAAANgIAIQgIAJgLAAQgJgBgIgFQgIgGAAgKIADgNQABgGgHAAQgEgBgDAIQgDAFAAAHQAAAPAMAKQALAJAPAAQAQgBALgKQALgLAAgQQAAgNgFgJQgGgKgMAAIg+AAIAAgJQAQABAAgCIgDgCQgEgCAAgDQAAgMAOgFQAHgCARAAQANABAJACQAMADAIAKQAGAGAAALIgLAAIAAAAQgCgKgLgGQgJgEgOAAIgOABQgHACAAAGQAAAAAAABQAAAAABAAQAAABAAAAQABABAAAAIAEABIAdAAQAJAAAHADIAGAEIAAAAIAAAAQAPALAAAcQAAAYgOARQgPAPgXAAQgWAAgOgOgAgMACQAAAFAEAEQAEADAFABQAGAAAEgEQAFgEAAgGQgFAGgIABQgJAAgGgJIAAADgAAAgMQAAAAAAABQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQABAAAAABQAAAAAAABQAAAAABABQAAAAAAAAQABAAABAAQAAABABAAIAEgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgBgDQgBgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQAAABAAAAQgBAAAAABgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAlgfIAAAAgAAfgjQgHgDgJAAIgdAAIgEgBQAAAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAgGAHgCIAOgBQAOAAAJAEQALAGACAKIgGgEg");
	this.shape_924.setTransform(267.8,196);

	this.shape_925 = new cjs.Shape();
	this.shape_925.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_925.setTransform(256.2,197.7);

	this.shape_926 = new cjs.Shape();
	this.shape_926.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLAAgNQAAgMgCgLQgFgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_926.setTransform(242,197.7);

	this.shape_927 = new cjs.Shape();
	this.shape_927.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_927.setTransform(235.7,199.1);

	this.shape_928 = new cjs.Shape();
	this.shape_928.graphics.f("#FFFF00").s().p("AAeAVQAIgEAAgHIgBgDQgBgGgMgCQgKgCgOgBQgNAAgKADQgMACgBAGIgBADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJADg");
	this.shape_928.setTransform(224.9,191.8);

	this.shape_929 = new cjs.Shape();
	this.shape_929.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_929.setTransform(224.5,197.7);

	this.shape_930 = new cjs.Shape();
	this.shape_930.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_930.setTransform(212.8,196.5);

	this.shape_931 = new cjs.Shape();
	this.shape_931.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgLAAQgBgGgIgEQgHgEgLABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAIgFQAGgFAKAAQAOAAAKAJQALAHAAAOQAAASgNAMQgMALgQAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgCACg");
	this.shape_931.setTransform(202.6,197.8);

	this.shape_932 = new cjs.Shape();
	this.shape_932.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_932.setTransform(192.3,202.3);

	this.shape_933 = new cjs.Shape();
	this.shape_933.graphics.f("#FFFF00").s().p("AAOAkQAJAAAIgEQAHgFADgIQAEgKgBgJQgBgKgHgHQgIgJgKABQgRABgEAPQADgCAEAAQAFAAADACQAKAHAAAQQgBAPgNAJQgLAJgRAAQgRAAgLgLQgMgKABgQQgBgRAMgIQgDgBgCgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANgBATQABATgNAOQgOANgUAAgAgogFQgFAEgBAGQABAKAIAFQAJAFAKAAQALAAAHgEQAHgFABgHQAAgEgCgDQgCgCgCAAQgDAAgCABQgBACAAADIgKAAIgBgIIABgIIgOAAQgHAAgFAFgAgmghQADABACAEQABAEgBAEIALAAQgBgHgDgEQgDgDgFAAIgEABg");
	this.shape_933.setTransform(191.7,197.7);

	this.shape_934 = new cjs.Shape();
	this.shape_934.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGAEgHQADgHAGgEQgGgDAAgGIABgFQAEgLAVAAQAQAAAFAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQAAALAPAGQALAEAPAAQAQAAAKgEQAPgGAAgMQAAgHgEgEQgEgGgIAAIgyAAQgIAAgEAGgAAMghQgFADgBAGIAaAAIAAgDQAAgEgCgCQgEgCgEAAQgFAAgFACgAgdgjQADACABADQAAADgCADIAUAAQAAgJgMgCIgEgBIgGABg");
	this.shape_934.setTransform(179.6,197.8);

	this.shape_935 = new cjs.Shape();
	this.shape_935.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_935.setTransform(161.9,197.7);

	this.shape_936 = new cjs.Shape();
	this.shape_936.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgKAAQgCgGgIgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgBACg");
	this.shape_936.setTransform(151.3,197.8);

	this.shape_937 = new cjs.Shape();
	this.shape_937.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_937.setTransform(144.5,191.4);

	this.shape_938 = new cjs.Shape();
	this.shape_938.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_938.setTransform(140.4,197.7);

	this.shape_939 = new cjs.Shape();
	this.shape_939.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_939.setTransform(128.5,197.7);

	this.shape_940 = new cjs.Shape();
	this.shape_940.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_940.setTransform(120.4,197.7);

	this.shape_941 = new cjs.Shape();
	this.shape_941.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGAEgHQADgHAGgEQgGgDAAgGIABgFQAEgLAUAAQARAAAFAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQAAALAPAGQALAEAPAAQAQAAAKgEQAPgGAAgMQAAgHgEgEQgEgGgIAAIgyAAQgIAAgEAGgAAMghQgFADgBAGIAaAAIAAgDQAAgEgCgCQgEgCgEAAQgFAAgFACgAgdgjQADACABADQAAADgCADIAUAAQAAgJgMgCIgEgBIgGABg");
	this.shape_941.setTransform(112.5,197.8);

	this.shape_942 = new cjs.Shape();
	this.shape_942.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_942.setTransform(97.9,197.9);

	this.shape_943 = new cjs.Shape();
	this.shape_943.graphics.f("#FFFF00").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJACgFIATAAQgEAHgFAHQgGAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJABALQAAALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_943.setTransform(89.7,196.5);

	this.shape_944 = new cjs.Shape();
	this.shape_944.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgMAAgLADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQAKgDASgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_944.setTransform(78.7,191.8);

	this.shape_945 = new cjs.Shape();
	this.shape_945.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgFAAIgCABg");
	this.shape_945.setTransform(78.3,197.7);

	this.shape_946 = new cjs.Shape();
	this.shape_946.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_946.setTransform(64.1,191.4);

	this.shape_947 = new cjs.Shape();
	this.shape_947.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_947.setTransform(59.6,197.7);

	this.shape_948 = new cjs.Shape();
	this.shape_948.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_948.setTransform(50.9,191.4);

	this.shape_949 = new cjs.Shape();
	this.shape_949.graphics.f("#FFFF00").s().p("AgmA0QgPgJAAgSQAAgQAKgHIgKAAIAAgJIAOAAQgDgBAAgFQAAgGAEgDQAJgHAOACQAQABAGAKQAEAGABAMIgaAAQgWAAgCAPQgBALAMAGQAMAFAOgBQAPAAAKgKQALgIADgPIABgHQAAgMgIgMQgHgLgKgFIgFAFIgGAEQgHAEgIAAQgMAAgEgGQgDgEACgHQABgDgGAAIgEABIAAgKQAJgCAJAAQANAAAIACQAVAEAPAPQAQARAAAbQABAXgQARQgQAQgYAAQgWAAgOgJgAgWgQQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABQABACgDACIAVAAQgBgFgJgDIgFgBIgGABgAgSgsQABAEAHAAQAJAAADgGQgHgDgNAAIAAAFg");
	this.shape_949.setTransform(47.4,196.4);

	this.shape_950 = new cjs.Shape();
	this.shape_950.graphics.f("#FFFF00").s().p("AgHALQgFgFAAgGQAAgEACgDQAEgFAGgBQAEAAAEADQAFAFAAAFQAAAEgDAEQgEAFgGAAQgEAAgDgCg");
	this.shape_950.setTransform(33.7,201.3);

	this.shape_951 = new cjs.Shape();
	this.shape_951.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIABgEQAFgNAYABQAOACAGAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAPAAQAPAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIgkAAQgSAAAAgLQAAgVAtAAQARAAANAHQAQAIAAANIgMAAQAAgIgMgFQgLgDgMAAQgXAAgBAFQgBAFAIAAIAeAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_951.setTransform(25.6,196.4);

	this.shape_952 = new cjs.Shape();
	this.shape_952.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIAAgEQAGgNAYABQANACAHAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAOAAQAQAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIg3AAIAAgJQAMAAABgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgEgCAAgEQgBgOApAAQASAAAOAHQAPAIAAANIgMAAQAAgIgMgFQgKgDgNAAQgUAAgBAFQgBAFAJAAIAZAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_952.setTransform(15,196.4);

	this.shape_953 = new cjs.Shape();
	this.shape_953.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgLAAQgBgGgIgEQgHgEgLABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAIgFQAGgFAKAAQAOAAAKAJQALAHAAAOQAAASgNAMQgMALgQAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgCACg");
	this.shape_953.setTransform(5.1,197.8);

	this.shape_954 = new cjs.Shape();
	this.shape_954.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_954.setTransform(-11,202.3);

	this.shape_955 = new cjs.Shape();
	this.shape_955.graphics.f("#FFFF00").s().p("AAcA0QgHgGAAgLIABgFIALAAIAAADQAAAOALAAQAIgBADgLQACgHAAgQQAAgMgBgJQgDgMgFAAQgHAAAAALIAAAWIgMAAIAAgWQAAgLgHgBQgDAAgCACIgCAFQAJALgBARQAAAVgNAMQgLAMgWABQgIAAgMgEQgMgFgEAAQgFAAgFAHIgJgGIARgYQgHgJAAgPIABgJQAEgPANgIQAKgFANAAIAHAAIAQgYIALAAIgPAZIAIADIAHAFIAFgFQAFgEAHAAQAIAAAFAGQAHgHAIABQAaABABAqQAAAUgHAOQgJAPgRABQgMAAgGgHgAgtArQAJADAIAAIAIAAQAMgCAHgJQAGgJAAgMQgBgLgGgIQgIgIgLgBIgKAOIAGAAQAGAAAFADQAFAEABAHQAAAHgEAGQgEAGgHACQgFACgGAAQgLAAgIgEIgGAIIAEAAQAFAAAFACgAguAYIAFACQAHABADgBQgGgBgCgEQgCgEABgDgAgeAPQAAAFAFAAQAFAAAAgFQAAgFgFAAQgFAAAAAFgAgzgEQgEAFAAAHQAAAEACAEIATgdQgLABgGAIg");
	this.shape_955.setTransform(-13.4,196.6);

	this.shape_956 = new cjs.Shape();
	this.shape_956.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_956.setTransform(-24.1,197.9);

	this.shape_957 = new cjs.Shape();
	this.shape_957.graphics.f("#FFFF00").s().p("AgDA8QgUgBgNgNQgLgMgBgTQAAgNAFgJQAEgJALgKIAVgTQAHgJACgFIATAAQgEAHgFAHQgGAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgIAJAAALQgBALAIAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_957.setTransform(-32.4,196.5);

	this.shape_958 = new cjs.Shape();
	this.shape_958.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_958.setTransform(-41.7,199.1);

	this.shape_959 = new cjs.Shape();
	this.shape_959.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgFgLgFgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_959.setTransform(-54.8,197.7);

	this.shape_960 = new cjs.Shape();
	this.shape_960.graphics.f("#FFFF00").s().p("AAAAyQgGAJgPAAQgOAAgJgLQgKgLABgOQABgRALgHIgPAAIAAgKIATAAQgDgCAAgGIABgEQAGgLAQAAIAHABQAMABAIAJQAHAJgBANIghAAQgJAAgGAFQgHAGAAAIQAAAIAFAFQAGAFAJAAQAGAAAFgEQAEgEAAgGIAAgNIAKAAIAAANQAAAGAEAEQAFAFAHgBQASAAACggQABgTgMgQQgNgQgSAAQgcgBgLASIgMAAQAOgcAkAAQAWABAPANQALAJAFAPQAGAOgBAPQgBAVgIANQgJAQgRAAQgOAAgIgJgAgUgWQACADAAADQAAADgCACIAXAAQgDgIgFgCQgEgCgFAAIgGABg");
	this.shape_960.setTransform(-62.7,196.5);

	this.shape_961 = new cjs.Shape();
	this.shape_961.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_961.setTransform(-74,196.5);

	this.shape_962 = new cjs.Shape();
	this.shape_962.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_962.setTransform(-87.5,191.4);

	this.shape_963 = new cjs.Shape();
	this.shape_963.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_963.setTransform(-92,197.7);

	this.shape_964 = new cjs.Shape();
	this.shape_964.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_964.setTransform(-104.1,196.4);

	this.shape_965 = new cjs.Shape();
	this.shape_965.graphics.f("#FFFF00").s().p("AgDA9QgUgBgNgMQgLgNgBgTQAAgMAFgKQAEgJALgJIAVgUQAHgIACgFIATAAQgEAHgFAGQgGAGgIAGQANgBAKAEQAcALAAAgQAAAVgPAOQgOANgUAAIgDgBgAgZgLQgIAJAAALQgBAMAIAHQAKAKAQAAQANAAAJgHQAMgJAAgOQAAgGgDgGQgDgJgIgEQgJgFgKAAQgRAAgJALgAAZgdQgHgFAAgIQAAgFACgDQAFgIAIAAQAEAAAEACQAHAFABAJQAAADgCAEQgFAIgJAAQgEAAgEgCgAAcgrQgBAGAGAAQAGAAAAgGQAAgGgGAAQgGAAABAGgAglghQgIgEAAgJQABgEACgDQAEgIAIAAQAFAAADACQAIAEAAAJQAAAEgCAEQgEAHgKAAQgDAAgEgCgAgjguQAAAGAFAAQAHAAAAgGQAAgGgHAAQgFAAAAAGg");
	this.shape_965.setTransform(-114.7,196.3);

	this.shape_966 = new cjs.Shape();
	this.shape_966.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_966.setTransform(-131.2,196.4);

	this.shape_967 = new cjs.Shape();
	this.shape_967.graphics.f("#FFFF00").s().p("AAOAkQAJAAAIgEQAIgFADgIQADgKgBgJQgBgKgHgHQgIgJgKABQgRABgEAPQACgCAFAAQAFAAADACQALAHgBAQQgBAPgNAJQgLAJgRAAQgRAAgLgLQgMgKAAgQQAAgRANgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQARAAAMAPQAKANABATQgBATgNAOQgNANgUAAgAgogFQgGAEAAAGQAAAKAJAFQAJAFAKAAQALAAAHgEQAHgFABgHQABgEgCgDQgCgCgEAAQgCAAgBABQgCACAAADIgLAAIAAgIIAAgIIgNAAQgHAAgFAFgAglghQACABABAEQACAEgCAEIANAAQgBgHgEgEQgEgDgFAAIgCABg");
	this.shape_967.setTransform(-142.9,197.7);

	this.shape_968 = new cjs.Shape();
	this.shape_968.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgKAAQgCgGgIgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgBACg");
	this.shape_968.setTransform(-153.8,197.8);

	this.shape_969 = new cjs.Shape();
	this.shape_969.graphics.f("#FFFF00").s().p("AgKATQAIgGAAgEIAAgCIgFgDQgFgEAAgFQAAgEACgDQAEgGAGAAQAEAAAEADQAFADAAAHQAAAFgCAEQgBADgFAFIgHAHg");
	this.shape_969.setTransform(-166.5,201.9);

	this.shape_970 = new cjs.Shape();
	this.shape_970.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIABgEQAFgNAYABQAOACAGAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAPAAQAPAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIgkAAQgSAAAAgLQAAgVAtAAQARAAANAHQAQAIAAANIgMAAQAAgIgMgFQgLgDgMAAQgXAAgBAFQgBAFAIAAIAeAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_970.setTransform(-174.9,196.4);

	this.shape_971 = new cjs.Shape();
	this.shape_971.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIAAgEQAGgNAYABQANACAHAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAOAAQAQAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIg3AAIAAgJQAMAAABgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgEgCAAgEQgBgOApAAQASAAAOAHQAPAIAAANIgMAAQAAgIgMgFQgKgDgNAAQgUAAgBAFQgBAFAJAAIAZAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_971.setTransform(-185.5,196.4);

	this.shape_972 = new cjs.Shape();
	this.shape_972.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMAAgKAJQgLAIABANQgBANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAIgFQAGgFAKAAQAOAAALAJQAKAHAAAOQAAASgNAMQgMALgRAAQgSAAgOgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgBACg");
	this.shape_972.setTransform(-195.4,197.8);

	this.shape_973 = new cjs.Shape();
	this.shape_973.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_973.setTransform(-208.9,197.9);

	this.shape_974 = new cjs.Shape();
	this.shape_974.graphics.f("#FFFF00").s().p("AgDA8QgUgBgNgNQgMgMAAgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJABgFIAUAAQgEAHgGAHQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgagMQgHAJgBALQAAALAIAIQAKAKAQAAQAMAAAKgHQAMgJAAgPQAAgGgCgFQgEgJgJgFQgIgEgKAAQgRAAgKALg");
	this.shape_974.setTransform(-217.2,196.5);

	this.shape_975 = new cjs.Shape();
	this.shape_975.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLAAgNQAAgMgCgLQgFgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_975.setTransform(-224.7,197.7);

	this.shape_976 = new cjs.Shape();
	this.shape_976.graphics.f("#FFFF00").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgMALABQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAJgMAAQgMABgIgIQgIgIABgKIACgGIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAIQAAAPALALQAMAKAQAAQAQgBAMgLQAMgNAAgQQAAgUgKgMQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaAAQgNABgMgGgAgPgEQAAAEAFAEQAEAEAGAAQAGAAAGgFQAFgEAAgFIAAgEQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgBgDgBQgFABAAAGg");
	this.shape_976.setTransform(-232.5,196.4);

	this.shape_977 = new cjs.Shape();
	this.shape_977.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_977.setTransform(-244.1,198.7);

	this.shape_978 = new cjs.Shape();
	this.shape_978.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_978.setTransform(-257.8,191.4);

	this.shape_979 = new cjs.Shape();
	this.shape_979.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_979.setTransform(-262.2,197.7);

	this.shape_980 = new cjs.Shape();
	this.shape_980.graphics.f("#FFFF00").s().p("AgKAvQgTAAgKgOQgKgNAAgSQAAgVAIgKQgFABgFAKQgEAKAAANQAAANAHANQAGAKAIAFIgPAAQgFgDgFgKQgIgNAAgQQAAgLAEgLQAEgKAFgGQALgLANAAIAAAGIADgDQAHgEAMAAIAGAAIAAALQAMgMARABQAUAAALAOQAKAMAAAUQABATgMAOQgLAOgUAAIgGAAIAAgJQgHAFgGACQgGACgIAAIgDAAgAARgPQAPAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgWgCgCAPQAEgEAHAAIAGABgAgcgaQgGAJAAALQAAANAHAIQAHAJAMAAQAJABAIgFQAIgEAAgIQAAgFgCgDQgDgEgFAAQgEAAgCADQgBACAAADIAAAFIgLAAQgCgLABgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_980.setTransform(-275.6,197.7);

	this.shape_981 = new cjs.Shape();
	this.shape_981.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_981.setTransform(-288.3,202.3);

	this.shape_982 = new cjs.Shape();
	this.shape_982.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQAAgRANgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgFAAIgCABg");
	this.shape_982.setTransform(-288.9,197.7);

	this.shape_983 = new cjs.Shape();
	this.shape_983.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_983.setTransform(239,175.2);

	this.shape_984 = new cjs.Shape();
	this.shape_984.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_984.setTransform(227.6,174);

	this.shape_985 = new cjs.Shape();
	this.shape_985.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_985.setTransform(217.8,176.6);

	this.shape_986 = new cjs.Shape();
	this.shape_986.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_986.setTransform(202.8,176.6);

	this.shape_987 = new cjs.Shape();
	this.shape_987.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_987.setTransform(192,176.2);

	this.shape_988 = new cjs.Shape();
	this.shape_988.graphics.f("#FFFF00").s().p("AgJA2QgEgEAAgGQAAgFAFgEQAEgEAEAAQAGAAAEAEQAEAEgBAFQAAAGgDAEQgEAEgGAAQgFAAgEgEgAgFAaIgFhTIAWAAIgFBTg");
	this.shape_988.setTransform(178.1,174.1);

	this.shape_989 = new cjs.Shape();
	this.shape_989.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgDgLQgDgLgGgBQgGAAgDADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_989.setTransform(173.5,175.2);

	this.shape_990 = new cjs.Shape();
	this.shape_990.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGADgHQAEgHAFgEQgFgDAAgGIABgFQAEgLAVAAQAPAAAGAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQAAALAPAGQAKAEAQAAQAQAAAKgEQAPgGAAgMQAAgHgEgEQgFgGgGAAIg0AAQgGAAgFAGgAAMghQgFADAAAGIAZAAIABgDQAAgEgEgCQgDgCgEAAQgFAAgFACgAgcgjQACACAAADQABADgCADIAUAAQAAgJgMgCIgEgBIgFABg");
	this.shape_990.setTransform(165.7,175.3);

	this.shape_991 = new cjs.Shape();
	this.shape_991.graphics.f("#FFFF00").s().p("AgmA0QgPgJAAgSQAAgQAKgHIgKAAIAAgJIAOAAQgDgBAAgFQAAgGAEgDQAJgHAOACQAQABAGAKQAEAGABAMIgaAAQgWAAgCAPQgBALAMAGQAMAFAOgBQAPAAAKgKQALgIADgPIABgHQAAgMgIgMQgHgLgKgFIgFAFIgGAEQgHAEgIAAQgMAAgEgGQgDgEACgHQABgDgGAAIgEABIAAgKQAJgCAJAAQANAAAIACQAVAEAPAPQAQARAAAbQABAXgQARQgQAQgYAAQgWAAgOgJgAgWgQQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABQABACgDACIAVAAQgBgFgJgDIgFgBIgGABgAgSgsQABAEAHAAQAJAAADgGQgHgDgNAAIAAAFg");
	this.shape_991.setTransform(154.4,173.9);

	this.shape_992 = new cjs.Shape();
	this.shape_992.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_992.setTransform(136.9,175.2);

	this.shape_993 = new cjs.Shape();
	this.shape_993.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_993.setTransform(128.7,168.9);

	this.shape_994 = new cjs.Shape();
	this.shape_994.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_994.setTransform(124.6,175.2);

	this.shape_995 = new cjs.Shape();
	this.shape_995.graphics.f("#FFFF00").s().p("AgCA+QgUgBgMgMQgNgNAAgTQAAgRAKgMQgDgBgDgEQgDgEgBgDIgCgKQAAgIAIgKQAIgJAOAAIAJABQAJADABAHIAFgJIATAAQgEAIgHAIQgHAHgHAFQANgBAKAEQAcALAAAgQAAAVgPAOQgPAMgUAAIgCAAgAgZgKQgIAJAAALQAAAMAIAHQAKAKAPAAQAOAAAJgHQAMgJAAgOQAAgGgDgGQgDgJgJgEQgIgFgKAAQgRAAgKALgAgfgnQgEAGAAAEQAAAHAFACIADgDIAWgUQgCgEgFAAIgEAAQgKAAgFAIg");
	this.shape_995.setTransform(113.4,173.7);

	this.shape_996 = new cjs.Shape();
	this.shape_996.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgFAAIgCABg");
	this.shape_996.setTransform(102.1,175.2);

	this.shape_997 = new cjs.Shape();
	this.shape_997.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_997.setTransform(89.2,175.2);

	this.shape_998 = new cjs.Shape();
	this.shape_998.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_998.setTransform(76.8,176.2);

	this.shape_999 = new cjs.Shape();
	this.shape_999.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_999.setTransform(63,168.9);

	this.shape_1000 = new cjs.Shape();
	this.shape_1000.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1000.setTransform(58.9,175.2);

	this.shape_1001 = new cjs.Shape();
	this.shape_1001.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_1001.setTransform(47.3,173.9);

	this.shape_1002 = new cjs.Shape();
	this.shape_1002.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_1002.setTransform(35.7,175.2);

	this.shape_1003 = new cjs.Shape();
	this.shape_1003.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1003.setTransform(22.1,168.9);

	this.shape_1004 = new cjs.Shape();
	this.shape_1004.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1004.setTransform(17.6,175.2);

	this.shape_1005 = new cjs.Shape();
	this.shape_1005.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1005.setTransform(5.4,174);

	this.shape_1006 = new cjs.Shape();
	this.shape_1006.graphics.f("#FFFF00").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1006.setTransform(-2.6,175.2);

	this.shape_1007 = new cjs.Shape();
	this.shape_1007.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_1007.setTransform(-8.8,176.6);

	this.shape_1008 = new cjs.Shape();
	this.shape_1008.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1008.setTransform(-19.5,174);

	this.shape_1009 = new cjs.Shape();
	this.shape_1009.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1009.setTransform(-31.5,175.2);

	this.shape_1010 = new cjs.Shape();
	this.shape_1010.graphics.f("#FFFF00").s().p("AgHALQgFgFAAgGQAAgEACgDQAEgFAGgBQAEAAAEADQAFAFAAAFQAAAEgDAEQgEAFgGAAQgEAAgDgCg");
	this.shape_1010.setTransform(-46,178.8);

	this.shape_1011 = new cjs.Shape();
	this.shape_1011.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEACgCQAFgLAOgFQAKgDASgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1011.setTransform(-54.7,169.3);

	this.shape_1012 = new cjs.Shape();
	this.shape_1012.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1012.setTransform(-54.7,175.2);

	this.shape_1013 = new cjs.Shape();
	this.shape_1013.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1013.setTransform(-63.4,180.9);

	this.shape_1014 = new cjs.Shape();
	this.shape_1014.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1014.setTransform(-67.1,175.2);

	this.shape_1015 = new cjs.Shape();
	this.shape_1015.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_1015.setTransform(-79.4,179.8);

	this.shape_1016 = new cjs.Shape();
	this.shape_1016.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1016.setTransform(-79.6,175.2);

	this.shape_1017 = new cjs.Shape();
	this.shape_1017.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1017.setTransform(-97.7,175.2);

	this.shape_1018 = new cjs.Shape();
	this.shape_1018.graphics.f("#FFFF00").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgfAVgNQAIgEALAAIAGAAIAAALQANgMARABQAUAAAKAOQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgJQgIAFgEACQgGACgJAAIgEAAgAAHgPQAQAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgVgCgDAPQAEgEAHAAIAFABgAglgaQgHAJAAALQAAANAHAIQAHAJANAAQAKABAHgFQAHgEABgIQAAgFgDgDQgDgEgEAAQgDAAgDADQgCACAAADIABAFIgLAAQgDgLACgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_1018.setTransform(-110.3,175.2);

	this.shape_1019 = new cjs.Shape();
	this.shape_1019.graphics.f("#FFFF00").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1019.setTransform(-124.5,175.2);

	this.shape_1020 = new cjs.Shape();
	this.shape_1020.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1020.setTransform(-132.2,174);

	this.shape_1021 = new cjs.Shape();
	this.shape_1021.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1021.setTransform(-139.9,168.9);

	this.shape_1022 = new cjs.Shape();
	this.shape_1022.graphics.f("#FFFF00").s().p("AgnApQgPgRAAgZQABgZAPgQQAQgQAagBQAWAAAOAPQAPAOAAAWQAAARgKAMQgLANgPABQgSAAgIgKQgKgKABgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQAKAAAHgJQAGgGgBgLQgBgNgGgIQgKgMgUAAQgRAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAALgQIANAAQgOAegkAAQgZgBgQgSg");
	this.shape_1022.setTransform(-143.5,176.5);

	this.shape_1023 = new cjs.Shape();
	this.shape_1023.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_1023.setTransform(-153.4,176.6);

	this.shape_1024 = new cjs.Shape();
	this.shape_1024.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1024.setTransform(-167.1,180.9);

	this.shape_1025 = new cjs.Shape();
	this.shape_1025.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1025.setTransform(-170.8,175.2);

	this.shape_1026 = new cjs.Shape();
	this.shape_1026.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1026.setTransform(-179.5,168.9);

	this.shape_1027 = new cjs.Shape();
	this.shape_1027.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1027.setTransform(-184,175.2);

	this.shape_1028 = new cjs.Shape();
	this.shape_1028.graphics.f("#FFFF00").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1028.setTransform(-192.9,175.2);

	this.shape_1029 = new cjs.Shape();
	this.shape_1029.graphics.f("#FFFF00").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJACgFIATAAIgKAOQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJABALQAAALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_1029.setTransform(-200,174);

	this.shape_1030 = new cjs.Shape();
	this.shape_1030.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgLAAQAAgGgJgEQgIgEgKABQgMAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAKAAQAOAAALAJQAKAHAAAOQAAASgNAMQgLALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgCgCgEgBQgDAAgBACg");
	this.shape_1030.setTransform(-209.7,175.3);

	this.shape_1031 = new cjs.Shape();
	this.shape_1031.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_1031.setTransform(-216.7,175.2);

	this.shape_1032 = new cjs.Shape();
	this.shape_1032.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGADgHQAEgHAFgEQgFgDAAgGIABgFQAEgLAUAAQARAAAFAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQAAALAPAGQALAEAPAAQAQAAAKgEQAOgGABgMQAAgHgEgEQgFgGgGAAIg0AAQgGAAgFAGgAAMghQgFADAAAGIAZAAIABgDQgBgEgDgCQgDgCgEAAQgGAAgEACgAgcgjQACACAAADQABADgBADIATAAQAAgJgMgCIgEgBIgFABg");
	this.shape_1032.setTransform(-224.6,175.3);

	this.shape_1033 = new cjs.Shape();
	this.shape_1033.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgLAAQAAgGgJgEQgHgEgLABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAIgFQAGgFAJAAQAPAAAKAJQALAHAAAOQAAASgNAMQgMALgQAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgEAAgCACg");
	this.shape_1033.setTransform(-234.9,175.3);

	this.shape_1034 = new cjs.Shape();
	this.shape_1034.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1034.setTransform(-241.9,175.2);

	this.shape_1035 = new cjs.Shape();
	this.shape_1035.graphics.f("#FFFF00").s().p("AgnApQgPgRAAgZQABgZAQgQQAQgQAZgBQAVAAAPAPQAOAOAAAWQAAARgJAMQgLANgQABQgQAAgKgKQgKgKACgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQAKAAAHgJQAGgGAAgLQgBgNgHgIQgKgMgVAAQgQAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAAKgQIAOAAQgOAeglAAQgYgBgQgSg");
	this.shape_1035.setTransform(-249.6,176.5);

	this.shape_1036 = new cjs.Shape();
	this.shape_1036.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_1036.setTransform(-261.1,173.9);

	this.shape_1037 = new cjs.Shape();
	this.shape_1037.graphics.f("#FFFF00").s().p("AAeAVQAIgEAAgHIgBgDQgBgGgMgCQgKgCgOgBQgNAAgKADQgMACgBAGIgBADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJADg");
	this.shape_1037.setTransform(228.9,146.8);

	this.shape_1038 = new cjs.Shape();
	this.shape_1038.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1038.setTransform(228.2,152.7);

	this.shape_1039 = new cjs.Shape();
	this.shape_1039.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1039.setTransform(218.5,152.9);

	this.shape_1040 = new cjs.Shape();
	this.shape_1040.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_1040.setTransform(209.4,153.7);

	this.shape_1041 = new cjs.Shape();
	this.shape_1041.graphics.f("#FFFF00").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgMALABQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAJgMAAQgMABgIgIQgIgIABgKIACgGIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAIQAAAPALALQAMAKAQAAQAQgBAMgLQAMgNAAgQQAAgUgKgMQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaAAQgNABgMgGgAgPgEQAAAEAFAEQAEAEAGAAQAGAAAGgFQAFgEAAgFIAAgEQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgBgDgBQgFABAAAGg");
	this.shape_1041.setTransform(192,151.4);

	this.shape_1042 = new cjs.Shape();
	this.shape_1042.graphics.f("#FFFF00").s().p("AgjAqQgQgNAAgVQAAgTAOgLQgBgKAEgHQAIgQARACQAIAAAFAGQAGgHAMABQALAAAGAHQAGAIAAASIgtAAIgTABQAAAMAJAIQAJAJALgBQAJgBAHgFQAHgEABgIIALAAQgCANgLAIQgLAIgOABQgbAAgLgUIgBAHQAAAOALAJQALAJANAAQAVAAALgNQAGgIAAgJIALAAQABAMgIALQgPAVgdAAQgVAAgPgMgAgQgnQgDAEAAAGIAJgBIAJAAQAAgMgIAAQgFAAgCADgAAKgeIATAAQgBgMgIAAQgKAAAAAMg");
	this.shape_1042.setTransform(180.9,153.5);

	this.shape_1043 = new cjs.Shape();
	this.shape_1043.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1043.setTransform(172.5,158.4);

	this.shape_1044 = new cjs.Shape();
	this.shape_1044.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1044.setTransform(168.8,152.7);

	this.shape_1045 = new cjs.Shape();
	this.shape_1045.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_1045.setTransform(150.7,151.4);

	this.shape_1046 = new cjs.Shape();
	this.shape_1046.graphics.f("#FFFF00").s().p("AAEA8QgEgCgCgFQgGAJgRAAQgTAAgIgWQgFgNABgVQABgOAIgKQAJgMANABQAGAAAEADQAEAEgBAGQAAACgDAEQgCAEAAADQABAJANABQAEAAAEgCQAFgDACgCIgHAAQgFAAgFgDQgFgDAAgFQgBgIAFgFQAFgGAHAAQATABAAAWQAAAKgHAHQgIAHgKABQgKABgHgEQgIgEgDgIIgBgFQAAgGAEgEQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAgBAAQgGAAgFAIQgFAJAAAIQgBAMAGAJQAGALAKAAQAPAAAAgRIAKAAQAAAJAEAEQAEAEAIAAQANAAAHgNQAFgKAAgPQAAgTgKgOQgNgRgVAAQgiAAgGAWIgMAAQADgOAOgJQAOgKAWAAQAdABAPAWQAMARAAAVQAAAagIAQQgKAUgVAAQgJAAgFgCgAAAgOQAAAGAFAAQADAAACgCQABAAAAgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgGAAAAAHg");
	this.shape_1046.setTransform(138.8,151.3);

	this.shape_1047 = new cjs.Shape();
	this.shape_1047.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_1047.setTransform(121.3,151.4);

	this.shape_1048 = new cjs.Shape();
	this.shape_1048.graphics.f("#FFFF00").s().p("AAEA8QgEgCgCgFQgGAJgRAAQgTAAgIgWQgFgNABgVQABgOAIgKQAJgMANABQAGAAAEADQAEAEgBAGQAAACgDAEQgCAEAAADQABAJANABQAEAAAEgCQAFgDACgCIgHAAQgFAAgFgDQgFgDAAgFQgBgIAFgFQAFgGAHAAQATABAAAWQAAAKgHAHQgIAHgKABQgKABgHgEQgIgEgDgIIgBgFQAAgGAEgEQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAgBAAQgGAAgFAIQgFAJAAAIQgBAMAGAJQAGALAKAAQAPAAAAgRIAKAAQAAAJAEAEQAEAEAIAAQANAAAHgNQAFgKAAgPQAAgTgKgOQgNgRgVAAQgiAAgGAWIgMAAQADgOAOgJQAOgKAWAAQAdABAPAWQAMARAAAVQAAAagIAQQgKAUgVAAQgJAAgFgCgAAAgOQAAAGAFAAQADAAACgCQABAAAAgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgGAAAAAHg");
	this.shape_1048.setTransform(109.5,151.3);

	this.shape_1049 = new cjs.Shape();
	this.shape_1049.graphics.f("#FFFF00").s().p("AgHALQgFgFAAgGQAAgEACgDQAEgFAGgBQAEAAAEADQAFAFAAAFQAAAEgDAEQgEAFgGAAQgEAAgDgCg");
	this.shape_1049.setTransform(95.4,156.3);

	this.shape_1050 = new cjs.Shape();
	this.shape_1050.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1050.setTransform(89.4,152.9);

	this.shape_1051 = new cjs.Shape();
	this.shape_1051.graphics.f("#FFFF00").s().p("AAOAkQAJAAAIgEQAHgFAEgIQADgKgBgJQgBgKgHgHQgIgJgKABQgRABgEAPQADgCAEAAQAFAAADACQAKAHAAAQQgBAPgNAJQgLAJgRAAQgRAAgLgLQgLgKAAgQQAAgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAALAPQALANgBATQABATgNAOQgOANgUAAgAgogFQgGAEAAAGQABAKAIAFQAJAFAKAAQALAAAHgEQAHgFABgHQAAgEgCgDQgBgCgDAAQgDAAgBABQgCACAAADIgKAAIgBgIIABgIIgOAAQgHAAgFAFgAgmghQADABACAEQABAEgBAEIAMAAQgCgHgDgEQgDgDgGAAIgDABg");
	this.shape_1051.setTransform(80,152.7);

	this.shape_1052 = new cjs.Shape();
	this.shape_1052.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1052.setTransform(70.6,152.9);

	this.shape_1053 = new cjs.Shape();
	this.shape_1053.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1053.setTransform(61.2,152.7);

	this.shape_1054 = new cjs.Shape();
	this.shape_1054.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1054.setTransform(43.2,152.7);

	this.shape_1055 = new cjs.Shape();
	this.shape_1055.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAHAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_1055.setTransform(32.6,152.8);

	this.shape_1056 = new cjs.Shape();
	this.shape_1056.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1056.setTransform(25.8,146.4);

	this.shape_1057 = new cjs.Shape();
	this.shape_1057.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1057.setTransform(21.7,152.7);

	this.shape_1058 = new cjs.Shape();
	this.shape_1058.graphics.f("#FFFF00").s().p("AgcA4QgMgHABgMQABgHAGgDQAFgDAIAAIAFABIAAAJIgDAAQgHAAAAAEQAAAHAYgBQAHAAAGgCQAIgCgBgDQAAgFgNABIgLABIAAgKQATgCALgHQANgJAAgOIgBgHQgDgMgLgGQgLgGgNAAQgPAAgLAGQgMAHAAANQgBALAIAGQAGAGALAAQAGAAAFgCQAFgDACgFQgEADgFAAQgHAAgFgEQgFgEAAgHQAAgJAFgFQAFgEAJAAQAKgBAGAHQAHAGgBALQAAAMgKAIQgIAHgNABQgSABgLgKQgLgKAAgRQAAgWAQgMQAPgMAXABQAYAAAOANQANANAAAUQAAAbgZAOQAIAFAAAIQAAAJgLAGQgKAGgPAAIgCAAQgQAAgKgFgAgIgSQgCACAAADQAAAEADACQACACADAAQACAAACgDQADgCAAgDQAAgDgDgCQgCgDgCAAQgDAAgDADg");
	this.shape_1058.setTransform(10.2,154.2);

	this.shape_1059 = new cjs.Shape();
	this.shape_1059.graphics.f("#FFFF00").s().p("AALBFQgJgEAAgJQgBgHAGgEQgTABgNgLQgOgLACgRQABgTAQgJQgGgBABgKQgHADgGALQgGAKAAALQAAANAIANQAGAKAHAFIgOAAQgGgDgFgKQgHgNAAgQQgBgLAFgLQAFgKAGgFQAFgFAGgDQgDgCAAgGQAAgEACgEQAHgMAeAAQAbAAAHALQADAFAAAEQAAAKgJADQAIAJABAOIgiAAQgKAAgHAGQgHAHAAAIQgBALAJAJQAIAHANABQATABANgHIAAALIgNAEQgLADgBAFQAAAEADACQADACAFAAQAFAAAEgDQAEgDAAgFIAAgEIAKAAIABAFQAAAKgJAGQgHAGgLABIgFAAQgIAAgGgDgAgFgkQADACABADQAAAEgDABIAFAAIAcAAQgDgHgJgDQgFgBgHAAIgKABgAATgyQAAAAAAABQAAAAABABQAAAAAAABQABAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAABAAQAFAAAAgFQAAAAgBgBQAAAAAAgBQAAgBgBAAQAAgBAAAAIgFgCQgFAAAAAFgAAAg5QgSACAAAHIABADIAFgBQAFgCALAAIAGAAQgCgCAAgDIACgEIgEAAIgGAAg");
	this.shape_1059.setTransform(-6.1,152.9);

	this.shape_1060 = new cjs.Shape();
	this.shape_1060.graphics.f("#FFFF00").s().p("AghA8QgTgBgMgNQgNgMgBgTQABgNAEgJQAFgJALgKIAUgTQAHgJADgFIATAAQgDAHgGAHQgFAGgJAGQANgBAKAEQAbAKAAAhQAAAUgOAOQgOANgUAAIgEAAgAg3gMQgIAJAAALQAAALAIAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgCgJgJgFQgIgEgLAAQgRAAgKALgAAcAyQgGgKAAgQQAAgTAQgaQAHgLALgPIg6AAIAAgLIBJAAIAAAKQgMAOgIANQgOAVAAASQAAATAQAAQAFAAAFgDQAEgDAAgFQAAgEgDgEIAKAAQAFAEAAALQAAAMgJAHQgIAIgMAAQgOAAgIgKg");
	this.shape_1060.setTransform(-20.1,151.5);

	this.shape_1061 = new cjs.Shape();
	this.shape_1061.graphics.f("#FFFF00").s().p("AgKAvQgTAAgKgOQgKgNAAgSQAAgVAIgKQgFABgFAKQgEAKAAANQAAANAHANQAGAKAIAFIgPAAQgFgDgFgKQgIgNAAgQQAAgLAEgLQAEgKAFgGQALgLANAAIAAAGIADgDQAHgEAMAAIAGAAIAAALQAMgMARABQAUAAALAOQAKAMAAAUQABATgMAOQgLAOgUAAIgGAAIAAgJQgHAFgGACQgGACgIAAIgDAAgAARgPQAPAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgWgCgCAPQAEgEAHAAIAGABgAgcgaQgGAJAAALQAAANAHAIQAHAJAMAAQAJABAIgFQAIgEAAgIQAAgFgCgDQgDgEgFAAQgEAAgCADQgBACAAADIAAAFIgLAAQgCgLABgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_1061.setTransform(-41,152.7);

	this.shape_1062 = new cjs.Shape();
	this.shape_1062.graphics.f("#FFFF00").s().p("AgjAqQgQgNAAgVQAAgTAOgLQgBgKAEgHQAIgQARACQAIAAAFAGQAGgHAMABQALAAAGAHQAGAIAAASIgtAAIgTABQAAAMAJAIQAJAJALgBQAJgBAHgFQAHgEABgIIALAAQgCANgLAIQgLAIgOABQgbAAgLgUIgBAHQAAAOALAJQALAJANAAQAVAAALgNQAGgIAAgJIALAAQABAMgIALQgPAVgdAAQgVAAgPgMgAgQgnQgDAEAAAGIAJgBIAJAAQAAgMgIAAQgFAAgCADgAAKgeIATAAQgBgMgIAAQgKAAAAAMg");
	this.shape_1062.setTransform(-53.4,153.5);

	this.shape_1063 = new cjs.Shape();
	this.shape_1063.graphics.f("#FFFF00").s().p("AABA6QgGgDgEgHQgCAFgFAEQgFADgIAAQgNAAgIgOQgGgMAAgRQAAgTALgOQAMgQATAAQAOAAAJAJQAJAJAAAOQAAAKgIAHIgFAEQgCADgBADQAAALAPAAQALAAAHgMQAGgLAAgPQAAgTgLgOQgMgPgXAAQgLAAgKAFQgKAFgFAJIgMAAQAFgPAOgIQANgHAQAAQAbAAAQAQQAIAHAFANQAFANABALQAAAcgMAQQgLAPgRAAQgKAAgGgCgAghAkQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgDAAgCACgAgpADQgDAHAAAJQAAAHACAGQAFgJAKgBQAJgBAHAGQABgGAFgFQAGgGAAgIQgBgFgDgFQgFgEgHAAQgTAAgHAPg");
	this.shape_1063.setTransform(-70.4,151.4);

	this.shape_1064 = new cjs.Shape();
	this.shape_1064.graphics.f("#FFFF00").s().p("AAEA8QgEgCgCgFQgGAJgRAAQgTAAgIgWQgFgNABgVQABgOAIgKQAJgMANABQAGAAAEADQAEAEgBAGQAAACgDAEQgCAEAAADQABAJANABQAEAAAEgCQAFgDACgCIgHAAQgFAAgFgDQgFgDAAgFQgBgIAFgFQAFgGAHAAQATABAAAWQAAAKgHAHQgIAHgKABQgKABgHgEQgIgEgDgIIgBgFQAAgGAEgEQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAgBAAQgGAAgFAIQgFAJAAAIQgBAMAGAJQAGALAKAAQAPAAAAgRIAKAAQAAAJAEAEQAEAEAIAAQANAAAHgNQAFgKAAgPQAAgTgKgOQgNgRgVAAQgiAAgGAWIgMAAQADgOAOgJQAOgKAWAAQAdABAPAWQAMARAAAVQAAAagIAQQgKAUgVAAQgJAAgFgCgAAAgOQAAAGAFAAQADAAACgCQABAAAAgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgGAAAAAHg");
	this.shape_1064.setTransform(-82.3,151.3);

	this.shape_1065 = new cjs.Shape();
	this.shape_1065.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1065.setTransform(-96.1,146.4);

	this.shape_1066 = new cjs.Shape();
	this.shape_1066.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1066.setTransform(-100.2,152.7);

	this.shape_1067 = new cjs.Shape();
	this.shape_1067.graphics.f("#FFFF00").s().p("AgkAyQgPgOAAgWQAAgOAGgLQAHgMANAAQAMAAACAKQADgLANAAQAJAAAHAIQAGAIAAAJQAAAMgIAIQgIAJgLAAQgJAAgIgGQgIgGAAgJIADgOQABgGgHAAQgEAAgDAHQgDAFAAAHQAAAQAMAJQALAJAPAAQAQAAALgLQALgLAAgQQAAgNgFgIQgGgKgMAAIgqAAQgUAAAAgNQAAgMAQgHQAMgFATAAQAcAAANAQQAHAHAAAKIgLAAIAAAAIAAAAIAAAAQAPALAAAdQAAAYgOAQQgPAQgXAAQgWAAgOgPgAgMACQAAAFAEAEQAEAEAFAAQAGAAAEgEQAFgEAAgGQgFAHgIAAQgJAAgGgJIAAADgAAAgMQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAABAAAAQABABAAAAQAAABAAAAQAAABABAAQAAAAAAAAQABABABAAQAAAAABAAIAEgCQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAIgBgEQgBgBAAAAQAAAAgBAAQAAgBgBAAQgBAAAAAAQgBAAAAAAQgBAAgBABQAAAAAAAAQgBAAAAABgAAlgfQgCgKgLgFQgJgFgOAAQgaAAAAAJQAAABAAAAQAAAAAAABQAAAAABAAQAAABABAAQAAABABAAQAAAAAAAAQABABABAAQAAAAABAAIAiAAQAJAAAHADIAGADIAAAAgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAfgiQgHgDgJAAIgiAAQgBAAAAAAQgBAAgBgBQAAAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAAAAAgBQAAgJAaAAQAOAAAJAFQALAFACAKIgGgDg");
	this.shape_1067.setTransform(-111.7,151);

	this.shape_1068 = new cjs.Shape();
	this.shape_1068.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_1068.setTransform(-121.3,154.1);

	this.shape_1069 = new cjs.Shape();
	this.shape_1069.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAGAGABAJIgKAAQgCgGgIgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgBACg");
	this.shape_1069.setTransform(-130.9,152.8);

	this.shape_1070 = new cjs.Shape();
	this.shape_1070.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1070.setTransform(-137.6,146.4);

	this.shape_1071 = new cjs.Shape();
	this.shape_1071.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1071.setTransform(-141.7,152.7);

	this.shape_1072 = new cjs.Shape();
	this.shape_1072.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1072.setTransform(-154.2,152.7);

	this.shape_1073 = new cjs.Shape();
	this.shape_1073.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_1073.setTransform(-171.9,151.4);

	this.shape_1074 = new cjs.Shape();
	this.shape_1074.graphics.f("#FFFF00").s().p("AgnApQgPgRAAgZQABgZAPgQQAQgQAagBQAWAAAOAPQAPAOAAAWQAAARgKAMQgLANgPABQgSAAgIgKQgKgKABgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQAKAAAHgJQAGgGgBgLQgBgNgGgIQgKgMgUAAQgRAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAALgQIANAAQgOAegkAAQgZgBgQgSg");
	this.shape_1074.setTransform(-183.1,154);

	this.shape_1075 = new cjs.Shape();
	this.shape_1075.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1075.setTransform(-194.5,151.5);

	this.shape_1076 = new cjs.Shape();
	this.shape_1076.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgEgLgGgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_1076.setTransform(-208.2,152.7);

	this.shape_1077 = new cjs.Shape();
	this.shape_1077.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1077.setTransform(-216.4,152.7);

	this.shape_1078 = new cjs.Shape();
	this.shape_1078.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_1078.setTransform(-226.7,154.1);

	this.shape_1079 = new cjs.Shape();
	this.shape_1079.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgHIAJAAIAAgMIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAAMQAIgBAAgFQAAgGgHAAIgBAAg");
	this.shape_1079.setTransform(-233.8,146.4);

	this.shape_1080 = new cjs.Shape();
	this.shape_1080.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQgBgMgDgLQgDgLgGgBQgGAAgEADIAAgLQAFgEAKAAQAMAAAIAQQAIAPAAAQQAAARgIAOQgIAPgNAAQgIAAgGgDg");
	this.shape_1080.setTransform(-234.1,152.7);

	this.shape_1081 = new cjs.Shape();
	this.shape_1081.graphics.f("#FFFF00").s().p("AgiAzQgPgLgBgRIAAgFQABgFAEgFQAEgGAEgCIgTAAIAAgJIAPAAQgCgEABgGQABgHAJgEQAIgDAKAAQANAAAGALQAJAJgBANIgbAAQgHAAgGAEQgGAEAAAHQgBALAMAHQAKAEAPAAQASAAALgMQAKgLABgQQABgUgLgNQgMgNgVAAQgXgCgNATIgMAAQAGgOANgHQANgHAQAAQAZAAAQARQAQARAAAZQAAAZgPARQgPASgYAAQgXAAgOgJgAgXgVQADABABADQABAEgCAEIAUAAQgBgGgEgDQgEgFgGABIgIABg");
	this.shape_1081.setTransform(-242,151.5);

	this.shape_1082 = new cjs.Shape();
	this.shape_1082.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgHgEgKABQgNAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAHgFQAHgFAJAAQAPAAAKAJQALAHAAAOQAAASgMAMQgMALgRAAQgTAAgNgOgAgCgLQgCADAAADQAAAEACABQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgEAAgCACg");
	this.shape_1082.setTransform(-252.2,152.8);

	this.shape_1083 = new cjs.Shape();
	this.shape_1083.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAIgFAEgIQADgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQACgCAEAAQAGAAADACQALAHgBAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRAMgIQgDgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQgBAKAKAFQAHAFAMAAQAJAAAIgEQAHgFABgHQAAgEgBgDQgCgCgEAAQgBAAgCABQgDACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgFAAIgCABg");
	this.shape_1083.setTransform(247,130.2);

	this.shape_1084 = new cjs.Shape();
	this.shape_1084.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_1084.setTransform(234.7,130.2);

	this.shape_1085 = new cjs.Shape();
	this.shape_1085.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1085.setTransform(217.4,130.2);

	this.shape_1086 = new cjs.Shape();
	this.shape_1086.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGAEgHQADgHAGgEQgGgDAAgGIABgFQAEgLAUAAQARAAAFAJQAHgJARAAQAIAAAHAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQAAALAPAGQAKAEAQAAQAPAAALgEQAPgGAAgMQABgHgFgEQgEgGgIAAIgyAAQgIAAgEAGgAAMggQgFACgBAGIAaAAIAAgDQAAgEgCgCQgEgCgEAAQgFAAgFADgAgdgjQADACABADQAAADgBADIATAAQAAgJgMgCIgEgBIgGABg");
	this.shape_1086.setTransform(205.8,130.3);

	this.shape_1087 = new cjs.Shape();
	this.shape_1087.graphics.f("#FFFF00").s().p("AgVAsIAAgLQADADAGgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgFAAgFADIAAgLQAGgEAJAAQAMAAAIAQQAIAPAAAQQgBARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1087.setTransform(197.7,130.2);

	this.shape_1088 = new cjs.Shape();
	this.shape_1088.graphics.f("#FFFF00").s().p("AgnApQgPgRABgZQAAgZAQgQQAQgQAYgBQAXAAAOAPQAOAOAAAWQAAARgJAMQgLANgQABQgQAAgKgKQgKgKACgPQABgOAKgIIgMAAIAAgLIAqAAIAAALIgIAAQgHAAgHAFQgEAFAAAHQAAAHAEAGQAHAEAIAAQAKAAAHgJQAGgGAAgLQgCgNgGgIQgKgMgVAAQgQAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAJABQATAAAKgQIAOAAQgOAeglAAQgYgBgQgSg");
	this.shape_1088.setTransform(190,131.5);

	this.shape_1089 = new cjs.Shape();
	this.shape_1089.graphics.f("#FFFF00").s().p("AgiA1QgPgJgBgRQgCgRAPgIQgEAAgDgDQgCgDAAgEIABgEQAFgNAYABQAOACAGAKQAHAIgBANIgfAAQgPAAAAAMQAAAJAMAFQAJADAPAAQAPAAAKgIQALgJABgMQABgSgJgKQgHgJgNAAIgkAAQgSAAAAgLQAAgVAtAAQARAAANAHQAQAIAAANIgMAAQAAgIgMgFQgLgDgMAAQgXAAgBAFQgBAFAIAAIAeAAQAMAAAKAIQARAOAAAdQAAAVgQAOQgQANgVAAQgTAAgNgIgAgZgMQADACAAADQAAAEgDADIAXAAQgCgHgEgCQgFgEgHAAIgFABg");
	this.shape_1089.setTransform(179,128.9);

	this.shape_1090 = new cjs.Shape();
	this.shape_1090.graphics.f("#FFFF00").s().p("AARBBQgRgBgHgKQgGALgNAAQgPAAgHgPQgGgMAAgTQAAgSALgMQAMgNASAAQAMAAAIAHQALAIAAALQAAAHgCAGQgCAGgDABQgIAEAAAGQAAALAQgBQALgBAGgLQAFgKAAgOQAAgPgKgLQgHgIgQAAIg9AAIAAgKIAOAAQgFgCAAgHIABgDQAFgIALgDQALgDAOAAQAhAAALAQQAEAGAAAHIAAAEIgJAAQAKAIAEALQAEAKAAAOQAAAVgJAPQgLARgRAAIgBAAgAghAoQgCACAAADQAAAFAEACIAFACQAEAAADgEQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgEgDgDIgFgBQgFAAgCADgAgoAMQgCAFAAAGQAAAGACAFQADgIAMAAQALgBAFAIQAAgHAEgFIAFgEQACgCABgCIABgEQAAgGgGgEQgGgDgIAAQgRAAgHAQgAgNgzQgNACAAAGQAAAFAIAAIAjAAQAGAAALAFIAAgCQAAgHgLgFQgKgEgLAAIgPAAg");
	this.shape_1090.setTransform(162.2,128.5);

	this.shape_1091 = new cjs.Shape();
	this.shape_1091.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAIgFQAGgFAKAAQAOAAAKAJQALAHAAAOQAAASgNAMQgMALgRAAQgSAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgCACg");
	this.shape_1091.setTransform(151.9,130.3);

	this.shape_1092 = new cjs.Shape();
	this.shape_1092.graphics.f("#FFFF00").s().p("AAEA8QgEgCgCgFQgGAJgRAAQgTAAgIgWQgFgNABgVQABgOAIgKQAJgMANABQAGAAAEADQAEAEgBAGQAAACgDAEQgCAEAAADQABAJANABQAEAAAEgCQAFgDACgCIgHAAQgFAAgFgDQgFgDAAgFQgBgIAFgFQAFgGAHAAQATABAAAWQAAAKgHAHQgIAHgKABQgKABgHgEQgIgEgDgIIgBgFQAAgGAEgEQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAgBAAQgGAAgFAIQgFAJAAAIQgBAMAGAJQAGALAKAAQAPAAAAgRIAKAAQAAAJAEAEQAEAEAIAAQANAAAHgNQAFgKAAgPQAAgTgKgOQgNgRgVAAQgiAAgGAWIgMAAQADgOAOgJQAOgKAWAAQAdABAPAWQAMARAAAVQAAAagIAQQgKAUgVAAQgJAAgFgCgAAAgOQAAAGAFAAQADAAACgCQABAAAAgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgGAAAAAHg");
	this.shape_1092.setTransform(141.3,128.8);

	this.shape_1093 = new cjs.Shape();
	this.shape_1093.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1093.setTransform(126.7,135.9);

	this.shape_1094 = new cjs.Shape();
	this.shape_1094.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1094.setTransform(123.2,130.2);

	this.shape_1095 = new cjs.Shape();
	this.shape_1095.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1095.setTransform(111.1,129);

	this.shape_1096 = new cjs.Shape();
	this.shape_1096.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAKAJQAHAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_1096.setTransform(101,130.3);

	this.shape_1097 = new cjs.Shape();
	this.shape_1097.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgNAAgKADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1097.setTransform(90.5,124.3);

	this.shape_1098 = new cjs.Shape();
	this.shape_1098.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_1098.setTransform(90.3,130.2);

	this.shape_1099 = new cjs.Shape();
	this.shape_1099.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgMAAgLADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQAKgDASgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1099.setTransform(78.7,124.3);

	this.shape_1100 = new cjs.Shape();
	this.shape_1100.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1100.setTransform(78.1,130.2);

	this.shape_1101 = new cjs.Shape();
	this.shape_1101.graphics.f("#FFFF00").s().p("AgHALQgFgFAAgGQAAgEACgDQAEgFAGgBQAEAAAEADQAFAFAAAFQAAAEgDAEQgEAFgGAAQgEAAgDgCg");
	this.shape_1101.setTransform(63.6,133.8);

	this.shape_1102 = new cjs.Shape();
	this.shape_1102.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgMAAgLADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEACgCQAFgLAOgFQAKgDASgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1102.setTransform(54.8,124.3);

	this.shape_1103 = new cjs.Shape();
	this.shape_1103.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1103.setTransform(54.8,130.2);

	this.shape_1104 = new cjs.Shape();
	this.shape_1104.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1104.setTransform(46.1,135.9);

	this.shape_1105 = new cjs.Shape();
	this.shape_1105.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1105.setTransform(42.4,130.2);

	this.shape_1106 = new cjs.Shape();
	this.shape_1106.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_1106.setTransform(30.2,134.8);

	this.shape_1107 = new cjs.Shape();
	this.shape_1107.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1107.setTransform(30,130.2);

	this.shape_1108 = new cjs.Shape();
	this.shape_1108.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1108.setTransform(11.9,130.2);

	this.shape_1109 = new cjs.Shape();
	this.shape_1109.graphics.f("#FFFF00").s().p("AgUAvQgSAAgLgOQgJgNAAgSQAAgfAVgNQAIgEALAAIAGAAIAAALQANgMARABQAUAAAKAOQALAMAAAUQAAATgLAOQgMAOgTAAIgGAAIAAgJQgIAFgEACQgGACgJAAIgEAAgAAHgPQAQAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgVgCgDAPQAEgEAHAAIAFABgAglgaQgHAJAAALQAAANAHAIQAHAJANAAQAKABAHgFQAHgEABgIQAAgFgDgDQgDgEgEAAQgDAAgDADQgCACAAADIABAFIgLAAQgDgLACgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_1109.setTransform(-0.8,130.2);

	this.shape_1110 = new cjs.Shape();
	this.shape_1110.graphics.f("#FFFF00").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJABgFIAUAAQgEAHgGAHQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJAAALQABALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgJgFQgIgEgKAAQgRAAgJALg");
	this.shape_1110.setTransform(-17.7,129);

	this.shape_1111 = new cjs.Shape();
	this.shape_1111.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1111.setTransform(-29.2,130.2);

	this.shape_1112 = new cjs.Shape();
	this.shape_1112.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIAAgDQgCgGgMgCQgKgCgOgBQgNAAgKADQgMACgCAGIAAADQAAAHAHAEIgKAAQgJgCAAgOQAAgEABgCQAGgLAOgFQALgDARgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1112.setTransform(-47.2,124.3);

	this.shape_1113 = new cjs.Shape();
	this.shape_1113.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1113.setTransform(-48,130.2);

	this.shape_1114 = new cjs.Shape();
	this.shape_1114.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1114.setTransform(-57.7,130.4);

	this.shape_1115 = new cjs.Shape();
	this.shape_1115.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_1115.setTransform(-66.7,131.2);

	this.shape_1116 = new cjs.Shape();
	this.shape_1116.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1116.setTransform(-84.2,130.2);

	this.shape_1117 = new cjs.Shape();
	this.shape_1117.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1117.setTransform(-95.8,130.2);

	this.shape_1118 = new cjs.Shape();
	this.shape_1118.graphics.f("#FFFF00").s().p("AgBA/QgVgBgMgMQgMgNAAgTQgBgMAFgKIACgCIAAgbIgJAAIAAgJIAJAAIAAgMIgJAAIAAgJIAZAAQAHAAAFAEQAFAEAAAIQAAAHgGAEQgHAEgKgBIAAAOQgGAGgEAHQAFgIAJgIIAUgUQAGgIADgFIATAAQgDAHgGAGQgFAGgJAGQANgBAKAEQAcALAAAgQAAAVgPAOQgOANgUAAIgCgBgAgYgJQgIAJAAALQAAAMAHAHQALAKAPAAQANAAAJgHQAMgJAAgOQAAgGgCgGQgDgJgJgEQgIgFgLAAQgRAAgJALgAgeg2IAAAMQAKAAAAgGQAAgGgIAAIgCAAg");
	this.shape_1118.setTransform(-106.8,128.6);

	this.shape_1119 = new cjs.Shape();
	this.shape_1119.graphics.f("#FFFF00").s().p("AgCAzQgGALgPAAQgQAAgJgNQgIgNABgUQABgXAOgMQAKgKAWAAIAHAAIAAALIgQAAQgIABgFAEQgLAIAAAPQgBAMAFAIQAEAJAKAAQAIAAAEgFQAEgEAAgIIAAgNIAKAAIAAANQAAARARAAQALAAAGgNQAFgMgBgSQAAgSgJgMQgNgQgVAAQgNAAgKAFQgKAFgEAIIgMAAQADgNAOgIQANgIATAAQAOAAAMAGQAJAEAFAGQARATABAfQAAAYgJAQQgKARgSAAQgPAAgGgLg");
	this.shape_1119.setTransform(-117.5,128.8);

	this.shape_1120 = new cjs.Shape();
	this.shape_1120.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgHgEgKABQgNAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgHAAgHgEQgFgGAAgIQAAgJAHgFQAHgFAJAAQAPAAAKAJQALAHAAAOQAAASgMAMQgNALgQAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgEAAgCACg");
	this.shape_1120.setTransform(-127.9,130.3);

	this.shape_1121 = new cjs.Shape();
	this.shape_1121.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgIAAgGgEQgFgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_1121.setTransform(-137.1,130.3);

	this.shape_1122 = new cjs.Shape();
	this.shape_1122.graphics.f("#FFFF00").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgMALABQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAJgMAAQgMABgIgIQgIgIABgKIACgGIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAIQAAAPALALQAMAKAQAAQAQgBAMgLQAMgNAAgQQAAgUgKgMQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaAAQgNABgMgGgAgPgEQAAAEAFAEQAEAEAGAAQAGAAAGgFQAFgEAAgFIAAgEQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgBgDgBQgFABAAAGg");
	this.shape_1122.setTransform(-153.3,128.9);

	this.shape_1123 = new cjs.Shape();
	this.shape_1123.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_1123.setTransform(-161.1,123.9);

	this.shape_1124 = new cjs.Shape();
	this.shape_1124.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1124.setTransform(-165.6,130.2);

	this.shape_1125 = new cjs.Shape();
	this.shape_1125.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgFgLgFgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_1125.setTransform(-174.5,130.2);

	this.shape_1126 = new cjs.Shape();
	this.shape_1126.graphics.f("#FFFF00").s().p("AAQA5IAAgVQgOACgKgBQgVgBgNgJQgOgLAAgSQAAgJAFgIQAFgHAHgDQgDgCgCgCIgBgGQAAgGAFgEQAJgHAQAAQAaAAAGAPQACgHAFgEQAGgEAGAAIALAAIAAALIgCgBQgDAAgBAEQAAABAEADIAHAHQAFAFAAAGQAAALgKAGQgIAEgLgBIAAAYIAJgDIAIgDIAAALIgIADIgJACIAAAXgAgZgQQgGAHAAAJQAAAUAXAFIALABIANgBIAAgwIgZAAQgKAAgGAHgAAcgcIAAASIACABQAEAAADgDQAEgDAAgEQAAgFgFgHQgFgGAAgDIABgCQgEABAAANgAgVgsQACADAAADQAAADgDABIADAAIACAAIAYAAQgCgHgIgCQgGgCgFAAIgHABg");
	this.shape_1126.setTransform(-182.4,131.2);

	this.shape_1127 = new cjs.Shape();
	this.shape_1127.graphics.f("#FFFF00").s().p("AgkAyQgPgOAAgWQAAgPAGgKQAHgMANAAQAMAAACAKQADgMANAAQAJAAAHAJQAGAIAAAIQAAANgIAIQgIAJgLAAQgJgBgIgFQgIgGAAgKIADgNQABgGgHAAQgEgBgDAIQgDAFAAAHQAAAPAMAKQALAJAPAAQAQgBALgKQALgLAAgQQAAgNgFgJQgGgKgMAAIg+AAIAAgJQAQABAAgCIgDgCQgEgCAAgDQAAgMAOgFQAHgCARAAQANABAJACQAMADAIAKQAGAGAAALIgLAAIAAAAIAAAAIAAAAQAPALAAAcQAAAYgOARQgPAPgXAAQgWAAgOgOgAgMACQAAAFAEAEQAEADAFABQAGAAAEgEQAFgEAAgGQgFAGgIABQgJAAgGgJIAAADgAAAgMQAAAAAAABQgBAAAAABQAAAAAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAAAAAQABAAABAAQAAABABAAIAEgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgBgDQgBgBAAAAQgBAAAAgBQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQAAABAAAAQgBAAAAABgAAlgfQgCgKgLgGQgJgEgOAAIgOABQgHACAAAGQAAAAAAABQABAAAAAAQAAABAAAAQABABAAAAIAEACIAdAAQAJAAAHACIAGAEIAAAAgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAfgjQgHgCgJAAIgdAAIgEgCQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQAAgGAHgCIAOgBQAOAAAJAEQALAGACAKIgGgEg");
	this.shape_1127.setTransform(-199.5,128.5);

	this.shape_1128 = new cjs.Shape();
	this.shape_1128.graphics.f("#FFFF00").s().p("AgeAhQgNgNAAgTQAAgVANgNQAOgNATAAQAWAAAKAJQAGAGABAJIgLAAQgBgGgIgEQgIgEgKABQgMAAgKAJQgLAIAAANQAAANAKAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgMgEQAEAEAAAKQAAAFgFAEQgGAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAIgFQAGgFAKAAQAOAAAKAJQALAHAAAOQAAASgNAMQgMALgRAAQgSAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQACgCAAgDQAAgDgCgCQgDgCgDgBQgDAAgCACg");
	this.shape_1128.setTransform(-209.5,130.3);

	this.shape_1129 = new cjs.Shape();
	this.shape_1129.graphics.f("#FFFF00").s().p("AgmAzQgOgNABgYQAAgPAMgKQAMgLAQAAIAMAAIAAALIgJAAQgNAAgIAGQgIAFAAAKQgBAPANAJQALAIAPgBQAQAAAKgJQAMgKAAgQQAAgSgKgJQgJgIgOAAIgiAAQgHAAgFgDQgEgEAAgFQAAgMANgGQAKgEAVAAQAZAAAMAIQAKAGAAAJQAAAEgEACQgDADgEABQARAMAAAZQAAAZgOAQQgPAQgYAAQgYAAgOgNgAAYguIgCAEQAAAGAGAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgEACgAgZgvIgBAEQAAAEAIAAIAdAAIgBgGQAAgDADgDIgNgBQgUAAgFAFg");
	this.shape_1129.setTransform(-219.6,128.5);

	this.shape_1130 = new cjs.Shape();
	this.shape_1130.graphics.f("#FFFF00").s().p("AAeAVQAIgEAAgHIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLADQgMACgBAGIgBADQAAAHAHAEIgKAAQgJgCAAgOQAAgEACgCQAFgLAOgFQALgDARgBQAoABAIATQACADAAADQAAANgJADg");
	this.shape_1130.setTransform(-230.9,124.3);

	this.shape_1131 = new cjs.Shape();
	this.shape_1131.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_1131.setTransform(-231.1,130.2);

	this.shape_1132 = new cjs.Shape();
	this.shape_1132.graphics.f("#FFFF00").s().p("AgqAVQgGgDAAgMIABgGQAGgUAqAAQAUAAALADQAQAFABAMQACAIgGAGQgGAFgJgBQgHAAgFgDQgGgFAAgHQAAgDACgDIgNgBQggAAgFAKIgBAEQAAAHAHAEgAAYABQAAABAAAAQAAABABABQAAAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAAAAAQABABABAAQAGAAAAgHQAAAAAAgBQAAAAAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBgBQgBAAAAAAQgBAAAAAAQgHAAAAAFg");
	this.shape_1132.setTransform(-248.4,124.3);

	this.shape_1133 = new cjs.Shape();
	this.shape_1133.graphics.f("#FFFF00").s().p("AgKAvQgTAAgKgOQgKgNAAgSQAAgVAIgKQgFABgFAKQgEAKAAANQAAANAHANQAGAKAIAFIgPAAQgFgDgFgKQgIgNAAgQQAAgLAEgLQAEgKAFgGQALgLANAAIAAAGIADgDQAHgEAMAAIAGAAIAAALQAMgMARABQAUAAALAOQAKAMAAAUQABATgMAOQgLAOgUAAIgGAAIAAgJQgHAFgGACQgGACgIAAIgDAAgAARgPQAPAFAAATQAAAPgKAMQAKABAHgEQAMgJgBgUQAAgLgIgJQgHgJgLAAQgWgCgCAPQAEgEAHAAIAGABgAgcgaQgGAJAAALQAAANAHAIQAHAJAMAAQAJABAIgFQAIgEAAgIQAAgFgCgDQgDgEgFAAQgEAAgCADQgBACAAADIAAAFIgLAAQgCgLABgLQABgKAIgLIgHgBQgLAAgHAJg");
	this.shape_1133.setTransform(-249.7,130.2);

	this.shape_1134 = new cjs.Shape();
	this.shape_1134.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAEgFAGQgHAJgCAEQgFAGAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1134.setTransform(-259.8,130.4);

	this.shape_1135 = new cjs.Shape();
	this.shape_1135.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1135.setTransform(-269.2,130.2);

	this.shape_1136 = new cjs.Shape();
	this.shape_1136.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_1136.setTransform(235.4,101.4);

	this.shape_1137 = new cjs.Shape();
	this.shape_1137.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1137.setTransform(231.3,107.7);

	this.shape_1138 = new cjs.Shape();
	this.shape_1138.graphics.f("#FFFF00").s().p("AAfAVQAHgEAAgHIgBgDQgBgGgMgCQgKgCgOgBQgMAAgLADQgMACgBAGIgBADQAAAHAHAEIgKAAQgJgCAAgOQAAgEACgCQAFgLAOgFQAKgDASgBQAoABAJATQABADAAADQAAANgJADg");
	this.shape_1138.setTransform(219.3,101.8);

	this.shape_1139 = new cjs.Shape();
	this.shape_1139.graphics.f("#FFFF00").s().p("AgjAqQgQgNAAgVQAAgTAOgLQgBgKAEgHQAIgQARACQAIAAAFAGQAGgHAMABQALAAAGAHQAGAIAAASIgtAAIgTABQAAAMAJAIQAJAJALgBQAJgBAHgFQAHgEABgIIALAAQgCANgLAIQgLAIgOABQgbAAgLgUIgBAHQAAAOALAJQALAJANAAQAVAAALgNQAGgIAAgJIALAAQABAMgIALQgPAVgdAAQgVAAgPgMgAgQgnQgDAEAAAGIAJgBIAJAAQAAgMgIAAQgFAAgCADgAAKgeIATAAQgBgMgIAAQgKAAAAAMg");
	this.shape_1139.setTransform(219.7,108.5);

	this.shape_1140 = new cjs.Shape();
	this.shape_1140.graphics.f("#FFFF00").s().p("AgQAQQgHgFgCgJIgBgEQAAgLAHgIIAAAMIAFAAQAAAHAEAEQAFAFAGgBQAHABAEgGQAFgEgBgIIALAAIAAAGQAAAKgIAIQgHAJgMAAQgJAAgHgGg");
	this.shape_1140.setTransform(211.4,113.4);

	this.shape_1141 = new cjs.Shape();
	this.shape_1141.graphics.f("#FFFF00").s().p("Ag2AbQgCgJACgHIgJAAIAAgJIAHgGIAIgJQgFAAgEgEQgDgEAAgFQAAgJAHgFQAGgGAJAAQAOgBAHAKQANgJATAAIAHAAQATABAMAPQAMAOgBASQgCATgMANQgMANgSAAIgHAAIAAgKQgMAKgSAAIgCAAQgdAAgGgUgAgFgaQAFAAAGADQAGAEADAEQAJAJAAAMQABASgKAMIAEAAQAIAAAGgEQAMgIABgPQABgLgFgJQgFgJgJgEQgIgEgNAAQgHAAgFACgAgggMIgRAPIAHAAQgDAHACAHQAEAMAUgBQALAAAHgHQAHgGABgLQAAgIgGgGQgGgHgJAAQgKAAgIAFgAgngjQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgFIAEgDQgCgEgEgBIgDAAIgDAAg");
	this.shape_1141.setTransform(207.7,107.7);

	this.shape_1142 = new cjs.Shape();
	this.shape_1142.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_1142.setTransform(193.2,101.4);

	this.shape_1143 = new cjs.Shape();
	this.shape_1143.graphics.f("#FFFF00").s().p("AgmA0QgPgJAAgSQAAgQAKgHIgKAAIAAgJIAOAAQgDgBAAgFQAAgGAEgDQAJgHAOACQAQABAGAKQAEAGABAMIgaAAQgWAAgCAPQgBALAMAGQAMAFAOgBQAPAAAKgKQALgIADgPIABgHQAAgMgIgMQgHgLgKgFIgFAFIgGAEQgHAEgIAAQgMAAgEgGQgDgEACgHQABgDgGAAIgEABIAAgKQAJgCAJAAQANAAAIACQAVAEAPAPQAQARAAAbQABAXgQARQgQAQgYAAQgWAAgOgJgAgWgQQABAAAAABQABAAAAAAQAAABAAAAQAAABAAABQABACgDACIAVAAQgBgFgJgDIgFgBIgGABgAgSgsQABAEAHAAQAJAAADgGQgHgDgNAAIAAAFg");
	this.shape_1143.setTransform(189.8,106.4);

	this.shape_1144 = new cjs.Shape();
	this.shape_1144.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_1144.setTransform(172.9,106.4);

	this.shape_1145 = new cjs.Shape();
	this.shape_1145.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1145.setTransform(161.7,106.5);

	this.shape_1146 = new cjs.Shape();
	this.shape_1146.graphics.f("#FFFF00").s().p("AguAgIAAgRIBSAAIAAgdIAGgIIAFgJIAAA/g");
	this.shape_1146.setTransform(150.4,112.3);

	this.shape_1147 = new cjs.Shape();
	this.shape_1147.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1147.setTransform(149.9,107.7);

	this.shape_1148 = new cjs.Shape();
	this.shape_1148.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1148.setTransform(138.1,106.5);

	this.shape_1149 = new cjs.Shape();
	this.shape_1149.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgIAAgFgEQgGgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_1149.setTransform(128,107.8);

	this.shape_1150 = new cjs.Shape();
	this.shape_1150.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPAAAQQgBARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1150.setTransform(115.2,107.7);

	this.shape_1151 = new cjs.Shape();
	this.shape_1151.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1151.setTransform(107.5,106.5);

	this.shape_1152 = new cjs.Shape();
	this.shape_1152.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1152.setTransform(95.8,107.7);

	this.shape_1153 = new cjs.Shape();
	this.shape_1153.graphics.f("#FFFF00").s().p("AgDA8QgUgBgNgNQgLgMgBgTQAAgNAFgJQAEgJALgKIAVgTQAHgJACgFIATAAQgEAHgFAHQgGAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgIAJAAALQgBALAIAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgCgFQgEgJgIgFQgJgEgKAAQgRAAgJALg");
	this.shape_1153.setTransform(84.6,106.5);

	this.shape_1154 = new cjs.Shape();
	this.shape_1154.graphics.f("#FFFF00").s().p("AgQAoQgGAHgPAAQgIAAgHgGQgGgFAAgJQAAgJAEgGIgHAAIAAgHIAGgHIAIgJIgCAAQgFAAgDgDQgDgEAAgFQAAgJAGgGQAHgHAIgBQAQgBAHALQAPgLATABQATACAMANQANANAAASQABAfgXANQgFADgKAAIgJgBIAAgHQgHAIgJAAIgBAAQgJAAgGgHgAgHgcQAOABAJALQALAMAAAPQAAAPgIAJIAFABQAHAAAHgGQAGgGACgJIAAgGQAAgSgMgLQgKgJgSAAQgGAAgHABgAgegMQgJAHgHAIIAHAAQgFAGABAIQAAADAEACQADADAEAAQAHAAADgIQABgEAAgNIAKAAQAAAOABAEQACAHAHgBQAFAAAEgGQADgFAAgHQAAgIgFgHQgFgHgGgBIgGAAQgMAAgHAFgAgrggQAEABABAEQABAEgBAEIAFgFIAGgGQgBgEgGgBQgHAAgCADg");
	this.shape_1154.setTransform(73.1,107.7);

	this.shape_1155 = new cjs.Shape();
	this.shape_1155.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgCgLQgFgLgFgBQgFAAgEADIAAgLQAEgEAKAAQAMAAAJAQQAGAPAAAQQABARgIAOQgJAPgMAAQgIAAgFgDg");
	this.shape_1155.setTransform(58.6,107.7);

	this.shape_1156 = new cjs.Shape();
	this.shape_1156.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1156.setTransform(50.4,107.7);

	this.shape_1157 = new cjs.Shape();
	this.shape_1157.graphics.f("#FFFF00").s().p("AACA6QgIgFAAgIQgBgIAGgDQgUABgOgLQgNgLABgRQABgTARgKQgDAAgCgDQgCgDAAgEIABgEQADgIAJgCQAGgCANAAQAVAAAJALQAIAJAAAMIgiAAQgJAAgHAHQgHAGgBAJQAAALAJAIQAJAIALAAQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIgBgEIALAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgHAAgHgCgAgOgvIACABIACADQAAAFgDABIAFgBIAbAAQgDgHgJgCQgEgCgHAAIgKACg");
	this.shape_1157.setTransform(40.1,109.1);

	this.shape_1158 = new cjs.Shape();
	this.shape_1158.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_1158.setTransform(33,101.4);

	this.shape_1159 = new cjs.Shape();
	this.shape_1159.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgDgLQgDgLgGgBQgGAAgDADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_1159.setTransform(32.7,107.7);

	this.shape_1160 = new cjs.Shape();
	this.shape_1160.graphics.f("#FFFF00").s().p("AgiAzQgQgLABgRIAAgFQAAgFAEgFQAEgGAEgCIgTAAIAAgJIAQAAQgDgEABgGQABgHAKgEQAHgDAKAAQANAAAGALQAJAJAAANIgcAAQgHAAgGAEQgHAEABAHQAAALALAHQAKAEAPAAQASAAALgMQAKgLABgQQAAgUgKgNQgLgNgWAAQgXgCgNATIgMAAQAGgOANgHQAOgHAPAAQAZAAAQARQAQARAAAZQAAAZgPARQgPASgYAAQgXAAgOgJgAgWgVQACABABADQABAEgCAEIAUAAQgBgGgDgDQgFgFgGABIgHABg");
	this.shape_1160.setTransform(24.8,106.5);

	this.shape_1161 = new cjs.Shape();
	this.shape_1161.graphics.f("#FFFF00").s().p("AgdAhQgOgNAAgTQAAgVAOgNQANgNATAAQAWAAAJAJQAIAGAAAJIgKAAQgBgGgJgEQgIgEgJABQgNAAgKAJQgKAIAAANQAAANAJAIQAJAIANAAQAKAAAIgFQAJgHAAgJQAAgNgNgEQAFAEAAAKQAAAFgGAEQgFAFgHgBQgIAAgGgEQgFgGAAgIQAAgJAHgFQAHgFAJAAQAPAAALAJQAKAHAAAOQAAASgMAMQgMALgSAAQgSAAgNgOgAgBgLQgDADAAADQAAAEADABQABACADAAQADAAADgCQACgCAAgDQAAgDgCgCQgDgCgCgBQgEAAgBACg");
	this.shape_1161.setTransform(14.6,107.8);

	this.shape_1162 = new cjs.Shape();
	this.shape_1162.graphics.f("#FFFF00").s().p("AgnAuQgOgPABgXQAAgQAMgLQALgLARAAIAMAAIAAALIgKAAQgNAAgHAHQgIAHAAALQgBAPANAJQALAIAPgBQARAAAMgMQAKgMAAgQQAAgSgLgNQgLgOgRAAQgMgBgKAGQgLAFgEAJIgLAAQAFgOANgIQANgIARAAQAYAAAPASQAPARAAAYQAAAagPARQgQARgYAAQgXAAgPgOg");
	this.shape_1162.setTransform(-1.4,106.4);

	this.shape_1163 = new cjs.Shape();
	this.shape_1163.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1163.setTransform(-12.7,107.7);

	this.shape_1164 = new cjs.Shape();
	this.shape_1164.graphics.f("#FFFF00").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAKgEQAHgDAKAAQAPAAAGAJQAEgIANgBIAIAAIAFgFIAEgFQAGgKAAgKIAQAAIgDALQgCAGgEAFIgJAKQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACg");
	this.shape_1164.setTransform(-24,106.3);

	this.shape_1165 = new cjs.Shape();
	this.shape_1165.graphics.f("#FFFF00").s().p("AgUAsIAAgLQACADAHgBQAFAAAEgKQAEgLgBgNQAAgMgDgLQgDgLgGgBQgGAAgDADIAAgLQAEgEAKAAQAMAAAJAQQAGAPABAQQAAARgIAOQgIAPgNAAQgIAAgFgDg");
	this.shape_1165.setTransform(-32.1,107.7);

	this.shape_1166 = new cjs.Shape();
	this.shape_1166.graphics.f("#FFFF00").s().p("AgaA3QgLgGgGgJQgKgOAAgWQAAgNAIgMQAIgLALAAQAKABADAHQAGgJALAAQAKAAAHAIQAHAIABAKQABALgKAKQgJAJgMAAQgMABgIgIQgIgIABgKIACgGIADgHQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQgFABgDAGQgDAHAAAIQAAAPALALQAMAKAQAAQAQgBAMgLQAMgNAAgQQAAgUgKgMQgLgOgUAAQgZAAgKASIgMAAQAGgOANgHQAMgIAQAAQAZABAOAOQAQAQABAaQABAbgPASQgPASgaAAQgNABgMgGgAgPgEQAAAEAFAEQAEAEAGAAQAGAAAGgFQAFgEAAgFIAAgEQgBAEgEADQgFADgFAAQgMAAgDgMIgCAIgAgCgRQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQABACADAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBgBgBQgCgBgDgBQgFABAAAGg");
	this.shape_1166.setTransform(-40,106.4);

	this.shape_1167 = new cjs.Shape();
	this.shape_1167.graphics.f("#FFFF00").s().p("AACAkQgGALgTAAQgNAAgJgKQgJgKAAgOQAAgPAKgIIgOAAIAAgLIAOAAQgCgBAAgGIAAgEQAFgOAVAAQAQAAAGALQAHgLAQAAQAJAAAHAEQAHAFABAIQABAHgDAFIgDAAQAPALgBARQAAAPgJAKQgJALgOAAQgQAAgIgLgAAIADIAAAKQAAAGAGADQAFACAGAAQAIAAAGgFQAGgGgBgIQAAgGgFgFQgFgEgIAAIgvAAQgIAAgFAFQgGAFAAAGQAAAIAFAFQAGAFAIAAQAHAAAFgCQAGgDAAgGIAAgKgAAWgjQgGAAgEAEQgDAFgBAFIAZAAQACgCAAgEQgBgIgKAAIgCAAgAgbghQADACABAEQAAADgCADIAVAAQgBgHgGgEQgEgDgFAAQgEAAgDACg");
	this.shape_1167.setTransform(-51.7,107.7);

	this.shape_1168 = new cjs.Shape();
	this.shape_1168.graphics.f("#FFFF00").s().p("AACBFQgIgEAAgJQgBgHAGgEQgUABgOgLQgNgLABgRQABgTARgJQgDgBgCgDQgCgDAAgDIABgEIABgDQgHgDAAgKQAAgEACgEQAHgMAfAAQAaAAAHALQACAFAAAEQAAAKgJADQAIAJABAOIgiAAQgJAAgHAGQgHAHgBAIQAAALAJAJQAJAHALABQATABAOgHIAAALIgNAEQgMADAAAFQAAAEADACQADACAFAAQAFAAAEgDQAEgDAAgFIgBgEIALAAIABAFQAAAKgJAGQgHAGgLABIgFAAQgHAAgHgDgAgOgkQADACAAADQAAAEgCABIAFAAIAbAAQgDgHgJgDQgEgBgHAAIgKABgAAKgyQAAAAAAABQAAAAAAABQABAAAAABQAAAAABABQAAAAABABQAAAAABAAQAAABABAAQABAAAAAAQAFAAAAgFQAAAAAAgBQAAAAAAgBQAAgBgBAAQAAgBgBAAIgFgCQgEAAAAAFgAgJg5QgTACAAAHIACADIAEgBQAGgCAMAAIAEAAQgBgCABgDIAAgEIgCAAIgHAAg");
	this.shape_1168.setTransform(-67.4,107.9);

	this.shape_1169 = new cjs.Shape();
	this.shape_1169.graphics.f("#FFFF00").s().p("AgnApQgPgRAAgZQABgZAPgQQAQgQAagBQAVAAAPAPQAOAOAAAWQAAARgJAMQgKANgRABQgQAAgKgKQgJgKABgPQABgOALgIIgNAAIAAgLIAqAAIAAALIgIAAQgIAAgFAFQgFAFAAAHQAAAHAFAGQAFAEAJAAQALAAAGgJQAGgGAAgLQgBgNgHgJQgKgLgUAAQgRAAgKAOQgJANAAATQAAARAJAMQAJAOAPADIAIABQAUAAALgQIANAAQgNAegmAAQgYgBgQgSg");
	this.shape_1169.setTransform(-78,109);

	this.shape_1170 = new cjs.Shape();
	this.shape_1170.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1170.setTransform(-89.4,106.5);

	this.shape_1171 = new cjs.Shape();
	this.shape_1171.graphics.f("#FFFF00").s().p("AgkAyQgPgOAAgWQAAgPAGgKQAHgMANAAQAMAAACAKQADgMANAAQAJAAAHAJQAGAIAAAIQAAANgIAIQgIAJgLAAQgJgBgIgFQgIgGAAgKIADgNQABgGgHAAQgEgBgDAIQgDAFAAAHQAAAPAMAKQALAJAPAAQAQgBALgKQALgLAAgQQAAgNgFgJQgGgKgMAAIg+AAIAAgJQAQABAAgCIgDgCQgEgCAAgDQAAgMAOgFQAHgCARAAQANABAJACQAMADAIAKQAGAGAAALIgLAAIAAAAQgCgKgLgGQgJgEgOAAIgOABQgHACAAAGQAAAAAAABQABAAAAAAQAAABAAAAQABABAAAAIAEACIAdAAQAJAAAHACIAGAEIAAAAIAAAAQAPALAAAcQAAAYgOARQgPAPgXAAQgWAAgOgOgAgMACQAAAFAEAEQAEADAFABQAGAAAEgEQAFgEAAgGQgFAGgIABQgJAAgGgJIAAADgAAAgMQAAAAAAABQgBAAAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAAAABAAQAAABABAAIAEgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgBgDQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABgAAlgfgAAlgfIAAAAIAAAAgAAlgfIAAAAgAAfgjQgHgCgJAAIgdAAIgEgCQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQAAgGAHgCIAOgBQAOAAAJAEQALAGACAKIgGgEg");
	this.shape_1171.setTransform(-106.1,106);

	this.shape_1172 = new cjs.Shape();
	this.shape_1172.graphics.f("#FFFF00").s().p("AgmAzQgOgNABgYQAAgPAMgKQAMgLAQAAIAMAAIAAALIgJAAQgNAAgIAGQgIAFAAAKQgBAPANAJQALAIAPgBQAQAAAKgJQAMgKAAgQQAAgSgKgJQgJgIgOAAIgiAAQgHAAgFgDQgEgEAAgFQAAgMANgGQAKgEAVAAQAZAAAMAIQAKAGAAAJQAAAEgEACQgDADgEABQARAMAAAZQAAAZgOAQQgPAQgYAAQgYAAgOgNgAAYguIgCAEQAAAGAGAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgEACgAgZgvIgBAEQAAAEAIAAIAdAAIgBgGQAAgDADgDIgNgBQgUAAgFAFg");
	this.shape_1172.setTransform(-117,106);

	this.shape_1173 = new cjs.Shape();
	this.shape_1173.graphics.f("#FFFF00").s().p("AgUAnQgIgIAAgNQAAgJADgIQADgEAGgIQAHgIATgOIgkAAIAAgMIA1AAIAAALIgMAJQgGAFgFAFQgHAJgCADQgFAHAAAIQABAFAEAEQADACAEAAQAKAAADgJIABgEQAAgEgCgDIAKAAQAGAHgBAKQgBALgIAHQgJAIgLgBQgMAAgIgGg");
	this.shape_1173.setTransform(-125.6,107.9);

	this.shape_1174 = new cjs.Shape();
	this.shape_1174.graphics.f("#FFFF00").s().p("AgmAyQgPgMAAgVQABgRAOgFQgHgDAAgIQAAgJAKgFQAIgEAKABQANAAAHALQAIAJAAANIggAAQgHAAgFAEQgFAEAAAHQABAQAWAFQAGACAJgBQATAAALgMQAKgLABgQQAAgUgLgNQgLgNgVAAQgYgCgNATIgMAAQAGgOAOgHQANgHARAAQAYAAAPARQAQARAAAZQAAAZgOARQgPASgZAAQgXAAgPgKgAgZgVQACABABADQACAEgCAEIAUAAQgBgGgEgDQgFgFgGABQgEAAgDABg");
	this.shape_1174.setTransform(-134.4,106.5);

	this.shape_1175 = new cjs.Shape();
	this.shape_1175.graphics.f("#FFFF00").s().p("AALA6QgJgFAAgIQgBgIAGgDQgTABgNgLQgOgLACgRQABgTAQgKQgGgBABgJQgHADgGAKQgGALAAALQAAANAIAMQAGAKAHAGIgOAAQgGgEgFgJQgHgNAAgPQgBgMAFgLQAFgKAGgGQAKgJALgCIAAADQAHgEAQAAQAWAAAKALQAHAJAAAMIghAAQgKAAgHAHQgHAGAAAJQgBALAJAIQAIAIANAAQATABANgHIAAALIgNAEQgLADgBAFQAAAEADACQADACAFAAQAFAAAEgDQAEgCAAgFIAAgEIAKAAIABAEQAAAKgJAHQgHAGgLAAIgFAAQgIAAgGgCgAgFgvQADABAAAEQABADgDACIAEgBIAdAAQgEgHgJgCQgEgCgHAAIgKACg");
	this.shape_1175.setTransform(-145,109.1);

	this.shape_1176 = new cjs.Shape();
	this.shape_1176.graphics.f("#FFFF00").s().p("AARBBQgRgBgHgKQgGALgNAAQgPAAgHgPQgGgMAAgTQAAgSALgMQAMgNASAAQAMAAAIAHQALAIAAALQAAAHgCAGQgCAGgDABQgIAEAAAGQAAALAQgBQALgBAGgLQAFgKAAgOQAAgOgKgLQgJgJgOAAIgsAAQgGAAgEgDQgDgEAAgFQAAgNASgGQANgFAPAAQAjAAAKASQADAFAAAFIgBAEIgJAAQAUARAAAbQAAAVgJAPQgLARgRAAIgBAAgAghAoQgCACAAADQAAAFAEACIAFACQAEAAADgEQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAgEgDgDIgFgBQgFAAgCADgAgoAMQgCAFAAAGQAAAGACAFQADgIAMAAQALgBAFAIQAAgHAEgFIAFgEQACgCABgCIABgEQAAgGgGgEQgGgDgIAAQgRAAgHAQgAgdgqQAAAAAAABQAAAAAAAAQAAABAAAAQABAAAAABIAFABIAoAAQAEAAAGACIAFACIAAgDQAAgGgKgEQgKgEgKAAQgcAAgDAJg");
	this.shape_1176.setTransform(-156.6,106);

	this.shape_1177 = new cjs.Shape();
	this.shape_1177.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQAEgLAAgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPAAAQQgBARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1177.setTransform(-170.5,107.7);

	this.shape_1178 = new cjs.Shape();
	this.shape_1178.graphics.f("#FFFF00").s().p("AANAkQAKAAAHgEQAJgFACgIQAEgKgBgJQgBgKgHgHQgHgJgLABQgRABgEAPQADgCADAAQAGAAADACQAKAHAAAQQAAAPgNAJQgMAJgRAAQgRAAgMgLQgKgKgBgQQABgRALgIQgCgBgDgEQgCgDAAgFQAAgIAHgFQAHgFAJAAQAIAAAGAFQAGAFACAIQAKgRAVgBQASAAAKAPQAMANAAATQAAATgOAOQgNANgVAAgAgpgFQgEAEAAAGQAAAKAJAFQAHAFAMAAQAKAAAHgEQAHgFABgHQAAgEgBgDQgDgCgDAAQgBAAgDABQgCACAAADIgKAAIAAgIIAAgIIgNAAQgHAAgGAFgAglghQACABABAEQACAEgCAEIAMAAQAAgHgEgEQgEgDgEAAIgDABg");
	this.shape_1178.setTransform(-178.8,107.7);

	this.shape_1179 = new cjs.Shape();
	this.shape_1179.graphics.f("#FFFF00").s().p("AgLAWIAAgTIgJAAIAAgIIAJAAIAAgLIgJAAIAAgJIAXAAQAIAAAFAEQAFAEAAAHQAAAHgGAEQgHADgJgBIAAAXgAgBgQIAAALQAIAAAAgFQAAgGgHAAIgBAAg");
	this.shape_1179.setTransform(-193,101.4);

	this.shape_1180 = new cjs.Shape();
	this.shape_1180.graphics.f("#FFFF00").s().p("AAKAuIAAgIQgHAGgGACQgGABgLAAQgQAAgKgLQgLgKgBgPQAAgOAEgJIALAAQgBADACACQABABAEAAQAFAAAEgCQAJgGACgGIAAgEQAAgIgFgCQgEgCgEACQAHACAAAIQAAAFgEADQgDADgFAAIgEgBQgJgCAAgLIABgGQAFgNARAAQAHAAAFADQAFADACAGQAJgNATABQAUABALATQAHANgBAOQAAAfgXALQgHAEgIAAIgLgBgAgEgQQAJAAAGAFQALAIAAAOQAAAPgJAJIAHABQAIAAAFgDQALgJAAgSQAAgLgIgKQgIgJgLAAIgCAAQgOAAgFAIgAgtAHQAAAHADAFQAGAIAOAAIAHgBQAJgBAGgGQAEgGAAgIQAAgEgCgDQgCgEgIgBQgDAAgGAEIgKAFQgFACgFAAQgFAAgDgCIAAAFg");
	this.shape_1180.setTransform(-197.1,107.7);

	this.shape_1181 = new cjs.Shape();
	this.shape_1181.graphics.f("#FFFF00").s().p("AAAAlQgHAKgNAAQgSAAgJgNQgIgMAAgVQABgWAPgOQAKgJAOgCQAGAAAEABIAAALQgGgCgJACQgKADgFAJQgGAJAAAKQAAAcATAAQAIAAAEgEQAFgEAAgHIAAgKIALAAIAAAKQAAAHAFAEQAEAEAHAAQAJAAAGgGQAFgFAAgKQAAgHgGgFQgFgGgJAAIgaAAQgBgRAGgHQAHgIAOAAQAZABAAATIgBAFIgJAAQAJAEAFAIQAFAIAAAJQAAARgIALQgJALgQAAQgQAAgHgKgAAIgYIAQAAIAHABIAAgDQAAgJgLAAQgLAAgBALg");
	this.shape_1181.setTransform(-209,107.7);

	this.shape_1182 = new cjs.Shape();
	this.shape_1182.graphics.f("#FFFF00").s().p("AgmAyQgRgNABgTQABgPAMgHQgIgCAAgJQAAgJAKgEQAHgDAKAAQAPAAAGAJQAEgIANgBIAIAAIAFgFIAEgFQAGgKAAgKIAQAAIgDALQgCAGgEAFIgJAKQAIAEABAJQABAFgCAFIgJAAQAEABAFAEQAIAHAAAOQAAASgRAMQgQAMgUAAQgWAAgQgMgAgjAEQgFAFAAAHQABALANAGQALAGAPAAQAPAAAKgFQANgHABgLQABgHgFgFQgFgEgKAAIgrAAQgHAAgFAEgAANgUQgFABgCACQgDADAAAEIASAAIAKABQABgCgBgEQgDgGgLAAIgEABgAgdgTQAEABAAADQABADgCACIAUAAQgCgHgGgCIgIgCIgHACg");
	this.shape_1182.setTransform(-220.3,106.3);

	this.shape_1183 = new cjs.Shape();
	this.shape_1183.graphics.f("#FFFF00").s().p("AgVAsIAAgLQAEADAFgBQAGAAAEgKQADgLABgNQAAgMgEgLQgEgLgFgBQgGAAgEADIAAgLQAGgEAJAAQAMAAAIAQQAIAPgBAQQAAARgHAOQgJAPgMAAQgHAAgHgDg");
	this.shape_1183.setTransform(-228.4,107.7);

	this.shape_1184 = new cjs.Shape();
	this.shape_1184.graphics.f("#FFFF00").s().p("AgDA8QgUgBgMgNQgMgMgBgTQAAgNAFgJQAFgJAKgKIAVgTQAHgJABgFIAUAAQgEAHgGAHQgFAGgIAGQANgBAKAEQAcAKAAAhQAAAUgPAOQgOANgUAAIgDAAgAgZgMQgJAJAAALQABALAHAIQAKAKAQAAQANAAAJgHQAMgJAAgPQAAgGgDgFQgDgJgJgFQgIgEgKAAQgRAAgJALg");
	this.shape_1184.setTransform(-235.5,106.5);

	this.shape_1185 = new cjs.Shape();
	this.shape_1185.graphics.f("#FFFF00").s().p("AgpAiQgNgLAAgUQAAgGADgHQAEgHAFgEQgFgDAAgGIABgFQAEgLAVAAQAPAAAGAJQAHgJARAAQAJAAAGAEQAHADABAIQABAFgDAFQAEAEADAGQADAIAAAGQAAAUgOAMQgPAMgaAAQgaAAgPgNgAglgJQgEAEAAAIQABALAOAGQAKAEAQAAQAPAAALgEQAPgGAAgMQAAgHgEgEQgFgGgHAAIgzAAQgGAAgFAGgAAMggQgFACAAAGIAZAAIABgDQAAgEgEgCQgDgCgEAAQgGAAgEADgAgcgjQACACAAADQABADgCADIAUAAQAAgJgMgCIgEgBIgFABg");
	this.shape_1185.setTransform(-246.4,107.8);

	this.shape_1186 = new cjs.Shape();
	this.shape_1186.graphics.f("#FFFF00").s().p("AATAUQgDgCgFgGQgGgGAAgEQgDgEAAgFQAAgGAGgEQADgDAFABQAGgBAEAGQADADgBAFQAAAGgEADIgGADIAAABQAAADADAEIAHAGgAgLAUQgDgCgFgGQgFgGgCgEQgBgEAAgFQAAgGAEgEQAEgDAEABQAHgBAEAGQACADABAFQAAAGgGADIgEADIgBABQAAADADAEIAHAGg");
	this.shape_1186.setTransform(-256,100.9);

	this.shape_1187 = new cjs.Shape();
	this.shape_1187.graphics.f("#FFFFFF").s().p("AgIALQgFgEAAgHQAAgEADgEQAEgFAGAAQAFAAAEADQAFAEAAAGQAAAFgDAEQgEAFgHAAQgEAAgEgDg");
	this.shape_1187.setTransform(205.9,78.5);

	this.shape_1188 = new cjs.Shape();
	this.shape_1188.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1188.setTransform(200.4,74.8);

	this.shape_1189 = new cjs.Shape();
	this.shape_1189.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKABQATAAAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABAEgCADIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1189.setTransform(192.3,73.5);

	this.shape_1190 = new cjs.Shape();
	this.shape_1190.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1190.setTransform(179.9,74.8);

	this.shape_1191 = new cjs.Shape();
	this.shape_1191.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKABQATAAAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABAEgCADIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1191.setTransform(167.5,73.5);

	this.shape_1192 = new cjs.Shape();
	this.shape_1192.graphics.f("#FFFFFF").s().p("AgRARQgHgGgDgKIgBgEQAAgLAJgIIAAANIAEAAQAAAGAFAFQAFAFAGAAQAHAAAFgGQAFgFgBgHIALAAIAAAFQAAAMgHAIQgJAJgMAAQgJAAgIgGg");
	this.shape_1192.setTransform(152.4,80.8);

	this.shape_1193 = new cjs.Shape();
	this.shape_1193.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1193.setTransform(148.6,74.8);

	this.shape_1194 = new cjs.Shape();
	this.shape_1194.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1194.setTransform(135.4,74.8);

	this.shape_1195 = new cjs.Shape();
	this.shape_1195.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1195.setTransform(124.1,74.8);

	this.shape_1196 = new cjs.Shape();
	this.shape_1196.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1196.setTransform(112.4,74.8);

	this.shape_1197 = new cjs.Shape();
	this.shape_1197.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1197.setTransform(100.8,74.8);

	this.shape_1198 = new cjs.Shape();
	this.shape_1198.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1198.setTransform(83.3,74.8);

	this.shape_1199 = new cjs.Shape();
	this.shape_1199.graphics.f("#FFFFFF").s().p("AgCBBQgWgBgNgNQgMgNgBgUQAAgSALgNQgDgBgDgEQgEgEgBgEIgCgKQAAgJAJgKQAIgKAPAAIAJABQAKADABAHIAFgIIAVAAQgFAIgHAIQgIAIgIAFQAOgBALAEQAeALAAAiQAAAWgRAPQgPAOgVAAIgCgBgAgagLQgJAKAAALQAAAMAIAIQALALAQAAQAOAAAKgHQANgKAAgPQAAgHgDgGQgEgJgJgFQgIgEgLAAQgSAAgKALgAgigpQgDAGAAAEQAAAHAFADIADgEIAXgVQgCgEgFAAIgEAAQgLAAgGAJg");
	this.shape_1199.setTransform(71.4,73.3);

	this.shape_1200 = new cjs.Shape();
	this.shape_1200.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1200.setTransform(59.1,74.8);

	this.shape_1201 = new cjs.Shape();
	this.shape_1201.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgBgNgLQgPgKAAgUQAAgJAFgJQAFgIAIgDQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbAAAHAPQACgHAFgEQAGgEAHAAIAMAAIAAALIgCAAQgEAAAAADQgBACAEADIAIAHQAEAFABAHQAAALgKAHQgJAEgMgBIAAAZIAKgCIAIgEIAAAMIgIADIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAEIAMACIANgBIAAg0IgaAAQgKAAgHAIgAAdgdIAAATIACAAQAFAAADgCQAEgDAAgFQAAgFgFgHQgFgHAAgEIAAgBQgEABAAAOgAgXgvQACADAAAEQAAADgCABIACAAIADAAIAZAAQgCgHgIgDQgGgCgGAAIgIABg");
	this.shape_1201.setTransform(46,75.9);

	this.shape_1202 = new cjs.Shape();
	this.shape_1202.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1202.setTransform(31.4,68.1);

	this.shape_1203 = new cjs.Shape();
	this.shape_1203.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1203.setTransform(27.1,74.8);

	this.shape_1204 = new cjs.Shape();
	this.shape_1204.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1204.setTransform(14.4,74.8);

	this.shape_1205 = new cjs.Shape();
	this.shape_1205.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1205.setTransform(1.6,74.8);

	this.shape_1206 = new cjs.Shape();
	this.shape_1206.graphics.f("#FFFFFF").s().p("AgCBBQgWgBgNgNQgMgNgBgUQAAgSALgNQgDgBgDgEQgEgEgBgEIgCgKQAAgJAJgKQAIgKAPAAIAJABQAKADABAHIAFgIIAVAAQgFAIgHAIQgIAIgIAFQAOgBALAEQAeALAAAiQAAAWgRAPQgPAOgVAAIgCgBgAgagLQgJAKAAALQAAAMAIAIQALALAQAAQAOAAAKgHQANgKAAgPQAAgHgDgGQgEgJgJgFQgIgEgLAAQgSAAgKALgAgigpQgDAGAAAEQAAAHAFADIADgEIAXgVQgCgEgFAAIgEAAQgLAAgGAJg");
	this.shape_1206.setTransform(-10.6,73.3);

	this.shape_1207 = new cjs.Shape();
	this.shape_1207.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1207.setTransform(-18.4,74.8);

	this.shape_1208 = new cjs.Shape();
	this.shape_1208.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1208.setTransform(-26.7,74.8);

	this.shape_1209 = new cjs.Shape();
	this.shape_1209.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1209.setTransform(-35.3,74.8);

	this.shape_1210 = new cjs.Shape();
	this.shape_1210.graphics.f("#FFFFFF").s().p("AgFAwIgLgFQgGgCgFAAQgJAAABAIIgLAAQgCgIAAgJQAAgKACgHQADgIAJgHIAOgLQAIgHAAgHQABgEgDgEQgDgEgEAAQgGAAgDADQgEADAAAEQAAALAIAEIgQAAQgFgGAAgHIAAgEQABgJAHgFQAHgGALAAQAJAAAHAHQAGAGAAAJQAAAJgFAIIgNALIgNALQgGAHAAAJIABADIAIgBQAFAAAKADQAJADAGAAQAFAAAEgCQAGgBAFgFQAGgGAAgIQABgIgHgGIgMgNQgGgHAAgIQAAgKAGgFQAHgGAJAAQAJAAAGAFQAFAFABAIQABAGgDAEQgDAFgFABQgEAAgEgDQgDgCgBgEQAAgIAHgCQgCgDgEAAQgEAAgDADQgDACAAAEQAAAJAOAHQASAIAAASQAAAQgMAKQgLAKgQAAQgJAAgGgCg");
	this.shape_1210.setTransform(-42.6,74.8);

	this.shape_1211 = new cjs.Shape();
	this.shape_1211.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKABQATAAAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABAEgCADIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1211.setTransform(-53.6,73.5);

	this.shape_1212 = new cjs.Shape();
	this.shape_1212.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgKQgKgQAAgWQAAgOAIgNQAJgNALABQALABAEAIQAFgKAMAAQALAAAHAJQAIAIABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgLIACgIIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAHQgBARANALQANALAQAAQARAAANgNQANgNgBgRQAAgVgLgNQgLgOgVgBQgbABgKASIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARARABAdQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgEAAgGIgBgDQgBAEgFADQgEADgGAAQgNgBgDgLIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1212.setTransform(-71.8,73.4);

	this.shape_1213 = new cjs.Shape();
	this.shape_1213.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAYAAQAJAAAFAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1213.setTransform(-80,68.1);

	this.shape_1214 = new cjs.Shape();
	this.shape_1214.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1214.setTransform(-84.8,74.8);

	this.shape_1215 = new cjs.Shape();
	this.shape_1215.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKABQATAAAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABAEgCADIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1215.setTransform(-97.6,73.5);

	this.shape_1216 = new cjs.Shape();
	this.shape_1216.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1216.setTransform(-110,74.8);

	this.shape_1217 = new cjs.Shape();
	this.shape_1217.graphics.f("#FFFFFF").s().p("AgCA2QgHALgPAAQgRAAgJgOQgJgNABgVQABgZAPgMQALgLAXAAIAHAAIAAAMIgRAAQgIABgFAEQgMAJAAAPQgBANAFAIQAFAKAKAAQAIAAAFgFQAEgFAAgHIAAgPIALAAIAAAPQAAARARAAQAMAAAGgOQAGgMgBgUQgBgSgJgNQgNgRgXAAQgNAAgLAFQgLAFgEAJIgNAAQAEgNAOgJQAOgIAUAAQAOAAANAGQAKAEAGAHQARATABAiQABAZgKAQQgLASgTAAQgPAAgHgLg");
	this.shape_1217.setTransform(-122.6,73.3);

	this.shape_1218 = new cjs.Shape();
	this.shape_1218.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1218.setTransform(-141.3,74.8);

	this.shape_1219 = new cjs.Shape();
	this.shape_1219.graphics.f("#FFFFFF").s().p("AgoA2QgOgOAAgZQAAgPANgMQAMgLARABIANAAIAAAKIgKAAQgOAAgIAHQgIAFAAALQgBAQANAJQAMAIAQAAQARAAALgKQAMgKAAgSQAAgTgLgJQgJgIgOAAIgkAAQgIAAgFgDQgFgFAAgFQAAgNAOgGQALgFAWAAQAaAAANALQAMALgCALIgNAAIABgDQAAgFgJgGQgLgGgRgBQgVAAgFAGIgCADQAAAFAIAAIAgAAQAMAAAMAIQATAMAAAcQAAAcgOAQQgPARgaAAQgZAAgPgOg");
	this.shape_1219.setTransform(-153.5,73);

	this.shape_1220 = new cjs.Shape();
	this.shape_1220.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1220.setTransform(-165.5,68.6);

	this.shape_1221 = new cjs.Shape();
	this.shape_1221.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1221.setTransform(-165.8,74.8);

	this.shape_1222 = new cjs.Shape();
	this.shape_1222.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1222.setTransform(-180.2,68.1);

	this.shape_1223 = new cjs.Shape();
	this.shape_1223.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1223.setTransform(-184.2,74.8);

	this.shape_1224 = new cjs.Shape();
	this.shape_1224.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1224.setTransform(-195.1,74.8);

	this.shape_1225 = new cjs.Shape();
	this.shape_1225.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1225.setTransform(-206.5,74.8);

	this.shape_1226 = new cjs.Shape();
	this.shape_1226.graphics.f("#FFFFFF").s().p("AgrAyQgJgKABgOQACgMALgHIgUAAIAAgJIAQAAQgDgEACgGQABgJAJgDQAIgEALAAQANABAIALQAIALAAAMIgaAAQgIAAgGAFQgHAEgBAHQAAAIALAFQAKAGARAAQAngBAAglQAAgNgJgLQgJgLgPAAIgtAAQgHgBgDgEQgEgEABgFQACgMAQgEQALgDAVgBQAcAAAQARQACADABAEQABAEAAADIgNAAQAIAEAGAMQAGANAAAKQABAbgOAQQgOASgaAAQgiAAgOgQgAgXgOQADABABADQABAEgCAEIAVAAQgBgGgEgEQgFgEgHAAIgHACgAgZguQAAADAIABIAfAAQAIAAAJADIAHAEQABgEgHgGQgIgHgXAAQgbAAABAGg");
	this.shape_1226.setTransform(-219.1,73.3);

	this.shape_1227 = new cjs.Shape();
	this.shape_1227.graphics.f("#FFFFFF").s().p("AgpAwQgPgPABgYQAAgRAMgMQANgMARAAIANAAIAAALIgLAAQgNAAgIAIQgIAIAAALQgBAPANAKQAMAIAQABQASAAAMgNQAMgNAAgSQAAgTgMgOQgMgOgSAAQgNgBgLAGQgKAGgFAJIgLAAQAFgPANgIQAOgJASABQAZAAAQASQAQASAAAbQAAAagQATQgQASgaAAQgZAAgPgQg");
	this.shape_1227.setTransform(-236.9,73.5);

	this.shape_1228 = new cjs.Shape();
	this.shape_1228.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1228.setTransform(-248.4,73.4);

	this.shape_1229 = new cjs.Shape();
	this.shape_1229.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1229.setTransform(-266.5,74.8);

	this.shape_1230 = new cjs.Shape();
	this.shape_1230.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKABQATAAAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABAEgCADIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1230.setTransform(-278.9,73.5);

	this.shape_1231 = new cjs.Shape();
	this.shape_1231.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1231.setTransform(260.4,50.2);

	this.shape_1232 = new cjs.Shape();
	this.shape_1232.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1232.setTransform(247.8,50.2);

	this.shape_1233 = new cjs.Shape();
	this.shape_1233.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1233.setTransform(238.8,50.2);

	this.shape_1234 = new cjs.Shape();
	this.shape_1234.graphics.f("#FFFFFF").s().p("AgbA6QgMgGgHgKQgKgPAAgWQAAgOAIgNQAJgMALABQALAAAEAIQAFgJAMAAQALgBAHAJQAIAHABAMQAAAMgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgMIACgHIADgHQABgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAIQgBARANAKQANALAQAAQARAAANgNQANgNgBgSQAAgUgLgOQgLgOgVABQgbAAgKATIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARARABAcQABAcgQAUQgQATgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgDAAgHIgBgDQgBAEgFACQgEADgGABQgNAAgDgNIgCAJgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1234.setTransform(230.6,48.8);

	this.shape_1235 = new cjs.Shape();
	this.shape_1235.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1235.setTransform(217.7,50.2);

	this.shape_1236 = new cjs.Shape();
	this.shape_1236.graphics.f("#FFFFFF").s().p("AgCBDQgVgBgNgNQgNgOAAgUQgBgNAGgKQAFgJALgLQAIgGAOgOQAGgJADgFIAUAAQgEAHgFAHQgGAGgJAHQAOgBAKAEQAeALAAAiQAAAWgQAOQgPAOgWAAIgCAAgAgagJQgIAJAAAMQAAAMAIAIQAKALARAAQAOAAAJgIQANgJAAgQQAAgGgDgGQgDgJgJgFQgJgFgLAAQgSAAgKAMgAgrgjIgJAAIAAgJIAJAAIAAgNIgJAAIAAgJIAaAAQAIAAAFAEQAGAEAAAIQAAAIgHAEQgHAEgLgBIAAAOQgHAHgEAHgAggg5IAAANQAKAAAAgHQAAgGgHAAIgDAAg");
	this.shape_1236.setTransform(205.3,48.5);

	this.shape_1237 = new cjs.Shape();
	this.shape_1237.graphics.f("#FFFFFF").s().p("AgoA0QgRgMABgWQABgSAPgFQgIgDAAgIQAAgLALgEQAIgFALABQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHACAKAAQATAAAMgOQALgLAAgRQABgVgLgNQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgIASAAQAZABARARQARAUAAAZQAAAagPASQgQATgaAAQgZAAgPgLgAgbgWQADACABACQABAFgCADIAVAAQgBgFgEgEQgFgFgHABIgHABg");
	this.shape_1237.setTransform(194.1,48.9);

	this.shape_1238 = new cjs.Shape();
	this.shape_1238.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1238.setTransform(176.7,48.9);

	this.shape_1239 = new cjs.Shape();
	this.shape_1239.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1239.setTransform(164.4,50.2);

	this.shape_1240 = new cjs.Shape();
	this.shape_1240.graphics.f("#FFFFFF").s().p("AAQA8IAAgWQgOACgLgBQgWAAgNgLQgPgKAAgUQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbABAHAOQACgHAFgDQAGgFAHAAIAMAAIAAALIgCgBQgEABAAAEQgBABAEADIAIAHQAEAFABAGQAAAMgKAGQgJAFgMgBIAAAZIAKgDIAIgDIAAALIgIAEIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAFIAMABIANgBIAAg0IgaAAQgKABgHAHgAAdgdIAAATIACAAQAFAAADgDQAEgDAAgEQAAgGgFgGQgFgHAAgEIAAgBQgEABAAAOgAgXguQACACAAADQAAAEgCACIACgBIADAAIAZAAQgCgGgIgDQgGgDgGAAIgIACg");
	this.shape_1240.setTransform(151.2,51.3);

	this.shape_1241 = new cjs.Shape();
	this.shape_1241.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1241.setTransform(132.4,50.2);

	this.shape_1242 = new cjs.Shape();
	this.shape_1242.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1242.setTransform(120.6,48.9);

	this.shape_1243 = new cjs.Shape();
	this.shape_1243.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1243.setTransform(108.4,50.2);

	this.shape_1244 = new cjs.Shape();
	this.shape_1244.graphics.f("#FFFFFF").s().p("AAQAxIAAgMIAGABQALAAAIgKQAIgKAAgMQABgMgHgJQgHgKgLgCIgFAAQgUAAgCASQABgCAEgBQAFAAADABQAIACADAIQAEAGgBAKQgBAPgMAKQgMAKgPAAQgYAAgMgOQgJgKAAgNQAAgHAEgGQADgHAGgEIgMAAQgDAAgDACIAAgMIANAAQgEgCAAgIIABgGQAFgMARAAQALAAAGAEQAHAFACAJQAJgRAXgBQASAAAMAQQALAQgBATQAAATgNAOQgNAPgSAAIgFgBgAgmgGQgGAFAAAGQAAAJAHAGQAIAGANAAQAMAAAGgEQAKgFAAgLQAAgHgIAAQgDAAgCABQgDACABAEIgLAAIAAgJIABgIIgMAAQgHAAgGAFgAgigkQADABABAFQAAAFgDADIAPAAQAAgHgEgEQgEgEgFAAIgDABg");
	this.shape_1244.setTransform(89,50.2);

	this.shape_1245 = new cjs.Shape();
	this.shape_1245.graphics.f("#FFFFFF").s().p("AgmA1QgQgPAAgXQAAgPAHgLQAHgOANAAQAMAAADALQAEgMANAAQAJAAAIAJQAGAIAAAJQABANgJAKQgJAIgLAAQgLAAgHgGQgJgHAAgJIADgOQABgHgGAAQgFAAgDAIQgDAFAAAHQAAAQAMAKQAMAKAQgBQARAAALgLQAMgMAAgQQAAgNgGgKQgGgKgMAAIhCAAIAAgKQARAAAAgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQgBAAAAgBQgEgCAAgCQAAgNAOgFQAIgCARAAQAPAAAJACQAMAEAIAKQAHAHAAALIgMAAQgBgLgMgFQgKgFgPAAIgOABQgHADAAAFQAAABAAAAQAAABAAAAQABAAAAABQAAAAABABIAEABIAfAAQAJAAAIADIAFAEQARAMAAAeQAAAagPAQQgQARgYAAQgXAAgPgPgAgNACQgBAFAFAFQAEADAFABQAHAAAEgEQAGgFgBgGQgEAHgJAAQgKAAgFgJIgBADgAAAgNQAAAAAAABQgBAAAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABAAAAQAAABABAAQAAABAAABQAAAAAAAAQABABAAAAQABAAAAAAQABABABAAQAAAAABgBQAAAAABAAQAAAAABgBQABAAAAAAQABgBAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAABgAAmghIAAAAgAAhglQgIgDgJAAIgfAAIgEgBQgBgBAAAAQAAgBgBAAQAAAAAAgBQAAAAAAgBQAAgFAHgDIAOgBQAPAAAKAFQAMAFABALIgFgEg");
	this.shape_1245.setTransform(76.7,48.4);

	this.shape_1246 = new cjs.Shape();
	this.shape_1246.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1246.setTransform(65.5,48.9);

	this.shape_1247 = new cjs.Shape();
	this.shape_1247.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1247.setTransform(57.6,50.2);

	this.shape_1248 = new cjs.Shape();
	this.shape_1248.graphics.f("#FFFFFF").s().p("AAQA8IAAgWQgOACgLgBQgWAAgNgLQgPgKAAgUQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbABAHAOQACgHAFgDQAGgFAHAAIAMAAIAAALIgCgBQgEABAAAEQgBABAEADIAIAHQAEAFABAGQAAAMgKAGQgJAFgMgBIAAAZIAKgDIAIgDIAAALIgIAEIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAFIAMABIANgBIAAg0IgaAAQgKABgHAHgAAdgdIAAATIACAAQAFAAADgDQAEgDAAgEQAAgGgFgGQgFgHAAgEIAAgBQgEABAAAOgAgXguQACACAAADQAAAEgCACIACgBIADAAIAZAAQgCgGgIgDQgGgDgGAAIgIACg");
	this.shape_1248.setTransform(49.3,51.3);

	this.shape_1249 = new cjs.Shape();
	this.shape_1249.graphics.f("#FFFFFF").s().p("AgRARQgHgGgDgKIAAgEQgBgLAJgIIAAANIAEAAQAAAGAFAFQAFAFAGAAQAHAAAFgGQAEgFAAgHIALAAIAAAFQABAMgJAIQgHAJgNAAQgJAAgIgGg");
	this.shape_1249.setTransform(33.8,56.2);

	this.shape_1250 = new cjs.Shape();
	this.shape_1250.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1250.setTransform(29.9,50.2);

	this.shape_1251 = new cjs.Shape();
	this.shape_1251.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAHgHIAFgKIAABDg");
	this.shape_1251.setTransform(17,55.1);

	this.shape_1252 = new cjs.Shape();
	this.shape_1252.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1252.setTransform(16.7,50.2);

	this.shape_1253 = new cjs.Shape();
	this.shape_1253.graphics.f("#FFFFFF").s().p("AgpAxQgPgQABgZQAAgQAMgMQANgMARAAIANAAIAAAMIgLAAQgNAAgIAHQgIAIAAALQgBAQANAJQAMAJAQAAQASAAAMgOQAMgMAAgSQAAgSgMgPQgMgOgSAAQgNgBgLAGQgKAGgFAKIgLAAQAFgQANgIQAOgJASAAQAZAAAQATQAQASAAAaQAAAbgQASQgQATgagBQgZAAgPgOg");
	this.shape_1253.setTransform(4.7,48.9);

	this.shape_1254 = new cjs.Shape();
	this.shape_1254.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1254.setTransform(-7.9,50.2);

	this.shape_1255 = new cjs.Shape();
	this.shape_1255.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1255.setTransform(-26.1,48.9);

	this.shape_1256 = new cjs.Shape();
	this.shape_1256.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1256.setTransform(-34,50.2);

	this.shape_1257 = new cjs.Shape();
	this.shape_1257.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1257.setTransform(-42.3,50.2);

	this.shape_1258 = new cjs.Shape();
	this.shape_1258.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1258.setTransform(-50.8,50.2);

	this.shape_1259 = new cjs.Shape();
	this.shape_1259.graphics.f("#FFFFFF").s().p("AgFAwIgLgFQgGgCgFAAQgJAAABAIIgLAAQgCgIAAgJQAAgKACgHQADgIAJgHIAOgLQAIgHAAgHQABgEgDgEQgDgEgEAAQgGAAgDADQgEADAAAEQAAALAIAEIgQAAQgFgGAAgHIAAgEQABgJAHgFQAHgGALAAQAJAAAHAHQAGAGAAAJQAAAJgFAIIgNALIgNALQgGAHAAAJIABADIAIgBQAFAAAKADQAJADAGAAQAFAAAEgCQAGgBAFgFQAGgGAAgIQABgIgHgGIgMgNQgGgHAAgIQAAgKAGgFQAHgGAJAAQAJAAAGAFQAFAFABAIQABAGgDAEQgDAFgFABQgEAAgEgDQgDgCgBgEQAAgIAHgCQgCgDgEAAQgEAAgDADQgDACAAAEQAAAJAOAHQASAIAAASQAAAQgMAKQgLAKgQAAQgJAAgGgCg");
	this.shape_1259.setTransform(-58.1,50.2);

	this.shape_1260 = new cjs.Shape();
	this.shape_1260.graphics.f("#FFFFFF").s().p("AgoA0QgRgMABgWQABgSAPgFQgIgDAAgIQAAgLALgEQAIgFALABQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHACAKAAQATAAAMgOQALgLAAgRQABgVgLgNQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgIASAAQAZABARARQARAUAAAZQAAAagPASQgQATgaAAQgZAAgPgLgAgbgWQADACABACQABAFgCADIAVAAQgBgFgEgEQgFgFgHABIgHABg");
	this.shape_1260.setTransform(-69.2,48.9);

	this.shape_1261 = new cjs.Shape();
	this.shape_1261.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1261.setTransform(-83.5,43.5);

	this.shape_1262 = new cjs.Shape();
	this.shape_1262.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAghAWgNQAIgFAMAAIAGABIAAALQAOgMASAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgJQgIAFgFACQgHADgJAAIgEgBgAAIgQQAQAFAAAUQAAAQgLAMQALACAHgFQANgIgBgVQAAgMgIgKQgIgJgMgBQgWgBgDAPQAFgEAHAAIAGABgAgogbQgGAJAAAMQAAANAHAJQAIAJANABQAKAAAIgFQAIgEAAgJQAAgFgCgDQgEgEgEAAQgEAAgCADQgCACAAAEIAAAEIgMAAQgCgLABgLQABgLAJgMIgHgBQgMAAgIAKg");
	this.shape_1262.setTransform(-87.7,50.2);

	this.shape_1263 = new cjs.Shape();
	this.shape_1263.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1263.setTransform(-99,50.2);

	this.shape_1264 = new cjs.Shape();
	this.shape_1264.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1264.setTransform(-106.4,50.2);

	this.shape_1265 = new cjs.Shape();
	this.shape_1265.graphics.f("#FFFFFF").s().p("AgbA6QgMgGgHgKQgKgPAAgWQAAgOAIgNQAJgMALABQALAAAEAIQAFgJAMAAQALgBAHAJQAIAHABAMQAAAMgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgMIACgHIADgHQABgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAIQgBARANAKQANALAQAAQARAAANgNQANgNgBgSQAAgUgLgOQgLgOgVABQgbAAgKATIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARARABAcQABAcgQAUQgQATgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgDAAgHIgBgDQgBAEgFACQgEADgGABQgNAAgDgNIgCAJgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1265.setTransform(-114.7,48.8);

	this.shape_1266 = new cjs.Shape();
	this.shape_1266.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1266.setTransform(-127.6,50.2);

	this.shape_1267 = new cjs.Shape();
	this.shape_1267.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1267.setTransform(-142.9,43.5);

	this.shape_1268 = new cjs.Shape();
	this.shape_1268.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1268.setTransform(-147.1,50.2);

	this.shape_1269 = new cjs.Shape();
	this.shape_1269.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1269.setTransform(-158,50.2);

	this.shape_1270 = new cjs.Shape();
	this.shape_1270.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1270.setTransform(-165.2,43.5);

	this.shape_1271 = new cjs.Shape();
	this.shape_1271.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1271.setTransform(-169.4,50.2);

	this.shape_1272 = new cjs.Shape();
	this.shape_1272.graphics.f("#FFFFFF").s().p("AAEA/QgEgCgCgFQgGAJgSAAQgUAAgJgWQgFgPABgVQABgPAJgLQAJgMAOAAQAGAAAEAEQAEADAAAHQAAACgDAEQgDAFABADQAAAKAOABQAEAAAFgDQAFgCABgDIgHAAQgFAAgFgDQgFgDgBgGQgBgIAGgGQAFgFAHAAQAVAAAAAYQAAAKgIAIQgIAHgLABQgKABgIgEQgIgEgDgJIgBgFQAAgHADgDQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQgGgBgGAJQgFAKgBAJQAAAMAGAKQAHALAKAAQAQAAAAgSIAKAAQAAAKAFAEQAEAEAJAAQANAAAHgOQAFgKAAgQQABgUgLgPQgOgSgVAAQgkAAgHAYIgMAAQACgPAPgKQAPgKAXAAQAeABAQAXQANASAAAWQABAcgJARQgKAUgWAAQgLAAgFgCgAAAgPQAAAGAGAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAgBAAQgGAAAAAHg");
	this.shape_1272.setTransform(-181.7,48.7);

	this.shape_1273 = new cjs.Shape();
	this.shape_1273.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1273.setTransform(-200.2,50.2);

	this.shape_1274 = new cjs.Shape();
	this.shape_1274.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1274.setTransform(-212.3,51.5);

	this.shape_1275 = new cjs.Shape();
	this.shape_1275.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1275.setTransform(-220.8,50.2);

	this.shape_1276 = new cjs.Shape();
	this.shape_1276.graphics.f("#FFFFFF").s().p("AgoA0QgSgNABgUQABgQANgIQgJgBAAgKQAAgJALgFQAHgEALABQAQgBAGALQAEgJAOgBIAIAAIAGgFIAEgFQAGgKABgLIAQAAIgDALQgCAHgEAFIgKAKQAIAEACAKQABAGgDAEIgJAAQAFABAFAFQAIAHAAAPQAAATgSANQgRAMgVAAQgXAAgRgNgAglAEQgFAGAAAHQABALANAHQAMAGAQAAQAQAAALgFQAOgHABgNQAAgHgFgFQgFgEgLAAIgtAAQgIAAgFAEgAAOgWQgFABgCACQgEAEAAAEIATAAQAGAAAFACQABgCgBgFQgDgGgMAAIgEAAgAgegVQADACABADQABADgCACIAUAAQgBgHgHgDIgHgBQgEAAgEABg");
	this.shape_1276.setTransform(-228.7,48.7);

	this.shape_1277 = new cjs.Shape();
	this.shape_1277.graphics.f("#FFFFFF").s().p("AgCBDQgVgBgNgNQgNgOAAgUQgBgNAGgKQAFgJALgLQAIgGAOgOQAGgJADgFIAUAAQgEAHgFAHQgGAGgJAHQAOgBAKAEQAeALAAAiQAAAWgQAOQgPAOgWAAIgCAAgAgagJQgIAJAAAMQAAAMAIAIQAKALARAAQAOAAAJgIQANgJAAgQQAAgGgDgGQgDgJgJgFQgJgFgLAAQgSAAgKAMgAgrgjIgJAAIAAgJIAJAAIAAgNIgJAAIAAgJIAaAAQAIAAAFAEQAGAEAAAIQAAAIgHAEQgHAEgLgBIAAAOQgHAHgEAHgAggg5IAAANQAKAAAAgHQAAgGgHAAIgDAAg");
	this.shape_1277.setTransform(-240.2,48.5);

	this.shape_1278 = new cjs.Shape();
	this.shape_1278.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1278.setTransform(-252.4,50.2);

	this.shape_1279 = new cjs.Shape();
	this.shape_1279.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1279.setTransform(-261.6,43.5);

	this.shape_1280 = new cjs.Shape();
	this.shape_1280.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1280.setTransform(-265.9,50.2);

	this.shape_1281 = new cjs.Shape();
	this.shape_1281.graphics.f("#FFFFFF").s().p("AAQA8IAAgWQgOACgLgBQgWAAgNgLQgPgKAAgUQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbABAHAOQACgHAFgDQAGgFAHAAIAMAAIAAALIgCgBQgEABAAAEQgBABAEADIAIAHQAEAFABAGQAAAMgKAGQgJAFgMgBIAAAZIAKgDIAIgDIAAALIgIAEIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAFIAMABIANgBIAAg0IgaAAQgKABgHAHgAAdgdIAAATIACAAQAFAAADgDQAEgDAAgEQAAgGgFgGQgFgHAAgEIAAgBQgEABAAAOgAgXguQACACAAADQAAAEgCACIACgBIADAAIAZAAQgCgGgIgDQgGgDgGAAIgIACg");
	this.shape_1281.setTransform(-278.6,51.3);

	this.shape_1282 = new cjs.Shape();
	this.shape_1282.graphics.f("#FFFFFF").s().p("AgsAWQgHgDAAgNIABgGQAHgVAsAAQAVAAAMADQAQAFACANQABAJgGAFQgGAFgJAAQgIAAgFgEQgGgEAAgIQAAgDABgEIgOAAQghAAgFAKIgBAEQAAAIAHAEgAAaABQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQADACACAAQAHAAAAgHQAAAAAAgBQAAAAgBAAQAAgBAAAAQAAgBgBAAQgCgCgDAAQgGAAAAAFg");
	this.shape_1282.setTransform(199.1,20.2);

	this.shape_1283 = new cjs.Shape();
	this.shape_1283.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1283.setTransform(200.7,27.9);

	this.shape_1284 = new cjs.Shape();
	this.shape_1284.graphics.f("#FFFFFF").s().p("AggA2QgQgJAAgQQAAgIAEgFQAFgFAIgBQAGAAAFAFQAEAEAAAGQABAOgRAAQAKAIAPABQALAAAIgFQAIgHAAgLQAAgOgJgEQgHgEgOAAIAAgMQARAAAGgHQAFgEAAgIQABgJgHgGQgHgFgIAAQgIAAgJAEIgFADQAMADAAAKQAAAGgFAEQgEADgHAAQgGABgFgFQgEgFAAgHQAAgOARgIQAOgHAPgBQARABAMAGQAPAJAAAPQAAAMgJAHQgGAEgOADQAPAEAHAEQAKAIAAAOQAAASgSAKQgOAHgSABQgSAAgNgIg");
	this.shape_1284.setTransform(183.3,25.6);

	this.shape_1285 = new cjs.Shape();
	this.shape_1285.graphics.f("#FFFFFF").s().p("AgmAqQgOgRAAgZQgBgZAOgRQAOgSAZAAQAYAAAPARQAPARAAAaQAAAZgOARQgOATgaAAQgYAAgOgTgAgXAAQAAAuAXAAQALAAAHgJQAGgKAAgbQAAgVgDgJQgFgPgRgBQgWAAAAAug");
	this.shape_1285.setTransform(171.5,25.6);

	this.shape_1286 = new cjs.Shape();
	this.shape_1286.graphics.f("#FFFFFF").s().p("AgmAqQgOgRAAgZQgBgZAOgRQAOgSAZAAQAYAAAPARQAPARAAAaQAAAZgOARQgOATgaAAQgYAAgOgTgAgXAAQAAAuAXAAQALAAAHgJQAGgKAAgbQAAgVgDgJQgFgPgRgBQgWAAAAAug");
	this.shape_1286.setTransform(159.6,25.6);

	this.shape_1287 = new cjs.Shape();
	this.shape_1287.graphics.f("#FFFFFF").s().p("AAHA7IgHgDQgVgKgFAAQgJAAgBAMIgMAAQAAgVAGgLQAHgMAUgIQAPgGAGgFQAMgIAAgMQAAgKgJgGQgHgFgJgBQgQAAgJAPIAHgBQAGAAADAEQAEADAAAGQAAAGgEAFQgFAEgHAAQgIAAgFgGQgEgGAAgIQAAgRAOgJQAOgIARgBQARAAANAJQAQAKAAASQAAAQgJAHQgHAHgTAEIgYAIQgLAGgIAMQAKgGAGAAQAFABAMADQAHACAOABQAJAAAEgHQACgDABgLIALAAQAAArgdABQgGAAgHgCg");
	this.shape_1287.setTransform(147.6,25.6);

	this.shape_1288 = new cjs.Shape();
	this.shape_1288.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1288.setTransform(133.4,19.7);

	this.shape_1289 = new cjs.Shape();
	this.shape_1289.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1289.setTransform(129.1,26.5);

	this.shape_1290 = new cjs.Shape();
	this.shape_1290.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1290.setTransform(116.4,20.2);

	this.shape_1291 = new cjs.Shape();
	this.shape_1291.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1291.setTransform(116.1,26.5);

	this.shape_1292 = new cjs.Shape();
	this.shape_1292.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1292.setTransform(104.4,25);

	this.shape_1293 = new cjs.Shape();
	this.shape_1293.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAHgHIAFgKIAABDg");
	this.shape_1293.setTransform(86.7,31.3);

	this.shape_1294 = new cjs.Shape();
	this.shape_1294.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAIgFQAJgEACgJQAEgKAAgKQgBgKgIgIQgIgJgLAAQgSABgEAQQACgCAFAAQAFAAAEACQAKAIAAAQQAAARgOAJQgMAKgSAAQgSgBgMgKQgMgLAAgRQAAgSANgJQgEgBgCgEQgCgDgBgFQAAgJAJgGQAHgEAJAAQAIAAAHAFQAGAFACAJQALgTAWAAQATAAALAPQAMAPAAASQAAAWgOANQgOAOgWABgAgqgFQgGAEAAAHQAAAJAJAHQAJAEALAAQALABAIgFQAHgEACgJQAAgEgCgDQgCgCgDAAQgCAAgDACQgCACAAACIgKAAIgBgIIABgJIgPAAQgHAAgFAGgAgogjQADABACAEQABAFgBAEIAMAAQAAgIgFgDQgDgEgGAAIgDABg");
	this.shape_1294.setTransform(86.1,26.5);

	this.shape_1295 = new cjs.Shape();
	this.shape_1295.graphics.f("#FFFFFF").s().p("AAEA/QgEgCgCgFQgGAJgSAAQgUAAgJgWQgFgPABgVQABgPAJgLQAJgMAOAAQAGAAAEAEQAEADAAAHQAAACgDAEQgDAFABADQAAAKAOABQAEAAAFgDQAFgCABgDIgHAAQgFAAgFgDQgFgDgBgGQgBgIAGgGQAFgFAHAAQAVAAAAAYQAAAKgIAIQgIAHgLABQgKABgIgEQgIgEgDgJIgBgFQAAgHADgDQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAAAgBgBQAAAAgBAAQgGgBgGAJQgFAKgBAJQAAAMAGAKQAHALAKAAQAQAAAAgSIAKAAQAAAKAFAEQAEAEAJAAQANAAAHgOQAFgKAAgQQABgUgLgPQgOgSgVAAQgkAAgHAYIgMAAQACgPAPgKQAPgKAXAAQAeABAQAXQANASAAAWQABAcgJARQgKAUgWAAQgLAAgFgCgAAAgPQAAAGAGAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAgBAAQgGAAAAAHg");
	this.shape_1295.setTransform(73.1,24.9);

	this.shape_1296 = new cjs.Shape();
	this.shape_1296.graphics.f("#FFFFFF").s().p("AgkA4QgPgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAAAAMQABAKAMAEQAJAEAQAAQAQAAALgJQAMgIAAgOQABgSgJgMQgIgJgNAAIg6AAIAAgJQANAAAAgBQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBgBgBAAQgDgCgBgDQgBgPAsAAQASAAAPAHQAQAIAAAOIgMAAQAAgIgNgFQgLgEgNAAQgWAAgBAGQAAAFAIAAIAbAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgOgIgAgagNQADACAAAEQAAAEgDADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1296.setTransform(55.1,25);

	this.shape_1297 = new cjs.Shape();
	this.shape_1297.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1297.setTransform(44.7,26.5);

	this.shape_1298 = new cjs.Shape();
	this.shape_1298.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBYAAIAAgfIAFgHIAGgKIAABDg");
	this.shape_1298.setTransform(33.8,31.3);

	this.shape_1299 = new cjs.Shape();
	this.shape_1299.graphics.f("#FFFFFF").s().p("AgbA6QgMgGgHgJQgKgQAAgWQAAgOAIgNQAJgMALABQALAAAEAIQAFgJAMAAQALgBAHAJQAIAHABAMQAAAMgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgKIACgIIADgHQABgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAHQgBARANALQANALAQAAQARAAANgNQANgNgBgSQAAgUgLgNQgLgOgVgBQgbABgKASIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARARABAcQABAdgQATQgQATgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgFQAGgDAAgHIgBgDQgBAEgFACQgEAEgGAAQgNAAgDgNIgCAJgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1299.setTransform(33.6,25.1);

	this.shape_1300 = new cjs.Shape();
	this.shape_1300.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAIgFQAIgEAEgJQADgKgBgKQgBgKgHgIQgIgJgLAAQgSABgEAQQACgCAFAAQAGAAADACQALAIgBAQQgBARgNAJQgMAKgSAAQgSgBgMgKQgMgLAAgRQAAgSANgJQgEgBgCgEQgCgDgBgFQABgJAHgGQAIgEAJAAQAJAAAGAFQAGAFACAJQALgTAWAAQASAAANAPQALAPAAASQAAAWgOANQgOAOgWABgAgrgFQgFAEAAAHQAAAJAJAHQAJAEAMAAQAKABAIgFQAHgEACgJQAAgEgCgDQgCgCgEAAQgBAAgDACQgCACAAACIgKAAIgBgIIABgJIgPAAQgHAAgGAGgAgogjQADABACAEQABAFgBAEIAMAAQAAgIgEgDQgEgEgFAAIgEABg");
	this.shape_1300.setTransform(21,26.5);

	this.shape_1301 = new cjs.Shape();
	this.shape_1301.graphics.f("#FFFFFF").s().p("AgtBCIAAgQIBWAAIAAgKQgRAIgYAAQgZAAgPgRQgQgRAAgYQAAgYASgPQARgQAXAAQAXAAAQAPQAQAQAAAVQAAAQgKANQgLANgQAAQgQAAgJgJQgMgJAAgOQAAgPAMgJIgMAAIAAgLIAqAAIAAALIgJAAQgGAAgFAEQgFAFAAAGQgBAIAFAFQAGAFAKAAQAJAAAGgGQAHgIAAgKQAAgQgLgKQgMgJgSAAQgPAAgKANQgJAMAAARQAAATAMAOQAMAOASAAQAVAAANgMIALAAIAAAqg");
	this.shape_1301.setTransform(2.3,28.1);

	this.shape_1302 = new cjs.Shape();
	this.shape_1302.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1302.setTransform(-6.1,26.5);

	this.shape_1303 = new cjs.Shape();
	this.shape_1303.graphics.f("#FFFFFF").s().p("AgpAxQgPgQABgZQAAgQAMgMQANgMARAAIANAAIAAALIgLAAQgNABgIAHQgIAIAAALQgBAQANAJQAMAJAQAAQASgBAMgNQAMgMAAgSQAAgTgMgOQgMgOgSAAQgNgBgLAGQgKAGgFAKIgLAAQAFgQANgIQAOgJASAAQAZABAQASQAQASAAAaQAAAbgQASQgQATgagBQgZAAgPgOg");
	this.shape_1303.setTransform(-14.1,25.1);

	this.shape_1304 = new cjs.Shape();
	this.shape_1304.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1304.setTransform(-24.8,26.5);

	this.shape_1305 = new cjs.Shape();
	this.shape_1305.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1305.setTransform(-36.5,26.5);

	this.shape_1306 = new cjs.Shape();
	this.shape_1306.graphics.f("#FFFFFF").s().p("AgnA1QgPgQAAgWQAAgQAGgLQAIgMANAAQANgBABALQAEgMANAAQAKAAAHAJQAIAIAAAJQAAANgJAKQgIAIgMAAQgKAAgIgGQgJgGABgKIACgOQABgHgHAAQgEAAgEAIQgCAFAAAHQAAAQAMAKQAMAKAQgBQARAAAMgLQALgLAAgRQAAgNgGgKQgGgKgNAAIhBAAIAAgKQARAAAAgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQgBAAgBAAQgDgDAAgDQAAgNAPgEQAHgCARAAQAPAAAJACQANAEAHAKQAHAHAAALIgLAAQgCgLgMgFQgKgFgOAAIgPABQgHADAAAFQAAABAAAAQAAABABAAQAAAAAAABQABAAAAABIAEABIAeAAQALAAAGADIAHAEQAQAMAAAeQAAAZgQARQgPARgYAAQgXAAgQgPgAgOACQABAGAEAEQAEADAGAAQAGABAFgEQAEgFAAgGQgEAHgJAAQgJAAgHgJIgBADgAAAgNQAAAAAAABQgBAAAAABQAAAAAAABQgBAAAAABQAAABABAAQAAABAAAAQAAABABAAQAAABAAABQAAAAAAAAQABABAAAAQABAAAAAAQABAAAAAAQABAAABAAQAAAAABAAQAAAAABgBQABAAAAAAQAAgBABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQgBAAAAgBQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAABgAAnghIAAAAgAAgglQgGgDgLAAIgeAAIgEgBQAAgBgBAAQAAgBAAAAQgBAAAAgBQAAAAAAgBQAAgFAHgDIAPgBQAOAAAKAFQAMAFACALIgHgEg");
	this.shape_1306.setTransform(-48.9,24.7);

	this.shape_1307 = new cjs.Shape();
	this.shape_1307.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1307.setTransform(-61.5,26.5);

	this.shape_1308 = new cjs.Shape();
	this.shape_1308.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1308.setTransform(-76.8,26.5);

	this.shape_1309 = new cjs.Shape();
	this.shape_1309.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAJgFQAIgEADgJQADgKgBgKQgBgKgHgIQgIgJgLAAQgSABgEAQQACgCAFAAQAFAAAEACQALAIgBAQQgBARgNAJQgMAKgSAAQgSgBgMgKQgMgLAAgRQAAgSANgJQgDgBgDgEQgDgDAAgFQABgJAHgGQAIgEAJAAQAIAAAHAFQAGAFACAJQALgTAWAAQASAAANAPQALAPAAASQAAAWgOANQgOAOgWABgAgrgFQgFAEAAAHQAAAJAKAHQAIAEAMAAQAKABAIgFQAHgEACgJQAAgEgCgDQgCgCgEAAQgBAAgCACQgDACAAACIgLAAIAAgIIABgJIgOAAQgIAAgGAGgAgogjQADABACAEQACAFgCAEIAMAAQAAgIgEgDQgEgEgFAAIgEABg");
	this.shape_1309.setTransform(-85.5,26.5);

	this.shape_1310 = new cjs.Shape();
	this.shape_1310.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1310.setTransform(-105.1,26.5);

	this.shape_1311 = new cjs.Shape();
	this.shape_1311.graphics.f("#FFFFFF").s().p("AgCA2QgHALgPAAQgRAAgJgOQgJgNABgVQABgZAPgNQALgKAXAAIAHAAIAAALIgRABQgIABgFAEQgMAJAAAPQgBANAFAIQAFAKAKAAQAIAAAFgFQAEgFAAgIIAAgOIALAAIAAAOQAAASARAAQAMAAAGgOQAGgMgBgUQgBgSgJgNQgNgRgXAAQgNgBgLAGQgLAFgEAJIgNAAQAEgNAOgJQAOgIAUAAQAOAAANAGQAKAEAGAHQARATABAhQABAagKAQQgLASgTAAQgPAAgHgLg");
	this.shape_1311.setTransform(-117.9,25);

	this.shape_1312 = new cjs.Shape();
	this.shape_1312.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1312.setTransform(-126.5,26.5);

	this.shape_1313 = new cjs.Shape();
	this.shape_1313.graphics.f("#FFFFFF").s().p("AACA9QgHgDgFgIQgBAGgGADQgFAEgIAAQgOAAgIgOQgHgNAAgSQAAgUALgQQANgQAVAAQAOAAAKAKQAJAJAAAOQAAALgJAIIgFAFQgCACAAAEQgBAKAQABQAMgBAHgMQAHgLAAgQQAAgUgNgQQgMgPgYAAQgMAAgKAFQgLAGgFAJIgNAAQAGgPAOgJQAOgHARgBQAdAAARARQAHAIAGAOQAGAOAAAKQABAegNASQgMAPgSAAQgKAAgGgCgAgiAmQgCACAAADQAAADACACQACACADAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgDgDAAQgDAAgCADgAgrADQgDAHAAAKQAAAHACAGQAFgJALgBQAJgBAHAHQABgHAGgFQAGgHAAgIQAAgFgEgFQgFgEgHAAQgUAAgIAPg");
	this.shape_1313.setTransform(-134.9,25.1);

	this.shape_1314 = new cjs.Shape();
	this.shape_1314.graphics.f("#FFFFFF").s().p("AgtBCIAAgQIBWAAIAAgKQgRAIgYAAQgZAAgPgRQgQgRAAgYQAAgYASgPQARgQAXAAQAXAAAQAPQAQAQAAAVQAAAQgKANQgLANgQAAQgQAAgJgJQgMgJAAgOQAAgPAMgJIgMAAIAAgLIAqAAIAAALIgJAAQgGAAgFAEQgFAFAAAGQgBAIAFAFQAGAFAKAAQAJAAAGgGQAHgIAAgKQAAgQgLgKQgMgJgSAAQgPAAgKANQgJAMAAARQAAATAMAOQAMAOASAAQAVAAANgMIALAAIAAAqg");
	this.shape_1314.setTransform(-153.1,28.1);

	this.shape_1315 = new cjs.Shape();
	this.shape_1315.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1315.setTransform(-165.2,26.4);

	this.shape_1316 = new cjs.Shape();
	this.shape_1316.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1316.setTransform(-177.5,20.2);

	this.shape_1317 = new cjs.Shape();
	this.shape_1317.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1317.setTransform(-177.8,26.5);

	this.shape_1318 = new cjs.Shape();
	this.shape_1318.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1318.setTransform(-192.2,19.7);

	this.shape_1319 = new cjs.Shape();
	this.shape_1319.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1319.setTransform(-196.9,26.5);

	this.shape_1320 = new cjs.Shape();
	this.shape_1320.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1320.setTransform(-210.8,26.5);

	this.shape_1321 = new cjs.Shape();
	this.shape_1321.graphics.f("#FFFFFF").s().p("AgoA0QgRgMABgWQABgSAPgFQgIgCAAgJQAAgLALgEQAIgFALABQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHABAKABQATgBAMgNQALgLAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASAAQAZAAARASQARAUAAAZQAAAagPASQgQATgaAAQgZAAgPgLgAgbgWQADABABADQABAFgCADIAVAAQgBgFgEgEQgFgFgHABIgHABg");
	this.shape_1321.setTransform(-223.6,25.1);

	this.shape_1322 = new cjs.Shape();
	this.shape_1322.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1322.setTransform(-234.4,26.5);

	this.shape_1323 = new cjs.Shape();
	this.shape_1323.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAYAAQAJAAAFAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1323.setTransform(-241.6,19.7);

	this.shape_1324 = new cjs.Shape();
	this.shape_1324.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1324.setTransform(-245.8,26.5);

	this.shape_1325 = new cjs.Shape();
	this.shape_1325.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1325.setTransform(-256.7,26.5);

	this.shape_1326 = new cjs.Shape();
	this.shape_1326.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1326.setTransform(-268.4,26.5);

	this.shape_1327 = new cjs.Shape();
	this.shape_1327.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1327.setTransform(-280,26.5);

	this.shape_1328 = new cjs.Shape();
	this.shape_1328.graphics.f("#FFFFFF").s().p("AgsAAIgfhWICXBWIiXBXg");
	this.shape_1328.setTransform(-305.9,-35.8);

	this.shape_1329 = new cjs.Shape();
	this.shape_1329.graphics.f("#FFFFFF").s().p("AgIALQgFgEAAgHQAAgEADgEQAEgFAGAAQAFAAAEADQAFAEAAAGQAAAFgDAEQgEAFgHAAQgEAAgEgDg");
	this.shape_1329.setTransform(47.2,-6.1);

	this.shape_1330 = new cjs.Shape();
	this.shape_1330.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1330.setTransform(41.6,-9.8);

	this.shape_1331 = new cjs.Shape();
	this.shape_1331.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKAAQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABADgCAEIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1331.setTransform(33.6,-11.1);

	this.shape_1332 = new cjs.Shape();
	this.shape_1332.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1332.setTransform(21.2,-9.8);

	this.shape_1333 = new cjs.Shape();
	this.shape_1333.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKAAQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABADgCAEIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1333.setTransform(8.8,-11.1);

	this.shape_1334 = new cjs.Shape();
	this.shape_1334.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1334.setTransform(-2,-9.8);

	this.shape_1335 = new cjs.Shape();
	this.shape_1335.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAaAAQAHAAAGAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1335.setTransform(-15.3,-16.5);

	this.shape_1336 = new cjs.Shape();
	this.shape_1336.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1336.setTransform(-20,-9.8);

	this.shape_1337 = new cjs.Shape();
	this.shape_1337.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgKQgKgQAAgWQAAgOAIgNQAJgNALABQALABAEAIQAFgKAMAAQALAAAHAIQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgLIACgIIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAHQgBARANALQANALAQAAQARAAANgNQANgNgBgRQAAgVgLgNQgLgOgVgBQgbABgKASIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARARABAdQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgEAAgGIgBgDQgBAEgFADQgEADgGAAQgNgBgDgLIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1337.setTransform(-33.1,-11.2);

	this.shape_1338 = new cjs.Shape();
	this.shape_1338.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1338.setTransform(-45.5,-9.8);

	this.shape_1339 = new cjs.Shape();
	this.shape_1339.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgBgNgLQgPgKAAgUQAAgJAFgJQAFgIAIgDQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbAAAHAPQACgHAFgEQAGgEAHAAIAMAAIAAALIgCAAQgEAAAAADQgBACAEADIAIAHQAEAFABAHQAAALgKAHQgJAEgMgBIAAAZIAKgCIAIgEIAAAMIgIADIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAEIAMACIANgBIAAg0IgaAAQgKAAgHAIgAAdgdIAAATIACAAQAFAAADgCQAEgDAAgFQAAgFgFgHQgFgHAAgEIAAgBQgEABAAAOgAgXgvQACADAAAEQAAADgCABIACAAIADAAIAZAAQgCgHgIgDQgGgCgGAAIgIABg");
	this.shape_1339.setTransform(-57.7,-8.7);

	this.shape_1340 = new cjs.Shape();
	this.shape_1340.graphics.f("#FFFFFF").s().p("AgsAWQgHgDAAgNIABgGQAHgVAsAAQAVAAAMADQAQAFACANQABAJgGAFQgGAFgJAAQgIAAgFgEQgGgEAAgIQAAgDABgEIgOAAQghAAgFAKIgBAEQAAAIAHAEgAAaABQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQADACACAAQAHAAAAgHQAAAAAAgBQAAAAgBAAQAAgBAAAAQAAgBgBAAQgCgCgDAAQgGAAAAAFg");
	this.shape_1340.setTransform(-76.3,-16.1);

	this.shape_1341 = new cjs.Shape();
	this.shape_1341.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1341.setTransform(-74.6,-8.4);

	this.shape_1342 = new cjs.Shape();
	this.shape_1342.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1342.setTransform(-85.8,-8.5);

	this.shape_1343 = new cjs.Shape();
	this.shape_1343.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKAAQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABADgCAEIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1343.setTransform(-97.7,-11.1);

	this.shape_1344 = new cjs.Shape();
	this.shape_1344.graphics.f("#FFFFFF").s().p("AAdA3QgHgHAAgLIABgFIALAAIAAACQAAAPANAAQAIAAADgNQACgHAAgQQAAgNgBgKQgDgMgFAAQgIAAAAALIAAAYIgMAAIAAgYQAAgMgIAAQgDAAgCACIgDAFQAKALgBATQAAAWgNANQgNAMgWABQgJAAgNgEQgNgFgEAAQgFAAgGAHIgIgGIARgaQgHgJAAgPIABgLQAEgPAOgIQAKgHAOAAIAIAAIAQgYIANAAIgQAaIAIAEIAHAFQACgEADgCQAFgEAIgBQAIABAGAGQAHgGAJgBQAbACABAtQAAAVgHAOQgKARgSAAQgMAAgHgHgAgwAtQAJAEAJAAIAJgBQAMgCAHgJQAHgKAAgMQgBgMgHgIQgHgJgMgBIgLAPIAGAAQAHAAAFAEQAFAEABAHQABAHgFAHQgEAGgHACQgGACgHAAQgLAAgIgEIgGAIIAEAAQAFAAAFACgAgxAZIAGACQAHACADgCQgGgBgDgEQgBgEABgDgAggAPQAAAGAFAAQAGAAAAgGQAAgFgGAAQgFAAAAAFgAg2gEQgEAFAAAIIABAHIAVgeQgMACgGAIg");
	this.shape_1344.setTransform(-118.2,-11);

	this.shape_1345 = new cjs.Shape();
	this.shape_1345.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgQANgGQgGgDAAgJIABgGQAGgNAUAAQARAAAGALQAIgLARAAQAKAAAHAGQAIAFAAAKQAAAHgDADQALAKAAAMQAAAVgPAMQgQANgbAAQgbAAgQgOgAgqADQAAAMAPAGQAMAFAPAAQARAAALgFQAPgGABgNQAAgFgEgFQgGAEgMADQAFACAAAHQAAAGgIADQgHADgKAAQgLAAgHgDQgIgEAAgHQAAgMAVAAIAKgBQAOgCADgCIgyAAQgRAAABAOgAgJAGQAAAEAKAAQAKAAAAgEQAAgEgKAAQgKAAAAAEgAAMghQgFAEAAAHIAcAAIABgEIgBgFQgDgGgJAAQgHAAgEAEgAgegkQAEACAAAEQABAEgDAEIAWAAQAAgLgMgEIgGgBQgDAAgDACg");
	this.shape_1345.setTransform(-132.4,-9.8);

	this.shape_1346 = new cjs.Shape();
	this.shape_1346.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1346.setTransform(-140.7,-16.5);

	this.shape_1347 = new cjs.Shape();
	this.shape_1347.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1347.setTransform(-145.3,-9.8);

	this.shape_1348 = new cjs.Shape();
	this.shape_1348.graphics.f("#FFFFFF").s().p("AgCBBQgWgBgNgNQgMgNgBgUQAAgSALgNQgDgBgDgEQgEgEgBgEIgCgKQAAgJAJgKQAIgKAPAAIAJABQAKADABAHIAFgIIAVAAQgFAIgHAIQgIAIgIAFQAOgBALAEQAeALAAAiQAAAWgRAPQgPAOgVAAIgCgBgAgagLQgJAKAAALQAAAMAIAIQALALAQAAQAOAAAKgHQANgKAAgPQAAgHgDgGQgEgJgJgFQgIgEgLAAQgSAAgKALgAgigpQgDAGAAAEQAAAHAFADIADgEIAKgJQgFABgEgEQgEgDABgEIgDADgAgWgwQAAAHAGAAQAHAAAAgHQAAgGgHAAQgGAAAAAGg");
	this.shape_1348.setTransform(-157.5,-11.3);

	this.shape_1349 = new cjs.Shape();
	this.shape_1349.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1349.setTransform(-169,-9.8);

	this.shape_1350 = new cjs.Shape();
	this.shape_1350.graphics.f("#FFFFFF").s().p("AgzAwQgJgOAAgTQABgTANgOQANgOATgBQAQAAAKAJQAKAJAAAPQgBAHgDAFIgKAKQgFAEgBAJIABADIAFgBIAGABIAGAAQANAAAHgOQAFgMABgOQAAgUgKgNQgMgQgcAAQgbAAgJAUIgMAAQAEgPAOgIQAOgIASgBQAZAAAQAPQAUASACAeQAAAXgIATQgKAVgRAAQgGAAgDgBIgGgGQgDgDgCABQgDAAAAAFIABADIgLAAIAAgJQgIALgKgBIgBABQgQAAgJgQgAgfAkQgCADgBADQABADACACQACADAEAAQADAAADgDQACgBAAgEQAAgDgCgDQgDgCgDAAQgEAAgCACgAgfgHQgIAFgEAGQgEAHAAAJQAAAJAEAHQAEgOAOAAQANAAAEALQABgLAEgFIAGgHQAEgDAAgEQgBgGgGgEQgGgDgJAAQgIAAgIADg");
	this.shape_1350.setTransform(-187.3,-11.1);

	this.shape_1351 = new cjs.Shape();
	this.shape_1351.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAGgHIAGgKIAABDg");
	this.shape_1351.setTransform(-199.6,-4.9);

	this.shape_1352 = new cjs.Shape();
	this.shape_1352.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgKQgKgQAAgWQAAgOAIgNQAJgNALABQALABAEAIQAFgKAMAAQALAAAHAIQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgLIACgIIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAHQgBARANALQANALAQAAQARAAANgNQANgNgBgRQAAgVgLgNQgLgOgVgBQgbABgKASIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARARABAdQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgEAAgGIgBgDQgBAEgFADQgEADgGAAQgNgBgDgLIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1352.setTransform(-199.8,-11.2);

	this.shape_1353 = new cjs.Shape();
	this.shape_1353.graphics.f("#FFFFFF").s().p("AgmA1QgQgQAAgWQAAgPAHgMQAHgNANABQAMAAACAKQAFgMAMAAQALAAAGAJQAIAIgBAKQAAAMgIAJQgJAJgLAAQgKAAgIgGQgIgHAAgJIACgOQABgHgGAAQgFAAgDAHQgDAGAAAHQAAARANAJQALAJAQAAQARABALgMQAMgMAAgRQAAgMgFgKQgHgKgMAAIhCAAIAAgLQARACAAgCQAAAAAAgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgEgDAAgDQAAgNAOgEQAIgCASAAQAOAAAJACQAMAFAJAJQAGAHAAALIgMAAQgBgKgLgHQgLgEgPAAIgOABQgHACAAAHQAAAAAAAAQAAABAAAAQABAAAAABQAAAAABABIAEABIAfAAQAJAAAIADIAFAEQARAMAAAeQAAAagQAQQgPARgYAAQgXAAgPgPgAgOACQAAAGAFAEQAEADAFAAQAHABAEgEQAGgEAAgHQgGAHgIAAQgJABgHgKIgBADgAAAgNQAAAAAAABQgBAAAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAQAAABAAAAQABABAAAAQABAAAAAAQABAAAAAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAABgBQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAABAAAAgAAmghIAAAAgAAhglQgIgDgJAAIgfAAIgEgBQgBgBAAAAQAAgBgBAAQAAAAAAgBQAAAAAAAAQAAgHAHgCIAOgBQAPAAALAEQALAHABAKIgFgEg");
	this.shape_1353.setTransform(-211.5,-11.6);

	this.shape_1354 = new cjs.Shape();
	this.shape_1354.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1354.setTransform(-223.7,-9.8);

	this.shape_1355 = new cjs.Shape();
	this.shape_1355.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgKQgKgQAAgWQAAgOAIgNQAJgNALABQALABAEAIQAFgKAMAAQALAAAHAIQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgLIACgIIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAHQgBARANALQANALAQAAQARAAANgNQANgNgBgRQAAgVgLgNQgLgOgVgBQgbABgKASIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARARABAdQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgEAAgGIgBgDQgBAEgFADQgEADgGAAQgNgBgDgLIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1355.setTransform(-242,-11.2);

	this.shape_1356 = new cjs.Shape();
	this.shape_1356.graphics.f("#FFFFFF").s().p("AgUApQgJgHAAgMQAAgMALgKQAHgGAMgHIgbAAIAAgLIAfgLIgfAAIAAgMIA4AAIAAANIgbAKIAaAAIAAAKQgPAGgLAIIgHAGQgEAFABAGQABAIAJAAQAFAAADgDQAEgDABgEQABgGgEgFIAMAAQAFAIAAAKQgBALgIAHQgIAIgMAAQgMAAgJgHg");
	this.shape_1356.setTransform(-251.1,-9.6);

	this.shape_1357 = new cjs.Shape();
	this.shape_1357.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1357.setTransform(-260.8,-9.8);

	this.shape_1358 = new cjs.Shape();
	this.shape_1358.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1358.setTransform(-275.2,-16.5);

	this.shape_1359 = new cjs.Shape();
	this.shape_1359.graphics.f("#FFFFFF").s().p("AgoA3QgQgKAAgTQAAgRALgHIgLAAIAAgJIAPAAQgEgCAAgFQAAgGAFgEQAJgGAPABQARABAHALQAEAHABAMIgcAAQgXAAgCAPQgBAMANAHQAMAFAPgBQAQgBALgJQALgJADgQIABgHQAAgOgIgMQgHgLgLgFIgGAFIgGAFQgHADgJAAQgMAAgEgGQgEgEADgIQABgDgHAAIgEABIAAgLIATgBQAOAAAIACQAWAEAQAQQARASABAcQAAAZgRARQgQARgaAAQgXAAgPgJgAgXgRQAAAAABAAQAAABABAAQAAABAAAAQAAABAAABQABADgDACIAWAAQgBgGgJgDIgHgBIgFABgAgTguQABAEAHAAQAKAAADgHQgHgDgOAAIAAAGg");
	this.shape_1359.setTransform(-278.9,-11.2);

	this.shape_1360 = new cjs.Shape();
	this.shape_1360.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgHADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1360.setTransform(140.2,-40.3);

	this.shape_1361 = new cjs.Shape();
	this.shape_1361.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1361.setTransform(135.5,-33.5);

	this.shape_1362 = new cjs.Shape();
	this.shape_1362.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1362.setTransform(122.6,-34.9);

	this.shape_1363 = new cjs.Shape();
	this.shape_1363.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAHgHIAFgKIAABDg");
	this.shape_1363.setTransform(110.7,-28.7);

	this.shape_1364 = new cjs.Shape();
	this.shape_1364.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgKQgKgQAAgWQAAgPAIgMQAJgNALABQALABAEAIQAFgKAMAAQALABAHAHQAIAIABALQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgLIACgIIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGABgDAHQgDAHAAAHQgBASANAKQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgMQgLgPgVAAQgbAAgKATIgNAAQAGgOAOgIQANgIARAAQAaAAAPAPQARASABAcQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1364.setTransform(110.5,-34.9);

	this.shape_1365 = new cjs.Shape();
	this.shape_1365.graphics.f("#FFFFFF").s().p("AgVApQgJgIAAgOQAAgLAEgHQADgFAHgHQAHgJAUgPIgmAAIAAgMIA4AAIAAALIgNAKQgHAEgEAGQgIAJgDAEQgEAHAAAIQAAAHAFACQADAEAEAAQALgBADgIIABgGQAAgDgCgEIALAAQAGAIgBAKQgBAMgJAHQgJAIgMAAQgNAAgIgHg");
	this.shape_1365.setTransform(101.2,-33.3);

	this.shape_1366 = new cjs.Shape();
	this.shape_1366.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1366.setTransform(93.4,-32.1);

	this.shape_1367 = new cjs.Shape();
	this.shape_1367.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1367.setTransform(79.8,-40.3);

	this.shape_1368 = new cjs.Shape();
	this.shape_1368.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1368.setTransform(75,-33.5);

	this.shape_1369 = new cjs.Shape();
	this.shape_1369.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1369.setTransform(61.9,-33.5);

	this.shape_1370 = new cjs.Shape();
	this.shape_1370.graphics.f("#FFFFFF").s().p("AgnA1QgPgPAAgXQAAgPAGgMQAIgNANABQAMAAACAKQAFgMAMAAQALAAAGAJQAIAIAAAKQAAANgJAIQgIAJgMAAQgKAAgIgGQgJgGABgKIACgPQACgGgIAAQgEAAgEAHQgCAGAAAHQAAAQANALQALAIAQABQARAAAMgMQALgMAAgRQAAgNgFgIQgHgLgNAAIhBAAIAAgLQARABAAgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQgBAAAAAAQgEgCAAgEQAAgNAPgEQAHgCASAAQAOAAAJACQANAFAIAJQAGAHAAALIgLAAQgCgKgLgHQgLgEgOAAIgPABQgHACAAAHQAAAAAAAAQAAABABAAQAAAAAAABQABAAAAAAIAEACIAeAAQALAAAGAEIAHADQAQAMAAAeQAAAagQAQQgPARgYAAQgXAAgQgPgAgOACQABAGAEADQAEAEAGAAQAGABAEgFQAFgDABgHQgGAHgIAAQgJABgHgKIgBADgAAAgNQAAAAAAABQgBAAAAABQAAAAAAABQgBAAAAABQAAABABAAQAAABAAAAQAAABABAAQAAABAAAAQAAABAAAAQABABAAAAQABAAAAAAQABAAAAAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAABgBQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAABAAAAgAAnghIAAAAgAAggkQgGgEgLAAIgeAAIgEgCQAAAAgBAAQAAgBAAAAQgBAAAAgBQAAAAAAAAQAAgHAHgCIAPgBQAOAAALAEQALAHACAKIgHgDg");
	this.shape_1370.setTransform(50.2,-35.3);

	this.shape_1371 = new cjs.Shape();
	this.shape_1371.graphics.f("#FFFFFF").s().p("AgmBCIAAgTIBHAAIAAgRQgOAFgWgBQgUgBgLgLQgLgLAAgQQAAgUARgKQgCAAgDgDQgCgEAAgEIABgEQACgKALgDQAHgCAOAAQAVAAAKANQAIAJAAAPIgiAAQgLAAgHAGQgHAFgBAKQgBALAKAJQAKAJAMgBQAWABAOgIIAAA0gAgNg0QAEACAAAFQABAFgEACIAGAAIAeAAQgBgIgLgFQgEgCgIAAIgEAAIgJABg");
	this.shape_1371.setTransform(39.6,-31.9);

	this.shape_1372 = new cjs.Shape();
	this.shape_1372.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1372.setTransform(28.5,-33.6);

	this.shape_1373 = new cjs.Shape();
	this.shape_1373.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgCgNgJQgPgMAAgTQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAPQACgGAFgFQAGgEAHAAIAMAAIAAALIgCAAQgEAAAAADQgBACAEADIAIAHQAEAGABAGQAAALgKAHQgJAEgMgBIAAAZIAKgCIAIgDIAAALIgIADIgKACIAAAYgAgbgRQgGAIAAAJQAAAWAYAEIAMACIANgBIAAgzIgaAAQgKgBgHAIgAAdgeIAAAUIACAAQAFAAADgCQAEgDAAgFQAAgFgFgHQgFgHAAgDIAAgCQgEABAAANgAgXgvQACAEAAADQAAADgCABIACAAIADAAIAZAAQgCgHgIgDQgGgCgGAAIgIABg");
	this.shape_1373.setTransform(16.2,-32.5);

	this.shape_1374 = new cjs.Shape();
	this.shape_1374.graphics.f("#FFFFFF").s().p("AgpAwQgPgPABgYQAAgRAMgMQANgMARAAIANAAIAAALIgLAAQgNAAgIAIQgIAIAAALQgBAPANAKQAMAIAQAAQASABAMgNQAMgNAAgSQAAgTgMgNQgMgPgSAAQgNAAgLAFQgKAGgFAJIgLAAQAFgPANgIQAOgJASABQAZAAAQASQAQATAAAaQAAAagQATQgQARgaABQgZgBgPgPg");
	this.shape_1374.setTransform(-2,-34.9);

	this.shape_1375 = new cjs.Shape();
	this.shape_1375.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1375.setTransform(-14.6,-33.5);

	this.shape_1376 = new cjs.Shape();
	this.shape_1376.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1376.setTransform(-27.5,-33.6);

	this.shape_1377 = new cjs.Shape();
	this.shape_1377.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1377.setTransform(-39.8,-33.5);

	this.shape_1378 = new cjs.Shape();
	this.shape_1378.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1378.setTransform(-52.1,-39.8);

	this.shape_1379 = new cjs.Shape();
	this.shape_1379.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAJgFQAHgEADgJQAEgKgBgJQgBgLgHgIQgIgJgLABQgSAAgEARQACgDAFAAQAGAAADADQAKAHAAAQQAAARgOAJQgMAJgSAAQgSABgMgMQgMgLAAgQQAAgSANgJQgDAAgDgFQgCgEAAgEQAAgJAHgFQAIgGAJAAQAIABAHAFQAGAFACAJQALgSAWgCQASAAAMAQQAMAPAAATQAAAVgOAOQgOAOgWgBgAgrgGQgFAFAAAHQAAAKAKAFQAIAFALABQALAAAIgFQAIgFAAgHQABgFgCgDQgCgCgEAAQgCAAgBACQgCABAAADIgMAAIAAgJIABgIIgOAAQgIABgGAEgAgogjQADABACAEQACAFgCADIAMAAQgBgHgEgEQgDgDgGAAIgDABg");
	this.shape_1379.setTransform(-52.5,-33.5);

	this.shape_1380 = new cjs.Shape();
	this.shape_1380.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1380.setTransform(-65.4,-39.8);

	this.shape_1381 = new cjs.Shape();
	this.shape_1381.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1381.setTransform(-66,-33.5);

	this.shape_1382 = new cjs.Shape();
	this.shape_1382.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1382.setTransform(-81.4,-33.5);

	this.shape_1383 = new cjs.Shape();
	this.shape_1383.graphics.f("#FFFFFF").s().p("AgoA0QgQgOACgaQABgQALgKQAMgLASAAIAQAAIAAALIgOAAQgNAAgIAHQgJAGAAAMQAAAOAMAIQAMAJAQgBQATgBAKgLQAKgMAAgSIAAgGQgDgXgPgMQgLAMgRAAQgRABgGgLQgCgBAAgFQAAgFACgCIgHAAIAAgLIANgBQAcABAQAHQASAJAJASQAJATgBAWQgBAXgPAPQgQAQgYABQgZAAgOgOgAgRgyQAAADADACQADADAEAAQALAAAFgHQgGgBgHgBIgMgBIgBACg");
	this.shape_1383.setTransform(-89.3,-35.1);

	this.shape_1384 = new cjs.Shape();
	this.shape_1384.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1384.setTransform(-97.3,-40.3);

	this.shape_1385 = new cjs.Shape();
	this.shape_1385.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1385.setTransform(-101.5,-33.5);

	this.shape_1386 = new cjs.Shape();
	this.shape_1386.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1386.setTransform(-113.5,-34.9);

	this.shape_1387 = new cjs.Shape();
	this.shape_1387.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgCgNgJQgPgMAAgTQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAPQACgGAFgFQAGgEAHAAIAMAAIAAALIgCAAQgEAAAAADQgBACAEADIAIAHQAEAGABAGQAAALgKAHQgJAEgMgBIAAAZIAKgCIAIgDIAAALIgIADIgKACIAAAYgAgbgRQgGAIAAAJQAAAWAYAEIAMACIANgBIAAgzIgaAAQgKgBgHAIgAAdgeIAAAUIACAAQAFAAADgCQAEgDAAgFQAAgFgFgHQgFgHAAgDIAAgCQgEABAAANgAgXgvQACAEAAADQAAADgCABIACAAIADAAIAZAAQgCgHgIgDQgGgCgGAAIgIABg");
	this.shape_1387.setTransform(-125.6,-32.5);

	this.shape_1388 = new cjs.Shape();
	this.shape_1388.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1388.setTransform(-140.5,-33.5);

	this.shape_1389 = new cjs.Shape();
	this.shape_1389.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1389.setTransform(-148.8,-33.6);

	this.shape_1390 = new cjs.Shape();
	this.shape_1390.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1390.setTransform(-161.1,-39.8);

	this.shape_1391 = new cjs.Shape();
	this.shape_1391.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1391.setTransform(-161.7,-33.5);

	this.shape_1392 = new cjs.Shape();
	this.shape_1392.graphics.f("#FFFFFF").s().p("AgiA/QgWgBgNgNQgNgOAAgUQAAgNAFgKQAFgJALgLIAWgUQAIgJACgFIAVAAQgEAHgGAHQgGAGgKAHQAPgBALAEQAcAKAAAjQAAAWgQAOQgOAOgVAAIgDAAgAg6gNQgJAJAAAMQAAAMAIAIQALALARAAQAOAAAKgIQALgJAAgQQAAgGgCgGQgDgJgJgFQgIgFgMAAQgSAAgKAMgAAeA0QgIgKAAgQQAAgVASgbQAHgMANgQIg+AAIAAgLIBNAAIAAAKQgNAPgJANQgOAXAAATQAAAUARAAQAGAAAEgDQAEgEAAgFQAAgEgCgFIAKAAQAFAFAAALQAAANgJAIQgJAIgNAAQgOAAgIgLg");
	this.shape_1392.setTransform(-176.9,-34.8);

	this.shape_1393 = new cjs.Shape();
	this.shape_1393.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAHgHIAFgKIAABDg");
	this.shape_1393.setTransform(-197.6,-28.7);

	this.shape_1394 = new cjs.Shape();
	this.shape_1394.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAIgFQAIgEAEgJQADgKAAgJQgBgLgIgIQgIgJgLABQgSAAgEARQACgDAFAAQAFAAAEADQAKAHAAAQQAAARgOAJQgMAJgSAAQgSABgMgMQgMgLAAgQQAAgSANgJQgEAAgCgFQgCgEgBgEQAAgJAIgFQAIgGAJAAQAJABAGAFQAGAFACAJQALgSAWgCQATAAAMAQQALAPAAATQAAAVgOAOQgOAOgWgBgAgqgGQgGAFAAAHQAAAKAJAFQAJAFAMABQAKAAAIgFQAHgFACgHQAAgFgCgDQgCgCgDAAQgDAAgCACQgCABAAADIgKAAIgBgJIABgIIgPAAQgHABgFAEgAgogjQADABACAEQABAFgBADIAMAAQAAgHgEgEQgEgDgFAAIgEABg");
	this.shape_1394.setTransform(-198.2,-33.5);

	this.shape_1395 = new cjs.Shape();
	this.shape_1395.graphics.f("#FFFFFF").s().p("AAEA/QgEgCgCgFQgGAJgSAAQgUAAgJgWQgFgPABgVQABgPAJgLQAJgMAOAAQAGAAAEAEQAEADAAAHQAAACgDAEQgDAFABADQAAAKAOABQAEAAAFgDQAFgCABgDIgHAAQgFAAgFgDQgFgDgBgGQgBgIAGgGQAFgFAHAAQAVAAAAAYQAAAKgIAIQgIAHgLABQgKABgIgEQgIgEgDgJIgBgFQAAgHADgDQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAAAAAgBQgBgBAAAAQAAAAgBgBQAAAAgBAAQgGgBgGAJQgFAKgBAJQAAAMAGAKQAHALAKAAQAQAAAAgSIAKAAQAAAKAFAEQAEAEAJAAQANAAAHgOQAFgKAAgQQABgUgLgPQgOgSgVAAQgkAAgHAYIgMAAQACgPAPgKQAPgKAXAAQAeABAQAXQANASAAAWQABAcgJARQgKAUgWAAQgLAAgFgCgAAAgPQAAAGAGAAQABAAABAAQAAAAABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAgBAAQgGAAAAAHg");
	this.shape_1395.setTransform(-211.2,-35.1);

	this.shape_1396 = new cjs.Shape();
	this.shape_1396.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1396.setTransform(-229.4,-34.9);

	this.shape_1397 = new cjs.Shape();
	this.shape_1397.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBYAAIAAgfIAFgHIAGgKIAABDg");
	this.shape_1397.setTransform(-241.4,-28.7);

	this.shape_1398 = new cjs.Shape();
	this.shape_1398.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1398.setTransform(-241.9,-33.5);

	this.shape_1399 = new cjs.Shape();
	this.shape_1399.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1399.setTransform(-254.1,-33.5);

	this.shape_1400 = new cjs.Shape();
	this.shape_1400.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1400.setTransform(-262.4,-40.3);

	this.shape_1401 = new cjs.Shape();
	this.shape_1401.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1401.setTransform(-266.7,-33.5);

	this.shape_1402 = new cjs.Shape();
	this.shape_1402.graphics.f("#FFFFFF").s().p("AgdA7QgNgHABgNQABgHAGgDQAGgDAHAAIAGAAIAAAKIgDAAQgHAAAAAEQAAAIAZgBQAHAAAHgCQAIgDAAgDQgBgFgNABIgMABIAAgLQAUgBAMgIQANgKAAgPIgBgHQgDgMgMgHQgLgGgOAAQgQAAgLAGQgNAIAAANQgBAMAIAGQAHAHALAAQAGAAAGgDQAGgDABgFQgEADgFAAQgHAAgGgEQgFgEAAgHQgBgKAGgFQAGgFAJAAQALAAAGAHQAHAHgBALQAAAMgKAJQgJAHgNABQgUACgLgLQgMgLAAgRQAAgYARgNQAQgMAYAAQAaABAPAOQANANAAAWQAAAcgbAPQAKAFgBAIQAAAKgLAGQgLAGgQABIgCAAQgQAAgLgGgAgIgTQgCACAAADQAAAEACACQADADADAAQACAAADgDQACgCAAgEQAAgDgCgCQgDgDgCAAQgEAAgCADg");
	this.shape_1402.setTransform(-278.9,-32);

	this.shape_1403 = new cjs.Shape();
	this.shape_1403.graphics.f("#FFFFFF").s().p("AgIALQgFgEAAgHQAAgEADgEQAEgFAGAAQAFAAAEADQAFAEAAAGQAAAFgDAEQgEAFgHAAQgEAAgEgDg");
	this.shape_1403.setTransform(-29.7,-64.6);

	this.shape_1404 = new cjs.Shape();
	this.shape_1404.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1404.setTransform(-35.2,-68.3);

	this.shape_1405 = new cjs.Shape();
	this.shape_1405.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKAAQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABADgCAEIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1405.setTransform(-43.3,-69.6);

	this.shape_1406 = new cjs.Shape();
	this.shape_1406.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1406.setTransform(-55.7,-68.3);

	this.shape_1407 = new cjs.Shape();
	this.shape_1407.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1407.setTransform(-64.4,-75);

	this.shape_1408 = new cjs.Shape();
	this.shape_1408.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIABAAQAJAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1408.setTransform(-68.7,-68.3);

	this.shape_1409 = new cjs.Shape();
	this.shape_1409.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAghAWgNQAIgFAMAAIAGABIAAALQAOgMASAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgJQgIAFgFACQgGADgKAAIgEgBgAAIgQQAQAFAAAUQAAAQgLAMQALACAHgFQANgIgBgVQAAgMgIgKQgIgJgMgBQgWgBgDAPQAFgEAHAAIAGABgAgogbQgGAJAAAMQAAANAHAJQAIAJANABQAKAAAIgFQAIgEAAgJQAAgFgCgDQgEgEgEAAQgEAAgCADQgCACAAAEIAAAEIgMAAQgCgLABgLQABgLAJgMIgHgBQgMAAgIAKg");
	this.shape_1409.setTransform(-81.6,-68.3);

	this.shape_1410 = new cjs.Shape();
	this.shape_1410.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1410.setTransform(-93.4,-69.6);

	this.shape_1411 = new cjs.Shape();
	this.shape_1411.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1411.setTransform(-105.6,-68.3);

	this.shape_1412 = new cjs.Shape();
	this.shape_1412.graphics.f("#FFFFFF").s().p("AgnA1QgPgPAAgXQAAgPAGgLQAIgNANAAQAMAAACAKQAFgMAMAAQALAAAGAJQAIAIgBAKQAAANgIAJQgJAIgLAAQgLAAgHgGQgIgGAAgKIACgOQACgHgIAAQgEAAgDAIQgDAFAAAHQAAARANAKQALAJAQAAQARAAALgMQAMgLAAgRQAAgNgFgJQgHgLgMAAIgsAAQgWAAAAgNQAAgNARgHQANgFAUAAQAdAAAPAQQAGAIAAAKIgMAAQgBgKgLgGQgLgFgOAAQgcAAAAAKQAAAAAAABQAAAAAAAAQABABAAAAQAAAAABABIAEABIAlAAQAJAAAHAEIAGADQARAMAAAeQAAAagQAQQgPARgYAAQgXAAgQgPgAgOACQABAGAEAEQAEAEAGAAQAGAAAEgEQAFgEABgHQgGAHgIAAQgJABgHgKIgBADgAAAgNQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQAAAAAAABQABAAAAAAQABAAAAABQABAAAAAAQABAAABAAQAAgBABAAQAAAAABAAQAAgBABAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAAAgBQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAQAAABAAAAgAAmghIAAAAgAAggkQgHgEgJAAIglAAIgEgBQgBgBAAAAQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAgKAcAAQAOAAALAFQALAGABAKIgGgDg");
	this.shape_1412.setTransform(-118,-70.1);

	this.shape_1413 = new cjs.Shape();
	this.shape_1413.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1413.setTransform(-130.1,-74.5);

	this.shape_1414 = new cjs.Shape();
	this.shape_1414.graphics.f("#FFFFFF").s().p("AAOAmQAKABAIgGQAJgEACgJQAEgKAAgJQgBgLgIgIQgIgJgLABQgSABgEAPQADgCAEAAQAGAAADACQALAIgBAQQAAAQgOAKQgMAJgSAAQgSABgMgLQgMgMAAgQQAAgSANgJQgDAAgDgFQgDgEABgEQgBgJAJgFQAGgGAKAAQAIABAHAFQAGAFACAJQALgTAWgBQASAAAMAQQAMAOAAAUQAAAVgOAOQgOAOgWgBgAgqgFQgGAEAAAHQAAAJAJAGQAJAFALABQALAAAIgFQAHgFABgHQABgEgCgEQgCgCgDAAQgDAAgCACQgBABAAADIgLAAIgBgJIABgIIgPAAQgHABgFAFgAgogjQADABACAEQABAFgBADIAMAAQAAgHgFgDQgDgEgGAAIgDABg");
	this.shape_1414.setTransform(-130.5,-68.3);

	this.shape_1415 = new cjs.Shape();
	this.shape_1415.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1415.setTransform(-147.7,-66.9);

	this.shape_1416 = new cjs.Shape();
	this.shape_1416.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1416.setTransform(-161.3,-75);

	this.shape_1417 = new cjs.Shape();
	this.shape_1417.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1417.setTransform(-165.9,-68.3);

	this.shape_1418 = new cjs.Shape();
	this.shape_1418.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1418.setTransform(-178.8,-68.3);

	this.shape_1419 = new cjs.Shape();
	this.shape_1419.graphics.f("#FFFFFF").s().p("AgDA4QgHAMgQAAQgPAAgJgMQgJgNAAgQQAAgcARgNQAKgHAcAAIAAALQgXAAgGAEQgHAFgCAEQgCAGAAAJQAAAKAEAIQAFAHAKAAQARAAAAgNIAAgPIALAAIAAAPQAAAOATAAQALAAAGgMQAGgJAAgNQAAgVgNgLQgIgHgMAAIguAAQgQAAAAgNQAAgZAxAAQAdAAAOAMQAIAIgBAMIgMAAIAAgCQAAgIgNgFQgKgEgNAAQgMAAgIADQgJACAAAFQgBAEAHAAIAlAAQARAAAMANQAQAPAAAbQAAAXgJAOQgLAQgSAAQgRAAgIgMg");
	this.shape_1419.setTransform(-191.1,-70.1);

	this.shape_1420 = new cjs.Shape();
	this.shape_1420.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1420.setTransform(-199.8,-68.3);

	this.shape_1421 = new cjs.Shape();
	this.shape_1421.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1421.setTransform(-208.1,-68.3);

	this.shape_1422 = new cjs.Shape();
	this.shape_1422.graphics.f("#FFFFFF").s().p("AgpAuQgTgOABgYQAAgTANgNQAGgGALgEQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAAAgBgBQgBgDAAgCQAAgEACgCQAFgJAKgCIAVgBQASAAALAJQANAIADARIgyAAQgQAAgKALQgIAIgBAPQAAAPAMAMQAMAMAPAAQAMgBAKgEQALgEAGgHQAHgLgBgJIALAAIAAAEQAAAJgFAJQgHAOgPAGQgOAIgSAAQgZAAgRgPgAgJguQACACAAADQAAAAAAABQAAABAAAAQAAABgBAAQAAABgBAAIApAAQgIgJgOgCIgIAAQgHAAgEACg");
	this.shape_1422.setTransform(-220.3,-67.1);

	this.shape_1423 = new cjs.Shape();
	this.shape_1423.graphics.f("#FFFFFF").s().p("AgoA0QgRgLABgWQABgTAPgFQgIgCAAgJQAAgLALgFQAIgDALAAQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHABAKAAQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQATgaABQgZAAgPgMgAgbgXQADACABAEQABADgCAEIAVAAQgBgFgEgEQgFgEgHAAIgHAAg");
	this.shape_1423.setTransform(-238.6,-69.6);

	this.shape_1424 = new cjs.Shape();
	this.shape_1424.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1424.setTransform(-249.9,-69.6);

	this.shape_1425 = new cjs.Shape();
	this.shape_1425.graphics.f("#FFFFFF").s().p("AgPAwQgFgBgFgGQgEgFAAgHIABgGIALAAIAAAEQAAAFADADQADADAGAAQAHAAAFgLQADgJAAgNQAAglgIAAQgFAAAAAFQAAAAAAABQAAAAAAABQAAAAABAAQAAABABAAQAGAIAAAIQAAASgSAAQgSAAAAgcQAAgQAGgOIAOAAQgEAFgDAIQgCAHAAAGQAAAPAIAAIAEgCQABgBAAAAQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAAAgBQgHgFAAgIQAAgEACgEQACgDADgDQAFgFAJAAQAOAAAHASQAFAOAAATQAAATgJAOQgKAPgPAAQgIAAgFgCg");
	this.shape_1425.setTransform(-258.7,-68.3);

	this.shape_1426 = new cjs.Shape();
	this.shape_1426.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAghAWgNQAIgFAMAAIAGABIAAALQAOgMASAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgJQgIAFgFACQgGADgKAAIgEgBgAAIgQQAQAFAAAUQAAAQgLAMQALACAHgFQANgIgBgVQAAgMgIgKQgIgJgMgBQgWgBgDAPQAFgEAHAAIAGABgAgogbQgGAJAAAMQAAANAHAJQAIAJANABQAKAAAIgFQAIgEAAgJQAAgFgCgDQgEgEgEAAQgEAAgCADQgCACAAAEIAAAEIgMAAQgCgLABgLQABgLAJgMIgHgBQgMAAgIAKg");
	this.shape_1426.setTransform(-268.6,-68.3);

	this.shape_1427 = new cjs.Shape();
	this.shape_1427.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1427.setTransform(-279.9,-68.3);

	this.shape_1428 = new cjs.Shape();
	this.shape_1428.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1428.setTransform(58,-92);

	this.shape_1429 = new cjs.Shape();
	this.shape_1429.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1429.setTransform(49.9,-90.7);

	this.shape_1430 = new cjs.Shape();
	this.shape_1430.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1430.setTransform(38,-93.4);

	this.shape_1431 = new cjs.Shape();
	this.shape_1431.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1431.setTransform(27.2,-92);

	this.shape_1432 = new cjs.Shape();
	this.shape_1432.graphics.f("#FFFFFF").s().p("AgdA3QgPgKgBgUQgCgQALgJIAIgFIgFgDQgCgEABgFQgDAAgFADQgEAEgDAFQgEAHAAANQAAAMAGANQAGAOAMAKIgPAAQgRgOgEgYIgBgLQAAgMAEgJQAEgLAHgFQAJgGALAAIADAAIAAADQAIgFAOABQARABAHALQAEAHABAMIgcAAQgXAAgCAPQgBAMANAHQAMAFAOgBQARgBALgJQALgJADgQIABgHQAAgOgIgMQgHgLgLgFIgGAFIgGAFQgIADgIAAQgMAAgEgGQgEgEADgIQABgDgHAAIgEABIAAgLIAUgBQAMAAAJACQAXAEAPAQQARASABAcQAAAZgRARQgQARgbAAQgWAAgOgJgAgNgRQACABABADQAAADgDACIAVAAQgBgGgIgDIgHgBIgFABgAgIguQAAAEAHAAQAJAAAEgHQgIgDgNAAIABAGg");
	this.shape_1432.setTransform(9.3,-93.5);

	this.shape_1433 = new cjs.Shape();
	this.shape_1433.graphics.f("#FFFFFF").s().p("AgRARQgHgGgDgKIgBgEQAAgLAJgIIAAANIAEAAQAAAGAFAFQAFAFAGAAQAHAAAFgGQAEgFAAgHIALAAIAAAFQAAAMgIAIQgHAJgNAAQgJAAgIgGg");
	this.shape_1433.setTransform(-0.7,-86);

	this.shape_1434 = new cjs.Shape();
	this.shape_1434.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1434.setTransform(-4.6,-92);

	this.shape_1435 = new cjs.Shape();
	this.shape_1435.graphics.f("#FFFFFF").s().p("AgVApQgJgIAAgOQAAgLAEgHQADgFAHgHQAHgJAUgPIgmAAIAAgMIA4AAIAAALIgNAKQgHAEgEAGQgIAJgDAEQgEAHAAAIQAAAHAFACQADAEAEAAQALgBADgIIABgGQAAgDgCgEIALAAQAGAIgBAKQgBAMgJAHQgJAIgMAAQgNAAgIgHg");
	this.shape_1435.setTransform(-14.9,-91.8);

	this.shape_1436 = new cjs.Shape();
	this.shape_1436.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgCgNgJQgPgMAAgTQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAPQACgGAFgFQAGgEAHAAIAMAAIAAALIgCAAQgEAAAAADQgBACAEADIAIAHQAEAGABAGQAAALgKAHQgJAEgMgBIAAAZIAKgCIAIgDIAAALIgIADIgKACIAAAYgAgbgRQgGAIAAAJQAAAWAYAEIAMACIANgBIAAgzIgaAAQgKgBgHAIgAAdgeIAAAUIACAAQAFAAADgCQAEgDAAgFQAAgFgFgHQgFgHAAgDIAAgCQgEABAAANgAgXgvQACAEAAADQAAADgCABIACAAIADAAIAZAAQgCgHgIgDQgGgCgGAAIgIABg");
	this.shape_1436.setTransform(-24.4,-91);

	this.shape_1437 = new cjs.Shape();
	this.shape_1437.graphics.f("#FFFFFF").s().p("AgpAwQgPgPABgYQAAgRAMgMQANgMARAAIANAAIAAALIgLAAQgNAAgIAIQgIAIAAALQgBAPANAKQAMAIAQAAQASABAMgNQAMgNAAgSQAAgTgMgNQgMgPgSAAQgNAAgLAFQgKAGgFAJIgLAAQAFgPANgIQAOgJASABQAZAAAQASQAQATAAAaQAAAagQATQgQARgaABQgZgBgPgPg");
	this.shape_1437.setTransform(-42.7,-93.4);

	this.shape_1438 = new cjs.Shape();
	this.shape_1438.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1438.setTransform(-54.7,-92.1);

	this.shape_1439 = new cjs.Shape();
	this.shape_1439.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1439.setTransform(-66.7,-90.7);

	this.shape_1440 = new cjs.Shape();
	this.shape_1440.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1440.setTransform(-75.2,-92);

	this.shape_1441 = new cjs.Shape();
	this.shape_1441.graphics.f("#FFFFFF").s().p("AgFAwIgLgFQgGgCgFAAQgJAAABAIIgLAAQgCgIAAgJQAAgKACgHQADgIAJgHIAOgLQAIgHAAgHQABgEgDgEQgDgEgEAAQgGAAgDADQgEADAAAEQAAALAIAEIgQAAQgFgGAAgHIAAgEQABgJAHgFQAHgGALAAQAJAAAHAHQAGAGAAAJQAAAJgFAIIgNALIgNALQgGAHAAAJIABADIAIgBQAFAAAKADQAJADAGAAQAFAAAEgCQAGgBAFgFQAGgGAAgIQABgIgHgGIgMgNQgGgHAAgIQAAgKAGgFQAHgGAJAAQAJAAAGAFQAFAFABAIQABAGgDAEQgDAFgFABQgEAAgEgDQgDgCgBgEQAAgIAHgCQgCgDgEAAQgEAAgDADQgDACAAAEQAAAJAOAHQASAIAAASQAAAQgMAKQgLAKgQAAQgJAAgGgCg");
	this.shape_1441.setTransform(-82.5,-92);

	this.shape_1442 = new cjs.Shape();
	this.shape_1442.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1442.setTransform(-92.1,-90.6);

	this.shape_1443 = new cjs.Shape();
	this.shape_1443.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1443.setTransform(-103,-93.5);

	this.shape_1444 = new cjs.Shape();
	this.shape_1444.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1444.setTransform(-120.6,-93.4);

	this.shape_1445 = new cjs.Shape();
	this.shape_1445.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1445.setTransform(-128.8,-98.8);

	this.shape_1446 = new cjs.Shape();
	this.shape_1446.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAgYALgNQAFgGAGgDQAIgFAJAAIAJABIAAALQANgMATAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgKIgNAIQgHADgKAAIgDgBgAANAlIAFABQAGAAACgEIAAgDIgDgLQgCAJgIAIgAAiAOIACAIQACAFAAADQAHgHAAgKQAAgFgFAAQgGAAAAAGgAglgdQgIAIgBALQgBAOAHAKQAHALAPABQAKAAAHgFQAIgFABgIQAAgFgDgDQgDgFgEAAQgEABgCACQgDACAAAEIABAFIgMAAQgDgMACgLQABgLAJgLIgGgBQgKAAgIAIgAAHgQQAOAFACANQADgFAHgBQAHgBADADQgBgKgHgGQgIgIgMgBQgJAAgGADQgJAEgBAIQADgFAIAAIAGABg");
	this.shape_1446.setTransform(-133.1,-92);

	this.shape_1447 = new cjs.Shape();
	this.shape_1447.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1447.setTransform(-145.2,-93.5);

	this.shape_1448 = new cjs.Shape();
	this.shape_1448.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1448.setTransform(-159.1,-98.8);

	this.shape_1449 = new cjs.Shape();
	this.shape_1449.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1449.setTransform(-159.5,-92);

	this.shape_1450 = new cjs.Shape();
	this.shape_1450.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAIgFQAJgEACgJQAEgKAAgJQgBgLgIgIQgIgJgLABQgSAAgEARQACgDAFAAQAFAAAEADQAKAHAAAQQAAARgOAJQgMAJgSAAQgSABgMgMQgMgLAAgQQAAgSANgJQgEAAgCgFQgCgEgBgEQAAgJAJgFQAHgGAJAAQAIABAHAFQAGAFACAJQALgSAWgCQATAAALAQQAMAPAAATQAAAVgOAOQgOAOgWgBgAgqgGQgGAFAAAHQAAAKAJAFQAJAFALABQALAAAIgFQAHgFACgHQAAgFgCgDQgCgCgDAAQgCAAgDACQgCABAAADIgKAAIgBgJIABgIIgPAAQgHABgFAEgAgogjQADABACAEQABAFgBADIAMAAQAAgHgFgEQgDgDgGAAIgDABg");
	this.shape_1450.setTransform(-168.2,-92);

	this.shape_1451 = new cjs.Shape();
	this.shape_1451.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1451.setTransform(-179.7,-92);

	this.shape_1452 = new cjs.Shape();
	this.shape_1452.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAYAAQAJAAAFAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1452.setTransform(-193,-98.8);

	this.shape_1453 = new cjs.Shape();
	this.shape_1453.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1453.setTransform(-197.2,-92);

	this.shape_1454 = new cjs.Shape();
	this.shape_1454.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1454.setTransform(-208.1,-92);

	this.shape_1455 = new cjs.Shape();
	this.shape_1455.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1455.setTransform(-219.8,-92);

	this.shape_1456 = new cjs.Shape();
	this.shape_1456.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1456.setTransform(-231.4,-92);

	this.shape_1457 = new cjs.Shape();
	this.shape_1457.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1457.setTransform(-244.6,-98.8);

	this.shape_1458 = new cjs.Shape();
	this.shape_1458.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1458.setTransform(-249.4,-92);

	this.shape_1459 = new cjs.Shape();
	this.shape_1459.graphics.f("#FFFFFF").s().p("AAdA3QgHgHAAgLIABgFIALAAIAAACQAAAPANAAQAIAAADgNQACgGAAgRQAAgNgBgKQgDgMgFAAQgIAAAAALIAAAYIgMAAIAAgYQAAgMgIAAQgDAAgCACIgDAFQAKALgBATQAAAWgNANQgNAMgWABQgJAAgNgEQgNgFgEAAQgFAAgGAHIgIgGIARgZQgHgKAAgPIABgKQAEgQAOgIQAKgHAOAAIAIAAIAQgYIANAAIgQAaIAIAEIAHAFQACgEADgCQAFgEAIgBQAIABAGAGQAHgGAJgBQAbACABAtQAAAVgHAOQgKARgSAAQgMAAgHgHgAgwAtQAJAEAJAAIAJgBQAMgCAHgJQAHgKAAgMQgBgMgHgIQgHgJgMgBIgLAPIAGAAQAHAAAFAEQAFAEABAGQABAJgFAGQgEAGgHADQgGABgHAAQgLAAgIgEIgGAIIAEAAQAFAAAFACgAgxAZIAGACQAHACADgCQgGgBgDgEQgBgEABgEgAggAPQAAAGAFAAQAGAAAAgGQAAgFgGAAQgFAAAAAFgAg2gEQgEAFAAAIIABAHIAVgeQgMACgGAIg");
	this.shape_1459.setTransform(-264.8,-93.2);

	this.shape_1460 = new cjs.Shape();
	this.shape_1460.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAGgHIAGgKIAABDg");
	this.shape_1460.setTransform(-278.8,-87.2);

	this.shape_1461 = new cjs.Shape();
	this.shape_1461.graphics.f("#FFFFFF").s().p("AgoA1QgRgNABgVQABgTAPgGQgIgBAAgJQAAgKALgGQAIgDALAAQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAEAAAIQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXAAQgZgBgNATIgNAAQAGgOAPgIQANgIASABQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1461.setTransform(-278.8,-93.4);

	this.shape_1462 = new cjs.Shape();
	this.shape_1462.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1462.setTransform(217.8,-122.5);

	this.shape_1463 = new cjs.Shape();
	this.shape_1463.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1463.setTransform(213.1,-115.8);

	this.shape_1464 = new cjs.Shape();
	this.shape_1464.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgLQgKgPAAgWQAAgPAIgMQAJgNALABQALABAEAIQAFgKAMAAQALABAHAHQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgLIACgHIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGABgDAHQgDAHAAAHQgBASANAKQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgNQgLgOgVAAQgbAAgKAUIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARASABAcQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1464.setTransform(200,-117.2);

	this.shape_1465 = new cjs.Shape();
	this.shape_1465.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1465.setTransform(187.6,-115.8);

	this.shape_1466 = new cjs.Shape();
	this.shape_1466.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgCgNgJQgPgMAAgTQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAQQACgHAFgFQAGgEAHAAIAMAAIAAALIgCgBQgEAAAAAEQgBACAEADIAIAHQAEAGABAFQAAANgKAFQgJAFgMgBIAAAZIAKgDIAIgCIAAALIgIADIgKADIAAAXgAgbgRQgGAIAAAJQAAAWAYAEIAMACIANgBIAAgzIgaAAQgKgBgHAIgAAdgeIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgGQgFgHAAgDIAAgCQgEABAAANgAgXgvQACADAAAEQAAADgCABIACAAIADAAIAZAAQgCgGgIgEQgGgCgGAAIgIABg");
	this.shape_1466.setTransform(175.4,-114.7);

	this.shape_1467 = new cjs.Shape();
	this.shape_1467.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1467.setTransform(160.8,-122.5);

	this.shape_1468 = new cjs.Shape();
	this.shape_1468.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1468.setTransform(156.2,-115.8);

	this.shape_1469 = new cjs.Shape();
	this.shape_1469.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1469.setTransform(144.1,-117.1);

	this.shape_1470 = new cjs.Shape();
	this.shape_1470.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgWQABgSAPgHQgIgBAAgJQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADADABADQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1470.setTransform(132.8,-117.1);

	this.shape_1471 = new cjs.Shape();
	this.shape_1471.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1471.setTransform(122.4,-114.4);

	this.shape_1472 = new cjs.Shape();
	this.shape_1472.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1472.setTransform(112.3,-115.8);

	this.shape_1473 = new cjs.Shape();
	this.shape_1473.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1473.setTransform(96.8,-114.4);

	this.shape_1474 = new cjs.Shape();
	this.shape_1474.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1474.setTransform(79.3,-115.8);

	this.shape_1475 = new cjs.Shape();
	this.shape_1475.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAghAWgNQAIgFAMAAIAGABIAAALQAOgMASAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgJQgIAFgFACQgHADgJAAIgEgBgAAIgQQAQAFAAAUQAAAQgLAMQALACAHgFQANgIgBgVQAAgMgIgKQgIgJgMgBQgWgBgDAPQAFgEAHAAIAGABgAgogbQgGAJAAAMQAAANAHAJQAIAJANABQAKAAAIgFQAIgEAAgJQAAgFgCgDQgEgEgEAAQgEAAgCADQgCACAAAEIAAAEIgMAAQgCgLABgLQABgLAJgMIgHgBQgMAAgIAKg");
	this.shape_1475.setTransform(66.8,-115.8);

	this.shape_1476 = new cjs.Shape();
	this.shape_1476.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1476.setTransform(57.9,-115.8);

	this.shape_1477 = new cjs.Shape();
	this.shape_1477.graphics.f("#FFFFFF").s().p("AAQAxIAAgMIAGABQALAAAIgKQAIgKAAgMQABgMgHgJQgHgKgLgCIgFAAQgUAAgCASQABgCAEgBQAFAAADABQAIACADAIQAEAGgBAKQgBAPgMAKQgMAKgPAAQgYAAgMgOQgJgKAAgNQAAgHAEgGQADgHAGgEIgMAAQgDAAgDACIAAgMIANAAQgEgCAAgIIABgGQAFgMARAAQALAAAGAEQAHAFACAJQAJgRAXgBQASAAAMAQQALAQgBATQAAATgNAOQgNAPgSAAIgFgBgAgmgGQgGAFAAAGQAAAJAHAGQAIAGANAAQAMAAAGgEQAKgFAAgLQAAgHgIAAQgDAAgCABQgDACABAEIgLAAIAAgJIABgIIgMAAQgHAAgGAFgAgigkQADABABAFQAAAFgDADIAPAAQAAgHgEgEQgEgEgFAAIgDABg");
	this.shape_1477.setTransform(49.1,-115.8);

	this.shape_1478 = new cjs.Shape();
	this.shape_1478.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1478.setTransform(36.9,-117.2);

	this.shape_1479 = new cjs.Shape();
	this.shape_1479.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAgYALgNQAFgGAGgDQAIgFAJAAIAJABIAAALQANgMATAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgKIgNAIQgHADgKAAIgDgBgAANAlIAFABQAGAAACgEIAAgDIgDgLQgCAJgIAIgAAiAOIACAIQACAFAAADQAHgHAAgKQAAgFgFAAQgGAAAAAGgAglgdQgIAIgBALQgBAOAHAKQAHALAPABQAKAAAHgFQAIgFABgIQAAgFgDgDQgDgFgEAAQgEABgCACQgDACAAAEIABAFIgMAAQgDgMACgLQABgLAJgLIgGgBQgKAAgIAIgAAHgQQAOAFACANQADgFAHgBQAHgBADADQgBgKgHgGQgIgIgMgBQgJAAgGADQgJAEgBAIQADgFAIAAIAGABg");
	this.shape_1479.setTransform(18.8,-115.8);

	this.shape_1480 = new cjs.Shape();
	this.shape_1480.graphics.f("#FFFFFF").s().p("AgkA4QgPgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAAAAMQABAKAMAEQAJAEAQAAQAQAAALgJQAMgIAAgOQABgSgJgMQgIgJgNAAIg6AAIAAgJQANAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQgBgBgBAAQgDgCgBgDQgBgPAsAAQASAAAPAHQAQAIAAAOIgMAAQAAgIgNgFQgLgEgNAAQgWAAgBAGQAAAFAIAAIAbAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgOgIgAgagNQADACAAAEQAAAEgDADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1480.setTransform(6.7,-117.2);

	this.shape_1481 = new cjs.Shape();
	this.shape_1481.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1481.setTransform(-3.8,-115.8);

	this.shape_1482 = new cjs.Shape();
	this.shape_1482.graphics.f("#FFFFFF").s().p("AgwAJQgDgGAAgGQAAgHADgEIALAAIgCAGQAAAEADAEQAKAJAaAAQAQAAALgEQAGgDAEgCQAEgEAAgGQAAgHgGgFIAIgGQAJAHAAAPQAAAGgBAEQgKAZgpAAQgjAAgNgUg");
	this.shape_1482.setTransform(-14.9,-110.2);

	this.shape_1483 = new cjs.Shape();
	this.shape_1483.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1483.setTransform(-14.9,-115.8);

	this.shape_1484 = new cjs.Shape();
	this.shape_1484.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1484.setTransform(-33,-114.5);

	this.shape_1485 = new cjs.Shape();
	this.shape_1485.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1485.setTransform(-41.5,-115.8);

	this.shape_1486 = new cjs.Shape();
	this.shape_1486.graphics.f("#FFFFFF").s().p("AgFAwIgLgFQgGgCgFAAQgJAAABAIIgLAAQgCgIAAgJQAAgKACgHQADgIAJgHIAOgLQAIgHAAgHQABgEgDgEQgDgEgEAAQgGAAgDADQgEADAAAEQAAALAIAEIgQAAQgFgGAAgHIAAgEQABgJAHgFQAHgGALAAQAJAAAHAHQAGAGAAAJQAAAJgFAIIgNALIgNALQgGAHAAAJIABADIAIgBQAFAAAKADQAJADAGAAQAFAAAEgCQAGgBAFgFQAGgGAAgIQABgIgHgGIgMgNQgGgHAAgIQAAgKAGgFQAHgGAJAAQAJAAAGAFQAFAFABAIQABAGgDAEQgDAFgFABQgEAAgEgDQgDgCgBgEQAAgIAHgCQgCgDgEAAQgEAAgDADQgDACAAAEQAAAJAOAHQASAIAAASQAAAQgMAKQgLAKgQAAQgJAAgGgCg");
	this.shape_1486.setTransform(-48.8,-115.8);

	this.shape_1487 = new cjs.Shape();
	this.shape_1487.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1487.setTransform(-58.4,-114.4);

	this.shape_1488 = new cjs.Shape();
	this.shape_1488.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1488.setTransform(-69.3,-117.2);

	this.shape_1489 = new cjs.Shape();
	this.shape_1489.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgWQABgSAPgHQgIgBAAgJQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADADABADQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1489.setTransform(-86.9,-117.1);

	this.shape_1490 = new cjs.Shape();
	this.shape_1490.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1490.setTransform(-95.1,-122.5);

	this.shape_1491 = new cjs.Shape();
	this.shape_1491.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAgYALgNQAFgGAGgDQAIgFAJAAIAJABIAAALQANgMATAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgKIgNAIQgHADgKAAIgDgBgAANAlIAFABQAGAAACgEIAAgDIgDgLQgCAJgIAIgAAiAOIACAIQACAFAAADQAHgHAAgKQAAgFgFAAQgGAAAAAGgAglgdQgIAIgBALQgBAOAHAKQAHALAPABQAKAAAHgFQAIgFABgIQAAgFgDgDQgDgFgEAAQgEABgCACQgDACAAAEIABAFIgMAAQgDgMACgLQABgLAJgLIgGgBQgKAAgIAIgAAHgQQAOAFACANQADgFAHgBQAHgBADADQgBgKgHgGQgIgIgMgBQgJAAgGADQgJAEgBAIQADgFAIAAIAGABg");
	this.shape_1491.setTransform(-99.4,-115.8);

	this.shape_1492 = new cjs.Shape();
	this.shape_1492.graphics.f("#FFFFFF").s().p("AgjA4QgQgKgCgRQgBgTAPgIQgEAAgDgDQgDgDAAgFIABgEQAGgNAZABQAPABAHALQAHAJgBAOIghAAQgQAAABAMQAAAKAMAEQAJAEAQAAQAQAAALgJQALgIABgOQABgSgJgMQgIgJgNAAIgnAAQgSAAAAgMQAAgVAvAAQASAAAOAHQARAIAAAOIgNAAQAAgIgNgFQgLgEgNAAQgYAAgBAGQgBAFAJAAIAfAAQANAAAKAIQASAOAAAgQAAAWgRAOQgQAOgWAAQgVAAgNgIgAgagNQADACAAAEQABAEgEADIAYAAQgCgHgEgDQgFgEgIAAIgFABg");
	this.shape_1492.setTransform(-111.5,-117.2);

	this.shape_1493 = new cjs.Shape();
	this.shape_1493.graphics.f("#FFFFFF").s().p("AgIALQgFgEAAgHQAAgEADgEQAEgFAGAAQAFAAAEADQAFAEAAAGQAAAFgDAEQgEAFgHAAQgEAAgEgDg");
	this.shape_1493.setTransform(-125.7,-112.1);

	this.shape_1494 = new cjs.Shape();
	this.shape_1494.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1494.setTransform(-131.2,-115.8);

	this.shape_1495 = new cjs.Shape();
	this.shape_1495.graphics.f("#FFFFFF").s().p("AAdA3QgHgHAAgLIABgGIALAAIAAADQAAAPANAAQAIAAADgMQACgHAAgRQAAgNgBgKQgDgMgFAAQgIAAAAALIAAAYIgMAAIAAgYQAAgMgIAAQgDAAgCACIgDAFQAKAMgBARQAAAXgNANQgNAMgWABQgJAAgNgEQgNgFgEAAQgFAAgGAHIgIgFIARgaQgHgKAAgPIABgKQAEgQAOgJQAKgFAOgBIAIAAIAQgYIANAAIgQAaIAIAEIAHAFQACgEADgCQAFgFAIAAQAIAAAGAHQAHgHAJAAQAbACABAtQAAAVgHAOQgKAQgSABQgMAAgHgHgAgwAtQAJAEAJAAIAJgBQAMgCAHgJQAHgKAAgMQgBgMgHgIQgHgJgMgBIgLAPIAGAAQAHAAAFAEQAFAEABAGQABAJgFAGQgEAGgHADQgGABgHAAQgLAAgIgEIgGAIIAEAAQAFAAAFACgAgxAZIAGADQAHABADgBQgGgCgDgEQgBgEABgEgAggAPQAAAGAFAAQAGAAAAgGQAAgFgGAAQgFAAAAAFgAg2gEQgEAEAAAJIABAHIAVgeQgMABgGAJg");
	this.shape_1495.setTransform(-141.8,-117);

	this.shape_1496 = new cjs.Shape();
	this.shape_1496.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBYAAIAAgfIAFgHIAGgKIAABDg");
	this.shape_1496.setTransform(-155.9,-110.9);

	this.shape_1497 = new cjs.Shape();
	this.shape_1497.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgWQABgSAPgHQgIgBAAgJQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADADABADQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1497.setTransform(-155.9,-117.1);

	this.shape_1498 = new cjs.Shape();
	this.shape_1498.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1498.setTransform(-170.2,-122.5);

	this.shape_1499 = new cjs.Shape();
	this.shape_1499.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1499.setTransform(-174.9,-115.8);

	this.shape_1500 = new cjs.Shape();
	this.shape_1500.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgLQgKgPAAgWQAAgPAIgMQAJgNALABQALABAEAIQAFgKAMAAQALABAHAHQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgLIACgHIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGABgDAHQgDAHAAAHQgBASANAKQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgNQgLgOgVAAQgbAAgKAUIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARASABAcQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1500.setTransform(-188,-117.2);

	this.shape_1501 = new cjs.Shape();
	this.shape_1501.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1501.setTransform(-200.4,-115.8);

	this.shape_1502 = new cjs.Shape();
	this.shape_1502.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLAAQgWgCgNgJQgPgMAAgTQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAQQACgHAFgFQAGgEAHAAIAMAAIAAALIgCgBQgEAAAAAEQgBACAEADIAIAHQAEAGABAFQAAANgKAFQgJAFgMgBIAAAZIAKgDIAIgCIAAALIgIADIgKADIAAAXgAgbgRQgGAIAAAJQAAAWAYAEIAMACIANgBIAAgzIgaAAQgKgBgHAIgAAdgeIAAAUIACAAQAFAAADgDQAEgCAAgFQAAgGgFgGQgFgHAAgDIAAgCQgEABAAANgAgXgvQACADAAAEQAAADgCABIACAAIADAAIAZAAQgCgGgIgEQgGgCgGAAIgIABg");
	this.shape_1502.setTransform(-212.6,-114.7);

	this.shape_1503 = new cjs.Shape();
	this.shape_1503.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAaAAQAHAAAGAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1503.setTransform(-227.2,-122.5);

	this.shape_1504 = new cjs.Shape();
	this.shape_1504.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1504.setTransform(-231.8,-115.8);

	this.shape_1505 = new cjs.Shape();
	this.shape_1505.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1505.setTransform(-243.9,-117.1);

	this.shape_1506 = new cjs.Shape();
	this.shape_1506.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgWQABgSAPgHQgIgBAAgJQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADADABADQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1506.setTransform(-255.2,-117.1);

	this.shape_1507 = new cjs.Shape();
	this.shape_1507.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1507.setTransform(-268.1,-115.8);

	this.shape_1508 = new cjs.Shape();
	this.shape_1508.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1508.setTransform(-279.9,-115.8);

	this.shape_1509 = new cjs.Shape();
	this.shape_1509.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAYAAQAJAAAFAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1509.setTransform(212.5,-146.3);

	this.shape_1510 = new cjs.Shape();
	this.shape_1510.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1510.setTransform(212.4,-146.3);

	this.shape_1511 = new cjs.Shape();
	this.shape_1511.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1511.setTransform(207.6,-139.5);

	this.shape_1512 = new cjs.Shape();
	this.shape_1512.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1512.setTransform(194.5,-139.6);

	this.shape_1513 = new cjs.Shape();
	this.shape_1513.graphics.f("#FFFFFF").s().p("AAdA3QgHgHAAgLIABgGIALAAIAAADQAAAQANgBQAIAAADgMQACgIAAgQQAAgNgBgKQgDgMgFAAQgIAAAAALIAAAYIgMAAIAAgYQAAgMgIAAQgDAAgCACIgDAFQAKAMgBARQAAAXgNAMQgNANgWABQgJAAgNgEQgNgFgEAAQgFAAgGAHIgIgFIARgaQgHgKAAgQIABgJQAEgQAOgJQAKgFAOgBIAIAAIAQgYIANAAIgQAaIAIADIAHAGQACgEADgCQAFgFAIAAQAIAAAGAHQAHgHAJAAQAbACABAtQAAAVgHAOQgKAQgSABQgMAAgHgHgAgwAtQAJAEAJAAIAJgBQAMgCAHgJQAHgJAAgNQgBgMgHgIQgHgJgMgBIgLAPIAGAAQAHAAAFADQAFAFABAGQABAJgFAGQgEAGgHADQgGABgHAAQgLAAgIgEIgGAIIAEAAQAFAAAFACgAgxAZIAGADQAHABADgBQgGgCgDgEQgBgDABgFgAggAPQAAAGAFAAQAGAAAAgGQAAgFgGAAQgFAAAAAFgAg2gEQgEAEAAAJIABAHIAVgeQgMACgGAIg");
	this.shape_1513.setTransform(180,-140.7);

	this.shape_1514 = new cjs.Shape();
	this.shape_1514.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgQANgGQgGgDAAgJIABgGQAGgNAUAAQARAAAGALQAIgLARAAQAKAAAHAGQAIAFAAAKQAAAHgDADQALAKAAAMQAAAVgPAMQgQANgbAAQgbAAgQgOgAgqADQAAAMAPAGQAMAFAPAAQARAAALgFQAPgGABgNQAAgFgEgFQgGAEgMADQAFACAAAHQAAAGgIADQgHADgKAAQgLAAgHgDQgIgEAAgHQAAgMAVAAIAKgBQAOgCADgCIgyAAQgRAAABAOgAgJAGQAAAEAKAAQAKAAAAgEQAAgEgKAAQgKAAAAAEgAAMghQgFAEAAAHIAcAAIABgEIgBgFQgDgGgJAAQgHAAgEAEgAgegkQAEACAAAEQABAEgDAEIAWAAQAAgLgMgEIgGgBQgDAAgDACg");
	this.shape_1514.setTransform(165.8,-139.5);

	this.shape_1515 = new cjs.Shape();
	this.shape_1515.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1515.setTransform(157.5,-146.3);

	this.shape_1516 = new cjs.Shape();
	this.shape_1516.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1516.setTransform(152.9,-139.5);

	this.shape_1517 = new cjs.Shape();
	this.shape_1517.graphics.f("#FFFFFF").s().p("AgCBBQgWgBgNgNQgMgNgBgUQAAgSALgNQgDgBgDgEQgEgEgBgEIgCgKQAAgJAJgKQAIgKAPAAIAJABQAKADABAHIAFgIIAVAAQgFAIgHAIQgIAIgIAFQAOgBALAEQAeALAAAiQAAAWgRAPQgPAOgVAAIgCgBgAgagLQgJAKAAALQAAAMAIAIQALALAQAAQAOAAAKgHQANgKAAgPQAAgHgDgGQgEgJgJgFQgIgEgLAAQgSAAgKALgAgigpQgDAGAAAEQAAAHAFADIADgEIAKgJQgFABgEgEQgEgDABgEIgDADgAgWgwQAAAHAGAAQAHAAAAgHQAAgGgHAAQgGAAAAAGg");
	this.shape_1517.setTransform(140.7,-141.1);

	this.shape_1518 = new cjs.Shape();
	this.shape_1518.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1518.setTransform(129.2,-139.5);

	this.shape_1519 = new cjs.Shape();
	this.shape_1519.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAYAAQAJAAAFAEQAGAEAAAIQgBAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1519.setTransform(114.8,-146.3);

	this.shape_1520 = new cjs.Shape();
	this.shape_1520.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1520.setTransform(111,-138.2);

	this.shape_1521 = new cjs.Shape();
	this.shape_1521.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1521.setTransform(100.2,-139.5);

	this.shape_1522 = new cjs.Shape();
	this.shape_1522.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1522.setTransform(88.9,-139.5);

	this.shape_1523 = new cjs.Shape();
	this.shape_1523.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1523.setTransform(80.3,-139.5);

	this.shape_1524 = new cjs.Shape();
	this.shape_1524.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1524.setTransform(72,-139.5);

	this.shape_1525 = new cjs.Shape();
	this.shape_1525.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAaAAQAHAAAGAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1525.setTransform(57.6,-146.3);

	this.shape_1526 = new cjs.Shape();
	this.shape_1526.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1526.setTransform(53.4,-139.5);

	this.shape_1527 = new cjs.Shape();
	this.shape_1527.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1527.setTransform(40.9,-139.5);

	this.shape_1528 = new cjs.Shape();
	this.shape_1528.graphics.f("#FFFFFF").s().p("AgpAuQgTgPABgWQAAgVANgMQAGgGALgEQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAAAgBgBQgBgCAAgDQAAgEACgDQAFgHAKgDIAVgBQASAAALAJQANAJADAQIgyAAQgQAAgKALQgIAJgBAOQAAAPAMAMQAMAMAPAAQAMgBAKgEQALgEAGgIQAHgKgBgJIALAAIAAAEQAAAKgFAIQgHANgPAIQgOAHgSAAQgZgBgRgOgAgJguQACACAAACQAAABAAABQAAABAAAAQAAABgBAAQAAABgBAAIApAAQgIgJgOgCIgIAAQgHAAgEACg");
	this.shape_1528.setTransform(28.7,-138.4);

	this.shape_1529 = new cjs.Shape();
	this.shape_1529.graphics.f("#FFFFFF").s().p("AgLAUQAJgGAAgFIAAgBIgGgDQgFgEAAgGQAAgEADgEQAEgGAGAAQAFAAADADQAGAEAAAHQAAAFgDAFIgFAIIgHAHg");
	this.shape_1529.setTransform(14,-135.1);

	this.shape_1530 = new cjs.Shape();
	this.shape_1530.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1530.setTransform(8.4,-146.3);

	this.shape_1531 = new cjs.Shape();
	this.shape_1531.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1531.setTransform(3.8,-139.5);

	this.shape_1532 = new cjs.Shape();
	this.shape_1532.graphics.f("#FFFFFF").s().p("AgVApQgJgIAAgOQAAgKAEgIQADgFAHgHQAHgJAUgQIgmAAIAAgLIA4AAIAAALIgNAJQgHAFgEAGQgIAJgDAEQgEAHAAAJQAAAFAFAEQADADAEAAQALAAADgJIABgGQAAgEgCgDIALAAQAGAIgBALQgBALgJAIQgJAHgMAAQgNAAgIgHg");
	this.shape_1532.setTransform(-6.3,-139.3);

	this.shape_1533 = new cjs.Shape();
	this.shape_1533.graphics.f("#FFFFFF").s().p("AgoA0QgSgNABgUQABgQANgHQgJgCAAgKQAAgJALgFQAHgEALAAQAQAAAGALQAEgJAOgCIAIABIAGgFIAEgGQAGgJABgLIAQAAIgDALQgCAGgEAGIgKALQAIADACAKQABAGgDAEIgJAAQAFABAFAFQAIAHAAAPQAAATgSANQgRAMgVAAQgXAAgRgNgAglAFQgFAFAAAHQABALANAHQAMAGAQAAQAQAAALgGQAOgGABgNQAAgGgFgFQgFgFgLAAIgtAAQgIAAgFAFgAAOgVQgFABgCABQgEAEAAAEIATAAQAGABAFABQABgDgBgEQgDgGgMAAIgEABgAgegUQADABABADQABADgCACIAUAAQgBgHgHgDIgHgBQgEAAgEACg");
	this.shape_1533.setTransform(-15.4,-141);

	this.shape_1534 = new cjs.Shape();
	this.shape_1534.graphics.f("#FFFFFF").s().p("AgpAhQgLgFAAgMQAAgIAGgDIAMAAQgEACAAAFQAAAGAKgBQAIABASgHQANgFAKAAIAFAAQALACAFAGIAAghQAGgFAFgJIAABCIgLAAQgHgKgNAAQgKABgJADQgSAHgOAAQgGABgGgCg");
	this.shape_1534.setTransform(-33.8,-134.6);

	this.shape_1535 = new cjs.Shape();
	this.shape_1535.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgXQABgRAPgHQgIgCAAgIQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHABAKAAQATAAAMgMQALgMAAgRQABgVgLgNQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZAAARASQARASAAAaQAAAagPASQgQAUgagBQgZABgPgLgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1535.setTransform(-33.6,-140.9);

	this.shape_1536 = new cjs.Shape();
	this.shape_1536.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1536.setTransform(-47.9,-146.3);

	this.shape_1537 = new cjs.Shape();
	this.shape_1537.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1537.setTransform(-52.6,-139.5);

	this.shape_1538 = new cjs.Shape();
	this.shape_1538.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgLQgKgPAAgWQAAgPAIgMQAJgNALACQALAAAEAIQAFgKAMAAQALABAHAHQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgLIACgHIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAIQgBAQANALQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgNQgLgNgVAAQgbgBgKAUIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARASABAbQABAdgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1538.setTransform(-65.7,-140.9);

	this.shape_1539 = new cjs.Shape();
	this.shape_1539.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1539.setTransform(-78.1,-139.5);

	this.shape_1540 = new cjs.Shape();
	this.shape_1540.graphics.f("#FFFFFF").s().p("AAQA8IAAgVQgOABgLgBQgWAAgNgKQgPgMAAgTQAAgKAFgIQAFgIAIgDQgEgBgCgDIgBgGQAAgGAFgEQAKgIARAAQAbAAAHAQQACgHAFgFQAGgEAHAAIAMAAIAAALIgCgBQgEAAAAAFQgBABAEADIAIAHQAEAGABAFQAAANgKAFQgJAFgMgBIAAAZIAKgDIAIgCIAAAKIgIAEIgKADIAAAXgAgbgRQgGAIAAAJQAAAVAYAGIAMABIANgBIAAgzIgaAAQgKAAgHAHgAAdgeIAAAUIACAAQAFAAADgDQAEgDAAgEQAAgGgFgGQgFgHAAgDIAAgCQgEABAAANgAgXgvQACAEAAACQAAAEgCACIACgBIADAAIAZAAQgCgGgIgEQgGgCgGAAIgIABg");
	this.shape_1540.setTransform(-90.3,-138.5);

	this.shape_1541 = new cjs.Shape();
	this.shape_1541.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgJAAIAAgJIAZAAQAHAAAGAEQAFAEABAIQAAAIgIADQgHADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1541.setTransform(-104.9,-146.3);

	this.shape_1542 = new cjs.Shape();
	this.shape_1542.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1542.setTransform(-109.5,-139.5);

	this.shape_1543 = new cjs.Shape();
	this.shape_1543.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1543.setTransform(-121.6,-140.8);

	this.shape_1544 = new cjs.Shape();
	this.shape_1544.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgXQABgRAPgHQgIgCAAgIQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHABAKAAQATAAAMgMQALgMAAgRQABgVgLgNQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZAAARASQARASAAAaQAAAagPASQgQAUgagBQgZABgPgLgAgbgXQADACABAEQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1544.setTransform(-132.9,-140.9);

	this.shape_1545 = new cjs.Shape();
	this.shape_1545.graphics.f("#FFFFFF").s().p("AACA9QgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgRQABgVARgJQgDgBgCgDQgCgDAAgEIABgEQAEgIAJgDQAHgDANAAQAWAAAKAMQAIAKAAANIgjAAQgKAAgIAHQgHAHgBAJQgBALAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgyIADACQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQABAFgEABIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABg");
	this.shape_1545.setTransform(-143.3,-138.1);

	this.shape_1546 = new cjs.Shape();
	this.shape_1546.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1546.setTransform(-153.4,-139.5);

	this.shape_1547 = new cjs.Shape();
	this.shape_1547.graphics.f("#FFFFFF").s().p("AACBJQgIgFgBgJQAAgIAGgDQgVABgOgMQgOgMABgSQABgUARgJQgDgBgCgDQgCgDAAgEIABgEIABgDQgHgDAAgLQAAgFACgDQAHgNAhAAQAbAAAIAMQACAFAAAEQAAALgJADQAJAJAAAPIgjAAQgKAAgIAHQgHAHgBAIQgBAMAKAJQAKAIAMABQAUABAOgIIAAAMIgNAEQgNAEAAAFQAAAEADACQADACAFAAQAGAAAEgDQAEgDAAgFIgBgEIAMAAIAAAEQAAALgIAHQgIAGgMABIgEAAQgJAAgHgDgAgPgmQADACABAEQAAADgDACIAGgBIAdAAQgEgHgJgDQgFgBgHAAIgLABgAAOgwIAFACQADgCAAgDQAAgEgJgDQgHgDgIAAQgKAAgHACQgKADAAAFQAAAAAAABQAAAAAAABQABAAAAABQAAAAABABIAEgCQAGgCANAAQALAAAHADg");
	this.shape_1547.setTransform(-168.9,-139.3);

	this.shape_1548 = new cjs.Shape();
	this.shape_1548.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAaAAQAHAAAGAEQAFAEAAAIQABAIgIADQgGADgKAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1548.setTransform(-176.4,-146.3);

	this.shape_1549 = new cjs.Shape();
	this.shape_1549.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1549.setTransform(-180.2,-138.2);

	this.shape_1550 = new cjs.Shape();
	this.shape_1550.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1550.setTransform(-191,-139.5);

	this.shape_1551 = new cjs.Shape();
	this.shape_1551.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1551.setTransform(-202.3,-139.5);

	this.shape_1552 = new cjs.Shape();
	this.shape_1552.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1552.setTransform(-210.9,-139.5);

	this.shape_1553 = new cjs.Shape();
	this.shape_1553.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1553.setTransform(-219.2,-139.5);

	this.shape_1554 = new cjs.Shape();
	this.shape_1554.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1554.setTransform(-238.1,-139.5);

	this.shape_1555 = new cjs.Shape();
	this.shape_1555.graphics.f("#FFFFFF").s().p("AgnA1QgPgPAAgXQAAgPAGgLQAIgNANAAQANAAABAKQAEgMANAAQAKAAAHAJQAIAIAAAKQAAANgJAJQgIAIgMAAQgKAAgIgGQgJgGABgKIACgOQABgHgHAAQgEAAgEAIQgCAFAAAHQAAARAMAKQAMAJAQAAQARAAAMgMQALgLAAgRQAAgNgGgJQgGgLgNAAIgsAAQgUAAgBgNQAAgNARgHQANgFATAAQAeAAAOAQQAHAIAAAKIgLAAQgCgKgMgGQgKgFgOAAQgcAAAAAKQAAAAAAABQAAAAAAAAQABABAAAAQAAAAABABIAEABIAkAAQALAAAGAEIAHADQAQAMAAAeQAAAagQAQQgPARgYAAQgXAAgQgPgAgOACQABAGAEAEQAEAEAGAAQAGAAAFgEQAEgEAAgHQgEAHgJAAQgJABgHgKIgBADgAAAgNQAAABAAAAQgBAAAAABQAAAAAAABQgBABAAAAQAAABABAAQAAABAAABQAAAAABABQAAAAAAABQAAAAAAABQABAAAAAAQABABAAAAQABAAAAAAQABAAABAAQAAAAABgBQAAAAABAAQABgBAAAAQAAgBABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAAAAAgBQAAgBgBAAQAAgBAAAAQgBAAAAgBQAAAAgBgBQgBAAAAAAQgBAAAAgBQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAQAAABAAAAgAAnghIAAAAgAAggkQgGgEgLAAIgkAAIgEgBQgBgBAAAAQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAgKAcAAQAOAAAKAFQAMAGACAKIgHgDg");
	this.shape_1555.setTransform(-250.5,-141.4);

	this.shape_1556 = new cjs.Shape();
	this.shape_1556.graphics.f("#FFFFFF").s().p("AgoA0QgQgOACgbQABgPALgLQAMgLASABIAQAAIAAALIgOAAQgNAAgIAHQgJAGAAAMQAAAOAMAIQAMAJAQgBQATgBAKgLQAKgMAAgRIAAgHQgDgXgPgMQgLAMgRAAQgRABgGgLQgCgCAAgDQAAgGACgCIgHAAIAAgLIANgBQAcABAQAHQASAJAJASQAJATgBAWQgBAYgPAOQgQAQgYAAQgZAAgOgNgAgRgyQAAADADACQADACAEABQALAAAFgHQgGgCgHAAIgMgBIgBACg");
	this.shape_1556.setTransform(-262.1,-141.1);

	this.shape_1557 = new cjs.Shape();
	this.shape_1557.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1557.setTransform(-270.3,-139.5);

	this.shape_1558 = new cjs.Shape();
	this.shape_1558.graphics.f("#FFFFFF").s().p("AgwAJQgDgGAAgGQAAgHADgEIALAAIgCAGQAAAEADAEQAKAJAaAAQAQAAALgEQAGgDAEgCQAEgEAAgGQAAgHgGgFIAIgGQAJAHAAAPQAAAGgBAEQgKAZgpAAQgjAAgNgUg");
	this.shape_1558.setTransform(-278.7,-134);

	this.shape_1559 = new cjs.Shape();
	this.shape_1559.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1559.setTransform(-278.7,-139.5);

	this.shape_1560 = new cjs.Shape();
	this.shape_1560.graphics.f("#FFFFFF").s().p("AgsAAIgfhWICXBWIiXBXg");
	this.shape_1560.setTransform(-305.9,-201.8);

	this.shape_1561 = new cjs.Shape();
	this.shape_1561.graphics.f("#FFFFFF").s().p("AgIALQgFgEAAgHQAAgEADgEQAEgFAGAAQAFAAAEADQAFAEAAAGQAAAFgDAEQgEAFgHAAQgEAAgEgDg");
	this.shape_1561.setTransform(-100.9,-171);

	this.shape_1562 = new cjs.Shape();
	this.shape_1562.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1562.setTransform(-110.2,-181);

	this.shape_1563 = new cjs.Shape();
	this.shape_1563.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1563.setTransform(-110.2,-174.8);

	this.shape_1564 = new cjs.Shape();
	this.shape_1564.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1564.setTransform(-118.8,-174.7);

	this.shape_1565 = new cjs.Shape();
	this.shape_1565.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1565.setTransform(-127.1,-174.8);

	this.shape_1566 = new cjs.Shape();
	this.shape_1566.graphics.f("#FFFFFF").s().p("AgDA/QgVgBgNgNQgNgOgBgUQAAgNAGgKQAFgJALgLIAWgUQAHgJACgFIAUAAQgEAHgGAHQgFAGgJAHQAOgBAKAEQAeAKAAAjQAAAWgQAOQgPAOgVAAIgDAAgAgbgNQgIAJAAAMQAAAMAHAIQALALARAAQANAAAKgIQANgJAAgQQAAgGgDgGQgDgJgKgFQgIgFgLAAQgSAAgKAMg");
	this.shape_1566.setTransform(-138.6,-176);

	this.shape_1567 = new cjs.Shape();
	this.shape_1567.graphics.f("#FFFFFF").s().p("AgoA1QgRgMABgWQABgSAPgHQgIgBAAgJQAAgKALgGQAIgEALABQANABAIAKQAIALAAANIgiAAQgHAAgFAEQgGAFAAAHQABARAXAFQAHACAKgBQATABAMgNQALgMAAgSQABgTgLgOQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgHASAAQAZgBARATQARASAAAaQAAAagPASQgQAUgaAAQgZgBgPgKgAgbgXQADADABADQABADgCAFIAVAAQgBgGgEgEQgFgEgHgBIgHABg");
	this.shape_1567.setTransform(-149.9,-176.1);

	this.shape_1568 = new cjs.Shape();
	this.shape_1568.graphics.f("#FFFFFF").s().p("AgNAyIgJgCIAAgLIAEABQAFAAAAgFQAAgDgFgGQgFgHAAgHQAAgHAEgDQAEgFAHgBQAHgBAEADQgBgPgCgGQgBgKgIgCQgHgBgFADIAAgLQAHgDAKABQAOABAIAQQAGANAAASQAAAVgKAPQgKAOgOAAIgDAAgAgFAFQgGABAAAGIABADIAFAIQACAEAAAEQADgFABgIQABgGAAgFQgCgDgCAAIgDABg");
	this.shape_1568.setTransform(-158.4,-174.7);

	this.shape_1569 = new cjs.Shape();
	this.shape_1569.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1569.setTransform(-168,-174.7);

	this.shape_1570 = new cjs.Shape();
	this.shape_1570.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1570.setTransform(-177.5,-174.7);

	this.shape_1571 = new cjs.Shape();
	this.shape_1571.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgLQgKgPAAgWQAAgPAIgMQAJgNALABQALABAEAIQAFgKAMAAQALABAHAHQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgLIACgHIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGABgDAHQgDAHAAAHQgBASANAKQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgNQgLgOgVAAQgbAAgKAUIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARASABAcQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1571.setTransform(-185.7,-176.1);

	this.shape_1572 = new cjs.Shape();
	this.shape_1572.graphics.f("#FFFFFF").s().p("AgCBDQgVgBgNgNQgNgOAAgUQgBgNAGgKQAFgJALgLQAIgGAOgOQAGgJADgFIAUAAQgEAHgFAHQgGAGgJAHQAOgBAKAEQAeALAAAiQAAAWgQAOQgPAOgWAAIgCAAgAgagJQgIAJAAAMQAAAMAIAIQAKALARAAQAOAAAJgIQANgJAAgQQAAgGgDgGQgDgJgJgFQgJgFgLAAQgSAAgKAMgAgrgjIgJAAIAAgJIAJAAIAAgNIgJAAIAAgJIAaAAQAIAAAFAEQAGAEAAAIQAAAIgHAEQgHAEgLgBIAAAOQgHAHgEAHgAggg5IAAANQAKAAAAgHQAAgGgHAAIgDAAg");
	this.shape_1572.setTransform(-197.2,-176.4);

	this.shape_1573 = new cjs.Shape();
	this.shape_1573.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1573.setTransform(-208.7,-181);

	this.shape_1574 = new cjs.Shape();
	this.shape_1574.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1574.setTransform(-209,-174.7);

	this.shape_1575 = new cjs.Shape();
	this.shape_1575.graphics.f("#FFFFFF").s().p("AgnA1QgPgPAAgXQAAgPAGgMQAIgNANAAQANAAACALQADgMAOAAQAJAAAIAJQAGAIABAKQAAANgJAIQgJAJgLAAQgLAAgHgGQgJgGAAgKIADgPQACgGgIAAQgEAAgEAHQgCAGAAAHQAAAQAMALQAMAIAQABQARAAAMgMQALgMAAgRQAAgNgGgIQgGgLgNAAIhBAAIAAgLQARABAAgBQAAAAAAgBQAAAAgBAAQAAAAgBgBQgBAAgBgBQgDgBAAgEQAAgNAOgEQAIgCARAAQAPAAAJADQANADAHAKQAHAHAAALIgLAAQgCgKgMgHQgKgEgPAAIgOABQgHADAAAGQAAAAAAAAQAAABAAAAQABAAAAABQABAAAAAAIAEACIAeAAQAKAAAHAEIAHADQAQAMAAAeQAAAagPAQQgQARgYAAQgXAAgQgPgAgNACQgBAGAFADQAEAEAFABQAHAAAFgFQAEgDAAgHQgEAHgJAAQgKAAgFgJIgBADgAAAgNQAAAAAAABQgBAAAAABQAAAAAAABQgBAAAAABQAAABABAAQAAABAAAAQAAABABAAQAAABAAAAQAAABAAAAQABABAAAAQABAAAAAAQABAAABAAQAAAAABAAQAAAAABAAQAAAAABgBQABAAAAgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAgBgBQgBAAAAAAQgBgBAAAAQgBAAAAAAQgBAAgBAAQAAAAgBABQAAAAgBAAQAAABAAAAgAAnghIAAAAgAAggkQgHgEgKAAIgeAAIgEgCQAAAAgBAAQAAgBgBAAQAAAAAAgBQAAAAAAAAQAAgGAHgDIAOgBQAPAAAKAEQAMAHACAKIgHgDg");
	this.shape_1575.setTransform(-227.3,-176.5);

	this.shape_1576 = new cjs.Shape();
	this.shape_1576.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1576.setTransform(-238,-174.7);

	this.shape_1577 = new cjs.Shape();
	this.shape_1577.graphics.f("#FFFFFF").s().p("AgVAxQgUAAgLgPQgJgNAAgTQAAghAWgNQAIgFAMAAIAGABIAAALQAOgMASAAQAVABALAOQALAMAAAWQAAAUgLAOQgNAPgUAAIgGAAIAAgJQgIAFgFACQgHADgJAAIgEgBgAAIgQQAQAFAAAUQAAAQgLAMQALACAHgFQANgIgBgVQAAgMgIgKQgIgJgMgBQgWgBgDAPQAFgEAHAAIAGABgAgogbQgGAJAAAMQAAANAHAJQAIAJANABQAKAAAIgFQAIgEAAgJQAAgFgCgDQgEgEgEAAQgEAAgCADQgCACAAAEIAAAEIgMAAQgCgLABgLQABgLAJgMIgHgBQgMAAgIAKg");
	this.shape_1577.setTransform(-249.3,-174.7);

	this.shape_1578 = new cjs.Shape();
	this.shape_1578.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1578.setTransform(-258.2,-174.7);

	this.shape_1579 = new cjs.Shape();
	this.shape_1579.graphics.f("#FFFFFF").s().p("AgbA6QgMgFgHgLQgKgPAAgWQAAgPAIgMQAJgNALABQALABAEAIQAFgKAMAAQALABAHAHQAIAJABAKQAAANgKAKQgKAKgMAAQgMAAgJgIQgIgIAAgLIACgHIADgHQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGABgDAHQgDAHAAAHQgBASANAKQANALAQAAQARAAANgNQANgNgBgRQAAgWgLgNQgLgOgVAAQgbAAgKAUIgNAAQAGgPAOgJQANgHARAAQAaAAAPAPQARASABAcQABAcgQASQgQAUgbAAQgOAAgMgGgAgQgEQAAAFAFADQAFAEAGAAQAHAAAGgEQAGgFAAgGIgBgDQgBAEgFADQgEACgGAAQgNABgDgMIgCAIgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQgCgDgDAAQgGAAAAAHg");
	this.shape_1579.setTransform(-266.5,-176.1);

	this.shape_1580 = new cjs.Shape();
	this.shape_1580.graphics.f("#FFFFFF").s().p("AACAmQgGAMgUAAQgOAAgJgLQgKgLAAgPQABgQAKgIIgPAAIAAgLIAPAAQgDgCAAgFIABgFQAFgPAWAAQARAAAGAMQAHgMARAAQAKAAAHAEQAIAFABAJQABAIgDAFIgDAAQAPALAAASQAAAQgKALQgKAMgOAAQgSAAgIgMgAAIADIAAALQAAAGAHADQAFADAHAAQAIAAAGgGQAGgGAAgIQAAgHgGgFQgFgFgIAAIgyAAQgIAAgGAFQgGAGAAAGQAAAJAFAFQAGAGAJAAQAHAAAFgDQAHgDAAgGIAAgLgAAYglQgHAAgEAFQgEAEAAAGIAaAAQACgCAAgEQgBgJgLAAIgBAAgAgcgjQADACABAEQAAAEgCADIAWAAQgBgIgGgEQgFgDgFAAQgEAAgDACg");
	this.shape_1580.setTransform(-278.9,-174.7);

	this.shape_1581 = new cjs.Shape();
	this.shape_1581.graphics.f("#FFFFFF").s().p("AgZBVQAOgLAJgWQAHgWAAgdIAAgBQAAgagGgVQgHgVgOgNIgDgCIAFgNQAMAGAKAOQALAPAGASQAGASABAUIAAAGQAAAUgGAUQgGATgLAPQgLAQgMAGg");
	this.shape_1581.setTransform(215.5,-200);

	this.shape_1582 = new cjs.Shape();
	this.shape_1582.graphics.f("#FFFFFF").s().p("AghArQgJgJAAgMQAAgQAMgIQALgIAWgBIAOAAIAAgGQAAgHgFgGQgEgEgIAAQgHAAgGAEQgEAEAAAFIgXAAQAAgHAFgIQAGgHAKgEQAJgEAKAAQASAAAKAJQALAIAAARIAAAsQAAAOAEAHIAAACIgXAAIgCgKQgLAMgPAAQgPAAgKgJgAgOAHQgFAEAAAJQgBAGAFAEQAEAEAHAAQAGAAAGgEQAGgDADgFIAAgTIgNAAQgLAAgHAEg");
	this.shape_1582.setTransform(207.4,-199.6);

	this.shape_1583 = new cjs.Shape();
	this.shape_1583.graphics.f("#FFFFFF").s().p("AgWA7IAAALIgVAAIAAiNIAWAAIAAA0QAKgMAQAAQASAAAKAOQALANAAAYIAAABQAAAXgLAOQgKAOgSAAQgRAAgKgNgAgVAAIAAApQAHANAOAAQAKAAAFgIQAHgIAAgQIAAgDQgBgQgFgHQgGgJgKAAQgOAAgHANg");
	this.shape_1583.setTransform(197.1,-201.6);

	this.shape_1584 = new cjs.Shape();
	this.shape_1584.graphics.f("#FFFFFF").s().p("AggArQgKgJAAgMQAAgQAMgIQAMgIAVgBIANAAIAAgGQAAgHgEgGQgEgEgIAAQgIAAgFAEQgEAEAAAFIgXAAQAAgHAGgIQAFgHAJgEQAKgEALAAQARAAALAJQAKAIAAARIAAAsQAAAOAEAHIAAACIgXAAIgCgKQgMAMgOAAQgPAAgJgJgAgOAHQgFAEgBAJQAAAGAFAEQAEAEAHAAQAGAAAGgEQAFgDADgFIAAgTIgMAAQgLAAgHAEg");
	this.shape_1584.setTransform(186.4,-199.6);

	this.shape_1585 = new cjs.Shape();
	this.shape_1585.graphics.f("#FFFFFF").s().p("AgVA7IgBALIgVAAIAAiNIAXAAIAAA0QAKgMAPAAQASAAALAOQAKANAAAYIAAABQAAAXgKAOQgKAOgTAAQgRAAgJgNgAgUAAIAAApQAFANAPAAQAKAAAGgIQAFgIABgQIAAgDQAAgQgGgHQgGgJgKAAQgPAAgFANg");
	this.shape_1585.setTransform(176.1,-201.6);

	this.shape_1586 = new cjs.Shape();
	this.shape_1586.graphics.f("#FFFFFF").s().p("AgKBFIAAhjIAVAAIAABjgAgIgwQgEgDAAgFQAAgFAEgEQADgDAFAAQAGAAAEADQADAEAAAFQAAAFgDADQgEAEgGAAQgFAAgDgEg");
	this.shape_1586.setTransform(168.1,-201.5);

	this.shape_1587 = new cjs.Shape();
	this.shape_1587.graphics.f("#FFFFFF").s().p("AgKBHIAAiNIAVAAIAACNg");
	this.shape_1587.setTransform(163.3,-201.7);

	this.shape_1588 = new cjs.Shape();
	this.shape_1588.graphics.f("#FFFFFF").s().p("AAlBEIgLggIgzAAIgLAgIgZAAIAziGIAVAAIAzCGgAgTARIAnAAIgUg3g");
	this.shape_1588.setTransform(154.5,-201.3);

	this.shape_1589 = new cjs.Shape();
	this.shape_1589.graphics.f("#FFFFFF").s().p("AgCBMQgLgPgGgUQgFgTAAgWQAAgVAFgTQAGgUALgPQAKgPANgGIAEANQgNAKgIAUQgHAVgBAaIAAAHQAAAcAHAWQAIAWAOAMIgEAMQgNgGgKgPg");
	this.shape_1589.setTransform(145.3,-200);

	this.shape_1590 = new cjs.Shape();
	this.shape_1590.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1590.setTransform(133,-199.5);

	this.shape_1591 = new cjs.Shape();
	this.shape_1591.graphics.f("#FFFFFF").s().p("AACA9QgHgDgFgHQgBAFgGAEQgFADgIAAQgOAAgIgOQgHgNAAgSQAAgUALgQQANgQAVAAQAOAAAKAJQAJAKAAAOQAAALgJAIIgFAFQgCACAAADQgBAMAQAAQAMAAAHgNQAHgLAAgQQAAgUgNgQQgMgPgYAAQgMAAgKAFQgLAGgFAJIgNAAQAGgPAOgJQAOgHARgBQAdAAARARQAHAIAGAOQAGANAAALQABAfgNARQgMAPgSAAQgKAAgGgCgAgiAmQgCACAAADQAAADACACQACACADABQADgBACgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgDABgCACgAgrADQgDAIAAAJQAAAHACAGQAFgJALgBQAJgBAHAGQABgGAGgFQAGgHAAgIQAAgFgEgFQgFgFgHABQgUAAgIAPg");
	this.shape_1591.setTransform(124.6,-200.9);

	this.shape_1592 = new cjs.Shape();
	this.shape_1592.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1592.setTransform(115.9,-199.5);

	this.shape_1593 = new cjs.Shape();
	this.shape_1593.graphics.f("#FFFFFF").s().p("AACA9QgHgDgFgHQgBAFgGAEQgFADgIAAQgOAAgIgOQgHgNAAgSQAAgUALgQQANgQAVAAQAOAAAKAJQAJAKAAAOQAAALgJAIIgFAFQgCACAAADQgBAMAQAAQAMAAAHgNQAHgLAAgQQAAgUgNgQQgMgPgYAAQgMAAgKAFQgLAGgFAJIgNAAQAGgPAOgJQAOgHARgBQAdAAARARQAHAIAGAOQAGANAAALQABAfgNARQgMAPgSAAQgKAAgGgCgAgiAmQgCACAAADQAAADACACQACACADABQADgBACgCQACgCAAgDQAAgDgCgCQgCgCgDgBQgDABgCACgAgrADQgDAIAAAJQAAAHACAGQAFgJALgBQAJgBAHAGQABgGAGgFQAGgHAAgIQAAgFgEgFQgFgFgHABQgUAAgIAPg");
	this.shape_1593.setTransform(107.5,-200.9);

	this.shape_1594 = new cjs.Shape();
	this.shape_1594.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1594.setTransform(95,-205.8);

	this.shape_1595 = new cjs.Shape();
	this.shape_1595.graphics.f("#FFFFFF").s().p("AgpArQgQgSAAgaQABgbAQgRQARgRAaAAQAYAAAPAPQAQAPAAAXQAAASgLAOQgKANgSABQgRAAgKgLQgLgLACgPQABgPALgIIgNAAIAAgMIAsAAIAAAMIgIAAQgIAAgGAFQgFAFAAAIQAAAHAFAGQAGAFAJAAQALAAAHgJQAGgHgBgMQAAgOgHgJQgLgMgVAAQgSAAgLAPQgJAOAAATQAAASAJAOQAKAPAQACIAJABQAUAAAMgQIAOAAQgOAfgoAAQgZAAgRgUg");
	this.shape_1595.setTransform(95.2,-198.2);

	this.shape_1596 = new cjs.Shape();
	this.shape_1596.graphics.f("#FFFFFF").s().p("AAQA8IAAgWQgOACgLgBQgWAAgNgLQgPgKAAgUQAAgKAFgIQAFgHAIgEQgEgBgCgDIgBgGQAAgGAFgFQAKgHARAAQAbABAHAOQACgHAFgDQAGgFAHAAIAMAAIAAALIgCgBQgEABAAAEQgBABAEADIAIAHQAEAFABAGQAAAMgKAGQgJAFgMgBIAAAZIAKgDIAIgDIAAALIgIAEIgKACIAAAYgAgbgRQgGAHAAAKQAAAWAYAFIAMABIANgBIAAg0IgaAAQgKABgHAHgAAdgdIAAATIACAAQAFAAADgDQAEgDAAgEQAAgGgFgGQgFgHAAgEIAAgBQgEABAAAOgAgXguQACACAAADQAAAEgCACIACgBIADAAIAZAAQgCgGgIgDQgGgDgGAAIgIACg");
	this.shape_1596.setTransform(83,-198.5);

	this.shape_1597 = new cjs.Shape();
	this.shape_1597.graphics.f("#FFFFFF").s().p("AADAWIAHgHQADgFAAgDIAAgBIgFgDQgFgDgBgHQABgEACgEQAEgGAIAAQAEAAAEADQAFAEAAAHQAAAFgCAEIgHALQgFAGgEADgAgcAWIAGgHQAEgFAAgDIAAgBIgGgDQgFgDAAgHQAAgEADgEQAEgGAGAAQAFAAAEADQAGAEAAAHQAAAFgDAEQgBAEgFAHIgJAJg");
	this.shape_1597.setTransform(67,-206.8);

	this.shape_1598 = new cjs.Shape();
	this.shape_1598.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1598.setTransform(60.4,-199.5);

	this.shape_1599 = new cjs.Shape();
	this.shape_1599.graphics.f("#FFFFFF").s().p("AgbA6QgMgGgHgKQgKgPAAgWQAAgOAIgNQAJgMALABQALAAAEAIQAFgJAMAAQALgBAHAJQAIAHABAMQAAAMgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgMIACgHIADgHQABgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAIQgBARANAKQANALAQAAQARAAANgNQANgNgBgSQAAgUgLgOQgLgOgVABQgbAAgKASIgNAAQAGgOAOgJQANgHARAAQAaAAAPAPQARARABAcQABAcgQAUQgQATgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgDAAgHIgBgDQgBAEgFACQgEADgGABQgNAAgDgNIgCAJgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1599.setTransform(52.1,-200.9);

	this.shape_1600 = new cjs.Shape();
	this.shape_1600.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1600.setTransform(37.8,-206.3);

	this.shape_1601 = new cjs.Shape();
	this.shape_1601.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1601.setTransform(33.2,-199.5);

	this.shape_1602 = new cjs.Shape();
	this.shape_1602.graphics.f("#FFFFFF").s().p("AgVApQgJgIAAgNQAAgLAEgIQADgEAHgJQAHgIAUgQIgmAAIAAgLIA4AAIAAALIgNAJQgHAGgEAFQgIAJgDAEQgEAHAAAJQAAAFAFAEQADACAEAAQALABADgKIABgEQAAgEgCgEIALAAQAGAIgBALQgBALgJAHQgJAIgMAAQgNAAgIgHg");
	this.shape_1602.setTransform(23.1,-199.3);

	this.shape_1603 = new cjs.Shape();
	this.shape_1603.graphics.f("#FFFFFF").s().p("AgoA0QgSgNABgUQABgQANgIQgJgBAAgKQAAgJALgFQAHgEALABQAQgBAGALQAEgJAOgBIAIAAIAGgFIAEgFQAGgKABgLIAQAAIgDALQgCAHgEAFIgKAKQAIAEACAKQABAGgDAEIgJAAQAFABAFAFQAIAHAAAPQAAATgSANQgRAMgVAAQgXAAgRgNgAglAEQgFAGAAAHQABALANAHQAMAGAQAAQAQAAALgFQAOgHABgNQAAgHgFgFQgFgEgLAAIgtAAQgIAAgFAEgAAOgWQgFABgCACQgEAEAAAEIATAAQAGAAAFACQABgCgBgFQgDgGgMAAIgEAAgAgegVQADACABADQABADgCACIAUAAQgBgHgHgDIgHgBQgEAAgEABg");
	this.shape_1603.setTransform(14,-201);

	this.shape_1604 = new cjs.Shape();
	this.shape_1604.graphics.f("#FFFFFF").s().p("AAUAWIgIgJIgHgLQgCgEAAgFQAAgHAFgEQAEgDAEAAQAIAAAEAGQACAEAAAEQAAAHgFADIgFADIgBABQAAADAEAFIAHAHgAgLAWIgJgJIgHgLQgCgEAAgFQAAgHAFgEQAEgDAFAAQAHAAAEAGQADAEAAAEQAAAHgGADIgFADIAAABQAAADADAFIAHAHg");
	this.shape_1604.setTransform(3.9,-206.8);

	this.shape_1605 = new cjs.Shape();
	this.shape_1605.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1605.setTransform(-8.2,-206.3);

	this.shape_1606 = new cjs.Shape();
	this.shape_1606.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1606.setTransform(-13,-199.5);

	this.shape_1607 = new cjs.Shape();
	this.shape_1607.graphics.f("#FFFFFF").s().p("AgoA0QgRgMABgWQABgSAPgFQgIgDAAgIQAAgLALgEQAIgFALABQANABAIALQAIAKAAANIgiAAQgHAAgFAEQgGAFAAAHQABASAXAEQAHACAKAAQATAAAMgOQALgLAAgRQABgVgLgNQgMgPgXgBQgZAAgNATIgNAAQAGgOAPgIQANgIASAAQAZABARARQARAUAAAZQAAAagPASQgQATgaAAQgZAAgPgLgAgbgWQADACABACQABAFgCADIAVAAQgBgFgEgEQgFgFgHABIgHABg");
	this.shape_1607.setTransform(-25.8,-200.9);

	this.shape_1608 = new cjs.Shape();
	this.shape_1608.graphics.f("#FFFFFF").s().p("AAOAmQAKAAAIgFQAJgEACgJQAEgKAAgKQgCgKgHgIQgIgJgLAAQgSABgEAQQADgCAEAAQAGAAADACQAKAIAAARQgBAQgNAJQgMAKgSAAQgSgBgMgKQgMgLAAgSQAAgRANgJQgDgBgDgDQgDgEABgFQgBgJAJgGQAGgEAKAAQAJgBAGAGQAGAFACAJQALgSAWgBQATAAALAPQAMAPAAASQAAAWgOANQgOAOgWABgAgqgFQgGAFAAAGQAAAJAJAHQAJAEALAAQALABAIgFQAIgEAAgJQABgEgCgDQgCgCgDAAQgDAAgCABQgBADAAACIgLAAIgBgIIABgJIgPAAQgHAAgFAGgAgogjQADABACAFQABAEgBAEIAMAAQAAgIgFgDQgDgEgGAAIgDABg");
	this.shape_1608.setTransform(-38.3,-199.5);

	this.shape_1609 = new cjs.Shape();
	this.shape_1609.graphics.f("#FFFFFF").s().p("AgfAkQgOgOAAgVQAAgWAOgOQAOgOAUAAQAXAAAKAKQAIAHAAAJIgLAAQgBgHgJgEQgIgDgKAAQgOAAgKAJQgLAKAAAOQAAAMAKAJQAJAJAOAAQALAAAIgGQAJgHAAgKQAAgOgMgDQAEAEAAAKQAAAFgGAFQgGAFgHAAQgIAAgGgGQgGgFAAgJQAAgJAIgGQAHgFAKAAQAPAAALAJQALAIAAAPQAAASgNAMQgNANgSAAQgTAAgOgOgAgCgLQgCADAAADQAAAEACABQACACADAAQAEAAACgCQADgBAAgEQAAgDgDgDQgCgCgEAAQgDAAgCACg");
	this.shape_1609.setTransform(-49.9,-199.5);

	this.shape_1610 = new cjs.Shape();
	this.shape_1610.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1610.setTransform(-63.1,-206.3);

	this.shape_1611 = new cjs.Shape();
	this.shape_1611.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1611.setTransform(-67.4,-199.5);

	this.shape_1612 = new cjs.Shape();
	this.shape_1612.graphics.f("#FFFFFF").s().p("AgxAiIAAgTIBXAAIAAgfIAGgHIAGgKIAABDg");
	this.shape_1612.setTransform(-79.9,-194.7);

	this.shape_1613 = new cjs.Shape();
	this.shape_1613.graphics.f("#FFFFFF").s().p("AAAAnQgHALgOAAQgTAAgJgOQgJgNAAgWQABgXAQgPQALgKAOgBQAGgBAFACIAAALQgHgDgJADQgKADgGAKQgGAJAAALQAAAdAUAAQAIAAAFgEQAFgEAAgIIAAgKIALAAIAAAKQAAAIAFAEQAFAEAIAAQAJAAAGgGQAGgGAAgKQAAgHgGgGQgHgGgJAAIgcAAQAAgRAHgJQAHgHAOAAQAaAAAAAVIAAAFIgJAAQAJADAFAJQAFAJAAAJQAAATgJALQgJAMgQAAQgRAAgIgLgAAJgZIARAAIAGAAIABgDQAAgJgMAAQgLAAgBAMg");
	this.shape_1613.setTransform(-80.2,-199.6);

	this.shape_1614 = new cjs.Shape();
	this.shape_1614.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1614.setTransform(-94.9,-199.5);

	this.shape_1615 = new cjs.Shape();
	this.shape_1615.graphics.f("#FFFFFF").s().p("AgbA6QgMgGgHgKQgKgPAAgWQAAgOAIgNQAJgMALABQALAAAEAIQAFgJAMAAQALgBAHAJQAIAHABAMQAAAMgKAKQgKAKgMAAQgMAAgJgIQgIgHAAgMIACgHIADgHQABgBAAAAQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgGAAgDAIQgDAHAAAIQgBARANAKQANALAQAAQARAAANgNQANgNgBgSQAAgUgLgOQgLgOgVABQgbAAgKASIgNAAQAGgOAOgJQANgHARAAQAaAAAPAPQARARABAcQABAcgQAUQgQATgbAAQgOAAgMgGgAgQgEQAAAEAFAEQAFAEAGAAQAHAAAGgFQAGgDAAgHIgBgDQgBAEgFACQgEADgGABQgNAAgDgNIgCAJgAgCgSQAAADACACQABACADAAQADAAACgCQACgCAAgDQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQgCgCgDAAQgGAAAAAHg");
	this.shape_1615.setTransform(-103.1,-200.9);

	this.shape_1616 = new cjs.Shape();
	this.shape_1616.graphics.f("#FFFFFF").s().p("AgRAqQgGAHgQAAQgJAAgGgFQgHgGAAgJQAAgKAEgHIgHAAIAAgHIAGgHIAJgKIgDAAQgFAAgDgDQgDgEAAgGQAAgJAGgHQAHgHAJAAQARgCAHALQAQgLAUACQAUABANAOQANAOAAATQABAggYAOQgFADgLAAIgJAAIAAgIQgHAIgKAAIgBAAQgJAAgHgHgAgHgeQAPACAJAKQAMAOAAAPQAAARgJAJIAFABQAIAAAHgHQAHgGABgJIABgHQAAgTgNgMQgLgJgTAAQgGAAgHABgAgfgNQgKAIgIAIIAIAAQgFAHABAHQAAAEADACQAEADAFAAQAHAAADgIQABgFAAgNIALAAQAAAOABAFQACAHAHAAQAFgBAEgGQAEgFAAgIQAAgIgFgIQgGgHgHgBIgGAAQgNAAgGAFgAgtgiQAEABABAFQABAEgCAEIAHgGIAGgGQgCgEgGgBQgHAAgCADg");
	this.shape_1616.setTransform(-122,-199.5);

	this.shape_1617 = new cjs.Shape();
	this.shape_1617.graphics.f("#FFFFFF").s().p("AAgAXQAIgGAAgHIAAgDQgCgGgMgCQgLgDgPAAQgOAAgKADQgNACgCAGIAAADQAAAIAHAFIgKAAQgKgDAAgOQAAgFACgCQAGgMAPgFQALgEASAAQAqAAAJAVQACADAAAEQAAANgKAEg");
	this.shape_1617.setTransform(-135,-205.8);

	this.shape_1618 = new cjs.Shape();
	this.shape_1618.graphics.f("#FFFFFF").s().p("Ag5AcQgCgJACgHIgJAAIAAgKIAHgHIAJgJQgGAAgEgEQgDgEAAgGQAAgJAHgGQAGgFAKgBQAPgBAHALQAOgKAUAAIAHAAQAUACANAPQAMAPgBATQgBAUgNAOQgNAOgTAAIgHgBIAAgKQgNAKgTABIgDAAQgeAAgGgWgAgFgcQAFABAGADQAGAEAEAEQAJAKABAMQAAATgKAMIAEABQAJAAAGgEQANgJABgQQABgLgFgJQgFgKgLgFQgIgEgNAAQgIAAgFACgAghgMQgIAFgLAKIAIAAQgDAHACAIQAEANAVgBQALgBAIgHQAHgHABgKQABgJgHgHQgGgHgKAAQgKAAgIAGgAgpglQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQAEABACADQACADgCAEIAGgEIAFgEQgDgEgEgBIgDAAIgDAAg");
	this.shape_1618.setTransform(-135.7,-199.5);

	this.shape_1619 = new cjs.Shape();
	this.shape_1619.graphics.f("#FFFFFF").s().p("AgWAuIAAgLQADACAHAAQAGAAAEgLQADgLAAgOQAAgMgDgMQgEgMgGgBQgGAAgEADIAAgMQAFgEAKAAQANAAAJASQAIAPAAARQAAASgIAPQgJAQgNAAQgJAAgGgEg");
	this.shape_1619.setTransform(-145.2,-199.5);

	this.shape_1620 = new cjs.Shape();
	this.shape_1620.graphics.f("#FFFFFF").s().p("AgoA0QgSgNABgUQABgQANgIQgJgBAAgKQAAgJALgFQAHgEALABQAQgBAGALQAEgJAOgBIAIAAIAGgFIAEgFQAGgKABgLIAQAAIgDALQgCAHgEAFIgKAKQAIAEACAKQABAGgDAEIgJAAQAFABAFAFQAIAHAAAPQAAATgSANQgRAMgVAAQgXAAgRgNgAglAEQgFAGAAAHQABALANAHQAMAGAQAAQAQAAALgFQAOgHABgNQAAgHgFgFQgFgEgLAAIgtAAQgIAAgFAEgAAOgWQgFABgCACQgEAEAAAEIATAAQAGAAAFACQABgCgBgFQgDgGgMAAIgEAAgAgegVQADACABADQABADgCACIAUAAQgBgHgHgDIgHgBQgEAAgEABg");
	this.shape_1620.setTransform(-153.1,-201);

	this.shape_1621 = new cjs.Shape();
	this.shape_1621.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1621.setTransform(-171.7,-199.5);

	this.shape_1622 = new cjs.Shape();
	this.shape_1622.graphics.f("#FFFFFF").s().p("AgrAyQgJgLABgNQACgNALgFIgUAAIAAgKIAQAAQgDgEACgFQABgJAJgFQAIgDALABQANAAAIALQAIAKAAAOIgaAAQgIAAgGADQgHAFgBAHQAAAIALAFQAKAFARABQAngBAAgmQAAgMgJgLQgJgMgPAAIgtAAQgHABgDgFQgEgEABgFQACgMAQgEQALgEAVABQAcAAAQAQQACACABAEQABAFAAADIgNAAQAIAFAGALQAGANAAALQABAagOAQQgOARgaAAQgiAAgOgPgAgXgPQADACABADQABAEgCAEIAVAAQgBgGgEgDQgFgFgHAAIgHABgAgZgtQAAACAIAAIAfAAQAIAAAJAFIAHADQABgEgHgGQgIgHgXAAQgbAAABAHg");
	this.shape_1622.setTransform(-184.3,-201.1);

	this.shape_1623 = new cjs.Shape();
	this.shape_1623.graphics.f("#FFFFFF").s().p("AgMAYIAAgUIgJAAIAAgIIAJAAIAAgNIgKAAIAAgJIAZAAQAJAAAFAEQAGAEgBAIQAAAIgGADQgIADgJAAIAAAXgAgBgRIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1623.setTransform(-198.5,-206.3);

	this.shape_1624 = new cjs.Shape();
	this.shape_1624.graphics.f("#FFFFFF").s().p("AAKAwIAAgIQgIAGgGACQgGACgMAAQgRgBgLgLQgLgLAAgQQgBgOAFgKIALAAQgBAEACABQABACAEAAQAFAAAFgDQAJgGACgHIABgEQAAgIgGgCQgEgCgFACQAIACAAAJQAAAFgEADQgDADgFAAIgFgBQgJgCAAgLQAAgEABgDQAFgOASAAQAHAAAGAEQAFADACAGQAKgNAUAAQAVABALAUQAHAOAAAPQAAAhgYAMQgIAEgIAAIgMgCgAALgMQALAJAAAPQAAAPgJAKIAIABQAIAAAFgEQAMgJAAgTQAAgMgJgKQgIgJgMgBQgQAAgGAJIACAAQAIAAAGAFgAgxAHQAAAIAEAFQAHAIAOAAIAIgBQAJgBAGgGQAFgHAAgHQAAgFgCgDQgCgFgJAAQgEgBgGAEIgKAGQgFACgGAAQgFAAgDgCIgBAFg");
	this.shape_1624.setTransform(-202.8,-199.5);

	this.shape_1625 = new cjs.Shape();
	this.shape_1625.graphics.f("#FFFFFF").s().p("AgrAkQgOgMAAgUQAAgHAEgIQADgHAGgEQgGgCAAgIIABgFQAFgMAVAAQARAAAGAKQAHgKASAAQAJAAAHAEQAHAFABAHQABAGgDAGQAFADADAHQADAIAAAHQAAAVgPAMQgQANgbAAQgbAAgQgOgAgngKQgFAFABAIQAAAMAPAGQAMAFAQAAQAQAAAMgFQAPgGAAgNQABgHgFgFQgFgFgHAAIg2AAQgHAAgFAFgAAMgiQgFADAAAFIAbAAIAAgDQAAgDgDgDQgEgCgEAAIgBAAQgFAAgFADgAgeglQACACABADQABAEgCACIAVAAQgBgJgMgCIgFgBIgFABg");
	this.shape_1625.setTransform(-215.4,-199.5);

	this.shape_1626 = new cjs.Shape();
	this.shape_1626.graphics.f("#FFFFFF").s().p("AgpAuQgTgOABgYQAAgUANgMQAGgHALgDQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAAAgBgBQgBgDAAgDQAAgDACgCQAFgJAKgCIAVgBQASABALAHQANAJADARIgyAAQgQAAgKAKQgIAKgBAOQAAAQAMALQAMALAPAAQAMAAAKgDQALgFAGgIQAHgKgBgJIALAAIAAAEQAAAKgFAIQgHANgPAHQgOAIgSAAQgZgBgRgOgAgJguQACACAAACQAAABAAABQAAABAAAAQAAABgBAAQAAABgBAAIApAAQgIgIgOgCIgIgBQgHAAgEACg");
	this.shape_1626.setTransform(-227.6,-198.4);

	this.shape_1627 = new cjs.Shape();
	this.shape_1627.graphics.f("#FFFFFF").s().p("AgOA7IAAgOIAKAAQAHAAACgDQABgDAAgNIg5AAIAAgOIA+hFIAUAAIAABDIAVAAIAAAQIgVAAQAAANACADQACADAIAAIAJAAIAAAOgAgeAKIAlAAIAAgpg");
	this.shape_1627.setTransform(-246,-200.4);

	this.shape_1628 = new cjs.Shape();
	this.shape_1628.graphics.f("#FFFFFF").s().p("AglAtQgLgPgBgaQAAgbAOgSQAPgTAaAAQANAAALAFQAPAHAAAMQgBAHgFAEQgEAFgHAAQgFAAgEgDQgFgEAAgFQAAgIAJgEQgHgEgLAAIgCAAQgNAAgFAQQgFALAAAQQARgKANAAQAQAAAMALQAMAKAAAQQAAASgQAMQgOAJgSAAIgCAAQgXAAgOgQgAgOAHQgFAGAAALQAAAXATAAQAKAAAGgGQAFgGAAgLQAAgYgVAAQgKAAgEAHg");
	this.shape_1628.setTransform(-257.7,-200.4);

	this.shape_1629 = new cjs.Shape();
	this.shape_1629.graphics.f("#FFFFFF").s().p("AgcA4QgNgHgBgLQAAgIAFgEQAEgFAHABQAHgBADAEQADAEABAFQAAAIgIADQAIAFAGAAQARAAAGgPQAEgKAAgUQgQALgOAAQgQAAgMgLQgLgKgBgQQAAgTAPgKQAOgKATAAQAZAAANAQQAMAPABAbQAAAagNASQgPATgZAAQgNABgMgGgAgQgnQgEAHAAAKQgBAKAGAHQAGAFAJAAQAJAAAGgFQAFgHAAgKQAAgKgFgHQgGgHgJgBQgKAAgGAIg");
	this.shape_1629.setTransform(-269.8,-200.3);

	this.shape_1630 = new cjs.Shape();
	this.shape_1630.graphics.f("#FFFFFF").s().p("AgjA7IAAgOIAKAAQAIAAADgCQADgCAAgIIAAg7IgYAAIAAgNQANgBAHgDQAHgDAGgLIAOAAIAABbQAAAHADACQADADAHgBIALAAIAAAOg");
	this.shape_1630.setTransform(-280.4,-200.4);

	this.instance_1 = new lib.Bitmap2copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(250.8,9.5);

	this.instance_2 = new lib.Bitmap1copy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(250.8,-195.5);

	this.shape_1631 = new cjs.Shape();
	this.shape_1631.graphics.f("#FFFFFF").s().p("AgsAAIgfhWICWBWIiWBXg");
	this.shape_1631.setTransform(-286.2,205.7);

	this.shape_1632 = new cjs.Shape();
	this.shape_1632.graphics.f("#FFFFFF").s().p("AgDA7QgHANgSgBQgPAAgKgNQgJgMAAgSQAAgdARgOQALgHAdAAIAAALQgYgBgHAGQgGAFgDAFQgCAFAAAJQAAAMAFAHQAFAJAKgBQASAAAAgOIAAgPIAMAAIAAAPQAAAPAUAAQAMAAAGgMQAGgKAAgOQAAgVgOgMQgIgIgMABIgxAAQgRAAAAgPQAAgaA0AAQAeAAAPANQAJAJgCAMIgNAAIAAgCQAAgJgNgFQgLgEgOAAQgMAAgIACQgKADAAAFQgBAFAIAAIAnAAQARAAANANQARARAAAcQAAAYgKAOQgLARgTAAQgSABgIgNg");
	this.shape_1632.setTransform(99.6,205.8);

	this.shape_1633 = new cjs.Shape();
	this.shape_1633.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgJQAEgFABgFIAABHg");
	this.shape_1633.setTransform(76.4,212.8);

	this.shape_1634 = new cjs.Shape();
	this.shape_1634.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgRANgHQgGgCAAgKQAAgDACgDQAGgOAVAAQASAAAGALQAIgLATAAQAJAAAJAHQAHAFAAAKQAAAHgCAEQALAKAAANQAAAVgPANQgRAPgdAAQgdAAgRgPgAgtACQABANAPAHQANAEAQABQASgBAMgEQAQgHAAgNQAAgFgEgGQgHAFgMADQAFACABAHQgBAGgIAEQgIACgKAAQgLAAgIgDQgJgDAAgIQAAgNAXAAIAKAAQAPgDADgDIg0AAQgSAAAAAPgAgKAGQAAAEALABQALgBAAgDQgBgFgKAAQgLAAAAAEgAANgjQgFAFgBAGIAeAAIABgEQAAgBAAgBQAAgBAAAAQgBgBAAAAQAAgBAAAAQgEgGgJAAQgHAAgEAEgAgfgnQADADABAEQAAAEgDAEIAYAAQAAgLgNgEIgGgBQgEAAgCABg");
	this.shape_1634.setTransform(44.2,207.8);

	this.shape_1635 = new cjs.Shape();
	this.shape_1635.graphics.f("#FFFFFF").s().p("AgsAAIgehWICVBWIiVBXg");
	this.shape_1635.setTransform(-286.2,115.7);

	this.shape_1636 = new cjs.Shape();
	this.shape_1636.graphics.f("#FFFFFF").s().p("AA3BAQgKgFAAgKQgBgIAGgEQgXABgPgNQgHgGgDgIQgCAJgGAGQgJAHgGACQgIACgNAAQgVAAgLgQQgKgNAAgUQAAgYAIgMQgGABgFALQgFALAAAQQAAAOAJAOQAGALAJAGIgRAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAEgLAGgHQALgMAPAAIAAAGIAEgDQAIgEAMAAIAHAAIAAAMQAMgMATAAQARAAAHAEQAHgEASAAQAZAAAKAMQAIAKABAOIgmAAQgLAAgIAHQgHAHgBALQgBALAKAJQAKAJAOABQAVABAPgIIAAAMQgIADgGACQgNADgBAGQAAAEAEACQADACAFAAQAGAAAFgDQAEgDAAgGIgBgEIAMAAIABAFQAAALgJAIQgJAGgMABIgEAAQgKAAgHgDgAg+gsQgGAKAAANQAAAOAHAIQAIAKAOABQALAAAJgFQAIgFABgIQAAgFgDgEQgDgFgGABQgEAAgCADQgDADAAAEIABAFIgNAAQgCgOABgLQACgPAIgJIgHgBQgNAAgIAKgAgEgdQAFAEACAHQAFgMALgGIgFgEQgCgCAAgEQgGgDgLAAQgIABgIAFQgIAGgBAIQAFgEAIAAQAHAAAGAEgAAlg1QADACAAADQABAEgDACIAFAAIAfAAQgEgIgJgDQgFgBgIAAQgHAAgEABg");
	this.shape_1636.setTransform(10,169.2);

	this.shape_1637 = new cjs.Shape();
	this.shape_1637.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgZQAAgQAHgMQAIgNANAAQAOAAACALQAEgMAOAAQAKAAAIAJQAHAJAAAKQAAANgJAJQgJAJgMAAQgLABgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQAMgMAAgTQAAgNgFgKQgIgLgNAAIhFAAIAAgKQASAAABgBIgFgDQgDgCAAgEQAAgNAPgFQAIgBASAAQAQAAAJACQAOAEAIAKQAHAIgBALIgLAAQgCgLgMgGQgLgFgPAAQgLAAgFABQgHADgBAGQAAABABAAQAAAAAAABQAAAAABABQAAAAABABQABAAAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHADQAYALAAAlQAAAbgRASQgQASgZAAQgYAAgRgQgAgOACQAAAGAEAEQAFAEAGABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAABQAAAAABABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAABgBQAAAAAAgBQABgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBABAAAAg");
	this.shape_1637.setTransform(-125.9,165.8);

	this.shape_1638 = new cjs.Shape();
	this.shape_1638.graphics.f("#FFFFFF").s().p("AARA0IAAgNIAGABQAMAAAIgKQAJgLAAgMQAAgNgHgKQgHgLgMgBIgFgBQgVAAgDATQACgCAEAAQAFgBAEABQAIADADAHQAEAHgBAKQgBAQgNALQgMAKgQAAQgZAAgNgOQgKgLAAgNQAAgIAEgGQAEgIAHgEIgOAAQgDAAgCACIAAgNIAOAAQgFgBAAgJIABgGQAFgMASAAQAMAAAGAEQAIAFABAJQAKgSAYAAQATgBANARQAMARgBAUQgBAUgOAPQgNAPgTAAIgFAAgAgogGQgGAFgBAGQAAAKAIAGQAIAGAOAAQAMAAAHgEQAKgFAAgLQAAgIgIAAQgDgBgCACQgDACAAAEIgLAAIAAgKIABgIIgMAAQgIAAgGAGgAgkgmQADABABAGQAAAEgCAEIAPAAQAAgIgEgEQgEgEgGAAIgDABg");
	this.shape_1638.setTransform(-245,167.7);

	this.shape_1639 = new cjs.Shape();
	this.shape_1639.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDAAgHIABgGQAFgMAWAAQASgBAGALQAIgLATABQAJAAAHADQAIAFABAIQABAGgEAGQAFAEADAHQAEAIAAAIQAAAVgPAOQgRANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABAMAPAHQAMAFASAAQARAAAMgFQAPgHACgMQAAgJgFgFQgFgGgIABIg5AAQgHgBgFAGgAANglQgFAEgBAFIAdAAIAAgDQAAgDgEgDQgDgCgFgBIgBAAQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQgBgJgNgDIgFAAIgFABg");
	this.shape_1639.setTransform(36.1,142.8);

	this.shape_1640 = new cjs.Shape();
	this.shape_1640.graphics.f("#FFFFFF").s().p("AgoA3QgRgQAAgXQAAgRAHgMQAIgNANAAQAOAAACALQAEgMAOAAQALAAAHAJQAHAIAAAKQAAAOgJAKQgJAIgMABQgLAAgIgHQgJgGAAgLIADgPQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQAMgMAAgTQABgNgHgJQgGgMgOAAIhFAAIAAgLQASABAAgCIgEgCQgDgBAAgFQAAgNAPgFQAIgCASAAQAPAAAKADQANAEAJAKQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgHACAAAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHAEQAYAKAAAlQAAAbgRASQgPASgaAAQgYgBgQgQgAgOACQAAAGAEAEQAFAEAFABQAHgBAFgEQAFgEAAgHQgFAHgJABQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAABQAAAAABABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_1640.setTransform(-90.5,140.8);

	this.shape_1641 = new cjs.Shape();
	this.shape_1641.graphics.f("#FFFFFF").s().p("AA3BAQgKgFAAgKQgBgIAGgEQgXABgPgNQgHgGgDgIQgCAJgGAGQgJAHgGACQgIACgNAAQgVAAgLgQQgKgNAAgUQAAgYAIgMQgGABgFALQgFALAAAQQAAAOAJAOQAGALAJAGIgRAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAEgLAGgHQALgMAPAAIAAAGIAEgDQAIgEAMAAIAHAAIAAAMQAMgMATAAQARAAAHAEQAHgEASAAQAZAAAKAMQAIAKABAOIgmAAQgLAAgIAHQgHAHgBALQgBALAKAJQAKAJAOABQAVABAPgIIAAAMQgIADgGACQgNADgBAGQAAAEAEACQADACAFAAQAGAAAFgDQAEgDAAgGIgBgEIAMAAIABAFQAAALgJAIQgJAGgMABIgEAAQgKAAgHgDgAg+gsQgGAKAAANQAAAOAHAIQAIAKAOABQALAAAJgFQAIgFABgIQAAgFgDgEQgDgFgGABQgEAAgCADQgDADAAAEIABAFIgNAAQgCgOABgLQACgPAIgJIgHgBQgNAAgIAKgAgEgdQAFAEACAHQAFgMALgGIgFgEQgCgCAAgEQgGgDgLAAQgIABgIAFQgIAGgBAIQAFgEAIAAQAHAAAGAEgAAlg1QADACAAADQABAEgDACIAFAAIAfAAQgEgIgJgDQgFgBgIAAQgHAAgEABg");
	this.shape_1641.setTransform(-176.8,144.2);

	this.shape_1642 = new cjs.Shape();
	this.shape_1642.graphics.f("#FFFFFF").s().p("AgFAyIgNgEQgGgCgEAAQgKAAABAHIgMAAQgCgIAAgJQAAgKACgIQADgJAKgHIAPgMQAIgHAAgHQABgFgCgEQgDgEgFAAQgGAAgEADQgDAEgBAEQAAALAIAFIgRAAQgEgHAAgHIAAgEQABgKAHgGQAIgFALAAQAKAAAHAGQAGAHAAAKQAAAJgFAIIgPAMQgJAIgDAEQgHAHAAAJIAAAEQAFgCAEAAQAGAAAKADQAKADAGAAIAKgBQAGgBAFgGQAGgGABgJQABgIgIgHIgNgMQgGgIgBgJQABgKAHgGQAGgFALAAQAIAAAHAFQAGAFAAAIQABAGgCAFQgDAFgGABQgFAAgEgDQgDgCAAgFQAAgIAGgCQgCgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgMAKgQAAQgJAAgHgCg");
	this.shape_1642.setTransform(-198,142.7);

	this.shape_1643 = new cjs.Shape();
	this.shape_1643.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgLAAQgLgDAAgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADABAEQgBAOgJAEg");
	this.shape_1643.setTransform(-238.4,136.1);

	this.shape_1644 = new cjs.Shape();
	this.shape_1644.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAIAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_1644.setTransform(127.6,117.7);

	this.shape_1645 = new cjs.Shape();
	this.shape_1645.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgbQAAgdARgSQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1645.setTransform(114.8,119.1);

	this.shape_1646 = new cjs.Shape();
	this.shape_1646.graphics.f("#FFFFFF").s().p("AgmA4QgRgLAAgUIAAgFQAAgFAFgGQAEgGAFgDIgVAAIAAgKIARAAQgDgEABgGQACgJAKgFQAIgDALAAQAOABAIALQAJALAAAOIgfAAQgIAAgGAEQgHAFAAAIQAAAMANAHQALAGAQgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAPgIARAAQAcAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgZAAgQgLgAgZgYQADACABAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgHAAIgIABg");
	this.shape_1646.setTransform(97,116.3);

	this.shape_1647 = new cjs.Shape();
	this.shape_1647.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgEIAABGg");
	this.shape_1647.setTransform(45.9,122.8);

	this.shape_1648 = new cjs.Shape();
	this.shape_1648.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_1648.setTransform(1.8,117.9);

	this.shape_1649 = new cjs.Shape();
	this.shape_1649.graphics.f("#FFFFFF").s().p("AgMAVQAKgHAAgFIAAgBIgGgEQgGgEAAgFQAAgFADgEQAEgGAHAAQAFAAAEACQAGAFAAAHQAAAGgDAEQgBAEgFAFIgIAIg");
	this.shape_1649.setTransform(-49.8,122.4);

	this.shape_1650 = new cjs.Shape();
	this.shape_1650.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgbQAAgdARgSQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1650.setTransform(-59.8,119.1);

	this.shape_1651 = new cjs.Shape();
	this.shape_1651.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgbQAAgdARgSQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1651.setTransform(-72.5,119.1);

	this.shape_1652 = new cjs.Shape();
	this.shape_1652.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_1652.setTransform(-82.2,117.9);

	this.shape_1653 = new cjs.Shape();
	this.shape_1653.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgbQAAgdARgSQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1653.setTransform(-135.3,119.1);

	this.shape_1654 = new cjs.Shape();
	this.shape_1654.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgbQAAgdARgSQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgNACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1654.setTransform(-148,119.1);

	this.shape_1655 = new cjs.Shape();
	this.shape_1655.graphics.f("#FFFFFF").s().p("AgWArQgKgIAAgOQAAgMAEgIQADgFAHgIQAIgJAVgRIgoAAIAAgLIA7AAIAAALIgNAKIgNALIgKAOQgFAHAAAJQABAGAEAEQAEACAEAAQALAAAEgJIABgFQAAgEgDgEIAMAAQAGAJgBALQgBAMgJAHQgKAJgMgBQgOAAgIgHg");
	this.shape_1655.setTransform(-157.8,117.9);

	this.shape_1656 = new cjs.Shape();
	this.shape_1656.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAghIAHgIQAEgFABgEIAABGg");
	this.shape_1656.setTransform(-244.2,122.8);

	this.shape_1657 = new cjs.Shape();
	this.shape_1657.graphics.f("#FFFFFF").s().p("AgsAAIgehWICVBWIiVBXg");
	this.shape_1657.setTransform(-286.2,24.7);

	this.shape_1658 = new cjs.Shape();
	this.shape_1658.graphics.f("#FFFFFF").s().p("AgoA4QgRgQAAgZQAAgPAHgNQAIgNAOAAQANAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAJQgJAKgMgBQgLAAgIgGQgJgHAAgJIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgSQAAgOgHgKQgGgLgOAAIhFAAIAAgKQASAAAAgBIgDgDQgEgCAAgDQAAgOAPgEQAIgCASAAQAQAAAJACQANAEAJAKQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFABQgIACABAHQAAABAAAAQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAgAAQALAAAIADQAXALAAAlQAAAbgQASQgQARgaABQgYAAgQgQgAgOACQAAAGAFAEQAEAEAFAAQAHABAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_1658.setTransform(65,74.8);

	this.shape_1659 = new cjs.Shape();
	this.shape_1659.graphics.f("#FFFFFF").s().p("AgpA4QgQgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgEAIQgCAGAAAIQAAARANAKQAMAKARAAQASAAAMgMQAMgMAAgSQAAgOgFgJQgIgMgMAAIgvAAQgWAAAAgNQAAgOARgHQAOgGAVAAQAeAAAQARQAHAIAAALIgMAAQgCgLgMgGQgLgFgPAAQgdAAgBAKQAAABABAAQAAABAAAAQAAABABAAQAAAAABABIAEABIAnAAQAKAAAIAEQAXAKAAAlQAAAbgRASQgPASgaAAQgYAAgRgQgAgOACQAAAGAFAEQADAEAHABQAGAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABAAAAgBQABAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQAAgBgBAAQAAgBAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_1659.setTransform(-76.3,74.8);

	this.shape_1660 = new cjs.Shape();
	this.shape_1660.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgQANgIQgGgCAAgJQAAgEABgDQAHgOAVAAQASAAAGALQAIgLATAAQAKABAIAFQAHAGABAKQAAAHgDAEQALAKAAANQAAAWgPANQgRAOgdgBQgdABgRgQgAgtADQABANAQAGQALAFARgBQASABAMgFQAPgGABgNQAAgHgEgFQgGAEgNAEQAFACAAAHQABAGgKADQgHADgKABQgLAAgIgEQgJgEAAgHQAAgNAXAAIALgBQAOgCADgCIg0AAQgSgBAAAQgAgJAGQAAAFAKgBQAKAAAAgDQABgFgLAAQgKAAAAAEgAAMgjQgEAFAAAGIAdAAIABgFQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBAAAAQgEgGgIAAQgIAAgFAEgAgggmQAEACABAEQABAEgEAEIAYAAQAAgLgNgEIgGgBQgEAAgDACg");
	this.shape_1660.setTransform(-118.5,76.8);

	this.shape_1661 = new cjs.Shape();
	this.shape_1661.graphics.f("#FFFFFF").s().p("AgCBGQgWgBgOgOQgNgNgBgWQAAgOAFgKQAFgKAMgLIAYgVIAJgPIAVAAQgEAHgGAHQgGAHgJAHQAOgBALAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAOAAAKgIQAOgKAAgQQAAgHgDgGQgDgKgKgFQgJgFgMAAQgSAAgLAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAGAEQAFAFAAAJQABAIgIAEQgHAEgLAAIAAAOQgHAHgFAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_1661.setTransform(-130.6,74.9);

	this.shape_1662 = new cjs.Shape();
	this.shape_1662.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPIADAIQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_1662.setTransform(-170,76.7);

	this.shape_1663 = new cjs.Shape();
	this.shape_1663.graphics.f("#FFFFFF").s().p("AAFBCQgEgCgDgFQgHAKgSAAQgWAAgJgYQgFgPABgXQABgQAJgLQAKgNAOAAQAHAAAEAEQAFAEgBAHQAAADgDAEQgDAEABAEQABAKAOABQAEAAAGgCQAFgDABgDQgDAAgEAAQgGAAgFgDQgFgEgBgGQgBgIAGgGQAFgGAIAAQAWABAAAYQAAAMgJAHQgIAIgMABQgKABgIgEQgJgFgDgJIgBgFQAAgHADgEQAAAAABgBQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAgBAAAAQAAgBgBAAQAAAAgBAAQgHgBgGAKQgGAJAAAKQAAANAGALQAHALALAAQARAAAAgTIALAAQgBAKAFAFQAFAEAJAAQAOAAAHgOQAGgLAAgRQAAgVgLgQQgOgSgXgBQgmAAgHAZIgNAAQADgQAQgKQAPgLAYABQAgAAARAYQAOAUAAAXQAAAdgJASQgLAWgXAAQgLAAgFgDgAAAgQQAAAHAHAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgHAAAAAHg");
	this.shape_1663.setTransform(-212,75.1);

	this.shape_1664 = new cjs.Shape();
	this.shape_1664.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1664.setTransform(-245.5,76.7);

	this.shape_1665 = new cjs.Shape();
	this.shape_1665.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1665.setTransform(134.7,51.7);

	this.shape_1666 = new cjs.Shape();
	this.shape_1666.graphics.f("#FFFFFF").s().p("AgGAyQgGgDgGgLQgIgOAAgSQAAgbAPgPQALgLAJAAQANAAAFAIIgFAIQgEgEgHAAQgEAAgEACQgHAFgEAJQgFALAAAPQAAAPAJAOQAFALAJAFg");
	this.shape_1666.setTransform(128.2,51.8);

	this.shape_1667 = new cjs.Shape();
	this.shape_1667.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAIADIAAALQABAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgGgFQgFgGgJAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgEgDgGAAQgEAAgDACg");
	this.shape_1667.setTransform(118.5,51.7);

	this.shape_1668 = new cjs.Shape();
	this.shape_1668.graphics.f("#FFFFFF").s().p("AADAXIAHgIQAEgFAAgCIAAgCIgGgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAGAEAAAIQAAAFgDAFIgHAKQgFAHgEADgAgeAXQADgDAEgFQAEgFAAgCIgBgCIgFgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAFAEAAAIQAAAFgCAFIgHAKQgGAHgDADg");
	this.shape_1668.setTransform(101.9,44.1);

	this.shape_1669 = new cjs.Shape();
	this.shape_1669.graphics.f("#FFFFFF").s().p("AgoA3QgRgQAAgXQAAgRAHgMQAIgNAOAAQANAAACALQAEgMAOAAQALAAAHAJQAHAJAAAKQAAANgJAJQgJAJgMAAQgLABgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgTQAAgNgHgJQgGgMgNAAIhGAAIAAgLQASABAAgBIgDgDQgEgCAAgEQAAgNAPgFQAIgBASAAQAQAAAJACQANAEAJAKQAGAHABAMIgMAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgIACABAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAHADQAYALAAAlQAAAbgQASQgQASgaAAQgYgBgQgQgAgOACQAAAGAEAEQAFAEAFABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBABAAAAg");
	this.shape_1669.setTransform(91.5,49.8);

	this.shape_1670 = new cjs.Shape();
	this.shape_1670.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgLAAQgLgDAAgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADABAEQgBAOgJAEg");
	this.shape_1670.setTransform(78.9,45.1);

	this.shape_1671 = new cjs.Shape();
	this.shape_1671.graphics.f("#FFFFFF").s().p("AAVAXQgDgDgGgHQgGgHgBgDQgCgFAAgFQAAgIAFgEQAFgDAEAAQAIAAAEAGQADAEAAAFQAAAHgGADIgFADIgBACQAAACAEAFQAEAFADADgAgMAXIgJgKIgHgKQgDgFAAgFQAAgIAGgEQAEgDAFAAQAHAAAFAGQACAEAAAFQAAAHgFADIgGADIAAACQAAACAEAFQADAFAEADg");
	this.shape_1671.setTransform(55.8,44.1);

	this.shape_1672 = new cjs.Shape();
	this.shape_1672.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAALg4QAAADADACQACACADAAQAFAAAAgFQAAgDgCgCQgDgCgDAAQgFAAAAAFgAgKg/QgUACAAAHIABAEIAFgBQAHgCANAAIAFAAQgBgDAAgDIABgFIgDAAIgIABg");
	this.shape_1672.setTransform(40.8,51.9);

	this.shape_1673 = new cjs.Shape();
	this.shape_1673.graphics.f("#FFFFFF").s().p("AgdA7QgPgHAAgMQAAgIAEgEQAFgFAIAAQAGAAAFAEQADAEAAAGQABAHgKAEQAKAEAHAAQAQABAGgQQAGgLAAgUQgTALgOAAQgRAAgMgLQgMgLAAgRQAAgTAPgMQAOgKAVAAQAbgBAOASQAMAQAAAcQAAAcgNASQgPAVgbAAQgOAAgMgGgAgQgpQgFAHgBALQAAALAHAGQAGAGAJAAQAJAAAGgGQAGgHAAgKQAAgLgGgHQgFgIgKAAQgLAAgFAIg");
	this.shape_1673.setTransform(22.5,50.8);

	this.shape_1674 = new cjs.Shape();
	this.shape_1674.graphics.f("#FFFFFF").s().p("AgdA7QgPgHAAgMQAAgIAEgEQAFgFAIAAQAGAAAFAEQADAEAAAGQABAHgKAEQAKAEAHAAQAQABAHgQQAEgLAAgUQgRALgPAAQgQAAgNgLQgMgLAAgRQgBgTARgMQANgKAVAAQAbgBANASQANAQAAAcQAAAcgNASQgPAVgbAAQgOAAgMgGgAgQgpQgFAHgBALQAAALAHAGQAFAGAKAAQAJAAAGgGQAGgHAAgKQAAgLgGgHQgFgIgKAAQgLAAgFAIg");
	this.shape_1674.setTransform(10,50.8);

	this.shape_1675 = new cjs.Shape();
	this.shape_1675.graphics.f("#FFFFFF").s().p("AgdA7QgPgHAAgMQAAgIAEgEQAGgFAHAAQAGAAAFAEQADAEAAAGQABAHgKAEQAKAEAGAAQARABAHgQQAEgLAAgUQgRALgOAAQgSAAgMgLQgMgLAAgRQgBgTARgMQANgKAVAAQAagBAOASQAOAQAAAcQAAAcgOASQgQAVgaAAQgOAAgMgGgAgQgpQgFAHgBALQAAALAHAGQAFAGAKAAQAJAAAGgGQAGgHAAgKQAAgLgGgHQgFgIgKAAQgLAAgFAIg");
	this.shape_1675.setTransform(-2.5,50.8);

	this.shape_1676 = new cjs.Shape();
	this.shape_1676.graphics.f("#FFFFFF").s().p("AgkA9IAAgOIAKABQAIAAADgDQADgCAAgIIAAg/IgYAAIAAgNQANAAAHgFQAHgDAHgLIAPAAIAABfQAAAIADACQACADAIAAIALgBIAAAOg");
	this.shape_1676.setTransform(-13.5,50.8);

	this.shape_1677 = new cjs.Shape();
	this.shape_1677.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1677.setTransform(-104.2,53.1);

	this.shape_1678 = new cjs.Shape();
	this.shape_1678.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgcARgSQASgRAcAAQAYAAAQAQQARAQAAAXQAAAUgLANQgLAPgTABQgSAAgKgLQgMgMACgRQABgPAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAJQAAAHAFAGQAGAFAKABQAMgBAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLAQQgKAOAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1678.setTransform(-141.2,53.1);

	this.shape_1679 = new cjs.Shape();
	this.shape_1679.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgEQgFgDAAgHIABgGQAFgMAWAAQASAAAGAKQAIgKATAAQAJAAAHADQAIAFABAIQABAGgEAGQAFAEADAHQAEAIAAAIQAAAWgPANQgRANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABANAPAGQAMAFASgBQARABAMgFQAPgGACgNQAAgJgFgFQgFgGgIABIg5AAQgHgBgFAGgAANglQgFAEgBAFIAdAAIAAgDQAAgDgEgDQgDgCgFAAIgBgBQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_1679.setTransform(-258.6,51.8);

	this.shape_1680 = new cjs.Shape();
	this.shape_1680.graphics.f("#FFFFFF").s().p("AADAXIAHgIQAEgFAAgCIAAgCIgGgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAGAEAAAIQAAAFgDAFIgHAKQgFAHgEADgAgeAXQADgDAEgFQAEgFAAgCIgBgCIgFgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAFAEAAAIQAAAFgCAFIgHAKQgGAHgDADg");
	this.shape_1680.setTransform(78.1,19.1);

	this.shape_1681 = new cjs.Shape();
	this.shape_1681.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgLADQgOADgCAFIgBAEQAAAIAIAFIgKAAQgKgDAAgPQAAgFABgCQAHgNAPgFQAMgEATAAQAtAAAJAWQABADABAEQAAAOgKAEg");
	this.shape_1681.setTransform(1.2,20.1);

	this.shape_1682 = new cjs.Shape();
	this.shape_1682.graphics.f("#FFFFFF").s().p("AAVAXQgDgDgGgHQgGgHgBgDQgCgFAAgFQAAgIAFgEQAFgDAEAAQAIAAAEAGQADAEAAAFQAAAHgGADIgFADIgBACQAAACAEAFQAEAFADADgAgMAXIgJgKIgHgKQgDgFAAgFQAAgIAGgEQAEgDAFAAQAHAAAFAGQACAEAAAFQAAAHgFADIgGADIAAACQAAACAEAFQADAFAEADg");
	this.shape_1682.setTransform(-22.6,19.1);

	this.shape_1683 = new cjs.Shape();
	this.shape_1683.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAIgMASAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAFADAIAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEAAAHIAbAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgEgDgGAAQgEAAgEACg");
	this.shape_1683.setTransform(-39.7,26.7);

	this.shape_1684 = new cjs.Shape();
	this.shape_1684.graphics.f("#FFFFFF").s().p("AgCBFQgXgBgNgOQgOgOgBgVQAAgTALgOQgDgBgDgEIgFgIIgBgLQgBgKAKgKQAIgKAQAAIAKABQAKADABAHIAGgJIAVAAQgFAJgHAJQgIAIgIAFQAOgBALAEQAfAMAAAkQAAAXgRAQQgPAOgXAAIgCAAgAgcgMQgIALAAAMQgBANAJAIQALALARAAQAPAAAKgHQANgLAAgQQAAgHgCgGQgEgJgKgGQgIgFgNAAQgSAAgLAMgAgjgrQgEAGgBAFQAAAHAGADIAEgEIAXgWQgBgEgGgBIgDAAQgMAAgGAKg");
	this.shape_1684.setTransform(-51.8,25.1);

	this.shape_1685 = new cjs.Shape();
	this.shape_1685.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAIAFIgMAAQgJgDgBgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQAAAOgLAEg");
	this.shape_1685.setTransform(-64,20.1);

	this.shape_1686 = new cjs.Shape();
	this.shape_1686.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAFgFQgFgCgBgHIACgGQAEgMAXAAQARgBAHAKQAHgKATABQAKAAAHADQAIAFABAIQABAGgDAGQAFADADAIQADAIAAAIQAAAVgQAOQgQAOgdAAQgdAAgRgPgAgpgLQgFAFAAAJQABAMAPAHQANAFARAAQARAAAMgFQAPgHABgMQABgJgFgFQgFgGgIABIg5AAQgHgBgFAGgAANgkQgFACgBAHIAcAAIABgEQAAgEgEgCQgDgCgFgBIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_1686.setTransform(-64,26.8);

	this.shape_1687 = new cjs.Shape();
	this.shape_1687.graphics.f("#FFFFFF").s().p("AgCA5QgHALgQABQgSAAgJgPQgJgOAAgXQABgZAQgNQAMgLAYAAIAHAAIAAALIgSABQgIABgGAFQgMAIAAARQgBANAEAJQAGAKALAAQAJAAAEgFQAEgFABgIIAAgQIALAAIAAAQQAAASATAAQAMAAAGgOQAGgOgBgUQAAgTgKgOQgOgSgYAAQgOAAgLAGQgMAFgEAJIgOAAQAEgOAPgIQAPgKAVABQAPAAAOAGQAKAEAGAIQASAUABAjQAAAbgKARQgLATgUAAQgQgBgHgLg");
	this.shape_1687.setTransform(-83.2,25.1);

	this.shape_1688 = new cjs.Shape();
	this.shape_1688.graphics.f("#FFFFFF").s().p("AgpA3QgQgPAAgYQAAgRAHgLQAIgOAOAAQANAAACALQAEgMAOAAQAKAAAIAJQAHAIAAAKQAAAOgJAKQgJAIgMABQgLAAgIgHQgJgGAAgLIADgPQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQASAAAMgMQANgMAAgTQgBgNgGgJQgHgMgMAAIhGAAIAAgLQATABgBgCIgDgCQgEgBAAgFQAAgNAPgFQAIgCATAAQAOAAAKADQANAEAJAKQAGAIABALIgMAAQgCgLgMgGQgLgFgQAAQgKAAgFACQgIACAAAGQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIAEQAXAKAAAlQAAAbgQASQgRASgZAAQgYgBgRgQgAgOACQAAAGAFAEQADAEAGABQAHgBAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABAAABgBQAAAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
	this.shape_1688.setTransform(-122.4,24.8);

	this.shape_1689 = new cjs.Shape();
	this.shape_1689.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_1689.setTransform(-135.2,26.7);

	this.shape_1690 = new cjs.Shape();
	this.shape_1690.graphics.f("#FFFFFF").s().p("AgFAyIgNgEQgFgCgFAAQgKAAABAHIgMAAQgCgIAAgJQAAgKADgIQACgJAKgHIAPgMQAIgHABgHQAAgFgDgEQgDgEgEAAQgGAAgEADQgDAEgBAEQgBALAJAFIgRAAQgEgHAAgHIAAgEQABgKAHgGQAIgFALAAQAJAAAIAGQAGAHAAAKQAAAJgFAIIgPAMQgKAIgCAEQgHAHABAJIAAAEQADgCAFAAQAGAAAKADQAKADAFAAIALgBQAGgBAFgGQAGgGABgJQAAgIgGgHIgOgMQgHgIABgJQgBgKAIgGQAGgFALAAQAJAAAGAFQAGAFABAIQABAGgDAFQgDAFgGABQgEAAgEgDQgEgCAAgFQgBgIAHgCQgCgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgLAKgSAAQgIAAgHgCg");
	this.shape_1690.setTransform(-170.8,26.7);

	this.shape_1691 = new cjs.Shape();
	this.shape_1691.graphics.f("#FFFFFF").s().p("AgQAzQgFgCgFgGQgEgFgBgIIABgFIAMAAIAAADQgBAFAEADQAEAEAFAAQAIAAAFgMQAEgJAAgOQAAgngJAAQgFAAAAAFQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAQAGAIAAAJQAAASgTAAQgTAAAAgeQAAgQAHgOIAPAAQgFAEgCAJQgDAHAAAGQAAARAJgBIAEgCQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgIgGAAgIQAAgEACgEQACgDAEgEQAFgEAKAAQAOAAAHASQAGAPAAAUQAAAUgJAPQgLAPgQAAQgJAAgFgBg");
	this.shape_1691.setTransform(-237.2,26.7);

	this.shape_1692 = new cjs.Shape();
	this.shape_1692.graphics.f("#FFFFFF").s().p("AARA0IAAgNIAGABQAMAAAIgKQAJgLAAgMQAAgNgHgKQgHgLgMgBIgFgBQgVAAgDATQACgCAEAAQAFgBAEABQAIADADAHQAEAHgBAKQgBAQgNALQgMAKgQAAQgZAAgNgOQgKgLAAgNQAAgIAEgGQAEgIAHgEIgOAAQgDAAgCACIAAgNIAOAAQgFgBAAgJIABgGQAFgMASAAQAMAAAGAEQAIAFABAJQAKgSAYAAQATgBANARQAMARgBAUQgBAUgOAPQgNAPgTAAIgFAAgAgogGQgGAFgBAGQAAAKAIAGQAIAGAOAAQAMAAAHgEQAKgFAAgLQAAgIgIAAQgDgBgCACQgDACAAAEIgLAAIAAgKIABgIIgMAAQgIAAgGAGgAgkgmQADABABAGQAAAEgCAEIAPAAQAAgIgEgEQgEgEgGAAIgDABg");
	this.shape_1692.setTransform(-247.8,26.7);

	this.shape_1693 = new cjs.Shape();
	this.shape_1693.graphics.f("#FFFFFF").s().p("AASBIQgSgBgIgLQgHAMgOAAQgQAAgJgRQgGgNAAgVQAAgUANgNQANgPAUAAQANAAAJAIQAMAJABAMQAAAHgDAHQgCAHgDACQgKADABAHQAAAMARgBQAMAAAHgNQAGgLAAgPQAAgQgMgNQgJgJgQAAIgxAAQgHAAgDgEQgEgEAAgFQAAgPATgHQAOgFASAAQApAAAKASQACAEAAAFQAAAEgCAEIgFAEQASARAAAdQABAXgMARQgLASgTAAIgCAAgAglAtQgCACAAADQAAAFAEADQADABADAAQAFAAADgEQABgCAAgDQAAgFgDgDQgDgBgDAAQgFAAgDAEgAgKAlQgBgJAGgFIAFgEQADgCABgDIAAgEQAAgHgHgEQgHgEgHAAQgUAAgHASQgCAGgBAHQAAAGACAFQAEgIANgBQAMAAAGAJIAAAAgAAXg0QgCACAAADQAAADACADQACACAEAAQACAAADgCQACgCAAgDQAAgEgCgCQgDgCgCAAQgEAAgCACgAghgwQgBAFAHAAIAkAAQgBgEAAgDQAAgFAEgCIgJgBQgiAAgCAKg");
	this.shape_1693.setTransform(153.8,-18.2);

	this.shape_1694 = new cjs.Shape();
	this.shape_1694.graphics.f("#FFFFFF").s().p("AgoA3QgRgPAAgZQAAgPAHgMQAIgOAOAAQANAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAJQAAAOgJAKQgJAJgMAAQgLgBgIgGQgJgHAAgKIADgPQABgHgHAAQgFAAgDAIQgDAGAAAHQAAASANAKQAMAKARAAQARAAANgMQANgMAAgSQAAgOgHgKQgGgLgNAAIhGAAIAAgKQASABAAgDIgDgCQgEgBAAgEQAAgOAPgEQAIgDASAAQAQAAAJAEQANADAJAKQAGAHAAAMIgLAAQgCgLgNgGQgKgFgQAAQgKAAgFABQgIADABAGQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAHAEQAYAKAAAlQAAAbgQASQgQARgaAAQgYABgQgRgAgOACQAAAGAEAEQAFAEAFAAQAHAAAFgEQAFgEAAgHQgFAHgJAAQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAABg");
	this.shape_1694.setTransform(115.4,-18.2);

	this.shape_1695 = new cjs.Shape();
	this.shape_1695.graphics.f("#FFFFFF").s().p("AgoBFIAAgTIBKAAIAAgSQgOAFgXgCQgVgBgMgLQgMgLAAgRQAAgVATgKQgDgBgDgEQgCgDAAgFIABgEQADgJALgEQAHgCAOAAQAXAAALANQAIAKABAPIglAAQgLAAgHAHQgJAGAAAKQgBALAKAJQALAKAMAAQAYAAAPgIIAAA2gAgNg2QADABAAAFQABAGgDACIAGgBIAfAAQgBgIgLgEQgFgCgIAAIgEAAIgJABg");
	this.shape_1695.setTransform(104.2,-14.6);

	this.shape_1696 = new cjs.Shape();
	this.shape_1696.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCgBgIIABgFQAFgNAXAAQARAAAHAKQAHgKATAAQAKAAAHAFQAIAEABAIQABAGgDAGQAEADAEAIQADAIAAAIQAAAVgQANQgQAOgdABQgdgBgRgPgAgpgLQgFAGAAAHQABANAQAHQAMAEARABQARgBAMgEQAPgHABgNQABgHgFgGQgFgFgIgBIg5AAQgIABgEAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgDgCQgEgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgNgDIgGAAIgFABg");
	this.shape_1696.setTransform(92.5,-16.2);

	this.shape_1697 = new cjs.Shape();
	this.shape_1697.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1697.setTransform(36.2,-16.3);

	this.shape_1698 = new cjs.Shape();
	this.shape_1698.graphics.f("#FFFFFF").s().p("AgFAyIgNgEQgFgCgFAAQgKAAACAHIgNAAQgCgIAAgJQAAgKADgIQADgJAJgHIAPgMQAIgHABgHQAAgFgDgEQgDgEgEAAQgGAAgEADQgDAEgBAEQgBALAJAFIgRAAQgEgHAAgHIAAgEQABgKAHgGQAIgFALAAQAJAAAIAGQAGAHAAAKQAAAJgFAIIgOAMQgLAIgCAEQgHAHABAJIAAAEQADgCAFAAQAGAAAKADQAKADAFAAIALgBQAGgBAFgGQAGgGABgJQAAgIgGgHIgOgMQgHgIABgJQgBgKAIgGQAGgFALAAQAJAAAGAFQAGAFABAIQABAGgDAFQgDAFgGABQgEAAgEgDQgEgCAAgFQgBgIAHgCQgCgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgLAKgSAAQgIAAgHgCg");
	this.shape_1698.setTransform(19.1,-16.3);

	this.shape_1699 = new cjs.Shape();
	this.shape_1699.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_1699.setTransform(-13.8,-14.9);

	this.shape_1700 = new cjs.Shape();
	this.shape_1700.graphics.f("#FFFFFF").s().p("AANBAQgLgFAAgKQAAgIAFgEQgUABgQgMQgOgMABgSQACgWASgKQgIgCABgKQgHADgGALQgHAMAAANQAAAOAJAOQAGALAJAGIgRAAQgGgEgGgKQgIgPAAgQQAAgOAFgMQAFgLAHgGQAMgLALgCIAAAEQAJgFARAAQAZAAALAMQAHAKABAOIglAAQgLAAgHAHQgJAHAAALQgBALALAJQAIAJAOABQAVABAPgIIAAAMQgHADgHACQgNADgBAGQAAAEAEACQADACAGAAQAFAAAFgDQAEgDAAgGIgBgEIAMAAIABAFQAAALgKAIQgHAGgNABIgEAAQgJAAgHgDgAgGg1QADACABAEQABAEgDABIAEAAIAfAAQgDgIgKgDQgFgBgIAAQgHAAgEABg");
	this.shape_1700.setTransform(-121.9,-14.8);

	this.shape_1701 = new cjs.Shape();
	this.shape_1701.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_1701.setTransform(-135.2,-16.3);

	this.shape_1702 = new cjs.Shape();
	this.shape_1702.graphics.f("#FFFFFF").s().p("AgFAyIgNgEQgFgCgFAAQgKAAABAHIgMAAQgCgIAAgJQAAgKADgIQACgJAKgHIAPgMQAIgHABgHQAAgFgDgEQgDgEgEAAQgGAAgEADQgDAEgBAEQgBALAJAFIgRAAQgEgHAAgHIAAgEQABgKAHgGQAIgFALAAQAJAAAIAGQAGAHAAAKQAAAJgFAIIgPAMQgKAIgCAEQgHAHABAJIAAAEQADgCAFAAQAGAAAKADQAKADAFAAIALgBQAGgBAFgGQAGgGABgJQAAgIgGgHIgOgMQgHgIABgJQgBgKAIgGQAGgFALAAQAJAAAGAFQAGAFABAIQABAGgDAFQgDAFgGABQgEAAgEgDQgEgCAAgFQgBgIAHgCQgCgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgLAKgSAAQgIAAgHgCg");
	this.shape_1702.setTransform(-170.8,-16.3);

	this.shape_1703 = new cjs.Shape();
	this.shape_1703.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_1703.setTransform(-192.3,-17.8);

	this.shape_1704 = new cjs.Shape();
	this.shape_1704.graphics.f("#FFFFFF").s().p("AgQAzQgFgCgFgGQgEgFgBgIIABgFIAMAAIAAADQgBAFAEADQAEAEAFAAQAIAAAFgMQAEgJAAgOQAAgngJAAQgFAAAAAFQAAAAAAABQAAABAAAAQABABAAAAQAAABABAAQAGAIAAAJQAAASgTAAQgTAAAAgeQAAgQAHgOIAPAAQgFAEgCAJQgDAHAAAGQAAARAJgBIAEgCQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgIgGAAgIQAAgEACgEQACgDAEgEQAFgEAKAAQAOAAAHASQAGAPAAAUQAAAUgJAPQgLAPgQAAQgJAAgFgBg");
	this.shape_1704.setTransform(-237.2,-16.3);

	this.shape_1705 = new cjs.Shape();
	this.shape_1705.graphics.f("#FFFFFF").s().p("AARA0IAAgNIAGABQAMAAAIgKQAJgLAAgMQAAgNgHgKQgHgLgMgBIgFgBQgVAAgDATQACgCAEAAQAFgBAEABQAIADADAHQAEAHgBAKQgBAQgNALQgMAKgQAAQgZAAgNgOQgKgLAAgNQAAgIAEgGQAEgIAHgEIgOAAQgDAAgCACIAAgNIAOAAQgFgBAAgJIABgGQAFgMASAAQAMAAAGAEQAIAFABAJQAKgSAYAAQATgBANARQAMARgBAUQgBAUgOAPQgNAPgTAAIgFAAgAgogGQgGAFgBAGQAAAKAIAGQAIAGAOAAQAMAAAHgEQAKgFAAgLQAAgIgIAAQgDgBgCACQgDACAAAEIgLAAIAAgKIABgIIgMAAQgIAAgGAGgAgkgmQADABABAGQAAAEgCAEIAPAAQAAgIgEgEQgEgEgGAAIgDABg");
	this.shape_1705.setTransform(-247.8,-16.3);

	this.shape_1706 = new cjs.Shape();
	this.shape_1706.graphics.f("#FFFFFF").s().p("AADBNQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgTQACgVASgKQgDgBgDgDQgCgEAAgEIABgEIACgDQgIgEAAgLQAAgFACgDQAIgOAiAAQAdAAAIANQACAFAAAFQAAALgKADQAKAKAAAPIglAAQgLAAgHAHQgIAHgBAKQgBAMALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPgoQADACAAAEQAAAEgCABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABgAALg4QAAADADACQACACADAAQAFAAAAgFQAAgDgCgCQgDgCgDAAQgFAAAAAFgAgKg/QgUACAAAHIABAEIAFgBQAHgCANAAIAFAAQgBgDAAgDIABgFIgDAAIgIABg");
	this.shape_1706.setTransform(200.1,-41.1);

	this.shape_1707 = new cjs.Shape();
	this.shape_1707.graphics.f("#FFFFFF").s().p("AglA+IAAgPIALAAQAIAAADgCQADgCAAgIIAAg+IgZAAIAAgPQAOAAAHgDQAHgEAHgMIAPAAIAABgQAAAIADACQACACAIAAIAMAAIAAAPg");
	this.shape_1707.setTransform(183.3,-42.2);

	this.shape_1708 = new cjs.Shape();
	this.shape_1708.graphics.f("#FFFFFF").s().p("AAHA+IgIgDQgVgKgGAAQgJAAgCANIgMAAQAAgXAHgMQAHgMAVgJQAQgGAGgEQAMgJAAgNQABgKgJgHQgIgGgKAAQgRAAgIAPIAHgBQAGAAADAEQAEAEAAAGQABAHgFAEQgFAEgIABQgIAAgFgHQgEgFgBgJQAAgRAPgLQAOgJASAAQATAAAOAJQAQALABATQAAARgKAIQgIAGgTAFIgaAIQgMAGgHANQAJgFAHAAQAGAAAMADIAWADQAKAAADgHQADgEABgMIAMAAQAAAvgeAAQgHAAgIgCg");
	this.shape_1708.setTransform(172.1,-42.2);

	this.shape_1709 = new cjs.Shape();
	this.shape_1709.graphics.f("#FFFFFF").s().p("AgdA7QgPgHAAgMQgBgIAFgEQAGgFAHAAQAHAAAEAEQADAEAAAGQAAAHgJAEQAKAEAGAAQARABAHgQQAEgLAAgUQgRALgOAAQgSAAgMgLQgNgLAAgRQABgTAQgMQAOgKAUAAQAagBAOASQAOAQAAAcQAAAcgOASQgPAVgbAAQgOAAgMgGgAgQgpQgFAHgBALQABALAFAGQAGAGAKAAQAKAAAFgGQAGgHAAgKQAAgLgFgHQgHgIgJAAQgLAAgFAIg");
	this.shape_1709.setTransform(159.6,-42.2);

	this.shape_1710 = new cjs.Shape();
	this.shape_1710.graphics.f("#FFFFFF").s().p("AgkA+IAAgPIAKAAQAIAAADgCQADgCAAgIIAAg+IgYAAIAAgPQANAAAHgDQAHgEAHgMIAPAAIAABgQAAAIADACQADACAHAAIALAAIAAAPg");
	this.shape_1710.setTransform(148.6,-42.2);

	this.shape_1711 = new cjs.Shape();
	this.shape_1711.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAEgMQAEgMAAgOQAAgNgDgMQgFgNgGgBQgGAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgJAPQgJARgOAAQgIAAgHgDg");
	this.shape_1711.setTransform(134.8,-41.3);

	this.shape_1712 = new cjs.Shape();
	this.shape_1712.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIAAgFQAGgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAHAGAIAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDgBgEQAAgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_1712.setTransform(125.8,-41.3);

	this.shape_1713 = new cjs.Shape();
	this.shape_1713.graphics.f("#FFFFFF").s().p("AgpA4QgQgRAAgYQAAgPAHgNQAIgNANAAQAOAAACAMQAEgNAOAAQAKAAAIAJQAHAJAAAKQAAANgJAJQgJAKgMgBQgLAAgIgGQgJgHAAgJIADgQQABgHgHAAQgFAAgEAIQgCAGAAAHQAAASANAKQAMAKARAAQASAAAMgMQAMgMAAgSQAAgOgFgKQgIgLgNAAIhFAAIAAgKQASABABgCIgFgDQgDgBAAgEQAAgOAPgEQAIgCASAAQAQAAAJADQAOADAIAKQAHAHgBAMIgLAAQgCgLgMgGQgLgFgPAAQgLAAgFABQgHACAAAHQAAABAAAAQAAAAAAABQAAAAABABQAAAAABABQABAAAAAAQABAAAAABQABAAAAAAQABAAABAAIAfAAQALAAAHADQAYALAAAlQAAAbgRASQgQARgZAAQgYAAgRgPgAgOACQAAAGAEAEQAFAEAGAAQAGABAFgFQAFgEAAgHQgFAIgJgBQgKABgGgKIgBADgAAAgOQAAAAAAABQgBABAAAAQAAABAAAAQgBABAAAAQAAABABABQAAAAAAABQAAAAABABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAAAAABAAQABAAAAAAQABAAAAgBQABAAAAAAQABgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_1713.setTransform(93.3,-43.2);

	this.shape_1714 = new cjs.Shape();
	this.shape_1714.graphics.f("#FFFFFF").s().p("AguAXQgIgDAAgNIABgHQAIgWAuAAQAXAAAMADQARAGACANQABAJgGAGQgHAFgJAAQgIAAgGgEQgGgFAAgHQAAgEACgEIgPAAQgjAAgGAKIgBAFQAAAIAIAEgAAbABQAAADACACQACACADAAQAHAAAAgHQAAgCgCgCQgCgCgDAAQgHAAAAAGg");
	this.shape_1714.setTransform(80.6,-47.9);

	this.shape_1715 = new cjs.Shape();
	this.shape_1715.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1715.setTransform(59.8,-41.3);

	this.shape_1716 = new cjs.Shape();
	this.shape_1716.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRALgIIgQAAIAAgMIAQAAQgEgCAAgGIABgFQAGgPAWAAQATAAAGAMQAHgMASAAQALAAAHAEQAIAFABAJQACAJgEAEIgCAAQAPANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAGADAHAAQAJAAAGgGQAHgHAAgIQgBgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGgBAHQAAAIAHAGQAGAGAIAAQAJAAAFgDQAHgDAAgGIAAgMgAAOgiQgEAEgBAHIAcAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgdglQADACAAAFQABADgCAEIAWAAQAAgJgHgEQgFgDgFAAQgEAAgDACg");
	this.shape_1716.setTransform(17.2,-41.3);

	this.shape_1717 = new cjs.Shape();
	this.shape_1717.graphics.f("#FFFFFF").s().p("AADAXIAHgIQAEgFAAgCIAAgCIgGgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAGAEAAAIQAAAFgDAFIgHAKQgFAHgEADgAgeAXQADgDAEgFQAEgFAAgCIgBgCIgFgDQgGgDAAgHQAAgFADgEQAEgGAIAAQAFAAAEADQAFAEAAAIQAAAFgCAFIgHAKQgGAHgDADg");
	this.shape_1717.setTransform(-41.2,-48.9);

	this.shape_1718 = new cjs.Shape();
	this.shape_1718.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgLgDgQAAQgPAAgLADQgNADgCAFIgBAEQABAIAHAFIgLAAQgKgDABgPQgBgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADgBAEQABAOgLAEg");
	this.shape_1718.setTransform(-77.8,-47.9);

	this.shape_1719 = new cjs.Shape();
	this.shape_1719.graphics.f("#FFFFFF").s().p("AgmA4QgRgLAAgUIAAgFQAAgFAFgGQAEgGAFgDIgVAAIAAgKIARAAQgDgEABgGQACgJAKgFQAIgDALAAQAOABAIALQAJALAAAOIgfAAQgIAAgGAEQgHAFAAAIQAAAMANAHQALAGAQgBQAUAAANgNQALgNABgSQAAgVgMgOQgMgQgYAAQgagBgOAUIgOAAQAHgPAPgIQAPgIARAAQAcAAARATQASAUAAAbQAAAbgQATQgRAVgbAAQgZAAgQgLgAgZgYQADACABAEQABAEgCAEIAWAAQgBgGgEgEQgFgFgHAAIgIABg");
	this.shape_1719.setTransform(-126.6,-42.7);

	this.shape_1720 = new cjs.Shape();
	this.shape_1720.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_1720.setTransform(-198.9,-42.8);

	this.shape_1721 = new cjs.Shape();
	this.shape_1721.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPIADAIQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_1721.setTransform(-218,-41.3);

	this.shape_1722 = new cjs.Shape();
	this.shape_1722.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCAAgIIABgFQAFgNAWAAQASABAGAJQAHgJAUgBQAJAAAHAFQAIAEABAIQABAGgEAGQAGAEACAHQAEAIAAAIQAAAVgPANQgRAOgdAAQgdAAgRgPgAgpgLQgFAGABAHQAAAOAPAGQAMAEASAAQARAAAMgEQAQgGABgOQAAgHgFgGQgFgFgIgBIg5AAQgHABgFAFgAANglQgFAEgBAFIAdAAIAAgCQAAgEgEgDQgDgCgFAAIgBgBQgGAAgEADgAgggnQADACAAADQABAEgCACIAWAAQgBgJgNgCIgFgBIgFABg");
	this.shape_1722.setTransform(-250.4,-41.2);

	this.shape_1723 = new cjs.Shape();
	this.shape_1723.graphics.f("#FFFFFF").s().p("AAVAXQgDgDgGgHQgGgHgBgDQgCgFAAgFQAAgIAFgEQAFgDAEAAQAIAAAEAGQADAEAAAFQAAAHgGADIgFADIgBACQAAACAEAFQAEAFADADgAgMAXIgJgKIgHgKQgDgFAAgFQAAgIAGgEQAEgDAFAAQAHAAAFAGQACAEAAAFQAAAHgFADIgGADIAAACQAAACAEAFQADAFAEADg");
	this.shape_1723.setTransform(-261.1,-48.9);

	this.shape_1724 = new cjs.Shape();
	this.shape_1724.graphics.f("#FFFFFF").s().p("AgMAVQAKgHAAgFIAAgBIgGgDQgGgFAAgGQAAgEADgEQAEgGAHAAQAFAAAEACQAGAEAAAIQAAAGgDAEQgBAEgFAFIgIAIg");
	this.shape_1724.setTransform(220.6,-61.6);

	this.shape_1725 = new cjs.Shape();
	this.shape_1725.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgRANgHQgGgCAAgJQAAgEACgDQAGgNAVAAQARgBAHALQAIgLASABQALAAAHAFQAJAGgBAKQABAHgDAEQALAKAAANQAAAWgQANQgQAOgdgBQgdABgRgPgAgtADQABANAPAGQANAFAQgBQASABAMgFQAPgGABgNQAAgHgEgFQgHAEgMAEQAGACAAAHQgBAGgIADQgIAEgKAAQgLAAgIgEQgJgDAAgIQAAgMAXAAIAKgCQAPgCADgCIg0AAQgTgBABAQgAgKAGQAAAFALAAQALgBAAgEQAAgEgMAAQgJAAgBAEgAANgjQgGAFAAAGIAdAAIACgFQAAAAAAgBQAAgBAAAAQgBgBAAAAQAAgBgBAAQgCgGgKAAQgHAAgEAEgAgfgnQADADABAEQAAAFgDADIAYAAQAAgLgNgEIgGgBQgEAAgCABg");
	this.shape_1725.setTransform(161,-66.2);

	this.shape_1726 = new cjs.Shape();
	this.shape_1726.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgZAMgOQAGgGAGgDQAJgFAJAAIAKAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgLQgJAGgFACQgGADgLAAIgDAAgAAOAnIAEABQAIAAABgFIABgCIgEgMQgCAJgIAJgAAjAPIADAIQACAFAAAEQAIgIAAgKQAAgGgGAAQgHAAAAAHgAgngfQgIAIgBAMQgBAPAHAKQAIAMAPABQAKAAAIgFQAIgFABgJQAAgFgCgDQgEgFgEAAQgEAAgDADQgDACAAAFIABAFIgNAAQgCgNABgLQACgMAJgMIgGgBQgLAAgIAIgAAHgRQAPAGACANQADgFAIgBQAHgBAEADQgCgKgIgIQgIgIgMAAQgKgBgGAEQgJAEgCAIQADgFAIAAIAHABg");
	this.shape_1726.setTransform(147.8,-66.3);

	this.shape_1727 = new cjs.Shape();
	this.shape_1727.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_1727.setTransform(124.9,-67.8);

	this.shape_1728 = new cjs.Shape();
	this.shape_1728.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQAAgRALgIIgPAAIAAgMIAPAAQgCgCAAgGIABgFQAFgPAWAAQATAAAGAMQAHgMATAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQgBAHAHADQAFADAJAAQAHAAAHgGQAHgHgBgIQAAgHgFgFQgGgGgJAAIg1AAQgIAAgGAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAGgDAAgGIAAgMgAAOgiQgFAEABAHIAbAAQACgDAAgEQgBgJgNAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgFgDgFAAQgEAAgEACg");
	this.shape_1728.setTransform(55.1,-66.3);

	this.shape_1729 = new cjs.Shape();
	this.shape_1729.graphics.f("#FFFFFF").s().p("AAMBAQgJgFgBgKQgBgIAHgEQgWABgOgMQgPgMABgSQACgWARgKQgGgCABgKQgIADgHALQgGAMAAANQAAAOAIAOQAHALAIAGIgQAAQgGgEgGgKQgIgPAAgQQAAgOAGgMQAEgLAHgGQAMgLALgCIAAAEQAJgFARAAQAZAAAKAMQAJAKAAAOIgmAAQgKAAgIAHQgIAHAAALQgBALAKAJQAKAJANABQAVABAPgIIAAAMQgHADgHACQgNADAAAGQgBAEAEACQAEACAEAAQAGAAAEgDQAFgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgJAGgMABIgFAAQgIAAgIgDgAgGg1QADACABAEQABAEgEABIAFAAIAfAAQgEgIgJgDQgFgBgIAAQgHAAgEABg");
	this.shape_1729.setTransform(-11.3,-64.8);

	this.shape_1730 = new cjs.Shape();
	this.shape_1730.graphics.f("#FFFFFF").s().p("AgFAyIgMgEQgHgCgEAAQgKAAABAHIgMAAQgCgIAAgJQAAgKACgIQADgJAKgHIAPgMQAIgHAAgHQABgFgCgEQgEgEgFAAQgFAAgEADQgEAEAAAEQAAALAIAFIgQAAQgGgHAAgHIABgEQABgKAHgGQAHgFALAAQALAAAHAGQAGAHAAAKQAAAJgGAIIgOAMQgJAIgEAEQgFAHgBAJIAAAEQAEgCAFAAQAFAAALADQAKADAGAAIAKgBQAGgBAFgGQAGgGAAgJQACgIgIgHIgNgMQgGgIgBgJQABgKAHgGQAHgFAKAAQAJAAAGAFQAGAFAAAIQABAGgDAFQgCAFgGABQgFAAgEgDQgDgCAAgFQAAgIAGgCQgCgDgEAAQgEAAgDADQgDADAAAEQAAAJAPAHQATAJAAATQAAARgNAKQgLAKgRAAQgKAAgGgCg");
	this.shape_1730.setTransform(-60.2,-66.3);

	this.shape_1731 = new cjs.Shape();
	this.shape_1731.graphics.f("#FFFFFF").s().p("AgmA7QgRgKgBgTQgBgUAQgHQgFgBgDgDQgDgDAAgFIABgFQAGgOAbACQAPABAIALQAHAKgBAOIgiAAQgRAAAAANQAAAKANAFQAKAEAQAAQASAAALgJQAMgJABgOQABgUgKgMQgIgJgOAAIgpAAQgTAAAAgNQAAgXAyAAQASAAAQAIQARAJAAAPIgNAAQAAgJgOgFQgMgFgOAAQgZAAgBAHQgBAFAJAAIAhAAQAOAAALAIQATAPAAAiQAAAXgSAPQgSAPgXAAQgVAAgPgJgAgcgOQADACAAAEQABAFgDADIAYAAQgBgHgFgEQgFgEgJAAIgFABg");
	this.shape_1731.setTransform(-81.7,-67.8);

	this.shape_1732 = new cjs.Shape();
	this.shape_1732.graphics.f("#FFFFFF").s().p("AgPAzQgGgCgGgGQgDgFAAgIIAAgFIAMAAIgBADQABAFADADQADAEAHAAQAHAAAFgMQAEgJAAgOQAAgngJAAQgFAAAAAFQAAAAAAABQAAABAAAAQABABAAAAQAAABAAAAQAHAIAAAJQAAASgSAAQgUAAAAgeQAAgQAGgOIAPAAQgEAEgDAJQgCAHAAAGQAAARAJgBIADgCQABAAAAgBQABAAAAAAQAAgBAAgBQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQgGgGAAgIQAAgEACgEQABgDAEgEQAEgEAKAAQAPAAAHASQAGAPAAAUQAAAUgKAPQgKAPgQAAQgJAAgEgBg");
	this.shape_1732.setTransform(-126.5,-66.3);

	this.shape_1733 = new cjs.Shape();
	this.shape_1733.graphics.f("#FFFFFF").s().p("AARA0IAAgNIAGABQAMAAAIgKQAJgLAAgMQAAgNgHgKQgHgLgMgBIgFgBQgVAAgDATQACgCAEAAQAFgBAEABQAIADADAHQAEAHgBAKQgBAQgNALQgMAKgQAAQgZAAgNgOQgKgLAAgNQAAgIAEgGQAEgIAHgEIgOAAQgDAAgCACIAAgNIAOAAQgFgBAAgJIABgGQAFgMASAAQAMAAAGAEQAIAFABAJQAKgSAYAAQATgBANARQAMARgBAUQgBAUgOAPQgNAPgTAAIgFAAgAgogGQgGAFgBAGQAAAKAIAGQAIAGAOAAQAMAAAHgEQAKgFAAgLQAAgIgIAAQgDgBgCACQgDACAAAEIgLAAIAAgKIABgIIgMAAQgIAAgGAGgAgkgmQADABABAGQAAAEgCAEIAPAAQAAgIgEgEQgEgEgGAAIgDABg");
	this.shape_1733.setTransform(-137.2,-66.3);

	this.shape_1734 = new cjs.Shape();
	this.shape_1734.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgOAAgMADQgNADgCAFIAAAEQAAAIAHAFIgLAAQgKgDAAgPQABgFABgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADAAAEQAAAOgKAEg");
	this.shape_1734.setTransform(-181.5,-72.9);

	this.shape_1735 = new cjs.Shape();
	this.shape_1735.graphics.f("#FFFFFF").s().p("AgCA5QgHAMgQAAQgSAAgJgPQgJgOAAgXQABgZAQgNQAMgMAYAAIAHAAIAAANIgSAAQgIABgGAEQgMAJAAARQgBAOAEAIQAGAKALAAQAJAAAEgGQAEgEABgIIAAgQIALAAIAAAQQAAASASAAQANAAAGgPQAGgNgBgUQAAgUgKgNQgOgSgYAAQgOAAgLAFQgMAGgEAJIgOAAQAEgOAPgJQAPgIAVgBQAPAAAOAHQAKAFAGAGQASAVABAjQABAbgLARQgLASgUABQgQAAgHgMg");
	this.shape_1735.setTransform(-214.7,-67.9);

	this.shape_1736 = new cjs.Shape();
	this.shape_1736.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1736.setTransform(-225.9,-64.8);

	this.shape_1737 = new cjs.Shape();
	this.shape_1737.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1737.setTransform(170.4,-112.3);

	this.shape_1738 = new cjs.Shape();
	this.shape_1738.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1738.setTransform(156.9,-112.3);

	this.shape_1739 = new cjs.Shape();
	this.shape_1739.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1739.setTransform(39.3,-112.3);

	this.shape_1740 = new cjs.Shape();
	this.shape_1740.graphics.f("#FFFFFF").s().p("AgwBFIAAgQIBbAAIAAgLQgRAIgaAAQgaAAgRgRQgQgRAAgaQABgZARgRQATgQAYAAQAYAAARAQQAQAPAAAYQAAARgKANQgLANgRAAQgRAAgKgIQgMgKAAgPQAAgQANgJIgMAAIAAgMIArAAIABAMIgKAAQgHAAgFAEQgFAFAAAGQgBAJAFAFQAHAFAJAAQALAAAGgHQAHgIAAgKQAAgRgMgKQgMgKgSAAQgRABgLANQgJANAAARQAAAUAMAPQANAPAUAAQAVAAAOgNIAMAAIAAAsg");
	this.shape_1740.setTransform(7.4,-110.6);

	this.shape_1741 = new cjs.Shape();
	this.shape_1741.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1741.setTransform(-24.9,-112.3);

	this.shape_1742 = new cjs.Shape();
	this.shape_1742.graphics.f("#FFFFFF").s().p("AAFBCQgEgCgDgFQgHAKgSAAQgWAAgJgYQgFgPABgXQABgQAJgLQAKgNAOAAQAHAAAEAEQAFAEgBAHQAAADgDAEQgDAEABAEQABAKAOABQAEAAAGgCQAFgDABgDQgDAAgEAAQgGAAgFgDQgFgEgBgGQgBgIAGgGQAFgGAIAAQAWABAAAYQAAAMgJAHQgIAIgMABQgKABgIgEQgJgFgDgJIgBgFQAAgHADgEQAAAAABgBQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAgBAAAAQAAgBgBAAQAAAAgBAAQgHgBgGAKQgGAJAAAKQAAANAGALQAHALALAAQARAAAAgTIALAAQgBAKAFAFQAFAEAJAAQAOAAAHgOQAGgLAAgRQAAgVgLgQQgOgSgXgBQgmAAgHAZIgNAAQADgQAQgKQAPgLAYABQAgAAARAYQAOAUAAAXQAAAdgJASQgLAWgXAAQgLAAgFgDgAAAgQQAAAHAHAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgHAAAAAHg");
	this.shape_1742.setTransform(-90.2,-113.9);

	this.shape_1743 = new cjs.Shape();
	this.shape_1743.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1743.setTransform(-122.8,-112.3);

	this.shape_1744 = new cjs.Shape();
	this.shape_1744.graphics.f("#FFFFFF").s().p("AgsAwQgSgPgBgYQABgVANgNQAIgHALgEIgEgDQgBgCAAgEQAAgEACgCQAFgJALgCIAWgBQATAAAMAJQAOAJACASIg0AAQgRAAgKAKQgJAKgBAPQAAAQAMANQANAMARAAQAMgBAKgEQAMgEAGgIQAHgLAAgKIALAAIAAAFQABAKgGAJQgIAOgPAHQgPAIgTAAQgaAAgTgQgAgKgwQADABAAADQAAAEgCABIArAAQgJgJgOgCIgKgBQgGAAgFADg");
	this.shape_1744.setTransform(-201,-111.1);

	this.shape_1745 = new cjs.Shape();
	this.shape_1745.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1745.setTransform(-220.8,-112.3);

	this.shape_1746 = new cjs.Shape();
	this.shape_1746.graphics.f("#FFFFFF").s().p("AAFBCQgEgCgDgFQgHAKgSAAQgWAAgJgYQgFgPABgXQABgQAJgLQAKgNAOAAQAHAAAEAEQAFAEgBAHQAAADgDAEQgDAEABAEQABAKAOABQAEAAAGgCQAFgDABgDQgDAAgEAAQgGAAgFgDQgFgEgBgGQgBgIAGgGQAFgGAIAAQAWABAAAYQAAAMgJAHQgIAIgMABQgKABgIgEQgJgFgDgJIgBgFQAAgHADgEQAAAAABgBQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBgBQAAgBAAAAQAAgBgBAAQAAAAgBAAQgHgBgGAKQgGAJAAAKQAAANAGALQAHALALAAQARAAAAgTIALAAQgBAKAFAFQAFAEAJAAQAOAAAHgOQAGgLAAgRQAAgVgLgQQgOgSgXgBQgmAAgHAZIgNAAQADgQAQgKQAPgLAYABQAgAAARAYQAOAUAAAXQAAAdgJASQgLAWgXAAQgLAAgFgDgAAAgQQAAAHAHAAQADAAACgCQACgCAAgDQAAgDgCgCQgCgCgDAAQgHAAAAAHg");
	this.shape_1746.setTransform(-258.5,-113.9);

	this.shape_1747 = new cjs.Shape();
	this.shape_1747.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1747.setTransform(179.8,-135.8);

	this.shape_1748 = new cjs.Shape();
	this.shape_1748.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgEQgHgDAAgHIABgGQAGgMAWAAQARAAAHAKQAIgKASAAQAKAAAHADQAIAFABAIQABAGgDAGQAEAEAEAHQADAIAAAIQAAAWgQANQgQANgdAAQgdAAgRgOgAgpgLQgFAFAAAJQABANAQAGQAMAFARgBQARABAMgFQAPgGABgNQABgJgFgFQgFgGgIABIg4AAQgJgBgEAGgAANglQgFAEgBAFIAcAAIABgDQAAgDgDgDQgEgCgEgBIgCAAQgGAAgEADgAgggnQADACABADQAAAEgCACIAWAAQAAgJgNgDIgGAAIgFABg");
	this.shape_1748.setTransform(120.8,-137.2);

	this.shape_1749 = new cjs.Shape();
	this.shape_1749.graphics.f("#FFFFFF").s().p("AgwBFIAAgQIBbAAIAAgLQgRAIgaAAQgaAAgRgRQgQgRAAgaQABgZARgRQATgQAYAAQAYAAARAQQAQAPAAAYQABARgLANQgLANgRAAQgRAAgKgIQgMgKAAgPQAAgQANgJIgMAAIAAgMIArAAIABAMIgKAAQgHAAgFAEQgFAFAAAGQgBAJAFAFQAHAFAJAAQALAAAGgHQAHgIAAgKQAAgRgMgKQgMgKgSAAQgRABgLANQgJANAAARQgBAUANAPQANAPAUAAQAWAAANgNIALAAIAAAsg");
	this.shape_1749.setTransform(75,-135.6);

	this.shape_1750 = new cjs.Shape();
	this.shape_1750.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1750.setTransform(42.7,-137.3);

	this.shape_1751 = new cjs.Shape();
	this.shape_1751.graphics.f("#FFFFFF").s().p("AgCA5QgHAMgRAAQgRAAgKgPQgIgOAAgXQABgZAPgNQAMgMAYAAIAIAAIAAANIgSAAQgJABgFAEQgMAKAAAQQgBAOAFAIQAFAKAKAAQAKAAAEgGQAFgEgBgIIAAgQIAMAAIAAAQQAAASATAAQAMAAAGgPQAGgNgBgUQAAgTgLgOQgNgSgYAAQgOAAgLAFQgLAGgGAJIgNAAQAEgOAPgIQAPgJAVAAQAPgBANAHQALAEAGAIQATAUAAAjQAAAagKASQgLATgUAAQgQAAgHgMg");
	this.shape_1751.setTransform(10.6,-138.9);

	this.shape_1752 = new cjs.Shape();
	this.shape_1752.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1752.setTransform(-2.8,-137.3);

	this.shape_1753 = new cjs.Shape();
	this.shape_1753.graphics.f("#FFFFFF").s().p("AgpA3QgQgQAAgXQAAgRAHgMQAIgNAOAAQANAAACALQAEgMAOAAQALAAAHAJQAHAJAAAKQAAANgJAJQgJAJgMAAQgLABgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQANgMAAgTQAAgNgHgJQgGgMgNAAIhGAAIAAgLQASABAAgCIgDgCQgEgCAAgEQAAgNAPgFQAIgBATAAQAPAAAJACQANAEAJAKQAGAHABAMIgMAAQgCgLgNgGQgKgFgQAAQgKAAgFACQgIACAAAGQAAAAABABQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAAAIAhAAQAKAAAIADQAXALAAAlQAAAbgQASQgRASgZAAQgYgBgRgQgAgOACQAAAGAFAEQAEAEAFABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBABAAAAg");
	this.shape_1753.setTransform(-28.7,-139.2);

	this.shape_1754 = new cjs.Shape();
	this.shape_1754.graphics.f("#FFFFFF").s().p("AAdA0QgSAAgJgMQgHAMgUAAQgPAAgKgLQgKgMAAgPQABgRAKgIIgPAAIAAgMIAPAAQgDgCAAgGIABgFQAGgPAWAAQATAAAGAMQAIgMASAAQAKAAAHAEQAJAFABAJQAAAJgDAEIgDAAQAQANAAATQAAAQgKAMQgLAMgOAAIgBAAgAAJADIAAALQAAAHAGADQAFADAIAAQAJAAAGgGQAHgHgBgIQAAgHgFgFQgHgGgIAAIg1AAQgHAAgHAGQgGAGAAAHQAAAIAFAGQAGAGAJAAQAJAAAFgDQAHgDgBgGIAAgMgAAOgiQgEAEAAAHIAbAAQACgDgBgEQgBgJgMAAQgHAAgEAFgAgeglQAEACABAFQAAADgCAEIAWAAQgBgJgGgEQgEgDgGAAQgEAAgEACg");
	this.shape_1754.setTransform(-41.5,-137.3);

	this.shape_1755 = new cjs.Shape();
	this.shape_1755.graphics.f("#FFFFFF").s().p("AgCBFQgXgBgNgOQgOgOgBgVQAAgTALgOQgDgBgDgEIgFgIIgCgLQAAgKAJgKQAJgKAQAAIAKABQAKADABAHIAGgJIAVAAQgFAJgHAJQgIAIgIAFQAOgBALAEQAgAMgBAkQABAXgSAQQgPAOgXAAIgCAAgAgcgMQgIALgBAMQAAANAJAIQALALARAAQAPAAAKgHQANgLAAgQQAAgHgDgGQgDgJgJgGQgKgFgMAAQgSAAgLAMgAgkgrQgDAGAAAFQgBAHAGADIAEgEIAXgWQgBgEgGgBIgDAAQgMAAgHAKg");
	this.shape_1755.setTransform(-72.7,-138.9);

	this.shape_1756 = new cjs.Shape();
	this.shape_1756.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgLAAQgLgDAAgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADABAEQgBAOgJAEg");
	this.shape_1756.setTransform(-84.9,-143.9);

	this.shape_1757 = new cjs.Shape();
	this.shape_1757.graphics.f("#FFFFFF").s().p("AgSASQgHgGgEgKIAAgEQAAgNAIgIIAAAOIAFAAQAAAGAFAFQAGAFAGABQAIAAAEgHQAGgFgBgIIALAAIABAGQAAANgJAJQgIAIgNAAQgKAAgIgGg");
	this.shape_1757.setTransform(-193.3,-131);

	this.shape_1758 = new cjs.Shape();
	this.shape_1758.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_1758.setTransform(194.4,-186.9);

	this.shape_1759 = new cjs.Shape();
	this.shape_1759.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1759.setTransform(144.2,-180.3);

	this.shape_1760 = new cjs.Shape();
	this.shape_1760.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1760.setTransform(132.6,-178.8);

	this.shape_1761 = new cjs.Shape();
	this.shape_1761.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgFQgHgCAAgHIABgGQAFgMAXAAQASgBAGAKQAHgKAUABQAJAAAHADQAIAFABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAVgQANQgQAPgdAAQgdAAgRgPgAgpgLQgFAFABAIQAAANAQAHQALAFASAAQARAAAMgFQAQgHAAgNQABgIgFgFQgFgGgIABIg4AAQgJgBgEAGgAANgkQgFACgBAHIAdAAIAAgEQAAgEgDgCQgEgCgEgBIgCAAQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_1761.setTransform(120.6,-180.2);

	this.shape_1762 = new cjs.Shape();
	this.shape_1762.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1762.setTransform(103.1,-178.8);

	this.shape_1763 = new cjs.Shape();
	this.shape_1763.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1763.setTransform(71.8,-180.3);

	this.shape_1764 = new cjs.Shape();
	this.shape_1764.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1764.setTransform(47.5,-178.8);

	this.shape_1765 = new cjs.Shape();
	this.shape_1765.graphics.f("#FFFFFF").s().p("AguAmQgOgNAAgVQAAgIAEgIQAEgHAGgFQgHgCAAgHIABgGQAFgMAXAAQASgBAGAKQAHgKAUABQAJAAAHADQAIAFABAIQABAGgEAGQAGADACAIQAEAIAAAIQAAAVgQANQgQAPgdAAQgdAAgRgPgAgpgLQgFAFABAIQAAANAQAHQALAFASAAQARAAAMgFQAQgHAAgNQABgIgFgFQgFgGgIABIg4AAQgJgBgEAGgAANgkQgFACgBAHIAdAAIAAgEQAAgEgDgCQgEgCgEgBIgCAAQgGAAgEAEgAgggnQADACABADQAAAEgCADIAWAAQgBgKgNgDIgFAAIgFABg");
	this.shape_1765.setTransform(-11.7,-180.2);

	this.shape_1766 = new cjs.Shape();
	this.shape_1766.graphics.f("#FFFFFF").s().p("AgrAuQgRgUABgcQAAgbARgTQASgRAcAAQAYAAAQAPQARAQAAAZQAAASgLAOQgLAPgTAAQgSABgKgLQgMgMACgQQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAFQgFAGAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKANAAAVQAAATAKAOQAKAQARADIAJAAQAWAAAMgRIAPAAQgPAhgpAAQgcAAgRgUg");
	this.shape_1766.setTransform(-43.9,-178.9);

	this.shape_1767 = new cjs.Shape();
	this.shape_1767.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgEQAAgNAIgIIAAANIAFAAQAAAHAGAFQAEAFAHAAQAHABAGgHQAEgFgBgIIAMAAIABAGQAAAMgJAKQgIAIgNAAQgKAAgIgGg");
	this.shape_1767.setTransform(-70.1,-174);

	this.shape_1768 = new cjs.Shape();
	this.shape_1768.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1768.setTransform(-126.9,-178.8);

	this.shape_1769 = new cjs.Shape();
	this.shape_1769.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1769.setTransform(-178.1,-180.3);

	this.shape_1770 = new cjs.Shape();
	this.shape_1770.graphics.f("#FFFFFF").s().p("AgoA4QgRgQAAgYQAAgQAHgMQAIgOANAAQAOAAACAMQAEgNAOAAQALAAAHAJQAHAJAAAKQAAANgJAKQgJAJgMAAQgLAAgIgHQgJgGAAgKIADgQQABgHgHAAQgFAAgDAIQgDAGAAAIQAAARANAKQAMAKARAAQARAAANgMQAMgMAAgSQABgOgHgJQgGgMgOAAIguAAQgWAAAAgNQAAgOARgHQAOgGAUAAQAgAAAPARQAGAIAAALIgLAAQgCgLgNgGQgKgFgQAAQgdAAAAAKQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAmAAQALAAAHAEQAYAKAAAlQAAAbgRASQgPASgaAAQgYAAgQgQgAgOACQAAAGAEAEQAFAEAFABQAHAAAFgFQAFgEAAgHQgFAIgJAAQgKAAgGgKIgBADgAAAgOQAAABAAAAQgBABAAAAQAAABAAABQgBAAAAABQAAAAABABQAAABAAAAQAAABABAAQAAABAAAAQAAABABAAQAAAAABABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQABAAAAgBQABAAAAgBQAAAAAAgBQABgBAAAAQAAgBgBAAQAAgBAAgBQAAAAgBgBQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAAAQgBABAAAAg");
	this.shape_1770.setTransform(-190.9,-182.2);

	this.shape_1771 = new cjs.Shape();
	this.shape_1771.graphics.f("#FFFFFF").s().p("AgDA7QgHANgSgBQgPAAgKgNQgJgMAAgSQAAgdARgOQALgHAdgBIAAAMQgYgBgHAGQgGAFgDAEQgCAGAAAJQAAAMAFAHQAFAJAKgBQASAAAAgOIAAgPIAMAAIAAAPQAAAPAUAAQAMAAAGgLQAGgLAAgOQAAgVgOgMQgIgIgMABIgxAAQgRAAAAgPQAAgaA0AAQAeAAAPANQAJAJgCAMIgNAAIAAgCQAAgJgNgFQgLgEgOAAQgMAAgIADQgKACAAAFQgBAFAIAAIAnAAQARAAANANQARAQAAAeQAAAXgKAOQgLARgTAAQgSABgIgNg");
	this.shape_1771.setTransform(-235,-182.2);

	this.shape_1772 = new cjs.Shape();
	this.shape_1772.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1772.setTransform(-246.3,-178.8);

	this.shape_1773 = new cjs.Shape();
	this.shape_1773.graphics.f("#FFFFFF").s().p("AgzAkIAAgUIBbAAIAAggIAHgJQAEgFABgEIAABGg");
	this.shape_1773.setTransform(-258.2,-175.2);

	this.shape_1774 = new cjs.Shape();
	this.shape_1774.graphics.f("#FFFFFF").s().p("AACBAQgHgDgFgIQgCAGgFAEQgGAEgJAAQgOAAgJgQQgHgNAAgTQAAgVAMgQQANgSAWAAQAQAAAKAKQAKAKAAAPQAAAMgKAIIgFAEQgDADAAAEQAAAMAQAAQANAAAHgNQAHgMAAgRQAAgVgNgQQgNgRgZAAQgMAAgLAGQgLAFgGAKIgNAAQAFgQAQgJQAOgIASAAQAeAAASASQAIAIAGAOQAGAPAAAMQABAfgNASQgMARgTAAQgLAAgHgDgAgkAoQgCACAAADQAAADACADQACACAEAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAQgEAAgCADgAgtADQgDAIAAAKQAAAHACAHQAFgKAMgBQAJgBAIAHQABgHAGgFQAHgIgBgIQAAgGgEgFQgGgEgHAAQgVAAgIAQg");
	this.shape_1774.setTransform(-258.5,-181.7);

	this.shape_1775 = new cjs.Shape();
	this.shape_1775.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_1775.setTransform(178.7,-203.9);

	this.shape_1776 = new cjs.Shape();
	this.shape_1776.graphics.f("#FFFFFF").s().p("AgXAxIAAgMQADACAHAAQAGAAAFgMQADgMAAgOQAAgNgDgMQgFgNgFgBQgHAAgEADIAAgMQAFgEALAAQANAAAKASQAIAQAAASQAAATgIAPQgKARgOAAQgIAAgHgDg");
	this.shape_1776.setTransform(149.2,-205.3);

	this.shape_1777 = new cjs.Shape();
	this.shape_1777.graphics.f("#FFFFFF").s().p("AgSASQgHgGgDgKIgBgFQAAgMAIgIIAAANIAFAAQAAAIAGAEQAEAGAHgBQAHAAAFgFQAGgGgBgIIALAAIABAGQAAAMgJAJQgIAJgNAAQgKAAgIgGg");
	this.shape_1777.setTransform(130.9,-199);

	this.shape_1778 = new cjs.Shape();
	this.shape_1778.graphics.f("#FFFFFF").s().p("AADBAQgJgFgBgKQAAgIAGgEQgWABgPgMQgPgMABgSQACgWASgKQgDgBgCgDQgDgEAAgEIABgEQAEgJAKgDQAHgCAOAAQAXAAALAMQAIAKAAAOIglAAQgLAAgHAHQgIAHgBALQgBALALAJQAKAJAMABQAWABAOgIIAAAMQgHADgHACQgNADAAAGQAAAEADACQAEACAFAAQAGAAAEgDQAEgDAAgGIAAgEIAMAAIAAAFQAAALgJAIQgIAGgMABIgFAAQgJAAgHgDgAgPg1IACACIACADQABAFgEABIAGAAIAeAAQgEgIgKgDQgEgBgIAAQgHAAgEABg");
	this.shape_1778.setTransform(60.4,-203.8);

	this.shape_1779 = new cjs.Shape();
	this.shape_1779.graphics.f("#FFFFFF").s().p("AgrAtQgRgTABgbQAAgcARgSQASgSAcAAQAYAAAQAPQARARAAAYQAAASgLAPQgLAOgTAAQgSABgKgMQgMgMACgPQABgQAMgJIgOAAIAAgMIAvAAIAAAMIgJAAQgJAAgGAGQgFAFAAAIQAAAIAFAGQAGAGAKgBQAMAAAHgJQAGgIAAgMQgBgPgHgJQgLgNgXAAQgTAAgLARQgKAOAAAUQAAATAKAOQAKAQARADIAJABQAWgBAMgRIAPAAQgPAhgpAAQgcAAgRgVg");
	this.shape_1779.setTransform(48.6,-203.9);

	this.shape_1780 = new cjs.Shape();
	this.shape_1780.graphics.f("#FFFFFF").s().p("AguAlQgOgMAAgVQAAgIAEgHQAEgIAFgFQgFgCAAgIIABgFQAFgNAWAAQASAAAGAKQAIgKASAAQAKAAAHAFQAIAEABAIQABAGgEAGQAFADAEAIQADAIAAAIQAAAVgPANQgRAOgdABQgdgBgRgPgAgpgLQgFAGAAAHQABANAPAHQANAEARABQARgBAMgEQAPgHACgNQAAgHgFgGQgFgFgIgBIg5AAQgHABgFAFgAANgkQgFACgBAHIAcAAIABgEQAAgEgEgCQgDgDgFAAIgBAAQgGAAgEAEgAgggnQADACAAADQABAEgCADIAWAAQAAgKgOgDIgFAAIgFABg");
	this.shape_1780.setTransform(14.9,-205.2);

	this.shape_1781 = new cjs.Shape();
	this.shape_1781.graphics.f("#FFFFFF").s().p("AgrAwQgUgPABgYQAAgVANgNQAHgHALgEIgCgDQgCgDAAgDQAAgDACgDQAFgJAMgCIAVgBQATAAANAJQAMAKAEARIg1AAQgRAAgKALQgJAJAAAQQgBAPANANQANAMAPAAQANAAALgEQAMgFAFgIQAIgLgCgKIAMAAIABAFQgBAJgFAKQgIAOgPAHQgPAIgSAAQgbAAgSgQgAgJgxQACACAAADQAAADgCACIAqAAQgHgJgPgCIgKgBQgGAAgEACg");
	this.shape_1781.setTransform(2,-204.1);

	this.shape_1782 = new cjs.Shape();
	this.shape_1782.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgMAAQgJgDgBgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQACADAAAEQgBAOgJAEg");
	this.shape_1782.setTransform(-17.5,-211.9);

	this.shape_1783 = new cjs.Shape();
	this.shape_1783.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1783.setTransform(-38.2,-205.3);

	this.shape_1784 = new cjs.Shape();
	this.shape_1784.graphics.f("#FFFFFF").s().p("AgCBGQgXgBgNgOQgOgNAAgWQAAgOAFgKQAGgKALgLIAYgVIAJgPIAVAAQgDAHgHAHQgGAHgJAHQAPgBAKAEQAgAMAAAkQAAAXgRAPQgQAPgXAAIgCgBgAgbgKQgJAKAAANQAAAMAIAJQAMALARAAQAPAAAJgIQAOgKAAgQQAAgHgDgGQgEgKgJgFQgJgFgMAAQgSAAgLAMgAgtglIgKAAIAAgKIAKAAIAAgNIgKAAIAAgKIAbAAQAJAAAFAEQAGAFABAJQAAAIgIAEQgHAEgLAAIAAAOQgIAHgEAIgAghg8IAAANQAKAAAAgGQAAgHgIAAIgCAAg");
	this.shape_1784.setTransform(-50.7,-207.1);

	this.shape_1785 = new cjs.Shape();
	this.shape_1785.graphics.f("#FFFFFF").s().p("AATBIQgTgBgIgLQgGAMgOAAQgRAAgIgRQgIgNAAgVQAAgUAOgNQANgPATAAQAOAAAKAIQALAJAAAMQABAHgDAHQgCAHgEACQgIADgBAHQAAAMASgBQAMAAAHgNQAGgLAAgPQAAgSgLgLQgIgJgSAAIhEAAIAAgMIAQAAQgGgBAAgIIAAgEQAGgJANgDQAMgDAPAAQAmAAAMASQAFAGAAAIIgBAEIgKAAQAMAJAEAMQAFALAAARQAAAXgMARQgLASgTAAIgBAAgAgkAtQgCACgBADQABAFAEADQACABADAAQAFAAACgEQACgCABgDQgBgFgEgDQgCgBgDAAQgFAAgCAEgAgKAlQAAgJAEgFIAGgEQACgCABgDIABgEQAAgHgHgEQgGgEgIAAQgUAAgHASQgCAGAAAHQAAAGACAFQADgIAOgBQALAAAGAJIAAAAgAgPg5QgNACAAAHQAAAFAJAAIAmAAQAHAAAMAGIAAgDQAAgHgMgFQgLgFgNAAIgRAAg");
	this.shape_1785.setTransform(-62.7,-207.2);

	this.shape_1786 = new cjs.Shape();
	this.shape_1786.graphics.f("#FFFFFF").s().p("AgWA0QgVAAgLgQQgLgOAAgUQAAgiAYgPQAIgEANAAIAHAAIAAAMQANgNAUABQAWAAAMAPQALANAAAWQABAWgNAPQgNAQgVAAIgHAAIAAgKQgIAGgGACQgGACgKAAIgEAAgAAIgRQARAFAAAWQAAAQgLANQALACAIgFQANgJgBgWQAAgNgIgKQgJgKgMAAQgXgCgEAQQAGgEAHAAIAGABgAgqgdQgHAKAAANQAAANAIAJQAIAKAOABQALAAAIgFQAIgFAAgJQABgFgDgDQgEgFgEABQgEAAgDADQgCACAAAEIABAFIgNAAQgDgNACgLQABgMAJgMIgHgBQgNAAgIAKg");
	this.shape_1786.setTransform(-75.9,-205.3);

	this.shape_1787 = new cjs.Shape();
	this.shape_1787.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1787.setTransform(-89.6,-205.3);

	this.shape_1788 = new cjs.Shape();
	this.shape_1788.graphics.f("#FFFFFF").s().p("AgNAZIAAgVIgJAAIAAgJIAJAAIAAgNIgKAAIAAgKIAbAAQAIAAAGAEQAGAFAAAIQAAAIgHADQgIAEgKAAIAAAZgAgBgSIAAANQAJAAAAgGQAAgHgIAAIgBAAg");
	this.shape_1788.setTransform(-140.3,-212.4);

	this.shape_1789 = new cjs.Shape();
	this.shape_1789.graphics.f("#FFFFFF").s().p("AAiAYQAIgGAAgIIAAgDQgCgGgNgCQgMgDgPAAQgPAAgKADQgOADgCAFIAAAEQgBAIAJAFIgLAAQgLgDAAgPQAAgFACgCQAGgNAQgFQALgEAUAAQAsAAAKAWQABADABAEQgBAOgJAEg");
	this.shape_1789.setTransform(-167.7,-211.9);

	this.shape_1790 = new cjs.Shape();
	this.shape_1790.graphics.f("#FFFFFF").s().p("AgXA0QgRAAgMgMQgMgLAAgRQgBgPAFgLIAMAAQgBAEACACQABABAEAAQAGAAAFgDQAJgGADgHIAAgEQAAgJgFgCQgFgCgFACQAIACAAAJQAAAGgEADQgEADgFAAIgFgBQgJgCAAgMIABgHQAFgOATAAQAIAAAFADQAGAEACAGQAKgOAWABQAWABALAVQAIAOAAAQQAAAjgaAMQgIAEgJAAIgMgBIAAgJQgIAGgGACQgHACgJAAIgEAAgAAMgNQAMAKAAAPQAAARgKAKIAIABQAJAAAFgEQANgKAAgUQgBgMgIgKQgJgLgNAAQgQgBgHAKIACAAQAJAAAGAFgAgzAIQAAAIAEAFQAHAIAPAAIAIAAQAKgBAHgHQAFgHAAgIQAAgFgDgDQgCgFgJgBQgEAAgGAEIgLAGQgGACgGAAQgFAAgDgCIgBAGg");
	this.shape_1790.setTransform(-168.1,-205.3);

	this.shape_1791 = new cjs.Shape();
	this.shape_1791.graphics.f("#FFFFFF").s().p("AgTA+QABgWABgKQADgRAJgMIAhgmQgMAEgGAAIgLgCIgRgCQgLAAgEAHQgEAHAAATIgOAAIAAg4IAOAAIAAAOQANgPAOAAQAKAAANAFIAJADIAFACQAIAAAFgJIAMAAQgCANgDAGIgSAbQgLAQgEAMQgGAQAAAgg");
	this.shape_1791.setTransform(-235.9,-206.2);

	this.shape_1792 = new cjs.Shape();
	this.shape_1792.graphics.f("#FFFFFF").s().p("AgiA6QgTgJAAgSQAAgLAIgHQAFgFANgFQgLgHgEgFQgGgHAAgLQAAgSARgKQAOgIASAAQATAAAPAIQAOAHAAAQQAAAJgHAHQgEAEgLAGQANAFAGAGQAIAIAAANQAAAVgSAKQgPAHgVAAQgUAAgOgGgAgeAdQAAAKAKAGQAJAEALAAQAMAAAGgDQALgDAAgLQAAgFgFgFQgFgDgGgCIgagJQgRAHAAAOgAgPguQgIAFAAAIQgBALAQAEIAWAGQANgIAAgMQABgHgHgFQgGgFgPAAIgBAAQgIAAgGADg");
	this.shape_1792.setTransform(-249.1,-206.2);

	this.shape_1793 = new cjs.Shape();
	this.shape_1793.graphics.f("#FFFFFF").s().p("AgkA+IAAgPIAKAAQAIABADgDQADgCAAgHIAAg/IgYAAIAAgPQANAAAHgDQAHgEAHgMIAPAAIAABhQAAAHADADQADABAHAAIALAAIAAAPg");
	this.shape_1793.setTransform(-260.2,-206.2);

	this.shape_1794 = new cjs.Shape();
	this.shape_1794.graphics.f("#FFFFFF").s().p("AgSA2IAAhYIgaAAIAAgbIAaAAIAAgmIAjAAIAAAmIAcAAIAAAbIgcAAIAABXQAAAJADAFQAEADAJAAQAGAAAGgBIAAAcQgMADgLAAQgoAAAAgug");
	this.shape_1794.setTransform(312.4,-258.1);

	this.shape_1795 = new cjs.Shape();
	this.shape_1795.graphics.f("#FFFFFF").s().p("AggBLQgQgHgIgMQgIgMgBgPIAjAAQABANAJAHQAJAHANAAQAOAAAIgGQAGgFABgJQAAgJgIgFQgIgFgRgEQgTgEgLgGQgbgMAAgYQAAgVARgNQARgOAaAAQAdAAARAOQARAOAAAWIgjAAQAAgKgIgHQgHgHgNAAQgLAAgHAGQgIAFAAAJQABAJAGAEQAHAFAUAEQAVAFAMAGQAMAGAGAJQAGAJAAAMQgBAWgRANQgSANgdAAQgSAAgPgHg");
	this.shape_1795.setTransform(300,-256.4);

	this.shape_1796 = new cjs.Shape();
	this.shape_1796.graphics.f("#FFFFFF").s().p("AAeBRIAAhmQAAgPgGgHQgHgIgPAAQgUAAgLAUIAABwIgkAAIAAieIAiAAIABATQASgWAbAAQAyAAABA5IAABog");
	this.shape_1796.setTransform(283.9,-256.5);

	this.shape_1797 = new cjs.Shape();
	this.shape_1797.graphics.f("#FFFFFF").s().p("AgRBtIAAidIAiAAIAACdgAgOhMQgFgFAAgIQAAgJAFgFQAFgFAJAAQAJAAAGAFQAFAFAAAJQAAAIgFAFQgGAFgJAAQgJAAgFgFg");
	this.shape_1797.setTransform(271.8,-259.4);

	this.shape_1798 = new cjs.Shape();
	this.shape_1798.graphics.f("#FFFFFF").s().p("AhFBrIAAjVICKAAIAAAeIhlAAIAAA7IBXAAIAAAdIhXAAIAABBIBmAAIAAAeg");
	this.shape_1798.setTransform(260.1,-259.1);

	this.shape_1799 = new cjs.Shape();
	this.shape_1799.graphics.f("#FFFFFF").s().p("AgSA2IAAhYIgaAAIAAgbIAaAAIAAgmIAjAAIAAAmIAcAAIAAAbIgcAAIAABXQAAAJADAFQAEADAJAAQAGAAAGgBIAAAcQgMADgLAAQgoAAAAgug");
	this.shape_1799.setTransform(238.2,-258.1);

	this.shape_1800 = new cjs.Shape();
	this.shape_1800.graphics.f("#FFFFFF").s().p("AgjBeIgBARIghAAIAAjgIAkAAIAABRQAPgRAZgBQAdAAARAWQARAVAAAmIAAACQAAAmgRAVQgQAWgdAAQgbAAgQgUgAghAAIAABBQAKAUAXAAQAQAAAJgMQAJgNAAgZIAAgFQAAgZgJgMQgJgNgQgBQgYAAgJAVg");
	this.shape_1800.setTransform(198.8,-259.5);

	this.shape_1801 = new cjs.Shape();
	this.shape_1801.graphics.f("#FFFFFF").s().p("AgRBwIAAjgIAjAAIAADgg");
	this.shape_1801.setTransform(186.2,-259.7);

	this.shape_1802 = new cjs.Shape();
	this.shape_1802.graphics.f("#FFFFFF").s().p("AA7BrIgSgyIhRAAIgSAyIgmAAIBQjVIAhAAIBQDVgAgeAbIA9AAIgfhXg");
	this.shape_1802.setTransform(172.4,-259.1);

	this.shape_1803 = new cjs.Shape();
	this.shape_1803.graphics.f("#FFFFFF").s().p("AAzAkQAMgIAAgMIAAgFQgDgJgTgEQgSgEgXAAQgWAAgRAEQgUAEgDAJIgBAFQAAANAMAHIgQAAQgPgFAAgVQAAgIACgEQAJgTAYgIQASgGAdAAQBDAAAOAhQADAFAAAHQAAAUgPAGg");
	this.shape_1803.setTransform(105.1,-266.1);

	this.shape_1804 = new cjs.Shape();
	this.shape_1804.graphics.f("#FFFFFF").s().p("AhBBNQgYgZABgnQAAgbAUgSQATgUAcABIAVAAIAAASIgSAAQgVAAgMAMQgNAMgBASQgBAZAVAPQATANAaAAQAdAAATgVQASgTAAgcQAAgegTgWQgTgWgcgCQgVAAgRAJQgRAJgHAQIgSAAQAIgZAVgNQAVgOAeAAQAoABAZAdQAZAdAAApQAAAsgaAbQgZAdgpABQgnAAgYgYg");
	this.shape_1804.setTransform(86.2,-258.4);

	this.shape_1805 = new cjs.Shape();
	this.shape_1805.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHgBANQABALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAABgJQgBgKgLAAIgDAAg");
	this.shape_1805.setTransform(73.4,-266.9);

	this.shape_1806 = new cjs.Shape();
	this.shape_1806.graphics.f("#FFFFFF").s().p("AADA8QgLATgeAAQgXAAgPgSQgPgRAAgXQABgaAQgMIgXAAIAAgSIAXAAQgEgDAAgJIACgIQAHgXAiAAQAcAAAKATQALgTAbAAQAQAAALAHQAMAIABAOQACAMgFAHIgEAAQAYATgBAdQAAAZgPARQgQATgXAAQgbgBgNgSgAANAFIAAARQAAAJAKAFQAIAFALAAQANAAAJgKQAKgJAAgNQAAgLgJgIQgJgIgMAAIhQAAQgNAAgIAJQgKAIAAALQAAANAJAJQAIAJAOAAQAMAAAIgEQALgFAAgKIAAgRgAAlg7QgKAAgGAHQgHAHAAAKIApAAQADgEAAgGQgCgOgQAAIgDAAgAgtg5QAFAEABAHQAAAGgDAFIAiAAQgBgNgKgHQgHgEgIAAQgGAAgFACg");
	this.shape_1806.setTransform(66.9,-256.2);

	this.shape_1807 = new cjs.Shape();
	this.shape_1807.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHAAANQAAALgKAGQgLAGgPgBIAAAlgAgBgbIAAATQAOAAAAgJQAAgKgMAAIgCAAg");
	this.shape_1807.setTransform(53.7,-266.9);

	this.shape_1808 = new cjs.Shape();
	this.shape_1808.graphics.f("#FFFFFF").s().p("AgiBOQgbgBgSgRQgRgRgBgZQgBgXAIgQIASAAQgCAGACACQADACAFAAQAJAAAHgEQAPgJADgLIABgHQAAgMgIgEQgIgDgHADQAMAEAAAOQAAAIgGAFQgFAEgIAAIgIgBQgOgEAAgRQAAgGACgGQAIgUAdAAQALAAAIAFQAJAFADAJQAQgVAgACQAhABARAgQAMAVAAAZQgBA0gmASQgMAGgOAAQgIAAgKgCIAAgNQgMAKgKADQgJACgPAAIgEAAgAASgUQASAPAAAXQAAAZgPAQIAMABQANAAAIgGQATgOAAgfQAAgTgOgPQgNgPgTgBQgYgBgLAOIABAAQAPAAAKAIgAhNALQAAAMAHAJQAJAMAXAAIANgBQAOgBAKgLQAIgKAAgMQAAgHgEgFQgEgIgNgBQgGgBgKAHQgMAIgEABQgIADgJAAQgJAAgEgDIgBAIg");
	this.shape_1808.setTransform(46.9,-256.2);

	this.shape_1809 = new cjs.Shape();
	this.shape_1809.graphics.f("#FFFFFF").s().p("AAzAkQAMgIAAgMIAAgFQgDgJgTgEQgSgEgXAAQgWAAgRAEQgUAEgDAJIgBAFQAAANAMAHIgQAAQgPgFgBgVQABgIACgEQAKgTAXgIQASgGAdAAQBDAAAOAhQADAFAAAHQAAAUgPAGg");
	this.shape_1809.setTransform(26.9,-266.1);

	this.shape_1810 = new cjs.Shape();
	this.shape_1810.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_1810.setTransform(27,-256.3);

	this.shape_1811 = new cjs.Shape();
	this.shape_1811.graphics.f("#FFFFFF").s().p("AAaBfIAAgjQgXADgRgBQgjgCgVgPQgYgSAAgfQAAgPAJgNQAIgMAMgGQgGgCgDgEQgDgFAAgEQAAgKAJgHQAPgMAcAAQAqABALAYQADgLAJgHQAJgHALAAIATAAIAAASIgEgBQgFAAgBAGQgBACAHAFQAJAHADAEQAHAIABAKQAAATgQAKQgPAHgSgBIAAAnIAQgEIAMgEIAAARIgMAFIgQAEIAAAmgAgrgcQgKAMAAAQQABAhAmAJQAHABAMABQANAAAIgCIAAhSIgrAAQgQAAgKAMgAAugvIAAAeIAEABQAGAAAGgFQAFgEAAgIQAAgIgHgLQgIgLAAgFIAAgDQgGACAAAWgAALg5QgDgLgNgFQgRgGgPAFQAEAEAAAFQAAAFgEADIAEAAIAEAAIAoAAIAAAAg");
	this.shape_1811.setTransform(7.6,-254.6);

	this.shape_1812 = new cjs.Shape();
	this.shape_1812.graphics.f("#FFFFFF").s().p("AADBgQgLgEgIgMQgCAJgJAFQgJAGgMAAQgWAAgNgXQgLgUAAgcQAAggASgZQAUgaAhAAQAXAAAQAPQAPAPAAAXQAAARgPAMIgHAIQgEAEgBAFQAAASAYAAQATAAAMgUQAKgRAAgaQAAgggTgYQgUgYgmAAQgTAAgQAHQgRAJgIAOIgUAAQAIgXAXgOQAVgMAcAAQAtAAAbAaQAMANAJAVQAJAWABASQABAvgUAbQgTAZgcAAQgRAAgJgEgAg3A8QgDADAAAFQAAAFADADQAEAEAFAAQAFAAADgEQADgDAAgFQAAgFgDgDQgEgDgEAAQgFAAgEADgAhEAFQgFAMAAAPQAAALADAKQAIgPASgCQANgBAMAKQACgKAIgIQALgLAAgMQgBgKgHgHQgIgHgLAAQgfAAgMAZg");
	this.shape_1812.setTransform(-56.8,-258.4);

	this.shape_1813 = new cjs.Shape();
	this.shape_1813.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHgBANQABALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAABgJQgBgKgLAAIgDAAg");
	this.shape_1813.setTransform(-70.2,-266.9);

	this.shape_1814 = new cjs.Shape();
	this.shape_1814.graphics.f("#FFFFFF").s().p("AhBBFQgZgeAAgpQABgqAagbQAagbArAAQAkAAAZAYQAYAXAAAlQAAAdgQAVQgRAWgcABQgbAAgQgRQgRgSACgZQACgXASgNIgVAAIAAgSIBHAAIAAASIgOAAQgNAAgJAIQgIAJAAALQgBANAJAIQAJAJAPgBQASAAAKgOQAKgMgBgSQgBgXgLgNQgQgUgjAAQgcABgRAYQgPAVAAAgQAAAcAPAVQAPAYAaAEIAOACQAgAAASgaIAXAAQgXAwg+AAQgpAAgageg");
	this.shape_1814.setTransform(-76.2,-254.2);

	this.shape_1815 = new cjs.Shape();
	this.shape_1815.graphics.f("#FFFFFF").s().p("AgiBBQgOgMAAgWQAAgRAGgMQAFgIAKgMQAMgOAfgZIg7AAIAAgSIBZAAIAAASIgVAPQgKAIgIAIQgNAPgEAGQgHAMABANQAAAJAHAFQAFAFAIAAQAQAAAFgOIACgIQAAgGgEgGIASAAQAJANgBAQQgCASgOAMQgOAMgTAAQgVAAgNgLg");
	this.shape_1815.setTransform(-90.8,-256);

	this.shape_1816 = new cjs.Shape();
	this.shape_1816.graphics.f("#FFFFFF").s().p("AAaBfIAAgjQgXADgRgBQgjgCgVgPQgYgSAAgfQAAgPAJgNQAIgMAMgGQgGgCgDgEQgDgFAAgEQAAgKAJgHQAPgMAcAAQAqABALAYQADgLAJgHQAJgHALAAIATAAIAAASIgEgBQgFAAgBAGQgBACAHAFQAJAHADAEQAHAIABAKQAAATgQAKQgPAHgSgBIAAAnIAQgEIAMgEIAAARIgMAFIgQAEIAAAmgAgrgcQgKAMAAAQQABAhAmAJQAHABAMABQANAAAIgCIAAhSIgrAAQgQAAgKAMgAAugvIAAAeIAEABQAGAAAGgFQAFgEAAgIQAAgIgHgLQgIgLAAgFIAAgDQgGACAAAWgAALg5QgDgLgNgFQgRgGgPAFQAEAEAAAFQAAAFgEADIAEAAIAEAAIAoAAIAAAAg");
	this.shape_1816.setTransform(-105.8,-254.6);

	this.shape_1817 = new cjs.Shape();
	this.shape_1817.graphics.f("#5F9D3F").s().p("EhHAADmIAAnLMCOBAAAIAAHLg");
	this.shape_1817.setTransform(121.5,-256.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_592},{t:this.shape_591},{t:this.shape_590},{t:this.shape_589,p:{x:-52.9,y:-252.8}},{t:this.shape_588},{t:this.shape_587},{t:this.shape_586},{t:this.shape_585},{t:this.shape_584},{t:this.shape_583},{t:this.shape_582},{t:this.shape_581},{t:this.shape_580,p:{x:64.7}},{t:this.shape_579,p:{x:78.8,y:-252.8}},{t:this.shape_578,p:{x:85.6,y:-263.4}},{t:this.shape_577},{t:this.shape_576},{t:this.shape_575,p:{x:130.1,y:-254.1}},{t:this.shape_574},{t:this.shape_573},{t:this.shape_572},{t:this.shape_571},{t:this.shape_570},{t:this.shape_569,p:{x:221.6,y:-255.9}},{t:this.shape_568,p:{x:231.3,y:-253.1}},{t:this.shape_567},{t:this.shape_566},{t:this.shape_565},{t:this.shape_564,p:{x:307.2,y:-253.1}},{t:this.shape_563},{t:this.shape_562,p:{x:340.6}},{t:this.shape_561},{t:this.shape_560,p:{x:-248.9,y:-187.7}},{t:this.shape_559},{t:this.shape_558},{t:this.shape_557,p:{x:-204.8,y:-188.4}},{t:this.shape_556},{t:this.shape_555,p:{x:-183.5,y:-186.8}},{t:this.shape_554,p:{x:-174.1,y:-186.8}},{t:this.shape_553},{t:this.shape_552},{t:this.shape_551,p:{x:-146.5,y:-193.9}},{t:this.shape_550},{t:this.shape_549},{t:this.shape_548},{t:this.shape_547},{t:this.shape_546,p:{x:-92.5,y:-186.8}},{t:this.shape_545,p:{x:-79.5,y:-188.3}},{t:this.shape_544,p:{x:-66,y:-186.8}},{t:this.shape_543},{t:this.shape_542},{t:this.shape_541,p:{x:-19.3,y:-186.8}},{t:this.shape_540},{t:this.shape_539,p:{x:12.5,y:-185.3}},{t:this.shape_538},{t:this.shape_537},{t:this.shape_536,p:{x:49.3,y:-186.8}},{t:this.shape_535,p:{x:58.3,y:-186.8}},{t:this.shape_534,p:{x:58.3,y:-193.4}},{t:this.shape_533},{t:this.shape_532},{t:this.shape_531},{t:this.shape_530,p:{x:101.2,y:-186.8}},{t:this.shape_529,p:{x:101.5,y:-193.9}},{t:this.shape_528},{t:this.shape_527},{t:this.shape_526},{t:this.shape_525,p:{x:146.2,y:-186.8}},{t:this.shape_524,p:{x:150.7,y:-193.9}},{t:this.shape_523},{t:this.shape_522},{t:this.shape_521},{t:this.shape_520},{t:this.shape_519,p:{x:-239.6,y:-161.8}},{t:this.shape_518,p:{x:-239.3,y:-168.9}},{t:this.shape_517},{t:this.shape_516,p:{x:-230.5,y:-155.9}},{t:this.shape_515,p:{x:-216.9,y:-161.8}},{t:this.shape_514,p:{x:-203.6,y:-163.2}},{t:this.shape_513,p:{x:-184.6,y:-161.8}},{t:this.shape_512,p:{x:-170.8,y:-161.8}},{t:this.shape_511},{t:this.shape_510,p:{x:-157.7,y:-163.2}},{t:this.shape_509},{t:this.shape_508},{t:this.shape_507},{t:this.shape_506,p:{x:-119.8,y:-160.3}},{t:this.shape_505},{t:this.shape_504,p:{x:-102.2,y:-161.8}},{t:this.shape_503},{t:this.shape_502,p:{x:-70,y:-161.8}},{t:this.shape_501},{t:this.shape_500,p:{x:-49.8,y:-161.8}},{t:this.shape_499,p:{x:-49.5,y:-168.9}},{t:this.shape_498,p:{x:-40.9,y:-163.2}},{t:this.shape_497},{t:this.shape_496,p:{x:-11.4,y:-163.2}},{t:this.shape_495,p:{x:0.3,y:-163.4}},{t:this.shape_494,p:{x:19.5,y:-161.8}},{t:this.shape_493,p:{x:19.9,y:-168.4}},{t:this.shape_492},{t:this.shape_491,p:{x:37,y:-168.9}},{t:this.shape_490},{t:this.shape_489,p:{x:54.5,y:-161.8}},{t:this.shape_488,p:{x:62.6,y:-160.3}},{t:this.shape_487,p:{x:74.1,y:-161.8}},{t:this.shape_486,p:{x:93.9,y:-160.7}},{t:this.shape_485,p:{x:102.7,y:-161.8}},{t:this.shape_484,p:{x:111.8,y:-161.8}},{t:this.shape_483,p:{x:125.5,y:-161.8}},{t:this.shape_482,p:{x:139.6,y:-161.8}},{t:this.shape_481,p:{x:152.9,y:-161.8}},{t:this.shape_480,p:{x:166.4,y:-161.8}},{t:this.shape_479,p:{x:171.2,y:-168.9}},{t:this.shape_478},{t:this.shape_477,p:{x:-245.5,y:-136.8}},{t:this.shape_476,p:{x:-227.1,y:-136.8}},{t:this.shape_475},{t:this.shape_474,p:{x:-205.8,y:-136.8}},{t:this.shape_473,p:{x:-196.4,y:-136.8}},{t:this.shape_472,p:{x:-191.9,y:-143.9}},{t:this.shape_471},{t:this.shape_470,p:{x:-174.4,y:-136.8}},{t:this.shape_469},{t:this.shape_468},{t:this.shape_467,p:{x:-137.3,y:-136.8}},{t:this.shape_466,p:{x:-127.9,y:-136.8}},{t:this.shape_465,p:{x:-116,y:-136.8}},{t:this.shape_464},{t:this.shape_463,p:{x:-85.6,y:-136.8}},{t:this.shape_462,p:{x:-85.3,y:-143.4}},{t:this.shape_461,p:{x:-73.1,y:-138.6}},{t:this.shape_460},{t:this.shape_459,p:{x:-52.4,y:-136.8}},{t:this.shape_458,p:{x:-42.5,y:-136.8}},{t:this.shape_457},{t:this.shape_456,p:{x:-23.3,y:-138.2}},{t:this.shape_455,p:{x:-11.4,y:-138.2}},{t:this.shape_454,p:{x:0.7,y:-136.8}},{t:this.shape_453,p:{x:9.4,y:-136.8}},{t:this.shape_452,p:{x:18.5,y:-136.8}},{t:this.shape_451,p:{x:18.5,y:-143.4}},{t:this.shape_450,p:{x:28.2,y:-132.9}},{t:this.shape_449,p:{y:-188.8,x:-285.9}},{t:this.shape_448,p:{x:-259.6,y:-87.8}},{t:this.shape_447},{t:this.shape_446,p:{x:-239.1,y:-87.8}},{t:this.shape_445,p:{x:-230.1,y:-87.8}},{t:this.shape_444,p:{x:-230.1,y:-94.4}},{t:this.shape_443},{t:this.shape_442,p:{x:-205.9,y:-87.8}},{t:this.shape_441,p:{x:-195.1,y:-89.2}},{t:this.shape_440,p:{x:-187.2,y:-87.8}},{t:this.shape_439,p:{x:-186.9,y:-94.9}},{t:this.shape_438,p:{x:-172.9,y:-87.8}},{t:this.shape_437},{t:this.shape_436,p:{x:-151.6,y:-87.8}},{t:this.shape_435,p:{x:-142.2,y:-87.8}},{t:this.shape_434,p:{x:-137.7,y:-94.9}},{t:this.shape_433},{t:this.shape_432},{t:this.shape_431,p:{x:-104.1,y:-87.8}},{t:this.shape_430},{t:this.shape_429},{t:this.shape_428,p:{x:-81.9,y:-87.8}},{t:this.shape_427,p:{x:-65.8,y:-87.8}},{t:this.shape_426},{t:this.shape_425},{t:this.shape_424,p:{x:-33,y:-94.4}},{t:this.shape_423,p:{x:-20.3,y:-89.2}},{t:this.shape_422,p:{x:-7.8,y:-89.3}},{t:this.shape_421,p:{x:11.5,y:-87.8}},{t:this.shape_420,p:{x:24.7,y:-87.8}},{t:this.shape_419,p:{x:29.2,y:-94.9}},{t:this.shape_418,p:{x:38.9,y:-87.8}},{t:this.shape_417,p:{x:39.8,y:-81.9}},{t:this.shape_416},{t:this.shape_415},{t:this.shape_414,p:{x:73.2,y:-87.8}},{t:this.shape_413,p:{x:74.1,y:-81.9}},{t:this.shape_412,p:{x:92.2,y:-87.8}},{t:this.shape_411,p:{x:104.4,y:-87.8}},{t:this.shape_410,p:{x:116.6,y:-87.8}},{t:this.shape_409,p:{x:127.4,y:-89.2}},{t:this.shape_408,p:{x:140,y:-87.8}},{t:this.shape_407,p:{x:140.5,y:-94.4}},{t:this.shape_406,p:{x:159,y:-86.3}},{t:this.shape_405,p:{x:167.2,y:-87.6}},{t:this.shape_404,p:{x:176.8,y:-89.7}},{t:this.shape_403},{t:this.shape_402,p:{x:-244.9,y:-62.8}},{t:this.shape_401,p:{x:-231.3,y:-62.8}},{t:this.shape_400,p:{x:-226.8,y:-69.9}},{t:this.shape_399},{t:this.shape_398,p:{x:-208,y:-64.2}},{t:this.shape_397,p:{x:-194.6,y:-62.8}},{t:this.shape_396,p:{x:-189.8,y:-69.9}},{t:this.shape_395,p:{x:-175.5,y:-61.3}},{t:this.shape_394,p:{x:-167.3,y:-62.6}},{t:this.shape_393},{t:this.shape_392,p:{x:-151.9,y:-69.9}},{t:this.shape_391,p:{x:-143.3,y:-64.2}},{t:this.shape_390},{t:this.shape_389,p:{x:-124,y:-61.7}},{t:this.shape_388,p:{x:-112.3,y:-62.8}},{t:this.shape_387,p:{x:-100.7,y:-62.8}},{t:this.shape_386,p:{x:-87.2,y:-62.8}},{t:this.shape_385,p:{x:-82.4,y:-69.9}},{t:this.shape_384,p:{x:-67.3,y:-64.2}},{t:this.shape_383},{t:this.shape_382,p:{x:-54.7,y:-64.2}},{t:this.shape_381,p:{x:-41.2,y:-62.8}},{t:this.shape_380,p:{x:-36.2,y:-69.9}},{t:this.shape_379},{t:this.shape_378,p:{x:-12.2,y:-62.8}},{t:this.shape_377},{t:this.shape_376},{t:this.shape_375,p:{x:13.8,y:-69.9}},{t:this.shape_374,p:{x:29.1,y:-61.7}},{t:this.shape_373},{t:this.shape_372,p:{x:54,y:-62.8}},{t:this.shape_371,p:{x:61.7,y:-62.8}},{t:this.shape_370},{t:this.shape_369,p:{x:83.9,y:-62.8}},{t:this.shape_368,p:{x:104.2,y:-62.8}},{t:this.shape_367,p:{x:117.5,y:-64.2}},{t:this.shape_366,p:{x:130.1,y:-62.8}},{t:this.shape_365},{t:this.shape_364,p:{x:144,y:-62.8}},{t:this.shape_363},{t:this.shape_362,p:{x:164,y:-64.2}},{t:this.shape_361},{t:this.shape_360,p:{x:188.4,y:-61.3}},{t:this.shape_359,p:{x:186.6,y:-69.4}},{t:this.shape_358,p:{x:-258.2,y:-39.4}},{t:this.shape_357,p:{x:-244.6,y:-37.8}},{t:this.shape_356},{t:this.shape_355,p:{x:-225,y:-36.2}},{t:this.shape_354,p:{x:-211.6,y:-37.8}},{t:this.shape_353,p:{x:-202,y:-37.8}},{t:this.shape_352},{t:this.shape_351},{t:this.shape_350,p:{x:-165.3,y:-39.2}},{t:this.shape_349},{t:this.shape_348},{t:this.shape_347,p:{x:-133,y:-44.4}},{t:this.shape_346},{t:this.shape_345,p:{x:-108.1,y:-37.8}},{t:this.shape_344,p:{x:-97.6,y:-37.8}},{t:this.shape_343,p:{x:-86,y:-37.8}},{t:this.shape_342,p:{x:-72.5,y:-37.8}},{t:this.shape_341,p:{x:-67.7,y:-44.9}},{t:this.shape_340,p:{x:-52.6,y:-39.2}},{t:this.shape_339},{t:this.shape_338,p:{x:-40,y:-39.2}},{t:this.shape_337,p:{x:-31.5,y:-37.8}},{t:this.shape_336,p:{x:-25.7,y:-33.9}},{t:this.shape_335,p:{y:-89.8,x:-285.9}},{t:this.shape_334,p:{x:-258,y:13.9}},{t:this.shape_333},{t:this.shape_332,p:{x:-234.3,y:12.5}},{t:this.shape_331,p:{x:-215.5,y:13.9}},{t:this.shape_330,p:{x:-201.7,y:13.8}},{t:this.shape_329,p:{x:-196.9,y:6.8}},{t:this.shape_328,p:{x:-187.2,y:13.9}},{t:this.shape_327,p:{x:-186.3,y:7.3}},{t:this.shape_326,p:{x:-174.6,y:13.9}},{t:this.shape_325,p:{x:-163.1,y:13.8}},{t:this.shape_324,p:{x:-149.8,y:13.9}},{t:this.shape_323,p:{x:-145.3,y:6.8}},{t:this.shape_322},{t:this.shape_321,p:{x:-125.7,y:6.8}},{t:this.shape_320,p:{x:-117.1,y:12.4}},{t:this.shape_319,p:{x:-103.9,y:13.9}},{t:this.shape_318,p:{x:-94.6,y:13.9}},{t:this.shape_317,p:{x:-86.1,y:12.3}},{t:this.shape_316,p:{x:-73.1,y:13.8}},{t:this.shape_315,p:{x:-59.8,y:12.4}},{t:this.shape_314},{t:this.shape_313,p:{x:-27.8,y:15}},{t:this.shape_312},{t:this.shape_311,p:{x:-14.8,y:19}},{t:this.shape_310},{t:this.shape_309,p:{x:4.6,y:19}},{t:this.shape_308,p:{x:16.4,y:15.4}},{t:this.shape_307,p:{x:18.8,y:6.8}},{t:this.shape_306,p:{x:28,y:13.9}},{t:this.shape_305},{t:this.shape_304,p:{x:53.8,y:13.8}},{t:this.shape_303,p:{x:67.3,y:13.8}},{t:this.shape_302},{t:this.shape_301,p:{x:87.2,y:12.4}},{t:this.shape_300,p:{x:87,y:19}},{t:this.shape_299,p:{x:105.1,y:13.9}},{t:this.shape_298,p:{x:117.2,y:13.8}},{t:this.shape_297},{t:this.shape_296,p:{x:135.8,y:13.9}},{t:this.shape_295,p:{x:140.3,y:6.8}},{t:this.shape_294,p:{x:149,y:12.6}},{t:this.shape_293,p:{x:157.8,y:13.9}},{t:this.shape_292},{t:this.shape_291,p:{x:-254.7,y:31.8}},{t:this.shape_290,p:{x:-240.8,y:38.9}},{t:this.shape_289,p:{x:-228.7,y:38.8}},{t:this.shape_288,p:{x:-224,y:31.8}},{t:this.shape_287,p:{x:-214.3,y:38.9}},{t:this.shape_286},{t:this.shape_285,p:{x:-200.7,y:37.4}},{t:this.shape_284,p:{x:-181.2,y:38.9}},{t:this.shape_283,p:{x:-180.9,y:32.3}},{t:this.shape_282},{t:this.shape_281,p:{x:-158.9,y:38.9}},{t:this.shape_280,p:{x:-150,y:37.4}},{t:this.shape_279,p:{x:-131.2,y:37.3}},{t:this.shape_278,p:{x:-119.4,y:37.3}},{t:this.shape_277},{t:this.shape_276,p:{x:-87.8,y:40}},{t:this.shape_275,p:{x:-74.8,y:38.8}},{t:this.shape_274,p:{x:-63.2,y:38.9}},{t:this.shape_273,p:{x:-51.3,y:38.9}},{t:this.shape_272,p:{x:-46.9,y:31.8}},{t:this.shape_271},{t:this.shape_270,p:{x:-18.6,y:38.9}},{t:this.shape_269,p:{x:-18.2,y:44}},{t:this.shape_268,p:{x:-4.5,y:37.3}},{t:this.shape_267,p:{x:15.1,y:37.3}},{t:this.shape_266},{t:this.shape_265,p:{x:27.8,y:44}},{t:this.shape_264},{t:this.shape_263,p:{x:61.3,y:37.4}},{t:this.shape_262,p:{x:73.9,y:40.3}},{t:this.shape_261,p:{x:86.4,y:37.4}},{t:this.shape_260},{t:this.shape_259,p:{x:118,y:37.4}},{t:this.shape_258,p:{x:126.6,y:38.9}},{t:this.shape_257,p:{x:-259.6,y:63.9}},{t:this.shape_256,p:{x:-247.7,y:63.9}},{t:this.shape_255,p:{x:-238.6,y:63.9}},{t:this.shape_254,p:{x:-229.7,y:62.4}},{t:this.shape_253,p:{x:-216.7,y:65.4}},{t:this.shape_252,p:{x:-204.9,y:62.4}},{t:this.shape_251},{t:this.shape_250,p:{x:-172.6,y:63.8}},{t:this.shape_249,p:{x:-167.8,y:56.8}},{t:this.shape_248,p:{x:-159.1,y:62.4}},{t:this.shape_247,p:{x:-159.1,y:69}},{t:this.shape_246,p:{x:-146,y:63.9}},{t:this.shape_245,p:{x:-136.9,y:63.9}},{t:this.shape_244,p:{x:-131.1,y:67.8}},{t:this.shape_243},{t:this.shape_242,p:{x:-258,y:114.9}},{t:this.shape_241},{t:this.shape_240,p:{x:-244.7,y:120}},{t:this.shape_239,p:{x:-231,y:114.9}},{t:this.shape_238,p:{x:-226,y:107.8}},{t:this.shape_237,p:{x:-210.1,y:114.8}},{t:this.shape_236,p:{x:-209.5,y:108.3}},{t:this.shape_235,p:{x:-196.5,y:114.9}},{t:this.shape_234,p:{x:-196.3,y:108.3}},{t:this.shape_233,p:{x:-183.8,y:113.3}},{t:this.shape_232,p:{x:-172.8,y:114.9}},{t:this.shape_231,p:{x:-161.5,y:113.4}},{t:this.shape_230,p:{x:-148.2,y:114.8}},{t:this.shape_229,p:{x:-133.8,y:114.9}},{t:this.shape_228,p:{x:-128.8,y:107.8}},{t:this.shape_227,p:{x:-114.9,y:114.9}},{t:this.shape_226,p:{x:-102.8,y:114.8}},{t:this.shape_225,p:{x:-93.5,y:114.9}},{t:this.shape_224,p:{x:-84.1,y:114.9}},{t:this.shape_223,p:{x:-79.6,y:107.8}},{t:this.shape_222,p:{x:-70.9,y:113.6}},{t:this.shape_221,p:{x:-62.2,y:114.9}},{t:this.shape_220},{t:this.shape_219,p:{x:-33.9,y:114.9}},{t:this.shape_218,p:{x:-33.5,y:120}},{t:this.shape_217,p:{x:-20.6,y:113.4}},{t:this.shape_216,p:{x:-7.8,y:113.4}},{t:this.shape_215,p:{x:11,y:113.4}},{t:this.shape_214},{t:this.shape_213,p:{x:22.5,y:114.9}},{t:this.shape_212,p:{x:34,y:114.8}},{t:this.shape_211,p:{x:38.2,y:107.8}},{t:this.shape_210,p:{x:53.8,y:114.9}},{t:this.shape_209,p:{x:64.1,y:115.1}},{t:this.shape_208,p:{x:74.9,y:114.9}},{t:this.shape_207,p:{x:85.4,y:118.8}},{t:this.shape_206,p:{x:99.5,y:114.9}},{t:this.shape_205,p:{x:110.9,y:116.3}},{t:this.shape_204},{t:this.shape_203,p:{x:119.8,y:107.8}},{t:this.shape_202,p:{x:129.3,y:114.8}},{t:this.shape_201,p:{x:149.3,y:114.8}},{t:this.shape_200,p:{x:149.5,y:120}},{t:this.shape_199,p:{x:161.3,y:116.4}},{t:this.shape_198,p:{x:163.7,y:107.8}},{t:this.shape_197},{t:this.shape_196,p:{x:184.1,y:114.9}},{t:this.shape_195,p:{x:195.7,y:114.8}},{t:this.shape_194,p:{x:208.9,y:114.9}},{t:this.shape_193,p:{x:213.4,y:107.8}},{t:this.shape_192,p:{x:-259.6,y:139.9}},{t:this.shape_191,p:{x:-247.5,y:139.8}},{t:this.shape_190},{t:this.shape_189,p:{x:-221.9,y:138.4}},{t:this.shape_188,p:{x:-208.1,y:139.9}},{t:this.shape_187,p:{x:-203.1,y:132.8}},{t:this.shape_186,p:{x:-194.4,y:138.4}},{t:this.shape_185,p:{x:-175.4,y:139.9}},{t:this.shape_184,p:{x:-175.1,y:133.3}},{t:this.shape_183},{t:this.shape_182,p:{x:-142.8,y:139.9}},{t:this.shape_181,p:{x:-129.2,y:138.4}},{t:this.shape_180,p:{x:-110.6,y:138.5}},{t:this.shape_179,p:{x:-99.9,y:139.9}},{t:this.shape_178},{t:this.shape_177,p:{x:-69.9,y:138.3}},{t:this.shape_176,p:{x:-57,y:139.9}},{t:this.shape_175,p:{x:-43.7,y:139.8}},{t:this.shape_174,p:{x:-35,y:139.9}},{t:this.shape_173,p:{x:-26.2,y:138.4}},{t:this.shape_172,p:{x:-8.5,y:139.9}},{t:this.shape_171,p:{x:2.9,y:138.4}},{t:this.shape_170,p:{x:11.7,y:139.9}},{t:this.shape_169,p:{x:12,y:132.8}},{t:this.shape_168,p:{x:20.6,y:138.4}},{t:this.shape_167},{t:this.shape_166,p:{x:50.1,y:138.5}},{t:this.shape_165,p:{x:61.8,y:138.2}},{t:this.shape_164,p:{x:74.3,y:139.8}},{t:this.shape_163,p:{x:87.8,y:139.8}},{t:this.shape_162},{t:this.shape_161,p:{x:-246.4,y:166.3}},{t:this.shape_160,p:{x:-228.2,y:166.4}},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157,p:{x:-198.7,y:164.8}},{t:this.shape_156,p:{x:-180.7,y:164.9}},{t:this.shape_155,p:{x:-168.8,y:164.9}},{t:this.shape_154,p:{x:-159.7,y:164.9}},{t:this.shape_153,p:{x:-150.1,y:164.8}},{t:this.shape_152,p:{x:-139.6,y:165.1}},{t:this.shape_151,p:{x:-129.1,y:164.8}},{t:this.shape_150,p:{x:-128.4,y:158.3}},{t:this.shape_149,p:{x:-108.9,y:163.4}},{t:this.shape_148,p:{x:-96.1,y:163.4}},{t:this.shape_147,p:{x:-78.3,y:164.9}},{t:this.shape_146,p:{x:-66.2,y:164.8}},{t:this.shape_145,p:{x:-57,y:164.9}},{t:this.shape_144,p:{x:-47.6,y:164.9}},{t:this.shape_143,p:{x:-43.1,y:157.8}},{t:this.shape_142,p:{x:-34.4,y:163.6}},{t:this.shape_141,p:{x:-25.6,y:164.9}},{t:this.shape_140,p:{x:-10.4,y:163.4}},{t:this.shape_139,p:{x:2.1,y:163.4}},{t:this.shape_138,p:{x:15.2,y:164.8}},{t:this.shape_137,p:{x:24.5,y:164.9}},{t:this.shape_136,p:{x:40.3,y:164.9}},{t:this.shape_135,p:{x:54.3,y:164.9}},{t:this.shape_134,p:{x:59.3,y:157.8}},{t:this.shape_133,p:{x:69,y:164.9}},{t:this.shape_132,p:{x:78.6,y:164.9}},{t:this.shape_131,p:{x:84.4,y:168.8}},{t:this.shape_130,p:{x:99.6,y:163.3}},{t:this.shape_129,p:{x:103.4,y:157.8}},{t:this.shape_128,p:{x:118.8,y:163.2}},{t:this.shape_127,p:{x:131.8,y:164.9}},{t:this.shape_126,p:{x:136.2,y:157.8}},{t:this.shape_125,p:{x:143.7,y:164.9}},{t:this.shape_124},{t:this.shape_123,p:{x:159.6,y:157.8}},{t:this.shape_122,p:{x:-257.5,y:189.9}},{t:this.shape_121,p:{x:-243.9,y:188.4}},{t:this.shape_120,p:{x:-224.2,y:189.9}},{t:this.shape_119,p:{x:-223.9,y:183.3}},{t:this.shape_118,p:{x:-211.7,y:188.1}},{t:this.shape_117,p:{x:-199.7,y:188.4}},{t:this.shape_116,p:{x:-191,y:189.9}},{t:this.shape_115,p:{x:-179.9,y:188.6}},{t:this.shape_114,p:{x:-164.2,y:189.9}},{t:this.shape_113,p:{x:-163.9,y:183.2}},{t:this.shape_112,p:{x:-151.1,y:191.3}},{t:this.shape_111,p:{x:-151.2,y:183.2}},{t:this.shape_110},{t:this.shape_109,p:{x:-119,y:189.9}},{t:this.shape_108,p:{x:-114.5,y:182.8}},{t:this.shape_107,p:{x:-104.8,y:189.9}},{t:this.shape_106,p:{x:-90.7,y:189.9}},{t:this.shape_105,p:{x:-77.4,y:189.8}},{t:this.shape_104,p:{x:-59.4,y:189.9}},{t:this.shape_103,p:{x:-48,y:188.4}},{t:this.shape_102,p:{x:-36.4,y:189.9}},{t:this.shape_101,p:{x:-24.3,y:189.8}},{t:this.shape_100,p:{x:-10.9,y:189.8}},{t:this.shape_99,p:{x:1.8,y:188.4}},{t:this.shape_98},{t:this.shape_97,p:{x:19.7,y:189.9}},{t:this.shape_96,p:{x:31.8,y:189.8}},{t:this.shape_95,p:{x:41,y:189.9}},{t:this.shape_94,p:{x:50.4,y:189.9}},{t:this.shape_93,p:{x:54.9,y:182.8}},{t:this.shape_92,p:{x:63.6,y:188.6}},{t:this.shape_91,p:{x:72.4,y:189.9}},{t:this.shape_90},{t:this.shape_89,p:{x:92.6,y:189.9}},{t:this.shape_88,p:{x:104,y:188.4}},{t:this.shape_87,p:{x:112.7,y:189.9}},{t:this.shape_86,p:{x:113,y:182.8}},{t:this.shape_85,p:{x:121.6,y:188.4}},{t:this.shape_84,p:{x:133.5,y:188.5}},{t:this.shape_83,p:{x:145.5,y:189.8}},{t:this.shape_82,p:{x:159.1,y:189.8}},{t:this.shape_81,p:{x:163.9,y:182.8}},{t:this.shape_80,p:{x:179.1,y:189.9}},{t:this.shape_79,p:{x:191.7,y:188.3}},{t:this.shape_78,p:{x:204.7,y:189.8}},{t:this.shape_77,p:{x:218.1,y:191.3}},{t:this.shape_76,p:{x:-258.2,y:213.4}},{t:this.shape_75,p:{x:-245.2,y:214.8}},{t:this.shape_74,p:{x:-245.2,y:208.3}},{t:this.shape_73,p:{x:-232.3,y:214.9}},{t:this.shape_72,p:{x:-232,y:208.3}},{t:this.shape_71,p:{x:-218.4,y:214.8}},{t:this.shape_70,p:{x:-205,y:216.3}},{t:this.shape_69,p:{x:-192.2,y:214.8}},{t:this.shape_68,p:{x:-178.7,y:214.8}},{t:this.shape_67,p:{x:-173.9,y:207.8}},{t:this.shape_66,p:{x:-158.3,y:214.9}},{t:this.shape_65,p:{x:-157.9,y:208.3}},{t:this.shape_64},{t:this.shape_63,p:{x:-133,y:216.4}},{t:this.shape_62,p:{x:-122.1,y:213.4}},{t:this.shape_61},{t:this.shape_60,p:{x:-109.5,y:213.4}},{t:this.shape_59,p:{x:-100.9,y:214.9}},{t:this.shape_58,p:{x:-95.1,y:218.8}},{t:this.shape_57,p:{x:-80,y:216.5}},{t:this.shape_56,p:{x:-67.1,y:214.9}},{t:this.shape_55,p:{x:-62.6,y:207.8}},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52,p:{x:-40.5,y:220}},{t:this.shape_51,p:{x:-20.6,y:214.8}},{t:this.shape_50,p:{x:-19.9,y:220.7}},{t:this.shape_49,p:{x:-7.1,y:213.4}},{t:this.shape_48,p:{x:12.8,y:214.8}},{t:this.shape_47,p:{x:13.5,y:220.7}},{t:this.shape_46,p:{x:26.3,y:213.4}},{t:this.shape_45,p:{x:37.9,y:214.9}},{t:this.shape_44,p:{x:49.5,y:214.8}},{t:this.shape_43,p:{x:62.7,y:214.9}},{t:this.shape_42,p:{x:67.2,y:207.8}},{t:this.shape_41,p:{x:82.2,y:213.3}},{t:this.shape_40,p:{x:94.8,y:214.8}},{t:this.shape_39,p:{x:113.2,y:215.1}},{t:this.shape_38,p:{x:124.3,y:214.8}},{t:this.shape_37,p:{x:124.5,y:220}},{t:this.shape_36,p:{x:139.3,y:213.6}},{t:this.shape_35,p:{x:142,y:220}},{t:this.shape_34,p:{x:161.9,y:214.8}},{t:this.shape_33,p:{x:175.3,y:216.3}},{t:this.shape_32,p:{x:-259.6,y:239.9}},{t:this.shape_31,p:{x:-247.5,y:239.8}},{t:this.shape_30,p:{x:-238.3,y:239.9}},{t:this.shape_29,p:{x:-228.9,y:239.9}},{t:this.shape_28,p:{x:-224.4,y:232.8}},{t:this.shape_27,p:{x:-215.7,y:238.6}},{t:this.shape_26,p:{x:-206.9,y:239.9}},{t:this.shape_25,p:{x:-191.5,y:239.8}},{t:this.shape_24,p:{x:-177.7,y:239.9}},{t:this.shape_23},{t:this.shape_22,p:{x:-164.6,y:238.5}},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19,p:{x:-129.2,y:240.1}},{t:this.shape_18,p:{x:-120.3,y:240.1}},{t:this.shape_17,p:{x:-109.1,y:239.8}},{t:this.shape_16,p:{x:-89.5,y:239.9}},{t:this.shape_15,p:{x:-89.1,y:233.3}},{t:this.shape_14},{t:this.shape_13,p:{x:-64.2,y:241.4}},{t:this.shape_12,p:{x:-53.6,y:238.3}},{t:this.shape_11,p:{x:-41.4,y:238.4}},{t:this.shape_10,p:{x:-28.8,y:238.4}},{t:this.shape_9,p:{x:-9.8,y:239.9}},{t:this.shape_8,p:{x:3.2,y:238.4}},{t:this.shape_7,p:{x:16.9,y:239.9}},{t:this.shape_6,p:{x:21.9,y:232.8}},{t:this.shape_5,p:{x:37,y:238.4}},{t:this.shape_4,p:{x:37,y:245}},{t:this.shape_3,p:{x:50.1,y:239.9}},{t:this.shape_2},{t:this.shape_1,p:{x:65,y:243.8}},{t:this.shape},{t:this.instance}]}).to({state:[{t:this.shape_797},{t:this.shape_796},{t:this.shape_580,p:{x:-124.3}},{t:this.shape_795},{t:this.shape_794},{t:this.shape_793},{t:this.shape_792,p:{x:-92.2,y:-255.6}},{t:this.shape_791},{t:this.shape_790},{t:this.shape_789},{t:this.shape_788},{t:this.shape_787},{t:this.shape_786},{t:this.shape_785},{t:this.shape_784},{t:this.shape_783,p:{x:21.1,y:-255.5}},{t:this.shape_782},{t:this.shape_781},{t:this.shape_780},{t:this.shape_779},{t:this.shape_579,p:{x:95.1,y:-252.8}},{t:this.shape_778},{t:this.shape_575,p:{x:120,y:-254.1}},{t:this.shape_777},{t:this.shape_776},{t:this.shape_775},{t:this.shape_774},{t:this.shape_773},{t:this.shape_772},{t:this.shape_562,p:{x:245.1}},{t:this.shape_771},{t:this.shape_770},{t:this.shape_769,p:{x:279.1,y:-252.9}},{t:this.shape_568,p:{x:293,y:-253.1}},{t:this.shape_768},{t:this.shape_767},{t:this.shape_569,p:{x:339,y:-255.9}},{t:this.shape_766},{t:this.shape_564,p:{x:366.6,y:-253.1}},{t:this.shape_765,p:{x:383.2,y:-252.9}},{t:this.shape_764},{t:this.shape_763,p:{x:-258.2,y:-202.5}},{t:this.shape_762},{t:this.shape_761},{t:this.shape_760},{t:this.shape_544,p:{x:-213.5,y:-203.6}},{t:this.shape_759,p:{x:-204.1,y:-203.6}},{t:this.shape_502,p:{x:-196.3,y:-203.6}},{t:this.shape_758},{t:this.shape_757},{t:this.shape_515,p:{x:-153.4,y:-203.6}},{t:this.shape_756},{t:this.shape_476,p:{x:-136.3,y:-203.6}},{t:this.shape_755,p:{x:-124.4,y:-203.6}},{t:this.shape_754},{t:this.shape_479,p:{x:-115.1,y:-210.7}},{t:this.shape_482,p:{x:-99.5,y:-203.6}},{t:this.shape_753},{t:this.shape_384,p:{x:-86.3,y:-205}},{t:this.shape_455,p:{x:-74.4,y:-205}},{t:this.shape_752,p:{x:-65.2,y:-198.9}},{t:this.shape_289,p:{x:-49.3,y:-203.6}},{t:this.shape_751,p:{x:-36.8,y:-205.4}},{t:this.shape_465,p:{x:-25.9,y:-203.6}},{t:this.shape_750},{t:this.shape_749},{t:this.shape_472,p:{x:-5.5,y:-210.7}},{t:this.shape_748},{t:this.shape_747},{t:this.shape_746},{t:this.shape_745},{t:this.shape_378,p:{x:48.5,y:-203.6}},{t:this.shape_448,p:{x:56.3,y:-203.6}},{t:this.shape_744,p:{x:68.7,y:-203.6}},{t:this.shape_439,p:{x:73.7,y:-210.7}},{t:this.shape_743},{t:this.shape_742},{t:this.shape_477,p:{x:115.2,y:-203.6}},{t:this.shape_434,p:{x:119.7,y:-210.7}},{t:this.shape_442,p:{x:-259.6,y:-178.6}},{t:this.shape_382,p:{x:-248.2,y:-180}},{t:this.shape_555,p:{x:-239.7,y:-178.6}},{t:this.shape_419,p:{x:-239.4,y:-185.7}},{t:this.shape_741},{t:this.shape_400,p:{x:-226.7,y:-185.7}},{t:this.shape_740},{t:this.shape_461,p:{x:-206.3,y:-180.4}},{t:this.shape_739},{t:this.shape_738},{t:this.shape_396,p:{x:-170.9,y:-185.7}},{t:this.shape_473,p:{x:-161.7,y:-178.6}},{t:this.shape_737},{t:this.shape_736,p:{x:-151.3,y:-173.9}},{t:this.shape_438,p:{x:-137.2,y:-178.6}},{t:this.shape_735},{t:this.shape_734,p:{x:-117.2,y:-178.6}},{t:this.shape_392,p:{x:-116.9,y:-185.7}},{t:this.shape_733,p:{x:-101.7,y:-178.5}},{t:this.shape_732},{t:this.shape_480,p:{x:-88.1,y:-178.6}},{t:this.shape_731},{t:this.shape_431,p:{x:-55.1,y:-178.6}},{t:this.shape_489,p:{x:-45.7,y:-178.6}},{t:this.shape_730},{t:this.shape_729},{t:this.shape_463,p:{x:-23.9,y:-178.6}},{t:this.shape_385,p:{x:-19.4,y:-185.7}},{t:this.shape_728},{t:this.shape_541,p:{x:9.2,y:-178.6}},{t:this.shape_727},{t:this.shape_726},{t:this.shape_514,p:{x:35.7,y:-180}},{t:this.shape_725},{t:this.shape_724,p:{x:-240.4,y:-153.6}},{t:this.shape_427,p:{x:-226.6,y:-153.6}},{t:this.shape_380,p:{x:-221.8,y:-160.7}},{t:this.shape_435,p:{x:-206.2,y:-153.6}},{t:this.shape_462,p:{x:-205.9,y:-160.2}},{t:this.shape_118,p:{x:-193.7,y:-155.4}},{t:this.shape_723},{t:this.shape_459,p:{x:-173,y:-153.6}},{t:this.shape_722},{t:this.shape_325,p:{x:-146.5,y:-153.6}},{t:this.shape_71,p:{x:-126.6,y:-153.6}},{t:this.shape_721},{t:this.shape_720},{t:this.shape_719,p:{x:-82.1,y:-153.6}},{t:this.shape_375,p:{x:-77.7,y:-160.7}},{t:this.shape_420,p:{x:-68.5,y:-153.6}},{t:this.shape_347,p:{x:-68.2,y:-160.2}},{t:this.shape_718,p:{x:-48.8,y:-153.6}},{t:this.shape_717},{t:this.shape_281,p:{x:-27.1,y:-153.6}},{t:this.shape_546,p:{x:-17.7,y:-153.6}},{t:this.shape_412,p:{x:-5.8,y:-153.6}},{t:this.shape_716},{t:this.shape_402,p:{x:24.6,y:-153.6}},{t:this.shape_715},{t:this.shape_714},{t:this.shape_713},{t:this.shape_712,p:{x:57.8,y:-153.6}},{t:this.shape_512,p:{x:67.7,y:-153.6}},{t:this.shape_711},{t:this.shape_362,p:{x:86.9,y:-155}},{t:this.shape_441,p:{x:98.8,y:-155}},{t:this.shape_304,p:{x:110.9,y:-153.6}},{t:this.shape_446,p:{x:119.6,y:-153.6}},{t:this.shape_275,p:{x:128.7,y:-153.6}},{t:this.shape_710},{t:this.shape_450,p:{x:138.4,y:-149.7}},{t:this.shape_709},{t:this.shape_410,p:{x:-259.6,y:-104.6}},{t:this.shape_708},{t:this.shape_707},{t:this.shape_226,p:{x:-229.8,y:-104.6}},{t:this.shape_706},{t:this.shape_411,p:{x:-209.3,y:-104.6}},{t:this.shape_705},{t:this.shape_704},{t:this.shape_470,p:{x:-187,y:-104.6}},{t:this.shape_397,p:{x:-170.9,y:-104.6}},{t:this.shape_703,p:{x:-157.7,y:-103.8}},{t:this.shape_702},{t:this.shape_701,p:{x:-138.7,y:-111.2}},{t:this.shape_498,p:{x:-126,y:-106}},{t:this.shape_700},{t:this.shape_350,p:{x:-94.4,y:-106}},{t:this.shape_699},{t:this.shape_536,p:{x:-73.1,y:-104.6}},{t:this.shape_483,p:{x:-63.2,y:-104.6}},{t:this.shape_341,p:{x:-58.2,y:-111.7}},{t:this.shape_698},{t:this.shape_697,p:{x:-43,y:-111.2}},{t:this.shape_516,p:{x:-42.9,y:-98.7}},{t:this.shape_212,p:{x:-29.9,y:-104.6}},{t:this.shape_386,p:{x:-10,y:-104.6}},{t:this.shape_388,p:{x:2.3,y:-104.6}},{t:this.shape_696,p:{x:13.5,y:-103.8}},{t:this.shape_329,p:{x:17.1,y:-111.7}},{t:this.shape_695},{t:this.shape_418,p:{x:45.7,y:-104.6}},{t:this.shape_417,p:{x:46.6,y:-98.7}},{t:this.shape_694,p:{x:66.1,y:-103.5}},{t:this.shape_693,p:{x:76.2,y:-104.4}},{t:this.shape_692},{t:this.shape_691},{t:this.shape_423,p:{x:110.7,y:-106}},{t:this.shape_201,p:{x:123.3,y:-104.6}},{t:this.shape_690},{t:this.shape_336,p:{x:133,y:-100.7}},{t:this.shape_191,p:{x:-257.7,y:-79.6}},{t:this.shape_689,p:{x:-247.2,y:-79.4}},{t:this.shape_368,p:{x:-236.7,y:-79.6}},{t:this.shape_236,p:{x:-236,y:-86.2}},{t:this.shape_195,p:{x:-223,y:-79.6}},{t:this.shape_245,p:{x:-214.3,y:-79.6}},{t:this.shape_340,p:{x:-205.4,y:-81}},{t:this.shape_122,p:{x:-185.5,y:-79.6}},{t:this.shape_444,p:{x:-184.6,y:-86.2}},{t:this.shape_688,p:{x:-171.5,y:-81}},{t:this.shape_687},{t:this.shape_686,p:{x:-156.4,y:-80.9}},{t:this.shape_414,p:{x:-140.2,y:-79.6}},{t:this.shape_323,p:{x:-135.2,y:-86.7}},{t:this.shape_486,p:{x:-119.8,y:-78.5}},{t:this.shape_338,p:{x:-107,y:-81}},{t:this.shape_114,p:{x:-94,y:-79.6}},{t:this.shape_685,p:{x:-81.8,y:-79.6}},{t:this.shape_684},{t:this.shape_153,p:{x:-50.2,y:-79.6}},{t:this.shape_683},{t:this.shape_682},{t:this.shape_342,p:{x:-22.9,y:-79.6}},{t:this.shape_681,p:{x:-9.7,y:-81.5}},{t:this.shape_128,p:{x:9.2,y:-81.2}},{t:this.shape_146,p:{x:22.8,y:-79.6}},{t:this.shape_680},{t:this.shape_127,p:{x:42.7,y:-79.6}},{t:this.shape_381,p:{x:56.7,y:-79.6}},{t:this.shape_679},{t:this.shape_320,p:{x:70.3,y:-81}},{t:this.shape_344,p:{x:88.1,y:-79.6}},{t:this.shape_401,p:{x:100,y:-79.6}},{t:this.shape_519,p:{x:109.1,y:-79.6}},{t:this.shape_678,p:{x:117.6,y:-81.1}},{t:this.shape_175,p:{x:130,y:-79.6}},{t:this.shape_244,p:{x:139.6,y:-75.7}},{t:this.shape_677},{t:this.shape_369,p:{x:-257.9,y:-25.6}},{t:this.shape_676},{t:this.shape_315,p:{x:-244.8,y:-27}},{t:this.shape_326,p:{x:-233.4,y:-25.6}},{t:this.shape_675},{t:this.shape_321,p:{x:-217.5,y:-32.7}},{t:this.shape_299,p:{x:-203.5,y:-25.6}},{t:this.shape_330,p:{x:-191.3,y:-25.6}},{t:this.shape_474,p:{x:-181.9,y:-25.6}},{t:this.shape_367,p:{x:-173.1,y:-27}},{t:this.shape_674},{t:this.shape_316,p:{x:-146.8,y:-25.6}},{t:this.shape_301,p:{x:-127,y:-27}},{t:this.shape_673},{t:this.shape_303,p:{x:-107.2,y:-25.6}},{t:this.shape_672},{t:this.shape_671},{t:this.shape_258,p:{x:-84.9,y:-25.6}},{t:this.shape_670},{t:this.shape_669},{t:this.shape_668,p:{x:-56.7,y:-20.5}},{t:this.shape_285,p:{x:-44.1,y:-27}},{t:this.shape_34,p:{x:-30.7,y:-25.6}},{t:this.shape_545,p:{x:-10.9,y:-27.1}},{t:this.shape_667,p:{x:-10.7,y:-20.5}},{t:this.shape_666},{t:this.shape_327,p:{x:1.8,y:-32.2}},{t:this.shape_324,p:{x:15.1,y:-25.6}},{t:this.shape_307,p:{x:19.6,y:-32.7}},{t:this.shape_422,p:{x:28.3,y:-27.1}},{t:this.shape_665},{t:this.shape_364,p:{x:61,y:-25.6}},{t:this.shape_413,p:{x:61.9,y:-19.7}},{t:this.shape_389,p:{x:81.3,y:-24.5}},{t:this.shape_664,p:{x:91.4,y:-25.4}},{t:this.shape_253,p:{x:101.5,y:-24.1}},{t:this.shape_359,p:{x:100.5,y:-32.2}},{t:this.shape_280,p:{x:113.4,y:-27.1}},{t:this.shape_261,p:{x:126,y:-27}},{t:this.shape_663},{t:this.shape_231,p:{x:-245.7,y:-2}},{t:this.shape_296,p:{x:-232.7,y:-0.6}},{t:this.shape_295,p:{x:-228.2,y:-7.7}},{t:this.shape_466,p:{x:-219,y:-0.6}},{t:this.shape_354,p:{x:-205,y:-0.6}},{t:this.shape_291,p:{x:-200,y:-7.7}},{t:this.shape_290,p:{x:-186.1,y:-0.6}},{t:this.shape_662},{t:this.shape_174,p:{x:-165.9,y:-0.6}},{t:this.shape_138,p:{x:-156.3,y:-0.6}},{t:this.shape_661},{t:this.shape_216,p:{x:-143.1,y:-2}},{t:this.shape_660},{t:this.shape_287,p:{x:-110.7,y:-0.6}},{t:this.shape_50,p:{x:-109.8,y:5.3}},{t:this.shape_374,p:{x:-90.4,y:0.5}},{t:this.shape_659},{t:this.shape_284,p:{x:-64.9,y:-0.6}},{t:this.shape_288,p:{x:-60.4,y:-7.7}},{t:this.shape_256,p:{x:-51.2,y:-0.6}},{t:this.shape_658},{t:this.shape_188,p:{x:-17.7,y:-0.6}},{t:this.shape_272,p:{x:-12.7,y:-7.7}},{t:this.shape_657},{t:this.shape_436,p:{x:5.1,y:-0.6}},{t:this.shape_101,p:{x:14.7,y:-0.6}},{t:this.shape_306,p:{x:34.9,y:-0.6}},{t:this.shape_246,p:{x:48.4,y:-0.6}},{t:this.shape_249,p:{x:52.9,y:-7.7}},{t:this.shape_242,p:{x:62.1,y:-0.6}},{t:this.shape_225,p:{x:71.2,y:-0.6}},{t:this.shape_656},{t:this.shape_655},{t:this.shape_654},{t:this.shape_274,p:{x:-259.6,y:24.4}},{t:this.shape_49,p:{x:-248.2,y:22.9}},{t:this.shape_653},{t:this.shape_257,p:{x:-231.7,y:24.4}},{t:this.shape_96,p:{x:-219.6,y:24.4}},{t:this.shape_132,p:{x:-210.3,y:24.4}},{t:this.shape_24,p:{x:-200.4,y:24.4}},{t:this.shape_237,p:{x:-186,y:24.4}},{t:this.shape_46,p:{x:-172.6,y:22.9}},{t:this.shape_11,p:{x:-153.5,y:22.9}},{t:this.shape_222,p:{x:-140.7,y:23.1}},{t:this.shape_240,p:{x:-140.5,y:29.5}},{t:this.shape_232,p:{x:-129,y:24.4}},{t:this.shape_652},{t:this.shape_651},{t:this.shape_650,p:{x:-99.1,y:17.8}},{t:this.shape_649},{t:this.shape_648},{t:this.shape_164,p:{x:-55.5,y:24.4}},{t:this.shape_230,p:{x:-41.9,y:24.4}},{t:this.shape_647},{t:this.shape_217,p:{x:-21.8,y:23}},{t:this.shape_154,p:{x:-12.9,y:24.4}},{t:this.shape_646},{t:this.shape_318,p:{x:4.8,y:24.4}},{t:this.shape_202,p:{x:20.9,y:24.4}},{t:this.shape_409,p:{x:33.7,y:23}},{t:this.shape_224,p:{x:46.1,y:24.4}},{t:this.shape_238,p:{x:50.6,y:17.3}},{t:this.shape_210,p:{x:59.8,y:24.4}},{t:this.shape_233,p:{x:79,y:22.9}},{t:this.shape_157,p:{x:91.3,y:24.4}},{t:this.shape_207,p:{x:100.9,y:28.3}},{t:this.shape_189,p:{x:-258.2,y:48}},{t:this.shape_530,p:{x:-249.3,y:49.4}},{t:this.shape_645},{t:this.shape_145,p:{x:-231.6,y:49.4}},{t:this.shape_355,p:{x:-216.4,y:51}},{t:this.shape_263,p:{x:-204.1,y:48}},{t:this.shape_219,p:{x:-191.4,y:49.4}},{t:this.shape_194,p:{x:-177.9,y:49.4}},{t:this.shape_228,p:{x:-173.4,y:42.3}},{t:this.shape_176,p:{x:-164.2,y:49.4}},{t:this.shape_149,p:{x:-144.5,y:48}},{t:this.shape_644},{t:this.shape_643},{t:this.shape_259,p:{x:-107,y:48}},{t:this.shape_642,p:{x:-107.2,y:54.6}},{t:this.shape_7,p:{x:-86.9,y:49.4}},{t:this.shape_641,p:{x:-76.1,y:49.6}},{t:this.shape_155,p:{x:-65.8,y:49.4}},{t:this.shape_227,p:{x:-47.5,y:49.4}},{t:this.shape_252,p:{x:-36.1,y:48}},{t:this.shape_255,p:{x:-27.6,y:49.4}},{t:this.shape_223,p:{x:-27.3,y:42.3}},{t:this.shape_640,p:{x:-18.6,y:50.8}},{t:this.shape_211,p:{x:-14.6,y:42.3}},{t:this.shape_173,p:{x:-6,y:48}},{t:this.shape_639},{t:this.shape_250,p:{x:-257.6,y:74.4}},{t:this.shape_332,p:{x:-244.8,y:73}},{t:this.shape_199,p:{x:-233.7,y:75.9}},{t:this.shape_180,p:{x:-223.3,y:73}},{t:this.shape_163,p:{x:-210.6,y:74.4}},{t:this.shape_221,p:{x:-201.2,y:74.4}},{t:this.shape_166,p:{x:-192.9,y:73}},{t:this.shape_638},{t:this.shape_105,p:{x:-162.4,y:74.4}},{t:this.shape_116,p:{x:-153.6,y:74.4}},{t:this.shape_73,p:{x:-138.2,y:74.4}},{t:this.shape_72,p:{x:-138,y:67.8}},{t:this.shape_182,p:{x:-124.1,y:74.4}},{t:this.shape_637},{t:this.shape_248,p:{x:-110.5,y:73}},{t:this.shape_254,p:{x:-97.9,y:72.9}},{t:this.shape_168,p:{x:-85.3,y:73}},{t:this.shape_144,p:{x:-65.9,y:74.4}},{t:this.shape_636},{t:this.shape_273,p:{x:-45.3,y:74.4}},{t:this.shape_113,p:{x:-45.1,y:67.8}},{t:this.shape_100,p:{x:-32.1,y:74.4}},{t:this.shape_131,p:{x:-22.4,y:78.3}},{t:this.shape_635,p:{x:-285.9,y:-27.6}},{t:this.shape_634},{t:this.shape_633,p:{x:-245.8,y:124.2}},{t:this.shape_8,p:{x:-233.5,y:121.9}},{t:this.shape_218,p:{x:-233.3,y:128.5}},{t:this.shape_213,p:{x:-221.8,y:123.4}},{t:this.shape_215,p:{x:-210.5,y:122}},{t:this.shape_120,p:{x:-197.4,y:123.4}},{t:this.shape_203,p:{x:-192.9,y:116.3}},{t:this.shape_632,p:{x:-180.2,y:115.8}},{t:this.shape_631},{t:this.shape_198,p:{x:-165.1,y:116.3}},{t:this.shape_630},{t:this.shape_629},{t:this.shape_206,p:{x:-127,y:123.4}},{t:this.shape_76,p:{x:-115.3,y:122}},{t:this.shape_137,p:{x:-106.5,y:123.4}},{t:this.shape_193,p:{x:-106.2,y:116.3}},{t:this.shape_628},{t:this.shape_79,p:{x:-79.3,y:121.9}},{t:this.shape_627},{t:this.shape_626},{t:this.shape_625,p:{x:-56.4,y:115.8}},{t:this.shape_83,p:{x:-39.7,y:123.4}},{t:this.shape_109,p:{x:-26.5,y:123.4}},{t:this.shape_200,p:{x:-25.9,y:128.5}},{t:this.shape_196,p:{x:-14.4,y:123.4}},{t:this.shape_186,p:{x:-3.1,y:122}},{t:this.shape_106,p:{x:10,y:123.4}},{t:this.shape_187,p:{x:14.5,y:116.3}},{t:this.shape_160,p:{x:28.8,y:124.9}},{t:this.shape_57,p:{x:46,y:125}},{t:this.shape_94,p:{x:58.9,y:123.4}},{t:this.shape_169,p:{x:63.4,y:116.3}},{t:this.shape_624},{t:this.shape_623},{t:this.shape_4,p:{x:91.9,y:128.5}},{t:this.shape_622,p:{x:-260.8,y:140.8}},{t:this.shape_621},{t:this.shape_151,p:{x:-237.3,y:148.4}},{t:this.shape_111,p:{x:-236.7,y:141.8}},{t:this.shape_171,p:{x:-217.4,y:146.9}},{t:this.shape_148,p:{x:-204.7,y:147}},{t:this.shape_311,p:{x:-204.7,y:153.5}},{t:this.shape_620,p:{x:-192,y:148.4}},{t:this.shape_143,p:{x:-187.6,y:141.3}},{t:this.shape_619,p:{x:-180.8,y:140.8}},{t:this.shape_192,p:{x:-165.4,y:148.4}},{t:this.shape_618},{t:this.shape_617},{t:this.shape_63,p:{x:-122.7,y:149.9}},{t:this.shape_616},{t:this.shape_309,p:{x:-104.8,y:153.5}},{t:this.shape_615},{t:this.shape_47,p:{x:-92,y:154.3}},{t:this.shape_235,p:{x:-79.1,y:148.4}},{t:this.shape_234,p:{x:-78.8,y:141.8}},{t:this.shape_13,p:{x:-66.8,y:149.9}},{t:this.shape_134,p:{x:-64.4,y:141.3}},{t:this.shape_614},{t:this.shape_82,p:{x:-35.6,y:148.4}},{t:this.shape_59,p:{x:-26.2,y:148.4}},{t:this.shape_139,p:{x:-17.4,y:147}},{t:this.shape_300,p:{x:-17.7,y:153.6}},{t:this.shape_66,p:{x:-4.4,y:148.4}},{t:this.shape_129,p:{x:0.1,y:141.3}},{t:this.shape_613,p:{x:15.2,y:147}},{t:this.shape_612},{t:this.shape_135,p:{x:40.3,y:148.4}},{t:this.shape_75,p:{x:54.1,y:148.4}},{t:this.shape_611},{t:this.shape_31,p:{x:-244.9,y:173.4}},{t:this.shape_184,p:{x:-244.4,y:166.8}},{t:this.shape_140,p:{x:-225.2,y:172}},{t:this.shape_610},{t:this.shape_179,p:{x:-213.7,y:173.4}},{t:this.shape_69,p:{x:-202.2,y:173.4}},{t:this.shape_126,p:{x:-198,y:166.3}},{t:this.shape_130,p:{x:-183,y:171.9}},{t:this.shape_172,p:{x:-171.7,y:173.4}},{t:this.shape_161,p:{x:-160.3,y:174.8}},{t:this.shape_504,p:{x:-147.6,y:173.4}},{t:this.shape_119,p:{x:-147.4,y:166.8}},{t:this.shape_56,p:{x:-134,y:173.4}},{t:this.shape_15,p:{x:-133.7,y:166.8}},{t:this.shape_58,p:{x:-123.9,y:177.3}},{t:this.shape_449,p:{y:121.4,x:-285.9}},{t:this.shape_313,p:{x:-258.2,y:220.3}},{t:this.shape_609,p:{x:-248.1,y:219.4}},{t:this.shape_121,p:{x:-238.3,y:217.7}},{t:this.shape_608},{t:this.shape_78,p:{x:-213.5,y:219.2}},{t:this.shape_87,p:{x:-204.1,y:219.2}},{t:this.shape_156,p:{x:-196.3,y:219.2}},{t:this.shape_607},{t:this.shape_606},{t:this.shape_117,p:{x:-153.9,y:217.7}},{t:this.shape_115,p:{x:-139,y:217.9}},{t:this.shape_68,p:{x:-122.9,y:219.2}},{t:this.shape_123,p:{x:-118.1,y:212.1}},{t:this.shape_147,p:{x:-104.2,y:219.2}},{t:this.shape_43,p:{x:-92.3,y:219.2}},{t:this.shape_95,p:{x:-83.2,y:219.2}},{t:this.shape_99,p:{x:-74.3,y:217.8}},{t:this.shape_103,p:{x:-55.4,y:217.7}},{t:this.shape_247,p:{x:-55.2,y:224.3}},{t:this.shape_268,p:{x:-41.5,y:217.7}},{t:this.shape_125,p:{x:-22.7,y:219.2}},{t:this.shape_605},{t:this.shape_467,p:{x:-2.7,y:219.2}},{t:this.shape_104,p:{x:5,y:219.2}},{t:this.shape_604,p:{x:16,y:217.7}},{t:this.shape_88,p:{x:28.2,y:217.7}},{t:this.shape_51,p:{x:48.2,y:219.2}},{t:this.shape_453,p:{x:57.6,y:219.2}},{t:this.shape_85,p:{x:66.4,y:217.8}},{t:this.shape_603},{t:this.shape_29,p:{x:79.4,y:219.2}},{t:this.shape_108,p:{x:83.9,y:212.1}},{t:this.shape_48,p:{x:93.4,y:219.2}},{t:this.shape_84,p:{x:106.2,y:217.8}},{t:this.shape_36,p:{x:120.3,y:217.9}},{t:this.shape_102,p:{x:134.4,y:219.2}},{t:this.shape_44,p:{x:145.9,y:219.2}},{t:this.shape_93,p:{x:150.1,y:212.1}},{t:this.shape_602},{t:this.shape_601,p:{x:-258.1,y:249.3}},{t:this.shape_97,p:{x:-246.6,y:244.2}},{t:this.shape_22,p:{x:-235.8,y:242.8}},{t:this.shape_30,p:{x:-227.9,y:244.2}},{t:this.shape_86,p:{x:-227.6,y:237.1}},{t:this.shape_136,p:{x:-218.4,y:244.2}},{t:this.shape_91,p:{x:-209.4,y:244.2}},{t:this.shape_600},{t:this.shape_40,p:{x:-188.3,y:244.2}},{t:this.shape_141,p:{x:-179.5,y:244.2}},{t:this.shape_12,p:{x:-164.6,y:242.7}},{t:this.shape_599,p:{x:-152.4,y:242.7}},{t:this.shape_10,p:{x:-139.8,y:242.8}},{t:this.shape_89,p:{x:-122.1,y:244.2}},{t:this.shape_62,p:{x:-110.7,y:242.8}},{t:this.shape_170,p:{x:-102.2,y:244.2}},{t:this.shape_81,p:{x:-101.9,y:237.1}},{t:this.shape_598},{t:this.shape_67,p:{x:-89.2,y:237.1}},{t:this.shape_597},{t:this.shape_404,p:{x:-62.2,y:242.3}},{t:this.shape_185,p:{x:-49.4,y:244.2}},{t:this.shape_55,p:{x:-45,y:237.1}},{t:this.shape_16,p:{x:-35.8,y:244.2}},{t:this.shape_407,p:{x:-35.4,y:237.6}},{t:this.shape_276,p:{x:-15.9,y:245.3}},{t:this.shape_60,p:{x:-3.1,y:242.8}},{t:this.shape_9,p:{x:9.5,y:244.2}},{t:this.shape_26,p:{x:18.5,y:244.2}},{t:this.shape_3,p:{x:27.9,y:244.2}},{t:this.shape_45,p:{x:39.9,y:244.2}},{t:this.shape_38,p:{x:51.4,y:244.2}},{t:this.shape_42,p:{x:55.6,y:237.1}},{t:this.shape_596,p:{x:63.5,y:244.4}},{t:this.shape_595},{t:this.shape_594,p:{x:94,y:242.7}},{t:this.shape_107,p:{x:107.7,y:244.2}},{t:this.shape_28,p:{x:112.7,y:237.1}},{t:this.shape_5,p:{x:127.8,y:242.8}},{t:this.shape_593},{t:this.shape_32,p:{x:139.3,y:244.2}},{t:this.shape_25,p:{x:150.8,y:244.2}},{t:this.shape_6,p:{x:155,y:237.1}},{t:this.shape_17,p:{x:163.8,y:244.2}},{t:this.shape_1,p:{x:173.5,y:248.1}},{t:this.shape_335,p:{y:217.2,x:-285.9}}]},1).to({state:[{t:this.shape_865},{t:this.shape_864},{t:this.shape_325,p:{x:-238.2,y:-150.8}},{t:this.shape_290,p:{x:-220.2,y:-150.8}},{t:this.shape_112,p:{x:-208.8,y:-149.4}},{t:this.shape_337,p:{x:-200.2,y:-150.8}},{t:this.shape_398,p:{x:-191.4,y:-152.2}},{t:this.shape_544,p:{x:-178,y:-150.8}},{t:this.shape_341,p:{x:-173.2,y:-157.9}},{t:this.shape_153,p:{x:-157.4,y:-150.8}},{t:this.shape_863},{t:this.shape_487,p:{x:-131.6,y:-150.8}},{t:this.shape_482,p:{x:-118,y:-150.8}},{t:this.shape_146,p:{x:-97.8,y:-150.8}},{t:this.shape_862},{t:this.shape_861},{t:this.shape_274,p:{x:-61,y:-150.8}},{t:this.shape_860},{t:this.shape_519,p:{x:-40.7,y:-150.8}},{t:this.shape_329,p:{x:-40.4,y:-157.9}},{t:this.shape_261,p:{x:-31.8,y:-152.2}},{t:this.shape_859,p:{x:-20.1,y:-152.6}},{t:this.shape_515,p:{x:-0.9,y:-150.8}},{t:this.shape_458,p:{x:13.5,y:-150.8}},{t:this.shape_225,p:{x:23.1,y:-150.8}},{t:this.shape_422,p:{x:38.4,y:-152.3}},{t:this.shape_245,p:{x:47.2,y:-150.8}},{t:this.shape_77,p:{x:56.1,y:-149.4}},{t:this.shape_318,p:{x:64.7,y:-150.8}},{t:this.shape_257,p:{x:72.4,y:-150.8}},{t:this.shape_604,p:{x:83.4,y:-152.3}},{t:this.shape_477,p:{x:102.5,y:-150.8}},{t:this.shape_858},{t:this.shape_857},{t:this.shape_280,p:{x:127,y:-152.3}},{t:this.shape_255,p:{x:135.8,y:-150.8}},{t:this.shape_856},{t:this.shape_855},{t:this.shape_391,p:{x:164.8,y:-152.2}},{t:this.shape_854},{t:this.shape_304,p:{x:188.8,y:-150.8}},{t:this.shape_275,p:{x:201.7,y:-150.8}},{t:this.shape_407,p:{x:201.7,y:-157.4}},{t:this.shape_207,p:{x:211.5,y:-146.9}},{t:this.shape_449,p:{y:-152.8,x:-285.9}},{t:this.shape_853},{t:this.shape_19,p:{x:-248.3,y:-91.6}},{t:this.shape_212,p:{x:-238.4,y:-91.8}},{t:this.shape_852},{t:this.shape_480,p:{x:-206.1,y:-91.8}},{t:this.shape_323,p:{x:-201.3,y:-98.9}},{t:this.shape_851},{t:this.shape_473,p:{x:-175.5,y:-91.8}},{t:this.shape_431,p:{x:-161.6,y:-91.8}},{t:this.shape_697,p:{x:-160.9,y:-98.4}},{t:this.shape_463,p:{x:-147.6,y:-91.8}},{t:this.shape_321,p:{x:-143.1,y:-98.9}},{t:this.shape_850},{t:this.shape_41,p:{x:-122,y:-93.3}},{t:this.shape_427,p:{x:-108.8,y:-91.8}},{t:this.shape_307,p:{x:-104,y:-98.9}},{t:this.shape_384,p:{x:-88.9,y:-93.2}},{t:this.shape_719,p:{x:-76.3,y:-91.8}},{t:this.shape_496,p:{x:-63.9,y:-93.2}},{t:this.shape_411,p:{x:-51.2,y:-91.8}},{t:this.shape_849,p:{x:-50.5,y:-98.4}},{t:this.shape_435,p:{x:-37.2,y:-91.8}},{t:this.shape_295,p:{x:-32.7,y:-98.9}},{t:this.shape_848},{t:this.shape_847},{t:this.shape_328,p:{x:7.7,y:-91.8}},{t:this.shape_201,p:{x:21.5,y:-91.8}},{t:this.shape_846},{t:this.shape_87,p:{x:49.7,y:-91.8}},{t:this.shape_845},{t:this.shape_495,p:{x:69.7,y:-93.4}},{t:this.shape_397,p:{x:82.8,y:-91.8}},{t:this.shape_386,p:{x:103.4,y:-91.8}},{t:this.shape_455,p:{x:116.2,y:-93.2}},{t:this.shape_466,p:{x:128.6,y:-91.8}},{t:this.shape_239,p:{x:142.6,y:-91.8}},{t:this.shape_291,p:{x:147.6,y:-98.9}},{t:this.shape_368,p:{x:-257.8,y:-66.8}},{t:this.shape_467,p:{x:-248.4,y:-66.8}},{t:this.shape_420,p:{x:-239,y:-66.8}},{t:this.shape_288,p:{x:-234.5,y:-73.9}},{t:this.shape_844},{t:this.shape_59,p:{x:-215.2,y:-66.8}},{t:this.shape_382,p:{x:-206.4,y:-68.2}},{t:this.shape_342,p:{x:-193,y:-66.8}},{t:this.shape_272,p:{x:-188.2,y:-73.9}},{t:this.shape_232,p:{x:-174.3,y:-66.8}},{t:this.shape_402,p:{x:-162.4,y:-66.8}},{t:this.shape_145,p:{x:-153.3,y:-66.8}},{t:this.shape_362,p:{x:-144.4,y:-68.2}},{t:this.shape_401,p:{x:-131.4,y:-66.8}},{t:this.shape_843,p:{x:-111.6,y:-65.7}},{t:this.shape_842},{t:this.shape_441,p:{x:-84.8,y:-68.2}},{t:this.shape_841},{t:this.shape_254,p:{x:-51.9,y:-68.3}},{t:this.shape_840},{t:this.shape_409,p:{x:-18.7,y:-68.2}},{t:this.shape_693,p:{x:-9.6,y:-66.6}},{t:this.shape_686,p:{x:2.3,y:-68.1}},{t:this.shape_839},{t:this.shape_278,p:{x:36.1,y:-68.3}},{t:this.shape_838},{t:this.shape_227,p:{x:61.7,y:-66.8}},{t:this.shape_195,p:{x:73.3,y:-66.8}},{t:this.shape_249,p:{x:77.4,y:-73.9}},{t:this.shape_718,p:{x:-258.5,y:-41.8}},{t:this.shape_74,p:{x:-258.3,y:-48.4}},{t:this.shape_231,p:{x:-245.5,y:-43.2}},{t:this.shape_330,p:{x:-225.9,y:-41.8}},{t:this.shape_681,p:{x:-212.7,y:-43.7}},{t:this.shape_316,p:{x:-199.6,y:-41.8}},{t:this.shape_213,p:{x:-187.3,y:-41.8}},{t:this.shape_216,p:{x:-176,y:-43.2}},{t:this.shape_116,p:{x:-167.6,y:-41.8}},{t:this.shape_837},{t:this.shape_306,p:{x:-139.4,y:-41.8}},{t:this.shape_115,p:{x:-124.2,y:-43.1}},{t:this.shape_369,p:{x:-108.5,y:-41.8}},{t:this.shape_137,p:{x:-99.4,y:-41.8}},{t:this.shape_350,p:{x:-90.5,y:-43.2}},{t:this.shape_303,p:{x:-77.2,y:-41.8}},{t:this.shape_173,p:{x:-63.9,y:-43.2}},{t:this.shape_121,p:{x:-45,y:-43.3}},{t:this.shape_667,p:{x:-44.8,y:-36.7}},{t:this.shape_138,p:{x:-31.5,y:-41.8}},{t:this.shape_247,p:{x:-30.8,y:-36.7}},{t:this.shape_36,p:{x:-16,y:-43.1}},{t:this.shape_206,p:{x:4.5,y:-41.8}},{t:this.shape_63,p:{x:15.1,y:-40.3}},{t:this.shape_324,p:{x:26.6,y:-41.8}},{t:this.shape_238,p:{x:31.1,y:-48.9}},{t:this.shape_296,p:{x:40.3,y:-41.8}},{t:this.shape_168,p:{x:53.3,y:-43.2}},{t:this.shape_836,p:{x:72.4,y:-40.7}},{t:this.shape_689,p:{x:82.5,y:-41.6}},{t:this.shape_175,p:{x:92.4,y:-41.8}},{t:this.shape_139,p:{x:105.1,y:-43.2}},{t:this.shape_219,p:{x:124,y:-41.8}},{t:this.shape_835},{t:this.shape_834},{t:this.shape_267,p:{x:153.4,y:-43.3}},{t:this.shape_164,p:{x:165.7,y:-41.8}},{t:this.shape_131,p:{x:175.3,y:-37.9}},{t:this.shape_335,p:{y:-93.8,x:-285.9}},{t:this.shape_833},{t:this.shape_233,p:{x:-246.3,y:12.7}},{t:this.shape_229,p:{x:-233.2,y:14.2}},{t:this.shape_196,p:{x:-220.7,y:14.2}},{t:this.shape_157,p:{x:-209.2,y:14.2}},{t:this.shape_228,p:{x:-205,y:7.1}},{t:this.shape_192,p:{x:-191.1,y:14.2}},{t:this.shape_76,p:{x:-179.4,y:12.8}},{t:this.shape_453,p:{x:-170.6,y:14.2}},{t:this.shape_179,p:{x:-162.8,y:14.2}},{t:this.shape_101,p:{x:-150.7,y:14.2}},{t:this.shape_91,p:{x:-141.5,y:14.2}},{t:this.shape_223,p:{x:-141.2,y:7.1}},{t:this.shape_763,p:{x:-125.8,y:15.3}},{t:this.shape_340,p:{x:-113,y:12.8}},{t:this.shape_504,p:{x:-100.4,y:14.2}},{t:this.shape_211,p:{x:-96,y:7.1}},{t:this.shape_165,p:{x:-87.5,y:12.6}},{t:this.shape_258,p:{x:-79.2,y:14.2}},{t:this.shape_338,p:{x:-70.3,y:12.8}},{t:this.shape_284,p:{x:-57.3,y:14.2}},{t:this.shape_203,p:{x:-52.8,y:7.1}},{t:this.shape_96,p:{x:-43.4,y:14.2}},{t:this.shape_701,p:{x:-42.9,y:7.6}},{t:this.shape_596,p:{x:-24.4,y:14.4}},{t:this.shape_832},{t:this.shape_332,p:{x:5.2,y:12.8}},{t:this.shape_95,p:{x:13.1,y:14.2}},{t:this.shape_831},{t:this.shape_105,p:{x:34.7,y:14.2}},{t:this.shape_85,p:{x:47.3,y:12.8}},{t:this.shape_830},{t:this.shape_208,p:{x:80,y:14.2}},{t:this.shape_198,p:{x:85,y:7.1}},{t:this.shape_320,p:{x:100.1,y:12.8}},{t:this.shape_311,p:{x:100.1,y:19.3}},{t:this.shape_256,p:{x:113.2,y:14.2}},{t:this.shape_309,p:{x:113.8,y:19.3}},{t:this.shape_250,p:{x:-257.7,y:39.2}},{t:this.shape_174,p:{x:-248.3,y:39.2}},{t:this.shape_246,p:{x:-238.9,y:39.2}},{t:this.shape_193,p:{x:-234.4,y:32.1}},{t:this.shape_744,p:{x:-224.7,y:39.2}},{t:this.shape_154,p:{x:-215.1,y:39.2}},{t:this.shape_315,p:{x:-206.2,y:37.8}},{t:this.shape_163,p:{x:-192.9,y:39.2}},{t:this.shape_187,p:{x:-188.1,y:32.1}},{t:this.shape_301,p:{x:-173,y:37.8}},{t:this.shape_242,p:{x:-160,y:39.2}},{t:this.shape_694,p:{x:-140.2,y:40.3}},{t:this.shape_664,p:{x:-130.1,y:39.4}},{t:this.shape_100,p:{x:-120.2,y:39.2}},{t:this.shape_172,p:{x:-108.6,y:39.2}},{t:this.shape_273,p:{x:-96.7,y:39.2}},{t:this.shape_169,p:{x:-92.3,y:32.1}},{t:this.shape_79,p:{x:-77.6,y:37.7}},{t:this.shape_156,p:{x:-66.6,y:39.2}},{t:this.shape_114,p:{x:-54.7,y:39.2}},{t:this.shape_143,p:{x:-50.3,y:32.1}},{t:this.shape_829},{t:this.shape_541,p:{x:-27.7,y:39.2}},{t:this.shape_134,p:{x:-22.7,y:32.1}},{t:this.shape_285,p:{x:-14.1,y:37.8}},{t:this.shape_83,p:{x:-1.4,y:39.2}},{t:this.shape_263,p:{x:17.7,y:37.8}},{t:this.shape_224,p:{x:30.8,y:39.2}},{t:this.shape_129,p:{x:35.3,y:32.1}},{t:this.shape_147,p:{x:42.8,y:39.2}},{t:this.shape_210,p:{x:54.7,y:39.2}},{t:this.shape_126,p:{x:59.2,y:32.1}},{t:this.shape_486,p:{x:74.6,y:40.3}},{t:this.shape_512,p:{x:88.4,y:39.2}},{t:this.shape_113,p:{x:89.3,y:32.6}},{t:this.shape_483,p:{x:103.1,y:39.2}},{t:this.shape_828},{t:this.shape_180,p:{x:135.4,y:37.8}},{t:this.shape_26,p:{x:143.3,y:39.2}},{t:this.shape_827},{t:this.shape_75,p:{x:164.8,y:39.2}},{t:this.shape_826},{t:this.shape_825},{t:this.shape_736,p:{x:205.1,y:43.9}},{t:this.shape_824},{t:this.shape_823},{t:this.shape_259,p:{x:-233.6,y:62.8}},{t:this.shape_822},{t:this.shape_821},{t:this.shape_820},{t:this.shape_418,p:{x:-182.5,y:64.2}},{t:this.shape_819},{t:this.shape_703,p:{x:-169,y:65}},{t:this.shape_818},{t:this.shape_252,p:{x:-137.9,y:62.8}},{t:this.shape_414,p:{x:-124.3,y:64.2}},{t:this.shape_123,p:{x:-119.3,y:57.1}},{t:this.shape_125,p:{x:-105.4,y:64.2}},{t:this.shape_194,p:{x:-93.5,y:64.2}},{t:this.shape_30,p:{x:-84.4,y:64.2}},{t:this.shape_817},{t:this.shape_57,p:{x:-57.4,y:65.8}},{t:this.shape_39,p:{x:-45.8,y:64.4}},{t:this.shape_816},{t:this.shape_69,p:{x:-23.4,y:64.2}},{t:this.shape_10,p:{x:-10.7,y:62.8}},{t:this.shape_136,p:{x:8.6,y:64.2}},{t:this.shape_599,p:{x:21.6,y:62.7}},{t:this.shape_176,p:{x:34.9,y:64.2}},{t:this.shape_108,p:{x:39.4,y:57.1}},{t:this.shape_151,p:{x:55.3,y:64.2}},{t:this.shape_696,p:{x:68.5,y:65}},{t:this.shape_389,p:{x:87.4,y:65.3}},{t:this.shape_104,p:{x:99.1,y:64.2}},{t:this.shape_44,p:{x:110.6,y:64.2}},{t:this.shape_82,p:{x:124.2,y:64.2}},{t:this.shape_815},{t:this.shape_678,p:{x:143.7,y:62.7}},{t:this.shape_594,p:{x:155.9,y:62.7}},{t:this.shape_40,p:{x:168.8,y:64.2}},{t:this.shape_65,p:{x:168.8,y:57.6}},{t:this.shape_58,p:{x:178.5,y:68.1}},{t:this.shape_814,p:{x:-285.9,y:12.2}},{t:this.shape_374,p:{x:-258.2,y:124.3}},{t:this.shape_381,p:{x:-244.4,y:123.2}},{t:this.shape_111,p:{x:-243.5,y:116.6}},{t:this.shape_364,p:{x:-229.7,y:123.2}},{t:this.shape_185,p:{x:-209.6,y:123.2}},{t:this.shape_184,p:{x:-209.3,y:116.6}},{t:this.shape_813},{t:this.shape_12,p:{x:-186.7,y:121.7}},{t:this.shape_812},{t:this.shape_354,p:{x:-155,y:123.2}},{t:this.shape_811},{t:this.shape_633,p:{x:-141.5,y:124}},{t:this.shape_810},{t:this.shape_248,p:{x:-110.4,y:121.8}},{t:this.shape_133,p:{x:-96.8,y:123.2}},{t:this.shape_93,p:{x:-91.8,y:116.1}},{t:this.shape_102,p:{x:-77.9,y:123.2}},{t:this.shape_215,p:{x:-66.5,y:121.8}},{t:this.shape_809},{t:this.shape_155,p:{x:-41.4,y:123.2}},{t:this.shape_86,p:{x:-36.9,y:116.1}},{t:this.shape_808},{t:this.shape_620,p:{x:-8.8,y:123.2}},{t:this.shape_4,p:{x:-8.4,y:128.3}},{t:this.shape_287,p:{x:5.3,y:123.2}},{t:this.shape_641,p:{x:16.1,y:123.4}},{t:this.shape_97,p:{x:24.7,y:123.2}},{t:this.shape_186,p:{x:36.1,y:121.8}},{t:this.shape_807},{t:this.shape_144,p:{x:61.2,y:123.2}},{t:this.shape_81,p:{x:65.7,y:116.1}},{t:this.shape_806},{t:this.shape_148,p:{x:93.3,y:121.8}},{t:this.shape_188,p:{x:106.8,y:123.2}},{t:this.shape_67,p:{x:111.8,y:116.1}},{t:this.shape_140,p:{x:126.9,y:121.8}},{t:this.shape_642,p:{x:126.7,y:128.4}},{t:this.shape_99,p:{x:139.5,y:121.8}},{t:this.shape_170,p:{x:148,y:123.2}},{t:this.shape_120,p:{x:163.8,y:123.2}},{t:this.shape_805},{t:this.shape_313,p:{x:-258.2,y:149.3}},{t:this.shape_152,p:{x:-248.1,y:148.4}},{t:this.shape_38,p:{x:-238.2,y:148.2}},{t:this.shape_78,p:{x:-218.3,y:148.2}},{t:this.shape_72,p:{x:-217.6,y:141.6}},{t:this.shape_9,p:{x:-204.7,y:148.2}},{t:this.shape_444,p:{x:-204.4,y:141.6}},{t:this.shape_117,p:{x:-191.6,y:146.7}},{t:this.shape_18,p:{x:-173.2,y:148.4}},{t:this.shape_109,p:{x:-161.7,y:148.2}},{t:this.shape_68,p:{x:-147.8,y:148.2}},{t:this.shape_89,p:{x:-129.2,y:148.2}},{t:this.shape_103,p:{x:-117.8,y:146.7}},{t:this.shape_122,p:{x:-104,y:148.2}},{t:this.shape_166,p:{x:-91,y:146.8}},{t:this.shape_804},{t:this.shape_803},{t:this.shape_106,p:{x:-47.3,y:148.2}},{t:this.shape_62,p:{x:-34.2,y:146.8}},{t:this.shape_24,p:{x:-20.7,y:148.2}},{t:this.shape_55,p:{x:-15.7,y:141.1}},{t:this.shape_802},{t:this.shape_45,p:{x:4.3,y:148.2}},{t:this.shape_161,p:{x:15.7,y:149.6}},{t:this.shape_141,p:{x:24.3,y:148.2}},{t:this.shape_60,p:{x:33.2,y:146.8}},{t:this.shape_88,p:{x:45.7,y:146.7}},{t:this.shape_31,p:{x:65.6,y:148.2}},{t:this.shape_801},{t:this.shape_94,p:{x:91.4,y:148.2}},{t:this.shape_66,p:{x:105,y:148.2}},{t:this.shape_276,p:{x:-258.2,y:174.3}},{t:this.shape_221,p:{x:-249.4,y:173.2}},{t:this.shape_13,p:{x:-241.3,y:174.7}},{t:this.shape_84,p:{x:-230.9,y:171.8}},{t:this.shape_25,p:{x:-218.8,y:173.2}},{t:this.shape_51,p:{x:-198.9,y:173.2}},{t:this.shape_22,p:{x:-186.1,y:171.8}},{t:this.shape_56,p:{x:-173.7,y:173.2}},{t:this.shape_182,p:{x:-153.2,y:173.2}},{t:this.shape_800},{t:this.shape_43,p:{x:-132.1,y:173.2}},{t:this.shape_799,p:{x:-121.7,y:173.4}},{t:this.shape_135,p:{x:-111,y:173.2}},{t:this.shape_42,p:{x:-106,y:166.1}},{t:this.shape_7,p:{x:-96.3,y:173.2}},{t:this.shape_15,p:{x:-95.4,y:166.6}},{t:this.shape_17,p:{x:-82.4,y:173.2}},{t:this.shape_48,p:{x:-68.9,y:173.2}},{t:this.shape_5,p:{x:-49.1,y:171.8}},{t:this.shape_29,p:{x:-36.1,y:173.2}},{t:this.shape_28,p:{x:-31.6,y:166.1}},{t:this.shape_32,p:{x:-24,y:173.2}},{t:this.shape_16,p:{x:-12.1,y:173.2}},{t:this.shape_6,p:{x:-7.6,y:166.1}},{t:this.shape_3,p:{x:8,y:173.2}},{t:this.shape_405,p:{x:18.3,y:173.4}},{t:this.shape_107,p:{x:29.1,y:173.2}},{t:this.shape_1,p:{x:39.6,y:177.1}},{t:this.shape_798}]},1).to({state:[{t:this.shape_1630},{t:this.shape_1629},{t:this.shape_1628},{t:this.shape_1627},{t:this.shape_1626},{t:this.shape_1625},{t:this.shape_1624},{t:this.shape_1623},{t:this.shape_1622},{t:this.shape_1621},{t:this.shape_1620},{t:this.shape_1619},{t:this.shape_1618},{t:this.shape_1617},{t:this.shape_1616},{t:this.shape_1615},{t:this.shape_1614},{t:this.shape_1613},{t:this.shape_1612},{t:this.shape_1611},{t:this.shape_1610},{t:this.shape_1609},{t:this.shape_1608},{t:this.shape_1607},{t:this.shape_1606},{t:this.shape_1605},{t:this.shape_1604},{t:this.shape_1603},{t:this.shape_1602},{t:this.shape_1601},{t:this.shape_1600},{t:this.shape_1599},{t:this.shape_1598},{t:this.shape_1597},{t:this.shape_1596},{t:this.shape_1595},{t:this.shape_1594},{t:this.shape_1593},{t:this.shape_1592},{t:this.shape_1591},{t:this.shape_1590},{t:this.shape_1589},{t:this.shape_1588},{t:this.shape_1587},{t:this.shape_1586},{t:this.shape_1585},{t:this.shape_1584},{t:this.shape_1583},{t:this.shape_1582},{t:this.shape_1581},{t:this.shape_1580},{t:this.shape_1579},{t:this.shape_1578},{t:this.shape_1577},{t:this.shape_1576},{t:this.shape_1575},{t:this.shape_1574},{t:this.shape_1573},{t:this.shape_1572},{t:this.shape_1571},{t:this.shape_1570},{t:this.shape_1569},{t:this.shape_1568},{t:this.shape_1567},{t:this.shape_1566},{t:this.shape_1565},{t:this.shape_1564},{t:this.shape_1563},{t:this.shape_1562},{t:this.shape_1561},{t:this.shape_1560},{t:this.shape_1559},{t:this.shape_1558},{t:this.shape_1557},{t:this.shape_1556},{t:this.shape_1555},{t:this.shape_1554},{t:this.shape_1553},{t:this.shape_1552},{t:this.shape_1551},{t:this.shape_1550},{t:this.shape_1549},{t:this.shape_1548},{t:this.shape_1547},{t:this.shape_1546},{t:this.shape_1545},{t:this.shape_1544},{t:this.shape_1543},{t:this.shape_1542},{t:this.shape_1541},{t:this.shape_1540},{t:this.shape_1539},{t:this.shape_1538},{t:this.shape_1537},{t:this.shape_1536},{t:this.shape_1535},{t:this.shape_1534},{t:this.shape_1533},{t:this.shape_1532},{t:this.shape_1531},{t:this.shape_1530},{t:this.shape_1529},{t:this.shape_1528},{t:this.shape_1527},{t:this.shape_1526},{t:this.shape_1525},{t:this.shape_1524},{t:this.shape_1523},{t:this.shape_1522},{t:this.shape_1521},{t:this.shape_1520},{t:this.shape_1519},{t:this.shape_1518},{t:this.shape_1517},{t:this.shape_1516},{t:this.shape_1515},{t:this.shape_1514},{t:this.shape_1513},{t:this.shape_1512},{t:this.shape_1511},{t:this.shape_1510},{t:this.shape_1509},{t:this.shape_1508},{t:this.shape_1507},{t:this.shape_1506},{t:this.shape_1505},{t:this.shape_1504},{t:this.shape_1503},{t:this.shape_1502},{t:this.shape_1501},{t:this.shape_1500},{t:this.shape_1499},{t:this.shape_1498},{t:this.shape_1497},{t:this.shape_1496},{t:this.shape_1495},{t:this.shape_1494},{t:this.shape_1493},{t:this.shape_1492},{t:this.shape_1491},{t:this.shape_1490},{t:this.shape_1489},{t:this.shape_1488},{t:this.shape_1487},{t:this.shape_1486},{t:this.shape_1485},{t:this.shape_1484},{t:this.shape_1483},{t:this.shape_1482},{t:this.shape_1481},{t:this.shape_1480},{t:this.shape_1479},{t:this.shape_1478},{t:this.shape_1477},{t:this.shape_1476},{t:this.shape_1475},{t:this.shape_1474},{t:this.shape_1473},{t:this.shape_1472},{t:this.shape_1471},{t:this.shape_1470},{t:this.shape_1469},{t:this.shape_1468},{t:this.shape_1467},{t:this.shape_1466},{t:this.shape_1465},{t:this.shape_1464},{t:this.shape_1463},{t:this.shape_1462},{t:this.shape_1461},{t:this.shape_1460},{t:this.shape_1459},{t:this.shape_1458},{t:this.shape_1457},{t:this.shape_1456},{t:this.shape_1455},{t:this.shape_1454},{t:this.shape_1453},{t:this.shape_1452},{t:this.shape_1451},{t:this.shape_1450},{t:this.shape_1449},{t:this.shape_1448},{t:this.shape_1447},{t:this.shape_1446},{t:this.shape_1445},{t:this.shape_1444},{t:this.shape_1443},{t:this.shape_1442},{t:this.shape_1441},{t:this.shape_1440},{t:this.shape_1439},{t:this.shape_1438},{t:this.shape_1437},{t:this.shape_1436},{t:this.shape_1435},{t:this.shape_1434},{t:this.shape_1433},{t:this.shape_1432},{t:this.shape_1431},{t:this.shape_1430},{t:this.shape_1429},{t:this.shape_1428},{t:this.shape_1427},{t:this.shape_1426},{t:this.shape_1425},{t:this.shape_1424},{t:this.shape_1423},{t:this.shape_1422},{t:this.shape_1421},{t:this.shape_1420},{t:this.shape_1419},{t:this.shape_1418},{t:this.shape_1417},{t:this.shape_1416},{t:this.shape_1415},{t:this.shape_1414},{t:this.shape_1413},{t:this.shape_1412},{t:this.shape_1411},{t:this.shape_1410},{t:this.shape_1409},{t:this.shape_1408},{t:this.shape_1407},{t:this.shape_1406},{t:this.shape_1405},{t:this.shape_1404},{t:this.shape_1403},{t:this.shape_335,p:{y:-141.8,x:-305.9}},{t:this.shape_1402},{t:this.shape_1401},{t:this.shape_1400},{t:this.shape_1399},{t:this.shape_1398},{t:this.shape_1397},{t:this.shape_1396},{t:this.shape_1395},{t:this.shape_1394},{t:this.shape_1393},{t:this.shape_1392},{t:this.shape_1391},{t:this.shape_1390},{t:this.shape_1389},{t:this.shape_1388},{t:this.shape_1387},{t:this.shape_1386},{t:this.shape_1385},{t:this.shape_1384},{t:this.shape_1383},{t:this.shape_1382},{t:this.shape_1381},{t:this.shape_1380},{t:this.shape_1379},{t:this.shape_1378},{t:this.shape_1377},{t:this.shape_1376},{t:this.shape_1375},{t:this.shape_1374},{t:this.shape_1373},{t:this.shape_1372},{t:this.shape_1371},{t:this.shape_1370},{t:this.shape_1369},{t:this.shape_1368},{t:this.shape_1367},{t:this.shape_1366},{t:this.shape_1365},{t:this.shape_1364},{t:this.shape_1363},{t:this.shape_1362},{t:this.shape_1361},{t:this.shape_1360},{t:this.shape_1359},{t:this.shape_1358},{t:this.shape_1357},{t:this.shape_1356},{t:this.shape_1355},{t:this.shape_1354},{t:this.shape_1353},{t:this.shape_1352},{t:this.shape_1351},{t:this.shape_1350},{t:this.shape_1349},{t:this.shape_1348},{t:this.shape_1347},{t:this.shape_1346},{t:this.shape_1345},{t:this.shape_1344},{t:this.shape_1343},{t:this.shape_1342},{t:this.shape_1341},{t:this.shape_1340},{t:this.shape_1339},{t:this.shape_1338},{t:this.shape_1337},{t:this.shape_1336},{t:this.shape_1335},{t:this.shape_1334},{t:this.shape_1333},{t:this.shape_1332},{t:this.shape_1331},{t:this.shape_1330},{t:this.shape_1329},{t:this.shape_1328},{t:this.shape_1327},{t:this.shape_1326},{t:this.shape_1325},{t:this.shape_1324},{t:this.shape_1323},{t:this.shape_1322},{t:this.shape_1321},{t:this.shape_1320},{t:this.shape_1319},{t:this.shape_1318},{t:this.shape_1317},{t:this.shape_1316},{t:this.shape_1315},{t:this.shape_1314},{t:this.shape_1313},{t:this.shape_1312},{t:this.shape_1311},{t:this.shape_1310},{t:this.shape_1309},{t:this.shape_1308},{t:this.shape_1307},{t:this.shape_1306},{t:this.shape_1305},{t:this.shape_1304},{t:this.shape_1303},{t:this.shape_1302},{t:this.shape_1301},{t:this.shape_1300},{t:this.shape_1299},{t:this.shape_1298},{t:this.shape_1297},{t:this.shape_1296},{t:this.shape_1295},{t:this.shape_1294},{t:this.shape_1293},{t:this.shape_1292},{t:this.shape_1291},{t:this.shape_1290},{t:this.shape_1289},{t:this.shape_1288},{t:this.shape_1287},{t:this.shape_1286},{t:this.shape_1285},{t:this.shape_1284},{t:this.shape_1283},{t:this.shape_1282},{t:this.shape_1281},{t:this.shape_1280},{t:this.shape_1279},{t:this.shape_1278},{t:this.shape_1277},{t:this.shape_1276},{t:this.shape_1275},{t:this.shape_1274},{t:this.shape_1273},{t:this.shape_1272},{t:this.shape_1271},{t:this.shape_1270},{t:this.shape_1269},{t:this.shape_1268},{t:this.shape_1267},{t:this.shape_1266},{t:this.shape_1265},{t:this.shape_1264},{t:this.shape_1263},{t:this.shape_1262},{t:this.shape_1261},{t:this.shape_1260},{t:this.shape_1259},{t:this.shape_1258},{t:this.shape_1257},{t:this.shape_1256},{t:this.shape_1255},{t:this.shape_1254},{t:this.shape_1253},{t:this.shape_1252},{t:this.shape_1251},{t:this.shape_1250},{t:this.shape_1249},{t:this.shape_1248},{t:this.shape_1247},{t:this.shape_1246},{t:this.shape_1245},{t:this.shape_1244},{t:this.shape_1243},{t:this.shape_1242},{t:this.shape_1241},{t:this.shape_1240},{t:this.shape_1239},{t:this.shape_1238},{t:this.shape_1237},{t:this.shape_1236},{t:this.shape_1235},{t:this.shape_1234},{t:this.shape_1233},{t:this.shape_1232},{t:this.shape_1231},{t:this.shape_1230},{t:this.shape_1229},{t:this.shape_1228},{t:this.shape_1227},{t:this.shape_1226},{t:this.shape_1225},{t:this.shape_1224},{t:this.shape_1223},{t:this.shape_1222},{t:this.shape_1221},{t:this.shape_1220},{t:this.shape_1219},{t:this.shape_1218},{t:this.shape_1217},{t:this.shape_1216},{t:this.shape_1215},{t:this.shape_1214},{t:this.shape_1213},{t:this.shape_1212},{t:this.shape_1211},{t:this.shape_1210},{t:this.shape_1209},{t:this.shape_1208},{t:this.shape_1207},{t:this.shape_1206},{t:this.shape_1205},{t:this.shape_1204},{t:this.shape_1203},{t:this.shape_1202},{t:this.shape_1201},{t:this.shape_1200},{t:this.shape_1199},{t:this.shape_1198},{t:this.shape_6,p:{x:87.4,y:67.5}},{t:this.shape_1197},{t:this.shape_1196},{t:this.shape_1195},{t:this.shape_1194},{t:this.shape_1193},{t:this.shape_1192},{t:this.shape_1191},{t:this.shape_1190},{t:this.shape_1189},{t:this.shape_1188},{t:this.shape_1187},{t:this.shape_814,p:{x:-305.9,y:24.2}},{t:this.shape_1186},{t:this.shape_1185},{t:this.shape_1184},{t:this.shape_1183},{t:this.shape_1182},{t:this.shape_1181},{t:this.shape_1180},{t:this.shape_1179},{t:this.shape_1178},{t:this.shape_1177},{t:this.shape_1176},{t:this.shape_1175},{t:this.shape_1174},{t:this.shape_1173},{t:this.shape_1172},{t:this.shape_1171},{t:this.shape_1170},{t:this.shape_1169},{t:this.shape_1168},{t:this.shape_1167},{t:this.shape_1166},{t:this.shape_1165},{t:this.shape_1164},{t:this.shape_1163},{t:this.shape_1162},{t:this.shape_1161},{t:this.shape_1160},{t:this.shape_1159},{t:this.shape_1158},{t:this.shape_1157},{t:this.shape_1156},{t:this.shape_1155},{t:this.shape_1154},{t:this.shape_1153},{t:this.shape_1152},{t:this.shape_1151},{t:this.shape_1150},{t:this.shape_1149},{t:this.shape_1148},{t:this.shape_1147},{t:this.shape_1146},{t:this.shape_1145},{t:this.shape_1144},{t:this.shape_1143},{t:this.shape_1142},{t:this.shape_1141},{t:this.shape_1140},{t:this.shape_1139},{t:this.shape_1138},{t:this.shape_1137},{t:this.shape_1136},{t:this.shape_1135},{t:this.shape_1134},{t:this.shape_1133},{t:this.shape_1132},{t:this.shape_1131},{t:this.shape_1130},{t:this.shape_1129},{t:this.shape_1128},{t:this.shape_1127},{t:this.shape_1126},{t:this.shape_1125},{t:this.shape_1124},{t:this.shape_1123},{t:this.shape_1122},{t:this.shape_1121},{t:this.shape_1120},{t:this.shape_1119},{t:this.shape_1118},{t:this.shape_1117},{t:this.shape_1116},{t:this.shape_1115},{t:this.shape_1114},{t:this.shape_1113},{t:this.shape_1112},{t:this.shape_1111},{t:this.shape_1110},{t:this.shape_1109},{t:this.shape_1108},{t:this.shape_1107},{t:this.shape_1106},{t:this.shape_1105},{t:this.shape_1104},{t:this.shape_1103},{t:this.shape_1102},{t:this.shape_1101},{t:this.shape_1100},{t:this.shape_1099},{t:this.shape_1098},{t:this.shape_1097},{t:this.shape_1096},{t:this.shape_1095},{t:this.shape_1094},{t:this.shape_1093},{t:this.shape_1092},{t:this.shape_1091},{t:this.shape_1090},{t:this.shape_1089},{t:this.shape_1088},{t:this.shape_1087},{t:this.shape_1086},{t:this.shape_1085},{t:this.shape_1084},{t:this.shape_1083},{t:this.shape_1082},{t:this.shape_1081},{t:this.shape_1080},{t:this.shape_1079},{t:this.shape_1078},{t:this.shape_1077},{t:this.shape_1076},{t:this.shape_1075},{t:this.shape_1074},{t:this.shape_1073},{t:this.shape_1072},{t:this.shape_1071},{t:this.shape_1070},{t:this.shape_1069},{t:this.shape_1068},{t:this.shape_1067},{t:this.shape_1066},{t:this.shape_1065},{t:this.shape_1064},{t:this.shape_1063},{t:this.shape_1062},{t:this.shape_1061},{t:this.shape_1060},{t:this.shape_1059},{t:this.shape_1058},{t:this.shape_1057},{t:this.shape_1056},{t:this.shape_1055},{t:this.shape_1054},{t:this.shape_1053},{t:this.shape_1052},{t:this.shape_1051},{t:this.shape_1050},{t:this.shape_1049},{t:this.shape_1048},{t:this.shape_1047},{t:this.shape_1046},{t:this.shape_1045},{t:this.shape_1044},{t:this.shape_1043},{t:this.shape_1042},{t:this.shape_1041},{t:this.shape_1040},{t:this.shape_1039},{t:this.shape_1038},{t:this.shape_1037},{t:this.shape_1036},{t:this.shape_1035},{t:this.shape_1034},{t:this.shape_1033},{t:this.shape_1032},{t:this.shape_1031},{t:this.shape_1030},{t:this.shape_1029},{t:this.shape_1028},{t:this.shape_1027},{t:this.shape_1026},{t:this.shape_1025},{t:this.shape_1024},{t:this.shape_1023},{t:this.shape_1022},{t:this.shape_1021},{t:this.shape_1020},{t:this.shape_1019},{t:this.shape_1018},{t:this.shape_1017},{t:this.shape_1016},{t:this.shape_1015},{t:this.shape_1014},{t:this.shape_1013},{t:this.shape_1012},{t:this.shape_1011},{t:this.shape_1010},{t:this.shape_1009},{t:this.shape_1008},{t:this.shape_1007},{t:this.shape_1006},{t:this.shape_1005},{t:this.shape_1004},{t:this.shape_1003},{t:this.shape_1002},{t:this.shape_1001},{t:this.shape_1000},{t:this.shape_999},{t:this.shape_998},{t:this.shape_997},{t:this.shape_996},{t:this.shape_995},{t:this.shape_994},{t:this.shape_993},{t:this.shape_992},{t:this.shape_991},{t:this.shape_990},{t:this.shape_989},{t:this.shape_988},{t:this.shape_987},{t:this.shape_986},{t:this.shape_985},{t:this.shape_984},{t:this.shape_983},{t:this.shape_982},{t:this.shape_981},{t:this.shape_980},{t:this.shape_979},{t:this.shape_978},{t:this.shape_977},{t:this.shape_976},{t:this.shape_975},{t:this.shape_974},{t:this.shape_973},{t:this.shape_972},{t:this.shape_971},{t:this.shape_970},{t:this.shape_969},{t:this.shape_968},{t:this.shape_967},{t:this.shape_966},{t:this.shape_965},{t:this.shape_964},{t:this.shape_963},{t:this.shape_962},{t:this.shape_961},{t:this.shape_960},{t:this.shape_959},{t:this.shape_958},{t:this.shape_957},{t:this.shape_956},{t:this.shape_955},{t:this.shape_954},{t:this.shape_953},{t:this.shape_952},{t:this.shape_951},{t:this.shape_950},{t:this.shape_949},{t:this.shape_948},{t:this.shape_947},{t:this.shape_946},{t:this.shape_945},{t:this.shape_944},{t:this.shape_943},{t:this.shape_942},{t:this.shape_941},{t:this.shape_940},{t:this.shape_939},{t:this.shape_938},{t:this.shape_937},{t:this.shape_936},{t:this.shape_935},{t:this.shape_934},{t:this.shape_933},{t:this.shape_932},{t:this.shape_931},{t:this.shape_930},{t:this.shape_929},{t:this.shape_928},{t:this.shape_927},{t:this.shape_926},{t:this.shape_925},{t:this.shape_924},{t:this.shape_923},{t:this.shape_922},{t:this.shape_921},{t:this.shape_920},{t:this.shape_919},{t:this.shape_918},{t:this.shape_917},{t:this.shape_916},{t:this.shape_915},{t:this.shape_914},{t:this.shape_913},{t:this.shape_912},{t:this.shape_911},{t:this.shape_910},{t:this.shape_909},{t:this.shape_908},{t:this.shape_907},{t:this.shape_906},{t:this.shape_905},{t:this.shape_904},{t:this.shape_903},{t:this.shape_902},{t:this.shape_901},{t:this.shape_900},{t:this.shape_899},{t:this.shape_898},{t:this.shape_897},{t:this.shape_896},{t:this.shape_895},{t:this.shape_894},{t:this.shape_893},{t:this.shape_892},{t:this.shape_891},{t:this.shape_890},{t:this.shape_889},{t:this.shape_888},{t:this.shape_887},{t:this.shape_886},{t:this.shape_885},{t:this.shape_884},{t:this.shape_883},{t:this.shape_882},{t:this.shape_881},{t:this.shape_880},{t:this.shape_879},{t:this.shape_878},{t:this.shape_877},{t:this.shape_876},{t:this.shape_875},{t:this.shape_874},{t:this.shape_873},{t:this.shape_872},{t:this.shape_871},{t:this.shape_870},{t:this.shape_869},{t:this.shape_868},{t:this.shape_867},{t:this.shape_866}]},1).to({state:[{t:this.shape_1817},{t:this.shape_1816},{t:this.shape_1815},{t:this.shape_1814},{t:this.shape_1813},{t:this.shape_1812},{t:this.shape_783,p:{x:-38.7,y:-258.9}},{t:this.shape_792,p:{x:-21,y:-259.1}},{t:this.shape_1811},{t:this.shape_1810},{t:this.shape_1809},{t:this.shape_1808},{t:this.shape_1807},{t:this.shape_1806},{t:this.shape_1805},{t:this.shape_1804},{t:this.shape_589,p:{x:105.2,y:-256.3}},{t:this.shape_1803},{t:this.shape_579,p:{x:125.1,y:-256.2}},{t:this.shape_578,p:{x:131.9,y:-266.9}},{t:this.shape_575,p:{x:150,y:-257.5}},{t:this.shape_1802},{t:this.shape_1801},{t:this.shape_1800},{t:this.shape_769,p:{x:215.1,y:-256.4}},{t:this.shape_568,p:{x:229,y:-256.5}},{t:this.shape_1799},{t:this.shape_1798},{t:this.shape_1797},{t:this.shape_1796},{t:this.shape_1795},{t:this.shape_1794},{t:this.shape_765,p:{x:326,y:-256.4}},{t:this.shape_569,p:{x:337.8,y:-259.4}},{t:this.shape_564,p:{x:349.9,y:-256.5}},{t:this.shape_1793},{t:this.shape_1792},{t:this.shape_1791},{t:this.shape_560,p:{x:-224.2,y:-206.2}},{t:this.shape_177,p:{x:-205.1,y:-206.9}},{t:this.shape_859,p:{x:-193.3,y:-207.1}},{t:this.shape_181,p:{x:-181.3,y:-206.8}},{t:this.shape_1790},{t:this.shape_1789},{t:this.shape_502,p:{x:-156,y:-205.3}},{t:this.shape_535,p:{x:-144.5,y:-205.3}},{t:this.shape_1788},{t:this.shape_456,p:{x:-125.3,y:-206.7}},{t:this.shape_476,p:{x:-113.9,y:-205.3}},{t:this.shape_514,p:{x:-102.6,y:-206.7}},{t:this.shape_1787},{t:this.shape_551,p:{x:-85.1,y:-212.4}},{t:this.shape_1786},{t:this.shape_529,p:{x:-71.5,y:-212.4}},{t:this.shape_1785},{t:this.shape_1784},{t:this.shape_1783},{t:this.shape_524,p:{x:-33.8,y:-212.4}},{t:this.shape_408,p:{x:-18,y:-205.3}},{t:this.shape_1782},{t:this.shape_1781},{t:this.shape_1780},{t:this.shape_744,p:{x:28.6,y:-205.3}},{t:this.shape_1779},{t:this.shape_1778},{t:this.shape_465,p:{x:76.7,y:-205.3}},{t:this.shape_171,p:{x:88.1,y:-206.8}},{t:this.shape_489,p:{x:96.8,y:-205.3}},{t:this.shape_357,p:{x:106.4,y:-205.3}},{t:this.shape_218,p:{x:107.1,y:-200.2}},{t:this.shape_480,p:{x:127,y:-205.3}},{t:this.shape_1777},{t:this.shape_294,p:{x:140.5,y:-206.6}},{t:this.shape_1776},{t:this.shape_431,p:{x:165.3,y:-205.3}},{t:this.shape_1775},{t:this.shape_1774},{t:this.shape_1773},{t:this.shape_1772},{t:this.shape_518,p:{x:-243.9,y:-187.4}},{t:this.shape_1771},{t:this.shape_545,p:{x:-215.8,y:-181.8}},{t:this.shape_498,p:{x:-203.1,y:-181.7}},{t:this.shape_1770},{t:this.shape_1769},{t:this.shape_499,p:{x:-173.6,y:-187.4}},{t:this.shape_843,p:{x:-158.2,y:-179.2}},{t:this.shape_142,p:{x:-145.4,y:-181.6}},{t:this.shape_269,p:{x:-145.2,y:-175.2}},{t:this.shape_1768},{t:this.shape_510,p:{x:-116.5,y:-181.7}},{t:this.shape_152,p:{x:-107.4,y:-180.1}},{t:this.shape_448,p:{x:-98.8,y:-180.3}},{t:this.shape_398,p:{x:-87.4,y:-181.7}},{t:this.shape_427,p:{x:-74.1,y:-180.3}},{t:this.shape_1767},{t:this.shape_442,p:{x:-55.3,y:-180.3}},{t:this.shape_1766},{t:this.shape_270,p:{x:-31.2,y:-180.3}},{t:this.shape_1765},{t:this.shape_298,p:{x:1.7,y:-180.3}},{t:this.shape_541,p:{x:16,y:-180.3}},{t:this.shape_423,p:{x:29.5,y:-181.7}},{t:this.shape_1764},{t:this.shape_422,p:{x:58.5,y:-181.8}},{t:this.shape_1763},{t:this.shape_262,p:{x:91.3,y:-178.9}},{t:this.shape_1762},{t:this.shape_1761},{t:this.shape_265,p:{x:120.8,y:-175.2}},{t:this.shape_1760},{t:this.shape_491,p:{x:135,y:-187.4}},{t:this.shape_1759},{t:this.shape_161,p:{x:157.2,y:-178.9}},{t:this.shape_438,p:{x:168.7,y:-180.3}},{t:this.shape_513,p:{x:180.2,y:-180.3}},{t:this.shape_411,p:{x:193.8,y:-180.3}},{t:this.shape_1758},{t:this.shape_336,p:{x:204.2,y:-176.4}},{t:this.shape_635,p:{x:-286.2,y:-207.3}},{t:this.shape_412,p:{x:-259.9,y:-137.3}},{t:this.shape_280,p:{x:-248.5,y:-138.8}},{t:this.shape_500,p:{x:-239.7,y:-137.3}},{t:this.shape_289,p:{x:-230.1,y:-137.3}},{t:this.shape_668,p:{x:-229.4,y:-132.2}},{t:this.shape_367,p:{x:-216.9,y:-138.7}},{t:this.shape_71,p:{x:-197.2,y:-137.3}},{t:this.shape_1757},{t:this.shape_92,p:{x:-183.7,y:-138.6}},{t:this.shape_470,p:{x:-175,y:-137.3}},{t:this.shape_397,p:{x:-158.9,y:-137.3}},{t:this.shape_77,p:{x:-145.5,y:-135.9}},{t:this.shape_34,p:{x:-125.7,y:-137.3}},{t:this.shape_512,p:{x:-111.3,y:-137.3}},{t:this.shape_485,p:{x:-101.7,y:-137.3}},{t:this.shape_386,p:{x:-85.6,y:-137.3}},{t:this.shape_1756},{t:this.shape_1755},{t:this.shape_254,p:{x:-60.8,y:-138.8}},{t:this.shape_1754},{t:this.shape_1753},{t:this.shape_688,p:{x:-16.2,y:-138.7}},{t:this.shape_1752},{t:this.shape_479,p:{x:1.7,y:-144.4}},{t:this.shape_472,p:{x:1.8,y:-144.4}},{t:this.shape_1751},{t:this.shape_391,p:{x:23.3,y:-138.7}},{t:this.shape_1750},{t:this.shape_799,p:{x:53,y:-137.1}},{t:this.shape_261,p:{x:62.6,y:-138.7}},{t:this.shape_1749},{t:this.shape_384,p:{x:87.4,y:-138.7}},{t:this.shape_368,p:{x:100.8,y:-137.3}},{t:this.shape_439,p:{x:105.6,y:-144.4}},{t:this.shape_1748},{t:this.shape_405,p:{x:130.8,y:-137.1}},{t:this.shape_382,p:{x:140.5,y:-138.7}},{t:this.shape_229,p:{x:154,y:-137.3}},{t:this.shape_493,p:{x:154.9,y:-143.9}},{t:this.shape_484,p:{x:167.9,y:-137.3}},{t:this.shape_1747},{t:this.shape_1746},{t:this.shape_226,p:{x:-244.9,y:-112.3}},{t:this.shape_52,p:{x:-244.2,y:-107.2}},{t:this.shape_410,p:{x:-232.7,y:-112.3}},{t:this.shape_1745},{t:this.shape_434,p:{x:-216.4,y:-119.4}},{t:this.shape_1744},{t:this.shape_483,p:{x:-187.2,y:-112.3}},{t:this.shape_419,p:{x:-182.2,y:-119.4}},{t:this.shape_219,p:{x:-173.4,y:-112.3}},{t:this.shape_191,p:{x:-159.7,y:-112.3}},{t:this.shape_388,p:{x:-147.6,y:-112.3}},{t:this.shape_481,p:{x:-136.1,y:-112.3}},{t:this.shape_1743},{t:this.shape_400,p:{x:-118.3,y:-119.4}},{t:this.shape_49,p:{x:-109.6,y:-113.8}},{t:this.shape_1742},{t:this.shape_153,p:{x:-76.6,y:-112.3}},{t:this.shape_37,p:{x:-75.9,y:-107.2}},{t:this.shape_41,p:{x:-57,y:-113.8}},{t:this.shape_46,p:{x:-44.5,y:-113.8}},{t:this.shape_1741},{t:this.shape_394,p:{x:-14.6,y:-112.1}},{t:this.shape_231,p:{x:-5,y:-113.7}},{t:this.shape_1740},{t:this.shape_362,p:{x:19.8,y:-113.7}},{t:this.shape_1739},{t:this.shape_451,p:{x:39.6,y:-118.9}},{t:this.shape_496,p:{x:51.9,y:-113.7}},{t:this.shape_245,p:{x:59.8,y:-112.3}},{t:this.shape_342,p:{x:69.5,y:-112.3}},{t:this.shape_455,p:{x:82.3,y:-113.7}},{t:this.shape_686,p:{x:96.3,y:-113.6}},{t:this.shape_454,p:{x:111.7,y:-112.3}},{t:this.shape_330,p:{x:131.7,y:-112.3}},{t:this.shape_441,p:{x:144.5,y:-113.7}},{t:this.shape_1738},{t:this.shape_1737},{t:this.shape_396,p:{x:174.9,y:-119.4}},{t:this.shape_554,p:{x:184.1,y:-112.3}},{t:this.shape_440,p:{x:193.2,y:-112.3}},{t:this.shape_70,p:{x:208.5,y:-110.9}},{t:this.shape_18,p:{x:220.3,y:-112.1}},{t:this.shape_244,p:{x:228.2,y:-108.4}},{t:this.shape_449,p:{y:-139.3,x:-286.2}},{t:this.shape_344,p:{x:-259.9,y:-66.3}},{t:this.shape_326,p:{x:-249.6,y:-66.3}},{t:this.shape_73,p:{x:-238.1,y:-66.3}},{t:this.shape_1736},{t:this.shape_392,p:{x:-223.5,y:-73.4}},{t:this.shape_1735},{t:this.shape_428,p:{x:-206,y:-66.3}},{t:this.shape_525,p:{x:-196.6,y:-66.3}},{t:this.shape_385,p:{x:-192.1,y:-73.4}},{t:this.shape_418,p:{x:-182.4,y:-66.3}},{t:this.shape_1734},{t:this.shape_316,p:{x:-167.9,y:-66.3}},{t:this.shape_299,p:{x:-149.2,y:-66.3}},{t:this.shape_1733},{t:this.shape_1732},{t:this.shape_208,p:{x:-115.6,y:-66.3}},{t:this.shape_327,p:{x:-114.8,y:-72.9}},{t:this.shape_303,p:{x:-101.1,y:-66.3}},{t:this.shape_1731},{t:this.shape_539,p:{x:-70.3,y:-64.8}},{t:this.shape_1730},{t:this.shape_436,p:{x:-52.5,y:-66.3}},{t:this.shape_350,p:{x:-43.6,y:-67.7}},{t:this.shape_620,p:{x:-24.6,y:-66.3}},{t:this.shape_1729},{t:this.shape_146,p:{x:1.2,y:-66.3}},{t:this.shape_371,p:{x:10.5,y:-66.3}},{t:this.shape_340,p:{x:25.7,y:-67.7}},{t:this.shape_601,p:{x:25.7,y:-61.2}},{t:this.shape_290,p:{x:43.6,y:-66.3}},{t:this.shape_1728},{t:this.shape_380,p:{x:59.5,y:-73.4}},{t:this.shape_338,p:{x:68.2,y:-67.7}},{t:this.shape_452,p:{x:80.9,y:-66.3}},{t:this.shape_138,p:{x:100.7,y:-66.3}},{t:this.shape_225,p:{x:110,y:-66.3}},{t:this.shape_1727},{t:this.shape_274,p:{x:135.9,y:-66.3}},{t:this.shape_1726},{t:this.shape_375,p:{x:152.2,y:-73.4}},{t:this.shape_1725},{t:this.shape_257,p:{x:172.6,y:-66.3}},{t:this.shape_445,p:{x:184.2,y:-66.3}},{t:this.shape_494,p:{x:197.4,y:-66.3}},{t:this.shape_341,p:{x:201.9,y:-73.4}},{t:this.shape_594,p:{x:210.6,y:-67.8}},{t:this.shape_1724},{t:this.shape_1723},{t:this.shape_1722},{t:this.shape_50,p:{x:-250.4,y:-35.4}},{t:this.shape_202,p:{x:-236.8,y:-41.3}},{t:this.shape_474,p:{x:-227.4,y:-41.3}},{t:this.shape_1721},{t:this.shape_1720},{t:this.shape_506,p:{x:-187.6,y:-39.8}},{t:this.shape_345,p:{x:-177.4,y:-41.3}},{t:this.shape_200,p:{x:-178.3,y:-36.2}},{t:this.shape_133,p:{x:-164.6,y:-41.3}},{t:this.shape_329,p:{x:-159.6,y:-48.4}},{t:this.shape_836,p:{x:-144.3,y:-40.2}},{t:this.shape_353,p:{x:-135.4,y:-41.3}},{t:this.shape_1719},{t:this.shape_409,p:{x:-114.3,y:-42.7}},{t:this.shape_115,p:{x:-100.3,y:-42.6}},{t:this.shape_487,p:{x:-78.1,y:-41.3}},{t:this.shape_1718},{t:this.shape_421,p:{x:-64.8,y:-41.3}},{t:this.shape_11,p:{x:-52,y:-42.8}},{t:this.shape_1717},{t:this.shape_320,p:{x:-24.7,y:-42.7}},{t:this.shape_205,p:{x:-12.2,y:-39.9}},{t:this.shape_232,p:{x:5.7,y:-41.3}},{t:this.shape_1716},{t:this.shape_145,p:{x:26.2,y:-41.3}},{t:this.shape_387,p:{x:35.2,y:-41.3}},{t:this.shape_318,p:{x:44,y:-41.3}},{t:this.shape_1715},{t:this.shape_209,p:{x:70,y:-41.1}},{t:this.shape_482,p:{x:80.3,y:-41.3}},{t:this.shape_1714},{t:this.shape_1713},{t:this.shape_477,p:{x:112.5,y:-41.3}},{t:this.shape_849,p:{x:112.8,y:-47.9}},{t:this.shape_1712},{t:this.shape_1711},{t:this.shape_1710},{t:this.shape_1709},{t:this.shape_1708},{t:this.shape_1707},{t:this.shape_1706},{t:this.shape_227,p:{x:-259.9,y:-16.3}},{t:this.shape_1705},{t:this.shape_1704},{t:this.shape_414,p:{x:-226.3,y:-16.3}},{t:this.shape_283,p:{x:-225.4,y:-22.9}},{t:this.shape_250,p:{x:-211.8,y:-16.3}},{t:this.shape_1703},{t:this.shape_488,p:{x:-181,y:-14.8}},{t:this.shape_1702},{t:this.shape_759,p:{x:-163.1,y:-16.3}},{t:this.shape_315,p:{x:-154.3,y:-17.7}},{t:this.shape_1701},{t:this.shape_1700},{t:this.shape_101,p:{x:-109.4,y:-16.3}},{t:this.shape_137,p:{x:-100.2,y:-16.3}},{t:this.shape_301,p:{x:-84.9,y:-17.7}},{t:this.shape_4,p:{x:-84.9,y:-11.2}},{t:this.shape_213,p:{x:-67,y:-16.3}},{t:this.shape_473,p:{x:-55.1,y:-16.3}},{t:this.shape_255,p:{x:-46,y:-16.3}},{t:this.shape_206,p:{x:-38.3,y:-16.3}},{t:this.shape_217,p:{x:-26.6,y:-17.7}},{t:this.shape_1699},{t:this.shape_323,p:{x:-9.8,y:-23.4}},{t:this.shape_122,p:{x:6.3,y:-16.3}},{t:this.shape_1698},{t:this.shape_734,p:{x:26.8,y:-16.3}},{t:this.shape_1697},{t:this.shape_196,p:{x:48,y:-16.3}},{t:this.shape_366,p:{x:59.6,y:-16.3}},{t:this.shape_463,p:{x:72.8,y:-16.3}},{t:this.shape_321,p:{x:77.3,y:-23.4}},{t:this.shape_1696},{t:this.shape_534,p:{x:92.5,y:-22.9}},{t:this.shape_1695},{t:this.shape_1694},{t:this.shape_640,p:{x:134.1,y:-14.9}},{t:this.shape_641,p:{x:143.9,y:-16.1}},{t:this.shape_1693},{t:this.shape_343,p:{x:166.6,y:-16.3}},{t:this.shape_207,p:{x:176.3,y:-12.4}},{t:this.shape_335,p:{y:-68.3,x:-286.2}},{t:this.shape_192,p:{x:-259.9,y:26.7}},{t:this.shape_1692},{t:this.shape_1691},{t:this.shape_381,p:{x:-226.3,y:26.7}},{t:this.shape_74,p:{x:-225.4,y:20.1}},{t:this.shape_163,p:{x:-211.8,y:26.7}},{t:this.shape_317,p:{x:-192.3,y:25.2}},{t:this.shape_406,p:{x:-181,y:28.2}},{t:this.shape_1690},{t:this.shape_712,p:{x:-163.1,y:26.7}},{t:this.shape_285,p:{x:-154.3,y:25.3}},{t:this.shape_1689},{t:this.shape_1688},{t:this.shape_189,p:{x:-109.9,y:25.3}},{t:this.shape_435,p:{x:-96.5,y:26.7}},{t:this.shape_307,p:{x:-92,y:19.6}},{t:this.shape_1687},{t:this.shape_1686},{t:this.shape_1685},{t:this.shape_1684},{t:this.shape_1683},{t:this.shape_295,p:{x:-35.3,y:19.6}},{t:this.shape_1682},{t:this.shape_763,p:{x:-11.8,y:27.8}},{t:this.shape_325,p:{x:1.2,y:26.7}},{t:this.shape_1681},{t:this.shape_420,p:{x:14.6,y:26.7}},{t:this.shape_291,p:{x:19.1,y:19.6}},{t:this.shape_80,p:{x:27.9,y:26.7}},{t:this.shape_288,p:{x:32.3,y:19.6}},{t:this.shape_216,p:{x:40.9,y:25.3}},{t:this.shape_304,p:{x:53.5,y:26.7}},{t:this.shape_650,p:{x:53.5,y:20.1}},{t:this.shape_402,p:{x:66.8,y:26.7}},{t:this.shape_272,p:{x:71.3,y:19.6}},{t:this.shape_1680},{t:this.shape_1679},{t:this.shape_667,p:{x:-258.4,y:56.8}},{t:this.shape_557,p:{x:-245.7,y:50.1}},{t:this.shape_151,p:{x:-232.5,y:51.7}},{t:this.shape_364,p:{x:-211.7,y:51.7}},{t:this.shape_24,p:{x:-197.1,y:51.7}},{t:this.shape_249,p:{x:-192.1,y:44.6}},{t:this.shape_263,p:{x:-183.4,y:50.3}},{t:this.shape_179,p:{x:-172,y:51.7}},{t:this.shape_275,p:{x:-160.5,y:51.7}},{t:this.shape_238,p:{x:-156.3,y:44.6}},{t:this.shape_1678},{t:this.shape_337,p:{x:-132.6,y:51.7}},{t:this.shape_719,p:{x:-117.2,y:51.7}},{t:this.shape_1677},{t:this.shape_82,p:{x:-90.8,y:51.7}},{t:this.shape_401,p:{x:-76.9,y:51.7}},{t:this.shape_694,p:{x:-57.2,y:52.8}},{t:this.shape_354,p:{x:-43.3,y:51.7}},{t:this.shape_332,p:{x:-30.3,y:50.3}},{t:this.shape_1676},{t:this.shape_1675},{t:this.shape_1674},{t:this.shape_1673},{t:this.shape_1672},{t:this.shape_1671},{t:this.shape_173,p:{x:66.3,y:50.3}},{t:this.shape_212,p:{x:78.9,y:51.7}},{t:this.shape_1670},{t:this.shape_1669},{t:this.shape_1668},{t:this.shape_1667},{t:this.shape_1666},{t:this.shape_1665},{t:this.shape_180,p:{x:147.1,y:50.3}},{t:this.shape_536,p:{x:155,y:51.7}},{t:this.shape_259,p:{x:163.9,y:50.3}},{t:this.shape_8,p:{x:-258.7,y:75.2}},{t:this.shape_1664},{t:this.shape_65,p:{x:-245.2,y:70.1}},{t:this.shape_369,p:{x:-231.9,y:76.7}},{t:this.shape_228,p:{x:-227.4,y:69.6}},{t:this.shape_1663},{t:this.shape_96,p:{x:-198.4,y:76.7}},{t:this.shape_35,p:{x:-197.7,y:81.8}},{t:this.shape_632,p:{x:-181,y:69.1}},{t:this.shape_1662},{t:this.shape_287,p:{x:-156,y:76.7}},{t:this.shape_252,p:{x:-142.4,y:75.3}},{t:this.shape_1661},{t:this.shape_1660},{t:this.shape_172,p:{x:-106.9,y:76.7}},{t:this.shape_201,p:{x:-95.3,y:76.7}},{t:this.shape_223,p:{x:-91.2,y:69.6}},{t:this.shape_1659},{t:this.shape_324,p:{x:-63.5,y:76.7}},{t:this.shape_701,p:{x:-63.1,y:70.1}},{t:this.shape_718,p:{x:-50.2,y:76.7}},{t:this.shape_519,p:{x:-41.2,y:76.7}},{t:this.shape_625,p:{x:-34.2,y:69.1}},{t:this.shape_248,p:{x:-17.7,y:75.3}},{t:this.shape_331,p:{x:-4.7,y:76.7}},{t:this.shape_156,p:{x:7.2,y:76.7}},{t:this.shape_195,p:{x:18.7,y:76.7}},{t:this.shape_296,p:{x:32,y:76.7}},{t:this.shape_211,p:{x:36.5,y:69.6}},{t:this.shape_284,p:{x:52.1,y:76.7}},{t:this.shape_1658},{t:this.shape_78,p:{x:78.1,y:76.7}},{t:this.shape_166,p:{x:90.9,y:75.3}},{t:this.shape_256,p:{x:103.3,y:76.7}},{t:this.shape_33,p:{x:122.8,y:78.1}},{t:this.shape_596,p:{x:134.6,y:76.9}},{t:this.shape_131,p:{x:142.5,y:80.6}},{t:this.shape_1657},{t:this.shape_358,p:{x:-258.5,y:116.1}},{t:this.shape_31,p:{x:-244.9,y:117.7}},{t:this.shape_1656},{t:this.shape_147,p:{x:-232.7,y:117.7}},{t:this.shape_755,p:{x:-220.8,y:117.7}},{t:this.shape_203,p:{x:-216.4,y:110.6}},{t:this.shape_246,p:{x:-200.8,y:117.7}},{t:this.shape_121,p:{x:-187.7,y:116.2}},{t:this.shape_546,p:{x:-168,y:117.7}},{t:this.shape_1655},{t:this.shape_1654},{t:this.shape_359,p:{x:-148.2,y:111.1}},{t:this.shape_1653},{t:this.shape_15,p:{x:-135.5,y:111.1}},{t:this.shape_125,p:{x:-123.8,y:117.7}},{t:this.shape_175,p:{x:-112.2,y:117.7}},{t:this.shape_198,p:{x:-108.1,y:110.6}},{t:this.shape_466,p:{x:-92.5,y:117.7}},{t:this.shape_1652},{t:this.shape_1651},{t:this.shape_113,p:{x:-72.6,y:111.1}},{t:this.shape_1650},{t:this.shape_234,p:{x:-59.9,y:111.1}},{t:this.shape_1649},{t:this.shape_486,p:{x:-34.4,y:118.8}},{t:this.shape_164,p:{x:-21.4,y:117.7}},{t:this.shape_347,p:{x:-21.4,y:111.1}},{t:this.shape_185,p:{x:-8.4,y:117.7}},{t:this.shape_1648},{t:this.shape_68,p:{x:12.4,y:117.7}},{t:this.shape_193,p:{x:17.2,y:110.6}},{t:this.shape_242,p:{x:32.8,y:117.7}},{t:this.shape_424,p:{x:33.1,y:111.1}},{t:this.shape_215,p:{x:45.9,y:116.3}},{t:this.shape_1647},{t:this.shape_168,p:{x:58.5,y:116.3}},{t:this.shape_224,p:{x:71.4,y:117.7}},{t:this.shape_187,p:{x:75.9,y:110.6}},{t:this.shape_752,p:{x:81.9,y:122.4}},{t:this.shape_1646},{t:this.shape_258,p:{x:105.9,y:117.7}},{t:this.shape_1645},{t:this.shape_169,p:{x:118.8,y:110.6}},{t:this.shape_1644},{t:this.shape_143,p:{x:132,y:110.6}},{t:this.shape_27,p:{x:147.1,y:116.4}},{t:this.shape_174,p:{x:155.8,y:117.7}},{t:this.shape_279,p:{x:164.4,y:116.2}},{t:this.shape_210,p:{x:177,y:117.7}},{t:this.shape_134,p:{x:181.5,y:110.6}},{t:this.shape_186,p:{x:-258.8,y:141.3}},{t:this.shape_609,p:{x:-249,y:142.9}},{t:this.shape_194,p:{x:-238.7,y:142.7}},{t:this.shape_1643},{t:this.shape_278,p:{x:-219.5,y:141.2}},{t:this.shape_395,p:{x:-208.1,y:144.2}},{t:this.shape_1642},{t:this.shape_95,p:{x:-190.3,y:142.7}},{t:this.shape_1641},{t:this.shape_157,p:{x:-160.7,y:142.7}},{t:this.shape_184,p:{x:-160.7,y:136.1}},{t:this.shape_176,p:{x:-147.4,y:142.7}},{t:this.shape_129,p:{x:-142.9,y:135.6}},{t:this.shape_104,p:{x:-135.3,y:142.7}},{t:this.shape_306,p:{x:-123.4,y:142.7}},{t:this.shape_126,p:{x:-119,y:135.6}},{t:this.shape_155,p:{x:-103.4,y:142.7}},{t:this.shape_1640},{t:this.shape_389,p:{x:-71.6,y:143.8}},{t:this.shape_188,p:{x:-57.7,y:142.7}},{t:this.shape_84,p:{x:-44.7,y:141.3}},{t:this.shape_128,p:{x:-26.2,y:141.1}},{t:this.shape_148,p:{x:-13.3,y:141.3}},{t:this.shape_247,p:{x:-13.3,y:147.8}},{t:this.shape_144,p:{x:-0.2,y:142.7}},{t:this.shape_123,p:{x:4.3,y:135.6}},{t:this.shape_360,p:{x:18.6,y:144.2}},{t:this.shape_1639},{t:this.shape_140,p:{x:48.8,y:141.3}},{t:this.shape_87,p:{x:57.4,y:142.7}},{t:this.shape_374,p:{x:-258.5,y:168.8}},{t:this.shape_1638},{t:this.shape_444,p:{x:-244.5,y:161.1}},{t:this.shape_149,p:{x:-231.4,y:166.3}},{t:this.shape_99,p:{x:-218.6,y:166.3}},{t:this.shape_154,p:{x:-210,y:167.7}},{t:this.shape_105,p:{x:-194.6,y:167.7}},{t:this.shape_120,p:{x:-181.3,y:167.7}},{t:this.shape_182,p:{x:-160.8,y:167.7}},{t:this.shape_22,p:{x:-147.8,y:166.3}},{t:this.shape_102,p:{x:-137.1,y:167.7}},{t:this.shape_1637},{t:this.shape_313,p:{x:-106.9,y:168.8}},{t:this.shape_273,p:{x:-93.6,y:167.7}},{t:this.shape_47,p:{x:-93.3,y:173.6}},{t:this.shape_136,p:{x:-80,y:167.7}},{t:this.shape_36,p:{x:-64.8,y:166.4}},{t:this.shape_372,p:{x:-50.4,y:167.7}},{t:this.shape_267,p:{x:-32.7,y:166.2}},{t:this.shape_308,p:{x:-21.3,y:169.2}},{t:this.shape_685,p:{x:-11.2,y:167.7}},{t:this.shape_141,p:{x:-3.5,y:167.7}},{t:this.shape_1636},{t:this.shape_97,p:{x:24.8,y:167.7}},{t:this.shape_100,p:{x:36.3,y:167.7}},{t:this.shape_51,p:{x:49.9,y:167.7}},{t:this.shape_407,p:{x:50.5,y:161.1}},{t:this.shape_58,p:{x:60.3,y:171.6}},{t:this.shape_1635},{t:this.shape_62,p:{x:-258.8,y:206.3}},{t:this.shape_751,p:{x:-246.9,y:205.9}},{t:this.shape_135,p:{x:-233.9,y:207.7}},{t:this.shape_117,p:{x:-220.3,y:206.2}},{t:this.shape_221,p:{x:-211.6,y:207.7}},{t:this.shape_109,p:{x:-202.2,y:207.7}},{t:this.shape_89,p:{x:-190.2,y:207.7}},{t:this.shape_83,p:{x:-178.7,y:207.7}},{t:this.shape_108,p:{x:-174.5,y:200.6}},{t:this.shape_199,p:{x:-160.2,y:209.2}},{t:this.shape_111,p:{x:-162,y:201.1}},{t:this.shape_622,p:{x:-145.1,y:200.1}},{t:this.shape_276,p:{x:-134.3,y:208.8}},{t:this.shape_75,p:{x:-121.3,y:207.7}},{t:this.shape_236,p:{x:-121.3,y:201.1}},{t:this.shape_106,p:{x:-107.9,y:207.7}},{t:this.shape_93,p:{x:-103.4,y:200.6}},{t:this.shape_724,p:{x:-94.6,y:207.7}},{t:this.shape_86,p:{x:-90.2,y:200.6}},{t:this.shape_139,p:{x:-81.6,y:206.3}},{t:this.shape_69,p:{x:-69,y:207.7}},{t:this.shape_150,p:{x:-69,y:201.1}},{t:this.shape_94,p:{x:-55.7,y:207.7}},{t:this.shape_81,p:{x:-51.2,y:200.6}},{t:this.shape_619,p:{x:-44.4,y:200.1}},{t:this.shape_44,p:{x:-27.7,y:207.7}},{t:this.shape_66,p:{x:-14.5,y:207.7}},{t:this.shape_67,p:{x:-10,y:200.6}},{t:this.shape_56,p:{x:-0.8,y:207.7}},{t:this.shape_233,p:{x:18.4,y:206.2}},{t:this.shape_334,p:{x:31,y:207.7}},{t:this.shape_119,p:{x:31.3,y:201.1}},{t:this.shape_1634},{t:this.shape_55,p:{x:48.4,y:200.6}},{t:this.shape_85,p:{x:57,y:206.3}},{t:this.shape_76,p:{x:76.1,y:206.3}},{t:this.shape_1633},{t:this.shape_160,p:{x:88.2,y:209.2}},{t:this.shape_42,p:{x:90.6,y:200.6}},{t:this.shape_1632},{t:this.shape_103,p:{x:112.4,y:206.2}},{t:this.shape_7,p:{x:126.1,y:207.7}},{t:this.shape_293,p:{x:135.7,y:207.7}},{t:this.shape_40,p:{x:-258.6,y:232.7}},{t:this.shape_43,p:{x:-245.3,y:232.7}},{t:this.shape_60,p:{x:-225.9,y:231.3}},{t:this.shape_613,p:{x:-213.4,y:231.3}},{t:this.shape_29,p:{x:-200,y:232.7}},{t:this.shape_38,p:{x:-186.8,y:232.7}},{t:this.shape_10,p:{x:-174.1,y:231.3}},{t:this.shape_9,p:{x:-155.2,y:232.7}},{t:this.shape_88,p:{x:-142.2,y:231.2}},{t:this.shape_30,p:{x:-133.4,y:232.7}},{t:this.shape_16,p:{x:-124,y:232.7}},{t:this.shape_733,p:{x:-104.4,y:232.8}},{t:this.shape_63,p:{x:-92.5,y:234.2}},{t:this.shape_25,p:{x:-81.3,y:232.7}},{t:this.shape_48,p:{x:-67.8,y:232.7}},{t:this.shape_28,p:{x:-63,y:225.6}},{t:this.shape_5,p:{x:-47.9,y:231.3}},{t:this.shape_114,p:{x:-34.9,y:232.7}},{t:this.shape_45,p:{x:-23,y:232.7}},{t:this.shape_17,p:{x:-11.5,y:232.7}},{t:this.shape_3,p:{x:1.8,y:232.7}},{t:this.shape_6,p:{x:6.3,y:225.6}},{t:this.shape_13,p:{x:14.2,y:234.2}},{t:this.shape_319,p:{x:32.2,y:232.7}},{t:this.shape_26,p:{x:41.5,y:232.7}},{t:this.shape_79,p:{x:50,y:231.2}},{t:this.shape_107,p:{x:63.2,y:232.7}},{t:this.shape_59,p:{x:72.8,y:232.7}},{t:this.shape_32,p:{x:86.9,y:232.7}},{t:this.shape_604,p:{x:97.9,y:231.2}},{t:this.shape_1,p:{x:113.4,y:236.6}},{t:this.shape_1631},{t:this.instance_2},{t:this.instance_1}]},1).wait(1));

	// Layer 5
	this.instance_3 = new lib.Symbol1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(121.5,-7.5);
	this.instance_3.alpha = 0.309;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5));

	// action
	this.shape_1818 = new cjs.Shape();
	this.shape_1818.graphics.f("#689F38").s().p("EhHAADmIAAnLMCOBAAAIAAHLg");
	this.shape_1818.setTransform(121.8,-256);

	this.instance_4 = new lib.Bitmap1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-333,-279);

	this.instance_5 = new lib.Bitmap1copy();
	this.instance_5.parent = this;
	this.instance_5.setTransform(235,-183);

	this.instance_6 = new lib.Bitmap5();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-333,-279);

	this.shape_1819 = new cjs.Shape();
	this.shape_1819.graphics.f("#FFFFFF").s().p("AgjBpQgRgIgIgLIARgWQAQAUAYAAQAQAAALgJQAKgLAAgSIAAgLQgPARgZAAQgcAAgRgVQgSgXAAgmQAAglASgWQARgWAdAAQAaAAAPATIACgQIAgAAIAACYQAAAggUASQgTASggAAQgRAAgRgHgAgXhEQgLAOABAbQgBAYALANQAJAOAQAAQAWAAAKgTIAAhFQgKgSgVAAQgRAAgJAOg");
	this.shape_1819.setTransform(298.5,-249.9);

	this.shape_1820 = new cjs.Shape();
	this.shape_1820.graphics.f("#FFFFFF").s().p("AAeBRIAAhmQAAgPgGgHQgHgIgPAAQgUAAgLAUIAABwIgkAAIAAieIAiAAIABATQASgWAbAAQAyAAABA5IAABog");
	this.shape_1820.setTransform(281.9,-253.1);

	this.shape_1821 = new cjs.Shape();
	this.shape_1821.graphics.f("#FFFFFF").s().p("AgRBtIAAidIAjAAIAACdgAgOhMQgFgFAAgIQAAgJAFgFQAFgFAJAAQAJAAAGAFQAFAFAAAJQAAAIgFAFQgGAFgJAAQgJAAgFgFg");
	this.shape_1821.setTransform(269.8,-255.9);

	this.shape_1822 = new cjs.Shape();
	this.shape_1822.graphics.f("#FFFFFF").s().p("AgRBxIAAjgIAjAAIAADgg");
	this.shape_1822.setTransform(262.1,-256.2);

	this.shape_1823 = new cjs.Shape();
	this.shape_1823.graphics.f("#FFFFFF").s().p("AAiBPIgihsIghBsIgdAAIgridIAiAAIAaBrIAghrIAaAAIAhBsIAZhsIAjAAIgrCdg");
	this.shape_1823.setTransform(247.1,-252.9);

	this.shape_1824 = new cjs.Shape();
	this.shape_1824.graphics.f("#FFFFFF").s().p("Ag0A8QgVgWAAgmIAAAAQAAgYAKgSQAJgTAQgKQARgKAVAAQAgAAAUAUQAUAVACAiIAAAIQAAAXgJATQgJASgRAKQgRAKgWAAQggAAgUgWgAgbgnQgKAOAAAbQAAAYAKAOQAKAOARAAQASAAAKgOQAKgOAAgaQAAgYgKgPQgKgOgSAAQgRAAgKAOg");
	this.shape_1824.setTransform(227.4,-252.9);

	this.shape_1825 = new cjs.Shape();
	this.shape_1825.graphics.f("#FFFFFF").s().p("AApBrIgqhSIgpAAIAABSIglAAIAAjVIBKAAQAlAAATARQAVARAAAfQgBAVgJAPQgLANgTAIIAxBZIAAACgAgqgEIAmAAQARAAALgKQALgJAAgQQAAgSgKgJQgKgJgSgBIgnAAg");
	this.shape_1825.setTransform(210.4,-255.7);

	this.shape_1826 = new cjs.Shape();
	this.shape_1826.graphics.f("#FFFFFF").s().p("AgOAOQgFgFAAgJQgBgHAGgGQAFgFAJgBQAKABAFAFQAFAGABAHQgBAJgFAFQgFAGgKgBQgIABgGgGg");
	this.shape_1826.setTransform(188.4,-246.8);

	this.shape_1827 = new cjs.Shape();
	this.shape_1827.graphics.f("#FFFFFF").s().p("AApBrIg/hcIgZAZIAABDIglAAIAAjVIAlAAIAABlIAVgaIA9hLIAtAAIhPBfIBUB2g");
	this.shape_1827.setTransform(176,-255.7);

	this.shape_1828 = new cjs.Shape();
	this.shape_1828.graphics.f("#FFFFFF").s().p("AgOAOQgGgFABgJQAAgHAEgGQAGgFAJgBQAKABAGAFQAEAGAAAHQAAAJgEAFQgGAGgKgBQgIABgGgGg");
	this.shape_1828.setTransform(161.1,-246.8);

	this.shape_1829 = new cjs.Shape();
	this.shape_1829.graphics.f("#FFFFFF").s().p("AgyBbQgTgRAAgeIAlAAQAAARAJAJQAIAJAPAAQAPAAAJgKQAJgKAAgSIAAiUIAlAAIAACVQAAAegTASQgUASgfAAQggAAgSgRg");
	this.shape_1829.setTransform(148,-255.5);

	this.shape_1830 = new cjs.Shape();
	this.shape_1830.graphics.f("#FFFFFF").s().p("AglAOIAAgbIBLAAIAAAbg");
	this.shape_1830.setTransform(128,-254.1);

	this.shape_1831 = new cjs.Shape();
	this.shape_1831.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHABANQgBALgKAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_1831.setTransform(109.9,-263.4);

	this.shape_1832 = new cjs.Shape();
	this.shape_1832.graphics.f("#FFFFFF").s().p("AgiBOQgbgBgSgRQgRgRgBgZQgBgXAIgQIASAAQgCAGACACQADACAFAAQAJAAAHgEQAPgJADgLIABgHQAAgMgIgEQgIgDgHADQAMAEAAAOQAAAIgGAFQgFAEgIAAIgIgBQgOgEAAgRQAAgGACgGQAIgUAdAAQALAAAIAFQAJAFADAJQAQgVAgACQAhABARAgQAMAVAAAZQgBA0gmASQgMAGgOAAQgIAAgKgCIAAgNQgMAKgKADQgJACgPAAIgEAAgAASgUQASAPAAAXQAAAZgPAQIAMABQANAAAIgGQATgOAAgfQAAgTgOgPQgNgPgTgBQgYgBgLAOIABAAQAPAAAKAIgAhNALQAAAMAHAJQAJAMAXAAIANgBQAOgBAKgLQAIgKAAgMQAAgHgEgFQgEgIgNgBQgGgBgKAHQgMAIgEABQgIADgJAAQgJAAgEgDIgBAIg");
	this.shape_1832.setTransform(103.1,-252.8);

	this.shape_1833 = new cjs.Shape();
	this.shape_1833.graphics.f("#FFFFFF").s().p("AAzAkQAMgIAAgMIAAgFQgDgJgTgEQgSgEgXAAQgWAAgRAEQgUAEgDAJIgBAFQAAANAMAHIgQAAQgPgFgBgVQABgIACgEQAJgTAYgIQASgGAdAAQBDAAAOAhQADAFAAAHQAAAUgPAGg");
	this.shape_1833.setTransform(83.1,-262.7);

	this.shape_1834 = new cjs.Shape();
	this.shape_1834.graphics.f("#FFFFFF").s().p("AhBBEQgZgdAAgqQABgpAagbQAagbArAAQAkAAAZAYQAYAYAAAkQAAAdgQAVQgRAWgcABQgbAAgQgSQgRgRACgZQACgXASgNIgVAAIAAgSIBHAAIAAASIgOAAQgNAAgJAIQgIAIAAAMQgBAMAJAKQAJAIAPAAQASgBAKgOQAKgMgBgSQgCgXgKgNQgRgTgigBQgcAAgRAZQgPAVAAAfQAAAdAPAWQAPAXAaAEIAOACQAgAAASgbIAXAAQgXAxg+AAQgpAAgagfg");
	this.shape_1834.setTransform(83.3,-250.7);

	this.shape_1835 = new cjs.Shape();
	this.shape_1835.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHAAANQAAALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAAAgJQAAgKgLAAIgDAAg");
	this.shape_1835.setTransform(70.3,-263.4);

	this.shape_1836 = new cjs.Shape();
	this.shape_1836.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAJAAAHgSQAGgSAAgVQAAgVgGgSQgGgTgKgBQgIAAgHAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgNAWQgOAagVAAQgNAAgKgGg");
	this.shape_1836.setTransform(69.9,-252.8);

	this.shape_1837 = new cjs.Shape();
	this.shape_1837.graphics.f("#FFFFFF").s().p("AgFBkQgigCgUgVQgVgVAAgfQgBgWAJgPQAIgQARgQQAMgJAXgXQAMgOADgJIAgAAQgGAMgJALQgJAKgPAKQAWgBARAGQAvARAAA3QAAAjgZAWQgYAWgiAAIgEAAgAgrgVQgOAPAAATQAAATANAMQARASAbAAQAVAAAQgMQAUgPAAgYQAAgLgEgKQgGgOgPgIQgNgHgRAAQgdAAgQASg");
	this.shape_1837.setTransform(58,-254.9);

	this.shape_1838 = new cjs.Shape();
	this.shape_1838.graphics.f("#FFFFFF").s().p("AgyA4QgWgWAAghQAAgiAWgWQAXgWAhAAQAjAAARAPQAMAKABAPIgSAAQgBgKgPgHQgNgFgQAAQgWAAgRAOQgRAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQAAgPAMgJQAMgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgXgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDAAgFQAAgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_1838.setTransform(41.8,-252.8);

	this.shape_1839 = new cjs.Shape();
	this.shape_1839.graphics.f("#FFFFFF").s().p("AgNASQgIgHAAgLQAAgHAEgGQAHgIAKAAQAHAAAGAEQAJAHAAAKQAAAIgEAGQgHAIgLAAQgHAAgGgEg");
	this.shape_1839.setTransform(20.6,-246.9);

	this.shape_1840 = new cjs.Shape();
	this.shape_1840.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHABANQgBALgKAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_1840.setTransform(12.2,-263.4);

	this.shape_1841 = new cjs.Shape();
	this.shape_1841.graphics.f("#FFFFFF").s().p("AAbBNIAAgMQgMANgPAAQgQAAgLgMQgLAMgYAAQgOAAgLgJQgKgJAAgOQAAgRAHgJIgMAAIAAgMIAJgMQAHgJAIgGIgEAAQgJAAgFgGQgEgGAAgJQAAgOAKgLQALgLAOgBQAagCAMARQAZgRAgACQAfACAVAWQAVAWAAAfQABAzgmAWQgIAFgRAAQgJAAgFgBgAgMgvQAYACAPARQASAUAAAaQAAAagNAOQADACAEAAQAMAAAMgLQALgKACgOIABgLQAAgegVgTQgQgPgeAAQgLAAgLADgAgygVQgPAMgNAOIAMAAQgHALABALQAAAGAGAEQAFAEAIAAQALAAAEgMQADgIAAgVIARAAQgBAXACAGQADAMAMgBQAJAAAGgKQAGgIAAgNQAAgNgJgMQgIgMgMgBIgJgBQgUAAgLAJgAhIg2QAHACABAHQACAGgDAHIAKgKIAKgJQgCgHgLAAIgCAAQgJAAgDAEg");
	this.shape_1841.setTransform(5,-252.8);

	this.shape_1842 = new cjs.Shape();
	this.shape_1842.graphics.f("#FFFFFF").s().p("AgyA4QgWgWAAghQAAgiAWgWQAXgWAgAAQAkAAARAPQALAKACAPIgSAAQgCgKgOgHQgNgFgQAAQgWAAgRAOQgRAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgJAHQgKAIgLAAQgNAAgKgJQgJgIAAgOQgBgPAMgJQAMgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgXgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDABgFQgBgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_1842.setTransform(-13.3,-252.8);

	this.shape_1843 = new cjs.Shape();
	this.shape_1843.graphics.f("#FFFFFF").s().p("AgNASQgIgHAAgLQAAgHAEgGQAHgIAKAAQAHAAAGAEQAJAHAAAKQAAAIgEAGQgHAIgLAAQgHAAgGgEg");
	this.shape_1843.setTransform(-24.9,-246.9);

	this.shape_1844 = new cjs.Shape();
	this.shape_1844.graphics.f("#FFFFFF").s().p("AhABfQgdgWACgfQACgaAUgNQgOgDAAgPQAAgOAQgIQAMgEARgBQAXAAALAPQADgEAEgDIAAgeIgOAAIAAgPIAOAAIAAgUIgOAAIAAgOIAoAAQANAAAIAGQAJAIAAAMQAAALgLAHQgLAGgQgBIAAAYIAFgCIANABQALgKAIgOQAHgOABgRIAaAAQgDASgKARQgJARgJAHQANAFADAQQADAPgFALQANAOAAAUQAAAfgdAVQgaASgiAAQgkAAgbgTgAg7ASQgJAJABALQABASAVALQATAKAZAAQAZgBASgIQAWgLABgUQAAgEgBgEQgFAJgJAFQgKAEgJAAQgGAAgFgCQgNgHAAgRIACgLIgxAAQgLAAgIAIgAAaASQAAALAJAAQAJAAAIgNQgGgGgTAAIgBAIgAAlgRQgEAFgBAGQAUABAHAFQACgEAAgFQAAgNgNAAQgGgBgFAGgAAKgSQgFAGAAAGIAIAAIADgJQADgGACgCQgHABgEAEgAgxgVQAGACABAEQABAGgDADIAeAAQgCgMgKgEQgGgCgFAAQgGAAgGADgAAVhiIAAATQAPAAAAgJQAAgKgMAAIgDAAg");
	this.shape_1844.setTransform(-38.8,-256.3);

	this.shape_1845 = new cjs.Shape();
	this.shape_1845.graphics.f("#FFFFFF").s().p("AgxA4QgXgWAAghQAAgiAXgWQAWgWAhAAQAjAAARAPQAMAKAAAPIgSAAQAAgKgPgHQgNgFgQAAQgWAAgQAOQgSAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgKAHQgIAIgMAAQgOAAgJgJQgKgIAAgOQABgPAMgJQALgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgWgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQAEgDgBgFQABgGgEgDQgEgEgFAAQgGAAgDADg");
	this.shape_1845.setTransform(-56.1,-252.8);

	this.instance_7 = new lib.Bitmap3();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-333,-279);

	this.text = new cjs.Text("", "12px 'FMEmaneex'");
	this.text.textAlign = "center";
	this.text.lineHeight = 16;
	this.text.parent = this;
	this.text.setTransform(-283.6,122.8);

	this.text_1 = new cjs.Text("", "12px 'FMEmaneex'");
	this.text_1.lineHeight = 16;
	this.text_1.parent = this;
	this.text_1.setTransform(-265.4,-60.9);

	this.shape_1846 = new cjs.Shape();
	this.shape_1846.graphics.f("#FFFFFF").s().p("Ag0BFQgPgOAAgUQAAgZATgNQATgOAiAAIAVAAIAAgKQAAgMgGgIQgHgHgNAAQgNAAgHAGQgIAGAAAJIgkAAQAAgNAJgLQAJgLAPgHQAOgGASAAQAbAAARAOQARAOAAAZIAABHQAAAVAGANIAAACIgkAAQgDgEgBgLQgRASgYAAQgYAAgPgNgAgWAMQgJAHAAAMQAAAKAGAHQAHAGAMAAQAJAAAKgGQAJgFAEgIIAAgeIgTAAQgTAAgKAHg");
	this.shape_1846.setTransform(227.5,-252.9);

	this.shape_1847 = new cjs.Shape();
	this.shape_1847.graphics.f("#FFFFFF").s().p("ABJBrIAAhHIADhdIg/CkIgZAAIg/ijIAEBcIAABHIgmAAIAAjVIAxAAIA9CjIA8ijIAxAAIAADVg");
	this.shape_1847.setTransform(206.2,-255.7);

	this.shape_1848 = new cjs.Shape();
	this.shape_1848.graphics.f("#FFFFFF").s().p("AAcBxIguhFIgPARIAAA0IgkAAIAAjgIAkAAIAACAIALgNIArgxIAqAAIg7BBIBCBdg");
	this.shape_1848.setTransform(178.8,-256.2);

	this.shape_1849 = new cjs.Shape();
	this.shape_1849.graphics.f("#FFFFFF").s().p("AgwA9QgUgWAAgmIAAgCQAAglAUgVQATgWAgAAQAdAAASAQQARARABAbIghAAQgBgOgIgJQgJgJgOAAQgQAAgKANQgIAMgBAaIAAAFQAAAZAJANQAKANAQAAQANAAAKgIQAIgIABgLIAhAAQAAAPgJAMQgJANgOAIQgPAHgRAAQghAAgTgVg");
	this.shape_1849.setTransform(162.3,-252.9);

	this.shape_1850 = new cjs.Shape();
	this.shape_1850.graphics.f("#FFFFFF").s().p("Ag0BFQgPgOAAgUQAAgZATgNQATgOAiAAIAVAAIAAgKQAAgMgGgIQgHgHgNAAQgNAAgHAGQgIAGAAAJIgkAAQAAgNAJgLQAJgLAPgHQAOgGASAAQAbAAARAOQARAOAAAZIAABHQAAAVAGANIAAACIgkAAQgDgEgBgLQgRASgYAAQgYAAgPgNgAgWAMQgJAHAAAMQAAAKAGAHQAHAGAMAAQAJAAAKgGQAJgFAEgIIAAgeIgTAAQgTAAgKAHg");
	this.shape_1850.setTransform(146.2,-252.9);

	this.shape_1851 = new cjs.Shape();
	this.shape_1851.graphics.f("#FFFFFF").s().p("AgmAOIAAgbIBNAAIAAAbg");
	this.shape_1851.setTransform(109,-254.1);

	this.shape_1852 = new cjs.Shape();
	this.shape_1852.graphics.f("#FFFFFF").s().p("AgrBcQgTgJgLgQQgQgYAAgjQAAgXANgUQAOgUASABQARABAFANQAJgPATAAQARAAAMANQAMAMABASQACAUgQAQQgQAPgUAAQgUAAgNgMQgNgMABgSQAAgGADgGIAFgLQAAgDgCgDQgDgCgEAAQgJABgFAMQgFALAAANQgBAaAUARQAUARAbAAQAbAAAUgUQAUgVgBgcQAAgigRgUQgSgWghAAQgqAAgRAeIgUAAQAJgXAWgNQAVgMAbAAQApAAAXAYQAbAbACAtQACAsgZAfQgaAegqAAQgXAAgTgJgAgagHQAAAIAIAGQAIAFAKABQALAAAJgIQAJgGAAgLIAAgFQgCAHgHAEQgIAFgJAAQgVAAgEgTQgEAKAAADgAgEgdQAAAFADADQADADAEAAQAFAAADgDQADgDAAgFQAAgEgDgDQgEgEgEAAQgKAAAAALg");
	this.shape_1852.setTransform(77.5,-255);

	this.shape_1853 = new cjs.Shape();
	this.shape_1853.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQANAAAIAGQAJAHAAANQAAALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQANAAAAgJQAAgKgLAAIgCAAg");
	this.shape_1853.setTransform(54.9,-263.4);

	this.shape_1854 = new cjs.Shape();
	this.shape_1854.graphics.f("#FFFFFF").s().p("AgiBBQgOgMAAgWQAAgRAGgMQAFgIAKgMQAMgOAfgZIg7AAIAAgSIBZAAIAAASIgVAPQgKAIgIAIQgNAPgEAGQgHAMABANQAAAJAHAFQAFAFAIAAQAQAAAFgOIACgIQAAgGgEgGIASAAQAJANgBAQQgCASgOAMQgOAMgTAAQgVAAgNgLg");
	this.shape_1854.setTransform(31.8,-252.5);

	this.shape_1855 = new cjs.Shape();
	this.shape_1855.graphics.f("#FFFFFF").s().p("Ag/BTQgdgVACggQABgZAUgNQgNgCAAgQQAAgOAQgIQAMgFARAAQAYAAAKAPQAIgMAWgDIANAAIAJgHQAEgEADgGQAJgPAAgQIAbAAIgFARQgEALgGAIQgGAJgJAHQANAHACAPQABAIgDAIIgPAAQAHABAIAIQANAMAAAXQAAAegcAVQgaASgjAAQgkAAgagTgAg7AHQgIAJABALQAAARAWALQATAKAYAAQAZAAATgJQAVgKABgUQABgKgIgJQgHgHgSAAIhIAAQgMAAgIAHgAAWgjQgIACgDAEQgGAFAAAHIAeAAQAJAAAHACQADgEgDgHQgEgJgSAAIgHAAgAgwghQAFADABAEQABAFgDAEIAhAAQgDgMgKgEQgHgDgGAAQgGAAgFADg");
	this.shape_1855.setTransform(17.4,-255.2);

	this.instance_8 = new lib.Bitmap4();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-333,-279);

	this.instance_9 = new lib.Path();
	this.instance_9.parent = this;
	this.instance_9.setTransform(121.5,-5.9,1,1,0,0,0,454.5,271.5);
	this.instance_9.alpha = 0.84;

	this.instance_10 = new lib.ClipGroup_0();
	this.instance_10.parent = this;
	this.instance_10.setTransform(82.1,1.6,1,1,0,0,0,496.7,279);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.shape_1818}]}).to({state:[{t:this.instance_6},{t:this.shape_1818},{t:this.instance_5}]},1).to({state:[{t:this.instance_7},{t:this.shape_1818},{t:this.shape_1845},{t:this.shape_1844},{t:this.shape_1843},{t:this.shape_1842},{t:this.shape_1841,p:{x:5}},{t:this.shape_1840},{t:this.shape_1839},{t:this.shape_1838},{t:this.shape_1837},{t:this.shape_1836,p:{x:69.9}},{t:this.shape_1835},{t:this.shape_1834},{t:this.shape_1833},{t:this.shape_1832},{t:this.shape_1831},{t:this.shape_1830},{t:this.shape_1829,p:{x:148}},{t:this.shape_1828},{t:this.shape_1827},{t:this.shape_1826},{t:this.shape_1825},{t:this.shape_1824},{t:this.shape_1823},{t:this.shape_1822},{t:this.shape_1821},{t:this.shape_1820},{t:this.shape_1819}]},1).to({state:[{t:this.instance_8},{t:this.shape_1818},{t:this.shape_1855},{t:this.shape_1854},{t:this.shape_1841,p:{x:47.7}},{t:this.shape_1853},{t:this.shape_1852},{t:this.shape_1836,p:{x:90.6}},{t:this.shape_1851},{t:this.shape_1829,p:{x:129}},{t:this.shape_1850},{t:this.shape_1849},{t:this.shape_1848},{t:this.shape_1847},{t:this.shape_1846},{t:this.text_1},{t:this.text}]},1).to({state:[{t:this.instance_10},{t:this.instance_9}]},1).wait(1));

	// Layer 1
	this.instance_11 = new lib.Image();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-713,-437,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-713,-437,1426.1,874.1);


(lib.btn5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AqmGzQgvAAgighQgigiAAgwIAAp/QAAgwAigiQAighAvAAIVMAAQAwAAAiAhQAiAiAAAwIAAJ/QAAAwgiAiQgiAhgwAAg");
	this.shape.setTransform(-0.1,71);

	this.instance = new lib.ClipGroup_2();
	this.instance.parent = this;
	this.instance.setTransform(-47.5,-26.8,1,1,0,0,0,140.1,78.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AsYLuIAA3bIYyAAIAAXbg");
	this.shape_1.setTransform(0.1,-30.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#73B148").s().p("AsYBuIAAjbIYyAAIAADbg");
	this.shape_2.setTransform(0.1,-103.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.instance},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-187.6,-114.5,280.1,229);


// stage content:
(lib.civic3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		createjs.Touch.enable(stage);
		
		var clip = this;
		var count = 5; // Define the number of buttons |Sanka|
		
		
		
		this.home_btn.visible = false;
		this.slider_mov.visible = false;
		
		
		
		
		for (var i = 1; i <= count; i++) {
			var namev = clip["name" + i];
			namev.visible = false;
		}
		
		
		
		
		var item_chk;
		var btn = []; //Define the array switchyes
		
		for (var i = 1; i <= count; i++) {
			var btnn = clip["btn" + i];
			btnn.name = i;
			btn[i - 1] = clip["btn" + i];
			btn[i - 1].addEventListener("click", btn_clk.bind(this));
		}
		
		
		function btn_clk(evt) {
		
		
		
			var item = evt.currentTarget.name; // Selecting (current target) button name |Sanka|
		
		
			if (clip["name" + item].visible == true) {
		
				
				this.slider_mov.visible = true;
				clip["slider_mov"].gotoAndStop(item - 1);
		
				
				item_chk = item;
				counta = item;
				next_back_fun();
				
		
		
				for (var i = 1; i <= count; i++) {
					var btnn = clip["btn" + i];
					btnn.mouseEnabled = false;
				}
				this.home_btn.visible = true;
				
				this.getStage().getChildAt(0).slider_mov.crnt_fun(item);
			}
		
			clip["name" + item].visible = true;
		
		
		}
		
		
		
		this.home_btn.addEventListener("click", close_clk.bind(this));
		
		function close_clk() {
			this.slider_mov.visible = false;
			for (var i = 1; i <= count; i++) {
				var btn = clip["btn" + i];
				btn.mouseEnabled = true;
			}
		
			this.home_btn.visible = false;
		
			this.back.visible = false;
			this.next.visible = false;
			
			this.getStage().getChildAt(0).slider_mov.stop_sound_fun();
		
		
		}
		var clip = this; 
		this.stop();
		var gcounta = 5;
		
		this.back.visible = false;
		this.next.visible = false;
		
		
		///////////// back next
		var counta;
		var slider_slider_movx = clip["slider_mov"].x ;
		
		
		
		
		//var slider_slider_movount = gcounta - 1;
		
		this.back.visible = false;
		
		
		
		this.next.addEventListener("click", next_btn.bind(this));
		
		
		function next_btn()
		{
			
			
			
			console.log(counta);
			counta = counta + 1;
			this.getStage().getChildAt(0).slider_mov.crnt_fun(counta);
			if(counta > 0)
			{
				this.back.visible = true;
				
				
			}
			
			
				if(counta == gcounta)
			{
				this.next.visible = false;
			}
			
		
			//alert(counta)
			
			//clip["slider_mov"].x = 301.45;
			clip["slider_mov"].gotoAndStop(counta-1);
			var crnt = clip["slider_mov"];
			//createjs.Tween.get(crnt).to({x:slider_slider_movx}, 350, crnt.transitionEase);
			
			
		
			
			this.getStage().getChildAt(0).slider_mov.stop_sound_fun();
		
		}
		
		
		
		
		this.back.addEventListener("click", back_btn.bind(this));
		
		
		function back_btn()
		{
			
			this.next.visible = true;
			
			console.log(counta);
			
			
			
			
			counta = counta - 1;
			if(counta == 1)
			{
				this.back.visible = false;
				
			}
			
		
			//alert(counta)
			
			//clip["slider_mov"].x = -348.55;
			clip["slider_mov"].gotoAndStop(counta-1);
			//var crnt = clip["slider_mov"];
			//createjs.Tween.get(crnt).to({x:slider_slider_movx}, 350, crnt.transitionEase);
			this.getStage().getChildAt(0).slider_mov.crnt_fun(counta);
			
			this.getStage().getChildAt(0).slider_mov.stop_sound_fun();
		
		}
		
		
		function next_back_fun()
		{
				
			
				clip.back.visible = false;
				clip.next.visible = false;
			
			
				
				if(item_chk < 5 ){
					clip.next.visible = true;
				}
				
				if(item_chk > 1 )
				{
					clip.back.visible = true;
				}
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// home_btn
	this.home_btn = new lib.Symbol2();
	this.home_btn.parent = this;
	this.home_btn.setTransform(1248.4,362.5,1,1,0,0,0,32.1,361.5);
	new cjs.ButtonHelper(this.home_btn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.home_btn).wait(1));

	// next
	this.next = new lib.next();
	this.next.parent = this;
	this.next.setTransform(1108.1,607,0.683,0.683);
	new cjs.ButtonHelper(this.next, 0, 1, 1);

	this.back = new lib.back();
	this.back.parent = this;
	this.back.setTransform(1068.5,607,0.683,0.683);
	new cjs.ButtonHelper(this.back, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.back},{t:this.next}]}).wait(1));

	// slider
	this.slider_mov = new lib.slider_mov();
	this.slider_mov.parent = this;
	this.slider_mov.setTransform(568,368);

	this.timeline.addTween(cjs.Tween.get(this.slider_mov).wait(1));

	// names
	this.name5 = new lib.name5();
	this.name5.parent = this;
	this.name5.setTransform(1043.1,451.1);

	this.name4 = new lib.name4();
	this.name4.parent = this;
	this.name4.setTransform(868.4,450.9);

	this.name3 = new lib.name3();
	this.name3.parent = this;
	this.name3.setTransform(690.3,450.9);

	this.name2 = new lib.name2();
	this.name2.parent = this;
	this.name2.setTransform(512.2,450.9);

	this.name1 = new lib.name1();
	this.name1.parent = this;
	this.name1.setTransform(334.2,449.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.name1},{t:this.name2},{t:this.name3},{t:this.name4},{t:this.name5}]}).wait(1));

	// btns
	this.btn5 = new lib.btn5();
	this.btn5.parent = this;
	this.btn5.setTransform(995.4,373.7,1,1,0,0,0,-47.6,0);
	new cjs.ButtonHelper(this.btn5, 0, 1, 1);

	this.btn4 = new lib.btn4();
	this.btn4.parent = this;
	this.btn4.setTransform(868.5,373.6);
	new cjs.ButtonHelper(this.btn4, 0, 1, 1);

	this.btn3 = new lib.btn3();
	this.btn3.parent = this;
	this.btn3.setTransform(690.5,373.6);
	new cjs.ButtonHelper(this.btn3, 0, 1, 1);

	this.btn2 = new lib.btn2();
	this.btn2.parent = this;
	this.btn2.setTransform(512.5,373.6);
	new cjs.ButtonHelper(this.btn2, 0, 1, 1);

	this.btn1 = new lib.btn1();
	this.btn1.parent = this;
	this.btn1.setTransform(334.5,373.6);
	new cjs.ButtonHelper(this.btn1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.btn1},{t:this.btn2},{t:this.btn3},{t:this.btn4},{t:this.btn5}]}).wait(1));

	// bg
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHgBANQABALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAABgJQgBgKgLAAIgDAAg");
	this.shape.setTransform(979.7,108.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiBOQgbgBgSgRQgRgRgBgZQgBgXAIgQIASAAQgCAGACACQADACAFAAQAJAAAHgEQAPgJADgLIABgHQAAgMgIgEQgIgDgHADQAMAEAAAOQAAAIgGAFQgFAEgIAAIgIgBQgOgEAAgRQAAgGACgGQAIgUAdAAQALAAAIAFQAJAFADAJQAQgVAgACQAhABARAgQAMAVAAAZQgBA0gmASQgMAGgOAAQgIAAgKgCIAAgNQgMAKgKADQgJACgPAAIgEAAgAASgUQASAPAAAXQAAAZgPAQIAMABQANAAAIgGQATgOAAgfQAAgTgOgPQgNgPgTgBQgYgBgLAOIABAAQAPAAAKAIgAhNALQAAAMAHAJQAJAMAXAAIANgBQAOgBAKgLQAIgKAAgMQAAgHgEgFQgEgIgNgBQgGgBgKAHQgMAIgEABQgIADgJAAQgJAAgEgDIgBAIg");
	this.shape_1.setTransform(972.9,119.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_2.setTransform(953,119.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhBBEQgZgdAAgqQABgpAagbQAagbArAAQAkAAAZAYQAYAYAAAkQAAAdgQAVQgRAWgcABQgbAAgQgRQgRgSACgZQACgXASgNIgVAAIAAgSIBHAAIAAASIgOAAQgNAAgJAIQgIAIAAAMQgBAMAJAKQAJAIAPAAQASgBAKgOQAKgMgBgSQgBgXgLgNQgQgTgjgBQgcAAgRAZQgPAVAAAfQAAAdAPAWQAPAXAaAEIAOACQAgAAASgbIAXAAQgXAxg+AAQgpAAgagfg");
	this.shape_3.setTransform(933.9,121.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AASBOIAAgPQgMAJgJADQgLAEgTAAQgfgBgSgXQgPgVAAgfQAAgzAjgWQAMgHAUAAIAKABIAAARQAVgTAdABQAhABASAWQARAUAAAhQABAhgTAXQgTAYggAAIgLgBgAAMgZQAaAHAAAhQAAAZgRAUQARACAMgHQAUgOgBghQgBgTgNgPQgMgPgTgBQgjgDgFAYQAIgFALAAIAJABgAg/grQgKAOAAATQAAAVALAOQAMAPAVABQARABAMgIQAMgHABgOQABgHgFgGQgFgHgHAAQgGABgEAEQgEADAAAHIABAHIgTAAQgEgTADgRQACgSANgSQgGgCgFAAQgTAAgMAQg");
	this.shape_4.setTransform(914.3,119.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgUAlIAAgfIgNAAIAAgOIANAAIAAgUIgOAAIAAgOIAoAAQAMAAAJAGQAJAHAAANQAAALgLAGQgLAGgQgBIAAAlgAgCgbIAAATQAOAAAAgJQAAgKgLAAIgDAAg");
	this.shape_5.setTransform(900.6,108.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAEBgQgOgHgBgPQAAgMAJgGQgiACgWgTQgWgSACgcQACghAbgPQgEgBgEgFQgDgFAAgGIABgHQAGgNAPgEQAKgEAVAAQAkAAAPATQANAOAAAWIg4AAQgQAAgMAKQgLALgBAQQgCARAQAOQAPANATABQAgABAXgLIAAASQgMAFgKACQgTAFgBAJQAAAGAFADQAFADAIAAQAJAAAHgEQAGgFAAgIIgBgHIASAAIABAIQAAARgOAKQgMAKgTACIgFAAQgOAAgMgFgAgYhPIAEACQADACAAADQABAHgGACIAJAAIAuAAQgGgMgOgFQgIgCgLAAQgMABgGACg");
	this.shape_6.setTransform(897,121.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhOA1IAAgdICLAAIAAgxIAJgMQAGgJADgGIAABpg");
	this.shape_7.setTransform(879.3,127);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhFA4QgWgSABggQgBgMAHgMQAGgMAIgFQgJgFAAgLQAAgEABgDQAHgUAjAAQAaAAAKAPQALgPAeAAQANAAAKAHQAMAGACAMQACAKgGAIQAIAGAFALQAEAMAAAMQAAAhgWATQgZAUgsABQgsAAgZgXgAg+gRQgIAIABANQABAUAYAJQASAIAaAAQAaAAASgIQAYgJABgUQAAgNgHgIQgIgIgLAAIhWAAQgMAAgHAIgAATg3QgHAFgCAIIAsAAIAAgEQAAgGgFgDQgFgEgHgBQgJAAgJAFgAgwg6QAEACABAFQABAFgDAEIAhAAQgBgOgUgEIgGgBQgGAAgDADg");
	this.shape_8.setTransform(878.9,119.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgPAAIAAgOIAoAAQANAAAIAGQAJAHAAANQAAALgLAGQgLAGgPgBIAAAlgAgBgbIAAATQAOAAgBgJQAAgKgLAAIgCAAg");
	this.shape_9.setTransform(856.2,108.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhaAsQgEgOADgLIgOAAIAAgPIALgMIAOgPQgJAAgGgGQgFgHAAgJQAAgOALgJQAKgJAPgBQAYgBALARQAWgQAhAAIALABQAfACAUAYQAUAYgCAfQgCAfgUAVQgUAXgfAAIgLgBIAAgQQgUAQgfABIgDAAQgwAAgKgjgAgIgtQAIABAKAGQAKAFAGAGQAOARAAATQABAegQAUIAHABQAOAAAKgHQATgNACgaQACgRgIgQQgIgOgRgJQgNgGgUAAQgNAAgIADgAg1gUQgMAIgSARIANAAQgFALADAMQAHAVAggCQATgBAMgLQAMgLABgRQABgOgKgLQgKgLgQgBQgQAAgNAKgAhBg7QgFABgCACQAHACADAEQADAFgCAHIAIgHIAIgGQgEgGgHgCIgEAAIgFAAg");
	this.shape_10.setTransform(848.7,119.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AASBOIAAgPQgMAJgJADQgLAEgTAAQgfgBgSgXQgPgVAAgfQAAgzAjgWQAMgHAUAAIAKABIAAARQAVgTAdABQAhABASAWQARAUAAAhQABAhgTAXQgTAYggAAIgLgBgAAMgZQAaAHAAAhQAAAZgRAUQARACAMgHQAUgOgBghQgBgTgNgPQgMgPgTgBQgjgDgFAYQAIgFALAAIAJABgAg/grQgKAOAAATQAAAVALAOQAMAPAVABQARABAMgIQAMgHABgOQABgHgFgGQgFgHgHAAQgGABgEAEQgEADAAAHIABAHIgTAAQgEgTADgRQACgSANgSQgGgCgFAAQgTAAgMAQg");
	this.shape_11.setTransform(827.6,119.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_12.setTransform(798.2,119.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ag/BTQgdgVACggQABgZAUgNQgNgCAAgQQAAgOAQgIQAMgFARAAQAYAAAKAPQAIgMAWgDIANAAIAJgHQAEgEADgGQAJgPAAgQIAbAAIgFARQgEALgGAIQgGAJgJAHQANAHACAPQABAIgDAIIgPAAQAHABAIAIQANAMAAAXQAAAegcAVQgaASgjAAQgkAAgagTgAg7AHQgIAJABALQAAARAWALQATAKAYAAQAZAAATgJQAVgKABgUQABgKgIgJQgHgHgSAAIhIAAQgMAAgIAHgAAWgiQgIABgDAEQgGAFAAAHIAeAAQAJAAAHACQADgEgDgHQgEgJgSAAIgHABgAgwghQAFADABAEQABAFgDAEIAhAAQgDgMgKgEQgHgDgGAAQgGAAgFADg");
	this.shape_13.setTransform(779.4,117);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_14.setTransform(750.6,119.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AhaAsQgEgOADgLIgOAAIAAgPIALgMIAOgPQgJAAgGgGQgFgHAAgJQAAgOALgJQAKgJAPgBQAYgBALARQAWgQAhAAIALABQAfACAUAYQAUAYgCAfQgCAfgUAVQgUAXgfAAIgLgBIAAgQQgUAQgfABIgDAAQgwAAgKgjgAgIgtQAIABAKAGQAKAFAGAGQAOARAAATQABAegQAUIAHABQAOAAAKgHQATgNACgaQACgRgIgQQgIgOgRgJQgNgGgUAAQgNAAgIADgAg1gUQgMAIgSARIANAAQgFALADAMQAHAVAggCQATgBAMgLQAMgLABgRQABgOgKgLQgKgLgQgBQgQAAgNAKgAhBg7QgFABgCACQAHACADAEQADAFgCAHIAIgHIAIgGQgEgGgHgCIgEAAIgFAAg");
	this.shape_15.setTransform(729.9,119.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag5BYQgZgOgCgcQgDgfAZgLQgHgBgEgFQgFgFAAgHIACgIQAIgUAoACQAXABAMARQAMAQgCAVIg1AAQgZAAAAAUQABAPATAIQAPAGAZAAQAaAAARgOQASgOABgVQACgegPgSQgMgOgUAAIg+AAQgeAAAAgUQAAgiBLAAQAdAAAWAMQAaAOAAAVIgUAAQABgNgVgIQgRgGgWgBQgmAAgCALQgBAHAOAAIAyAAQAUAAARANQAcAXAAAyQAAAjgbAXQgaAWgkAAQggAAgVgOgAgqgVQAEADAAAGQABAHgFAEIAmAAQgDgKgHgFQgIgGgMAAIgIABg");
	this.shape_16.setTransform(710.2,117.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhABTQgdgVACggQACgZAUgMQgOgDAAgQQAAgOAPgHQgFgGAAgNQAAgGABgDQAHgQAUgGQAMgEAXAAQA0AAAGAeQABAHgEAHQAFgEAFgIQAKgRABgQIAaAAQgGAXgJANQgHALgJAHQANAGADAPQABAIgEAIIgOAAQAHABAIAIQANAMAAAXQAAAfgdAUQgaATgiAAQgkAAgbgUgAg7AHQgJAJABALQABASAVALQATAJAZAAQAZAAASgJQAWgKABgUQABgKgIgJQgIgHgSAAIhIAAQgLAAgIAHgAAVgiQgHACgEADQgGAFAAAHIAeAAQAKAAAHACQADgDgDgHQgEgKgTAAIgHABgAgxghQAGADABAEQABAFgDAEIAgAAQgCgMgLgEQgGgCgGAAQgGAAgGACgAgDgmQAEgGAFgDQgLgCgDgIQgFgKAEgJQgiAAgIALQgCADAAAEQAAAEACACQADgBAKAAQAZAAAKAPgAAKhMQgDADAAAEQAAAFADADQADADAFAAQAEAAADgDQADgDAAgFQAAgEgDgEQgDgDgEAAQgFAAgDAEg");
	this.shape_17.setTransform(692.2,116.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgiBOQgbgBgSgRQgRgRgBgZQgBgXAIgQIASAAQgCAGACACQADACAFAAQAJAAAHgEQAPgJADgLIABgHQAAgMgIgEQgIgDgHADQAMAEAAAOQAAAIgGAFQgFAEgIAAIgIgBQgOgEAAgRQAAgGACgGQAIgUAdAAQALAAAIAFQAJAFADAJQAQgVAgACQAhABARAgQAMAVAAAZQgBA0gmASQgMAGgOAAQgIAAgKgCIAAgNQgMAKgKADQgJACgPAAIgEAAgAASgUQASAPAAAXQAAAZgPAQIAMABQANAAAIgGQATgOAAgfQAAgTgOgPQgNgPgTgBQgYgBgLAOIABAAQAPAAAKAIgAhNALQAAAMAHAJQAJAMAXAAIANgBQAOgBAKgLQAIgKAAgMQAAgHgEgFQgEgIgNgBQgGgBgKAHQgMAIgEABQgIADgJAAQgJAAgEgDIgBAIg");
	this.shape_18.setTransform(662.7,119.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AASBOIAAgPQgMAJgJADQgLAEgTAAQgfgBgSgXQgPgVAAgfQAAgzAjgWQAMgHAUAAIAKABIAAARQAVgTAdABQAhABASAWQARAUAAAhQABAhgTAXQgTAYggAAIgLgBgAAMgZQAaAHAAAhQAAAZgRAUQARACAMgHQAUgOgBghQgBgTgNgPQgMgPgTgBQgjgDgFAYQAIgFALAAIAJABgAg/grQgKAOAAATQAAAVALAOQAMAPAVABQARABAMgIQAMgHABgOQABgHgFgGQgFgHgHAAQgGABgEAEQgEADAAAHIABAHIgTAAQgEgTADgRQACgSANgSQgGgCgFAAQgTAAgMAQg");
	this.shape_19.setTransform(642.4,119.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgyA4QgWgWAAghQAAgiAWgWQAXgWAhAAQAjAAARAPQAMAKABAPIgSAAQgBgKgPgHQgNgFgQAAQgWAAgRAOQgRAPAAAWQAAAVAPAOQAQAOAWAAQARAAANgKQAPgKAAgRQAAgVgVgGQAIAHAAAPQAAAJgKAHQgJAIgLAAQgOAAgJgJQgKgIAAgOQAAgPAMgJQAMgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgXgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQADgDAAgFQAAgGgDgDQgEgEgFAAQgGAAgDADg");
	this.shape_20.setTransform(624.6,119.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_21.setTransform(597.6,119.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("Ag/BTQgdgVACggQABgZAUgNQgNgCAAgQQAAgOAQgIQAMgFARAAQAYAAAKAPQAIgMAWgDIANAAIAJgHQAEgEADgGQAJgPAAgQIAbAAIgFARQgEALgGAIQgGAJgJAHQANAHACAPQABAIgDAIIgPAAQAHABAIAIQANAMAAAXQAAAegcAVQgaASgjAAQgkAAgagTgAg7AHQgIAJABALQAAARAWALQATAKAYAAQAZAAATgJQAVgKABgUQABgKgIgJQgHgHgSAAIhIAAQgMAAgIAHgAAWgiQgIABgDAEQgGAFAAAHIAeAAQAJAAAHACQADgEgDgHQgEgJgSAAIgHABgAgwghQAFADABAEQABAFgDAEIAhAAQgDgMgKgEQgHgDgGAAQgGAAgFADg");
	this.shape_22.setTransform(578.8,117);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AASBOIAAgPQgMAJgJADQgLAEgTAAQgfgBgSgXQgPgVAAgfQAAgzAjgWQAMgHAUAAIAKABIAAARQAVgTAdABQAhABASAWQARAUAAAhQABAhgTAXQgTAYggAAIgLgBgAAMgZQAaAHAAAhQAAAZgRAUQARACAMgHQAUgOgBghQgBgTgNgPQgMgPgTgBQgjgDgFAYQAIgFALAAIAJABgAg/grQgKAOAAATQAAAVALAOQAMAPAVABQARABAMgIQAMgHABgOQABgHgFgGQgFgHgHAAQgGABgEAEQgEADAAAHIABAHIgTAAQgEgTADgRQACgSANgSQgGgCgFAAQgTAAgMAQg");
	this.shape_23.setTransform(549.5,119.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTAlIAAgfIgPAAIAAgOIAPAAIAAgUIgQAAIAAgOIApAAQAMAAAJAGQAIAHAAANQAAALgKAGQgLAGgQgBIAAAlgAgCgbIAAATQAPAAAAgJQAAgKgMAAIgDAAg");
	this.shape_24.setTransform(535.8,108.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgjBJIAAgSQAFAEAKAAQAKAAAGgSQAGgSAAgVQAAgVgFgSQgIgTgIgBQgKAAgGAFIAAgTQAIgHAQAAQAVAAAOAcQAMAXAAAdQAAAdgMAWQgOAagWAAQgNAAgKgGg");
	this.shape_25.setTransform(535.3,119.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAAA+QgMARgWAAQgeAAgPgXQgOgUABgjQABglAZgXQARgQAXgCQAJgBAHACIAAASQgKgEgPAFQgPAFgKAOQgKAPAAARQAAAvAhAAQAMAAAIgHQAIgGAAgMIAAgQIASAAIAAAPQAAANAIAGQAIAHAMAAQAOAAAKgKQAJgJAAgPQAAgNgKgJQgKgJgOAAIgtAAQgBgcAMgNQALgMAXAAQApABAAAgIAAAIIgPAAQAOAGAJAOQAIANAAAPQAAAegOASQgPATgaAAQgaAAgMgRgAANgpIAbAAIAKABIABgFQAAgOgSAAQgTAAgBASg");
	this.shape_26.setTransform(522.3,119.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgxA4QgXgWAAghQAAgiAXgWQAWgWAgAAQAlAAAQAPQALAKABAPIgSAAQgBgKgOgHQgNgFgQAAQgWAAgRAOQgRAPAAAWQAAAVAQAOQAPAOAWAAQARAAANgKQAPgKAAgRQAAgVgUgGQAHAHAAAPQAAAJgJAHQgJAIgMAAQgNAAgKgJQgKgIABgOQAAgPAMgJQALgJAQAAQAZAAARAOQARAOAAAYQAAAdgUATQgUATgdAAQgfAAgWgWgAgDgSQgEAEAAAFQAAAGAEADQADAEAFAAQAGAAAEgEQAEgDAAgFQAAgGgEgDQgEgEgFAAQgGAAgDADg");
	this.shape_27.setTransform(504.9,119.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAzAkQAMgIAAgMIAAgFQgDgJgTgEQgSgEgXAAQgWAAgRAEQgUAEgDAJIgBAFQABANALAHIgRAAQgPgFABgVQgBgIADgEQAJgTAYgIQASgGAdAAQBDAAAOAhQACAFAAAHQAAAUgOAGg");
	this.shape_28.setTransform(487.4,109.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAaBOIAAgTIAIABQATAAAMgQQANgPAAgTQAAgTgKgPQgKgRgSgCIgJgBQgfAAgEAdQACgDAHgBQAHgBAGACQAMADAFAMQAGALgBAPQgCAYgUAQQgSAPgZAAQglAAgTgVQgPgQAAgUQAAgLAGgLQAGgLAJgGIgTAAQgFAAgEADIAAgTIAVAAQgHgDAAgNQAAgFABgEQAIgSAcAAQARAAAJAGQAMAIACANQAPgaAkgBQAdgBATAaQASAYgCAfQgBAegUAWQgVAXgcAAIgHAAgAg9gKQgJAIAAAJQgBAQAMAJQAMAJAVAAQASAAALgHQAPgIAAgQQAAgMgLgBQgGAAgDADQgEAEAAAGIgRAAIABgQIABgMIgTAAQgMAAgJAIgAgxgwQABAHgEAGIAYAAQgBgLgGgHQgGgFgJAAIgEAAQAEACABAIg");
	this.shape_29.setTransform(486.7,119.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAaBfIAAgjQgXADgRgBQgjgCgVgPQgYgSAAgfQAAgPAJgNQAIgMAMgGQgGgCgDgEQgDgFAAgEQAAgKAJgHQAPgMAcAAQAqABALAYQADgLAJgHQAJgHALAAIATAAIAAASIgEgBQgFAAgBAGQgBACAHAFQAJAHADAEQAHAIABAKQAAATgQAKQgPAHgSgBIAAAnIAQgEIAMgEIAAARIgMAFIgQAEIAAAmgAgrgcQgKAMAAAQQABAhAmAJQAHABAMABQANAAAIgCIAAhSIgrAAQgQAAgKAMgAAugvIAAAeIAEABQAGAAAGgFQAFgEAAgIQAAgIgHgLQgIgLAAgFIAAgDQgGACAAAWgAALg5QgDgLgNgFQgRgGgPAFQAEAEAAAFQAAAFgEADIAEAAIAEAAIAoAAIAAAAg");
	this.shape_30.setTransform(466.6,121);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgDBVQgLASgYAAQgbAAgOgWQgOgVABgiQACgnAXgUQASgQAkAAIALAAIAAASQgTgBgIACQgMABgJAGQgSAOgBAZQAAAUAGANQAIAPARAAQANAAAHgIQAGgHAAgMIAAgXIASAAIAAAXQAAAbAcAAQASAAAKgVQAJgUgCgfQgBgegPgUQgVgbgjAAQgVAAgRAIQgRAJgHAOIgVAAQAGgWAXgNQAXgNAeAAQAXAAAVAJQAPAHAJALQAcAeABA2QABAogPAaQgRAcgeAAQgYAAgLgSg");
	this.shape_31.setTransform(437.5,117);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("Ag5BYQgZgOgCgcQgDgfAZgLQgHgBgEgFQgFgFAAgHIACgIQAIgUAoACQAXABAMARQAMAQgCAVIg1AAQgZAAAAAUQABAPATAIQAPAGAZAAQAaAAARgOQASgOABgVQACgegPgSQgMgOgUAAIg+AAQgeAAAAgUQAAgiBLAAQAdAAAWAMQAaAOAAAVIgUAAQABgNgVgIQgRgGgWgBQgmAAgCALQgBAHAOAAIAyAAQAUAAARANQAcAXAAAyQAAAjgbAXQgaAWgkAAQggAAgVgOgAgqgVQAEADAAAGQABAHgFAEIAmAAQgDgKgHgFQgIgGgMAAIgIABg");
	this.shape_32.setTransform(419.1,117.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("Ag5BYQgZgOgCgcQgDgfAZgLQgHgBgEgFQgFgFAAgHIACgIQAIgUAoACQAXABAMARQAMAQgCAVIg1AAQgZAAAAAUQABAPATAIQAPAGAZAAQAaAAARgOQASgOABgVQACgegPgSQgMgOgUAAIg+AAQgeAAAAgUQAAgiBLAAQAdAAAWAMQAaAOAAAVIgUAAQABgNgVgIQgRgGgWgBQgmAAgCALQgBAHAOAAIAyAAQAUAAARANQAcAXAAAyQAAAjgbAXQgaAWgkAAQggAAgVgOgAgqgVQAEADAAAGQABAHgFAEIAmAAQgDgKgHgFQgIgGgMAAIgIABg");
	this.shape_33.setTransform(401.4,117.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#33691E").s().p("EhHAAFKIAAqTMCOBAAAIAAKTg");
	this.shape_34.setTransform(688,119.2);

	this.instance = new lib.Image();
	this.instance.parent = this;
	this.instance.setTransform(-145,-69,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(495,291,1426.1,874.1);
// library properties:
lib.properties = {
	width: 1280,
	height: 720,
	fps: 30,
	color: "#999999",
	opacity: 1.00,
	manifest: [
		{src:"images/civic3_atlas_.jpg?1565003943682", id:"civic3_atlas_"},
		{src:"sounds/audio5.mp3?1565003948134", id:"audio5"},
		{src:"sounds/audio1.mp3?1565003948134", id:"audio1"},
		{src:"sounds/audio3.mp3?1565003948134", id:"audio3"},
		{src:"sounds/audio4.mp3?1565003948134", id:"audio4"},
		{src:"sounds/audio2.mp3?1565003948134", id:"audio2"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;